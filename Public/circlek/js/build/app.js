/**
 * Script ejemplo para elixir.
 */

$(document).ready(function () {
  if ($('#ContractType_IndexContract').length) {
        //Inicia el Script
      
   }
});
//courseevaluation-CompanyEvaluation
$(document).ready(function() { // 6,32 5,38 2,34
        if ($('#courseevaluation-CompanyEvaluation').length) {

            var total = parseInt($('#Work_capacities').val());
            var totalContratados = parseInt($('#EmployeesTotal').val());

           // var Vacantes = ( (total - totalContratados) * 100 / total);
            var Ocupacion = (parseInt($('#EmployeesTotal').val())) * 100 / parseInt($('#Work_capacities').val());
            var Capacitacion = (parseInt($('#ProgressTotal').val()));

            //Con esta variable obtengo el porcentaje de la vista del div
            var Sol = parseInt($('#Solicitantes').text());
            var Psy = parseInt($('#Psy').text());
            var Con = parseInt($('#Con').text());


        //INICIA SCRIPT
        $(".graphOcupation").circliful({
            animation: 1,
            animationStep: 2,
            foregroundBorderWidth: 15,
            backgroundBorderWidth: 10,
            percent: (total > 0) ? Ocupacion : 0,
            iconColor: '#2daae3',
            icon: 'f0c0',
            iconSize: '40',
            iconPosition: 'middle',
            foregroundColor: '#2daae3',
        });
        $(".graphVacant").circliful({
                animation: 1,
                animationStep: 5,
                foregroundBorderWidth: 7,
                backgroundBorderWidth: 7,
                textSize: 28,
                textStyle: 'font-size: 12px;',
                textColor: '#666',
                multiPercentage: 1,
                percentages: [
                    {'percent': Sol, 'color': '#3180B8', 'title': 'Elemento1' },
                    {'percent': Psy, 'color': '#4ADBEA', 'title': 'Elemento2' },
                    {'percent': Con, 'color': '#49EBA8', 'title': 'Elemento3' },
                ],
                multiPercentageLegend: 0,
                replacePercentageByText: '',
                backgroundColor: '#eee',
                icon: 'f1ad',
                iconPosition: 'middle',
                iconColor: '#273B4E',
        });
        // $(".graphVacant").circliful({
        //     animation: 1,
        //     animationStep: 2,
        //     foregroundBorderWidth: 15,
        //     backgroundBorderWidth: 10,
        //     percent: (total > 0) ? Vacantes : 0,
        //     iconColor: '#00a65a',
        //     icon: 'f1ad',
        //     iconSize: '40',
        //     iconPosition: 'middle',
        //     foregroundColor: '#00a65a'
        // });
        $(".graphTraining").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 15,
            backgroundBorderWidth: 10,
            percent: (total > 0) ? Capacitacion : 0,
            iconColor: '#ff7676',
            icon: 'f2b5',
            iconSize: '40',
            iconPosition: 'middle',
            foregroundColor: '#ff7676',
        });
    }
});
//employees/kardex.blade.php
$(document).ready(function() { // 6,32 5,38 2,34
	if ($('#kardex_Employee').length) {

        //INICIA SCRIPT
//start modal new employees
        $('#EmployeeNew').click(function() {
            console.log('Hola');
            var nameEmployee = $('#selectEmployee').val();
            var company = $('#newCompany').val();
            var jobtitle = $('#inputjobTitle').val();
            var jobcenter = $('#inputJobCenter').val();
            var nameEmployee = $("#selectEmployee option:selected").html().replace('\n','');
            var applicantID = $('#selectEmployee option:selected').val();
            //var keyEmployee = $('#keyEmployee').val();

            $.ajax({
              type: 'POST',
              url: "update",
              data: {
                 _token: $("meta[name=csrf-token]").attr("content"),
                 Applicant_id: applicantID,
                 Company : company,
                 inputjobTitle : jobtitle,
                 inputJobCenter : jobcenter,
                 nameEmployee : nameEmployee,
                 //keyEmployee : keyEmployee
             },
             success: function(data) {
                 if (data.errors) {
                    faltante();
                }
                else{
                    correcto();
                    location.reload();
                }
            }
        });
        })

        $('#newEmployee').click(function() {
            $('#modalTitle').text('Crear nuevo empleado');
            $('#EmployeeUp').text('Guardar');
            $('#nameEmployee').val(1);
            $('#nameEmployee').trigger('change');
            $('#inputjobTitle').val('');
            $('#inputJobCenter').val('');
        });

//end modal new employees
        
//modal employees update 
        $('.btnUpdateEmployee').click(function() {
        	var all = $(this);
            $('#Applicant_id').val(all.attr('data-id'));
        	$.ajax({
        		type: 'POST',
        		url: "search",
        		data: {
        			_token: $("meta[name=csrf-token]").attr("content"),
        			Employee_id: all.attr('data-id'),
        		},
        		success: function(data) {
        			if (data.errors) {
        			}
        			else{
        				$('#inputCompany').val(data.inputCompany);
        				$('#updatejobTitle').val(data.inputjobTitle).trigger('change.select2');
        				$('#updateJobCenter').val(data.inputJobCenter).trigger('change.select2');
        				$('#nameEmployee').val(data.name);
                    }
                }
            });
        });

        $('#EmployeeUp').click(function(){
            var company = $('#inputCompany').val();
            var jobtitle = $('#updatejobTitle').val();
            var jobcenter = $('#updateJobCenter').val();
            var applicantID = $('#Applicant_id').val();
            var nameEmployee = $('#nameEmployee').val();

            $.ajax({
              type: 'POST',
              url: "update",
              data: {
                 _token: $("meta[name=csrf-token]").attr("content"),
                 Applicant_id: applicantID,
                 Company : company,
                 inputjobTitle : jobtitle,
                 inputJobCenter : jobcenter,
                 nameEmployee : nameEmployee
             },
             success: function(data) {
                 if (data.errors) {
                    faltante();
                }
                else{
                    correcto();
                    location.reload();
                }
            }
        });
        })
//end modal employees update

//sweet alert
        function correcto() {
        	swal({
        		title: "Datos guardados correctamente",
        		type: "success",
        		timer: 2000,
      showCancelButton: false, // There won't be any cancel button
      showConfirmButton: false // There won't be any confirm button
  })
            .catch(swal.noop);
        }

        function faltante() {
        	swal({
        		title: "¡Espera!",
        		text: "Faltan datos por ingresar",
        		imageUrl: "../img/icon-lte-09.png",
        		imageWidth: 200,
        		imageHeight: 200,
        		timer: 2000,
        		showCancelButton: false,
        		showConfirmButton: false
        	}).catch(swal.noop);
        }
//end sweet alert
    }
});
$(document).ready(function () {
    if ($('#tab_demographics').length) {
        //Inicia el Script
        $('.deletexp').click(function() {
        var xp = $(this).parent('td').parent('tr');
        $.ajax({
         type: "POST",
         url: "../../../job_title_experience/delete",
         data: {
             _token: $("meta[name=csrf-token]").attr("content"),
             Id : $(this).attr("data-info")
         },
         success: function(data){
                xp.remove();
         }
        });
    });

    }
});
$(document).ready(function () {
    if ($('#tab_knowledges').length) {
        //Inicia el Script
        $('.delete_function').click(function() {
            var fn = $(this).parent('td').parent('tr');
            $.ajax({
               type: "POST",
               url: "../../../jobtitle_office/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                fn.remove();
            }
        });
        });
        $('.delete_mac').click(function() {
            var mac = $(this).parent('td').parent('tr');
            
            $.ajax({
               type: "POST",
               url: "../../../jobtitle_machine/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                mac.remove();
                
            }
        });
        });
        $('.delete_soft').click(function() {
            var soft = $(this).parent('td').parent('tr');
            $.ajax({
               type: "POST",
               url: "../../../jobtitle_software/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                soft.remove();
                
            }
        });
        });

    }
});
$(document).ready(function () {
    if ($('#family_datas').length) {
        //Inicia el Script
        $('.delete_fam').click(function() {
            var fam = $(this).parent('td').parent('tr');

            $.ajax({
               type: "POST",
               url: "../family_data/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                fam.remove();
            }
        });
        });
    }
});
$(document).ready(function () {
    if ($('#personal_references').length) {
        //Inicia el Script
        $('.deletework_PersonalReferences').click(function() {
            var ref = $(this).parent('td').parent('tr');
            $.ajax({
               type: "POST",
               url: "../personal_references/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                ref.remove();
            }
        });
        });
    }
});
$(document).ready(function () {
    if ($('#scholarship_datas').length) {
        //Inicia el Script
        $('.delete_schools').click(function() {
            var schools = $(this).parent('td').parent('tr');
            $.ajax({
               type: "POST",
               url: "../scholarship_datas/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                schools.remove();
            }
        });
        });
    }
});
$(document).ready(function () {
    if ($('#work_history').length) {
        //Inicia el Script
        $('.deletework_h').click(function() {
            var work = $(this).parent('td').parent('tr');
           
            $.ajax({
               type: "POST",
               url: "../work_history/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                 work.remove();
            }
        });
        });
    }
});
//JobAplication/showdocument.blade.php
$(document).ready(function () {
    if ($('#JobAplication_ShowDocument').length) {

        //Inicia el Script

        /**
         * Inicializa el venobox.
         */
        $('.venobox').venobox({
            bgcolor: '#5dff5e',         // default: '#fff'
            titleattr: 'data-title',    // default: 'title'
            idattr: 'data-id',
            numeratio: true,            // default: false
            infinigall: true    
        }); 

        /**
         * Oculta o muestra el dialogo de comentarios.
         */
        $(".comentary-target").on('click', function () {
            var CommentaryPop = $(this).parent().parent().parent().children(".comentary-pop")[0];
            
            if ($(CommentaryPop).attr("isshow") == "true") {
                $(CommentaryPop).hide(500);
                $(CommentaryPop).attr("isshow", "false");
            } else {
                $(CommentaryPop).show(500);
                $(CommentaryPop).attr("isshow", "true");
            }
        });

        /**
         * Trigger para mandar el comentario de un documento al servidor.
         */
        $(".commentary-btn").on('click', function () {
            ProgressDialogModal.ShowModal();
            var commentaryMessage = $(this).parent().find('#comment').val();
            var idDocument = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "../../documents/commentary",
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: idDocument,
                    messageDocument : commentaryMessage,
                },
                success: function(data) {
                    ProgressDialogModal.HideModal();

                    swal({
                        title: "El comentario se ha modificado",
                        type: "success",
                        timer: 0,
                        showCancelButton: false, // There won't be any cancel button
                        showConfirmButton: false // There won't be any confirm button
                      }).catch(swal.noop);
                },
                error: function (error) {
                    ProgressDialogModal.HideModal();
                    swal({
                        title: "Oops.. No se pudo cambiar el comentario",
                        type: "error",
                        timer: 0,
                        showCancelButton: false,
                        showConfirmButton: false
                      }).catch(swal.noop);
                }
            });
        });

        /**
         * Trigger para los botones de validacion.
         */
        $(".validatedButton").on('click', function () {
            var isValidated = parseInt($(this).attr('data-validated'));
            var idDocument = $(this).attr('data-id');
            var nameDocument = $(this).attr('data-name');
            var idUser = $(this).attr('data-user');

            swal({
                title: '¿Estas seguro?',
                text: (isValidated) ? "Vas a invalidar el documento" : "El documento sera valido",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: (isValidated) ? "Invalidar" : "Validar",
                cancelButtonText: 'Cancelar',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
              }).then((result) => {
                if (result) {
                    
                    validatedDocument(this,isValidated, idDocument, nameDocument, idUser);
                } 
                else {
                  
                }
              })
        });

        /**
         * Valida o invalida el documento.
         * 
         * @param {any} button Boton que lo ejecuta.
         * @param {any} isValidated Estado a asignar al documento.
         * @param {any} id Id del Documento.
         * @param {any} name Nombre del documento.
         */
        function validatedDocument(button, isValidated, id, name, idUser)
        {
            ProgressDialogModal.ShowModal();
            
            var ParentBase = $(button).parent().parent().parent().parent()[0];
            var venoboxLabel = $(ParentBase).find("#a-venobox");
            var validateButton = button;

            $.ajax({
                type: "POST",
                url: isValidated ? "../../documents/invalidated" : "../../documents/verified",
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: id,
                    title : name,
                    iduser : idUser
                },
                success: function(data) {
                    ProgressDialogModal.HideModal();

                    if(!isValidated)
                    {
                        $(venoboxLabel).addClass("venobox-validated");
                        $(validateButton).attr("data-validated","1");
                        $(validateButton).html("Invalidar");
                    }
                    else{
                        $(venoboxLabel).removeClass("venobox-validated");
                        $(validateButton).attr("data-validated","0");
                        $(validateButton).html("Validar");
                    }
                    //location.reload();
                    swal({
                        title: isValidated ? "El documento ha sido invalidado" : "El documento ha sido validado",
                        type: "success",
                        timer: 0,
                        showCancelButton: false, // There won't be any cancel button
                        showConfirmButton: false // There won't be any confirm button
                      }).catch(swal.noop);
                },
                error: function (error) {
                    ProgressDialogModal.HideModal();
                    swal({
                        title: "Oops.. No se pudo cambiar el estado",
                        type: "error",
                        timer: 0,
                        showCancelButton: false,
                        showConfirmButton: false
                      }).catch(swal.noop);
                }
            });
        }


    }
});
//Tracking-index.blade.php
$(document).ready(function() { // 6,32 5,38 2,34
	if ($('#tracking_index').length) {
        //INICIA SCRIPT
        $(".matchModal").on('click', function(event) {
        	var matchUser = $(this).attr('data-info');
        	$.ajax({
        		type: 'POST',
        		url: 'matchporcentaje',
        		data: {
        			'_token': $('meta[name=csrf-token]').attr('content'),
        			'applicant' : matchUser
        		},
                
        		success: function (data) {
        			$('#id_applicant').val(data.personal_information[0].general_personal_data_id);
        			$('#name_applicant').text(data.personal_information[0].first_name + ' ' +data.personal_information[0].last_name);
        			$('#work_applicant').text(data.work[0]['job_title_name']);
        			$('#test-circle').empty();
        			$('#test-circle').circliful({
        				foregroundColor:( data.porcentaje[0].percentage_match > 69 ) ? "green": "red" ,
        				backgroundColor: "#eee",
        				pointColor: "none",
        				fillColor: 'none',
        				foregroundBorderWidth: 20,
        				backgroundBorderWidth: 10,
        				pointSize: 28.5,
        				fontColor: '#aaa',
        				animation: 1,
        				animationStep: 5,
        				icon:'A AVANCE ',
        				iconSize: '17',
        				iconColor: '#999',
        				iconPosition: 'middle',
        				target: 0,
        				start: 0,
        				showPercent: 1,
        				percentageTextSize: 22,
        				textAdditionalCss: '',
        				targetPercent: 0,
        				targetTextSize: 17,
        				targetColor: '#2980B9',
        				textStyle: 'top',
        				textColor: '#000',
        				multiPercentage: 0,
        				percentages: null,
        				textBelow: 1,
        				noPercentageSign: false,
        				replacePercentageByText: null,
        				halfCircle: false,
        				animateInView: false,
        				decimals: 0,
        				alwaysDecimals: false,
        				percent: data.porcentaje[0].percentage_match
        			});
        			document.getElementById("VerDetalles").disabled = false;
        			$('#matchModal').modal('show');
        		}
        	});
        });

        $(document).on('click', '#VerDetalles', function() {
        	$.ajax({
        		type: 'POST',
        		url: '../tracking/matchdetalles',
        		data: {
        			'_token': $('meta[name=csrf-token]').attr('content'),
        			'applicant' : $('#id_applicant').val()
        		},
        		success: function (data) {
        			var lenguajes = "";
        			var lenguajesnot = "";
        			var level="";
        			var levelnot="";
        			var carrer="";
        			var carrernot="";
        			var aca_grade="";
        			var aca_grade_not="";
        			var arraylenguajes=[];
        			var array_aca_grade=[];
        			var array_carrer=[];
        			$('#gender_applicant').empty();
        			$('#edad_applicant').empty();
        			$('#civilstatus_applicant').empty();
        			$('#lan_applicant').empty();
        			$('#lannot_applicant').empty();
        			$('#aca_grade_applicant').empty();
        			$('#aca_grade_not_applicant').empty();
        			$('#carrer_applicant').empty();
        			$('#carrer_not_applicant').empty();
        			$('#level_applicant').empty();
        			$('#levelnot_applicant').empty();
        			$('#gender_applicant').text(data.personal_information[0].gender);
        			$('#edad_applicant').text(data.personal_information[0].age);
        			$('#civilstatus_applicant').text(data.personal_information[0].civil_status);

        			if (data.personal_information[0].gender == data.work[0].gender || data.work[0].gender == "Indistinto") {
        				$('#gender_applicant').css("color","green");
        			}
        			else{
        				$('#gender_applicant').css("color","red");
        			}
        			if ((data.personal_information[0].age>=data.work[0].min_age)&&(data.personal_information[0].age<=data.work[0].max_age)) {
        				$('#edad_applicant').css("color","green")
        			}
        			else{
        				$('#edad_applicant').css("color","red")
        			}
        			for (var i = 0; i < data.lanasp[0].length; i++) {
        				arraylenguajes[i]=0;
        				for (var c = 0; c < data.lansol[0].length; c++) {
        					if (data.lanasp[0][i].language == data.lansol[0][c].language) {
        						arraylenguajes[i]=1;
        					}
        				}
        				if (arraylenguajes[i]==1) 
        				{
        					if (data.lanasp[0][i].level >= 70) {
        						level = level + data.lanasp[0][i].level + '.<br>';
        						lenguajes = lenguajes + data.lanasp[0][i].language + '.<br>';
        					}
        					else
        					{
        						lenguajesnot = lenguajesnot + data.lanasp[0][i].language + '.<br>';
        						levelnot = levelnot + data.lanasp[0][i].level + '.<br>';
        					}
        				}
        				else{
        					lenguajesnot = lenguajesnot + data.lanasp[0][i].language + '.<br>';
        					levelnot = levelnot + data.lanasp[0][i].level + '.<br>';
        				}	
        			}
        			if (data.personal_information[0].civil_status == data.work[0].civil_status || data.work[0].civil_status == "Indistinto") {
        				$('#civilstatus_applicant').css("color","green");
        			}
        			else
        			{
        				$('#civilstatus_applicant').css("color","red");
        			}
        			for (var i = 0; i < data.aca_grade[0].length; i++) {
        				array_aca_grade[i]=0
        				for (var c = 0; c < data.aca_grade_sol[0].length; c++) {
        					if (data.aca_grade[0][i] == data.aca_grade_sol[0][c]) {
        						array_aca_grade[i]=1;

        					}
        				}
        				if (array_aca_grade[i]==1) {
        					aca_grade = aca_grade + data.aca_grade[0][i] + '.<br>';
        				}
        				else{
        					aca_grade_not = aca_grade_not + data.aca_grade[0][i] + '.<br>';
        				}	
        			}

        			for (var i = 0; i < data.degree_name[0].length; i++) {
        				array_carrer[i]=0;
        				for (var c = 0; c < data.degree_name_sol[0].length; c++) {
        					if (data.degree_name[0][i] == data.degree_name_sol[0][c]) {
        						array_carrer[i]=1;
        					}
        				}
        				if (array_aca_grade[i]==1) {
        					carrer = carrer + data.degree_name[0][i] + '.<br>';
        				}
        				else{
        					carrernot = carrernot + data.degree_name[0][i] + '.<br>';
        				}	
        			}
        			$('#lan_applicant').append(lenguajes).css("color","green");
        			$('#lannot_applicant').append(lenguajesnot).css("color","black");
        			$('#aca_grade_applicant').append(aca_grade).css("color","green");
        			$('#aca_grade_not_applicant').append(aca_grade_not).css("color","black");
        			$('#carrer_applicant').append(carrer).css("color","green");
        			$('#carrer_not_applicant').append(carrernot).css("color","black");
        			$('#level_applicant').append(level).css("color","green");
        			$('#levelnot_applicant').append(levelnot).css("color","black");
			// document.getElementById("VerDetalles").disabled = true;
		}
	});
$('#Modaldetalles').modal('show');
});
    // call the tablesorter plugin 
    $("#tableAspirants").tablesorter({ 
        // pass the headers argument and assing a object 
        headers: { 
            // assign the secound column (we start counting zero) 
            1: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }, 
            // assign the third column (we start counting zero) 
            6: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            } 
        } 
    });
}
});
//training/_formmodule.blade.php
$(document).ready(function() {
        //START SCRIPT
//start modal new employees
        $('#ModuleNew').click(function() {
            console.log('Hello');
            let course = $('#course').val();
            let moduleName = $('#moduleName').val();

            $.ajax({
                type: 'POST',
                url: "../module/create",
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    course: course,
                    moduleName : moduleName
                },
                success: function(data) {
                    if (data.errors) {
                        faltante();
                    }
                    else{
                        correcto();
                        location.reload();
                    }
                }
            });
        });

//end modal new employees

//sweet alert
        function correcto() {
            swal({
                title: "Datos guardados correctamente",
                type: "success",
                timer: 2000,
                showCancelButton: false, // There won't be any cancel button
                showConfirmButton: false // There won't be any confirm button
            })
                .catch(swal.noop);
        }

        function faltante() {
            swal({
                title: "¡Espera!",
                text: "Faltan datos por ingresar",
                imageUrl: "../img/icon-lte-09.png",
                imageWidth: 200,
                imageHeight: 200,
                timer: 2000,
                showCancelButton: false,
                showConfirmButton: false
            }).catch(swal.noop);
        }
//end sweet alert

});
//jobapplication/forms/general_knowledges.blade.php
$(document).ready(function() {
	if ($('#general_knowledge').length) {
		//INICIA SCRIPT
		$('.deleteLanguage').click(function() {
		// deleteLan();
		var languageJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('languageGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				languageJob.remove();
			}
		});
	});
		$('.deleteOficina').click(function() {
		// deleteLan();
		var officeJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('officeGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				officeJob.remove();
			}
		});
	});
		$('.deleteMaquinaria').click(function() {
		// deleteLan();
		var machineJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('machineGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				machineJob.remove();
			}
		});
	});
		$('.deleteSoftware').click(function() {
		// deleteLan();
		var softJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('softGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				softJob.remove();
			}
		});
	});
		$('.deleteOtros').click(function() {
		// deleteLan();
		var otherJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('otherGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				otherJob.remove();
			}
		});
	});


	}
	
});
//# sourceMappingURL=app.js.map
