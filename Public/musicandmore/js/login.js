$(document).ready(function() {

	var url = (window.location.pathname).split("/");
	validador()
	function validador(){
		var temp = url[url.length - 1];
		if (temp == 'login') {
			$('img[name=logo_company]').focus();
		}
		else{
			$('input[name=user_auth]').focus();
		}
	}

	//login principal
	$("#password").on("keyup",function(){
		if($(this).val())
			$(".glyphicon-eye-open").show();
		else
			$(".glyphicon-eye-open").hide();
	});
	$(".glyphicon-eye-open").mousedown(function(){
		$("#password").attr('type','text');
	}).mouseup(function(){
		$("#password").attr('type','password');
	}).mouseout(function(){
		$("#password").attr('type','password');
	});

	//vista register
	$("#password_register").on("keyup",function(){
	if($(this).val())
		$("#glyphicon-eye-open1").show();
	else
		$("#glyphicon-eye-open1").hide();
	});
	$("#glyphicon-eye-open1").mousedown(function(){
		$("#password_register").attr('type','text');
	}).mouseup(function(){
		$("#password_register").attr('type','password');
	}).mouseout(function(){
		$("#password_register").attr('type','password');
	});

	$("#password_confirmation").on("keyup",function(){
	if($(this).val())
		$("#glyphicon-eye-open2").show();
	else
		$("#glyphicon-eye-open2").hide();
	});
	$("#glyphicon-eye-open2").mousedown(function(){
		$("#password_confirmation").attr('type','text');
	}).mouseup(function(){
		$("#password_confirmation").attr('type','password');
	}).mouseout(function(){
		$("#password_confirmation").attr('type','password');
	});

	//vista login alternative 
	$("#password_alternative").on("keyup",function(){
	if($(this).val())
		$(".glyphicon-eye-open").show();
	else
		$(".glyphicon-eye-open").hide();
	});
	$(".glyphicon-eye-open").mousedown(function(){
		$("#password_alternative").attr('type','text');
	}).mouseup(function(){
		$("#password_alternative").attr('type','password');
	}).mouseout(function(){
		$("#password_alternative").attr('type','password');
	});



	//alternative_login
	$('#btn_loginalternative').click(function() {
		$.ajax({
			type:'POST',
			url: '../circlek/validationuser',
			data: {
				'_token': $('meta[name=csrf-token]').attr('content'),
				'user'    : $('input[name=user_auth]').val(),
				'pass'	  : $('input[name=user_password]').val()
			},
			success: function (data) {	
				if ((data.errors)) { 
					if(data.errors.pass || data.errors.user){
						swal("Algo anda mal", 'Por favor ingresa credenciales validas' , "error");	
					}
					if (data.errors.Auth) {
						swal("Algo anda mal", data.errors.Auth , "error");	
					}
					if (data.errors.EmailExists) {
						redirect(data.errors.EmailExists);
					} 
				} else {
					emailInsert(data.id);
				}
			}
		}); 
	});

	function emailInsert(id){
		swal({
			title: 'Actualización de Datos',
			html: '<h3>Ingresa un correo válido</h3>'+'<input id="correo_user" class="swal2-input" autofocus placeholder="User ID">'+
			'<h3>Ingresa una nueva contraseña</h3>'+'<input id="pass_user" type="password" class="swal2-input">',
			showCancelButton: true,
			confirmButtonText: 'Guardar',
			showLoaderOnConfirm: true,
			allowOutsideClick: false
		}).then(function () {
			$.ajax({
				type:'POST',
				url: '../public/useremail',
				data: {
					'_token': $('meta[name=csrf-token]').attr('content'),
					'user'    : id,
					'correo' : $('#correo_user').val(),
					'pass_auth' : $('#pass_user').val()
				},
				success: function (data) {	
					redirect();
				}, error: function (xhr, ajaxOptions, thrownError) {
					swal("Error en los campos!", "Por favor intenta otra vez", "error");
				}
			}); 
		}).catch(swal.noop)
	}

	function redirect(texto){
		swal({
			title: 'Cuenta Actualizada',
			text: texto,
			timer: 5000,
			allowOutsideClick: false,showLoaderOnConfirm: true
		}).then(
		function () {window.location.href = "../public/login";},
		function (dismiss) {
			if (dismiss === 'timer') {
				window.location.href = "../public/login";
			}
		}
		).catch(swal.noop)
	}
	//end_alternative_login
});
