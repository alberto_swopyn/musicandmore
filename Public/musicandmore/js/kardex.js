$(document).ready(function() {

//start modal new employees
        $('#EmployeeNew').click(function() {
            var idEmployee = $('#selectEmployee').val();
            var company = $('#newCompany').val();
            var jobtitle = $('#inputjobTitle').val();
            var jobcenter = $('#inputJobCenter').val();
            var nameEmployee = $("#selectEmployee option:selected").html().replace('\n','');
            var applicantID = $('#selectEmployee option:selected').val();
            //var keyEmployee = $('#keyEmployee').val();
            console.log(jobcenter);
            $.ajax({
              type: 'POST',
              url: "create/employee/new",
              data: {
                 _token: $("meta[name=csrf-token]").attr("content"),
                 Applicant_id: applicantID,
                 Company : company,
                 inputjobTitle : jobtitle,
                 inputJobCenter : jobcenter,
                 nameEmployee : nameEmployee,
                 idEmployee: idEmployee
             },
             success: function(data) {
                 if (data.errors) {
                    faltante();
                }
                else{
                    correcto();
                    location.reload();
                }
            }
        });
        });

//end modal new employees
        


//sweet alert
        function correcto() {
        	swal({
        		title: "Datos guardados correctamente",
        		type: "success",
        		timer: 2000,
      showCancelButton: false, // There won't be any cancel button
      showConfirmButton: false // There won't be any confirm button
  })
            .catch(swal.noop);
        }

        function faltante() {
        	swal({
        		title: "¡Espera!",
        		text: "Faltan datos por ingresar",
        		imageUrl: "../img/icon-lte-09.png",
        		imageWidth: 200,
        		imageHeight: 200,
        		timer: 2000,
        		showCancelButton: false,
        		showConfirmButton: false
        	}).catch(swal.noop);
        }
//end sweet alert
    
});