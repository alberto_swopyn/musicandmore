<?php 
 
use Illuminate\Database\Migrations\Migration; 
use Illuminate\Database\Schema\Blueprint; 
use Illuminate\Support\Facades\Schema; 
 
class CreateTaskDocumentsTable extends Migration 
{ 
    /** 
     * Run the migrations. 
     * 
     * @return void 
     */ 
    public function up() 
    { 
        Schema::create('task_documents', function (Blueprint $table) { 
            $table->increments('id'); 
            $table->string('name'); 
            $table->integer('task_id')->unsigned(); 
            $table->timestamps(); 
 
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade'); 
        }); 
    } 

    /** 
     * Reverse the migrations. 
     * 
     * @return void 
     */ 
    public function down() 
    { 
        Schema::dropIfExists('task_documents'); 
    } 
} 