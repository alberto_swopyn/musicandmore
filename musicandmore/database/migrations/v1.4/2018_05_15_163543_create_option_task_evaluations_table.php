<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionTaskEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_task_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_evaluations_id')->unsigned();
            $table->string('option');
            $table->string('validation');
            $table->double('value', 4, 2);
            $table->timestamps();

            $table->foreign('task_evaluations_id')->references('id')->on('task_evaluations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_task_evaluations');
    }
}
