<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_mensajes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('assignment_conversation_id');
            $table->integer('id_assignments')->unsigned();
            $table->integer('id_task')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->string('message');
            $table->boolean('check');
            $table->timestamps();

            $table->foreign('id_assignments')->references('id')->on('assignments')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_task')->references('id')->on('tasks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_mensajes');
    }
}
