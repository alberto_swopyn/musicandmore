<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssistancesAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistances_admin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_group');
            $table->integer('id_teacher');
            $table->datetime('date');
            $table->integer('assistance');
            $table->timestamps();

            $table->foreign('id_teacher')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_group')->references('id')->on('groups')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistances_admin');
    }
}
