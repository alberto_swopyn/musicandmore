<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
        Schema::create('payroll_teacher', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_pay');
            $table->integer('user_id')->unsigned();
            $table->integer('id_group')->unsigned();
            $table->integer('num_hour');
            $table->float('quantity_pay');
            $table->string('comment');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
             $table->foreign('id_group')->references('id')->on('groups')->onUpdate('cascade')->onDelete('cascade');
        });
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll');
    }
}
