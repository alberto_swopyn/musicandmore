<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssingmentPaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('assingment_pays', function (Blueprint $table) {
            $table->increments('id');
            //$table->string('assignments_id')->nullable();
            $table->integer('pays_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->boolean('is_completed');
            $table->string('comment');
            $table->timestamps();

            $table->foreign('pays_id')->references('id')->on('pays')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assingment_pays');
    }
}
