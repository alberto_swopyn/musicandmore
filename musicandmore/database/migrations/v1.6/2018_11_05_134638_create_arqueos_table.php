<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArqueosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arqueos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cutbox_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->integer('mil');
            $table->integer('quinientos');
            $table->integer('doscientos');
            $table->integer('cien');
            $table->integer('cincuenta');
            $table->integer('veinte');
            $table->integer('diez');
            $table->integer('cinco');
            $table->integer('dos');
            $table->integer('uno');
            $table->integer('cincuenta_centavos');
            $table->integer('veinte_centavos');
            $table->integer('diez_centavos');
            $table->datetime('date');
            $table->timestamps();

            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cutbox_id')->references('id')->on('cut_boxs')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arqueos');
    }
}
