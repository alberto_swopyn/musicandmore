<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionSecondsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_seconds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question_seconds_id')->nullable();
            $table->integer('sections_id')->unsigned();
            $table->string('text');
            $table->string('supports_1')->nullable();
            $table->string('supports_2')->nullable();
            $table->string('supports_3')->nullable();
            $table->string('supports_4')->nullable();
            $table->string('supports_5')->nullable();
            $table->string('supports_6')->nullable();
            $table->string('supports_7')->nullable();
            $table->string('supports_8')->nullable();
            $table->string('supports_9')->nullable();
            $table->string('supports_10')->nullable();
            $table->string('answers_1')->nullable();
            $table->string('answers_2')->nullable();
            $table->string('answers_3')->nullable();
            $table->timestamps();

            $table->foreign('sections_id')->references('id')->on('sections')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_seconds');
    }
}
