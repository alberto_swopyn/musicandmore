<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('section_grades_id')->nullable();
            $table->integer('users_id')->unsigned();
            $table->integer('sections_id')->unsigned();
            $table->integer('grade');
            $table->timestamps();

            $table->foreign('sections_id')->references('id')->on('sections')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_grades');
    }
}
