<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('name');
            $table->string('id_company');
            $table->string('job_title_profile_id');
            $table->string('profile_job_center_id');

            $table->foreign('profile_job_center_id')->references('id')->on('profile_job_centers')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('job_title_profile_id')->references('id')->on('job_title_profiles')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('id_company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
