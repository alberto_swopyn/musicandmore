<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachedFormQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attached_form_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attached_form_questions_id')->nullable();
            $table->integer('attached_forms_id')->unsigned();
            $table->string('type');
            $table->string('text');
            $table->timestamps();

            $table->foreign('attached_forms_id')->references('id')->on('attached_forms')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attached_form_questions');
    }
}
