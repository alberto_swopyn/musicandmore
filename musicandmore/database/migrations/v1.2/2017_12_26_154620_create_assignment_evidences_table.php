<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentEvidencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_evidences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('assignment_evidences_id')->nullable();
            $table->integer('assignments_id')->unsigned();
            $table->string('type');
            $table->string('name');
            $table->binary('document');
            $table->timestamps();

            $table->foreign('assignments_id')->references('id')->on('assignments')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_evidences');
    }
}
