<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachedFormAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attached_form_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attached_form_answers_id')->nullable();
            $table->integer('attached_form_questions_id')->unsigned();
            $table->string('text');
            $table->timestamps();

            $table->foreign('attached_form_questions_id')->references('id')->on('attached_form_questions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attached_form_answers');
    }
}
