<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAttachedFormAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_attached_form_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_attached_form_answers')->nullable();
            $table->integer('assignments_id')->unsigned();
            $table->integer('attached_form_answers_id')->unsigned();
            $table->timestamps();

            $table->foreign('assignments_id')->references('id')->on('assignments')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('attached_form_answers_id')->references('id')->on('attached_form_answers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_attached_form_answers');
    }
}
