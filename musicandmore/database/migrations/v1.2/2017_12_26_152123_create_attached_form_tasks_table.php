<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachedFormTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attached_form_tasks', function (Blueprint $table) {
           $table->increments('id');
           $table->string('attached_form_tasks_id')->nullable();
           $table->integer('attached_forms_id')->unsigned();
           $table->integer('tasks_id')->unsigned();
           $table->timestamps();

           $table->foreign('attached_forms_id')->references('id')->on('attached_forms')->onUpdate('cascade')->onDelete('cascade');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attached_form_tasks');
    }
}
