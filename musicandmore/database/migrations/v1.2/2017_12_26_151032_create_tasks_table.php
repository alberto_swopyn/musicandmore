<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tasks_id')->nullable();
            $table->integer('companies_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->string('description');
            $table->dateTime('initial_date');
            $table->dateTime('final_date');
            $table->string('comment');
            $table->timestamps();

            $table->foreign('companies_id')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
