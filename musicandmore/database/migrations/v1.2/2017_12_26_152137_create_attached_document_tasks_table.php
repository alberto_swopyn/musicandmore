<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachedDocumentTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attached_document_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attached_document_tasks_id')->nullable();
            $table->integer('attached_documents_id')->unsigned();
            $table->integer('tasks_id')->unsigned();
            $table->timestamps();

            $table->foreign('attached_documents_id')->references('id')->on('attached_documents')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attached_document_tasks');
    }
}
