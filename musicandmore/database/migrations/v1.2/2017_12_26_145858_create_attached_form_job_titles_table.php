<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachedFormJobTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attached_form_job_titles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attached_form_job_titles_id')->nullable();
            $table->integer('job_titles_id')->unsigned();
            $table->integer('attached_forms_id')->unsigned();
            $table->timestamps();

            $table->foreign('job_titles_id')->references('id_inc')->on('tree_job_profiles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('attached_forms_id')->references('id')->on('attached_forms')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attached_form_job_titles');
    }
}
