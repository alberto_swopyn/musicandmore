<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTrainingPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_training_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('training_plan_id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->timestamps();

            $table->foreign('training_plan_id')->references('id')->on('training_plans')->onDelete('cascade');
            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_training_plan');
    }
}
