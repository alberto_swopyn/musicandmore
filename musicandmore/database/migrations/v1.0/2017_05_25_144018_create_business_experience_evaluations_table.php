<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessExperienceEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_experience_evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_experience_evaluations_id');
            $table->string('evaluation_committees_id');
            $table->string('sales_service');
            $table->string('staff_management');
            $table->string('inventory_management');
            $table->string('cash_management');
            $table->string('sales_experience');
            $table->string('computer_skills');
            $table->string('fluid_communication');
            $table->string('good_leader');
            $table->string('ordained_person');
            $table->string('tolerance');
            $table->string('positive_enthusiastic_person');
            $table->string('discipline_person');
            $table->string('potential_candidate');
            $table->string('neat_person');
            $table->string('committed_person');
            $table->string('shop_management');
            $table->string('clear_idea');
            $table->string('track_record');
            $table->string('stable_person');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_experience_evaluations');
    }
}
