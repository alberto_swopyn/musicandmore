<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobTitleProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_title_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_title_profiles_id');
            $table->string('job_title_name');
            $table->string('job_center');
            $table->string('job_title_objetive');
            $table->integer('min_salary');
            $table->integer('max_salary');
            $table->string('function_type');
            $table->integer('min_age');
            $table->integer('max_age');
            $table->string('gender');
            $table->string('civil_status');
            $table->string('scholarship');
            $table->string('college_career');
            $table->string('interview_type');
            $table->string('contract');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_title_profiles');
    }
}
