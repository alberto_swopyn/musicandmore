<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('companies', function (Blueprint $table) {
            $table->integer('company_contract_id')->unsigned()->change();
            $table->foreign('company_contract_id')->references('id')->on('company_contracts');
            $table->integer('contact_id')->unsigned()->change();
            $table->foreign('contact_id')->references('id')->on('contacts');

         });
         

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
