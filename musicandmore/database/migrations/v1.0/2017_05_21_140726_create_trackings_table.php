<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            
            $table->integer('job_application_id')->nullable();
            $table->boolean('video')->nullable();
            $table->integer('lms')->nullable();
            $table->integer('job_application')->nullable();
            $table->integer('document')->nullable();
            $table->integer('interview')->nullable();
            $table->integer('psychometric')->nullable();
            $table->integer('service_survey')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trackings');
    }
}
