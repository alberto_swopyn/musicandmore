<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationCommitteesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_committees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('evaluation_committees_id');
            $table->string('candidate_first_name');
            $table->string('candidate_last_name');
            $table->string('proceeds_evaluation');
            $table->string('territorial_manager');
            $table->string('human_resources');
            $table->string('capture_advisor');
            $table->string('commission_agent');
            $table->string('duty_manager_1');
            $table->string('duty_manager_2');
            $table->string('floor_assistant_1');
            $table->string('floor_assistant_2');
            $table->string('floor_assistant_3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_committees');
    }
}
