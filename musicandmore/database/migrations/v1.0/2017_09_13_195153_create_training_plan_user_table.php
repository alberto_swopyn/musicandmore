<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingPlanUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('training_plan_user', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('training_plan_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->timestamps();

                $table->foreign('training_plan_id')->references('id')->on('training_plans')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_plan_user');
    }
}
