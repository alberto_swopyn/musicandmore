<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingPlanTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_training_plan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('training_plan_id')->unsigned();
                $table->integer('training_id')->unsigned();
                $table->timestamps();

                $table->foreign('training_plan_id')->references('id')->on('training_plans')->onDelete('cascade');
                $table->foreign('training_id')->references('id')->on('trainings')->onDelete('cascade');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('training_plan_training');
    }
}
