<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_company');
            $table->string('name');
            $table->string('rfc',13);
            $table->string('address');
            $table->binary('logo');
            $table->binary('image');
            $table->string('phone',12);
            $table->integer('company_contract_id')->unsigned();
            $table->foreign('company_contract_id')->references('id')->on('company_contracts');
            $table->integer('contact_id')->unsigned();
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->string('bussines_name');
            $table->string('tax_regime',8);
            $table->string('bank_account');


            $table->integer('pay_type_id')->unsigned();
            $table->foreign('pay_type_id')->references('id')->on('pay_types');

            $table->integer('company_type');
            $table->integer('specialty');
            $table->integer('no_employees');
            $table->integer('no_branch_office');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
