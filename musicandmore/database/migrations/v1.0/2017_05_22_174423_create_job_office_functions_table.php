<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOfficeFunctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_office_functions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_office_functions_id');
            $table->integer('job_title_profiles_id')->unsigned();
            $table->string('function');
            $table->timestamps();
            $table->foreign('job_title_profiles_id')->references('id')->on('job_title_profiles')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_office_functions');
    }
}
