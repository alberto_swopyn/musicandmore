<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyEvaluationCommitteesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_evaluation_committees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('family_evaluation_committees_id');
            $table->string('evaluation_committees_id');
            $table->string('family_integration');
            $table->string('puntuality');
            $table->string('personal_cleanliness');
            $table->string('family_communication');
            $table->string('supported_values');
            $table->string('expenses');
            $table->string('operative_vs_activity');
            $table->string('store_responsability');
            $table->string('candidate_integration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_evaluation_committees');
    }
}
