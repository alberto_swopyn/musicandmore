<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersPsychometricTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers_psychometric_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('personal_id');
            $table->integer('answer_1_1')->nullable();
            $table->integer('answer_1_2')->nullable();
            $table->integer('answer_2_1')->nullable();
            $table->integer('answer_2_2')->nullable();
            $table->integer('answer_3_1')->nullable();
            $table->integer('answer_3_2')->nullable();
            $table->integer('answer_4_1')->nullable();
            $table->integer('answer_4_2')->nullable();
            $table->integer('answer_5_1')->nullable();
            $table->integer('answer_5_2')->nullable();
            $table->integer('answer_6_1')->nullable();
            $table->integer('answer_6_2')->nullable();
            $table->integer('answer_7_1')->nullable();
            $table->integer('answer_7_2')->nullable();
            $table->integer('answer_8_1')->nullable();
            $table->integer('answer_8_2')->nullable();
            $table->integer('answer_9_1')->nullable();
            $table->integer('answer_9_2')->nullable();
            $table->integer('answer_10_1')->nullable();
            $table->integer('answer_10_2')->nullable();
            $table->integer('answer_11_1')->nullable();
            $table->integer('answer_11_2')->nullable();
            $table->integer('answer_12_1')->nullable();
            $table->integer('answer_12_2')->nullable();
            $table->integer('answer_13_1')->nullable();
            $table->integer('answer_13_2')->nullable();
            $table->integer('answer_14_1')->nullable();
            $table->integer('answer_14_2')->nullable();
            $table->integer('answer_15_1')->nullable();
            $table->integer('answer_15_2')->nullable();
            $table->integer('answer_16_1')->nullable();
            $table->integer('answer_16_2')->nullable();
            $table->integer('answer_17_1')->nullable();
            $table->integer('answer_17_2')->nullable();
            $table->integer('answer_18_1')->nullable();
            $table->integer('answer_18_2')->nullable();
            $table->integer('answer_19_1')->nullable();
            $table->integer('answer_19_2')->nullable();
            $table->integer('answer_20_1')->nullable();
            $table->integer('answer_20_2')->nullable();
            $table->integer('answer_21_1')->nullable();
            $table->integer('answer_21_2')->nullable();
            $table->integer('answer_22_1')->nullable();
            $table->integer('answer_22_2')->nullable();
            $table->integer('answer_23_1')->nullable();
            $table->integer('answer_23_2')->nullable();
            $table->integer('answer_24_1')->nullable();
            $table->integer('answer_24_2')->nullable();
            $table->integer('answer_25_1')->nullable();
            $table->integer('answer_25_2')->nullable();
            $table->integer('answer_26_1')->nullable();
            $table->integer('answer_26_2')->nullable();
            $table->integer('answer_27_1')->nullable();
            $table->integer('answer_27_2')->nullable();
            $table->integer('answer_28_1')->nullable();
            $table->integer('answer_28_2')->nullable();
            $table->integer('answer_29_1')->nullable();
            $table->integer('answer_29_2')->nullable();
            $table->integer('answer_30_1')->nullable();
            $table->integer('answer_30_2')->nullable();
            $table->integer('answer_31_1')->nullable();
            $table->integer('answer_31_2')->nullable();
            $table->integer('answer_32_1')->nullable();
            $table->integer('answer_32_2')->nullable();
            $table->integer('answer_33_1')->nullable();
            $table->integer('answer_33_2')->nullable();
            $table->integer('answer_34_1')->nullable();
            $table->integer('answer_34_2')->nullable();
            $table->integer('answer_35_1')->nullable();
            $table->integer('answer_35_2')->nullable();
            $table->integer('answer_36_1')->nullable();
            $table->integer('answer_36_2')->nullable();
            $table->integer('answer_37_1')->nullable();
            $table->integer('answer_37_2')->nullable();
            $table->integer('answer_38_1')->nullable();
            $table->integer('answer_38_2')->nullable();
            $table->integer('answer_39_1')->nullable();
            $table->integer('answer_39_2')->nullable();
            $table->integer('answer_40_1')->nullable();
            $table->integer('answer_40_2')->nullable();
            $table->integer('answer_41_1')->nullable();
            $table->integer('answer_41_2')->nullable();
            $table->integer('answer_42_1')->nullable();
            $table->integer('answer_42_2')->nullable();
            $table->integer('answer_43_1')->nullable();
            $table->integer('answer_43_2')->nullable();
            $table->integer('answer_44_1')->nullable();
            $table->integer('answer_44_2')->nullable();
            $table->integer('answer_45_1')->nullable();
            $table->integer('answer_45_2')->nullable();
            $table->integer('answer_46_1')->nullable();
            $table->integer('answer_46_2')->nullable();
            $table->integer('answer_47_1')->nullable();
            $table->integer('answer_47_2')->nullable();
            $table->integer('answer_48_1')->nullable();
            $table->integer('answer_48_2')->nullable();
            $table->integer('answer_49_1')->nullable();
            $table->integer('answer_49_2')->nullable();
            $table->integer('answer_50_1')->nullable();
            $table->integer('answer_50_2')->nullable();
            $table->integer('answer_51_1')->nullable();
            $table->integer('answer_51_2')->nullable();
            $table->integer('answer_52_1')->nullable();
            $table->integer('answer_52_2')->nullable();
            $table->integer('answer_53_1')->nullable();
            $table->integer('answer_53_2')->nullable();

            $table->integer('result')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers_psychometric_tests');
    }
}
