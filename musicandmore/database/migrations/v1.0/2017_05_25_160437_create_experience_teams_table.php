<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienceTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experience_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('experience_teams_id');
            $table->string('evaluation_committees_id');
            $table->string('sale_1');
            $table->string('sale_2');
            $table->string('sale_3');
            $table->string('sale_4');
            $table->string('sale_5');
            $table->string('inventory_1');
            $table->string('inventory_2');
            $table->string('inventory_3');
            $table->string('inventory_4');
            $table->string('inventory_5');
            $table->string('customer_service_1');
            $table->string('customer_service_2');
            $table->string('customer_service_3');
            $table->string('customer_service_4');
            $table->string('customer_service_5');
            $table->string('cash_management_1');
            $table->string('cash_management_2');
            $table->string('cash_management_3');
            $table->string('cash_management_4');
            $table->string('cash_management_5');
            $table->string('since_performance_1');
            $table->string('since_performance_2');
            $table->string('since_performance_3');
            $table->string('since_performance_4');
            $table->string('since_performance_5');
            $table->string('salary_1');
            $table->string('salary_2');
            $table->string('salary_3');
            $table->string('salary_4');
            $table->string('salary_5');
            $table->string('role_assignment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experience_teams');
    }
}
