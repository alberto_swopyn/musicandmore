<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobTitleProfileTrainingPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('job_title_profile_training_plan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('training_plan_id')->unsigned();
                $table->integer('job_title_profile_id')->unsigned();
                $table->timestamps();

                $table->foreign('training_plan_id')->references('id')->on('training_plans')->onDelete('cascade');
                $table->foreign('job_title_profile_id')->references('id')->on('job_title_profiles')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_plan_job_title_profile');
    }
}
