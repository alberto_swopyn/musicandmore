<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

//login_alternative
Route::name('useralternative')->get('/loginuser', 'Auth\LoginalternativeController@index'); //alternative_login
Route::post('useremail', 'Auth\LoginalternativeController@update'); //alternative_login
Route::post('validationuser', 'Auth\LoginalternativeController@search'); //alternative_login
Route::name('verifyemail')->get('/verifyemail/{token}', 'Auth\RegisterController@verify');

// 'middleware' => ['role:admin']

// //acceso para admin o hr, utiliza el operador OR
// 'middleware' => ['role:admin|hr']f

// //acceso para admin y hr utiliza el operador AND
// 'middleware' => ['role:admin', 'role:hr']

// //admin y hr deben tener permisos para leer y buscar
// 'middleware' => ['ability:admin|owner,read|search,true']

/*ASSIGN ROLES*/
Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function () {
        Route::name('admin')->get('admin/users');
        Route::get('/', 'AdminController@welcome');
        Route::get('/manage', ['middleware' => ['permission:manage-admins'], 'uses' => 'AdminController@manageAdmins']);
    });

//START DOCUMENTS
    Route::name('documents_personal')->get('documents/createdocument',
        'Jobapplication\PersonaldataController@createDocument');
    Route::name('document_add')->post('documents/adddocument',
        'Jobapplication\PersonaldataController@addDocument');
    Route::name('show_image')->get('documents/{document}',
        'Jobapplication\PersonaldataController@showDocument');
    Route::name('document_delete')->delete('documents/deletedocument/{document}',
        'Jobapplication\PersonaldataController@deleteDocument');
    Route::name('documents_verified')->post('documents/verified',
        'Jobapplication\PersonaldataController@verifiedDocument');
    Route::name('documents_show')->get('jobapplication/show/{document}',
        'Jobapplication\PersonaldataController@showDocumentUser');
    Route::name('documents_invalidated')->post('documents/invalidated',
        'Jobapplication\PersonaldataController@invalidatedDocument');
    Route::name('documents_commentary')->post('documents/commentary',
        'Jobapplication\PersonaldataController@updateCommentaryDocument');
//END DOCUMENTS

//START STUDY

//END TRAININIG STUDY


//START TRAINING COURSES

    Route::name('training_index')->get('training/index',
        'Training\TrainingController@index');

    Route::name('training_edit')->get('training/{training}/edit',
        'Training\TrainingController@edit');

    Route::name('training_update')->put('training/{training}',
        'Training\TrainingController@update');

    Route::name('training_delete')->delete('training/{training}',
        'Training\TrainingController@delete');

    Route::name('training_course')->get('training/{training}/course',
        'Training\TrainingController@play');

    Route::name('training_progress')->post('training/progress',
        'Training\TrainingController@progress');

    Route::name('training_kardex')->get('training/kardex/{training}',
        'Training\TrainingController@kardexUser');

    Route::name('training_index_assign')->get('training/indexassign',
        'Training\TrainingController@indexassign');

    Route::name('training_create_assign')->get('training/createassign',
        'Training\TrainingController@createassign');

    Route::name('training_create_assign_plan')->get('training/createassign/plan',
        'Training\TrainingController@createassignplan');

    Route::name('training_store_assign')->post('training/storeassign',
        'Training\TrainingController@storeassign');

    Route::name('training_store_assign_plan')->post('training/storeassign/plan',
        'Training\TrainingController@storeassignplan');

    Route::name('training_edit_assign')->get('training/{trainingplan}/editassign',
        'Training\TrainingController@editassign');

    Route::name('training_update_assign')->put('trainingplan/{trainingplan}/updateassign',
        'Training\TrainingController@updateassign');

    Route::name('training_delete_assign')->delete('trainingplan/{trainingplan}',
        'Training\TrainingController@deleteassign');

    Route::name('training_create')->get('training/create', 'Training\TrainingController@create');

    Route::name('training_add')->post('training/add', 'Training\TrainingController@add');

    Route::name('new_module')->get('module/new', 'Training\TrainingController@createModule');

    Route::name('create_module')->post('module/create', 'Training\TrainingController@addModule');

    Route::name('delete_module')->delete('module/{module}/delete', 'Training\TrainingController@deleteModule');

    Route::name('module_assign_update')->put('module/{module}/update', 'Training\TrainingController@moduleUpdate');

    Route::name('module_edit')->get('module/edit/{module}', 'Training\TrainingController@editModule');

//END TRAINING COURSES

    Route::group(['middleware' => ['role:docente|alumno|chido|admin']], function () {
//START TRACKING
        Route::name('tracking_document')->post('tracking/document',
            'Tracking\TrackingController@document');

        Route::name('tracking_interview')->post('tracking/interview',
            'Tracking\TrackingController@interview');

        Route::name('tracking_index')->get('tracking/index',
            'Tracking\TrackingController@index');

        Route::name('tracking_contract')->get('tracking/contract/{user}',
            'Tracking\TrackingController@contract');

        Route::name('tracking_interview_result')->post('tracking/interview_result',
            'Tracking\TrackingController@interview_result');

        Route::name('match_search')->post('tracking/search',
            'Tracking\TrackingController@search');

        Route::name('match_porcentaje')->post('tracking/matchporcentaje',
            'Tracking\TrackingController@matchporcentaje');

        Route::name('match_detalles')->post('tracking/matchdetalles',
            'Tracking\TrackingController@matchdetalles');
//END TRACKING

//START CANDIDATE WELCOME
        Route::name('candidate_welcome')->get('tracking/welcome', 'Tracking\TrackingController@welcome');
        Route::name('candidate_start')->post('tracking/start', 'Tracking\TrackingController@start');
        Route::name('candidate_video')->post('tracking/video', 'Tracking\TrackingController@video');
        Route::name('candidate_level')->post('tracking/level', 'Tracking\TrackingController@level');
//END CANDIDATE WELCOME

//START COURSE EVALUATION
        Route::name('resultsAdmin')->get('courses/resultsAdmin/{user}/{curso}',
            'Courseevaluation\CourseevaluationController@result');
//END COURSE EVALUATION

//START JOBTITLE PROFILE
        Route::name('jobtitleprofile')->get('jobtitleprofile/create/{jobprofile_id}/{jobprofile_name}', 'Jobtitleprofile\JobtitleprofileController@create');

        Route::name('jobtitleprofile_add')->post('job_title_profile/add',
            'Jobtitleprofile\JobtitleprofileController@addprofile');

        Route::name('jobtitleprofile_interview_add')->post('job_title_profile_interview/add',
            'Jobtitleprofile\JobtitleprofileController@addInterview');

        Route::name('jobtitleprofile_contract_add')->post('job_title_profile_contract/add',
            'Jobtitleprofile\JobtitleprofileController@addContract');

        Route::name('jobtitleprofile_demographic_add')->post('job_title_profile_demographic/add',
            'Jobtitleprofile\JobtitleprofileController@addDemographic');

        Route::name('joblanguage_add')->post('job_language/add',
            'Jobtitleprofile\JobtitlelanguageController@add');

        Route::name('joblanguage_delete')->post('job_language/delete',
            'Jobtitleprofile\JobtitlelanguageController@delete');

        Route::name('jobtitle_office_add')->post('jobtitle_office/add',
            'Jobtitleprofile\JobofficefunctionController@add');

        Route::name('jobtitle_office_delete')->post('jobtitle_office/delete',
            'Jobtitleprofile\JobofficefunctionController@delete');

        Route::name('jobtitle_machine_add')->post('jobtitle_machine/add',
            'Jobtitleprofile\JobtitlemachineController@add');

        Route::name('jobtitle_machine_delete')->post('jobtitle_machine/delete',
            'Jobtitleprofile\JobtitlemachineController@delete');

        Route::name('jobtitle_function_add')->post('jobtitle_function/add',
            'Jobtitleprofile\JobtitlefunctionController@add');

        Route::name('jobtitle_function_delete')->post('jobtitle_function/delete',
            'Jobtitleprofile\JobtitlefunctionController@delete');

        Route::name('jobtitle_attitude_add')->post('jobtitle_attitude/add',
            'Jobtitleprofile\JobtitleattitudeController@add');

        Route::name('jobtitle_attitude_delete')->post('jobtitle_attitude/delete',
            'Jobtitleprofile\JobtitleattitudeController@delete');

        Route::name('jobtitle_software_add')->post('jobtitle_software/add',
            'Jobtitleprofile\JobtitlesoftwareController@add');

        Route::name('jobtitle_software_delete')->post('jobtitle_software/delete',
            'Jobtitleprofile\JobtitlesoftwareController@delete');

        Route::name('jobtitle_responsability_add')->post('jobtitle_responsability/add',
            'Jobtitleprofile\JobtitleresponsabilityController@add');

        Route::name('jobtitle_responsability_delete')->post('jobtitle_responsability/delete',
            'Jobtitleprofile\JobtitleresponsabilityController@delete');

        Route::name('jobtitleprofile_scholar')->post('job_title_profile_scholar/add',
            'Jobtitleprofile\JobtitleprofileController@addscholar');

        Route::name('jobtitleprofile_scholar')->post('job_title_profile_scholar/delete',
            'Jobtitleprofile\JobtitleprofileController@deletescholar');

        Route::name('jobtitle_experience_add')->post('job_title_experience/add',
            'Jobtitleprofile\JobtitleexperienceController@add');

        Route::name('jobtitle_experience_delete')->post('job_title_experience/delete',
            'Jobtitleprofile\JobtitleexperienceController@delete');

        Route::name('jobtitle_documents_add')->post('job_title_documents/add',
            'Jobtitleprofile\JobtitledocumentController@add');

        Route::name('show_image')->get('documents/{document}', 'Jobapplication\PersonaldataController@showDocument');

        Route::post('search_education_profile', 'Jobtitleprofile\JobtitleprofileController@select_education_profile');
//END JOB TITLE

//START JOB CENTER
        //Route::name('jobcenter')->get('jobcenter/create',function(){return view('adminlte::jobcenter.create');});
        Route::name('jobcenter')->get('profile_job_centers/create/{jobcenter_id}/{jobcenter_name}', 'JobCenter\ProfilejobcenterController@create');

        Route::name('profile_job_centers_add')->post('profile_job_centers/add',
            'JobCenter\ProfilejobcenterController@add');

        Route::name('address_job_centers_add')->post('address_job_centers/add',
            'JobCenter\AddressjobcenterController@add');

        Route::name('workcapacity_job_centers_add')->post('workcapacity_job_centers/add',
            'JobCenter\WorkcapacityController@add');

        Route::name('workcapacity_job_centers_delete')->post('workcapacity_job_centers/delete',
            'JobCenter\WorkcapacityController@delete');

        Route::name('general_information_job_centers_add')->post('general_information_job_centers/add',
            'JobCenter\GeneralinformationController@add');

        Route::name('goal_job_centers_add')->post('goal_job_centers/add',
            'JobCenter\GoalJobCenterController@add');

        Route::name('sumaPersonal')->get('workcapacity_job_centers/sum/{jobcenter_id}',
            'JobCenter\WorkcapacityController@totPersonal');
//END JOB CENTER

//START JOBCENTER TREE
        Route::name('tree_jobcenter')->get('tree_jobcenter/create',
            'Tree\TreeController@create');

        Route::name('tree_jobcenter_read')->get('tree_jobcenter/read',
            'Tree\TreeController@read');

        Route::name('tree_jobcenter_write')->post('tree_jobcenter/write',
            'Tree\TreeController@write');
//END JOBCENTER TREE

//START JOBCENTER SUMMARY
        Route::name('tree_jobcenter_summary')->get('tree_jobcenter/createsummary',
            'JobCenter\ProfilejobcenterController@create2');
//START JOBCENTER SUMMARY

//START JOBPROFILE TREE
        Route::name('tree_jobprofile')->get('tree_jobprofile/create',
            'Treejobprofile\TreejobprofileController@create');

        Route::name('tree_jobprofile_read')->get('tree_jobprofile/read',
            'Treejobprofile\TreejobprofileController@read');

        Route::name('tree_jobprofile_write')->post('tree_jobprofile/write',
            'Treejobprofile\TreejobprofileController@write');
//END JOBPROFILE TREE

//START OPERATIVE INTERVIEW
        Route::name('interview_create')->get('interview/create/{user}', 'Interview\InterviewController@create');
        Route::name('interviewAdd')->post('interview/save', 'Interview\InterviewController@save');
//END OPERATIVE INTERVIEW

//START SCHEDULED INTERVIEW
        Route::name('scheduled_interview_create')->get('scheduledinterview/create',
            'Scheduledinterview\ScheduledinterviewController@create');

        Route::name('searchInterview')->post('scheduled_interview/search',
            'Scheduledinterview\ScheduledinterviewController@search');

        Route::name('scheduled_interview_userview')->get('scheduledinterview/userview',
            'Scheduledinterview\ScheduledinterviewController@userview');

        Route::name('scheduled_interview_add')->post('scheduled_interview/add',
            'Scheduledinterview\ScheduledinterviewController@add');
//END SCHEDULED INTERVIEW

        //SWOPYN SECURITY
        //Route::name('Security_index')->get('Security\HomeController@index');

//START CONTRACTS
        //view
        Route::name('contracts_index')->get('/contract/index/{usuario}',
            'Contracttype\ContracttypeController@index');

        //pdf
        Route::get('pdf', 'Contracttype\ContracttypeController@invoice');

        Route::name('pdf_contract')->get('/contract/{user}',
            'Contracttype\ContracttypeController@invoice');

        //add register to table
        Route::name('contractdownload')->get('/contractdownload/{user}',
            'Contracttype\ContractController@addDownload');

        //add register to table
        Route::name('contractupload')->post('contractupload',
            'Contracttype\ContractController@addUpload');

        Route::name('contractupdate')->post('contract/update',
            'Contracttype\ContracttypeController@update');
//END CONTRACTS

//START CREATE EVALUATION
        Route::name('kardex_question_evaluation')->get('evaluation/courses/{course}',
            'Courseevaluation\EvaluationcreateController@kardex');

        Route::name('evaluation_course_create')->get('evaluationcourse/create/{course}/{question}',
            'Courseevaluation\EvaluationcreateController@create');

        Route::name('evaluation_question_edit')->get('evaluationcourse/edit/{question}',
            'Courseevaluation\EvaluationcreateController@editView');

        Route::name('evaluation_question_delete')->get('evaluationcourse/delete/{question}',
            'Courseevaluation\EvaluationcreateController@delete');

        Route::name('evaluation_question_save')->post('evaluationcourse/save',
            'Courseevaluation\EvaluationcreateController@save');

        Route::name('evaluation_question_edit_it')->post('evaluationcourse/editquestion',
            'Courseevaluation\EvaluationcreateController@edit');

        Route::name('companyEvaluation')->get('courses/companyEvaluation',
            'Courseevaluation\CourseevaluationController@CompanyEval');
//END CREATE EVALUATION

//EMPLOYEES
        Route::name('kardex_employees')->get('employees/kardex',
            'Employees\EmployeeController@kardex');
        Route::name('kardexEmployeeCourses')->get('employees/evaluations/kardex/{jobcenter}/{jobTitle}',
            'Employees\EmployeeController@kardexEvaluations');
        Route::name('SearchKardex')->get('employees/evaluations/kardex/{search}',
            'Employees\EmployeeController@kardexSucursales');
        Route::name('index_employees')->get('employees/index/{id}',
            'Employees\EmployeeController@index');
        Route::name('update_employees')->post('employees/update',
            'Employees\EmployeeController@update');
        Route::name('searchEmployee')->post('employees/search',
            'Employees\EmployeeController@searchEmployee');
        Route::name('create_employee')->post('create/employee/new', 'Employees\EmployeeController@createEmployee');
//END EMPLOYEES

//START TRAINING COURSES
        Route::name('training_study')->get('training/study',
            'Training\TrainingController@study');

        Route::name('training_personal_study')->get('training/personal/study/{id}',
            'Training\TrainingController@personalStudy');

        Route::name('module_personal_study')->get('module/personal/study/{id}',
            'Training\TrainingController@moduleStudy');

        Route::name('training_add')->post('training/add',
            'Training\TrainingController@add');

//START COURSE EVALUATION
        Route::name('intro')->get('courses/intro/{course}',
            'Courseevaluation\CourseevaluationController@intro');

        Route::name('course_view')->get('courses/index/{course}',
            'Courseevaluation\CourseevaluationController@index');

        Route::name('question_save')->post('courses/save',
            'Courseevaluation\CourseevaluationController@save');

        Route::name('resultsUser')->get('courses/resultsUser/{course}',
            'Courseevaluation\CourseevaluationController@resultsUser');
//END COURSE EVALUATION

//START DESCRIPTION COMPANY
        Route::name('user_trainings')->get('training/users/{user}',
            'Training\TrainingController@kardexUser');
        Route::get('circlek', 'Company\CompanyController@login');

        Route::name('company_show')->get('company/index', 'Company\CompanyController@index');

        Route::name('company_create')->get('company/create', 'Company\CompanyController@create');

        Route::name('company_add')->post('company/add', 'Company\CompanyController@add');

        Route::name('company_edit')->get('company/{company}/edit', 'Company\CompanyController@edit');

        Route::name('company_update')->put('company/{company}', 'Company\CompanyController@update');

        Route::name('company_delete')->delete('company/{company}', 'Company\CompanyController@delete');

        Route::name('show_image')->get('company/show_image/{company}', 'Company\CompanyController@showimg');

        Route::name('show_message')->post('show_message', 'Message\MessageController@showMessage');

        Route::name('paytype_create')->get('paytype/create', 'Paytype\PaytypeController@create');

        Route::name('paytype_add')->post('paytype/add', 'Paytype\PaytypeController@add');

        Route::name('course')->get('company/course', 'Company\CompanyController@course');

        Route::name('course_update')->post('company/course_update/{company}', 'Company\CompanyController@courseupdate');

        Route::name('course_finish')->post('message/course_finish', 'Message\MessageController@addInterview');
//END DESCRIPTION COMPANY

//START JOB APPLICATION
        Route::name('jobapplication')->get('jobapplication/create',
            'Jobapplication\JobapplicationController@create');
        Route::name('jobapplicationstudent')->get('jobapplicationstudent/create',
            'Jobapplication\JobapplicationController@createStudent');
        Route::name('actual_studies_add')->post('actual_studies/add',
            'Jobapplication\ActualstudiesController@add');
        Route::name('economic_datas_add')->post('economic_datas/add',
            'Jobapplication\EconomicdatasController@add');
        Route::name('family_data_add')->post('family_data/add',
            'Jobapplication\FamilydataController@add');
        Route::name('family_data_delete')->post('family_data/delete',
            'Jobapplication\FamilydataController@delete');
        Route::name('general_knowledge_add')->post('general_knowledge/add',
            'Jobapplication\GeneralknowledgeController@add');
        Route::name('personal_addresses_add')->post('personaladdresses/add',
            'Jobapplication\PersonaladdressesController@add');
        Route::name('general_personal_data_add')->post('personaldata/add',
            'Jobapplication\PersonaldataController@add');
            Route::name('general_personal_data_add')->post('personaldata/addStudent',
            'Jobapplication\PersonaldataController@addStudent');
        Route::name('add_picture')->post('personaldata/addpicture',
            'Jobapplication\PersonaldataController@addPicture');
        Route::name('showPicture')->get('personaldata/showpicture/{id}',
            'Jobapplication\PersonaldataController@showPicture');
        Route::name('personal_references_add')->post('personal_references/add',
            'Jobapplication\PersonalreferencesController@add');
        Route::name('personal_references_delete')->post('personal_references/delete',
            'Jobapplication\PersonalreferencesController@delete');
        Route::name('scholarship_datas_add')->post('scholarship_datas/add',
            'Jobapplication\ScholarshipdatasController@add');
        Route::name('scholarship_datas_delete')->post('scholarship_datas/delete',
            'Jobapplication\ScholarshipdatasController@delete');
        Route::name('work_general_datas_add')->post('work_general_datas/add',
            'Jobapplication\WorkgeneraldatasController@add');
        Route::name('work_general_datas_addStudent')->post('work_general_datas/addStudent',
            'Jobapplication\WorkgeneraldatasController@addStudent');
        Route::name('work_history_add')->post('work_history/add',
            'Jobapplication\WorkhistoryController@add');
        Route::name('work_history_delete')->post('work_history/delete',
            'Jobapplication\WorkhistoryController@delete');
        Route::name('job_application_pdf')->get('job_application_pdf/{id_user}', 'Jobapplication\JobapplicationController@pdf');
        Route::post('language_application/add',
            'Jobapplication\LanguageapplicationController@add');
        Route::post('office_application/add',
            'Jobapplication\OfficeapplicationController@add');
        Route::post('machines_application/add',
            'Jobapplication\MachinesapplicationController@add');
        Route::post('software_application/add',
            'Jobapplication\SoftwareapplicationController@add');
        Route::post('others_application/add',
            'Jobapplication\OthersapplicationController@add');
        Route::post('search_education', 'Jobapplication\JobapplicationController@select_education'); //update
        Route::name('languageGeneral_delete')->post('languageGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteLang');
        Route::name('officeGeneral_delete')->post('officeGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteOffice');
        Route::name('machineGeneral_delete')->post('macGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteMac');
        Route::name('softGeneral_delete')->post('softGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteSoft');
        Route::name('otherGeneral_delete')->post('otherGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteOther');
        Route::name('student_detail')->get('jobapplicationstudent/create/{users}', 'Jobapplication\JobapplicationController@editStudent');
//END JOBAPPLICATION

//START BRANCHES COMPANY
        //Route::name('branch_show')->get('branch/index','Branch\BranchController@show');

        Route::name('branch_index')->get('branch/index', 'Branch\BranchController@index');

        Route::name('branch_create')->get('branch/create', 'Branch\BranchController@create');

        Route::name('branch_add')->post('branch/add', 'Branch\BranchController@add');

        Route::name('branch_edit')->get('branch/{branch}/edit', 'Branch\BranchController@edit');

        Route::name('branch_update')->put('branch/{branch}', 'Branch\BranchController@update');

        Route::name('branch_delete')->delete('branch/{branch}', 'Branch\BranchController@delete');
//END BRANCHES COMPANY

//START PSYCHOMETRIC TEST
        Route::name('psychometric_test_intro')->get('psychometric/intro', 'Psychometric\PsychometricController@intro');
        Route::name('psychometric_test')->get('psychometric/test', 'Psychometric\PsychometricController@test');
        Route::name('psychometric_test_answer')->get('psychometric/answer/{psychometric}', 'Psychometric\PsychometricController@answer');
        Route::name('show_image_psychometric')->get('psychometric/show_image/{psychometric}', 'Psychometric\PsychometricController@showimg');
        Route::name('psychometric_thanks')->get('psychometric/thanks', 'Psychometric\PsychometricController@thanks');
        Route::name('answer_add')->post('psychometric/add', 'Psychometric\PsychometricController@add');
        Route::name('psychometric_score')->post('psychometric/score', 'Psychometric\PsychometricController@score');
//END PSYCHOMETRIC TEST

    });

    Route::group(['middleware' => ['role: chido|docente|alumno|admin']], function () {

//START COURSE EVALUATION
        Route::name('intro')->get('courses/intro/{course}',
            'Courseevaluation\CourseevaluationController@intro');

        Route::name('course_view')->get('courses/index/{course}',
            'Courseevaluation\CourseevaluationController@index');

        Route::name('question_save')->post('courses/save',
            'Courseevaluation\CourseevaluationController@save');

        Route::name('resultsUser')->get('courses/resultsUser/{course}',
            'Courseevaluation\CourseevaluationController@resultsUser');

//END COURSE EVALUATION

//START CANDIDATE CAPABLE
        Route::name('candidate_video')->post('tracking/video', 'Tracking\TrackingController@video');
//END CANDIDATE CAPABLE

//START JOBAPLICATION CAPABLE
        Route::name('jobapplication')->get('jobapplication/create',
            'Jobapplication\JobapplicationController@create');
        Route::name('family_data_add')->post('family_data/add',
            'Jobapplication\FamilydataController@add');
        Route::name('family_data_delete')->post('family_data/delete',
            'Jobapplication\FamilydataController@delete');
        Route::name('actual_studies_add')->post('actual_studies/add',
            'Jobapplication\ActualstudiesController@add');
        Route::name('economic_datas_add')->post('economic_datas/add',
            'Jobapplication\EconomicdatasController@add');
        Route::name('general_knowledge_add')->post('general_knowledge/add',
            'Jobapplication\GeneralknowledgeController@add');
        Route::name('personal_addresses_add')->post('personaladdresses/add',
            'Jobapplication\PersonaladdressesController@add');
        Route::name('general_personal_data_add')->post('personaldata/add',
            'Jobapplication\PersonaldataController@add');
        Route::name('add_picture')->post('personaldata/addpicture',
            'Jobapplication\PersonaldataController@addPicture');
        Route::name('showPicture')->get('personaldata/showpicture/{id}',
            'Jobapplication\PersonaldataController@showPicture');
        Route::name('personal_references_add')->post('personal_references/add',
            'Jobapplication\PersonalreferencesController@add');
        Route::name('personal_references_delete')->post('personal_references/delete',
            'Jobapplication\PersonalreferencesController@delete');
        Route::name('scholarship_datas_add')->post('scholarship_datas/add',
            'Jobapplication\ScholarshipdatasController@add');
        Route::name('scholarship_datas_delete')->post('scholarship_datas/delete',
            'Jobapplication\ScholarshipdatasController@delete');
        Route::name('work_general_datas_add')->post('work_general_datas/add',
            'Jobapplication\WorkgeneraldatasController@add');
        Route::name('work_history_add')->post('work_history/add',
            'Jobapplication\WorkhistoryController@add');
        Route::name('work_history_delete')->post('work_history/delete',
            'Jobapplication\WorkhistoryController@delete');
        Route::post('language_application/add',
            'Jobapplication\LanguageapplicationController@add');
        Route::post('office_application/add',
            'Jobapplication\OfficeapplicationController@add');
        Route::post('machines_application/add',
            'Jobapplication\MachinesapplicationController@add');
        Route::post('software_application/add',
            'Jobapplication\SoftwareapplicationController@add');
        Route::post('others_application/add',
            'Jobapplication\OthersapplicationController@add');
        Route::post('search_education', 'Jobapplication\JobapplicationController@select_education'); //update
        Route::name('languageGeneral_delete')->post('languageGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteLang');
        Route::name('officeGeneral_delete')->post('officeGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteOffice');
        Route::name('machineGeneral_delete')->post('macGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteMac');
        Route::name('softGeneral_delete')->post('softGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteSoft');
        Route::name('otherGeneral_delete')->post('otherGeneral/delete',
            'Jobapplication\GeneralknowledgeController@deleteOther');
        Route::name('job_application_pdf')->get('job_application_pdf/{id_user}', 'Jobapplication\JobapplicationController@pdf');
//END JOBAPLICATION CAPABLE

//START PSYCHOMETRIC TEST
        Route::name('psychometric_test_intro')->get('psychometric/intro', 'Psychometric\PsychometricController@intro');
        Route::name('psychometric_test')->get('psychometric/test', 'Psychometric\PsychometricController@test');
        Route::name('psychometric_test_answer')->get('psychometric/answer/{psychometric}', 'Psychometric\PsychometricController@answer');
        Route::name('show_image_psychometric')->get('psychometric/show_image/{psychometric}', 'Psychometric\PsychometricController@showimg');
        Route::name('psychometric_thanks')->get('psychometric/thanks', 'Psychometric\PsychometricController@thanks');
        Route::name('answer_add')->post('psychometric/add', 'Psychometric\PsychometricController@add');
        Route::name('psychometric_score')->post('psychometric/score', 'Psychometric\PsychometricController@score');
//END PSYCHOMETRIC TEST

//START TRAINING CAPABLE
        Route::name('training_study')->get('training/study',
            'Training\TrainingController@study');
        Route::name('training_course')->get('training/{training}/course',
            'Training\TrainingController@play');
//END TRAINING CAPABLE

//START COURSE EVALUATION
        Route::name('intro')->get('courses/intro/{course}',
            'Courseevaluation\CourseevaluationController@intro');

        Route::name('course_view')->get('courses/index/{course}',
            'Courseevaluation\CourseevaluationController@index');

        Route::name('question_save')->post('courses/save',
            'Courseevaluation\CourseevaluationController@save');

        Route::name('resultsUser')->get('courses/resultsUser/{course}',
            'Courseevaluation\CourseevaluationController@resultsUser');
//END COURSE EVALUATION

    });

/*END ASSIGN ROLES*/

//START ERROR PAGES
    //Route::get('pagenotfound', ['as' => 'notfound', 'uses' => 'HomeController@pagenotfound']);

    //Route::get('prohibit', ['as' => 'prohibit', 'uses' => 'HomeController@prohibit']);

    Route::get('internal', ['as' => 'internal', 'uses' => 'HomeController@internal']);
//END ERROR PAGES

//START TASKS
    Route::name('tasks')->get('tasks/my_tasks',
        'Tasks\TaskController@myTasks');

    Route::name('task_download')->get('tasks/my_tasks/{task}/download', 'Tasks\TaskController@downloadDoc');

    Route::name('assign_view')->get('tasks/my_tasks/{assign}/{tasks}/{users}/view', 'Tasks\TaskController@assignview');

    Route::name('evidence_add')->put('tasks/my_tasks/{assign}/{tasks}/{users}/add_evidence', 'Tasks\TaskController@addEvidence');

    Route::name('evidence_add_admin')->put('tasks/my_tasks/{assign}/{tasks}/{users}/add_evidenceAdmin', 'Tasks\TaskController@addEvidenceAdmin');

    Route::name('evidence_download')->get('tasks/{assign}/{tasks}/download', 'Tasks\TaskController@downloadEvidence');

    Route::name('evidence_download2')->get('tasks/{evi}/download', 'Tasks\TaskController@downloadEvidence');

    Route::name('evidence_delete')->delete('tasks/{assign}/{tasks}/{users}/{evi}/delete', 'Tasks\TaskController@deleteEvidence');

    Route::name('evidence_delete_admin')->delete('tasks/{assign}/{tasks}/{users}/{evi}/deleteAdmin', 'Tasks\TaskController@deleteEvidenceAdmin');

    Route::name('message_add')->put('my_tasks/view/{assign}/{tasks}/{users}/add_message', 'Tasks\TaskController@addMessage');

    Route::name('message_add_admin')->put('my_tasks/view/{assign}/{tasks}/{users}/add_messageAdmin', 'Tasks\TaskController@addMessageAdmin');

    Route::name('finish_task')->put('tasks/my_tasks/{assign}/{tasks}/{users}/finish', 'Tasks\TaskController@finishTask');

    Route::name('tasks_admin')->get('tasks/all_tasks',
        'Tasks\TaskController@adminTasks');

    Route::name('assign_view_admin')->get('tasks/all_tasks/{assign}/{tasks}/{users}/view_Admin', 'Tasks\TaskController@assignviewAdmin');

    Route::name('tasks_edit')->get('tasks/all_tasks/{tasks}/edit', 'Tasks\TaskController@editTasks');

    Route::name('tasks_update')->put('tasks/all_tasks/{tasks}', 'Tasks\TaskController@updateTasks');

    Route::name('tasks_delete')->delete('tasks/all_tasks/{tasks}', 'Tasks\TaskController@deleteTasks');

//Assignments options
    Route::name('assign_edit')->get('tasks/all_tasks/{assign}/editAssign', 'Tasks\TaskController@editAssign');

    Route::name('assign_update')->put('tasks/all_tasks/{assign}/updateAssign', 'Tasks\TaskController@updateAssign');

    Route::name('assign_delete')->delete('tasks/all_tasks/{assign}/deleteAssign', 'Tasks\TaskController@deleteAssign');
//prueba

//START Laura's TASKS
    Route::name('returntask')->get('tasks/returntask', 'Tasks\TaskController@returntask');

    Route::name('createtask')->get('tasks/createtask', 'Tasks\TaskController@createtask');

    Route::name('createassign')->get('tasks/createassign', 'Tasks\TaskController@createassign');

    Route::name('task_add')->post('task/add', 'Tasks\TaskController@add');

    Route::name('task_assig')->post('task/assigment', 'Tasks\TaskController@addassign');
//END Laura's TASKS
    Route::name('returntask')->get('tasks/returntask', 'Tasks\TaskController@returntask');
    Route::name('createtask')->get('tasks/createtask', 'Tasks\TaskController@createtask');
    Route::name('task_add')->post('task/add', 'Tasks\TaskController@add');
    Route::name('task_assig')->post('task/assigment', 'Tasks\TaskController@addassign');
//END Laura's TASKS
    //NOTIFICATIONS
    Route::get('/notifications/{id}', 'Tasks\TaskController@deleteNot');
//END NOTIFICATIONS
    //END TASKS

//STAR QUESTION TASKS
    Route::name('formulario_question_task')->get('evaluation/tasks/{task}',
        'Tasks\TaskController@formulario');
    Route::name('question_task_create')->get('evaluation/task/create/{task}/{question}',
        'Tasks\TaskController@create');
    Route::name('evaluation_task_edit_it')->post('evaluationtask/editquestion',
        'Tasks\TaskController@edit');
    Route::name('evaluation_task_save')->post('evaluationtask/save',
        'Tasks\TaskController@save');
    Route::name('task_question_delete')->get('evaluationtask/delete/{question}',
        'Tasks\TaskController@delete');
//END TASKS

//START TASK EVALUATION
    Route::name('introtask')->get('tasks/intro/{task}',
        'Tasks\TaskController@intro');

    Route::name('task_view')->get('tasks/index/{task}',
        'Tasks\TaskController@index');

    Route::name('question_task_save')->post('tasks/saveResult',
        'Tasks\TaskController@saveResult');

    Route::name('resultsUserTask')->get('tasks/resultsUserT/{task}',
        'Tasks\TaskController@resultsUser');
//END TASK EVALUATION

//START PAYS
Route::name('pays')->get('pays/my_pays','Pays\PayController@myPays');
Route::name('pays_admin')->get('pays/all_pays','Pays\PayController@adminPays');
Route::name('search_admin')->get('pays/all_pays/search','Pays\PayController@adminPays');
Route::name('search_admin2')->get('pays/all_pays/search2','Pays\PayController@adminPays');
Route::name('pays_edit')->get('pays/all_pays/{pays}/edit', 'Pays\PayController@editPays');
Route::name('pays_edit_r')->get('pays/all_pays/{pays}/edit/recurrent', 'Pays\PayController@editPays_r');
Route::name('pays_update')->put('pays/all_pays/{pays}', 'Pays\PayController@updatePays');
Route::name('pays_update_r')->put('pays/all_pays/recurrent/{pays}', 'Pays\PayController@updatePays_r');
Route::name('pays_delete')->delete('pays/all_pays/{pays}', 'Pays\PayController@deletePays');
Route::name('pays_delete_r')->delete('pays/all_pays/recurrent/{pays}', 'Pays\PayController@deletePays_r');
Route::name('createpays')->get('pays/createpay', 'Pays\PayController@createpay');
Route::name('createpays_r')->get('pays/create/recurrent','Pays\PayController@createpay_r');
Route::name('pay_add')->post('pay/add', 'Pays\PayController@add');
Route::name('pay_add_r')->post('pay/add/r', 'Pays\PayController@add_r');
Route::name('pays_teacher')->get('pays/payroll','Pays\PayController@paysteacher');
Route::name('nomineS')->get('pays/payroll/search','Pays\PayController@paysteacher');
Route::name('nomineN')->get('pays/payroll/searchN','Pays\PayController@paysteacher');
Route::name('nomine_create')->get('pays/create/{group}','Pays\PayController@nominecreate');
Route::name('nomine_add')->post('pays/add','Pays\PayController@payadd');
Route::name('nomine')->get('pays/payroll_teacher','Pays\PayController@payroll_teacher');
Route::name('paynomine')->get('pays/payroll_teacher/search','Pays\PayController@payroll_teacher');
Route::name('ticket_nomine')->get('pays/payroll_teacher/{id}/ticket','Pays\PayController@ticket_nomine');
Route::name('ticket_pays')->get('pays/{id}/ticket','Pays\PayController@payticket');
Route::name('ticket_pays_r')->get('pays/{id}/ticket_r','Pays\PayController@payticket_r');
Route::name('discount_admin')->get('discount/admin','Pays\PayController@adminDiscount');
Route::name('search_discount')->get('discount/admin/search','Pays\PayController@adminDiscount');
Route::name('create_discount')->get('discount/create','Pays\PayController@create_discount');
Route::name('add_discount')->post('discount/add','Pays\PayController@add_discount');
Route::name('edit_discount')->get('discount/{discount}/edit','Pays\PayController@edit_discount');
Route::name('update_discount')->put('discount/update/{discount}','Pays\PayController@update_discount');
Route::name('delete_discount')->delete('discount/delete/{discount}','Pays\PayController@delete_discount');
Route::name('box_admin')->get('pays/box','Pays\PayController@box_admin');
Route::name('search_box')->get('pays/box/search','Pays\PayController@box_admin');
Route::name('search_box2')->get('pays/box/search2','Pays\PayController@box_admin');
Route::name('box_unic')->get('pays/box/unic','Pays\PayController@box_unic');
Route::name('box_rec')->get('pays/box/rec','Pays\PayController@box_rec');
Route::name('box_unic_add')->post('pays/box/unic/add','Pays\PayController@box_unic_add');
Route::name('box_unic_cal')->get('pays/box/calc','Pays\PayController@box_unic_calc');
Route::name('box_unic_edit')->get('pays/box/unic/{pays}/edit', 'Pays\PayController@box_unic_edit');
Route::name('box_unic_up')->put('pays/box/unic/{pays}', 'Pays\PayController@box_unic_update');
Route::name('box_unic_delete')->delete('pays/box/unic/delete/{pays}', 'Pays\PayController@box_unic_delete');
Route::name('box_rec_add')->post('pays/box/rec/add','Pays\PayController@box_rec_add');
Route::name('box_rec_cal')->get('pays/box/c','Pays\PayController@box_rec_calc');
Route::name('box_rec_edit')->get('pays/box/rec/{pays}/edit', 'Pays\PayController@box_rec_edit');
Route::name('box_rec_up')->put('pays/box/rec/{pays}', 'Pays\PayController@box_rec_up');
Route::name('box_rec_delete')->delete('pays/box/rec/delete/{pays}', 'Pays\PayController@box_rec_delete');
Route::name('cut_box')->get('pays/cut/box','Pays\PayController@cut_box');
Route::name('cut_box_report')->get('pays/cut/box/report','Pays\PayController@cut_box_report');
Route::name('search_cut_box')->get('pays/cut/box/report/search','Pays\PayController@cut_box_report');
Route::name('details_box')->get('pays/details/{cut}/cut/box','Pays\PayController@details_box');
Route::name('details_np')->get('pays/details/{teacher}/np','Pays\PayController@details_np');
Route::name('details_npp')->get('pays/details/{nomine}/npp','Pays\PayController@details_npp');
Route::name('add_arqueo')->post('pays/cut/box/arq/{idcut}','Pays\PayController@add_arqueo');
Route::name('details_box_pdf')->get('pays/cut/{cut}/detail/box','Pays\PayController@detail_box_pdf');
Route::name('desface_pay')->get('pays/cut/{cut}/desfase/pay','Pays\PayController@desface_pay');
Route::name('add_deposito')->post('pays/cut/box/des/{idcut}','Pays\PayController@add_deposito');
Route::name('details_depos')->get('pays/details/{cut}/depos/box','Pays\PayController@details_depos');
Route::name('report_adeudos')->get('pays/report/adeudos', 'Pays\PayController@reporteAdeudos');
Route::name('report_adeudos_user')->get('pays/report/adeudos/{idUser}', 'Pays\PayController@reporteAdeudosByuser');
Route::name('cuts_job_center')->get('pays/cuts/job/center', 'Pays\PayController@cutsByJobCenter');
//END PAYS

//START SUBJECTS
    Route::name('subjects')->get('subjects', 'Cathedra\CathedraController@subjects');
     Route::name('indexS')->get('subjects/search', 'Cathedra\CathedraController@subjects');

    Route::name('subject_add')->post('subjects/add', 'Cathedra\CathedraController@addSubject');

    Route::name('subject_update')->put('subjects/edit/{subject}', 'Cathedra\CathedraController@updateSubject');

    Route::name('subject_delete')->delete('subjects/delete/{subject}', 'Cathedra\CathedraController@deleteSubject');
//END SUBJECTS

//START CATHEDRA
    Route::name('indexcath')->get('cathedra/index', 'Cathedra\CathedraController@index');
    Route::name('indexC')->get('cathedra/index/search', 'Cathedra\CathedraController@index');
    Route::name('cathedra_create')->get('cathedra/create', 'Cathedra\CathedraController@createcath');
    Route::name('cathedra_add')->post('cathedra/add', 'Cathedra\CathedraController@addcath');
    Route::name('cathedra_edit')->get('cathedra/{cathedra}/editcathedra', 'Cathedra\CathedraController@editcath');
    Route::name('cathedra_update')->put('cathedra/{cathedra}/updatecathedra', 'Cathedra\CathedraController@updatecathedra');
    Route::name('cathedra_delete')->delete('cathedra/{cathedra}', 'Cathedra\CathedraController@delete');
//END CATHEDRA

//START SCHOOL_CONTROL
    Route::name('groups')->get('groups/index', 'Groups\GroupController@index');
    Route::name('groups_create')->get('groups/create', 'Groups\GroupController@create');
    Route::name('groups_add')->post('groups/add', 'Groups\GroupController@add');
    Route::name('groups_edit')->get('groups/{group}/editgroup', 'Groups\GroupController@edit');
    Route::name('groups_update')->put('groups/{group}/updategroup', 'Groups\GroupController@update');
    Route::name('groups_delete')->delete('groups/{group}', 'Groups\GroupController@delete');
    Route::name('indexG')->get('groups/index/search','Groups\GroupController@index');
    Route::name('indexA')->get('groups/index/search/student','Groups\GroupController@index');
    Route::name('groups_assign')->get('groups/assign', 'Groups\GroupController@createassign');
    Route::name('groups_assign_add')->post('groups/assign/add', 'Groups\GroupController@addassign');
    Route::name('groups_assign_delete')->delete('group/assign/{assign}/delete', 'Groups\GroupController@deleteassign');
    Route::name('classroom')->get('classroom','Groups\GroupController@classroom_index');
    Route::name('indexclass')->get('classroom/search','Groups\GroupController@classroom_index');
    Route::name('classroom_add')->post('classroom/add','Groups\GroupController@classroom_add');
    Route::name('classroom_update')->put('classroom/edit/{classroom}','Groups\GroupController@classroom_update');
    Route::name('classroom_delete')->delete('classroom/delete/{classroom}','Groups\GroupController@classroom_delete');
    Route::name('student_status')->get('students/status','Groups\GroupController@status');
    Route::name('index_status')->get('students/status/search','Groups\GroupController@status');
    Route::name('studenStatus_update')->put('students/status/{studentStatus}', 'Groups\GroupController@updateStatus');
    Route::name('teachers')->get('teachers','Groups\GroupController@teachers');
    Route::name('indexM')->get('teachers/search','Groups\GroupController@teachers');
    Route::name('teacher_add')->post('teachers/add', 'Groups\GroupController@addTeacher');
    Route::name('teacherStatus_update')->put('teachers/status/{teacherStatus}', 'Groups\GroupController@updateStatusTeacher');
//END SCHOOL_CONTROL

//START ASSISTANCE
    Route::name('assistance')->get('assistance/index', 'Groups\GroupController@assistance');
    Route::name('assistance_edit')->get('assistance/{group}/editassistance', 'Groups\GroupController@createassistance');
    Route::name('assistance_add')->post('assistance/add', 'Groups\GroupController@addassistance');
    Route::name('report_assistance')->get('assistance/admin', 'Groups\GroupController@report_assistance');
    Route::name('index6')->get('assistance/admin/index', 'Groups\GroupController@report_assistance');
    Route::name('report_assistances')->get('assistance/student','Groups\GroupController@report_assistance_student');
    Route::name('report_assistance_detail')->get('assistance/admin/detail/{idTeacher}/{month}/{year}/{type}', 'Groups\GroupController@report_assistance_detail');
    Route::name('report_assistance_detail_student')->get('assistance/admin/detail/student/{idStudent}/{month}/{year}/{type}', 'Groups\GroupController@report_assistance_detail_student');
    Route::name('searchByMonth')->get('assistance/admin/searchByMonth', 'Groups\GroupController@report_assistance');
    Route::name('searchByMonthStudent')->get('assistance/student/searchByMonth', 'Groups\GroupController@report_assistance_student');
    Route::name('index7')->get('assistance/student/index','Groups\GroupController@report_assistance_student');
    Route::name('index7_teacher')->get('assistance/teacher/index/search','Groups\GroupController@report_assistance');
    Route::name('assistance_teacher')->get('assistance/teacher','Groups\GroupController@assistance_teacher');
    Route::name('index8')->get('assistance/teacher/index','Groups\GroupController@assistance_teacher');
    Route::name('add_ass_teacher')->post('assistance/teacher/add','Groups\GroupController@add_ass_teacher');
    Route::name('add_ass_teacher_other')->post('assistance/other/teacher/add/{idTeacher}','Groups\GroupController@add_ass_teacher_other');

    Route::name('alum_list')->get('assistance/{group}/alumnos', 'Groups\GroupController@alum_list');
    Route::name('group_detail')->get('assistance/report/{teacher}/{month}/{year}/group','Groups\GroupController@group_detail');

//END ASSISTANCE

//START SCHEDULE
    Route::name('schedule')->get('schedule', 'Schedule\ScheduleController@schedule');
    Route::name('schedule_student')->get('schedule_student', 'Schedule\ScheduleController@schedule_student');
    Route::name('schedule_teacher')->get('schedule_teacher', 'Schedule\ScheduleController@schedule_teacher');
    Route::name('schedule_by_jobcenter')->get('schedule/job/center', 'Schedule\ScheduleController@scheduleByJobCenter');
//END SCHEDULE

//START CHANGEPASS
Route::name('change_pass')->get('change/password', 'Password\PasswordController@password');
Route::name('update_pass')->get('change/password/changed', 'Password\PasswordController@passUpdate');
Route::name('success_pass')->get('change/password/success', 'Password\PasswordController@passSuccess');
//END CHANGEPASS

//START SAMPLE USERS 
Route::name('sample_users')->get('sample/users', 'Sampleusers\SampleusersController@index'); 
 
Route::name('sampleuser_add')->post('sample/users/add', 'Sampleusers\SampleusersController@add'); 
 
Route::name('sampleuser_update')->put('sample/users/edit/{sample}', 'Sampleusers\SampleusersController@updateSample'); 
 
Route::name('sample_delete')->delete('sample/delete/{sample}', 'Sampleusers\SampleusersController@deleteSample'); 
 
Route::name('sample_inscribe')->post('sample/users/inscribe/{sample}', 'Sampleusers\SampleusersController@inscribe'); 
//END SAMPLE USERS 

//START REGISTRATION
Route::name('create_student')->get('registration/student', 'Registrations\RegistrationController@registration_student');

Route::name('add_student')->post('registration/student/add', 'Registrations\RegistrationController@registration_add');

Route::name('create_student_evaluation')->get('registration/student/evaluation', 'Registrations\RegistrationController@registration_student_evaluation');

Route::name('create_teacher')->get('registration/teacher', 'Registrations\RegistrationController@registration_teacher');
Route::name('student_edit')->get('registration/{student}/edit', 'Registrations\RegistrationController@editstudent');
Route::name('student_up')->put('registration/{student}/updatestudent', 'Registrations\RegistrationController@updatestudent');

Route::name('student_quiz')->get('quiz/{student}', 'Registrations\RegistrationController@registration_student_evaluation');
Route::name('add_evaluation')->post('quiz/{student}/add', 'Registrations\RegistrationController@add_student_evaluation');

//END REGISTRATION
});
