<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * CABECERAS SEGURIDAD API REST APP
 */
/*-- START CABECERAS -- */
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, GX-Auth-Token, Origin, Authorization');
/*-- START CABECERAS -- */



Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['prefix' => 'v1','middleware' => 'auth:api'], function () {});


#region JSON

Route::group(['prefix' => 'js'], function () {

    //Prefijo para el modulo de tareas.
    Route::group(['prefix' => 'task'], function () {

        /**
         * Ruta para obtener una tarea por su Id.
         * 
         * @url public/circlek/api/js/task/task/{id}
         */
        Route::get('/task/{id}', 'Tasks\TaskController@TaskByIdJson');

        /**
         * Ruta para obtener el ultimo id de un documento de tarea.
         * 
         * @url public/circlek/api/js/task/task/{id}
         */
        Route::get('/getdocumenttask', 'Tasks\TaskController@GetDataDocumentTask');

        /**
         * Ruta para obtener el ultimo id de una evidencia.
         * 
         * @url public/circlek/api/js/task/task/{id}
         */
        Route::get('/getevidencetask', 'Tasks\TaskController@GetDataDocumentEvidence');

        /**
         * Ruta para obtener las tareas de un usuario por su Id.
         * 
         * @url public/circlek/api/js/task/task/createdBy/{idUser}
         */
        Route::get('/task/createdBy/{idUser}', 'Tasks\TaskController@CreatedByUserIdJson');

        /**
         * Ruta para obtener las tareas de un usuario por su Id.
         * 
         * @url public/circlek/api/js/task/task/createdBy/{idUser}
         */
        Route::get('/task/createdBystatus/{idUser}', 'Tasks\TaskController@GetStatusTaskCreatedByUserJson');

        /**
         * Ruta para obtener el estatus de una tarea por su Id.
         * 
         * @url public/circlek/api/js/task/task/createdBy/{idUser}
         */
        Route::get('/task/assignmentsBystatusTask/{idTask}', 'Tasks\TaskController@GetStatusTaskByIdTaskJson');

        /**
         * Ruta para obtener el estatus de las asignaciones.
         * 
         * @url public/circlek/api/js/task/task/createdBy/{idUser}
         */
        Route::get('/task/assignmentsBystatusTaskJson/{idUser}', 'Tasks\TaskController@GetStatusTasksAssignedsByUserJson');

        /**
         * Ruta para obtener el progreso de una tarea por su Id.
         * 
         * @url public/circlek/api/js/task/task/createdBy/{idUser}
         */
        Route::get('/task/progressTaskById/{idTask}', 'Tasks\TaskController@GetProgressByIdTask');

        /**
         * Ruta para guardar una nueva tarea.
         * 
         * @url public/circlek/api/js/task/task/add/{dataTask}
         */
        Route::post('/task/add', 'Tasks\TaskController@AddNewTaskJson');

        /**
         * Ruta para finalizar una tarea por usuario.
         * 
         * @url public/circlek/api/js/task/task/add/{dataTask}
         */
        Route::post('task/assignment/finaliceTask', 'Tasks\TaskController@SaveCompletedTask');

        /**
         * Ruta para actualizar el progreso de una tarea por su Id.
         * 
         * @url public/circlek/api/js/task/task/add/{dataTask}
         */
        Route::post('task/updateProgressByIdJson', 'Tasks\TaskController@UpdateProgressTask');

        /**
         * Ruta para guardar un nuevo documento de una tarea.
         * 
         * @url public/circlek/api/js/task/task/add/{dataTask}
         */
        Route::post('/task/addDocument', 'Tasks\TaskController@AddNewDocumentTaskJson');

        /**
         * Ruta para guardar una nueva evidencia de tarea en la bd.
         * 
         * @url public/circlek/api/js/task/task/add/{dataTask}
         */
        Route::post('/task/addEvidence', 'Tasks\TaskController@AddNewEvidenceTaskJson');

        /**
         * Ruta para guardar un nuevo archivo tipo documento tarea en la carpeta storage.
         * 
         * @url public/circlek/api/js/task/task/add/{dataTask}
         */
        Route::get('/addFileDocument', 'Tasks\TaskController@saveFileTaskDocument');

        /**
         * Ruta para guardar una nueva asignación.
         * 
         * @url public/circlek/api/js/task/task/add/{dataTask}
         */
        Route::post('/task/addAssignment', 'Tasks\TaskController@AddNewAssignmentTaskJson');

        /**
         * Ruta para guardar un nuevo mensaje.
         * 
         * @url public/circlek/api/js/task/task/add/{dataTask}
         */
        Route::post('task/addMessageTask', 'Tasks\TaskController@SaveMessageTaskByUser');

        /**
         * Ruta para obtener las asignaciones de una tarea por su usuario.
         * 
         * @url public/circlek/api/js/task/assignment/byUser/{id}
         */
        Route::get('/assignment/byUser/{id}','Tasks\AssignmentController@GetAssignmetByUserJson');

        /**
         * Ruta para obtener la asignacion de una tarea por su Id.
         * 
         * @url public/circlek/api/js/task/task/createdBy/{id}
         */
        Route::get('/assignment/byTask/{id}', 'Tasks\TaskController@GetAssignmentsByTaskIdJson');

        /**
         * Descargar el documento de la tarea por su Id.
         */
        Route::get('/getDocumentTask/byTask/{task}', 'Tasks\TaskController@downloadDocumentTask');

        /**
         * Descarga la evidencia de una tarea por id de assignemnt, task y user. 
         */
        Route::get('/getDownloadEvidence/{name}', 'Tasks\TaskController@downloadEvidenceByUserJson');

        /**
         * Elimina una evidencia por el nombre.
         */
        Route::get('/deleteEvidenceJson/{name}', 'Tasks\TaskController@deleteEvidenceByNameJson');

        /**
         * Obtiene los mensajes de una conversación por tarea y asignación de usuario.
         */
        Route::get('task/getMessagesByJson/{id_assignments}/{id_task}', 'Tasks\TaskController@getMessagesTaskByIdJson');

        /**
         * Obtiene todas las asignaciones creadas por un usuario.
         */
        Route::get('assignment/getAssignemntsByUserJson/{idUser}', 'Tasks\TaskController@GetAssignmentsByUserIdJson'); 

        /**
         * Obtiene todas las asignaciones de un usuario por su Id.
         */
        Route::get('assignment/getAssignemntsFromUserJson/{idUser}', 'Tasks\TaskController@getAssignmentsFromUserJson');
        
        /**
         * Ruta para obtener la asignacion completa de una tarea por su Id.
         * 
         * @url public/musicandmore/api/js/task/assignment/byAssign/{idAssign}/{idUser}
         */
        Route::get('/assignment/byAssign/{idAssign}/{idUser}', 'Tasks\TaskController@GetAssignmentsByAssignIdJson');

        Route::get('/evidence/byUser/{assigment_id}/{task_id}/{user_id}', 'Tasks\TaskController@GetEvidencesByUser');

        /**
         * Ruta para obtener la notificación desde la APP.
         *
         * @url public/circlek/api/protoc/task/assignment/notification/{id}/{task}
         */
        Route::get('/assignment/notification/{user}/{task}/{userAssign}/{idAssign}','Tasks\TaskController@notificationReciveJson');


    });

     //Prefijo para el modulo de usuarios.
     Route::group(['prefix' => 'user'], function () {

        /**
         * Retorna un JSON con los datos de los usuarios.
         * 
         * @url public/musicandmore/api/js/user/employees
         */
        Route::get('/employees', 'Tasks\TaskController@GetEmployeesJson');

        /**
         * Guarda el token del dispositivo usuario.
         *
         * @url public/circlek/api/protoc/task/user/tokenapp/{id}
         */
        Route::post('/tokenapp', 'Tasks\TaskController@SaveTokenUserJson');

        /**
         * Retorna un LoginResponse en base a los datos de sesión del usuario desde la app.
         * @url public/musicandmore/api/js/user/authenticate/{email}/{password}
         */
        Route::get('/authenticate/{email}/{password}', 'Auth\LoginController@authenticate');

    });

    //Prefijo para el modulo de Horario
    Route::group(['prefix' => 'schedule'], function () {

        /**
         * Retorna un JSON con las clases o eventos de un alumno.
         * @param idUser
         * @url public/musicandmore/api/js/user/employees
         */
        Route::get('/events/{idUser}', 'Schedule\ScheduleController@getEventsScheduleJson');

        /**
         * Retorna un JSON con las clases o eventos que imparte un maestro.
         * @param idTeacher
         * @url public/musicandmore/api/js/user/employees
         */
        Route::get('/events/teacher/{idTeacher}', 'Schedule\ScheduleController@getEventsScheduleByTeacher');

    });

    //Prefijo para el módulo de Asistencia.
    
    Route::group(['prefix' => 'assistance'], function() {

        /**
         * Retorna un JSON con todos los grupos asignados a un docente por su id de empleado.
         * @param idTeacher
         */
        Route::get('/groups/{idTeacher}', 'Groups\GroupController@getGroupsByTeacherForJSON');

        /**
         * Retorna un JSON con todos los grupos asignados a un docente por su id de empleado y día actual.
         * @param idTeacher
         */
        Route::get('/groups/{idTeacher}/day', 'Groups\GroupController@getGroupsByTeacherByDayForJSON');

        /**
         * Retorna un JSON con todos los grupos asignados a un docente por su id de empleado.
         * @param idTeacher
         */
        Route::get('/students/{idGroup}', 'Groups\GroupController@getListStudentsByIdGroup');

        /**
         * Guarda una nueva asistencia de grupo por Json.
         */
        Route::post('/save/general', 'Groups\GroupController@saveAsistancebyJson');

        /**
         * Guarda una nueva asistencia por alumno.
         */
        Route::post('/save/student', 'Groups\GroupController@saveAsistanceStudentbyJson');

        /**
         * Retorna un codeResponse en base al pase de lista.
         * @url public/musicandmore/api/js/assistance/validate/{id_group}/{id_teacher}
         */
        Route::get('/validate/{id_group}/{id_teacher}', 'Groups\GroupController@verifyAssistance');

        /**
         * Retorna un codeResponse en base al pase de lista del maestro.
         * @url public/musicandmore/api/js/assistance/validate/teacher/{id_group}/{id_teacher}
         */
        Route::get('/validate/teacher/{id_group}/{id_teacher}', 'Groups\GroupController@verifyAssistanceByTeacher');

    });
    
});