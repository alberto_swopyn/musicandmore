<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'accepted'              => 'Debe ser aceptado.',
    'active_url'            => 'No es una URL válida.',
    'after'                 => 'Debe ser una fecha después de :date.',
    'alpha'                 => 'Sólo puede contener letras.',
    'alpha_dash'            => 'Sólo puede contener letras, números y guiones.',
    'alpha_num'             => 'Sólo puede contener letras y números.',
    'array'                 => 'Debe ser un arreglo.',
    'before'                => 'Debe ser una fecha antes :date.',
    'between'               => [
        'numeric' => 'Debe estar entre :min - :max.',
        'file'    => 'Debe estar entre :min - :max kilobytes.',
        'string'  => 'Debe estar entre :min - :max caracteres.',
        'array'   => 'Debe tener entre :min y :max elementos.',
    ],
    'boolean'               => 'Debe ser verdadero o falso.',
    'confirmed'             => 'El campo de confirmación de :Attribute no coincide.',
    'date'                  => 'No es una fecha válida.',
    'date_format'           => 'No corresponde con el formato :format.',
    'different'             => 'Los campos :Attribute y :other deben ser diferentes.',
    'digits'                => 'Debe ser de :digits dígitos.',
    'digits_between'        => 'Debe tener entre :min y :max dígitos.',
    'dimensions'            => 'No tiene una dimensión válida.',
    'distinct'              => 'Tiene un valor duplicado.',
    'email'                 => 'El formato del :Attribute es inválido.',
    'exists'                => 'Seleccionado es inválido.',
    'file'                  => 'Debe ser un archivo.',
    'filled'                => 'Es requerido.',
    'image'                 => 'Debe ser una imagen.',
    'in'                    => 'Seleccionado es inválido.',
    'in_array'              => 'No existe en :other.',
    'integer'               => 'Debe ser un entero.',
    'ip'                    => 'Debe ser una dirección IP válida.',
    'json'                  => 'Debe ser una cadena JSON válida.',
    'max'                   => [
        'numeric' => 'Debe ser menor que :max.',
        'file'    => 'Debe ser menor que :max kilobytes.',
        'string'  => 'Debe ser menor que :max caracteres.',
        'array'   => 'Debe tener al menos :min elementos.',
    ],
    'mimes'                 => 'Debe ser un archivo de tipo: :values.',
    'mimetypes'             => 'Debe ser un archivo de tipo: :values.',
    'min'                   => [
        'numeric' => 'Debe tener al menos :min.',
        'file'    => 'Debe tener al menos :min kilobytes.',
        'string'  => 'Debe tener al menos :min caracteres.',
        'array'   => 'Debe tener al menos :min items.',
    ],
    'not_in'                => 'Seleccionado es invalido.',
    'numeric'               => 'Debe ser un número.',
    'present'               => 'Debe estar presente.',
    'regex'                 => 'El formato des inválido.',
    'required'              => 'Es requerido.',
    'required_if'           => 'Es requerido cuando el campo :other es :value.',
    'required_unless'       => 'Es requerido a menos que :other esté presente en :values.',
    'required_with'         => 'Es requerido cuando :values está presente.',
    'required_with_all'     => 'Es requerido cuando :values está presente.',
    'required_without'      => 'Es requerido cuando :values no está presente.',
    'required_without_all'  => 'Es requerido cuando ningún :values está presente.',
    'same'                  => ':other debe coincidir.',
    'size'                  => [
        'numeric' => 'Debe ser :size.',
        'file'    => 'Debe tener :size kilobytes.',
        'string'  => 'Debe tener :size caracteres.',
        'array'   => 'Debe contener :size elementos.',
    ],
    'string'                => 'Debe ser sólo letras.',
    'timezone'              => 'Debe ser una zona válida.',
    'unique'                => 'Ya ha sido tomado.',
    'url'                   => 'El formato de :Attribute es inválido.',
    'uploaded'              => 'No ha podido ser cargado.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "Attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given Attribute rule.
    |
    */
    'custom' => [
        'Attribute-name' => [
            'rule-name'  => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap Attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes' => [
        'username' => 'usuario',
        'password' => 'contraseña'
    ],
];