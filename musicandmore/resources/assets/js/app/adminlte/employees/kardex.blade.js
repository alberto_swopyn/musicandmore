//employees/kardex.blade.php
$(document).ready(function() { // 6,32 5,38 2,34
	if ($('#kardex_Employee').length) {

        //INICIA SCRIPT
//start modal new employees
        $('#EmployeeNew').click(function() {
            console.log('Hola');
            var nameEmployee = $('#selectEmployee').val();
            var company = $('#newCompany').val();
            var jobtitle = $('#inputjobTitle').val();
            var jobcenter = $('#inputJobCenter').val();
            var nameEmployee = $("#selectEmployee option:selected").html().replace('\n','');
            var applicantID = $('#selectEmployee option:selected').val();
            //var keyEmployee = $('#keyEmployee').val();

            $.ajax({
              type: 'POST',
              url: "update",
              data: {
                 _token: $("meta[name=csrf-token]").attr("content"),
                 Applicant_id: applicantID,
                 Company : company,
                 inputjobTitle : jobtitle,
                 inputJobCenter : jobcenter,
                 nameEmployee : nameEmployee,
                 //keyEmployee : keyEmployee
             },
             success: function(data) {
                 if (data.errors) {
                    faltante();
                }
                else{
                    correcto();
                    location.reload();
                }
            }
        });
        })

        $('#newEmployee').click(function() {
            $('#modalTitle').text('Crear nuevo empleado');
            $('#EmployeeUp').text('Guardar');
            $('#nameEmployee').val(1);
            $('#nameEmployee').trigger('change');
            $('#inputjobTitle').val('');
            $('#inputJobCenter').val('');
        });

//end modal new employees
        
//modal employees update 
        $('.btnUpdateEmployee').click(function() {
        	var all = $(this);
            $('#Applicant_id').val(all.attr('data-id'));
        	$.ajax({
        		type: 'POST',
        		url: "search",
        		data: {
        			_token: $("meta[name=csrf-token]").attr("content"),
        			Employee_id: all.attr('data-id'),
        		},
        		success: function(data) {
        			if (data.errors) {
        			}
        			else{
        				$('#inputCompany').val(data.inputCompany);
        				$('#updatejobTitle').val(data.inputjobTitle).trigger('change.select2');
        				$('#updateJobCenter').val(data.inputJobCenter).trigger('change.select2');
        				$('#nameEmployee').val(data.name);
                    }
                }
            });
        });

        $('#EmployeeUp').click(function(){
            var company = $('#inputCompany').val();
            var jobtitle = $('#updatejobTitle').val();
            var jobcenter = $('#updateJobCenter').val();
            var applicantID = $('#Applicant_id').val();
            var nameEmployee = $('#nameEmployee').val();

            $.ajax({
              type: 'POST',
              url: "update",
              data: {
                 _token: $("meta[name=csrf-token]").attr("content"),
                 Applicant_id: applicantID,
                 Company : company,
                 inputjobTitle : jobtitle,
                 inputJobCenter : jobcenter,
                 nameEmployee : nameEmployee
             },
             success: function(data) {
                 if (data.errors) {
                    faltante();
                }
                else{
                    correcto();
                    location.reload();
                }
            }
        });
        })
//end modal employees update

//sweet alert
        function correcto() {
        	swal({
        		title: "Datos guardados correctamente",
        		type: "success",
        		timer: 2000,
      showCancelButton: false, // There won't be any cancel button
      showConfirmButton: false // There won't be any confirm button
  })
            .catch(swal.noop);
        }

        function faltante() {
        	swal({
        		title: "¡Espera!",
        		text: "Faltan datos por ingresar",
        		imageUrl: "../img/icon-lte-09.png",
        		imageWidth: 200,
        		imageHeight: 200,
        		timer: 2000,
        		showCancelButton: false,
        		showConfirmButton: false
        	}).catch(swal.noop);
        }
//end sweet alert
    }
});