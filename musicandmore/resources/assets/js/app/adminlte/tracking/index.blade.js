//Tracking-index.blade.php
$(document).ready(function() { // 6,32 5,38 2,34
	if ($('#tracking_index').length) {
        //INICIA SCRIPT
        $(".matchModal").on('click', function(event) {
        	var matchUser = $(this).attr('data-info');
        	$.ajax({
        		type: 'POST',
        		url: 'matchporcentaje',
        		data: {
        			'_token': $('meta[name=csrf-token]').attr('content'),
        			'applicant' : matchUser
        		},
                
        		success: function (data) {
        			$('#id_applicant').val(data.personal_information[0].general_personal_data_id);
        			$('#name_applicant').text(data.personal_information[0].first_name + ' ' +data.personal_information[0].last_name);
        			$('#work_applicant').text(data.work[0]['job_title_name']);
        			$('#test-circle').empty();
        			$('#test-circle').circliful({
        				foregroundColor:( data.porcentaje[0].percentage_match > 69 ) ? "green": "red" ,
        				backgroundColor: "#eee",
        				pointColor: "none",
        				fillColor: 'none',
        				foregroundBorderWidth: 20,
        				backgroundBorderWidth: 10,
        				pointSize: 28.5,
        				fontColor: '#aaa',
        				animation: 1,
        				animationStep: 5,
        				icon:'A AVANCE ',
        				iconSize: '17',
        				iconColor: '#999',
        				iconPosition: 'middle',
        				target: 0,
        				start: 0,
        				showPercent: 1,
        				percentageTextSize: 22,
        				textAdditionalCss: '',
        				targetPercent: 0,
        				targetTextSize: 17,
        				targetColor: '#2980B9',
        				textStyle: 'top',
        				textColor: '#000',
        				multiPercentage: 0,
        				percentages: null,
        				textBelow: 1,
        				noPercentageSign: false,
        				replacePercentageByText: null,
        				halfCircle: false,
        				animateInView: false,
        				decimals: 0,
        				alwaysDecimals: false,
        				percent: data.porcentaje[0].percentage_match
        			});
        			document.getElementById("VerDetalles").disabled = false;
        			$('#matchModal').modal('show');
        		}
        	});
        });

        $(document).on('click', '#VerDetalles', function() {
        	$.ajax({
        		type: 'POST',
        		url: '../tracking/matchdetalles',
        		data: {
        			'_token': $('meta[name=csrf-token]').attr('content'),
        			'applicant' : $('#id_applicant').val()
        		},
        		success: function (data) {
        			var lenguajes = "";
        			var lenguajesnot = "";
        			var level="";
        			var levelnot="";
        			var carrer="";
        			var carrernot="";
        			var aca_grade="";
        			var aca_grade_not="";
        			var arraylenguajes=[];
        			var array_aca_grade=[];
        			var array_carrer=[];
        			$('#gender_applicant').empty();
        			$('#edad_applicant').empty();
        			$('#civilstatus_applicant').empty();
        			$('#lan_applicant').empty();
        			$('#lannot_applicant').empty();
        			$('#aca_grade_applicant').empty();
        			$('#aca_grade_not_applicant').empty();
        			$('#carrer_applicant').empty();
        			$('#carrer_not_applicant').empty();
        			$('#level_applicant').empty();
        			$('#levelnot_applicant').empty();
        			$('#gender_applicant').text(data.personal_information[0].gender);
        			$('#edad_applicant').text(data.personal_information[0].age);
        			$('#civilstatus_applicant').text(data.personal_information[0].civil_status);

        			if (data.personal_information[0].gender == data.work[0].gender || data.work[0].gender == "Indistinto") {
        				$('#gender_applicant').css("color","green");
        			}
        			else{
        				$('#gender_applicant').css("color","red");
        			}
        			if ((data.personal_information[0].age>=data.work[0].min_age)&&(data.personal_information[0].age<=data.work[0].max_age)) {
        				$('#edad_applicant').css("color","green")
        			}
        			else{
        				$('#edad_applicant').css("color","red")
        			}
        			for (var i = 0; i < data.lanasp[0].length; i++) {
        				arraylenguajes[i]=0;
        				for (var c = 0; c < data.lansol[0].length; c++) {
        					if (data.lanasp[0][i].language == data.lansol[0][c].language) {
        						arraylenguajes[i]=1;
        					}
        				}
        				if (arraylenguajes[i]==1) 
        				{
        					if (data.lanasp[0][i].level >= 70) {
        						level = level + data.lanasp[0][i].level + '.<br>';
        						lenguajes = lenguajes + data.lanasp[0][i].language + '.<br>';
        					}
        					else
        					{
        						lenguajesnot = lenguajesnot + data.lanasp[0][i].language + '.<br>';
        						levelnot = levelnot + data.lanasp[0][i].level + '.<br>';
        					}
        				}
        				else{
        					lenguajesnot = lenguajesnot + data.lanasp[0][i].language + '.<br>';
        					levelnot = levelnot + data.lanasp[0][i].level + '.<br>';
        				}	
        			}
        			if (data.personal_information[0].civil_status == data.work[0].civil_status || data.work[0].civil_status == "Indistinto") {
        				$('#civilstatus_applicant').css("color","green");
        			}
        			else
        			{
        				$('#civilstatus_applicant').css("color","red");
        			}
        			for (var i = 0; i < data.aca_grade[0].length; i++) {
        				array_aca_grade[i]=0
        				for (var c = 0; c < data.aca_grade_sol[0].length; c++) {
        					if (data.aca_grade[0][i] == data.aca_grade_sol[0][c]) {
        						array_aca_grade[i]=1;

        					}
        				}
        				if (array_aca_grade[i]==1) {
        					aca_grade = aca_grade + data.aca_grade[0][i] + '.<br>';
        				}
        				else{
        					aca_grade_not = aca_grade_not + data.aca_grade[0][i] + '.<br>';
        				}	
        			}

        			for (var i = 0; i < data.degree_name[0].length; i++) {
        				array_carrer[i]=0;
        				for (var c = 0; c < data.degree_name_sol[0].length; c++) {
        					if (data.degree_name[0][i] == data.degree_name_sol[0][c]) {
        						array_carrer[i]=1;
        					}
        				}
        				if (array_aca_grade[i]==1) {
        					carrer = carrer + data.degree_name[0][i] + '.<br>';
        				}
        				else{
        					carrernot = carrernot + data.degree_name[0][i] + '.<br>';
        				}	
        			}
        			$('#lan_applicant').append(lenguajes).css("color","green");
        			$('#lannot_applicant').append(lenguajesnot).css("color","black");
        			$('#aca_grade_applicant').append(aca_grade).css("color","green");
        			$('#aca_grade_not_applicant').append(aca_grade_not).css("color","black");
        			$('#carrer_applicant').append(carrer).css("color","green");
        			$('#carrer_not_applicant').append(carrernot).css("color","black");
        			$('#level_applicant').append(level).css("color","green");
        			$('#levelnot_applicant').append(levelnot).css("color","black");
			// document.getElementById("VerDetalles").disabled = true;
		}
	});
$('#Modaldetalles').modal('show');
});
    // call the tablesorter plugin 
    $("#tableAspirants").tablesorter({ 
        // pass the headers argument and assing a object 
        headers: { 
            // assign the secound column (we start counting zero) 
            1: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }, 
            // assign the third column (we start counting zero) 
            6: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            } 
        } 
    });
}
});