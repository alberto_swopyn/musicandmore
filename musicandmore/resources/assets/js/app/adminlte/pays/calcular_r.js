$(document).ready(function(){
    $('#calcular_r').click(function(){
		let aument;
        let curso = $('#Curso').val();
        let student = $('#Estudiante').val();
		let discount = $('#Descuento').val();
		if(document.querySelector('input[name="no_aument"]:checked')) {
                aument = 1;
            }

        if(curso.length === 0){
            missingText('Seleccione el pago correspondiente')
        }else if(student.length === 0){
            missingText('Seleccione el estudiante correspondiente')
        }else if(discount.length === 0){
            missingText('Seleccione el descuento correspondiente')
        }else{
            calcular();
        }

        function calcular(){
            $.ajax({
                type: 'GET',
                url: "c",
                data:{
                    _token: $("meta[name=csrf-token]").attr("content"),
                    curso:curso,
                    student:student,
					discount:discount,
					aument:aument
                },
                success: function(data){
                    if (data.errors){
                        faltante();
                        //missingText('Algo salio mal');
                    }
                    else{
                        var json = JSON.parse(JSON.stringify(data));
                        document.getElementById("price").innerHTML = data.price;
                        correcto();
                    }
                }
            });
        }
    });
});

function correcto() {
    swal({
        title: "Calculo realizado",
        type: "success",
        timer: 2000,
showCancelButton: false, // There won't be any cancel button
showConfirmButton: false // There won't be any confirm button
})
    .catch(swal.noop);
}

function faltante() {
    swal({
        title: "¡Espera!",
        text: "Faltan datos por ingresar",
        imageUrl: "../img/icon-lte-09.png",
        imageWidth: 200,
        imageHeight: 200,
        timer: 2000,
        showCancelButton: false,
        showConfirmButton: false
    }).catch(swal.noop);
}