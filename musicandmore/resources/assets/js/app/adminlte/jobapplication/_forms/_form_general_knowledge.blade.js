//jobapplication/forms/general_knowledges.blade.php
$(document).ready(function() {
	if ($('#general_knowledge').length) {
		//INICIA SCRIPT
		$('.deleteLanguage').click(function() {
		// deleteLan();
		var languageJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('languageGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				languageJob.remove();
			}
		});
	});
		$('.deleteOficina').click(function() {
		// deleteLan();
		var officeJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('officeGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				officeJob.remove();
			}
		});
	});
		$('.deleteMaquinaria').click(function() {
		// deleteLan();
		var machineJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('machineGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				machineJob.remove();
			}
		});
	});
		$('.deleteSoftware').click(function() {
		// deleteLan();
		var softJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('softGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				softJob.remove();
			}
		});
	});
		$('.deleteOtros').click(function() {
		// deleteLan();
		var otherJob = $(this).parent('td').parent('tr');
		$.ajax({
			type: "POST",
			url: "{{ route('otherGeneral_delete') }}",
			data: {
				_token: $("meta[name=csrf-token]").attr("content"),
				Id : $(this).attr("data-info")
			},
			success: function(data){
				otherJob.remove();
			}
		});
	});


	}
	
});