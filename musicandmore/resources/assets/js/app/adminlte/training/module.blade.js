//training/_formmodule.blade.php
$(document).ready(function() {
        //START SCRIPT
//start modal new employees
        $('#ModuleNew').click(function() {
            console.log('Hello');
            let course = $('#course').val();
            let moduleName = $('#moduleName').val();

            $.ajax({
                type: 'POST',
                url: "../module/create",
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    course: course,
                    moduleName : moduleName
                },
                success: function(data) {
                    if (data.errors) {
                        faltante();
                    }
                    else{
                        correcto();
                        location.reload();
                    }
                }
            });
        });

//end modal new employees

//sweet alert
        function correcto() {
            swal({
                title: "Datos guardados correctamente",
                type: "success",
                timer: 2000,
                showCancelButton: false, // There won't be any cancel button
                showConfirmButton: false // There won't be any confirm button
            })
                .catch(swal.noop);
        }

        function faltante() {
            swal({
                title: "¡Espera!",
                text: "Faltan datos por ingresar",
                imageUrl: "../img/icon-lte-09.png",
                imageWidth: 200,
                imageHeight: 200,
                timer: 2000,
                showCancelButton: false,
                showConfirmButton: false
            }).catch(swal.noop);
        }
//end sweet alert

});