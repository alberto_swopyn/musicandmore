//courseevaluation-CompanyEvaluation
$(document).ready(function() { // 6,32 5,38 2,34
        if ($('#courseevaluation-CompanyEvaluation').length) {

            var total = parseInt($('#Work_capacities').val());
            var totalContratados = parseInt($('#EmployeesTotal').val());

           // var Vacantes = ( (total - totalContratados) * 100 / total);
            var Ocupacion = (parseInt($('#EmployeesTotal').val())) * 100 / parseInt($('#Work_capacities').val());
            var Capacitacion = (parseInt($('#ProgressTotal').val()));

            //Con esta variable obtengo el porcentaje de la vista del div
            var Sol = parseInt($('#Solicitantes').text());
            var Psy = parseInt($('#Psy').text());
            var Con = parseInt($('#Con').text());


        //INICIA SCRIPT
        $(".graphOcupation").circliful({
            animation: 1,
            animationStep: 2,
            foregroundBorderWidth: 15,
            backgroundBorderWidth: 10,
            percent: (total > 0) ? Ocupacion : 0,
            iconColor: '#2daae3',
            icon: 'f0c0',
            iconSize: '40',
            iconPosition: 'middle',
            foregroundColor: '#2daae3',
        });
        $(".graphVacant").circliful({
                animation: 1,
                animationStep: 5,
                foregroundBorderWidth: 7,
                backgroundBorderWidth: 7,
                textSize: 28,
                textStyle: 'font-size: 12px;',
                textColor: '#666',
                multiPercentage: 1,
                percentages: [
                    {'percent': Sol, 'color': '#3180B8', 'title': 'Elemento1' },
                    {'percent': Psy, 'color': '#4ADBEA', 'title': 'Elemento2' },
                    {'percent': Con, 'color': '#49EBA8', 'title': 'Elemento3' },
                ],
                multiPercentageLegend: 0,
                replacePercentageByText: '',
                backgroundColor: '#eee',
                icon: 'f1ad',
                iconPosition: 'middle',
                iconColor: '#273B4E',
        });
        // $(".graphVacant").circliful({
        //     animation: 1,
        //     animationStep: 2,
        //     foregroundBorderWidth: 15,
        //     backgroundBorderWidth: 10,
        //     percent: (total > 0) ? Vacantes : 0,
        //     iconColor: '#00a65a',
        //     icon: 'f1ad',
        //     iconSize: '40',
        //     iconPosition: 'middle',
        //     foregroundColor: '#00a65a'
        // });
        $(".graphTraining").circliful({
            animation: 1,
            animationStep: 6,
            foregroundBorderWidth: 15,
            backgroundBorderWidth: 10,
            percent: (total > 0) ? Capacitacion : 0,
            iconColor: '#ff7676',
            icon: 'f2b5',
            iconSize: '40',
            iconPosition: 'middle',
            foregroundColor: '#ff7676',
        });
    }
});