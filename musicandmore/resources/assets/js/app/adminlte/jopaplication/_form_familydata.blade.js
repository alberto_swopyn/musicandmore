$(document).ready(function () {
    if ($('#family_datas').length) {
        //Inicia el Script
        $('.delete_fam').click(function() {
            var fam = $(this).parent('td').parent('tr');

            $.ajax({
               type: "POST",
               url: "../family_data/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                fam.remove();
            }
        });
        });
    }
});