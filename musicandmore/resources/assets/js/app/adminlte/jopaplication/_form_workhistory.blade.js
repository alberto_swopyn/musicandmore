$(document).ready(function () {
    if ($('#work_history').length) {
        //Inicia el Script
        $('.deletework_h').click(function() {
            var work = $(this).parent('td').parent('tr');
           
            $.ajax({
               type: "POST",
               url: "../work_history/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                 work.remove();
            }
        });
        });
    }
});