$(document).ready(function () {
    if ($('#personal_references').length) {
        //Inicia el Script
        $('.deletework_PersonalReferences').click(function() {
            var ref = $(this).parent('td').parent('tr');
            $.ajax({
               type: "POST",
               url: "../personal_references/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                ref.remove();
            }
        });
        });
    }
});