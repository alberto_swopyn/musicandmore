$(document).ready(function () {
    if ($('#scholarship_datas').length) {
        //Inicia el Script
        $('.delete_schools').click(function() {
            var schools = $(this).parent('td').parent('tr');
            $.ajax({
               type: "POST",
               url: "../scholarship_datas/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                schools.remove();
            }
        });
        });
    }
});