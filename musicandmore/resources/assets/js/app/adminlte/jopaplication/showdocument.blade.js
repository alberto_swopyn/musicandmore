//JobAplication/showdocument.blade.php
$(document).ready(function () {
    if ($('#JobAplication_ShowDocument').length) {

        //Inicia el Script

        /**
         * Inicializa el venobox.
         */
        $('.venobox').venobox({
            bgcolor: '#5dff5e',         // default: '#fff'
            titleattr: 'data-title',    // default: 'title'
            idattr: 'data-id',
            numeratio: true,            // default: false
            infinigall: true    
        }); 

        /**
         * Oculta o muestra el dialogo de comentarios.
         */
        $(".comentary-target").on('click', function () {
            var CommentaryPop = $(this).parent().parent().parent().children(".comentary-pop")[0];
            
            if ($(CommentaryPop).attr("isshow") == "true") {
                $(CommentaryPop).hide(500);
                $(CommentaryPop).attr("isshow", "false");
            } else {
                $(CommentaryPop).show(500);
                $(CommentaryPop).attr("isshow", "true");
            }
        });

        /**
         * Trigger para mandar el comentario de un documento al servidor.
         */
        $(".commentary-btn").on('click', function () {
            ProgressDialogModal.ShowModal();
            var commentaryMessage = $(this).parent().find('#comment').val();
            var idDocument = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: "../../documents/commentary",
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: idDocument,
                    messageDocument : commentaryMessage,
                },
                success: function(data) {
                    ProgressDialogModal.HideModal();

                    swal({
                        title: "El comentario se ha modificado",
                        type: "success",
                        timer: 0,
                        showCancelButton: false, // There won't be any cancel button
                        showConfirmButton: false // There won't be any confirm button
                      }).catch(swal.noop);
                },
                error: function (error) {
                    ProgressDialogModal.HideModal();
                    swal({
                        title: "Oops.. No se pudo cambiar el comentario",
                        type: "error",
                        timer: 0,
                        showCancelButton: false,
                        showConfirmButton: false
                      }).catch(swal.noop);
                }
            });
        });

        /**
         * Trigger para los botones de validacion.
         */
        $(".validatedButton").on('click', function () {
            var isValidated = parseInt($(this).attr('data-validated'));
            var idDocument = $(this).attr('data-id');
            var nameDocument = $(this).attr('data-name');
            var idUser = $(this).attr('data-user');

            swal({
                title: '¿Estas seguro?',
                text: (isValidated) ? "Vas a invalidar el documento" : "El documento sera valido",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: (isValidated) ? "Invalidar" : "Validar",
                cancelButtonText: 'Cancelar',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                reverseButtons: true
              }).then((result) => {
                if (result) {
                    
                    validatedDocument(this,isValidated, idDocument, nameDocument, idUser);
                } 
                else {
                  
                }
              })
        });

        /**
         * Valida o invalida el documento.
         * 
         * @param {any} button Boton que lo ejecuta.
         * @param {any} isValidated Estado a asignar al documento.
         * @param {any} id Id del Documento.
         * @param {any} name Nombre del documento.
         */
        function validatedDocument(button, isValidated, id, name, idUser)
        {
            ProgressDialogModal.ShowModal();
            
            var ParentBase = $(button).parent().parent().parent().parent()[0];
            var venoboxLabel = $(ParentBase).find("#a-venobox");
            var validateButton = button;

            $.ajax({
                type: "POST",
                url: isValidated ? "../../documents/invalidated" : "../../documents/verified",
                data: {
                    _token: $("meta[name=csrf-token]").attr("content"),
                    id: id,
                    title : name,
                    iduser : idUser
                },
                success: function(data) {
                    ProgressDialogModal.HideModal();

                    if(!isValidated)
                    {
                        $(venoboxLabel).addClass("venobox-validated");
                        $(validateButton).attr("data-validated","1");
                        $(validateButton).html("Invalidar");
                    }
                    else{
                        $(venoboxLabel).removeClass("venobox-validated");
                        $(validateButton).attr("data-validated","0");
                        $(validateButton).html("Validar");
                    }
                    //location.reload();
                    swal({
                        title: isValidated ? "El documento ha sido invalidado" : "El documento ha sido validado",
                        type: "success",
                        timer: 0,
                        showCancelButton: false, // There won't be any cancel button
                        showConfirmButton: false // There won't be any confirm button
                      }).catch(swal.noop);
                },
                error: function (error) {
                    ProgressDialogModal.HideModal();
                    swal({
                        title: "Oops.. No se pudo cambiar el estado",
                        type: "error",
                        timer: 0,
                        showCancelButton: false,
                        showConfirmButton: false
                      }).catch(swal.noop);
                }
            });
        }


    }
});