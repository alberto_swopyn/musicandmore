$(document).ready(function () {
    if ($('#tab_knowledges').length) {
        //Inicia el Script
        $('.delete_function').click(function() {
            var fn = $(this).parent('td').parent('tr');
            $.ajax({
               type: "POST",
               url: "../../../jobtitle_office/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                fn.remove();
            }
        });
        });
        $('.delete_mac').click(function() {
            var mac = $(this).parent('td').parent('tr');
            
            $.ajax({
               type: "POST",
               url: "../../../jobtitle_machine/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                mac.remove();
                
            }
        });
        });
        $('.delete_soft').click(function() {
            var soft = $(this).parent('td').parent('tr');
            $.ajax({
               type: "POST",
               url: "../../../jobtitle_software/delete",
               data: {
                   _token: $("meta[name=csrf-token]").attr("content"),
                   Id : $(this).attr("data-info")
               },
               success: function(data){
                soft.remove();
                
            }
        });
        });

    }
});