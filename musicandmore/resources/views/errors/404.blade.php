<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
  <title>Page not found </title>
  <link rel="shortcut icon" href="img/icon_swopyn.png">
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script language="JavaScript">
    /* Determinamos el tiempo total en segundos */
    var totalTiempo=10;
    /* Determinamos la url donde redireccionar */

    function updateReloj()
    {
      document.getElementById('CuentaAtras').innerHTML = "Redireccionando en "+totalTiempo+", segundos por favor espere";
      if(totalTiempo==0)
      {
       javascript:history.back()
      }else{
        /* Restamos un segundo al tiempo restante */
        totalTiempo-=1;
        /* Ejecutamos nuevamente la funci��n al pasar 1000 milisegundos (1 segundo) */
        setTimeout("updateReloj()",1000);
      }
    }
    window.onload=updateReloj;
  </script> 
</head>
<style type="text/css">
  html, body, .container-table {
    height: 100%;
  }
  .container-table {
    display: table;
  }
  .vertical-center-row {
    display: table-cell;
    vertical-align: middle;
  }
</style>
<body>
  <div class="container container-table">
    <div class="row vertical-center-row">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <img class="img-responsive center-block"  src="{{ asset('/img/doggysad.gif') }}">
          </div>
          <div class="col-sm-8 text-center">
            <img class="img-responsive" src="{{ asset('/img/FirmaSwopynCVS-01.png') }}">
            <h1 >PÁGINA NO ENCONTRADA - 404 </h1>
            <h3 >Es decir, esto es un error.<br>Si estás pérdido regresa a <a href="javascript:history.back()">nuestra web.</a></h3>
            <br><h4 id='CuentaAtras'></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>