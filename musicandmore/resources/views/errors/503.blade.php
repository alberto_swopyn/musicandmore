<!DOCTYPE html>
<html lang="en">
<head>
  <title>Error </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
  html, body, .container-table {
    height: 100%;
  }
  .container-table {
    display: table;
  }
  .vertical-center-row {
    display: table-cell;
    vertical-align: middle;
  }
</style>
<body>  
  <div class="container container-table">
    <div class="row vertical-center-row">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <img class="img-responsive center-block"  src="gifs/doggysad.gif">
          </div>
          <div class="col-sm-8 text-center">
            <img class="img-responsive" src="PNG/FirmaSwopynCVS-01.png">
            <h1 >Error HTTP 503 Servicio no disponible </h1>
            <h3 >Por favor espera un momento e intenta de nuevo.</h3>
            <h3 >Para más información lee. <a href="Informes">Informacion</a></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>