<!DOCTYPE html>
<html lang="en">
<head>
  <title>Error </title>
  <link rel="shortcut icon" href="img/icon_swopyn.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script language="JavaScript">
    /* Determinamos el tiempo total en segundos */
    var totalTiempo=10;
    /* Determinamos la url donde redireccionar */

    function updateReloj()
    {
      document.getElementById('CuentaAtras').innerHTML = "Redireccionando en "+totalTiempo+", segundos por favor espere";
      if(totalTiempo==0)
      {
       javascript:history.back()
     }else{
      /* Restamos un segundo al tiempo restante */
      totalTiempo-=1;
      /* Ejecutamos nuevamente la función al pasar 1000 milisegundos (1 segundo) */
      setTimeout("updateReloj()",1000);
    }
  }
  window.onload=updateReloj;
</script> 
</head>
<style type="text/css">
  html, body, .container-table {
    height: 100%;
  }
  .container-table {
    display: table;
  }
  .vertical-center-row {
    display: table-cell;
    vertical-align: middle;
  }
</style>
<body>  
  <div class="container container-table">
    <div class="row vertical-center-row">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <img class="img-responsive center-block"  src="img/doggysad.gif">
          </div>
          <div class="col-sm-8 text-center">
            <img class="img-responsive" src="img/FirmaSwopynCVS-01.png">
            <h1 >Error HTTP 500 Error interno del servidor </h1>
            <h3 >¡Vaya! Algo salío mal</h3><br>
            <h3 ><a href="javascript:history.back()">Trata de volver a cargar esta página</a> o no dudes <br> 
            en contactarte con nosotros si el problema persiste. </h3>
            <h4 id='CuentaAtras'></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>