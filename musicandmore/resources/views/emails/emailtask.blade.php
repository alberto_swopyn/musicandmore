<img src="{{ asset( 'img/logo_circle_k.png') }}" alt="" class="img-responsive">
<h1 style="text-align: center;">Asignación de Tareas</h1>
<div style="text-align: justify;">
	<h1 style="text-align: center;">Se ha asignado una nueva tarea</h1>
</div>
<br>
<div style="">
	<h1 style="text-align: center;">Nombre:</h1>
		<p style="text-align: center;">{{$task->title}}</p>
	<h1 style="text-align: center;">Objetivo:</h1>
		<p style="text-align: center;">{{$task->objective}}</p>
	<h1 style="text-align: center;">Descripción:</h1>
		<p style="text-align: center;">{{$task->description}}</p>
	<h1 style="text-align: center;">Fecha Inicial:</h1>
		<p style="text-align: center;">{{$task->initial_date}}</p>
	<h1 style="text-align: center;">Fecha Final:</h1>
		<p style="text-align: center;">{{$task->final_date}}</p>
	<h1 style="text-align: center;">Comentario:</h1>
		<p style="text-align: center;">{{$task->comment}}</p>
</div>
<br>
<p style="text-align: justify;">Gracias</p>