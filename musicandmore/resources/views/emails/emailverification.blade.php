@component('mail::message')

# Bienvenido

<div style="text-align: justify;">
<p style="text-align: justify;">
A partir de este momento te llevaremos de la mano dentro de tu proceso y para ello, necesitamos que nos ayudes con completar toda la información que se solicita al 100%. La primera actividad será el llenado de solicitud, la cual te llevará a realizar una prueba. Una vez que se concluya, se programará una entrevista para darte mayor información sobre nuestro proceso.</p>
<p style="text-align: justify;">
En Circle K, nos interesa que todo tu proceso sea transparente, es por ello, que en caso de que tengas alguna duda o comentario, puedes comunicarte al 55 52619800 o bien escribirnos al siguiente correo para apoyarte circlek.mexico@gmail.com.</p>
<p><label>
Vente pa´k
</p></label>
</div>


@component('mail::button', ['url' => route('verifyemail', $token )])
Verificar correo
@endcomponent

Gracias,<br>
Circle K
@endcomponent
