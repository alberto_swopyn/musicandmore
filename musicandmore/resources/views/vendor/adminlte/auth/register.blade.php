@extends('adminlte::layouts.auth')
@section('htmlheader_title')
Register
include
@endsection

@section('content')
<link rel="stylesheet" href="{{ asset('/css/style_auth.css')}}">
<script src="{{ asset('/js/ProgressDialogModal.js') }}" ></script>
<body class="hold-transition register-page text-center" style="background-color: white;">
  <div id="app">
    <div class="row">
      <div class="col-md-8 resize_col" style="padding-right: 0px">
        <img src="{{ asset('/img/M&M.png') }}" alt="" class="center-block img-responsive" id="imgcontainer">
      </div>  
      <div class="clearfix visible-xs"></div>
      <div class="col-md-4 col-xs-12 resize_col">
        <div class="register-box">
          <div class="register-logo">
            <a href="{{ url('/home') }}"><img src="{{asset('/img/Pain.png')}}" style="width: 200px; height: 200px; " name="logo_company"; alt="";></a>
          </div>

          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <div class="register-box-body">
            <p class="login-box-msg">Queremos que seas parte de nuestro equipo. <br> <label style="font-size: 1.1em">Regístrate para comenzar</label></p>
            <form action="{{ url('/register') }}" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="group">      
                <input type="text" required name="name" class="cssinput">
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input">{{ trans('adminlte_lang::message.username') }}</label>
              </div>
              @if (config('auth.providers.users.field','email') === 'username')
              <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.username') }}" name="username"/>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              @endif
              <div class="group">      
                <input type="text" required name="email" class="cssinput">
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input">{{ trans('adminlte_lang::message.email') }}</label>
              </div>
              <div class="group"> 
              <div class="password">     
                <input required id="password_register" name="password" type="password"  class="cssinput" autocomplete="off">
                <span class="glyphicon glyphicon-eye-open" id="glyphicon-eye-open1"></span>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input">{{ trans('adminlte_lang::message.password') }}</label>
              </div>
            </div>
              <div class="group"> 
              <div class="password">     
                <input  required id="password_confirmation" name="password_confirmation" type="password" class="cssinput">
                <span class="glyphicon glyphicon-eye-open" id="glyphicon-eye-open2"></span>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input" style="font-size: 1.3em">{{ trans('adminlte_lang::message.retypepassword') }}</label>
              </div>
              </div>
              <div class="group">
                <div class="col-xs-12 text-left">
                  <div class="col-xs-1">
                    <input type="checkbox" name="terms">  
                  </div>
                  <div class="col-xs-10">
                    <a href="#termsModal" data-toggle="modal">{{ trans('adminlte_lang::message.terms') }}</a>
                  </div>
                </div>
              </div>
              <div class="group">
                <div class="col-xs-12">
                <br>
                  <button type="submit" class="btn btn-primary btn-block" style="font-size: 2em">{{ trans('adminlte_lang::message.register') }}</button>
                </div><!-- /.col -->
              </div>
            </form>
            <div class="col-xs-12 text-center">
            <br>
              <table>
               <tbody>
                 <tr>
                   <td width="10%">
                     <img src="{{ asset('/img/icon_swopyn.png') }}" alt="" width="100%">
                   </td>
                   <td width="3%"></td>
                   <td width="79%">
                     <p>Si ya eres miembro entra <a style="font-weight: bold;" href="{{ url('/login') }}">aquí</a></p>
                   </td>
                 </tr>
               </tbody>
             </table>
             <table>
               <tbody>
                 <tr>
                   <td width="10%">
                     <img src="{{ asset('/img/exclamation.png') }}" alt="" width="100%">
                   </td>
                   <td width="3%"></td>
                   <td width="82%">
                     <p>Si ya tienes una cuenta asignada por Recursos Humanos ingresa <a style="font-weight: bold;" href="{{ route('useralternative') }}">aquí</a></p>
                   </td>
                 </tr>
               </tbody>
             </table>
           </div>
         </div><!-- /.form-box -->
       </div><!-- /.register-box -->
     </div>
   </div>
 </div>
 @include('adminlte::layouts.partials.scripts_auth')
 @include('adminlte::auth.terms')
<style type="text/css">
  .password  {
    position: relative;
  }
  .password .glyphicon {
    display:none;
    right: 15px;
    position: absolute;
    top: 12px;
    cursor:pointer;
  }

</style>
 <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
  increaseArea: '20%' // optional
});
  });
</script>

<script>
	ProgressDialogModal.InitModal($("#app"));
</script>

</body>

@endsection