@extends('adminlte::layouts.auth')
@section('htmlheader_title')
Log in
@endsection

@section('content')
<link rel="stylesheet" href="{{ asset('/css/style_auth.css')}}">
<script src="{{ asset('/js/ProgressDialogModal.js') }}" ></script>
<body class="hold-transition login-page text-center" style="background-color: white;">
  <div id="app">
    <div class="row">
      <div class="col-md-8 resize_col" style="padding-right: 0px">
        <img src="{{ asset('/img/M&M.png') }}" alt="" class="center-block img-responsive" id="imgcontainer">
      </div>  
      <div class="clearfix visible-xs"></div>
      <div class="col-md-4 col-xs-12 resize_col">
        <div class="login-box">
          <div class="login-logo">
            <a href="{{ url('/home') }}"><img src="{{asset('/img/Pain.png')}}" style="width: 200px; height: 200px; " name="logo_company"; alt="";></a>
          </div>

          <div class="login-box-body">
            <input type="hidden" id="user" value="">
            <strong><p class="login-box-msg">Ingresa con el Usuario y Contraseña asignado por Recursos Humanos</p></strong> <br>
            <div class="group">      
              <input type="text" required name="user_auth" class="cssinput">
              <span class="highlight"></span>
              <span class="bar"></span>
              <label class="title_input">Usuario</label>
            </div>      
            <div class="group">   
              <div class="password">    
                <input type="password" required id="password_alternative" name="user_password" class="cssinput" autocomplete="off">
                <span class="glyphicon glyphicon-eye-open"></span>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="title_input">{{ trans('adminlte_lang::message.password') }}</label>
              </div>
            </div>
            <div class="group">
              <div class="col-xs-12">
                <button type="button" class="btn-primary btn btn-block" id="btn_loginalternative" name="btn_loginalternative" style="font-size: 2em">{{ trans('adminlte_lang::message.buttonsign') }}</button>
                <br>
              </div><!-- /.col -->
            </div>
            <br>
            <br>
            <table>
             <tbody>
               <tr>
               <td width="10%">
                   <img src="{{ asset('/img/icon_swopyn.png') }}" alt="" width="100%">
                 </td>
                 <td width="3%"></td>
                 <td width="80%" class="text-left">
                   <p>Si ya eres miembro entra <strong><a href="{{ url('/login') }}">aquí</a></strong></p>
                 </td>
               </tr>
             </tbody>
           </table>
         </div><!-- /.form-box -->
       </div><!-- /.register-box -->
     </div>
   </div>
 </div>
 @include('adminlte::layouts.partials.scripts_auth')
 <style type="text/css">
  .password{
    position: relative;
  }
  .password .glyphicon,#password2 .glyphicon {
    display:none;
    right: 15px;
    position: absolute;
    top: 12px;
    cursor:pointer;
  }
</style>
<script>
	ProgressDialogModal.InitModal($("#app"));
</script>
</body>

@endsection
