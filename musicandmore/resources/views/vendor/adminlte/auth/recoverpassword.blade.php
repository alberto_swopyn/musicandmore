<div class="modal fade" id="RecoverPassword" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="login-logo text-center">
          <a href="#"><b>Recuperar </b>Contraseña</a>
        </div><!-- /.login-logo -->
        <form action="{{ url('/password/email') }}" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="group">      
              <input type="text" required name="{{ config('auth.providers.users.field','email') }}" class="cssinput" name="email" value="{{ old('email') }}">
              <span class="highlight"></span>
              <span class="bar"></span>
              <label style="margin-left: 0.8em;">{{ trans('adminlte_lang::message.email') }}</label>
            </div>
          <div class="group">
            <button type="button" class="btn btn-primary" style="background-color: black; border-color: black;">Enviar Contraseña</button>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- <div class="modal fade" id="RecoverPassword" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Terms and conditions</h3>
            </div>

            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, veniam numquam has te. No suas nonumes recusabo mea, est ut graeci definitiones. His ne melius vituperata scriptorem, cum paulo copiosae conclusionemque at. Facer inermis ius in, ad brute nominati referrentur vis. Dicat erant sit ex. Phaedrum imperdiet scribentur vix no, ad latine similique forensibus vel.</p>
                <p>Dolore populo vivendum vis eu, mei quaestio liberavisse ex. Electram necessitatibus ut vel, quo at probatus oportere, molestie conclusionemque pri cu. Brute augue tincidunt vim id, ne munere fierent rationibus mei. Ut pro volutpat praesent qualisque, an iisque scripta intellegebat eam.</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div> -->