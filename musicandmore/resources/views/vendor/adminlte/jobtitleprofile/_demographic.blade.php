<div class="form-horizontal panel-body" id="tab_demographics">
	<div class="principal-container">
		<div class="col-md-10 col-md-offset-1">
			<label>
				Edad (años)<span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_minage"></span><span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_maxage"></span><br>
				<div class="form-group">
					<label class="control-label col-md-2" style="color: gray">Mínimo</label>
					<div class="col-md-4">
						<input type="number" class="form-control" id="minAge" name="minAge" max="80" min="15" value="{{ $jobprofile->min_age or old('minAge') }}" style="border: none; outline: none; background: transparent; font-size: 1.3em; color: #000;">
					</div>
					<label class="control-label col-md-2" style="color: gray">Máximo</label>
					<div class="col-md-4">
						<input type="number" class="form-control" id="maxAge" name="maxAge" max="80" min="15" value="{{ $jobprofile->max_age or old('maxAge') }}" style="border: none; outline: none; background: transparent; font-size: 1.3em; color: #000;">
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1">
			<label>
				Género <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_generopuesto"></span>
				<select name="GeneroPuesto" id="GeneroPuesto" class="form-control">
					<option value="">Selecciona una opción</option>
					<option value="Indistinto"@if ('Indistinto' == $jobprofile->gender) selected='selected' @endif>Indistinto</option>
					<option value="Masculino"@if ('Masculino' == $jobprofile->gender) selected='selected' @endif>Masculino</option>
					<option value="Femenino"@if ('Femenino' == $jobprofile->gender) selected='selected' @endif>Femenino</option>
				</select>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1">
			<label>
				Estado Civil<span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_estcivilpuesto"></span>
				<select name="EstadoCivilPuesto" id="EstadoCivilPuesto" class="form-control">
					<option value="">Selecciona una opción</option>
					<option value="Indistinto"@if ('Indistinto' == $jobprofile->civil_status) selected='selected' @endif>Indistinto</option>
					<option value="Casado"@if ('Casado' == $jobprofile->civil_status) selected='selected' @endif>Casado</option>
					<option value="Soltero"@if ('Soltero' == $jobprofile->civil_status) selected='selected' @endif>Soltero</option>
					<option value="Unión Libre"@if ('Union Libre' == $jobprofile->civil_status) selected='selected' @endif>Unión Libre</option>
					<option value="Divorciado"@if ('Divorciado' == $jobprofile->civil_status) selected='selected' @endif>Divorciado</option>
					<option value="Viudo"@if ('Viudo' == $jobprofile->civil_status) selected='selected' @endif>Viudo</option>
				</select>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto;">
			<label>
				Educación <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_responsabilidadespuesto"></span>
				<div class="form-group table-responsive">
					<table class="table">
						<tbody>
							<tr>
								<td style="border: none">
									<div class="col-md-12">
										<label class="control-label col-md-2" style="color: #c1003d">Grado Academico</label>
										<div class="col-md-10">
											<select id="GradoAcademicoProfile" name="GradoAcademicoProfile"  class="js-example-basic-single form-control "  data-toggle="true" required  style="width: 100%; font-weight: bold;">
												<option value="">Seleccione uno ...</option>
												@foreach ($edu_level as $edu_level)
												<option value="{{ $edu_level-> id }}">{{ $edu_level -> name }}</option>
												@endforeach
											</select>
										</div>
									</div>
								</td>
								<td rowspan="2" style="vertical-align: middle; border: none;">
									<button type="button" class="btn btn-warning form-control text-center " style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a; " id="addscholarprofile">
										<span class="glyphicon glyphicon-plus-sign"></span>
									</button>
								</td>
							</tr>
							<tr>
								<td style="border: none">
									<div class="col-md-12">
										<label class="control-label col-md-2" style="color: gray;">Carrera</label>
										<div class="col-md-10">
											<select id="CarreraProfile" name="CarreraProfile"  class="form-control js-example-basic-single "  required data-toggle="tooltip" style="width: 100%;" disabled="true">
												<option value="">Seleccione uno ...</option>
											</select>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto;">
			<table class="table table-striped" style="margin-top: 1em;">
				<thead>
					<tr>
						<th class="text-center">Grado Academico</th>
						<th class="text-center" colspan="2">Carrera</th>
					</tr>
				</thead>
				<tbody id="tablaScholarProfile_aca">
					@foreach ($scho_profile as $sp)
					<tr class="valor{{$sp->id}}">
						<td width="45%" style="vertical-align: middle;" class="text-center">{{ $sp -> grade_academic }}</td>
						<td width="45%" style="vertical-align: middle;" class="text-center">{{ $sp -> carrer }}</td>
						<td width="10%" style="vertical-align: middle;" class="text-center">
							<button  class="btn btn-sm btn-circle deletescholarprofile" data-info="{{ $sp->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
								<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button>

						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-10 col-md-offset-1">
			<label>
				Experiencia <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_experienciapuesto"></span>
				<div class="form-group">
					<div class="col-md-6">
						<input type="text" class="form-control" placeholder="Área de experiencia" name="inputExperiencia" id="inputExperiencia" style="border:none; outline: none; background: transparent; font-size: 1.3em; color: #000;">
					</div>
					<div class="col-md-2">
						<input type="number" class="form-control" name="ExperienciaTiempo" placeholder="1" style="border:none; outline: none; background: transparent; font-size: 1.3em; color: #000;" min="0">
					</div>
					<div class="col-md-2">
						<select name="experience_time" id="experience_time" class="form-control" data-toggle="tooltip" title="Meses o Años" style="border:none; outline: none; background: transparent; font-size: 1.3em; color: #000;">
							<option value="meses">meses</option>
							<option value="años">años</option>
						</select>
					</div>
					<div class="col-md-2 text-center">
						<button type="button" class="btn btn-warning form-control" id="add_Experiencia" style="max-width: 3em; background-color: #1bb49a; border-color: #1bb49a;">
							<span class="glyphicon glyphicon-plus-sign"></span>
						</button>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-10 col-md-offset-1" style="height: auto; display: @if(count($exp)>0) auto; @else none; @endif" id="divExperiences">
			<table class="table table-striped" style="margin-top: 1em;">
				<tbody id="tablaExperienciaContenido">
					@foreach ($exp as $exp)
					<tr>
						<td class="text-center">{{ $exp->experience_name }}</td>
						<td class="text-center">{{ $exp->experience_time }}</td>
						<td class="text-center">
							<button  class="btn btn-sm btn-circle deletexp" data-info="{{ $exp->id }}" style="background: none; height: auto; width: 15px; padding: 0">
								<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size:1.5em;"></i>
							</button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="form-group text-right col-xs-12" style="margin-top: 2em">
			<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="guardar_demographic">Siguiente</button>
		</div>
	</div>
</div>