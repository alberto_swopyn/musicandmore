@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Aspirantes</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						  
						
						  			
							<div class="panel panel-default">
							  <div class="panel-heading">Documentos entregados</div>
							  <div class="panel-body">
							  	<div class="progress">
								  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" 
								  	aria-valuemax="{{ $documents->count() }}" 
									style=
									@if ($documents->count() > 0)
									  	"width: {{ ($uploads->count()*100) / $documents->count() }}%;"
									@else
										"width: 0%;"
									@endif
									>
								  </div>
								</div>
							   
							  </div>
							  <div class="row text-center">
							  		{{ $uploads->count() }}/{{ $documents->count() }}
							  </div>
							</div>
							<h4>Documentos solicitados </h4>
							<hr>
							<div class="container" style="width: 100%;">
								<div class="row">
							@foreach($documents as $document)
									@php ($i = 0)
									@foreach($uploads as $upload)
										@if ($upload->job_title_documents_id == $document->id)
										@php($i++)
										<div class="col-md-4" >
										<h4>{{ $document->document_name }} </h4>
											<div class="thumbnail">
												@php 
													echo '<img src="data:image/jpeg;base64,'.base64_encode( $upload->image ).'"  alt="" style="width:100%" class="img-responsive"/>';
												@endphp
												<div class="text-right" style="margin-top:4px">
													@php 
														echo '<a href="data:image/jpeg;base64,'.base64_encode( $upload->image ).'" class="btn btn-success" download="'.$document->document_name.'.jpg"><i class="glyphicon glyphicon-cloud-download"></i></a>';
													@endphp
													<button class="btn btn-danger pull-right" data-toggle="modal" data-target="#myModal{{$upload->id}}"> <i class="glyphicon glyphicon-trash"></i></button>
												
													@if (strlen($upload->comment) != 0)
														<div class="text-left">
															<h4 class="media-heading left">Comentarios:</h4>
															<p>{{$upload->comment}}</p>
														</div>
													@endif
													
												</div>
											</div>
										</div>

										<div id="myModal{{$upload->id}}" class="modal fade" role="dialog">
											<div class="modal-dialog">
												<!-- Modal content-->
												<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Eliminar Documento</h4>
												</div>
												<div class="modal-body">
													<p>¿Realmente desea eliminar el documento?</p>
												</div>
												<div class="modal-footer">
													<form action="{{ route('document_delete', [ 'document' => $upload->id ]) }}" method="POST"  >
														{{ csrf_field() }}
														{{ method_field('DELETE') }}
														<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
														<input type="submit" class="btn btn-danger" value="Borrar"  />
													</form>
												</div>
												</div>

											</div>
										</div>
										@endif
									
									@endforeach

									@if($i == 0)
										<div class="col-md-4 text-center">
											<h4>{{ $document->document_name }} </h4>
											<form action="{{ route('document_add') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="file">
												{{ csrf_field() }}
												<div class="dz-message" data-dz-message>
													<i class="glyphicon glyphicon-cloud-upload" style="font-size:30px"></i>
													<br/>
													<span>Arrastra y suelta tus archivos aquí.</span>
												</div>
												<input type="hidden" class="form-control" id="Perfil" name="Perfil" value="{{ $document->job_title_profiles_id }}">
												<input type="hidden" class="form-control" id="NombreImagen" name="NombreImagen" value="{{ $document->id }}">
												
											</form>
										</div>
									@endif
					
						@endforeach
						</div>
						</div>
						<center>	
						<hr>
						<form action="{{ route('tracking_document') }}"  method="POST" role="form" >
						{{ csrf_field() }}
						<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Finalizar</button>				
						</form>			
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>


		</div>
	</div>
 <script>
 	
 	Dropzone.options.myDropzone = {
  maxFilesize: 1, //mb- Image files not above this size
  uploadMultiple: false, // set to true to allow multiple image uploads
  parallelUploads: 2, //all images should upload same time
  maxFiles: 1, //number of images a user should upload at an instance
  acceptedFiles: ".png,.jpg,.jpeg", //allowed file types, .pdf or anyother would throw error
  addRemoveLinks: true, // add a remove link underneath each image to 
  autoProcessQueue: true // Prevents Dropzone from uploading dropped files immediately
};
 </script>
   
@endsection
