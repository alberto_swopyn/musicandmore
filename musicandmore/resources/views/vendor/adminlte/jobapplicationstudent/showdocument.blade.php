@extends('adminlte::layouts.app') @section('htmlheader_title') {{ trans('adminlte_lang::message.home') }} @endsection @section('main-content')

<div class="container-fluid spark-screen" id="JobAplication_ShowDocument">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Validaci&oacute;n de Documentaci&oacute;n</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">

					<div class="main container" style="width:100%">
						<div class="row">
							@foreach($documents as $document) 
							<div class="col-md-3">
									<a class="venobox {{$document->validated?'venobox-validated':''}}" id="a-venobox" data-gall="gallery01" data-id="{{ $document->id }}" data-title="{{ $document->job_title_documents_id }}" href="{{ route('show_image', ['document' => $document->id]) }} ">
										@php
											echo '<img src="data:image/jpeg;base64,'.base64_encode( $document->image ).'"  alt="" style="width:100%" class="img-responsive"/>';
										@endphp
									</a>
							
								<div class="row">
									<div class="col-ms-offset-3  col-ms-6 text-center
												col-xs-offset-0  col-xs-12 comentaryPopupContainer">

									 	<div class="btn-group panel-actions-documents">
											<button id="validateButton" type="button" class="btn btn-default validatedButton" data-name="{{$document->job_title_documents_id}}" data-id="{{$document->id}}" data-validated="{{($document->validated)}}" data-user="{{$document->user_id}}">{{($document->validated) ? "Invalidar" : "Validar"}}</button>
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<span class="caret"></span>
												<span class="sr-only">Toggle Dropdown</span>
											</button>
											<ul class="dropdown-menu">
												<li class="comentary-target"><a>Comentar</a></li>
											</ul>
										</div>

										<div class="panel panel-default comentary-pop" isshow="false" style="display:none">
											<div class="panel-heading">Comentario</div>
											<div class="panel-body">
												<div class="form-group">
												<textarea class="form-control" placeholder="Observacion del documento" rows="3" id="comment">{{$document->comment}}</textarea>
												</div>
												<button class="btn btn-success pull-right commentary-btn" style="background-color: black; border-color: black;" data-id="{{$document->id}}">Comentar</button>
											</div>
										</div>
									</div>
								</div>
								<hr>
							</div>
							

							<div class="hidden">{{ $id_doc = $document->user_id }}</div>

							@endforeach

						</div>
						<hr>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
<div>

@endsection