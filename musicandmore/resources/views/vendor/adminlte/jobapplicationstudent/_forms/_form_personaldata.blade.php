<div class="row tab-pane fade" id="general_personal_datas">
	<h3 class="titleJob text-center " style="text-align: center;">Datos Personales</h3>
	<h4 class="subtitleJob text-center">Llena tus datos generales</h4>
	<div class="principal-container">
		<div class="col-lg-6" style="vertical-align: middle;">
			<label>
				Apellidos
				<input type="text" class="form-control hola" id="Apellidos" name="Apellidos" placeholder="Apellidos"  value="{{ $info->last_name or old('Apellidos') }}" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-6" style="vertical-align: middle;">
			<label>
				Nombre(s) <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_primernombre"></span>
				<input type="text" class="form-control" id="PrimerNombre" name="PrimerNombre" placeholder="Nombre(s)" value="{{ $info->first_name or old('PrimerNombre') }}" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Genéro <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_genero"></span>
				<div class="form-group">
					<div class="col-xs-6">
						<span class="radio-inline ok">
							<input type="radio" name="Genero" id="Genero" value="Masculino" @if ('Masculino' == $info->gender) checked='checked' @endif @role(['docente']) disabled="true" @endrole>Masculino
						</span>
					</div>
					<div class="col-xs-6">
						<span class="radio-inline ok">
							<input type="radio" name="Genero" id="Genero" value="Femenino" @if ('Femenino' == $info->gender) checked='checked' @endif @role(['docente']) disabled="true" @endrole> Femenino
						</span>
					</div>
				</div>
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Fecha de nacimiento <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_fechanacimiento"></span>
				<input type="date" class="form-control" name="Nacimiento" id="Nacimiento" min="1950-01-01" max="2000-12-31"  value="{{ $info->birthday or old('Nacimiento') }}" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Lugar Nacimiento <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_lugarnacimiento"></span>
				<input type="text" class="form-control" id="LugarNacimiento" name="LugarNacimiento" placeholder="Ej. Ciudad de México"  value="{{ $info->birth_place or old('LugarNacimiento') }}" style="text-transform: capitalize;" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				Número Teléfonico <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_telefonojob"></span>
				<input type="text" class="form-control" id="Telefono_job" name="Telefono_job" placeholder="10 dígitos" maxlength="10" value="{{ $info->phone_number or old('Telefono_job') }}" @role(['docente']) disabled="true" @endrole/>
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				Número de Celular <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_celular"></span>
				<input type="text" class="form-control" id="Celular" name="Celular" placeholder="10 dígitos" maxlength="10"  value="{{ $info->cellphone or old('Celular') }}" @role(['docente']) disabled="true" @endrole/>
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				E-MAIL <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_curp"></span>
				<input type="email" id="Curp" name="Curp" class="form-control" placeholder="ejemplo@ejemplo.com" value="{{ $info->email or old('Curp') }}" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
	</div>
	<div class="col-xs-12 text-right">
		<br>
		<button type="button" class="btn btn-primary btn-lg next-step button_job" style="background-color: black; border-color: black" id="guarda_personaldatastudent">Siguiente</button>
	</div>
</div>
