<div class="row tab-pane fade" id="economic_datas">

	<h3 class="titleJob text-center">Información Económica</h3>
	<h4 class="subtitleJob text-center">Queremos cubrir tus necesidades económicas básicas. Cuéntanos, ¿Cuáles son?.</h4>

	<div class="principal-container">
		<br>
		
		<div class="col-lg-6">
			<label>¿Tiene algún ingreso económico extra? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_ingresoExtra"></span>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input onclick="MontoIngreso.disabled = false"  type="radio" name="IngresoExtra" id="IngresoExtra" value="Si" required @if (count($economic)>0 || 'Si' == $economic->other_income) checked='checked' @endif>Si
					</span>
				</div>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input onclick="MontoIngreso.disabled=true; MontoIngreso.value=false" type="radio" name="IngresoExtra" id="IngesoExtra" value="No" @if (count($economic)>0 || 'No' == $economic->other_income) checked='checked' @endif>No     
					</span>
				</div>
			</label>
		</div>

		<div class="col-lg-6">
			<label>Monto total del ingreso extra <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_MontoIngreso"></span><br>
				<span class="glyphicon glyphicon-usd">&nbsp;</span>&nbsp;<input type="number" class="form-control" id="MontoIngreso" placeholder="1500.00" name="MontoIngreso" min="0" max="150000" disabled="true" value="{{ $economic->other_ammount_income or old('MontoIngreso') }}" style="width: 15em; text-align: center;"><span style="margin-left: 1em;">mensuales</span>
			</label>
		</div>

		<div class="col-lg-6">
			<label>¿Su pareja tiene empleo? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_ParejaTrabaja"></span><br>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input onclick="LugarPareja.disabled = false" type="radio" name="ParejaTrabaja" id="ParejaTrabaja" value="Si" required @if (count($economic)>0 || 'Si' == $economic->couple_works) checked='checked' @endif>Si
					</span>
				</div>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input type="radio" name="ParejaTrabaja" onclick="LugarPareja.disabled=true; LugarPareja.value=''" id="ParejaTrabaja" value="No" @if (count($economic)>0 || 'No' == $economic->couple_works) checked='checked' @endif>No     
					</span>
				</div>
			</label>
		</div>	

		<div class="col-lg-6">
			<label>EspecifÍque el lugar donde trabaja su pareja <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_LugarPareja"></span>
				<input type="text" id="LugarPareja" name="LugarPareja" class="form-control" placeholder="Nombre de la empresa" disabled="true" value="{{ $economic->couple_work_place or old('LugarPareja') }}">
			</label>
		</div>

		<div class="col-lg-6">
			<label>¿Cuenta con casa propia? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_CasaPropia"></span><br> 
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input type="radio" name="CasaPropia" id="CasaPropia" value="Si" required @if (count($economic)>0 || 'Si' == $economic->own_home) checked='checked' @endif>Si
					</span>
				</div>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input type="radio" name="CasaPropia" id="CasaPropia" value="No" @if (count($economic)>0 || 'No' == $economic->own_home) checked='checked' @endif>No     
					</span>
				</div>
			</label>
		</div>

		<div class="col-lg-6">
			<label>¿Renta casa? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_RentaCasa"></span><br>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input type="radio" name="RentaCasa" id="RentaCasa" value="Si" required @if (count($economic)>0 || 'Si' == $economic->rent_home) checked='checked' @endif>Si
					</span>
				</div>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input type="radio" name="RentaCasa" id="RentaCasa" value="No" @if (count($economic)>0 || 'No' == $economic->rent_home) checked='checked' @endif>No     
					</span>
				</div>
			</label>
		</div>

		<div class="col-lg-6">
			<label>¿Cuenta con automóvil propio? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_CarroPropio"></span><br>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input type="radio" name="CarroPropio" id="CarroPropio" value="Si" required @if (count($economic)>0 || 'Si' == $economic->own_car) checked='checked' @endif>Si
					</span>
				</div>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input type="radio" name="CarroPropio" id="CarroPropio" value="No" @if (count($economic)>0 || 'No' == $economic->own_car) checked='checked' @endif>No     
					</span>
				</div>
			</label>
		</div>
		
		<div class="col-lg-6">
			<label>¿Tienes deudas? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_Deudas"></span><br>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input type="radio" onclick="MontoDeudas.disabled = false" name="Deudas" id="Deudas" value="Si" required @if (count($economic)>0 || 'Si' == $economic->debs) checked='checked' @endif>Si
					</span>
				</div>
				<div class="col-xs-6">
					<span class="radio-inline ok">
						<input type="radio" onclick="MontoDeudas.disabled=true; MontoDeudas.value=false" name="Deudas" id="Deudas" value="No" @if (count($economic)>0 || 'No' == $economic->debs) checked='checked' @endif>No     
					</span>
				</div>
			</label>
		</div>

		
		<div class="col-lg-6">
			<label>¿Cúanto abona mensualmente? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_MontoDeudas"></span><br>
				<span class="glyphicon glyphicon-usd">&nbsp;</span>&nbsp;<input type="number" class="form-control" disabled="true" placeholder="1500.00" id="MontoDeudas" name="MontoDeudas" min="0" max="150000" value="{{ $economic->pay_month or old('MontoDeudas') }}" style="width: 15em; text-align: center;"><span style="margin-left: 1em">mensuales</span>
			</label>
		</div>

		<div class="col-lg-6">
			<label>¿Cuánto gasta mensualmente? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_GastoMensual"></span><br>
				<span class="glyphicon glyphicon-usd">&nbsp;</span>&nbsp;<input type="number" class="form-control" id="GastoMensual" name="GastoMensual" min="0" placeholder="1500.00" max="150000" required value="{{ $economic->monthly_expenses or old('GastoMensual') }}" style="width: 15em; text-align: center;"><span style="margin-left: 1em">mensuales</span>
			</label>
		</div>

	</div>

	<div class="row" align="right">
		
	</div>
	<div class="col-xs-12 text-right">
			<br>
			<button type="button" class="btn btn-primary" style="background-color: black; border-color: black;" id="guarda_economicdata" style="margin: 1em">Siguiente</button>
		</div>
</div>