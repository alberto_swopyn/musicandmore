@section('personal_style')
@include('adminlte::tasks.PersonalSelect2')
@stop

<div class="row tab-pane fade" id="work_general_datas">

	<h3 class="titleJob text-center">OTRAS</h3>
	<h4 class="subtitleJob text-center">Ayúdanos con estos datos adicionales</h4>

	<div class="principal-container">

		<div class="col-lg-6">
			<label>¿Cómo se enteró de la academia? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_otros"></span>
				<select id="otros" name="otros" class="form-control" data-live-search="true" @role(['docente']) disabled="true" @endrole>
					<option value="" disabled selected> Seleccione...</option>
					<option value="Redes sociales" @if ('Redes sociales' === $work_g->knowledge_employment) selected='selected' @endif>Redes sociales</option>
					<option value="Volantes" @if ('Volantes' === $work_g->knowledge_employment) selected='selected' @endif>Volantes</option>
					<option value="Publicidad en internet" @if ('Publicidad en internet' === $work_g->knowledge_employment) selected='selected' @endif>Publicidad en internet</option>
					<option value="Recomendación" @if ('Recomendación' === $work_g->knowledge_employment) selected='selected' @endif>Recomendación</option>
					<option value="E-mail" @if ('E-mail' === $work_g->knowledge_employment) selected='selected' @endif>E-mail</option>
					<option value="Periodico o revista" @if ('Periodico o revista' === $work_g->knowledge_employment) selected='selected' @endif>Periodico o revista</option>
					<option value="Otro" @if ('Otro' === $work_g->knowledge_employment) selected='selected' @endif>Otro</option>
				</select>
			</label>
			<br>
		</div>

		<div class="col-lg-12">
			<label> ¿Cuál crees que sería tu mayor meta a lograr, musicalmente hablando? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_NombreReferencia"></span>
				<input type="text" id="NombreReferencia" name="NombreReferencia" class="form-control" maxlength="50" required value="{{ old('NombreReferencia') }}" placeholder="Escribe tu respuesta">
			</label>
		</div>

		<div class="col-lg-12">
			<label>¿Cómo te gustaría o pudiéramos ayudarte con esa meta? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_NombreReferencia"></span>
				<input type="text" id="NombreReferencia" name="NombreReferencia" class="form-control" maxlength="50" required value="{{ old('NombreReferencia') }}" placeholder="Escribe tu respuesta">
			</label>
		</div>

		<div class="col-lg-12">
			<label>¿Hay algún otro producto o servicio similar al nuestro que estés utilizando y del que quieras platicarnos? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_NombreReferencia"></span>
				<input type="text" id="NombreReferencia" name="NombreReferencia" class="form-control" maxlength="50" required value="{{ old('NombreReferencia') }}" placeholder="Escribe tu respuesta">
			</label>
		</div>

		<div class="col-lg-12">
			<label>¿En qué lugares, negocios, plazas o centros comerciales te gusta pasar el tiempo? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_NombreReferencia"></span>
				<input type="text" id="NombreReferencia" name="NombreReferencia" class="form-control" maxlength="50" required value="{{ old('NombreReferencia') }}" placeholder="Escribe tu respuesta">
			</label>
		</div>

		<div class="col-lg-12">
			<label>¿Qué redes sociales y aplicaciones utilizas más? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_NombreReferencia"></span>
				<input type="text" id="NombreReferencia" name="NombreReferencia" class="form-control" maxlength="50" required value="{{ old('NombreReferencia') }}" placeholder="Escribe tu respuesta">
			</label>
		</div>

		<div class="col-lg-12">
			<label>Aparte de la música ¿tienes otros hobbies? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_NombreReferencia"></span>
				<input type="text" id="NombreReferencia" name="NombreReferencia" class="form-control" maxlength="50" required value="{{ old('NombreReferencia') }}" placeholder="Escribe tu respuesta">
			</label>
		</div>

	<div class="row" align="center">
		<button type="button" class="btn-lg btn-success" id="guarda_workgeneralstudent" style="margin-top: 1em;"><span class="glyphicon glyphicon-ok"></span> Finalizar Solicitud</button>
	</div>

	<div class="col-xs-12 text-right">
		<br>
		<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black;" id="next-test">Siguiente</button>
	</div>
	
</div>
