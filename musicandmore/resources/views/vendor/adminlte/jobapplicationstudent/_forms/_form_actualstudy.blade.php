<div class="col-md-12" id="actualstudies" name="actualstudies" style="display: none;">
	<h3 class="titleJob text-center" style="margin-top: 2em;">ESTUDIOS ACTUALES</h3>
	<div class="principal-container">
		<div class="col-lg-6 col-xs-12">
			<label>
				Nombre de la Escuela <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_nombreEscuela"></span>
				<input type="text" id="Name_Escuela" class="form-control" name="Name_Escuela"  placeholder="Nombre de la institución" required value="{{ $actualschool->school_name or old('Name_Escuela') }}">
			</label>
		</div>
		<div class="col-lg-6 col-xs-12">
			<label>
				Dirección <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_direccionEscuela"></span>
				<input type="text" class="form-control" id="Direccion_Escuela" name="Direccion_Escuela" required value="{{ $actualschool->school_address or old('Direccion_Escuela') }}" placeholder="Direccion de la institución">
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				Grado Actual <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_GradoActual"></span>
				<input type="text" class="form-control" id="GradoActual" name="GradoActual" placeholder="Bachillerato, Licenciatura, Maestría, Doctorado" required value="{{ $actualschool->degree or old('GradoActual') }}" >
			</label>
		</div>
		<div class="col-lg-4 col-xs-6">
			<label>
				Período Inicial <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_PeriodoInicialEscuela"></span>
				<input type="date" id="PeriodoInicialEscuela" name="PeriodoInicialEscuela" class="form-control" required value="{{ $actualschool->initial_period or old('PeriodoInicialEscuela') }}" step="1" min="1950-01-01" max="2017-31-31" value="2017-01-01">
			</label>
		</div>
		<div class="col-lg-4 col-xs-6">
			<label>
				Período Final <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_PeriodoFinalEscuela"></span>
				<input type="date" id="PeriodoFinalEscuela" name="PeriodoFinalEscuela" class="form-control" required value="{{ $actualschool->final_period or old('PeriodoFinalEscuela') }}" step="1" min="1950-01-01" max="2017-31-31">
			</label>
		</div>
	</div>
	<div class="col-xs-12 text-center">
		<br>
		<button type="button" class="btn btn-danger" style="background-color: black; border-color: black;" id="guarda_actualstudies" style="width:8em; height:3em">Guardar</button>
	</div>
</div>