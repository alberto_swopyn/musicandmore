<div class="row tab-pane fade" id="personal_addresses">
	<h3 class="titleJob text-center">DOMICILIO</h3>
	<h4 class="subtitleJob text-center">Compártenos la dirección en donde actualmente resides</h4>
	<div class="principal-container">
		<div class="col-lg-6">
			<label>
				Calle <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_calle"></span></b>
				<input type="text" id="Calle" name="Calle" class="form-control" placeholder="Ej. Av. de la Convención de 1914" maxlength="30" value="{{ $dir->street or old('Calle') }}" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-3 col-xs-6">
			<label>
				Número Exterior <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_numeroExterior"></span>
				<input type="number" id="NumeroExterior" name="NumeroExterior" class="form-control" placeholder="Número Exterior" min="1" maxlength="10" value="{{ $dir->ext_number or old('NumeroExterior') }}" max="9999" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-3 col-xs-6">
			<label>
				Número Interior <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_numeroInterior"></span>
				<input type="text" id="NumeroInterior" name="NumeroInterior" class="form-control" placeholder="Número Interior" min="1" value="{{ $dir->int_number or old('NumeroInterior') }}" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-6">
			<label>
				Fraccionamiento <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_fraccionamiento"></span>
				<input type="text" id="Fraccionamiento" name="Fraccionamiento" class="form-control" placeholder="Ej. Zona Centro" maxlength="50" value="{{ $dir->fraccionamiento or old('Fraccionamiento') }}" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-6">
			<label>
				Código Postal <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_codigoPostal"></span>
				<input type="text" id="CodigoPostal" name="CodigoPostal" class="form-control" placeholder="Ej. 20000" maxlength="5" value="{{ $dir->zip_code or old('CodigoPostal') }}" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-6">
			<label>
				Ciudad <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_ciudad"></span>
				<input type="text" id="Ciudad" name="Ciudad" class="form-control" placeholder="Aguascalientes" value="{{ $dir->city or old('Ciudad') }}" @role(['docente']) disabled="true" @endrole>
			</label>
		</div>
		<div class="col-lg-6">
			<label>
				Estado <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_estado"></span>
				<select id="Estado_personal" name="Estado_personal" class="form-control" data-live-search="true" required="true" value="{{ $dir->state or old('Estado_personal') }}" @role(['docente']) disabled="true" @endrole>
					<option value="">Seleccione una opción</option>
					<option value="Aguascalientes" @if ('Aguascalientes' == $dir->state) selected ='selected' @endif>Aguascalientes</option>
					<option value="Baja California"@if ('Baja California' === $dir->state) selected ='selected' @endif>Baja California</option>
					<option value="Baja California Sur"@if ('Baja California Sur' === $dir->state) selected = 'selected' @endif>Baja California Sur</option>
					<option value="Campeche"@if ('Campeche' == $dir->state) selected = 'selected' @endif>Campeche</option>
					<option value="Coahuila de Zaragoza"@if ('Coahuila de Zaragoza' === $dir->state) selected = 'selected' @endif>Coahuila de Zaragoza</option>
					<option value="Colima"@if ('Colima' == $dir->state) selected = 'selected' @endif>Colima</option>
					<option value="Chiapas"@if ('Chiapas' == $dir->state) selected = 'selected' @endif>Chiapas</option>
					<option value="Chihuahua"@if ('Chihuahua' == $dir->state) selected = 'selected' @endif>Chihuahua</option>
					<option value="Distrito Federal"@if ('Distrito Federal' === $dir->state) selected = 'selected' @endif>Distrito Federal</option>
					<option value="Durango"@if ('Durango' == $dir->state) selected = 'selected' @endif>Durango</option>
					<option value="Guanajuato"@if ('Guanajuato' == $dir->state) selected = 'selected' @endif>Guanajuato</option>
					<option value="Guerrero"@if ('Guerrero' == $dir->state) selected = 'selected' @endif>Guerrero</option>
					<option value="Hidalgo"@if ('Hidalgo' == $dir->state) selected = 'selected' @endif>Hidalgo</option>
					<option value="Jalisco"@if ('Jalisco' == $dir->state) selected = 'selected' @endif>Jalisco</option>
					<option value="México"@if ('México' == $dir->state) selected = 'selected' @endif>México</option>
					<option value="Michoacán de Ocampo"@if ('Michoacán de Ocampo' === $dir->state) selected = 'selected' @endif>Michoacán de Ocampo</option>
					<option value="Morelos"@if ('Morelos' == $dir->state) selected = 'selected' @endif>Morelos</option>
					<option value="Nayarit"@if ('Nayarit' == $dir->state) selected = 'selected' @endif>Nayarit</option>
					<option value="Nuevo León"@if ('Nuevo León' === $dir->state) selected = 'selected' @endif>Nuevo León</option>
					<option value="Oaxaca"@if ('Oaxaca' == $dir->state) selected = 'selected' @endif>Oaxaca</option>
					<option value="Puebla"@if ('Puebla' == $dir->state) selected = 'selected' @endif>Puebla</option>
					<option value="Querétaro"@if ('Querétaro' == $dir->state) selected = 'selected' @endif>Querétaro</option>
					<option value="Quintana Roo"@if ('Quintana Roo' === $dir->state) selected = 'selected' @endif>Quintana Roo</option>
					<option value="San Luis Potosí"@if ('San Luis Potosí' === $dir->state) selected = 'selected' @endif>San Luis Potosí</option>
					<option value="Sinaloa"@if ('Sinaloa' == $dir->state) selected = 'selected' @endif>Sinaloa</option>
					<option value="Sonora"@if ('Sonora' == $dir->state) selected = 'selected' @endif>Sonora</option>
					<option value="Tabasco"@if ('Tabasco' == $dir->state) selected = 'selected' @endif>Tabasco</option>
					<option value="Tamaulipas"@if ('Tamaulipas' == $dir->state) selected = 'selected' @endif>Tamaulipas</option>
					<option value="Tlaxcala"@if ('Tlaxcala' == $dir->state) selected = 'selected' @endif>Tlaxcala</option>
					<option value="Veracruz de Ignacio de la Llave"@if ('Veracruz de Ignacio de la Llave' === $dir->state) selected = 'selected' @endif>Veracruz de Ignacio de la Llave</option>
					<option value="Yucatán"@if ('Yucatán' == $dir->state) selected = 'selected' @endif>Yucatán</option>
					<option value="Zacatecas"@if ('Zacatecas' == $dir->state) selected = 'selected' @endif>Zacatecas</option>
				</select>
			</label>
		</div>
	</div>
	<div class="col-xs-12 text-right">
		<br>
		<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black;" id="guarda_personaladdress">Siguiente</button>
	</div>
</div>
