<div class="row tab-pane fade " id="general_knowledge">

	<h3 class="titleJob text-center">CONOCIMIENTOS</h3>
	<h4 class="subtitleJob text-center">¿Cuáles son tus habilidades?</h4>
	<h5 class="subtitleJob text-center">Especifíca los trabajos que mejor desempeñas</h5>

	<div class="principal-container">
		<div class="col-lg-offset-2 col-lg-2" style="border: 0">
			<label>Lenguaje <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_lenguaje"></span>
				<select id="languageknow" name="languageknow" class="form-control" data-live-search="true">
					<option value=""> Seleccione...</option>
					@foreach ($lanknow as $l)
					<option value="{{ $l->language }}">{{ $l->language }}</option>
					@endforeach
				</select>
			</label>
		</div>

		<div class="col-lg-6" style="border: 0; ">
			<label>
				<div class="col-lg-3">
					<label><span>Leer&nbsp;</span><span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_nivel"></span></label>
					<select id="languageread" name="languageread" data-live-search="true" style="color: black;" class="form-control">
						<option value=""> Seleccione...</option>
						@foreach ($lanlevel as $l)
						<option value="{{ $l->value }}">{{ $l->name }}</option>
						@endforeach
					</select>	
				</div>
				<div class="col-lg-3">
					<label><span>Escibir&nbsp;</span><span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_languagewrite"></span></label>
					<select id="languagewrite" name="languagewrite" data-live-search="true" style="color: black;" class="form-control">
						<option value=""> Seleccione...</option>
						@foreach ($lanlevel as $l)
						<option value="{{ $l->value }}">{{ $l->name }}</option>
						@endforeach
					</select>	
				</div>
				<div class="col-lg-3">
					<label><span>Hablar&nbsp;</span><span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_languagespeak"></span></label>
					<select id="languagespeak" name="languagespeak " data-live-search="true" style="color: black;" class="form-control">
						<option value=""> Seleccione...</option>
						@foreach ($lanlevel as $l)
						<option value="{{ $l->value }}">{{ $l->name }}</option>
						@endforeach
					</select>	
				</div>
				<div class="col-lg-3">
					<label><span>Escuchar&nbsp;</span><span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_languagelisten"></span></label>	
					<select id="languagelisten" name="languagelisten" data-live-search="true" style="color: black;" class="form-control">
						<option value=""> Seleccione...</option>
						@foreach ($lanlevel as $l)
						<option value="{{ $l->value }}">{{ $l->name }}</option>
						@endforeach
					</select>	
				</div>
			</label>
		</div>
		<button type="button" class="btn form-control " style="max-width: 5em; border-radius: 50%; height: 5em;" id="add_language" >
			<span class="glyphicon glyphicon-plus-sign" style="font-size: 2em; color: #1bb49a;"></span>
		</button>
		<table class="table text-left table-striped" style="margin-bottom: 0px; width: 50%; margin: 0 auto; margin-top: 3em;">
			<thead>
				<tr>
					<th class="text-center">Lenguaje</th>
					<th class="text-center">Nivel</th>
				</tr>
			</thead>
			@foreach ($lan as $lan)
			<tbody id="tablaLanguage">
				<tr>
					<td class="text-center">{{ $lan->language }}</td>
					<td class="text-center">{{ $lan->level }}&nbsp;%</td>
					<td class="text-center">
						<button class="btn btn-sm btn-circle deleteLanguage" data-info="{{ $lan->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
							<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
						</button>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<div class="col-lg-4" style="border: 0">
			<label>Oficina <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_oficina"></span>
				<input type="text" class="form-control" id="Officeknow" name="Officeknow" placeholder="Contraloría" maxlength="50">
			</label>
		</div>

		<div class="col-lg-2" style="border: 0">
			<button type="button" class="btn form-control" style="max-width: 5em; border-radius: 50%; height: 5em; background: none" id="add_office" >
				<span class="glyphicon glyphicon-plus-sign" style="font-size: 2em; color: #1bb49a;"></span>
			</button>
		</div>

		<div class="col-lg-4" style="border: 0">
			<label>Maquinaria <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_maquinas"></span>
				<input type="text" id="Machinesknow" name="Machinesknow" class="form-control" placeholder="Montacargas, Maquina de soldar, etc">
			</label>
		</div>

		<div class="col-lg-2" style="border: 0">
			<button type="button" class="btn form-control" style="max-width: 5em; border-radius: 50%; height: 5em; background: none" id="add_Machines">
				<span class="glyphicon glyphicon-plus-sign" style="font-size: 2em; color: #1bb49a;"></span>
			</button>
		</div>

		<div class="row">
			<div class="col-lg-6 table-responsive" style="height: auto; border: 0">
				<table class="table text-center table-striped" style="margin-bottom: 0px; margin-top: 2em;">
					<thead>
						<tr>
							<th class="text-center">Conocimiento oficina</th>
						</tr>
					</thead>
					<tbody id="tablaOficina">
						@foreach ($off as $off)
						<tr>
							<td>{{ $off->office }}</td>
							<td>
								<button class="btn btn-sm btn-circle deleteOficina" data-info="{{ $off->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
									<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
								</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			<div class="col-lg-6 table-responsive" style="height: auto; border: 0">
				<table class="table text-center table-striped" style="margin-bottom: 0px; margin-top: 2em;">
					<thead>
						<tr>
							<th class="text-center">Conocimiento máquinas</th>
						</tr>
					</thead>
					<tbody id="tablaMaquinaria">
						@foreach ($mac as $mac)
						<tr>
							<td class="text-center">{{ $mac->machines }}</td>
							<td class="text-center">
								<button class="btn btn-sm btn-circle deleteMaquinaria" data-info="{{ $mac->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
									<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
								</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-lg-4" style="border: 0">
			<label>Software <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_software"></span>
				<input type="text" id="Softknow" name="Softknow" class="form-control" placeholder="Microsoft Office, AutoCAD, etc">
			</label>
		</div>

		<div class="col-lg-2" style="border: 0">
			<button type="button" class="btn form-control" style="max-width: 5em; border-radius: 50%; height: 5em; background: none" id="add_soft">
				<span class="glyphicon glyphicon-plus-sign" style="font-size: 2em; color: #1bb49a;"></span>
			</button>
		</div>

		<div class="col-lg-4" style="border: 0">
			<label>Otros trabajos <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_otrostrabajo"></span>
				<input type="text" class="form-control" id="Othersknow" name="Othersknow" placeholder="Puedes especificar varios trabajos, sepáralos por comas" required>
			</label>
		</div>

		<div class="col-lg-2" style="border: 0">
			<button type="button" class="btn form-control" style="max-width: 5em; border-radius: 50%; height: 5em; background: none" id="add_others">
				<span class="glyphicon glyphicon-plus-sign" style="font-size: 2em; color: #1bb49a;"></span>
			</button>
		</div>

		<div class="row">
			<div class="col-lg-6 table-responsive" style="height: auto; border: 0">
				<table class="table text-center table-striped" style="margin-bottom: 0px; margin-top: 2em;">
					<thead>
						<tr>
							<th class="text-center">Conocimiento software</th>
						</tr>
					</thead>
					<tbody id="tablaSoftware">
						@foreach ($soft as $soft)
						<tr>
							<td class="text-center">{{ $soft->software }}</td>
							<td class="text-center">
								<button class="btn btn-sm btn-circle deleteSoftware" data-info="{{ $soft->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
									<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
								</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			<div class="col-lg-6 table-responsive" style="height: auto; border: 0">
				<table class="table text-center table-striped" style="margin-bottom: 0px; margin-top: 2em;">
					<table class="table text-center table-striped" style="margin-bottom: 0px;">
						<thead>
							<tr>
								<th class="text-center">Conocimiento otros</th>
							</tr>
						</thead>
						<tbody id="tablaOtros">
							@foreach ($others as $others)
							<tr>
								<td class="text-center">{{ $others->others }}</td>
								<td class="text-center">
									<button class="btn btn-sm btn-circle deleteOtros" data-info="{{ $others->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
										<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
									</button>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>
		<br>
		<div class="row" align="right" style="margin-right: .5em">
			<button type="button" class="btn btn-primary" style="background-color: black; border-color: black;" id="guarda_generalknowledge">Siguiente</button>
		</div>
	</div>