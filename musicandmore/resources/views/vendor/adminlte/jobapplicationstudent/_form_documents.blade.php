<div class="container-fluid spark-screen">
	<div class="row">
		<br>
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box-header text-center with-border">
				<h2>Documentos </h2>
			</div>
			<div class="box-body">
				<div class="col-md-12 text-center">
					<h4>Estas a punto de finalizar tu proceso</h4>
					<p class="text-justify">
						Es necesario que subas cada uno de los documentos requeridos en el siguiente enlace, para ello te pedimos captures tus documentos digitalmente y de forma legible. Te recordamos que únicamente puedes adjuntar imagenes en formato : <strong>.JPG, .JPEG, .PNG o .BITMAP</strong>
					</p>
					<a href="{{ route('documents_personal') }}"><button class="btn btn-info" style="background-color: black; border-color: black;">Subir Documentos</button></a>
				</div>
			</div>
		</div>
	</div>
	<!--.box-->
</div>