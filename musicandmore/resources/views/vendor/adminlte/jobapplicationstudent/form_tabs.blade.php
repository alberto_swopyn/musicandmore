<div class="col-xs-12">
  <ul class="nav nav-tabs nav-justified" id="steps">

    <li class="active Pasos_Tracking">
      <a data-toggle="tab" href="#section-jobapplication" class="enlace_Pasos">
        <h3 class="stepTitle">Solicitud de alumno</h3>
        <h5 class="borra">Paso 1</h5>
      </a>
    </li>
  </ul>

  <div class="tab-content">
    <div id="section-jobapplication" class="tab-pane fade in active">
      @include('adminlte::jobapplicationstudent/form_sections')
    </div>
    <div id="section-pruebas" class="tab-pane fade" style="margin-top: 3em">
     @include('adminlte::psychometric/intro')
   </div>
   <div id="section-interview" class="tab-pane fade">
    @include('adminlte::scheduledinterview.user_view')
  </div>
  <div id="section-documents" class="tab-pane fade">
    @include('adminlte::jobapplicationstudent._form_documents')
  </div>
  <div id="section-contract" class="tab-pane fade">
    @include('adminlte::contracttype/view_contract')
  </div>
</div>
</div>