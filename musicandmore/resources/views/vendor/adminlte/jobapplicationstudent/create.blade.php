@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('css-personal')

<style>
.principal-container
{
	width: 100%;
}

.principal-container > div > label
{
	text-align: left;
	display: block;
	padding: 1.1em 1.1em .6em;
	font-size: 1em;
	color: #f5216e;
	cursor: pointer;
}
.principal-container > [class^='col-'] { 
	border: 1px solid #E4E4E4; 
	height: 6em;
}  
.ok, .principal-container > div > label > input, .principal-container > div > label > select, .principal-container > div > label > center > input
{
	display: inline-block;
	position: relative;
	width: 100%;
	margin: 5px -5px 0;
	padding: 7px 5px 3px;
	border: none;
	outline: none;
	background: transparent;
	font-size: 1.3em;
	color: #000;
}
.principal-container > div > label > center > input{
	width: 3.25em;
}
@media screen and (max-width: 480px) {
	.principal-container > div > label{
		font-size: .8em;
	}
	.ok, .principal-container > div > label > input, .principal-container > div > label > select, .principal-container > div > label > center > input{
		font-size: 1.1em;
	}	
}
</style>
@stop
@section('main-content')

<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">

				<div class="box-body" style="width: 100%;">
					@if (isset($tabs_pass))
					@if ($tabs_pass->doc_verified == $tabs_pass->document)
					<input type="hidden" id="pass_tabs" value="4">
					@elseif (isset($tabs_pass->interview))
					<input type="hidden" id="pass_tabs" value="3">
					@elseif (isset($tabs_pass->psychometric))
					<input type="hidden" id="pass_tabs" value="2">
					@elseif (isset($tabs_pass->job_title_profile_id))
					<input type="hidden" id="pass_tabs" value="1">
					@else
					<input type="hidden" id="pass_tabs" value="0">
					@endif
					<input type="hidden" id="job_tabs" value="{{ $tabs_pass->job_application }}">
					@endif
					@include('adminlte::jobapplicationstudent.form_tabs')
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->

		</div>
	</div>
</div>
<script>
	Dropzone.options.myDropzone = {
  maxFilesize: 1, //mb- Image files not above this size
  uploadMultiple: false, // set to true to allow multiple image uploads
  parallelUploads: 2, //all images should upload same time
  maxFiles: 1, //number of images a user should upload at an instance
  acceptedFiles: ".png,.jpg,.jpeg", //allowed file types, .pdf or anyother would throw error
  addRemoveLinks: true, // add a remove link underneath each image to 
  autoProcessQueue: true // Prevents Dropzone from uploading dropped files immediately
};
</script>
@endsection
@if(isset($interview_see))
@section('personal-scripts')
<script>
	var ubication = $('#ubi_data').val();
	var ubi = ubication.split(",");
	var lat = ubi[0];
	var lng = ubi[1];
	var map;
	var marker;
	function initMap() {
		var myOptions = { 
			zoom: 13, 
			center: new google.maps.LatLng(lat,lng),
		} 
		map = new google.maps.Map(document.getElementById('mapinterview'), myOptions);
		marker = new google.maps.Marker({
			map: map,
			animation: google.maps.Animation.DROP,
			position: new google.maps.LatLng(lat, lng),

		});
	}
</script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDldpjKxaPoyGu2NLMSH1tfvqIfsZg0eqA&callback=initMap" async defer type="text/javascript"></script>
@stop
@endif
