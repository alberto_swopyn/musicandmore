@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Detalle de Asistencias</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
							</div>
						<h3 class="titleCenter" style="text-align: center;">Detalle de Asistencias</h3>
						<h4 class="titleCenter" style="text-align: center;">Profesor: {{$teacher->name}}</h4>
						<div class="table-responsive">
						<table class="table table-hover table table-striped sortable" id="tableTasks">
						<thead style="background: black; color: white;">
								<th class="text-center">Fecha</th>
								<th class="text-center">Grupo</th>
								<th class="text-center">Nº de Asistencias</th>
								<th class="text-center">Nº de Faltas</th>
								<th class="text-center">Nº de Justificaciones</th>
						</thead>
						@foreach($a as $ass)		
						<tbody>
							<tr>
								<td class="text-center"><?php echo date("d-m-Y",strtotime($ass->date)); ?>
								<td class="text-center">
									<strong>{{$ass->curso}}</strong>
									</td>
								<td class="text-center"><button class="btn btn-info" disabled>{{$students->where('assistance','=',1)->where('id_group',$ass->group)->where('date','=',$ass->date)->count()}}</button></td>
								<td class="text-center"><button class="btn btn-danger" disabled>{{$students->where('assistance','=',0)->where('id_group',$ass->group)->where('date','=',$ass->date)->count()}}</button></td>
								<td class="text-center"><button class="btn" style="background-color: black; color: white" disabled>{{$students->where('assistance','=',2)->where('id_group',$ass->group)->where('date','=',$ass->date)->count()}}</button></td>
							</tr>
							</tbody>
						@endforeach
					</table>
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>