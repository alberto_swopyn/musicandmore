@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Caja de Cobros</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
					<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'search_box','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						<div class="col-md-4 col-md-offset-8" style="float: right; width: 500px;" >
						<a href="{{ Route('box_unic') }}" class="btn btn-success" style="background-color: black; border-color: black;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span>Cobro de Pagos Únicos</a>
						<a href="{{ Route('box_rec') }}" class="btn btn-success" style="background-color: black; border-color: black;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span>Cobro de Pagos Recurrentes</a>
					</div>
					</div>

					<h3 class="titleCenter" style="text-align: center;">Pagos Únicos Realizados</h3>
					<div class="table-responsive">
						<table class="table table-hover table table-striped sortable" id="tableEmployees">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Alumno</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">-%</th>
								<th class="text-center">Total</th>
								<th class="text-center">Fecha limite</th>
								<th class="text-center">Fecha de Pago</th>
								<th class="text-center">Método de Pago</th>
								<th class="text-center">Comentario</th>
								<th class="text-center">Razón de Edición</th>
								<th class="text-center">Pagado</th>
								<th colspan="3" class="text-center">Opciones</th>
							</tr>
						</thead>
						@foreach($asignacion as $a)
						@if($a->type == 1)
						<tbody>
							
								<tr>
									<td class="text-center"><a href="{{route('ticket_pays',$a->assi)}}" target=”_blank”>
									<strong>{{$a->title}}</strong></a></td>
									<td class="text-center">{{$a->name}}</td>
									<td class="text-center">${{$a->quantity}}</td>
									<td class="text-center">{{$a->desc_cant}}%</td>
									<td class="text-center">${{$a->total}}</td>
									@if($a->limit_date == null)
									<td class="text-center">No requerida</td>
									@else
									<td class="text-center"><?php echo date("d-m-Y",strtotime($a->limit_date)); ?></td>
									@endif
									<td class="text-center"><?php echo date("d-m-Y",strtotime($a->date_pay)); ?></td>
									<td class="text-center">{{$a->metodo}}</td>
									<td class="text-center">{{$a->comment}}</td>
									<td class="text-center">{{$a->reason}}</td>
									@if($a->is_completed == 1)
									<td class="text-center">
									<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="0%" disabled="true">
									<i class="fa fa-check-circle" style="color: white;"></i></td>
									</button></td>
									<td>
										<a href="{{ Route('box_unic_edit', $a->assi) }}" class="btn">
											<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
										</a>
									</td>
									<!--<td>
										<form action="{{Route('box_unic_delete',$a->assi)}}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn" style="background-color: transparent;">
												<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
											</button>
										</form>
									</td>-->
								</tr>
								@endif
							</tbody>
							@endif
							@endforeach
						</table>
						<center>{{$asignacion->links()}}<center>
					</div>

<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->

						<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->


						<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'search_box2','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search2" id="search2">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						<h3 class="titleCenter" style="text-align: center;">Pagos Recurrentes Realizados</h3>
						<div class="table-responsive">
						<table class="table table-hover table-striped sortable" id="tableEmployees">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Alumno</th>
								<th class="text-center">Asignatura</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">-%</th>
								<th class="text-center">+%</th>
								<th class="text-center">Beca</th>
								<th class="text-center">Total</th>
								<th class="text-center">Fecha de Pago</th>
								<th class="text-center">Método de Pago</th>
								<th class="text-center">Comentario</th>
								<th class="text-center">Razón de Edición</th>
								<th class="text-center">Pagado</th>
								<th colspan="3" class="text-center">Opciones</th>
							</tr>
						</thead>
							@foreach($ass_rec as $a)
							@if($a->type == 2)							
							<tbody>
								<tr>
									
									<td class="text-center"><a href="{{route('ticket_pays_r',$a->assi)}}" target=”_blank”><strong>{{$a->title}}</strong></a></td>
									<td class="text-center">{{$a->name}}</td>
									<td class="text-center">{{$a->subject}}</td>
									<td class="text-center">${{$a->quantity}}</td>
									<td class="text-center">{{$a->desc_cant}}%</td>
									<td class="text-center">{{$a->aument}}%</td>
									@if($a->beca == null)
									<td class="text-center">0%</td>
									@else
									<td class="text-center">{{$a->beca}}%</td>
									@endif
									<td class="text-center">${{$a->total}}</td>
									<td class="text-center"><?php echo date("d-m-Y",strtotime($a->date_pay)); ?></td>
									<td class="text-center">{{$a->metodo}}</td>
									<td class="text-center">{{$a->comment}}</td>
									<td class="text-center">{{$a->reason}}</td>
									@if($a->is_completed == 1)
									<td class="text-center">
									<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="0%" disabled="true"><i class="fa fa-check-circle" style="color: white;"></i></td>
									</button></td>
									<td>
										<a href="{{ Route('box_rec_edit', $a->assi) }}" class="btn">
											<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
										</a>
									</td>
									<!--<td>
										<form action="{{ Route('box_rec_delete', $a->assi) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn" style="background-color: transparent;">
												<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
											</button>
										</form>
									</td>-->
								</tr>
								@endif
							</tbody>
							@endif
							@endforeach
						</table>
						<center>{{$ass_rec->links()}}</center>
					</div>
					@if($cut == false)
					<div align="center">
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter">
  						Corte de Caja
						</button>
					@endif
						</div>
						
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
				

			</div>
		</div>

<!-- Ventana para el arqueo-->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Arqueo para corte de caja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ Route('cut_box') }}" method="GET" id="form">
      	{{ csrf_field() }}
		<div class="modal-body">
		<h4 class="modal-title">Introducir las cantidades correspondientes</h4>
		<br>
		<br>
		<div class="col-md-4">
			<label for="mil"><img src="{{ asset( 'img/1000.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
			<input type="number" name="mil" id="mil" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="quinientos"><img src="{{ asset( 'img/500.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
			<input type="number" name="quinientos" id="quinientos" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="doscientos"><img src="{{ asset( 'img/200.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
			<input type="number" name="doscientos" id="doscientos" style="width:80px;height:30px" value="">
		</div>
		<br>
		<br>
		<div class="col-md-4">
		<label for="cien"><img src="{{ asset( 'img/100.png' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
			<input type="number" name="cien" id="cien" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="cincuenta"><img src="{{ asset( 'img/50.jpg' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
			<input type="number" name="cincuenta" id="cincuenta" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="veinte"><img src="{{ asset( 'img/20.jpg' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
			<input type="number" name="veinte" id="veinte" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="diez"><img src="{{ asset( 'img/10.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="diez" id="diez" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="cinco"><img src="{{ asset( 'img/5.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="cinco" id="cinco" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="dos"><img src="{{ asset( 'img/2.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="dos" id="dos" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="uno"><img src="{{ asset( 'img/1.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="uno" id="uno" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="cincuenta_centavos"><img src="{{ asset( 'img/50-Centavos.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="cincuenta_centavos" id="cincuenta_centavos" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="veinte_centavos"><img src="{{ asset( 'img/20-c.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="veinte_centavos" id="veinte_centavos" style="width:80px;height:30px" value="">
		</div>
		<div class="col-md-4">
			<label for="diez_centavos"><img src="{{asset('img/10centavos.jpg')}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="diez_centavos" id="diez_centavos"  style="width:80px;height:30px" value="">
		</div>
		</div>
		<br>
      <div class="modal-footer" style="margin-top: 500px">
        <button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
  </form>
    </div>
  </div>
</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>