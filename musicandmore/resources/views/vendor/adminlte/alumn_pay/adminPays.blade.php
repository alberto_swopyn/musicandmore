@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Catálogo de Pagos</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
					<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'search_admin','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						<div class="col-md-4 col-md-offset-8" style="float: right; width: 500px;" >
						<a href="{{ route('createpays') }}" class="btn btn-success" style="background-color: black; border-color: black;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span>Pagos Únicos</a>
						<a href="{{ route('createpays_r') }}" class="btn btn-success" style="background-color: black; border-color: black;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span>Pagos Recurrentes</a>
					</div>
					</div>

					<h3 class="titleCenter" style="text-align: center;">Pagos Únicos</h3>
					<div class="table-responsive">
						<table class="table table-hover table table-striped sortable" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Descripción</th>
								<th class="text-center">Cantidad </th>
								<th class="text-center">Fecha limite </th>
								<th class="text-center">Comentario</th>
								<th colspan="3" class="text-center">Opciones</th>
							</tr>
						</thead>
						@foreach($pay as $pays)
						@if($pays->type == 1)
						<tbody>
							
								<tr>
									<td class="text-center">{{$pays->title}}</td>
									<td class="text-center">{{$pays->description}}</td>
									<td class="text-center">${{$pays->quantity}}</td>
									@if($pays->limit_date == null)
									<td class="text-center">No requerida</td>
									@else
									<td class="text-center"><?php echo date("d-m-Y",strtotime($pays->limit_date)); ?></td>
									@endif
									<td class="text-center">{{$pays->comment}}</td>
									<td>
										<a href="{{ Route('pays_edit', $pays->id) }}" class="btn">
											<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
										</a>
									</td>
									<!--<td>
										<form action="{{ route('pays_delete', $pays->id) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn" style="background-color: transparent;">
												<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
											</button>
										</form>
									</td>-->
								</tr>
							</tbody>
							@endif
						@endforeach
						</table>
						<center>{{$pay->links()}}</center>
					</div>

<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->

						<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->


						<div class="col-md-12 text-center" >
						<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'search_admin2','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search2" id="search2">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						<h3 class="titleCenter" style="text-align: center;">Pagos Recurrentes</h3>
						<div class="table-responsive">
						<table class="table table-hover table table-striped sortable" id="tableEmployees">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Descripción </th>
								<th class="text-center">Asignatura </th>
								<th class="text-center">Cantidad </th>
								<th class="text-center">Fecha Limite </th>
								<th class="text-center">Comentario </th>
								<th class="text-center">% de Sanción </th>
								<th colspan="2" class="text-center">Opciones </th>
							</tr>
						</thead>
							@foreach($rec as $recs)
							@if($recs->type == 2)
							<tbody>
								<tr>
									<td class="text-center">{{$recs->title}}</td>
									<td class="text-center">{{$recs->description}}</td>
									<td class="text-center">{{$recs->subject}}</td>
									<td class="text-center">${{$recs->quantity}}</td>
									<td class="text-center">{{$limit_date}}</td>
									<td class="text-center">{{$recs->comment}}</td>
									<td class="text-center">{{$recs->aument}}%</td>
										<td>
										<a href="{{ Route('pays_edit_r', $recs->id) }}" class="btn">
											<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
										</a>
									</td>
									<!--<td>
										<form action="{{ route('pays_delete_r', $recs->id) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn" style="background-color: transparent;">
												<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
											</button>
										</form>
									</td>-->
								</tr>
							</tbody>
						@endif
						@endforeach
						</table>
						{{$rec->links()}}
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>