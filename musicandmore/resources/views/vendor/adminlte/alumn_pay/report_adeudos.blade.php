@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Reporte de Adeudos</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
					<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'search_admin','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						<div class="col-md-4 col-md-offset-8" style="float: right; width: 500px;" >
					</div>
					</div>

					<h3 class="titleCenter" style="text-align: center;">Adeudos al día de hoy</h3>
					<div class="table-responsive">
						<table class="table table-hover table table-striped tablesorter sortable" id="tableEmployees">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">ID</th>
								<th class="text-center">Alumno</th>
								<th class="text-center">Adeudos Pendientes</th>
								<th class="text-center">Monto Total</th>
                                <th class="text-center">Ultima Mensualidad</th>
							</tr>
						</thead>
						@foreach($adeudos as $adeudo)
						<tbody>
							<tr>
								<td class="text-center">{{$adeudo['id_user']}}</td>
								<td class="text-center"><a href="{{ Route('report_adeudos_user', ['idUser' => $adeudo['id_user']]) }}">{{$adeudo['name']}}</a></td>
								<td class="text-center">{{$adeudo['adeudos']}}</td>
								<td class="text-center">$ {{$adeudo['monto']}}</td>
								<td class="text-center">{{$adeudo['ultima_mens']}}</td>
							</tr>
						</tbody>
						@endforeach
						</table>
					</div>
					

					<!-- INICIA MODAL PARA INSCRIBIR -->
						<div class="modal fade" id="meses" tabindex="-1">
							<div class="modal-dialog">
								<div class="modal-content">
									<!-- Modal Header -->
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>
									<!-- Modal body -->
									<div class="modal-body">
										<h4 class="modal-title">Meses Adeudados</h4>
									</div>
								</div>
							</div>
						</div>
					<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->

						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>