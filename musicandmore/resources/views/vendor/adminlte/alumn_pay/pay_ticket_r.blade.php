<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ticket de cobro: Pago recurrente</title>
	<style>
		body{
			font-family: 'Open Sans', sans-serif
		}
	</style>
</head>
<body>
	<div style="float: left; width: 300px;">
		<div align="center"><img src="{{ asset('/img/M&M.png') }}" alt="" width="150px" height="100px"></div>
		<p style="text-align: center"><strong>Music and More</strong></p>
		<div>
			<p style="text-align: center; font-weight: bold;">Folio: PR-{{$asignacion->assi}}</p>
			<p style="text-align:justify;"><strong>Fecha:</strong> <?php echo date("d-m-Y",strtotime($asignacion->date_pay)); ?></p>
			<h4 style="text-align: center;">Ticket de Pago: {{$asignacion->title}}</h4>
		<p><strong>Alumno: </strong>{{$asignacion->name}}</p>
		<p><strong>Pago Realizado: </strong>{{$asignacion->title}}</p>  
		<p><strong>Asignatura: </strong>{{$asignacion->subject}}</p>
		<p><strong>Cantidad por pagar: </strong>${{$asignacion->quantity}}</p>
		<p><strong>Descuento: </strong>{{$asignacion->desc_cant}}%</p>
		<p><strong>Sanción: </strong>{{$asignacion->aument}}%</p>
		<p><strong>Método de Pago: </strong>{{$asignacion->metodo}}</p>
		<p><strong>Fecha de Pago: </strong><?php echo date("d-m-Y",strtotime($asignacion->date_pay)); ?></p>
		<p><strong>Comentarios: </strong>{{$asignacion->comment}}</p>
		<p><strong>Total: $ {{$asignacion->total}}</strong></p>
			<br>
			<p style="text-align: center"><strong>Firma de Recibido: </strong></p>
			<p style="text-align: center">_________________________</p>
			<p style="text-align: center">{{$asignacion->padre}}</p>
			<p style="text-align: center"><strong>Firma del Administrador: </strong></p>
			<p style="text-align: center">_________________________</p>
		</div>
	</div>
	<div style="float: right; width: 300px;">
		<div align="center"><img src="{{ asset('/img/M&M.png') }}" alt="" width="150px" height="100px"></div>
		<p style="text-align: center"><strong>Music and More</strong></p>
		<div>
			<p style="text-align: center; font-weight: bold;">Folio: PR-{{$asignacion->assi}}</p>
			<p style="text-align:justify;"><strong>Fecha:</strong> <?php echo date("d-m-Y",strtotime($asignacion->date_pay)); ?></p>
			<h4 style="text-align: center;">Ticket de Pago: {{$asignacion->title}}</h4>
		<p><strong>Alumno: </strong>{{$asignacion->name}}</p>
		<p><strong>Pago Realizado: </strong>{{$asignacion->title}}</p>  
		<p><strong>Asignatura: </strong>{{$asignacion->subject}}</p>
		<p><strong>Cantidad por pagar: </strong>${{$asignacion->quantity}}</p>
		<p><strong>Descuento: </strong>{{$asignacion->desc_cant}}%</p>
		<p><strong>Sanción: </strong>{{$asignacion->aument}}%</p>
		<p><strong>Método de Pago: </strong>{{$asignacion->metodo}}</p>
		<p><strong>Fecha de Pago: </strong><?php echo date("d-m-Y",strtotime($asignacion->date_pay)); ?></p>
		<p><strong>Comentarios: </strong>{{$asignacion->comment}}</p>
		<p><strong>Cantidad Total a pagar: $ {{$asignacion->total}}</strong></p>
			<br>
			<p style="text-align: center"><strong>Firma de Recibido: </strong></p>
			<p style="text-align: center">_________________________</p>
			<p style="text-align: center">{{$asignacion->padre}}</p>
			<p style="text-align: center"><strong>Firma del Administrador: </strong></p>
			<p style="text-align: center">_________________________</p>
			<h6>Nota: Este comprobante es una copia para el alumno.</h6>
		</div>
	</div>
</body>
</html>