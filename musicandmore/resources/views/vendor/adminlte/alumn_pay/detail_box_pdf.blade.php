<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Reporte de Corte de Caja</title>
	<style>
		body{
			font-family: 'Open Sans', sans-serif
		}
	</style>
</head>
<body>
		<div align="center"><img src="{{ asset('/img/M&M.png') }}" alt="" width="150px" height="100px"></div>
		<p style="text-align: center"><strong>Music and More</strong></p>
		<div>
			<p style="text-align: center; font-weight: bold;">Folio: C-#{{$cut->id}}</p>
			<p style="text-align:justify;"><strong>Fecha y hora: <?php echo date("d-m-Y",strtotime($arqueo->date))?></strong></p>
			<h4 style="text-align: center;">Reporte de Corte de Caja</h4>
			<h4 class="titleCenter" style="text-align: center;">Pagos Únicos</h4>
					<div class="table-responsive">
						<table class="table table-hover table table-striped" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Alumno</th>
								<!--<th class="text-center">Cantidad</th>-->
								<!--<th class="text-center">-%</th>-->
								<th class="text-center">Total</th>
								<th class="text-center" colspan="3">Método de Pago</th>
								<!--<th class="text-center">Comentario</th>-->
							</tr>
						</thead>
						@foreach($box_unic as $a)
						@if($a->type == 1)
						<tbody>
							
								<tr>
									<td class="text-center">
									<strong>{{$a->title}}</strong></a></td>
									<td class="text-center">{{$a->name}}</td>
									<!--<td class="text-center">${{$a->quantity}}</td>-->
									<!--<td class="text-center">{{$a->desc_cant}}%</td>-->
									<td class="text-center">${{$a->total}}</td>
									<td class="text-center">{{$a->metodo}}</td>
									<!--<td class="text-center">{{$a->comment}}</td>-->
								</tr>
							</tbody>
							@endif
							@endforeach
						</table>
					</div>
						<h4 class="titleCenter" style="text-align: center;">Pagos Recurrentes</h4>
						<div class="table-responsive">
						<table class="table table-hover table table-striped" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Alumno</th>
								<th class="text-center">Asignatura</th>
								<!--<th class="text-center">Cantidad</th>
								<th class="text-center">-%</th>
								<th class="text-center">+%</th>
								<th class="text-center">Beca</th>-->
								<th class="text-center">Total</th>
								<th class="text-center" colspan="3">Método de Pago</th>
								<!--<th class="text-center">Comentario</th>-->
							</tr>
						</thead>
							@foreach($box_rec as $a)
							@if($a->type == 2)							
							<tbody>
								<tr>
									
									<td class="text-center"><strong>{{$a->title}}</strong></a></td>
									<td class="text-center">{{$a->name}}</td>
									<td class="text-center">{{$a->subject}}</td>
									<!--<td class="text-center">${{$a->quantity}}</td>
									<td class="text-center">{{$a->desc_cant}}%</td>
									<td class="text-center">{{$a->aument}}%</td>
									<td class="text-center">{{$a->beca}}%</td>-->
									<td class="text-center">${{$a->total}}</td>
									<td class="text-center">{{$a->metodo}}</td>
									<!--<td class="text-center">{{$a->comment}}</td>-->
								</tr>
							</tbody>
							@endif
							@endforeach
						</table>
					</div>
<p>___________________________________________________________________________________</p>
<div style="float: left;width: 600px">
	<p><strong>Total: ${{$cut->total}}</strong></p>
	@if($cut->total_cheq == null)
	<p>Total Cheques: $0</p>
	@else
	<p>Total Cheques: ${{$cut->total_cheq}}</p>
	@endif
	@if($cut->total_cred == null)
	<p>Total Crédito: $0</p>
	@else
	<p>Total Crédito: ${{$cut->total_cred}}</p>
	@endif
	@if($cut->total_deb == null)
	<p>Total Débito: $0</p>
	@else
	<p>Total Débito: ${{$cut->total_deb}}</p>
	@endif
	@if($cut->total_transf == null)
	<p>Total Transferencia: $0</p>
	@else
	<p>Total Transferencia: ${{$cut->total_transf}}</p>
	@endif
	@if($cut->total_depos == null)
	<p>Total Deposito: $0</p>
	@else
	<p>Total Deposito: ${{$cut->total_depos}}</p>
	@endif
	@if($cut->total_efec == null)
	<p>Total Efectivo: $0</p>
	@else
	<p><strong>Total Efectivo: ${{$cut->total_efec}}</strong></p>
	@endif
	<p><strong>Total del Arqueo: ${{$sum_arq}}</strong> </p>
	@if($desface > 0)
	<p><strong>Faltante: ${{$desface}}</strong></p>
	@elseif($desface < 0)
	<?php $d = $desface; $t = $d*-1; ?>
	<p><strong>Sobrante: ${{$t}}</strong></p>
	@elseif($desface = 0)
	<p></p>
	@endif
</div>
	</div>
</body>
</html>