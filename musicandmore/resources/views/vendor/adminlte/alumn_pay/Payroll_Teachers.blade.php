@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">NÓMINA</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'nomineS','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
					</div>
						<h3 class="titleCenter" style="text-align: center;">Nominas por Pagar</h3>
						<div class="table-responsive text-center">
						<table class="table table-hover table table-striped tablesorter sortable" id="tableEmployees">
						<thead style="background: black; color: white;">
							<tr>
								<th class="tableCourse" style="color: white">#ID</th>
								<th class="tableCourse" style="color: white">Maestro</th>
								<th class="tableCourse" style="color: white">Costo por clase</th>
								<th class="tableCourse" style="color: white">Nº de Horas</th>
								<th class="tableCourse" style="color: white">Total</th>
								<th class="tableCourse" style="color: white">Crear Pago</th>
								<th colspan="2" class="text-center">Ver</th>
							</tr>
						</thead>							
						<tbody>
							@foreach($teacher as $teachers)
								<tr>
									@if($a->where('id_teacher','=',$teachers->employee_id)->count() > 0)
									<td class="text-center">#{{$teachers->employee_id}}</td>
									<td class="text-center">{{$teachers->name}}</td>
									<td class="text-center">$100</td>
									<td class="text-center">{{$a->where('id_teacher','=',$teachers->employee_id)->count()}} hrs.</td>
									<?php 
										$num = $a->where('id_teacher','=',$teachers->employee_id)->count();
										$t = $num*100;
									 ?> 
									<td class="text-center">${{$t}}</td> 
									<td class="text-center">
										<a href="{{ Route('nomine_create', $teachers->employee_id) }}" class="btn">
											<i class="fa fa-money fa-2x" aria-hidden="true"></i>
										</a>
									</td>
									<td class="text-center">
										<a href="{{ Route('details_np', $teachers->employee_id) }}" class=" btn btn-warning" target="_blank">
											<i class="fa fa-plus" aria-hidden="true"></i> Detalles
										</a>
									</td>
								</tr>
									@endif
								@endforeach
							</tbody>
						</table>
						</div>

						<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'nomineN','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search2" id="search2">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						<div class="col-md-12 text-center" >

						<h3 class="titleCenter" style="text-align: center;">Pagos de Nomina</h3>
						<div class="table-responsive">
						<table class="table table-hover table table-striped tablesorter sortable" id="tableE">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center"># ID</th>
								<th class="text-center">Título</th>
								<th class="text-center">Profesor</th>
								<th class="text-center">Horas Pagadas</th>
								<th class="text-center">Cantidad Pagada</th>
								<th class="text-center">Fecha</th>
								<th class="text-center">Estado</th>
								<th class="text-center">Ver</th>
							</tr>
						</thead>
						
						@foreach($nomine as $nomines)
							<tbody>
								<tr>
									<td class="text-center">#{{$nomines->id}}</td>
									<td class="text-center">
									<a href="{{route('ticket_nomine',$nomines->id)}}" target="_blank"><strong>{{$nomines->name_pay}}</strong></a>
									</td>
									<td class="text-center">{{$nomines->teacher}}</td>
									<td class="text-center">{{$nomines->num_hour}} hrs</td>
									<td class="text-center">${{$nomines->quantity_pay}}</td>
									<td class="text-center"><?php echo date("d-m-Y",strtotime($nomines->date)); ?></td>
									@if($nomines->is_completed == 1)
									<td class="text-center">
									<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="0%" disabled="true">
									<i class="fa fa-check-circle" style="color: white;"></i></td>
									</button></td>
									<td class="text-center">
										<a href="{{Route('details_npp',$nomines->id)}}" class="btn btn-warning" target="_blank">
											<i class="fa fa-plus" aria-hidden="true"></i> Detalles
										</a>
									</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
						<center>{{$nomine->links()}}</center>
						</div>
						</div>
						</div>

						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
		<!--Ventana modal de detalles-->
		@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>