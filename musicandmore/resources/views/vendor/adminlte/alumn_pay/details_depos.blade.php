@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Detalles de Depósito</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
							</div>
					

					<h3 class="titleCenter" style="text-align: center;">Depósito de {{$deposito->name_cut}}</h3>
						<h4 class="titleCenter" style="text-align: center;">Persona que realizo el depósito: {{$deposito->name}}</h4>
					<div class="table-responsive">
						<table class="table table-hover table table-striped" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">#</th>
								<th class="text-center">Corte de Caja</th>
								<th class="text-center">Realizado por</th>
								<th class="text-center">Nº Folio</th>
								<th class="text-center">Nº Autorización</th>
								<th class="text-center">Banco</th>
								<th class="text-center">Total Depositado</th>
								<th class="text-center">Fecha</th>
								<th class="text-center">Evidencia</th>
							</tr>
						</thead>
						<tbody>
							
								<tr>
									<td class="text-center">	
									<strong>{{$deposito->depos}}</strong></a></td>
									<td class="text-center">	
									<strong>{{$deposito->name_cut}}</strong></a></td>
									<td class="text-center">{{$deposito->name}}</td>
									<td class="text-center">{{$deposito->folio}}</td>
									<td class="text-center">{{$deposito->aut}}</td>
									<td class="text-center">{{$deposito->banco}}</td>
									<td class="text-center">${{$deposito->total}}</td>
									<td class="text-center"><?php echo date("d-m-Y",strtotime($deposito->date)); ?>
									<td class="text-center">@php
											echo '<img src="data:image/jpeg;base64,'.base64_encode( $deposito->image ).'"  alt="" style="width:100%" class="img-responsive"/>';
										@endphp</td>
								</tr>
							</tbody>
						</table>
					</div>

<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->
						</div>
						
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
				

			</div>
		</div>
@endsection