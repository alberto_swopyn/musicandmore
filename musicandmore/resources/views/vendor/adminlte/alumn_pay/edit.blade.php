@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::alumn_pay.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Editar Pago Único</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
					@include('adminlte::layouts.partials._errors')
					<form action="{{ Route('pays_update', $pay->id) }}" method="POST" role="form" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
	<div class="panel-body">

		<div class="col-xs-6">

			<div class="col-md-12 text-center">
				<label for="Nombre" style="font-size: 1.5em">Nombre del pago</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder=""   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{$pay->title}}" required="required">
			</div>

		</div>

		<div class="col-xs-6">

			<div class="col-md-12 text-center">
				<label for="Descripcion" style="font-size: 1.5em">Descripción del pago</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Descripcion" name="Descripcion" placeholder="" >{{$pay->description}}</textarea>
			</div>

		</div>
		<div class="form-group row">
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<label for="Cantidad" style="font-size: 1.5em; padding-top: 1em;">Cantidad a Pagar:</label>
				</div>
	           <div class="col-md-8 col-md-offset-2">
	                <input type="text" class="form-control" id="Cantidad" name="Cantidad" placeholder=""   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{$pay->quantity}}" required="required">
		        </div>
			</div>
        <div class="form-group row">
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<label for="Fecha_limite" style="font-size: 1.5em; padding-top: 1em;">Fecha límite de pago</label>
				</div>
	            <div class="col-md-8 col-md-offset-2">
	            	@if($pay->habilitar == 1)
	               <input id="Fecha_limite" type="date" name="Fecha_limite" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px; padding-left: 3em;" required value="<?php echo date('Y-m-d',strtotime($pay->limit_date)) ?>" disabled/>
	               <br>
	                <input id="no_fecha" type="checkbox" name="no_fecha" value="1" checked="checked"><strong style="font-size: 16px; color: red;" > No requiere fecha límite</strong>
	                @else
	                <input id="Fecha_limite" type="date" name="Fecha_limite" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px; padding-left: 3em;" required value="<?php echo date('Y-m-d',strtotime($pay->limit_date)) ?>"/>
	               <br>
	                <input id="no_fecha" type="checkbox" name="no_fecha" value="1"><strong style="font-size: 16px; color: red;" > No requiere fecha límite</strong>
	                @endif
		        </div>
			</div>
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<label for="Comentarios" style="font-size: 1.5em">Comentarios</label>
				</div>
				<div class="col-md-8 col-md-offset-2">
	                <textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Comentarios" name="Comentarios" placeholder="Comentarios del pago" value="" required="required">{{$pay->comment}}
	                </textarea>
		        </div>

			</div>

        </div>
        <br>
			<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
		</div>
		</div>
	</form>
	</div>
</div>
<script>
	$(document).ready(function(){
		document.getElementById("no_fecha").onclick = function(){
    			if (document.getElementById("Fecha_limite").disabled){
    				document.getElementById("Fecha_limite").disabled = false
    			}else{
    				document.getElementById("Fecha_limite").disabled = true
    			}
    		}

    });
</script>
@endsection