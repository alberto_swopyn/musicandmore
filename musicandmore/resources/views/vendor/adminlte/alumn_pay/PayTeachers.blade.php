@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Mis Nominas</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'paynomine','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
					</div>
						<h3 class="titleCenter" style="text-align: center;">Nominas Pagadas</h3>
						<div class="table-responsive">
						<table class="table table-hover table table-striped tablesorter sortable" id="tableEmployees">
						<thead style="background: black; color: white;">
								<th class="text-center">#ID</th>
								<th class="text-center">Título</th>
								<th class="text-center">Profesor</th>
								<th class="text-center">Horas Pagadas</th>
								<th class="text-center">Cantidad Pagada</th>
								<th class="text-center">Fecha</th>
								<th class="text-center">Estado</th>
						</thead>
						@foreach($nomine as $nomines)		
						@if($nomines->is_completed == 1)
						<tbody>
							<tr>
								<td class="text-center">#{{$nomines->id}}
								<td class="text-center">
									<strong>{{$nomines->name_pay}}</strong>
									</td>
									<td class="text-center">{{$nomines->teacher}}</td>
									<td class="text-center">{{$nomines->num_hour}}</td>
									<td class="text-center">$ {{$nomines->quantity_pay}}</td>
									<td class="text-center"><?php echo date("d-m-Y",strtotime($nomines->date)); ?></td>
								<td class="text-center">
									<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="" disabled="true">
									<strong>Pagado </strong><i class="fa fa-check-circle" style="color: white;"></i></td>
									</button></td>
							</tr>
							</tbody>
							@endif
						@endforeach
					</table>
					<center>{{$nomine->links()}}</center>
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>