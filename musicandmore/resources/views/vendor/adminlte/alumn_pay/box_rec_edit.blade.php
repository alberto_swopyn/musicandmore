@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::alumn_pay.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Edición de Cobro: Pago Recurrente</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
		@include('adminlte::layouts.partials._errors')
					<form action="{{ Route('box_rec_up', $ass->id) }}" method="POST" role="form" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
    	 <div class="pull-right">
			<a href="javascript:void(0)" data-perform="panel-collapse">
				<i class="ti-minus"></i></a>
			<a href="javascript:void(0)" data-perform="panel-dismiss">
				<i class="ti-close"></i></a>
		</div>
	<div class="panel-body">

		
		<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div>
					<span style="font-size: 1.5em">
						Pago a Asignar:
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-money" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="pay[]" class="form-control" id="Curso">
								@foreach($pay as $pays)
								<option value="{{ $pays->id}}"
									{{ (in_array($pays, old('pays', []))) }} 
									@<?php if ($pays->id == $ass->pays_id): ?>
									selected
								<?php endif ?>>
								{{$pays->title }}/{{$pays->subject}}/${{$pays->quantity}}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div>

					<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div>
					<span style="font-size: 1.5em">
						Estudiante que realiza el pago:
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-users" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="student[]" class="form-control" id="Estudiante">
								@foreach($student as $students)
								<option value="{{ $students->id}}"
									{{ (in_array($students, old('students', []))) }} @<?php if ($students->id == $ass->users_id): ?>
									selected
								<?php endif ?>>{{ $students->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div>
					<span style="font-size: 1.5em">
						</div>
					</div>
				</div>

						<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div>
					<span style="font-size: 1.5em">
						Descuento: 
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-percent" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="discount[]" class="form-control" id="Descuento">
								@foreach($discount as $d)
								<option value="{{ $d->id}}"
									{{ (in_array($d, old('d', []))) }} 
									@<?php if ($d->id == $ass->discount): ?>
									selected
								<?php endif ?>>
									{{ $d->title }}/ {{$d->quantity}} %
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div>
					<span style="font-size: 1.5em">
						</div>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div>
					<span style="font-size: 1.5em">
						Método de Pago: 
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-credit-card-alt" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="pago[]" class="form-control" id="Pago">
								@foreach($types as $t)
								<option value="{{ $t->id}}"
									{{ (in_array($t, old('$t', []))) }} @<?php if ($t->id == $ass->type_pay): ?>
									selected
								<?php endif ?>>
									{{ $t->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div>
					<span style="font-size: 1.5em">
						</div>
					</div>
				</div>
<br>
<br>
	<div class="form-group">
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<p></p>
					<label for="Comentarios" style="font-size: 1.5em">Comentarios</label>
				</div>
	            <div class="input-group col-md-offset-1 col-md-11">
	             <textarea style=" border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Comentarios" name="Comentarios" placeholder="Comentarios del pago" value="">{{$ass->comment}}</textarea>
		        </div>

			</div>
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<p></p>
					<label for="Reason" style="font-size: 1.5em">Razón por la que se edita</label>
				</div>
	            <div class="input-group col-md-offset-1 col-md-11">
	             <textarea style=" border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Reason" name="Reason" placeholder="Razón por la que se edita" value="" required></textarea>
		        </div>
			
			</div>
				@if($day > $dia)
        		<label style="font-size: 1.3em;"><strong>Desactivar Aumento<strong></label>
        		<br>
        		@if($ass->no_aument == 1)
        		<input type="checkbox" id="no_aument" name="no_aument" value = "1" checked="checked"><strong style="font-size: 16px; color: red;" > Marcar para desactivar aumento</strong>
        		@else
        		<input type="checkbox" id="no_aument" name="no_aument" value = "1"><strong style="font-size: 16px; color: red;" > Marcar para desactivar aumento</strong>
        		@endif
        	@endif
        </div>
        <br>
        <br>
        <br>
			<div class="col-md-12 text-center">
				<p></p>
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Actualizar</button>
		</div>
		</div>
	</form>
	</div>
</div>
<script>
      $(document).ready(function(){
          $('#Curso').select2();
          $('#Estudiante').select2();
          $('#Descuento').select2();
          $('#Pago').select2();
       });
    </script>
@endsection