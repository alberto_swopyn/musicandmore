@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Detalles Corte de Caja</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
							</div>
					

					<h3 class="titleCenter" style="text-align: center;">Pagos Únicos</h3>
					<div class="table-responsive">
						<table class="table table-hover table table-striped tablesorter" id="tableEmployees">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Alumno</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">-%</th>
								<th class="text-center">Total</th>
								<th class="text-center">Fecha limite</th>
								<th class="text-center">Fecha de Pago</th>
								<th class="text-center">Método de Pago</th>
								<th class="text-center">Comentario</th>
								<th class="text-center">Pagado</th>
							</tr>
						</thead>
						@foreach($box_unic as $a)
						@if($a->type == 1)
						<tbody>
							
								<tr>
									<td class="text-center">
									<strong>{{$a->title}}</strong></a></td>
									<td class="text-center">{{$a->name}}</td>
									<td class="text-center">${{$a->quantity}}</td>
									<td class="text-center">{{$a->desc_cant}}%</td>
									<td class="text-center">${{$a->total}}</td>
									@if($a->limit_date == null)
									<td class="text-center">No requerida</td>
									@else
									<td class="text-center"><?php echo date("d-m-Y",strtotime($a->limit_date)); ?></td>
									@endif
									<td class="text-center"><?php echo date("d-m-Y",strtotime($a->date_pay)); ?></td>
									<td class="text-center">{{$a->metodo}}</td>
									<td class="text-center">{{$a->comment}}</td>
									@if($a->is_completed == 1)
									<td class="text-center">
									<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="0%" disabled="true">
									<i class="fa fa-check-circle" style="color: white;"></i></td>
									</button></td>
								</tr>
								@endif
							</tbody>
							@endif
							@endforeach
						</table>
						<center>{{$box_unic->links()}}</center>
					</div>

<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->

						<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->


						
						<h3 class="titleCenter" style="text-align: center;">Pagos Recurrentes</h3>
						<div class="table-responsive">
						<table class="table table-hover table table-striped tablesorter" id="tableEmployees1">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Alumno</th>
								<th class="text-center">Asignatura</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">-%</th>
								<th class="text-center">+%</th>
								<th class="text-center">Beca</th>
								<th class="text-center">Total</th>
								<th class="text-center">Fecha de Pago</th>
								<th class="text-center">Método de Pago</th>
								<th class="text-center">Comentario</th>
								<th class="text-center">Pagado</th>
							</tr>
						</thead>
							@foreach($box_rec as $a)
							@if($a->type == 2)							
							<tbody>
								<tr>
									
									<td class="text-center"><strong>{{$a->title}}</strong></a></td>
									<td class="text-center">{{$a->name}}</td>
									<td class="text-center">{{$a->subject}}</td>
									<td class="text-center">${{$a->quantity}}</td>
									<td class="text-center">{{$a->desc_cant}}%</td>
									<td class="text-center">{{$a->aument}}%</td>
									<td class="text-center">{{$a->beca}}%</td>
									<td class="text-center">${{$a->total}}</td>
									<td class="text-center"><?php echo date("d-m-Y",strtotime($a->date_pay)); ?></td>
									<td class="text-center">{{$a->metodo}}</td>
									<td class="text-center">{{$a->comment}}</td>
									@if($a->is_completed == 1)
									<td class="text-center">
									<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="0%" disabled="true"><i class="fa fa-check-circle" style="color: white;"></i></td>
									</button></td>
								</tr>
								@endif
							</tbody>
							@endif
							@endforeach
						</table>
						<center>{{$box_rec->links()}}</center>
					</div>
						</div>
						
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>
				

			</div>
		</div>
@endsection
@section('personal-js')
<style>

table.tablesorter thead tr .header {
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}
table.tablesorter thead tr .headerSortUp {
}
table.tablesorter thead tr .headerSortDown {
}
</style>
<!-- <link rel="stylesheet" href="{{ asset('css/style_sorterTable.css') }}"> -->
<script src="{{ asset('js/jquery.tablesorter.js') }}"></script>
<script>
    // call the tablesorter plugin 
    $("#tableEmployees").tablesorter( {sortList: [[0,0], [1,0]]} ); 
    $("#tableEmployees1").tablesorter( {sortList: [[0,0], [1,0]]} ); 
</script>
<script>
	$(".applicantsList-single").select2();
	$(".applicantsList-single").change(function(event) {
		$('#Applicant_id').val($(this).val());
	});
	$(".jobTitleList-single").select2();
	$(".jobCenterList-single").select2();
</script>
@stop