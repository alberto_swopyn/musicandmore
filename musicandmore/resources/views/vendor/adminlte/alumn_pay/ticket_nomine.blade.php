<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pago de Nomina</title>
	<style>
		body{
			font-family: 'Open Sans', sans-serif
		}
	</style>
</head>
<body>
	<div style="float: left; width: 300px;">
		<div align="center"><img src="{{ asset('/img/M&M.png') }}" alt="" width="150px" height="100px"></div>
		<p style="text-align: center"><strong>Music and More</strong></p>
		<div>
			<p style="text-align:justify;"><strong>Fecha: <?php echo date("d-m-Y",strtotime($nomine->date)); ?></strong> </p>
			<h4 style="text-align: center;">Pago de Nomina</h4>
			<br>
			<p><strong>Nombre del pago: </strong>{{$nomine->name_pay}}</p>
			<p><strong>Profesor: </strong>{{$nomine->teacher}}</p>
			<p><strong>Numero de Horas Pagadas: </strong>{{$nomine->num_hour}} hora/s</p>
			<p><strong>Comentarios: </strong>{{$nomine->comment}}</p>
			<p><strong>Cantidad Total a pagar: </strong>$ {{$nomine->quantity_pay}}</p>
			<br>
			<p style="text-align: center"><strong>Firma de Recibido: </strong></p>
			<p style="text-align: center">_________________________</p>
			<p style="text-align: center">{{$nomine->teacher}}</p>
			<p style="text-align: center"><strong>Firma del Administrador: </strong></p>
			<p style="text-align: center">_________________________</p>
		</div>
	</div>
	<div style="float: right; width: 300px;">
		<div align="center"><img src="{{ asset('/img/M&M.png') }}" alt="" width="150px" height="100px"></div>
		<p style="text-align: center"><strong>Music and More</strong></p>
		<div>
			<p style="text-align:justify;"><strong>Fecha: <?php echo date("d-m-Y",strtotime($nomine->date)); ?></strong> </p>
			<h4 style="text-align: center;">Pago de Nomina</h4>
			<br>
			<p><strong>Nombre del pago: </strong>{{$nomine->name_pay}}</p>
			<p><strong>Profesor: </strong>{{$nomine->teacher}}</p>
			<p><strong>Numero de Horas Pagadas: </strong>{{$nomine->num_hour}} hora/s</p>
			<p><strong>Comentarios: </strong>{{$nomine->comment}}</p>
			<p><strong>Cantidad Total a pagar: </strong>$ {{$nomine->quantity_pay}}</p>
			<br>
			<p style="text-align: center"><strong>Firma de Recibido: </strong></p>
			<p style="text-align: center">_________________________</p>
			<p style="text-align: center">{{$nomine->teacher}}</p>
			<p style="text-align: center"><strong>Firma del Administrador: </strong></p>
			<p style="text-align: center">_________________________</p>
			<br>
			<h6>Nota: Este comprobante es una copia para el maestro.</h6>
		</div>
	</div>
</body>
</html>