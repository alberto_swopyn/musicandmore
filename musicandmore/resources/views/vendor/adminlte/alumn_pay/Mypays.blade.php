@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Mis Pagos</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						
					</div>
					</div>
						<h3 class="titleCenter" style="text-align: center;">Pagos Únicos Realizados</h3>
					<div class="table-responsive">
						<table class="table table-hover table table-striped sortable" id="tableEmployees">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Descripción</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">Descuento</th>
								<th class="text-center">Total</th>
								<th class="text-center">Fecha limite</th>
								<th class="text-center">Fecha de Pago</th>
								<th class="text-center">Método de Pago</th>
								<th class="text-center">Comentario</th>
								<th colspan="3" class="text-center">Pagado</th>
							</tr>
						</thead>
						@foreach($asignacion as $a)
						@if($a->type == 1)
						<tbody>
							
								<tr>
									<td class="text-center">{{$a->title}}</td>
									<td class="text-center">{{$a->description}}</td>
									<td class="text-center">${{$a->quantity}}</td>
									<td class="text-center">{{$a->desc_cant}}%</td>
									<td class="text-center">${{$a->total}}</td>
									<td class="text-center"><?php echo date("d-m-Y",strtotime($a->limit_date)); ?></td>
									<td class="text-center"><?php echo date("d-m-Y",strtotime($a->date_pay)); ?></td>
									<td class="text-center">{{$a->metodo}}</td>
									<td class="text-center">{{$a->comment}}</td>
									@if($a->is_completed == 1)
									<td class="text-center">
									<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="0%" disabled="true">
									<i class="fa fa-check-circle" style="color: white;"></i></td>
									</button></td>
								</tr>
								@endif
							</tbody>
							@endif
							@endforeach
						</table>
						{{$asignacion->links()}}
					</div>

<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->

						<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->


						
						<h3 class="titleCenter" style="text-align: center;">Pagos Recurrentes Realizados</h3>
						<div class="table-responsive">
						<table class="table table-hover table table-striped sortable" id="tableEmployees2">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Descripción</th>
								<th class="text-center">Asignatura</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">-%</th>
								<th class="text-center">+%</th>
								<th class="text-center">Total</th>
								<th class="text-center">Fecha de Pago</th>
								<th class="text-center">Método de Pago</th>
								<th class="text-center">Comentario</th>
								<th colspan="3" class="text-center">Pagado</th>
							</tr>
						</thead>
							@foreach($ass_rec as $a)
							@if($a->type == 2)							
							<tbody>
								<tr>
									
									<td class="text-center">{{$a->title}}</td>
									<td class="text-center">{{$a->description}}</td>
									<td class="text-center">{{$a->subject}}</td>
									<td class="text-center">${{$a->quantity}}</td>
									<td class="text-center">{{$a->desc_cant}}%</td>
									<td class="text-center">{{$a->aument}}%</td>
									<td class="text-center">${{$a->total}}</td>
									<td class="text-center"><?php echo date("d-m-Y",strtotime($a->date_pay)); ?></td>
									<td class="text-center">{{$a->metodo}}</td>
									<td class="text-center">{{$a->comment}}</td>
									@if($a->is_completed == 1)
									<td class="text-center">
									<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="0%" disabled="true">
									<i class="fa fa-check-circle" style="color: white;"></i></td>
									</button></td>
								</tr>
								@endif
							</tbody>
							@endif
							@endforeach
						</table>
						{{$ass_rec->links()}}
					</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>