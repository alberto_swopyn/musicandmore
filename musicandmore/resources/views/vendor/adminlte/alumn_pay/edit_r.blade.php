@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::alumn_pay.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Editar Pago Recurrente</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
					@include('adminlte::layouts.partials._errors')
					<form action="{{ Route('pays_update_r', $pay->id) }}" method="POST" role="form" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
	<div class="panel-body">

		<div class="col-xs-6">

			<div class="col-md-12 text-center">
				<label for="Nombre" style="font-size: 1.5em">Nombre del pago</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder=""   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{$pay->title}}" required>
			</div>

		</div>

		<div class="col-xs-6">
			<div class="col-md-12 text-center">
				<label for="Descripcion" style="font-size: 1.5em">Descripción del pago</label>
			</div>
			<div class="col-md-8 col-md-offset-2">
				<textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Descripcion" name="Descripcion" placeholder="" >{{$pay->description}}</textarea>
			</div>

		</div>

		<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div>
					<span style="font-size: 1.5em">
						Asignatura
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-users" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="group[]" class="form-control" id="Curso">
								@foreach($group as $groups)
								<option value="{{$groups->id}}"
									{{ (in_array($groups, old('groups', []))) }} 
									@<?php if ($groups->id == $pay->id_course): ?>
									selected
								<?php endif ?>
									>{{ $groups->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div>
					<span style="font-size: 1.5em"></span>
						</div>
					</div>
				</div>
</div>
		<div class="form-group row">
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<label for="Cantidad" style="font-size: 1.5em">Cantidad a pagar</label>
				</div>
	           <div class="col-md-8 col-md-offset-2">
	                <input type="number" class="form-control" id="Cantidad" name="Cantidad" placeholder="$0.00" min="0" required style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{$pay->quantity}}">
		        </div>
			</div>
        </div>
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<label for="Comentarios" style="font-size: 1.5em">Comentarios</label>
				</div>
	            <div class="col-md-8 col-md-offset-2">
	                <textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Comentarios" name="Comentarios" placeholder="Comentarios del pago" value="" >{{$pay->comment}}</textarea>
		        </div>

			</div>
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<label for="Fecha_limite" style="font-size: 1.5em; padding-top: 1em;">Porcentaje a sancionar</label>
				</div>
	            <div class="col-md-8 col-md-offset-2">
	            	<input type="number" class="form-control" id="Sancion" name="Sancion" placeholder="%" min="0" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required value="{{$pay->aument}}">
		        </div>
			</div>

        </div>
        <br>
			<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
		</div>
		</div>
	</form>
	</div>
</div>
<script>
      $(document).ready(function(){
          $('#Curso').select2();
       });
    </script>
@endsection