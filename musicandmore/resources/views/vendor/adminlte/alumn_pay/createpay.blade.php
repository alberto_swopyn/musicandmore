@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
@section('personal_style')
@include('adminlte::alumn_pay.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Crear Pago Único</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
		<form method="POST" enctype="multipart/form-data" action="{{ Route('pay_add') }}" role="form">
     	{{ csrf_field() }}
    	 <div class="pull-right">
			<a href="javascript:void(0)" data-perform="panel-collapse">
				<i class="ti-minus"></i></a>
			<a href="javascript:void(0)" data-perform="panel-dismiss">
				<i class="ti-close"></i></a>
		</div>
	<div class="panel-body">

	<h3 style="text-align: center;" ><strong>Creación de Pago Único</strong></h3>
	<br>
		<div class="col-xs-6">

			<div class="col-md-12 text-center">
				<label for="Nombre" style="font-size: 1.5em">Nombre del pago</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="" required  style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px">
			</div>

		</div>

		<div class="col-xs-6">

			<div class="col-md-12 text-center">
				<label for="Descripcion" style="font-size: 1.5em">Descripción del pago</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Descripcion" name="Descripcion" placeholder=""></textarea>
			</div>

		</div>

		<div class="form-group row">
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<label for="Cantidad" style="font-size: 1.5em">Cantidad a pagar:</label>
				</div>

	    <div class="col-md-8 col-md-offset-2">
	            	<input type="number" class="form-control" id="Cantidad" name="Cantidad" placeholder="$0.00" min="0" required style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px">
		        </div>
			</div>
        </div>
        <div class="form-group row">
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<label for="Fecha_limite" style="font-size: 1.5em; padding-top: 1em;">Fecha límite de pago</label>
				</div>
	             <div class="col-md-8 col-md-offset-2">
	                <input id="Fecha_limite" type="date" name="Fecha_limite" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px; padding-left: 3em;">
	                <br>
	                <input id="no_fecha" type="checkbox" name="no_fecha"><strong style="font-size: 16px; color: red;" value = "1"> No requiere fecha límite</strong>
		        </div>
			</div>
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<label for="Comentarios" style="font-size: 1.5em">Comentarios</label>
				</div>
	            <div class="col-md-8 col-md-offset-2">
	                <textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Comentarios" name="Comentarios" placeholder="Comentarios del pago" value="" ></textarea>
		        </div>

			</div>
        </div>
        <br>
			<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
		</div>
		</div>
	</form>
	</div>
</div>
<script>
	$(document).ready(function(){
		document.getElementById("no_fecha").onclick = function(){
    			if (document.getElementById("Fecha_limite").disabled){
    				document.getElementById("Fecha_limite").disabled = false
    			}else{
    				document.getElementById("Fecha_limite").disabled = true
    			}
    		}
    	});
</script>
@endsection