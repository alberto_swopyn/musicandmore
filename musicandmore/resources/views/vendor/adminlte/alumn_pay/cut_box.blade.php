@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Cortes de Caja</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
							<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'search_cut_box','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>

						
					</div>
					
					@if($cont > 1)
					<form method="GET" action="{{ Route('cuts_job_center') }}" role="form">
							{{ csrf_field() }}
					<div>
						<label for="selectEmployee" >Sucursales:</label>
					</div>
					<div class="col-md-6">
						<select name="selectEmployee" id="selectEmployee" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;">
							@foreach($sucursal as $su)
							<option value="{{ $su->tree_job_center_id }}">
								{{ $su->text }}
							</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6">
						<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
					</div>
					</form>
					@endif
					

						<h3 class="titleCenter" style="text-align: center;">Reportes</h3>
						<h4 class="titleCenter" style="text-align: center;">Cortes de Caja Con Arqueo</h4>
						<div class="table-responsive">
						<table class="table table-hover table table-striped sortable" id="tableEmployees">
						<thead style="background: black; color: white;">
								<th class="text-center">#ID</th>
								<th class="text-center">Descripción</th>
								<th class="text-center">Realizado por</th>
								<th class="text-center">Cobros</th>
								<th class="text-center">Total</th>
								<th class="text-center">Total Efectivo</th>
								<th class="text-center">Total Arqueo</th>
								<th class="text-center">Faltante</th>
								<th class="text-center">Sobrante</th>
								<th class="text-center">Fecha del Corte</th>
								<th class="text-center">Arqueo</th>
								<th class="text-center">Estado</th>
								<th class="text-center" colspan="3">Opciones</th>
						</thead>
						<?php $x = 0 ?>
						@foreach($cut as $c)
						<?php $x = $c->box; $b = $c->name_cut ?>
						@if($c->arqueo == 1)
						<tbody>
							<tr>
								<td class="text-center">#{{$c->box}}</td>
								<td class="text-center"><a href="{{route('details_box_pdf',$c->box)}}" target="_blank"><strong>{{$c->name_cut}}</strong></a></td>
								<td class="text-center">{{$c->name}}</td>
								<td class="text-center">{{$c->num_cobros}}</td>
								<td class="text-center" style="color: red"><strong>${{$c->total}}</strong>
								</td>
								@if($c->total_efec == null)
								<td class="text-center">$0</td>
								@else
								<td class="text-center">${{$c->total_efec}}</td>
								@endif
								<td class="text-center">${{$c->sum_arq}}</td>
								@if($c->desface >= 0)
								<td class="text-center">${{$c->desface}}</td>
								@else
								<td class="text-center"></td>
								@endif
								@if($c->desface < 0)
								<?php $d = $c->desface;?>
								<?php $t = $d*-1; ?>
								<td class="text-center">${{$t}}</td>
								@else
								<td class="text-center"></td>
								@endif
								<td class="text-center"><?php echo date("d-m-Y",strtotime($c->date)); ?>
								</td>
								
								<td class="text-center">
									<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="0%" disabled="true"><i class="fa fa-check-circle" style="color: white;"></i>
									</button></td>
								@if($c->depositado == 0)
									<td class="text-center" style="color: red"><strong>Sin depositar</strong>
									</td>
								@else
									<td class="text-center" style="color: red"><a class="pull-left" href="{{Route('details_depos',$c->box)}}"><strong>Deposito Realizado</strong></a>
									</td>
								@endif
								<td class="text-center">
									<a href="{{Route('details_box',$c->box)}}" class="btn btn-warning" target="_blank">
									<i class="fa fa-plus" aria-hidden="true"></i></a>
								</td>
								@if($c->depositado == 0)
								<td class="text-center">
									<a href="" class="btn btn-success" data-toggle="modal" data-target="#confirm-edi<?php echo $x; ?>">
									<i class="fa fa-bank" aria-hidden="true"></i></a>
									<div class="modal fade" id="confirm-edi<?php echo $x; ?>" tabindex="-1">
										<div class="modal-dialog">
											<div class="modal-content">
											<!-- Modal Header -->
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
												</div>
											<form action="{{ route('add_deposito', ['idcut' => $x]) }}" method="POST" id="form" enctype="multipart/form-data">
												{{ csrf_field() }}
													<div class="modal-body">
											<h4 class="modal-title">Agregar deposito</h4>
											<br>
											<div class="col-md-8">
												<label for="cuenta" style="text-align: justify; font-size: 15px">Número de Cuenta: </label>
												<input type="text" name="cuenta" id="cuenta" style="width:190px;height:30px;text-align: justify; font-size: 15px" required>
											</div>
											<br>
											<br>
											<div class="col-md-8">
												<label for="folio" style="text-align: justify; font-size: 15px">Número de Folio: </label>
												<input type="text" name="folio" id="folio" style="width:205px;height:30px;text-align: justify; font-size: 15px" required>
											</div>
											<br>
											<br>
											<div class="col-md-8">
												<label for="aut" style="text-align: justify; font-size: 15px">Número de Autorización: </label>
												<input type="text" name="aut" id="aut" style="width:150px;height:30px;text-align: justify; font-size: 15px" required>
											</div>
											<br>
											<br>
											<div class="col-md-8">
												<label for="cie" style="text-align: justify; font-size: 15px">Banco: </label>
												<input type="text" name="cie" id="cie" style="width:285px;height:30px;text-align: justify; font-size: 15px" required>
											</div>
											<br>
											<br>
											<div class="col-md-8">
												<label for="tot" style="text-align: justify; font-size: 15px">Total Depositado: </label>
												<input type="number" name="tot" id="tot" style="width:200px;height:30px;text-align: justify; font-size: 15px" required>
											</div>
											<br>
											<br>
											<div class="form-group">
													<div class="text-center">
														<label for="ArchivosT">Ticket (Imagen de Evidencia)</label>
													</div>
													<div class="col-md-8 col-md-offset-2" >
                									<input type="file" name="ruta" id="ruta" accept="image/jpeg, image/png, image/jpg" onKeyDown="return intro(event)" class="form-control" required>
	        										</div>
												</div>
												<br>
												<br>
      											<div class="modal-footer">
        										<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
  											</form>
    									</div>
  									</div>
									</div>
								</td>
								@endif
								@if($c->desface > 0)
								<td class="text-center">
									<a href="{{Route('desface_pay',$c->box)}}" class="btn btn-danger" target="_blank">
									<i class="fa fa-newspaper-o	" aria-hidden="true"></i></a>
								</td>
								@endif
							</tr>
							</tbody>
							@endif
						@endforeach
					</table>
					<center>{{$cut->links()}}</center>
						</div>

						<h4 class="titleCenter" style="text-align: center;">Cortes de Caja Sin Arqueo</h4>
						<div class="table-responsive">
						<table class="table table-hover table table-striped sortable" id="tableEmployees2">
						<thead style="background: black; color: white;">
								<th class="text-center">#ID</th>
								<th class="text-center">Descripción</th>
								<th class="text-center">Realizado por</th>
								<th class="text-center">Nº de Cobros</th>
								<th class="text-center">Fecha del Corte</th>
								<th class="text-center">Arqueo</th>
								<th class="text-center" colspan="3">Opciones</th>
						</thead>
						<?php $a = 0; $b = ""; ?>
						@foreach($cut_sn as $c)
						<?php $a = $c->box; $b = $c->name_cut ?>
						@if($c->arqueo == 0)
						<tbody>
							<tr>
								<td class="text-center">#{{$c->box}}</td>
								<td class="text-center">{{$c->name_cut}}</td>
								<td class="text-center">{{$c->name}}</td>
								<td class="text-center">{{$c->num_cobros}}</td>
								<td class="text-center"><?php echo date("d-m-Y",strtotime($c->date)); ?>
								<td class="text-center">
								<button type="submit" class="btn btn-danger" aria-label="Left Align" class="btn btn-warning" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>"><i class="fa fa-times-circle" style="color: white;"></i>
							</button>
						<div class="modal fade" id="confirm-edit<?php echo $a; ?>" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
		<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		<!-- Modal body -->
		<form action="{{ route('add_arqueo', ['idcut' => $a]) }}" method="POST" id="form">
		{{ csrf_field() }}
				<div class="modal-body">
		<h4 class="modal-title">Introducir las cantidades correspondientes</h4>
		<br>
		<div class="col-md-4">
			<label for="mil"><img src="{{ asset( 'img/1000.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
			<input type="number" name="mil" id="mil" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="quinientos"><img src="{{ asset( 'img/500.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
			<input type="number" name="quinientos" id="quinientos" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="doscientos"><img src="{{ asset( 'img/200.jpg' )}}" class="img-responsive" height="70px" width="120px"></label>
			<input type="number" name="doscientos" id="doscientos" style="width:80px;height:30px">
		</div>
		<br>
		<br>
		<div class="col-md-4">
		<label for="cien"><img src="{{ asset( 'img/100.png' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
			<input type="number" name="cien" id="cien" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="cincuenta"><img src="{{ asset( 'img/50.jpg' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
			<input type="number" name="cincuenta" id="cincuenta" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="veinte"><img src="{{ asset( 'img/20.jpg' )}}" class="img-responsive" height="70px" width="120px" style="margin-top: 20px"></label>
			<input type="number" name="veinte" id="veinte" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="diez"><img src="{{ asset( 'img/10.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="diez" id="diez" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="cinco"><img src="{{ asset( 'img/5.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="cinco" id="cinco" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="dos"><img src="{{ asset( 'img/2.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="dos" id="dos" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="uno"><img src="{{ asset( 'img/1.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="uno" id="uno" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="cincuenta_centavos"><img src="{{ asset( 'img/50-Centavos.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="cincuenta_centavos" id="cincuenta_centavos" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="veinte_centavos"><img src="{{ asset( 'img/20-c.jpg' )}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="veinte_centavos" id="veinte_centavos" style="width:80px;height:30px">
		</div>
		<div class="col-md-4">
			<label for="diez_centavos"><img src="{{asset('img/10centavos.jpg')}}" class="img-responsive img-circle" height="60px" width="60px" style="margin-top: 10px"></label>
			<br>
			<input type="number" name="diez_centavos" id="diez_centavos"  style="width:80px;height:30px">
		</div>
		</div>
		<br>
      <div class="modal-footer" style="margin-top: 500px">
        <button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
  </form>
    </div>
  </div>
</div>
						</td>
								
								<td class="text-center">
									<a href="{{Route('details_box',$c->box)}}" class="btn btn-warning" target="_blank">
									<i class="fa fa-plus" aria-hidden="true"></i>
										</a>
									</td>
							</tr>
							</tbody>
							@endif
						@endforeach
					</table>
					<center>{{$cut_sn->links()}}</center>
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>

@endsection


@section('personal-js')

<script>
	$(".applicantsList-single").select2();
	$(".applicantsList-single").change(function(event) {
		$('#Applicant_id').val($(this).val());
	});
	$(".jobTitleList-single").select2();
	$(".jobCenterList-single").select2();
</script>

<script src="{{ asset('js/sorttable.js') }}"></script>
@stop
<script src="{{ asset('js/sorttable.js') }}"></script>