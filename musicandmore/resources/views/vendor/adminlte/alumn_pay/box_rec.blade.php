@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::alumn_pay.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Cobro de Pago Recurrente</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
		<form method="POST" enctype="multipart/form-data" action="{{ Route('box_rec_add') }}" role="form">
     	{{ csrf_field() }}
    	 <div class="pull-right">
			<a href="javascript:void(0)" data-perform="panel-collapse">
				<i class="ti-minus"></i></a>
			<a href="javascript:void(0)" data-perform="panel-dismiss">
				<i class="ti-close"></i></a>
		</div>
	<div class="panel-body">

		
		<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div>
					<span style="font-size: 1.5em">
						Pago a Asignar:
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-money" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="pay[]" class="form-control" id="Curso">
								@foreach($pay as $pays)
								<option value="{{ $pays->id}}"
									{{ (in_array($pays, old('pays', []))) }} >{{ $pays->title }}/{{$pays->subject}}/${{$pays->quantity}}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div>

					<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div>
					<span style="font-size: 1.5em">
						Estudiante que realiza el pago:
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-users" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="student[]" class="form-control" id="Estudiante">
								@foreach($student as $students)
								<option value="{{ $students->id}}"
									{{ (in_array($students, old('students', []))) }} >{{ $students->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div>
					<span style="font-size: 1.5em">
						</div>
					</div>
				</div>

						<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div>
					<span style="font-size: 1.5em">
						Descuento: 
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-percent" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="discount[]" class="form-control" id="Descuento">
								@foreach($discount as $d)
								<option value="{{ $d->id}}"
									{{ (in_array($d, old('d', []))) }} >
									{{ $d->title }}/{{$d->quantity}}%
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div>
					<span style="font-size: 1.5em">
						</div>
					</div>
				</div>
						<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div>
					<span style="font-size: 1.5em">
						Método de Pago: 
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-credit-card-alt" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="pago[]" class="form-control" id="Pago">
								@foreach($types as $t)
								<option value="{{ $t->id}}"
									{{ (in_array($t, old('$t', []))) }} >
									{{ $t->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div>
					<span style="font-size: 1.5em">
						</div>
					</div>
				</div>
				<br>
				<br>

<div class="form-group">
			<div class="col-xs-6">
				<div class="col-md-12 text-center">
					<p></p>
					<label for="Comentarios" style="font-size: 1.5em">Comentarios</label>
				</div>
	            <div class="input-group col-md-offset-1 col-md-11">
	             <textarea style=" border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Comentarios" name="Comentarios" placeholder="Comentarios del pago" value=""></textarea>
		        </div>
		        <br>
			@if($day > $dia)
        		<label style="font-size: 1.3em;"><strong>Desactivar Aumento<strong></label>
        		<br>
        		<input type="checkbox" id="no_aument" name="no_aument" value = "1"><strong style="font-size: 16px; color: red;" > Marcar para desactivar aumento</strong>
        	@endif
			</div>
			<br>
				<span style="font-weight:bold;font-size:15pt;text-align:center">Total a pagar: 
					<span>$</span><label id="price"></label>
				</span>
        </div>
        <br>
        <br>
        <br>
			<div class="col-md-12 text-center">
				<br>
			<button type="button" class="btn btn-primary" name="calcular_r" id="calcular_r">Calcular</button>
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Pagar</button>
		</div>
		</div>
	</form>
	</div>
</div>
<script>
      $(document).ready(function(){
          $('#Curso').select2();
          $('#Estudiante').select2();
          $('#Descuento').select2();
          $('#Pago').select2();
       });
	</script>
	<script>
		$(document).ready(function(){
    $('#calcular_r').click(function(){
		let aument;
        let curso = $('#Curso').val();
        let student = $('#Estudiante').val();
		let discount = $('#Descuento').val();
		if(document.querySelector('input[name="no_aument"]:checked')) {
                aument = 1;
            }

        if(curso.length === 0){
            missingText('Seleccione el pago correspondiente')
        }else if(student.length === 0){
            missingText('Seleccione el estudiante correspondiente')
        }else if(discount.length === 0){
            missingText('Seleccione el descuento correspondiente')
        }else{
            calcular();
        }

        function calcular(){
            $.ajax({
                type: 'GET',
                url: "c",
                data:{
                    _token: $("meta[name=csrf-token]").attr("content"),
                    curso:curso,
                    student:student,
					discount:discount,
					aument:aument
                },
                success: function(data){
                    if (data.errors){
                        faltante();
                        //missingText('Algo salio mal');
                    }
                    else{
                        var json = JSON.parse(JSON.stringify(data));
                        document.getElementById("price").innerHTML = data.price;
                        correcto();
                    }
                }
            });
        }
    });
});

function correcto() {
    swal({
        title: "Calculo realizado",
        type: "success",
        timer: 2000,
showCancelButton: false, // There won't be any cancel button
showConfirmButton: false // There won't be any confirm button
})
    .catch(swal.noop);
}

function faltante() {
    swal({
        title: "¡Espera!",
        text: "Faltan datos por ingresar",
        imageUrl: "../img/icon-lte-09.png",
        imageWidth: 200,
        imageHeight: 200,
        timer: 2000,
        showCancelButton: false,
        showConfirmButton: false
    }).catch(swal.noop);
}
	</script>
@endsection