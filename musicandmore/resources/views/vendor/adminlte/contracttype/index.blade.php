@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
<style>
#text_contrato{
	height: 500px;
	overflow-y: scroll;
	overflow-x:hidden;
}
.principal-container 
{ 
	width: 100%; 
} 

.principal-container > div > label 
{ 
	text-align: left; 
	display: block; 
	padding: 1.1em 1.1em .6em; 
	font-size: 1em; 
	color: #f5216e; 
	cursor: pointer; 
} 
.principal-container > [class^='col-'] {  
	border: 1px solid #E4E4E4;  
	height: 6em; 
}   
.ok, .principal-container > div > label > input, .principal-container > div > label > select, .principal-container > div > label > center > input 
{ 
	display: inline-block; 
	position: relative; 
	width: 100%; 
	margin: 5px -5px 0; 
	padding: 7px 5px 3px; 
	border: none; 
	outline: none; 
	background: transparent; 
	font-size: 1.3em; 
	color: #000; 
} 
.principal-container > div > label > center > input{ 
	width: 3.25em; 
} 
@media screen and (max-width: 480px) { 
	.principal-container > div > label{ 
		font-size: .8em; 
	} 
	.ok, .principal-container > div > label > input, .principal-container > div > label > select, .principal-container > div > label > center > input{ 
		font-size: 1.1em; 
	}   
}
</style>
@section('main-content')
<div class="container-fluid spark-screen" id="ContractType_IndexContract">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border">
					<h2 class="titleCenter">Contrato</h2>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					@if(Session::has('flash_message'))
					<p class="alert alert-success text-center">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('flash_message') }}
					</p>
					@endif
					<input type="hidden" value="{{ $pasador }}" id="pass_contract"/>
					<div class="col-md-12">
						<p><h3> En esta sección podrás visualizar el contrato.</h3>Deberás revisar los datos personales del aspirante y hacer clic en el botón de aceptar en caso de estar de acuerdo con la información, lo que te generará un archivo PDF descargable el cual deberá firmar y posteriormente deberá subir un archivo con la firma de aceptación del mismo.</p>
					</div>
					
					@if(Session::has('flash_message'))
					<br>
					<div class="row text-center">
						<button class="btn btn-success btn-lg" id="finalizaContrato"><span class="glyphicon glyphicon-ok"></span> FINALIZAR </button>
					</div>
					<br>
					<br>
					@else
					<form action="{{ route('contractupload') }}" method="POST" role="form" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="row col-md-12 text-center upload_div" style="padding-bottom: 2em">
							<div class="col-md-12">
								<p class="text-center error alert alert-danger hidden" id="error_contractfile"></p>
								<b><p class="text-left">Por favor sube tu documento completo con las firmas correspondientes</p></b>
								<h5 style="font-weight: bold; font-size: 15px; color: red; text-align: left">Solo se admiten archivos .pdf </h5>
							</div>
							<div class="col-md-8">

								<input type="file" class="form-control" id="Contractfile" name="Contractfile" val="{{ old('Contractfile') }}" required="true" accept=".pdf">
								<input type="hidden" id="usuario" name="usuario" value="{{ $users->general_personal_data_id }}">
							</div>
							<div class="col-md-4">
								<a href="#exampleModal" data-toggle="modal"><span style="font-size: 2em"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
								<button class="btn btn-danger" type="submit">Subir Contrato</button>
							</div>
						</div>
					</form>
					@endif
					<div class="row col-md-12" >
						<div name="text_contrato" id="text_contrato" class="well">
							@foreach ($temp_structure as $t)	
							@php
							eval("\$t = \"$t\";");
							@endphp
							{!!$t!!}
							<br>
							<br>
							<br>
							<br>
							@endforeach
							
						</div>
						<div class="row text-center">
							<div class="col-md-6">
								
							</div>
							<div class="col-md-6">
								<a href="{{ route('pdf_contract', [ '$usuario' => $users->general_personal_data_id ]) }}" download><button type="button" class="btn btn-success" id="download_btn" >Aceptar</button></a>
							</div>
						</div>
					</div>
				</div>
				<!--box-body-->
			</div>
			<!--.box-->
		</div>
	</div>
</div>
<div class="container-fluid spark-screen">

	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="row">
			<div class="col-md-12">
				<!-- Default box-->
				<div class="modal-dialog" role="document">
					<form action="{{ route('contractupdate') }}" method="POST">
						{{ csrf_field() }}
						<div class="modal-content">
							<div class="box">
								<div class="modal-header">
									<div class="box-header with-border">
										<button type="button" class="close btn btn-lg col-lg-2" style="background-color: black; border-color: black;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 style="color: black" class="modal-title titleCenter" id="exampleModalLabel">Actualización de Datos</h4>
									</div>		
								</div>
								<div class="box-body">
									<ul class="nav nav-tabs">
										<li class="active" style="margin-left: 10%; width: 40%" data-toggle="tooltip" title="Datos Personales">
											<a data-toggle="tab" href="#PersonalDataTab" id="PersonalData">
												<div class="row text-center">
													<i style="color: black" class="fa fa-user-o fa-4x" aria-hidden="true"></i>
												</div>

											</a>
										</li>
										<li style="width: 40%" data-toggle="tooltip" title="Domicilio">
											<a data-toggle="tab" href="#PersonaladdressTab" id="Personaladdress">
												<div class="row text-center">
													<i style="color: black" class="fa fa-address-book fa-4x" aria-hidden="true"></i>
												</div>
											</a>
										</li>
									</ul>
									<div class="tab-content">
										<input type="hidden" id="usuario" name="usuario" value="{{ $users->general_personal_data_id }}">
										
										<div class="tab-pane fade in active" id="PersonalDataTab">
											<div class="form-horizontal panel-body" id="_profile_center">	
												<div class="principal-container">
													<div class="col-lg-6">
														<label>
															Nombres: <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<input type="text" class="form-control" id="first_name_upgrade" name="first_name_upgrade" value="{{ $users->first_name }}" placeholder="Nombres">
														</label>
													</div>
													<div class="col-lg-6">
														<label>
															Apellidos:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<input type="text" class="form-control" id="last_name_upgrade" name="last_name_upgrade" value="{{ $users->last_name }}" placeholder="Apellidos">
														</label>
													</div>
													<div class="col-lg-6">
														<label>
															CURP:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<input type="text" class="form-control" id="curp_upgrade" name="curp_upgrade" value="{{ $users->curp }}" placeholder="Curp" maxlength="18">
														</label>
													</div>
													<div class="col-lg-6">
														<label>
															RFC:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<input type="text" class="form-control" id="rfc_upgrade" name="rfc_upgrade" value="{{ $users->rfc }}" placeholder="RFC" maxlength="13">
														</label>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane fade" id="PersonaladdressTab">
											<div class="form-horizontal panel-body" id="_profile_center">
												<div class="principal-container">
													<div class="col-lg-6">
														<label>Calle:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<input type="text" class="form-control" id="street_upgrade" name="street_upgrade" value="{{ $dir -> street }}" placeholder="Calle">
														</label>
													</div>
													<div class="col-lg-6">
														<label>
															Número Exterior:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<input type="number" class="form-control" id="ext_number_upgrade" name="ext_number_upgrade" value="{{ $dir -> ext_number }}" placeholder="Número Exterior">
														</label>
													</div>
													<div class="col-lg-6">
														<label>
															Número Interoir:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<input type="number" class="form-control" id="int_number_upgrade" name="int_number_upgrade" value="{{ $dir -> int_number }}"  placeholder="Número Interoir">
														</label>
													</div>
													<div class="col-lg-6">
														<label>Fraccionamiento:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<input type="text" class="form-control" id="fraccionamiento" name="fraccionamiento" value="{{ $dir -> fraccionamiento }}" placeholder="Fraccionamiento">
														</label>
													</div>
													<div class="col-lg-6">
														<label>Código Postal:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<input type="text" class="form-control" id="zip_code_upgrade"  name="zip_code_upgrade" value="{{ $dir -> zip_code }}" maxlength="5" placeholder="Código Postal">
														</label>
													</div>
													<div class="col-lg-6">
														<label>Estado:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_jobcenter"></span>
															<select id="Estado_personal" name="Estado_personal" class="form-control" data-live-search="true" required="true" value="{{ $dir->state or old('Estado_personal') }}">
																<option value="">Seleccione una opción</option>
																<option value="Aguascalientes" @if ('Aguascalientes' == $dir->state) selected ='selected' @endif>Aguascalientes</option>
																<option value="Baja California"@if ('Baja California' === $dir->state) selected ='selected' @endif>Baja California</option>
																<option value="Baja California Sur"@if ('Baja California Sur' === $dir->state) selected = 'selected' @endif>Baja California Sur</option>
																<option value="Campeche"@if ('Campeche' == $dir->state) selected = 'selected' @endif>Campeche</option>
																<option value="Coahuila de Zaragoza"@if ('Coahuila de Zaragoza' === $dir->state) selected = 'selected' @endif>Coahuila de Zaragoza</option>
																<option value="Colima"@if ('Colima' == $dir->state) selected = 'selected' @endif>Colima</option>
																<option value="Chiapas"@if ('Chiapas' == $dir->state) selected = 'selected' @endif>Chiapas</option>
																<option value="Chihuahua"@if ('Chihuahua' == $dir->state) selected = 'selected' @endif>Chihuahua</option>
																<option value="Distrito Federal"@if ('Distrito Federal' === $dir->state) selected = 'selected' @endif>Distrito Federal</option>
																<option value="Durango"@if ('Durango' == $dir->state) selected = 'selected' @endif>Durango</option>
																<option value="Guanajuato"@if ('Guanajuato' == $dir->state) selected = 'selected' @endif>Guanajuato</option>
																<option value="Guerrero"@if ('Guerrero' == $dir->state) selected = 'selected' @endif>Guerrero</option>
																<option value="Hidalgo"@if ('Hidalgo' == $dir->state) selected = 'selected' @endif>Hidalgo</option>
																<option value="Jalisco"@if ('Jalisco' == $dir->state) selected = 'selected' @endif>Jalisco</option>
																<option value="México"@if ('México' == $dir->state) selected = 'selected' @endif>México</option>
																<option value="Michoacán de Ocampo"@if ('Michoacán de Ocampo' === $dir->state) selected = 'selected' @endif>Michoacán de Ocampo</option>
																<option value="Morelos"@if ('Morelos' == $dir->state) selected = 'selected' @endif>Morelos</option>
																<option value="Nayarit"@if ('Nayarit' == $dir->state) selected = 'selected' @endif>Nayarit</option>
																<option value="Nuevo León"@if ('Nuevo León' === $dir->state) selected = 'selected' @endif>Nuevo León</option>
																<option value="Oaxaca"@if ('Oaxaca' == $dir->state) selected = 'selected' @endif>Oaxaca</option>
																<option value="Puebla"@if ('Puebla' == $dir->state) selected = 'selected' @endif>Puebla</option>
																<option value="Querétaro"@if ('Querétaro' == $dir->state) selected = 'selected' @endif>Querétaro</option>
																<option value="Quintana Roo"@if ('Quintana Roo' === $dir->state) selected = 'selected' @endif>Quintana Roo</option>
																<option value="San Luis Potosí"@if ('San Luis Potosí' === $dir->state) selected = 'selected' @endif>San Luis Potosí</option>
																<option value="Sinaloa"@if ('Sinaloa' == $dir->state) selected = 'selected' @endif>Sinaloa</option>
																<option value="Sonora"@if ('Sonora' == $dir->state) selected = 'selected' @endif>Sonora</option>
																<option value="Tabasco"@if ('Tabasco' == $dir->state) selected = 'selected' @endif>Tabasco</option>
																<option value="Tamaulipas"@if ('Tamaulipas' == $dir->state) selected = 'selected' @endif>Tamaulipas</option>
																<option value="Tlaxcala"@if ('Tlaxcala' == $dir->state) selected = 'selected' @endif>Tlaxcala</option>
																<option value="Veracruz de Ignacio de la Llave"@if ('Veracruz de Ignacio de la Llave' === $dir->state) selected = 'selected' @endif>Veracruz de Ignacio de la Llave</option>
																<option value="Yucatán"@if ('Yucatán' == $dir->state) selected = 'selected' @endif>Yucatán</option>
																<option value="Zacatecas"@if ('Zacatecas' == $dir->state) selected = 'selected' @endif>Zacatecas</option>
															</select>
														</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--box-body-->
								</div>
								<div class="modal-footer">
									<div class="text-center">
										<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection