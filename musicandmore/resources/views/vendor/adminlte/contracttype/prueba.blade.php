

<!--
Contrato 5 
<div class='text-justify'>
	<p class='text-right'><b>México, Distrito Federal a {$fecha}</b></p>
	<br>
	<h4 class='text-center'><b>CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V.</b><br>PLAN DE PREVISIÓN SOCIAL</h4>
	<p>Se establece el presente PLAN MÚLTIPLE DE PRESTACIONES DE PREVISIÓN SOCIAL para los trabajadores de la CONSORCIO EMPRESARIAL ALMIR, S.A.  DE C.V., mismo que será otorgado a todo el personal como principio de generalidad, tomando en consideración lo dispuesto por la Ley del Impuesto Sobre la Renta y su Reglamento.</p>
	<p><ins><b>Objetivos:</b></ins></p>
	<p>
		<b>a)</b> Contribuir al bienestar de los empleados de la <b>CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V.</b> y de sus familiares,<br>
		<b>b)</b> Evitar la reducción del salario real,<br>
		<b>c)</b> Introducir las técnicas más avanzadas para la administración del personal, entregando incentivos a los trabajadores,<br>
		<b>d)</b> Estimular el desarrollo personal de los beneficiarios del presente plan,<br>
		<b>e)</b> Proteger el patrimonio familiar de los empleados,<br>
		<b>f)</b> Fomentar las actividades culturales y deportivas para los empleados y sus familias,<br>
		<b>g)</b> En general mejorar las remuneraciones del personal que presta sus servicios en la empresa y su calidad de vida.<br>
	</p>
	<p><ins><b>Contenido:</b></ins></p>
	<p>El plan de previsión social que se establece comprende lo siguiente: <br>
		<b>1. </b>Alto costo de la vida del trabajador<br>
		<b>2. </b>Actividades culturales<br>
	</p>
	<p><ins><b>Fundamento:</b></ins></p>
	<p>El presente Plan de Previsión Social encuentra su marco legal y fundamento en las siguientes disposiciones legales vigentes: <br>
		<p><b>A.- Artículo 123 Constitucional Apartado A.- </b><i>“Toda persona tiene derecho al trabajo digno y socialmente útil; al efecto, se promoverán la creación de empleos y la organización social de trabajo...”</i></p>

		<p><b>B.- Artículo 86 de la Ley Federal del Trabajo.- </b><i>“El salario se integra con los pagos hechos en efectivo por cuota diaria, gratificaciones, percepciones, habitación, primas, comisiones, prestaciones en especie y cualquiera otra cantidad o prestación que se entregue al trabajador por su trabajo.”</i></p>

		<p><b>C.- Artículo 1410 de la Ley Federal del Trabajo.- </b><i>“Para los efectos de este Capítulo el salario a que se refiere el artículo 136 se integra con los pagos hechos en efectivo por cuota diaria, y las gratificaciones, percepciones, alimentación, habitación, primas, comisiones, prestaciones en especie y cualquier otra cantidad o prestación que se entregue al trabajador por sus servicios; no se tomarán en cuenta dada su naturaleza, los siguientes conceptos: <br>a) Los instrumentos de trabajo, tales como herramientas, ropa y otros similares;  <br> b) El ahorro, cuando se integre por un depósito de cantidad semanaria o mensual igual del trabajador y de la empresa; y las cantidades otorgadas por el patrón para fines sociales o sindicales; <br> c) Las aportaciones al Instituto de Fondo Nacional de la Vivienda para los Trabajadores y las participaciones en las utilidades de las empresas; <br> d) La alimentación y la habitación cuando no se proporcionen gratuitamente al trabajador, así como las despensas; <br> e) Los premios por asistencia; <br> f) Los pagos por tiempo extraordinario, salvo cuando este tipo de servicios esté pactado en forma de tiempo fijo; <br> g) Las cuotas al Instituto Mexicano del Seguro Social a cargo del trabajador que cubran las empresas.”</i></p>

		<p><b>D.- Artículo 7 quinto párrafo de la Ley del Impuesto Sobre la Renta.- </b><i>“Para los efectos de esta ley, se considera previsión social las erogaciones efectuadas que tengan por objeto satisfacer contingencias o necesidades presentes o futuras, así como el otorgar beneficios a favor de los trabajadores o de los socios o miembros de las sociedades cooperativas, tendientes a su superación física, social, económica o cultural, que les permitan el mejoramiento en su calidad de vida y en la de su familia. En ningún caso se considerará previsi ón social a las erogaciones efectuadas a favor de personas que no tengan el carácter de trabajadores o de socios o miembros de sociedades cooperativas.”</i></p>
	</p>
	<p>Bajo ese tenor, en la Ciudad de México, Distrito Federal, siendo el día 6 de  Junio de 2017, suscriben el presente PLAN DE PREVISIÓN SOCIAL en beneficio de los trabajadores de la <b>CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V.</b>, la cual en lo sucesivo será denominada EL PATRÓN, y cada uno de sus empleados.</p>
	<p>Ambas partes suscriben el presente Plan de Previsión Social al amparo de las siguientes:</p>
	<p class='text-center'><b><ins>DISPOSICIONES</ins></b></p>
	<p><b>PRIMERA.- DEFINICIONES</b><br>
		Cuando en este Plan de Previsión Social se haga referencia a los siguientes conceptos, deberá entenderse lo que se define en este plan y, en caso de duda deberá de interpretarse de la manera más acorde con la naturaleza y finalidad del mismo. <br>
		<br>
		Para el caso del presente instrumento se entiende por:
		<br>
		<b>PLAN.- </b>El plan de Prestaciones de Previsión Social otorgado y constituido conforme a este documento, así como a los estatutos o instructivos que al efecto se consignen. <br>
		<b>PRESTACIONES.- </b>Cada uno de los diferentes tipos de beneficios que puede obtener el trabajador, en los términos y condiciones que se establecen en este plan. <br>
		<b>PATRÓN.- </b>De conformidad con el artículo 10 de  la Ley Federal del Trabajo, se entiende por patrón a la persona física o moral que utiliza los servicios de uno o varios trabajadores. En el caso del presente instrumento, tiene la calidad de Patrón la empresa <ins>CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V.</ins>, tal como se señaló con anterioridad.<br>
		<b>TRABAJADOR.- </b>De conformidad con el artículo 10  de la Ley Federal del Trabajo, se entiende por trabajador toda persona física que prestea otra, física o moral, un trabajo personal subordinado. <br>
		<b>TRABAJADOR ACTIVO.- </b>Para el caso del presente plan, se considera al trabajador activo como beneficiario del mismo, considerándose que se encuentra activo el trabajador que no se encuentre suspendido, tenga permisos o licencias sin goce de sueldo. En caso de i ncapacidad, por cualquier causa, se considerará como trabajador activo sólo para efectos de las prestaciones que concede este Plan. <br>
		<b>SALARIO.- </b>Cantidad que el trabajador reciba a cambio de su trabajo ordinario, retribución cubierta por la <ins>CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V.</ins> <br>
		<b>SHCP.-</b> Secretaría de Hacienda y Crédito Público. <br>
		<b>EJERCICIO FISCAL.- </b>Periodo comprendido del 1 de enero al 31 de  diciembre de cada año.
	</p><br>
	<p><b>SEGUNDA.- VIGENCIA</b><br>
		La vigencia de este Plan de Prestaciones de Previsión Social será del 1º de enero al 31 de  diciembre  de 2017, pudiendo en su transcurso ser reformado o adicionado por la partes signantes. <br>Asimismo, podrá ser ratificado anualmente para mantener su vigencia.
	</p><br>
	<p><b>TERCERA.- OBJETO</b><br>
		El objeto de este Plan es dar cabal cumplimiento a los principios y postulados que sobre el particular se señalan en la Constitución Política de los Estados Unidos Mexicanos y en la Ley Federal del Trabajo, en lo referente a la seguridad y a la previsión social, así como estimular y mejorar la relación obrero-patronal. <br><br>También tendrá como propósito fundamental, elevar el nivel de vida social e integral de los trabajadores y de sus familias, así como prever aquellas contingencias que puedan demeritar su nivel de vida o su patrimonio familiar.
	</p><br>
	<p><b>CUARTA.- BENEFICIARIOS</b><br>
		Los beneficiarios de este Plan, son los “trabajadores activos” de la empresa <b>CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V.</b>, de manera general, cualquier que sea su categoría, nivel o puesto. <br><br>Se entiende como TRABAJADOR ACTIVO aquél que en términos de las definiciones del presente Plan, no se encuentre suspendido y sin goce de sueldo a causa de permisos o licencias sin goce de sueldo y que tampoco haya dejado de prestar su servicio activo a l a empresa <b>CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V.</b>
	</p><br>
	<p><b>QUINTA.- BENEFICIOS</b><br>
		<p style='padding-left: 3em'><ins >1. ALTO COSTO DE LA VIDA DEL TRABAJADOR</ins><br><br>
			EL PATRÓN entregará al trabajador por concepto de “Alto Costo de la Vida del Trabajador” un importe que consistirá en hasta $5,919.00 (cinco mil novecientos diecinueve pesos 00/100 m.n.), con el objeto de mejorar las condiciones de vida y económicas del trabajador, tomando en cuenta que el ingreso que perciba todo empleado debe ser remunerador y suficiente para satisfacer las necesidades económicas normales de un Jefe de Familia.
		</p><br>
		<p style='padding-left: 3em'><ins >2. ACTIVIDADES CULTURALES</ins><br><br>
			La empresa <b>CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V.</b> otorgará el presente beneficio para lograr el objetivo de elevar el nivel de vida del trabajador, tanto de él como de su familia, promoviendo actividades culturales en conjunto o separadamente de su sindicato.
		</p>
	</p><br>
	<p><b>SEXTA.- </b>
		La interpretación de este Plan será atendiendo a la naturaleza y finalidad de sus objetivos, y de manera supletoria se aplicarán las disposiciones relativas y aplicables vigentes.
	</p><br>
	<p><b>SÉPTIMA.- </b>
		Las partes se someten expresamente a la jurisdicción de las Juntas de Conciliación y Arbitraje del Distrito Federal, las cuales serán las únicas competentes para conocer de las controversias que se susciten; por tal motivo los beneficiarios renuncian desde este momento a cualquier otro fuero que por razón de domicilio les pudiera corresponder para el ejercicio de cualquier acción legal.
	</p><br>
	<br>
	<br>
	<br>
	<br>
	<hr style='width: 250px; height: 1px; background-color: black'>
	<p class='text-center'>
		<b>Por: CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V.<br>ARTURO ESPINOSA SUAREZ</b>
	</p>
	<br>
	<br>
	<br>
	<br>
	<br>
	<hr style='width: 250px; height: 1px; background-color: black'>
	<p class='text-center'>
		<b>C. {$user->name}</b>
	</p>
</div> -->

<!-- 
Contrato 6
<div class='text-justify'>
	<p class='text-right'><b>México, Aguascalientes a {$fecha}</b></p>
	<br>
	<h4 class='text-center'><b>ORDEN DE PAGO POR MEDIO DE MI NÓMINA</b></h4>
	<p><b>AT’N REPRESENTANTE LEGAL Y/O DEPARTAMENTO DE RECURSOS HUMANOS DE CONSORCIO EMPRESARIAL ALMIR S.A DE C.V CON DOMICILIO EN LAGO MANITOBA NUM 9 INT 4 AMPLIACION GRANADA MIGUEL HIDALGO C.P 11529 DISTRITO FEDERAL <br> P R E S E N T E.-</b></p>

	<p>El suscrito {$user->name}en mi carácter de empleado de la moral CONSORCIO EMPRESARIAL ALMIR, S.A. DE C.V., manifiesto y expongo:</p>
	<p>Por medio de la presente autorizó a mi patrón para que realice los descuentos salariales de manera mensual vía Nómina, en términos de lo que establece la fracción primera del artículo 136 de la Ley Federal del Trabajo, en caso de que dentro de mis responsabilidades como trabajador incurra en los siguientes supuestos:</p>
	<p>1. Que durante un inventario de mercancía, el faltante sea Junior al 1% que se tiene como tolerancia.</p>
	<p>2. Que durante un corte de caja, exista faltante de efectivo.</p>
	<p>Dichos descuentos salariales deberán ser entregados de manera mensual a moral <b>EK CONVENIENCIA, S.A. DE C.V.</b>, en su domicilio o en la cuenta bancaria que esta última designe y hasta que cubra el total de deuda que tengo con dicha empresa y que no pude exceder de un mes de salario, tal y como lo establece el referido artículo.</p>
	<p>La presente la realizo expresa y voluntariamente, solicitando a mi patrón me auxilie en el cumplimiento de mis adeudos con la sociedad denominada <b>EK CONVENIENCIA, S.A DE C.V</b>, razón por la cual no me reservo acción ni derecho alguno que ejercitar en contra de mi único patrón, la citada empresa ni de quién sus intereses representen.</p>
	<p style='font-size: .7em'>...ARTÍCULO 110.- Los descuentos en los salarios de los trabajadores, están prohibidos salvo en los casos y con los requisitos siguientes:...<br> ...I. Pago de deudas contraídas con el patrón por anticipo de salarios, pagos hechos con exceso al trabajador, errores, pérdidas, averías o adquisición de artículos producidos por la empresa o establecimiento. La cantidad exigible en ningún caso podrá ser Junior del importe de los salarios de un mes y el descuento será el que convengan el trabajador y el patrón, sin que pueda ser Junior del treinta por ciento del excedente del salario mínimo;...</p>
	<p>Agradezco de antemano la atención inmediata a la presente.<p>
	<br>
	<br>	
	<br>
	<br>
	<br>
	<hr style='width: 250px; height: 1px; background-color: black'>
	<p class='text-center'>
		<b>C. {$user->name}</b>
	</p>
</div> -->
		

<!-- 
Contrato 4
<div class='text-justify'> 
	<p class='text-right'><b>México, Distrito Federal a {$fecha}</b></p>
	<br> 
	<h4 class='text-center'>RECURSOS CON RENTABILIDAD Y VISION DE FUTURO MACBEY SA DE CV</h4> 
	<br> 
	<p>Quien suscribe, <b>{$user->name}</b>, con domicilio fiscal en <b>{$direccion}</b>, Registro federal de contribuyente <b>{$RFC}</b>, y CURP {$CURP}, en atención a lo establecido en el artículo 110, fracción V de la Ley del Impuesto Sobre la Renta vigente, me dirijo a usted para solicitarle que a partir de esta fecha se asimilen en los términos de dicha disposición legal en materia fiscal a salarios, los honorarios que ustedes me cubren por la prestación de mis servicios profesionales independientes, en virtud de que he optado por pagar el impuesto en los términos del Capitulo l del Título IV de la Ley del Impuesto sobre la Renta.</p> 
	<br> 
	<p>De acuerdo a lo que solicito en la presente, hago manifiesto ante cualquier tipo de autoridad del orden público o privado que tal hecho no representa de ninguna manera un cambio en la relación jurídica profesional entre esa empresa y quien suscribe la presente, ya que sigue sin existir subordinación o dependencia y tampoco se me cubre salario alguno, en consecuencia esta se encuentra regida por las leyes civiles. Razón por la cual manifiesto por mi propio derecho y voluntad que relevo a la empresa de cualquier responsabilidad legal que pudiere surgir derivada de la petición expresa mía, en relación con mi solicitud de que se de tratamiento fiscal a mis ingresos por honorarios como si fueran salarios, dado que tal opción la ejerzo en base a disposición legal que expresamente así lo permite y por así convenir de mis intereses, profesionales y patrimoniales, tomando el compromiso de ratificar en el mismo sentido de lo que expongo en la presente mi decisión ante cualquier tipo de autoridad de los tres niveles de gobierno que así lo solicite.</b></p> 
	<br><br> 
	<p class='text-center'><b>A t e n t a m e n t e,</b></p> 
	<br> 
	<br> 
	<hr style='width: 30%; height: 1px; background-color: black'> 
	<p class='text-center'><b>{$user->name}</b></p> 
</div> -->

<!-- 
Contrato 3
<div class='text-justify'> 
	<p class='text-right'><b>México, Distrito Federal a {$fecha}</b></p> 
	<br> 
	<h4 class='text-center'>SISTEMAS DE INVESTIGACION Y ADMINISTRACION S.A DE C.V</h4> 
	<br> 
	<p>Quien suscribe, <b>{$user->name}</b>, con domicilio fiscal en <b>{$direccion}</b>, Registro federal de contribuyente <b>{$RFC}</b>, y CURP <b>{$CURP}</b>, en atención a lo establecido en el artículo 110, fracción V de la Ley del Impuesto Sobre la Renta vigente, me dirijo a usted para solicitarle que a partir de esta fecha se asimilen en los términos de dicha disposición legal en materia fiscal a salarios, los honorarios que ustedes me cubren por la prestación de mis servicios profesionales independientes, en virtud de que he optado por pagar el impuesto en los términos del Capitulo l del Título IV de la Ley del Impuesto sobre la Renta.</p> 
	<br> 
	<p>De acuerdo a lo que solicito en la presente, hago manifiesto ante cualquier tipo de autoridad del orden público o privado que tal hecho no representa de ninguna manera un cambio en la relación jurídica profesional entre esa empresa y quien suscribe la presente, ya que sigue sin existir subordinación o dependencia y tampoco se me cubre salario alguno, en consecuencia esta se encuentra regida por las leyes civiles. Razón por la cual manifiesto por mi propio derecho y voluntad que relevo a la empresa de cualquier responsabilidad legal que pudiere surgir derivada de la petición expresa mía, en relación con mi solicitud de que se de tratamiento fiscal a mis ingresos por honorarios como si fueran salarios, dado que tal opción la ejerzo en base a disposición legal que expresamente así lo permite y por así convenir de mis intereses, profesionales y patrimoniales, tomando el compromiso de ratificar en el mismo sentido de lo que expongo en la presente mi decisión ante cualquier tipo de autoridad de los tres niveles de gobierno que así lo solicite.</b></p> 
	<br><br> 
	<p class='text-center'><b>A t e n t a m e n t e,</b></p> 
	<br>
	<br> 
	<hr style='width: 30%; height: 1px; background-color: black'> 
	<p class='text-center'><b>{$user->name}</b></p> 
</div> -->


<!-- Contrato 2
<div class='text-justify'> 
	<p class='text-right'><b>México, Distrito Federal a {$fecha}</b></p> 
	<br> 
	<p><b>C. {$user->name} <br> P R E S E N T E .</b></p> 
	<br> 
	<p>Por medio de la presente, en mi calidad de representante del órgano de administración de esta sociedad, me permito hacer de su conocimiento que por acuerdo tomado en Asamblea General de Socios, se decidió admitirlo a partir de la emisión de la presente, como Socio Industrial de esta Sociedad Civil (O MERCANTIL) denominada <b>CONJUGACION EMPRESARIAL Y DINAMISMO DE EMPRENDEDORES S.C.</b></p> 
	<br> 
	<p>Sírvase por tanto conocer todos los derechos que tendrá a partir de esta fecha, así como las obligaciones con las que deberá cumplir y que se contienen en los Estatutos y/o Políticas (Códigos) de la Sociedad, así como sus respectivas modificaciones, debiendo ajustarse a los lineamientos que son establecidos por dichos cuerpos normativos de carácter obligatorio para todos los socios.</p> 
	<br> 
	<p>En este sentido, también le comunico que a partir de esta fecha, usted quedará como Socio Industrial en el Libro de Registro de Socios aportando su industria para desarrollar actividades como parte integrante de esta sociedad en los lugares y clientes asignados por esta agrupación profesional.</p> 
	<br><br> 
	<p class='text-center'><b>A t e n t a m e n t e,</b></p> 
	<br> 
	<br> 
	<hr style='width: 30%; height: 1px; background-color: black'> 
	<p class='text-center'><b>SILVIA ELENA CARRASCO NAVARRETE <br> SOCIO ADMINISTRADOR</b></p> 
	<br> 
	<br> 
	<div class='text-center' style='margin-left: 66%'> 
		<p><b>Firma de recibido</b></p> 
		<br> 
		<br> 
		<hr style='height: 1px; background-color: black'> 
		<p><b>{$user->name}</b></p> 
	</div> 
</div>  -->

<div class='text-justify'> 
	<p class='text-right'><b>México, Aguascalientes a {$fecha}</b></p> 
	<br> 
	<p><b>Administrador Único, <br> CONJUGACION EMPRESARIAL Y DINAMISMO DE EMPRENDEDORES S.C <br> P R E S E N T E.</b></p> <br> <br> 
	<p >Por medio de este conducto, hago de su conocimiento, que es de mi interés incorporarme a la Sociedad Civil (O MERCANTIL) denominada <b>CONJUGACION EMPRESARIAL Y DINAMISMO DE EMPRENDEDORES S.C</b>, en calidad de Socio Industrial para aportar mi industria.</p> 
	<br> 
	<p>Esta petición la realizó una vez que he conocido y reunido todos y cada uno de los requisitos que se contemplan en los estatutos y políticas de la sociedad, así como de haber presentado la documentación que se me requirió.</p> 
	<br> 
	<p>Por tanto, desde este momento manifiesto mi conformidad a respetar los estatutos de la Sociedad, así como las normas y políticas vigentes establecidas por el órgano administrador de la misma, en caso de ser admitida mi incorporación. Asimismo, reconozco y admito que la relación que, en caso de ser aceptado, me vinculará a esta personal moral, será única y exclusivamente de carácter civil, por lo que en caso de presentarse cualquier contradicción en el futuro que deba resolverse vía jurisdiccional, será la autoridad en materia civil la competente para conocerlo.</p> 
	<br>
	<br> 
	<br> 
	<br> 
	<br> 
	<p class='text-center'><b>A t e n t a m e n t e</b></p> 
	<br> 
	<br> 
	<hr style='width: 200px; background-color: black; height: 1px'/>
	<p class='text-center'><b>{$user -> name }</b></p>
</div>