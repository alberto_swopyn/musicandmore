<div id="experiences" class="tab-pane fade in active" style="overflow-y: scroll; overflow-x:hidden; height: 35em;" >
	<div class="col-md-12">
		<label>Selecciona al solicitante presente</label>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_interview_user"></p></b>
			<select name="interview_applicant" id="interview_applicant" class="form-control">
				<option value="">Selecciona uno..</option>
				@foreach ($applicants as $applicant)
				{{ $name = $applicant -> first_name . ' ' . $applicant -> last_name }}
				<option value="{{ $applicant-> id }}">{{ $name }}</option>
				@endforeach
			</select>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 1</h5> <h6>Manejo de clientes difíciles</h6>
		<p style="border: 1px solid #000000; text-align: center">Una parte importante del puesto, es brindar atención y excelente servicio a los clientes. Háblame de la última vez en la que tuviste que atender a un cliente difícil.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Qué ocurrió? <br>
			- ¿Por qué podrías decirme que se trataba de un cliente difícil? <br>
			- ¿Lograste realizar la venta? <br>		
			- ¿Qué aprendiste de lo que ocurrió? <br>
			- ¿Qué hubieras hecho diferente? <br>
			- ¿Te ha ocurrido algo similar? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_exp_ask_1"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask1" id="exp_ask1" type="radio" value="1">1</label></td>
						<td>No supo cómo manejar la situación.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask1" id="exp_ask1" type="radio" value="2">2</label></td>
						<td>Las experiencias le han servido para mejorar su atención y servicio.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask1" id="exp_ask1" type="radio" value="3">3</label></td>
						<td>El cliente se fue satisfecho y logró la venta.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 2</h5> <h6>Manejo de recursos económicos</h6>
		<p style="border: 1px solid #000000; text-align: center">En tu anterior trabajo, manejaste dinero en efectivo.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Estás acostumbrado a realizar depósitos en bancos? <br>
			- ¿Tienes experiencia en realizar cortes de caja? <br>				
			- ¿Alguna vez te ha hecho falta dinero en tus cortes? <br>				
			- ¿Te sientes cómodo manejando dinero? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_exp_ask_2"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask2" id="exp_ask2" type="radio" value="1">1</label></td>
						<td>No está acostumbrado a manejar dinero.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask2" id="exp_ask2" type="radio" value="2">2</label></td>
						<td>No se siente cómodo manejando dinero.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask2" id="exp_ask2" type="radio" value="3">3</label></td>
						<td>Tiene experiencia en el manejo de dinero y en realizar cortes de caja.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 3</h5> <h6>Empleo de estrategias para fomentar las ventas</h6>
		<p style="border: 1px solid #000000; text-align: center">Una faceta importante en el puesto es la venta de discos e instrumentos músicales. Háblame de las estrategías que utilizabas en tu anterior empleo para abordar a un cliente.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Qué es lo primero de haces cuando ingresa un cliente potencial? <br>
			- ¿Cómo detectas sus necesidades? <br>
			- ¿Qué tipo de preguntas le formulas? <br>
			- ¿Le indicas las promociones que tiene la tienda? <br>
			- Cuando te preguntan algo que no conoces, ¿qué acciones emprendes? <br>
			- ¿Le das su "espacio" al cliente o te mantienes "cerca" del mismo? <br>
			- ¿Le indicas las diferentes formas de pago? <br>			
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_exp_ask_3"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask3" id="exp_ask3" type="radio" value="1">1</label></td>
						<td>No tiene un protocolo para abordar clientes.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask3" id="exp_ask3" type="radio" value="2">2</label></td>
						<td>Tiene un protocolo y sabe detectar las necesidades del cliente.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask3" id="exp_ask3" type="radio" value="3">3</label></td>
						<td>Tiene un protocolo, sabe detectar las necesidades del cliente y solucionar situaciones de las que no conoce. Asimismo, tiene conocimiento de las promociones y formas de pago que la tienda puede ofrecer.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 4</h5> <h6>Habilidad para resolver conflictos con pares</h6>
		<p style="border: 1px solid #000000; text-align: center">Me podrías describir alguna situación en la que hayas tenido un conflicto con algún compañero de trabajo. ¿Cómo lo resolviste?</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Qué ocurrió? <br>
			- ¿Cómo reaccionaste? ¿Cómo reaccionó tu compañero? <br>
			- ¿Cuánto tiempo duró el conflicto? <br>
			- ¿Tuvo que intervenir su jefe inmediato para resolverlo? <br>
			- ¿Ese fue el motivo de tu salida del empleo? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_exp_ask_4"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask4" id="exp_ask4" type="radio" value="1">1</label></td>
						<td>No resolvió el conflicto y tiene malas relaaciones interpresonales.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask4" id="exp_ask4" type="radio" value="2">2</label></td>
						<td>Busca la ayuda de un superior para resolver conflictos con sus pares.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask4" id="exp_ask4" type="radio" value="3">3</label></td>
						<td>Es capaz de resolver los conflictos por sí mismo.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 5</h5> <h6>Manejo de responsabilidades</h6>
		<p style="border: 1px solid #000000; text-align: center">¿Cuáles son las funciones de mayor responsabilidad que tuviste?</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Manejo de caja chica? <br>
			- ¿Manejo de personal? <br>
			- ¿Manejo de invetario? <br>
			- ¿Apertura y cierre de tienda? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_exp_ask_5"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask5" id="exp_ask5" type="radio" value="1">1</label></td>
						<td>No ha tenido responsabilidades.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask5" id="exp_ask5" type="radio" value="2">2</label></td>
						<td>Medianamente, ha tenido responsabilidad.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask5" id="exp_ask5" type="radio" value="3">3</label></td>
						<td>Ha tenido responsabilidades.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 6</h5> <h6>Manejo y resolución de problemas típicos</h6>
		<p style="border: 1px solid #000000; text-align: center">¿Cuáles fueron los problemas típicos en tu anterior empleo? Hábleme de alguno en específico.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Qué ocurria? <br>
			- ¿Cómo se resolvió? <br>
			- ¿Cuál fue tu rol en la resolución del problema? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_exp_ask_6"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask6" id="exp_ask6" type="radio" value="1">1</label></td>
						<td>No se involucra en la resolución de los problemas cotidianos.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask6" id="exp_ask6" type="radio" value="2">2</label></td>
						<td>Sólo se involucra en la resolución sí se solicita.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask6" id="exp_ask6" type="radio" value="3">3</label></td>
						<td>Se involucra de manera activa en la resolución de problemas.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 7</h5> <h6>Iniciativa para proponer mejoras</h6>
		<p style="border: 1px solid #000000; text-align: center">¿Alguna vez identificaste algún aspecto que pudiera mejorarse en tu anterior trabajo y realizaste la propuesta correspondiente.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Qué ocurria? <br>
			- ¿En qué consistió? <br>
			- ¿Cómo fue tomada la propuesta por tu jefe inmediato? <br>
			- ¿Cómo ayudó a mejorar? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_exp_ask_7"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask7" id="exp_ask7" type="radio" value="1">1</label></td>
						<td>No se involucra en la resolución de los problemas cotidianos.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask7" id="exp_ask7" type="radio" value="2">2</label></td>
						<td>Sólo se involucra en la resolución sí se solicita.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="exp_ask7" id="exp_ask7" type="radio" value="3">3</label></td>
						<td>Se involucra de manera activa en la resolución de problemas.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row text-center">
		<button class="btn-default btn" style="background-color: black; border-color: black;" id="save_experience" name="save_experience">Guardar</button>
	</div>
</div>