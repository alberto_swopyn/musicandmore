<div id="organization_order" class="tab-pane fade" style="overflow-y: scroll; overflow-x:hidden; height: 35em;" >
	<div class="col-md-12">
		<h5>Pregunta 1</h5> <h6>Capacidad de orden y organización</h6>
		<p style="border: 1px solid #000000; text-align: center">Una parte importante del puesto es mantener organizado y limpio el centro de trabajo. Con relación a dichas actividades, indícame en qué momento las llevarías a cabo.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Al inicio de las operaciones de la tienda? <br>
			- ¿Cada vez que lo solicite el jefe inmediato? <br>
			- ¿Al finalizar las operaciones de la tienda? <br>
			- ¿En todo momento, siempre hay algo que hacer? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_order_organization_ask_1"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask1" id="org_order_ask1" type="radio" value="1">1</label></td>
						<td>No tiene una actitud proactiva.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask1" id="org_order_ask1" type="radio" value="2">2</label></td>
						<td>Requiere que el jefe inmediato le indique qué hacer y en qué momento.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask1" id="org_order_ask1" type="radio" value="3">3</label></td>
						<td>Prefiere mantener una actitud proactiva.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 2</h5> <h6>Capacidad para construir un plan de trabajo</h6>
		<p style="border: 1px solid #000000; text-align: center">¿Cómo organizabas tus actividades en el anterior puesto de trabajo?</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Te suministraban un plan de trabajo? <br>
			- ¿Tu elaborabas tu plan de trabajo? <br>
			- ¿Te sentías cómodo siguiendo un plan de trabajo? ¿Prefieres elaborar tu propio plan de trabajo? <br>
			- ¿Cómo se distribuían tus actividades en una semana laboral? <br>			
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_order_organization_ask_2"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask2" id="org_order_ask2" type="radio" value="1">1</label></td>
						<td>Prefiere que se le suministre un plan de trabajo.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask2" id="org_order_ask2" type="radio" value="2">2</label></td>
						<td>Prefiere participar medianamente en la elaboración del plan de trabajo.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask2" id="org_order_ask2" type="radio" value="3">3</label></td>
						<td>Prefiere tener la libertad de construir su plan de trabajo.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 3</h5> <h6>Capacidad de discriminación de actividades</h6>
		<p style="border: 1px solid #000000; text-align: center">Podrías indicarme el orden de importancia de las siguientes actividades:  el cliente, acomodar la mercancia y mantener limpio el centro de trabajo.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Qué orden elegirías?
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_order_organization_ask_3"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask3" id="org_order_ask3" type="radio" value="1">1</label></td>
						<td>Mantener limpio el centro de trabajo o acomodar la mercancia y el cliente.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask3" id="org_order_ask3" type="radio" value="2">2</label></td>
						<td>El cliente, acomodar la mercancia y mantener limpio el centro de trabajo.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask3" id="org_order_ask3" type="radio" value="3">3</label></td>
						<td>El cliente, mantener limpio el centro de trabajo y acomodar la mercancia.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 4</h5> <h6>Manejo de productos de limpieza</h6>
		<p style="border: 1px solid #000000; text-align: center">Se encuentra acostumbrado a utilizar productos de limpieza tales como cloro, limpiador de vidrios, detergente líquido, quitamanchas.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Los ha utilizado en su anterior empleo? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_order_organization_ask_4"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask4" id="org_order_ask4" type="radio" value="1">1</label></td>
						<td>Es una actividad que no realiza.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask4" id="org_order_ask4" type="radio" value="2">2</label></td>
						<td>Es una actividad que realiza ocasionalmente.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="org_order_ask4" id="org_order_ask4" type="radio" value="3">3</label></td>
						<td>Es una actividad que realiza con frecuencia.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row text-center">
		<button class="btn-default btn" style="background-color: black; border-color: black;" id="save_order_organization" name="save_order_organization">Guardar</button>
	</div>
</div>