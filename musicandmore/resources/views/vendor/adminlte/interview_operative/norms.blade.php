<div id="norms" class="tab-pane fade" style="overflow-y: scroll; overflow-x:hidden; height: 35em;" >
	<div class="col-md-12">
		<h5>Pregunta 1</h5><h6>Riesgo de llegar tarde</h6>
		<p style="border: 1px solid #000000; text-align: center">Me podrías indicar, ¿cuáles son las principales actividades que realizas antes de ir a tu centro de trabajo.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Llevar a los hijos a la escuela? <br>
			- ¿Llevar a la esposa a la escuela? <br>
			- ¿Realizar ejercicio? <br>
			- ¿Ir a la escuela? <br>
			- ¿Alguna otra? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_norms_ask_1"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="norms_ask1" id="norms_ask1" type="radio" value="1">1</label></td>
						<td>Existe alto riesgo de que llegue tarde al centro de trabajo.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="norms_ask1" id="norms_ask1" type="radio" value="2">2</label></td>
						<td>Existe riesgo medio de que llegue tarde al trabajo</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="norms_ask1" id="norms_ask1" type="radio" value="3">3</label></td>
						<td>No existe riesgo de que llegue tarde al trabajo</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 2</h5><h6>Apego a normas</h6>
		<p style="border: 1px solid #000000; text-align: center">En tu anterior empleo, tuviste algún problema con el reglamento de la tienda.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿De qué tipo fue el problema? <br>
			- ¿Te llamaron la atención por tu vestimenta? <br>
			- ¿Te llamaron la atención por llegar tarde? <br>
			- ¿Te levantaron alguna acta administrativa? ¿Cuál fue la razón? <br>
			- ¿Te llamaron la atención por no realizar las funciones asignadas? <br>
			- ¿Te llamaron la atención por algún problema con un cliente? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_norms_ask_2"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="norms_ask2" id="norms_ask2" type="radio" value="1">1</label></td>
						<td>Le han levantado actas administrativas</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="norms_ask2" id="norms_ask2" type="radio" value="2">2</label></td>
						<td>Le han llamado la atención por la vestimenta, llegar tarde, por no llevar a cabo las actividades asignadas o por problemas con clientes.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="norms_ask2" id="norms_ask2" type="radio" value="3">3</label></td>
						<td>No ha tenido problemas</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 3</h5><h6>Apego a normas</h6>
		<p style="border: 1px solid #000000; text-align: center">En tu anterior empleo, había un reglamento de trabajo y se te dio a conocer al ingresar.</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Recuerda algunos puntos importantes del mismo? <br>
			- ¿Te lo entregaron impreso? <br>
			- ¿Te hicieron examen sobre el contenido del mismo? <br>
			- ¿Te notificaban de las actualizaciones al mismo? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_norms_ask_3"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="norms_ask3" id="norms_ask3" type="radio" value="1">1</label></td>
						<td>No había reglamento.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="norms_ask3" id="norms_ask3" type="radio" value="2">2</label></td>
						<td>Había reglamento y nunca lo leí.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="norms_ask3" id="norms_ask3" type="radio" value="3">3</label></td>
						<td>Leí detenidamente el contenido del reglamento.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row text-center">
		<button class="btn-default btn" style="background-color: black; border-color: black;" id="save_norms" name="save_norms">Guardar</button>
	</div>
</div>