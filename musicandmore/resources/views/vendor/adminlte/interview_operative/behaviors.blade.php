<div id="behaviors" class="tab-pane fade" style="overflow-y: scroll; overflow-x:hidden; height: 25em;" >
	<div class="col-md-12">
		<h5>Pregunta 1</h5>
		<p style="border: 1px solid #000000; text-align: center">¿El candidato llego a tiempo?</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_behaviors_ask_1"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask1" id="behaviors_ask1" type="radio" value="1">1</label></td>
						<td>Llegó con más de 15 minutos tarde.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask1" id="behaviors_ask1" type="radio" value="2">2</label></td>
						<td>Llegó más temprano de lo acordado.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask1" id="behaviors_ask1" type="radio" value="3">3</label></td>
						<td>Llegó a tiempo.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 2</h5>
		<p style="border: 1px solid #000000; text-align: center">¿El candidato llegó vestido de acuerdo a la ocasión?</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Su vestimenta era muy estrafalario? <br>
			- ¿El candidato venía bien vestido? <br>
			- ¿La ropa no era acorde a la situacion climatológica del momento? (Hacia calor y vestía con chamarra). <br>
			- ¿La ropa no era acorde a la situacion? ¿Vestia de forma adeacuada para la entrevista? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_behaviors_ask_2"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask2" id="behaviors_ask2" type="radio" value="1">1</label></td>
						<td>Vestía de forma poco adecuada para la entrevista.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask2" id="behaviors_ask2" type="radio" value="2">2</label></td>
						<td>Vestia ropa casual nada fuera de común.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask2" id="behaviors_ask2" type="radio" value="3">3</label></td>
						<td>Vestía de forma adecuada.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 3</h5>
		<p style="border: 1px solid #000000; text-align: center">¿Cómo fue el comportamiento del candidato durante la entrevista?</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Estaba nervioso? <br>
			- ¿Se le notaba incomodo? <br>
			- ¿Veía continuamente el reloj? <br>
			- ¿Estaba relajado? <br>
			- ¿Hablaba demaciado y era dificil hacerle preguntas? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_behaviors_ask_3"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask3" id="behaviors_ask3" type="radio" value="1">1</label></td>
						<td>Se le notaba muy nervioso e incómodo.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask3" id="behaviors_ask3" type="radio" value="2">2</label></td>
						<td>Se notaba un poco nervioso pero nada fuera de lo común.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask3" id="behaviors_ask3" type="radio" value="3">3</label></td>
						<td>Estuvo relajado durante toda la sesión de entrevista.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<h5>Pregunta 4</h5>
		<p style="border: 1px solid #000000; text-align: center">¿Cómo fue su estado de ánimo durante la entrevista?</p>
		<h6>Preguntas de apoyo</h6>
		<p style="border: 1px solid #000000;">
			- ¿Estaba nervioso? <br>		
			- ¿Se le notaba incómodo? <br>
			- ¿Veía continuamente el reloj? <br>
			- ¿Estaba relajado? <br>
			- ¿Hablaba demasiado y era difícil hacerle preguntas? <br>
		</p>
		<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_behaviors_ask_4"></p></b>
		<div class="table-responsive">          
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask4" id="behaviors_ask4" type="radio" value="1">1</label></td>
						<td>Se le notaba muy nervioso e incómodo.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask4" id="behaviors_ask4" type="radio" value="2">2</label></td>
						<td>Se notaba un poco nervioso pero nada fuera de lo común.</td>
					</tr>
					<tr>
						<td><label class="radio-inline"><input name="behaviors_ask4" id="behaviors_ask4" type="radio" value="3">3</label></td>
						<td>Estuvo relajado durante toda la sesión de la entrevista.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row text-center">
		<button class="btn-default btn" style="background-color: black; border-color: black;" id="save_behaviors" name="save_behaviors">Guardar</button>
	</div>
</div>