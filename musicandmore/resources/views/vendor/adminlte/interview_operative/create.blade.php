@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Entrevista {{$usersInterview->name}} <input type="hidden" id="usersInterview" value="{{$usersInterview->id}}"></h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<input type="hidden" value="" id="operative_interview_id" name="operative_interview_id">
					<div class="col-sm-4">
						<ul class="nav nav-pills nav-stacked">
							@foreach ($sections as $key =>$s)
							<li class="@if($key == 0) active @endif" data-toggle="tooltip" title="{{$s->name}}"><a class="tabsInterviews" id="tab{{str_replace(' ', '', $s->name)}}" data-toggle="tab" href="#{{str_replace(' ', '', $s->name)}}">{{$s->name}}</a>
							</li>
							@endforeach
							
						</ul>
					</div>
					<div class="col-sm-8">
						<div class="tab-content">
							@foreach($sections as $key => $section)
							<div id="{{str_replace(' ', '', $section->name)}}" class="tab-pane fade @if ($key == 0) in active @endif" style="overflow-y: scroll; overflow-x:hidden; height: 35em;">
								<input type="hidden" class="sectionId" value="{{$section->id}}">
								@foreach ($questions as $qs)
								@if ($qs->sections_id == $section->id)
								<div class="col-md-12">
									<label>{{$qs->text}}</label>
									<b><p class="text-center text-danger hidden" style="color: red; margin-bottom: -3px;" id="error_interview_user"></p></b>
									<h6>Preguntas de apoyo</h6>
									<p style="border: 1px solid #000000;">
										@if ($qs->supports_1 != null || $qs->supports_1 != '')
										-{{$qs->supports_1}} <br>
										@endif
										@if ($qs->supports_2 != null || $qs->supports_2 != '')
										-{{$qs->supports_2}} <br>
										@endif
										@if ($qs->supports_3 != null || $qs->supports_3 != '')
										-{{$qs->supports_3}} <br>
										@endif
										@if ($qs->supports_4 != null || $qs->supports_4 != '')
										-{{$qs->supports_4}} <br>
										@endif
										@if ($qs->supports_5 != null || $qs->supports_5 != '')
										-{{$qs->supports_5}} <br>
										@endif
										@if ($qs->supports_6 != null || $qs->supports_6 != '')
										-{{$qs->supports_6}} <br>
										@endif
										@if ($qs->supports_7 != null || $qs->supports_7 != '')
										-{{$qs->supports_7}} <br>
										@endif
										@if ($qs->supports_8 != null || $qs->supports_8 != '')
										-{{$qs->supports_8}} <br>
										@endif
										@if ($qs->supports_9 != null || $qs->supports_9 != '')
										-{{$qs->supports_9}} <br>
										@endif
										@if ($qs->supports_10 != null || $qs->supports_10 != '')
										-{{$qs->supports_10}} <br>
										@endif
									</p>
									<div class="table-responsive">          
										<table class="table table-striped">
											<thead>
												<tr>
													<th>#</th>
													<th>Descripción</th>
												</tr>
											</thead>
											<tbody>
												@if ($qs->answers_1 != null || $qs->answers_1 != '')
												<tr>
													<td><label class="radio-inline"><input name="answer_{{$qs->id}}" id="answer_{{$qs->id}}" type="radio" value="1" @foreach ($answerUsers as $answers) @if($answers->questions_id == $qs->id) @if($answers->value == 1) checked="checked" @endif @endif @endforeach>1</label></td>
													<td>{{$qs->answers_1}}</td>
												</tr>
												@endif
												@if ($qs->answers_2 != null || $qs->answers_2 != '')
												<tr>
													<td><label class="radio-inline"><input name="answer_{{$qs->id}}" id="answer_{{$qs->id}}" type="radio" value="2" @foreach ($answerUsers as $answers) @if($answers->questions_id == $qs->id) @if($answers->value == 2) checked="checked" @endif @endif @endforeach>2</label></td>
													<td>{{$qs->answers_2}}</td>
												</tr>
												@endif
												@if ($qs->answers_3 != null || $qs->answers_3 != '')
												<tr>
													<td><label class="radio-inline"><input name="answer_{{$qs->id}}" id="answer_{{$qs->id}}" type="radio" value="3" @foreach ($answerUsers as $answers) @if($answers->questions_id == $qs->id) @if($answers->value == 3) checked="checked" @endif @endif @endforeach>3</label></td>
													<td>{{$qs->answers_3}}</td>
												</tr>
												@endif
											</tbody>
										</table>
									</div>
								</div>
								@endif
								@endforeach
								<div class="row text-center">
									<button class="btn-success btn save_interview" style="background-color: black; border-color: black;" name="save_interview" data-active="{{str_replace(' ', '', $section->name)}}">@if ($section == $sections->last()) Finalizar @else Guardar @endif</button>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				<!--box-body-->
			</div>
			<!--.box-->
		</div>
	</div>
</div>
@endsection