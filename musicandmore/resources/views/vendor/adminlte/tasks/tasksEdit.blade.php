@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="panel panel-primary" style="border-color: #bce8f1;">
	<table class="table table-hover table table-striped">
		<thead style="background: black; color: white;">
			<tr>
				<th class="text-center">Atualizar Tarea</th>
			</tr>
	    </thead>
	  </table>
	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a>
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a>
	</div>

	<div class="panel-body">

		<form class="" action="{{ route('tasks_update',['tasks'=>$tasks->id]) }}" method="POST" role="form" enctype="multipart/form-data">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
		<div class="form-group col-md-12">

			<div class="col-md-12 text-center">
				<label for="Nombre" style="font-size: 1.5em">Nombre de la tarea</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<input type="text" class="form-control" id="title" name="title" placeholder="Nueva tarea" value="{{ $tasks->title }}"  style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px">
			</div>

		</div>

		<div class="form-group row">
					<div class="col-xs-6">
						<br>
						<br>
			            <div class="text-center">
							<label for="Fecha_inicial" style="font-size: 1.5em">Objetivo</label>
						</div>

			            <div class="col-md-12 text-center" >
			                <div class="col-md-12">
			                	<input style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="objective" name="objective" placeholder="Objetivo de la nueva tarea" value="{{ $tasks->objective }}"></input>
			                </div>
				        </div>
			        </div>

		            <div class="col-xs-6">
		            	<br>
						<br>
			            <div class="text-center">
							<label for="Fecha_final" style="font-size: 1.5em">Descripción</label>
						</div>

			            <div class="col-md-12 text-center" >
			                <input style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="description" name="description" placeholder="Descripcion de la nueva tarea" value="{{ $tasks->description }}"></input>
				        </div>
			        </div>
		        </div>

		<div class="form-group row">
			<div class="col-xs-6">
				<br>
				<br>
            	<div class="text-center">
					<label for="Fecha_inicial" style="font-size: 1.5em">Fecha inicial</label>
				</div>

				<div class="col-md-12 text-center" >
					<input id="initial_date" type="datetime" name="initial_date" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $tasks->initial_date }}"required/>
				</div>
	     	</div>

	     	<div class="col-xs-6">
	     		<br>
				<br>
	     		<div class="text-center">
	     			<label for="Fecha_final" style="font-size: 1.5em">Fecha final</label>
	     		</div>

	     		<div class="col-md-12 text-center" >
                	<input id="final_date" type="datetime" name="final_date" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $tasks->final_date }}" required>
	       		</div>
	    	</div>
        </div>

        <div class="form-group col-md-12">
        	<br>
			<div class="text-center">
				<label for="Comentarios" style="font-size: 1.5em">Comentarios</label>
			</div>
			<div class="col-md-12">
				<input style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="comment" name="comment" placeholder="Comentarios de la nueva tarea" value="{{ $tasks->comment }}"></input>
			</div>
		</div>
			<br>
			<br>
		<div class="form-group">
				<div class="text-center">
					<label for="ArchivosT" style="font-size: 1.5em">Archivos</label>
				</div>
				<div class="col-md-8 col-md-offset-2" >
                	<input id="ArchivosT" type="file" name="ArchivosT" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" class="form-control">
	        	</div>
		</div>
		<div class="form-group">
			<br>
			<br>
			<br>
		    <div class="text-center col-md-12">
				<label for="" style="font-size: 1.5em">Formulario</label>
			</div>
			<div class="text-center">
			<a href="{{ route('formulario_question_task', [ '$task' => $tasks->id ]) }}" title="Editar Formulario" class="btn btn-success" style="background-color: #1bb49a; border-color: #1bb49a;"><span class="glyphicon glyphicon-tasks" aria-hidden="true" style="width: 8em;"></span></a>
			</div>
		</div>

		<br>

		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="">Guardar</button>
		</div>
		</form>
	</div>
</div>
		@endsection