@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Tus Tareas</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						<i class="glyphicon glyphicon-search"></i>
						<input type="text" value="" data-table="tableTasks" class="search_employees" placeholder="Buscar"/>
					</div>
					</div>
						<h3 class="titleCenter" style="text-align: center;">Tareas Creadas</h3>
						<div class="table-responsive">
						<table class="table table-hover table table-striped" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Objetivo</th>
								<th class="text-center">Descripción</th>
								<th class="text-center">Fecha Inicio</th>
								<th class="text-center">Fecha Final</th>
								<th class="text-center">Progreso</th>
							</tr>
						</thead>							
						@foreach($tasks as $task)

						<tbody>
								<tr>
									<td class="text-center">{{$task-> title}}</td>
									<td class="text-center">{{$task-> objective}}</td>
									<td class="text-center">{{$task-> description}}</td>
									<td class="text-center">{{$task-> initial_date}}</td>
									<td class="text-center">{{$task-> final_date}}</td>
									@if($task->progress == 100)
										<td class="text-center">
											<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="100%" disabled="true">
											<strong>100.00% </strong><i class="fa fa-check-circle" style="color: white;"></i></td>
										</button></td>
									@elseif($task->progress == 0)
										<td class="text-center">
											<button type="button" class="btn btn-danger" aria-label="Left Align" data-toggle="tooltip" title="0.00%" disabled="disabled"><strong>0.00% </strong><i class="fa fa-times-circle" aria-hidden="true"></i>
										</button></td>
									@else
										<td class="text-center">
											<button type="button" class="btn btn-info" aria-label="Left Align" data-toggle="tooltip" title="0.00%" disabled="disabled"><strong>{{$task-> progress}}% </strong>
										</button></td>
									@endif
								</tr>
							</tbody>
						@endforeach
						</table>
						{{$tasks->links()}}

						<h3 class="titleCenter" style="text-align: center;">Tareas Asignadas</h3>
						<table class="table table-hover table table-striped" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Fecha Inicio</th>
								<th class="text-center">Fecha Final</th>
								<th class="text-center">Progreso</th>
							</tr>
						</thead>
						@foreach($assign as $assigns)
							<tbody>
								<tr>
									<td class="text-center"><a href="{{ route('assign_view', ['assign'=> $assign->id, 'tasks'=> $assign->taskid, 'users'=>$assign->user_id]) }}" title="{{$assign-> title}}" ><strong>
										{{$assigns-> title}}</strong></a></td>
									<td class="text-center">{{$assigns-> initial_date}}</td>
									<td class="text-center">{{$assigns-> final_date}}</td>
									@if($assigns->is_completed == 100)
										<td class="text-center">
											<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="100%" disabled="true">
											<strong>100.00% </strong><i class="fa fa-check-circle" style="color: white;"></i></td>
										</button></td>
									@elseif($assigns->is_completed == 0)
										<td class="text-center">
											<button type="button" class="btn btn-danger" aria-label="Left Align" data-toggle="tooltip" title="0.00%" disabled="disabled"><strong>0.00% </strong><i class="fa fa-times-circle" aria-hidden="true"></i>
										</button></td>
									@endif
								</tr>
							</tbody>
						@endforeach
						</table>
						{{$assign->links()}}
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
		@endsection