@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h1>¡Gracias por tomarte el tiempo de contestar esta evaluación!</h1>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="col-md-12 text-center">
						<span class="text-muted" style="font-size: 2.5em">TU PUNTUACIÓN FUE DE: <br> </span>
					</div>
					<span class="text-muted" style="font-size: 3.5em">{{ number_format($TrainingUser->correct) }} / {{ $count }} </span>
						<input type="hidden" value="{{ $TrainingUser->grade }}" id="grade_user">

					<div class="col-md-6 text-center">

						<div class="col-md-offset-3 col-md-6" id="test-circle4"></div>
					</div>

					<div class="text-center col-md-6">
						<div class="row">
							<img src="{{ asset('img/teamCircle.png') }}" class="img-responsive container" >
						</div>

					</div>
					<div class="col-md-12 text-center">
						<br><p style="font-size: 1.3em;">¡Ahora puedes continuar con los demás cursos! </p>
						<a href="{{route('training_study')}}" class="btn-primary" id="EndEvaluation"><i style="font-size: 1.3em;" class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp; REGRESAR A CURSOS</a>
					</div>
				</div>
			</div>
			<!--box-body-->
		</div>
		<!--.box-->
	</div>
</div>
@endsection
@section('circliful')
<script src="{{ asset('js/jquery.circliful.js')}}"></script>
<script>

    $( document ).ready(function() { // 6,32 5,38 2,34
    	$("#test-circle4").circliful({
    		animation: 1,
    		animationStep: 1,
    		foregroundColor: ($('#grade_user').val() > 69) ? 'green' : 'red',
    		foregroundBorderWidth: 20,
    		backgroundBorderWidth: 15,
    		percent: $('#grade_user').val(),
    		textSize: 30,
    		textStyle: 'font-size: 12px;',
    		textColor: '#666',
    		// halfCircle: 1,
    	});
    });
</script>
@stop
