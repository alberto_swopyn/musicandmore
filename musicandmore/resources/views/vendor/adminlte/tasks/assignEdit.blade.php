@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('personal_style')
@include('adminlte::tasks.PersonalSelect2')
@stop


@section('main-content')
<div class="panel panel-primary" style="border-color: #bce8f1;">
	<div class="panel-heading"  style="color: white; background: #00a7e1;">
		<h3>Actualizar Asignación</h3>
	</div>
	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a>
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a>
	</div>

	<div class="panel-body">
		<form class="" action="{{ route('assign_update',['assign'=>$assign->id]) }}" method="POST" role="form">
          		{{ csrf_field() }}
         		{{ method_field('PUT') }}
		<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				
				<div>
					<span style="font-size: 1.5em">
						Tareas
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-book" aria-hidden="true" style="font-size: 2em; color: #029abf"></i>
							</span>
							<select name="task[]" multiple="multiple" class="form-control multiple" id="task">
								@foreach($tasks as $task)
								<option value="{{ $task->id}}"
									@foreach ($tasksassign as $taskassign)
									@if($taskassign->id == $task->id)
									selected ='selected'
									@endif
									@endforeach	
									{{ (in_array($task, old('task', []))) }} >{{ $task->title }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div>
					<span style="font-size: 1.5em">
						Usuarios
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em">
								<i class="fa fa-user" aria-hidden="true" style="font-size: 2em; color: #029abf"></i>
							</span>
							<select name="users[]" multiple="multiple" class="form-control multiple" id="users">
							@foreach($users as $user)
								<option value="{{ $user->id}}" 
									@foreach ($usersassign as $userassign)
									@if($userassign->id == $user->id)
									selected ='selected'
									@endif
									@endforeach	
									{{ (in_array($user, old('users', []))) }} >{{ $user->name }}
								</option>
							@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="form-group row">
		            <div class="text-center">
						<label for="Fecha_inicial" style="font-size: 1.5em">Fecha inicial</label>
					</div>

		            <div class="col-md-12 text-center" >
		                <input id="initial_date" type="datetime" name="initial_date" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $assign->initial_date }}" required>
			        </div>
		        </div>

		        <br>

		        <div class="form-group row">
		            <div class="text-center">
						<label for="Fecha_final" style="font-size: 1.5em">Fecha final</label>
					</div>

		            <div class="col-md-12 text-center" >
		                <input id="final_date" type="datetime" name="final_date" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $assign->final_date }}" required>
			        </div>
		        </div>

				<br>

		        <div class="form-group row">
		            <div class="text-center">
						<label for="Hora" style="font-size: 1.5em">Hora</label>
					</div>
					<div class="col-md-4 text-center" ></div>
		            <div class="col-md-4 text-center" >
		                <input id="Hora" type="time" name="Hora" @if($hora != null) value="{{ $assign->hour }}" @endif class="form-control" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
			        </div>
			        <div class="col-md-4 text-center" ></div>
		        </div>

		        <br>
		        <div class="form-group row">
		            <div class="text-center">
						<label for="Rep" style="font-size: 1.5em">Repetir los días:</label>
					</div>

		            <div class="col-md-3 text-center" >
		            	@if($assign->rep_monday == 1)
			                <input id="Rep_monday" type="checkbox" name="Rep_monday" value="1" checked=""> <span for="Rep" style="font-size: 1.5em">Lunes</span> </input>
				        </div>
				        @endif
				        @if($assign->rep_monday == 0)
			                <input id="Rep_monday" type="checkbox" name="Rep_monday" value="1"> <span for="Rep" style="font-size: 1.5em">Lunes</span> </input>
				        </div>
				        @endif

						@if($assign->rep_tuesday == 1)
							<div class="col-md-3 text-center" >
			                <input id="Rep_tuesday" type="checkbox" name="Rep_tuesday" value="1" checked=""> <span for="Rep" style="font-size: 1.5em">Martes</span> </input>
				        </div>
						@endif
						@if($assign->rep_tuesday == 0)
							<div class="col-md-3 text-center" >
			                <input id="Rep_tuesday" type="checkbox" name="Rep_tuesday" value="1" > <span for="Rep" style="font-size: 1.5em">Martes</span> </input>
				        </div>
						@endif
				        
						
						@if($assign->rep_wednesday == 1)
							<div class="col-md-3 text-center" >
			                <input id="Rep_wednesday" type="checkbox" name="Rep_wednesday" value="1" checked=""> <span for="Rep" style="font-size: 1.5em">Miércoles</span> </input>
				        </div>
						@endif
						@if($assign->rep_wednesday == 0)
							<div class="col-md-3 text-center" >
			                <input id="Rep_wednesday" type="checkbox" name="Rep_wednesday" value="1" > <span for="Rep" style="font-size: 1.5em">Miércoles</span> </input>
				        </div>
						@endif
				        

				        @if($assign->rep_thursday == 1)
							<div class="col-md-3 text-center" >
			                <input id="Rep_thursday" type="checkbox" name="Rep_thursday" value="1" checked=""> <span for="Rep" style="font-size: 1.5em">Jueves</span> </input>
				        </div>
						@endif
						@if($assign->rep_thursday == 0)
							<div class="col-md-3 text-center" >
			                <input id="Rep_thursday" type="checkbox" name="Rep_thursday" value="1" > <span for="Rep" style="font-size: 1.5em">Jueves</span> </input>
				        </div>
						@endif


				        @if($assign->rep_friday == 1)
							<div class="col-md-4 text-center" >
			                <input id="Rep_friday" type="checkbox" name="Rep_friday" value="1" checked=""> <span for="Rep" style="font-size: 1.5em">Viernes</span> </input>
				        </div>
						@endif
						@if($assign->rep_friday == 0)
							<div class="col-md-4 text-center" >
			                <input id="Rep_friday" type="checkbox" name="Rep_friday" value="1" > <span for="Rep" style="font-size: 1.5em">Viernes</span> </input>
				        </div>
						@endif

						

				        @if($assign->rep_saturday == 1)
							<div class="col-md-4 text-center" >
			                <input id="Rep_saturday" type="checkbox" name="Rep_saturday" value="1" checked=""> <span for="Rep" style="font-size: 1.5em">Sábado</span> </input>
				        </div>
						@endif
						@if($assign->rep_saturday == 0)
							<div class="col-md-4 text-center" >
			                <input id="Rep_saturday" type="checkbox" name="Rep_saturday" value="1" > <span for="Rep" style="font-size: 1.5em">Sábado</span> </input>
				        </div>
						@endif


				        @if($assign->rep_sunday == 1)
							<div class="col-md-4 text-center" >
			                <input id="Rep_sunday" type="checkbox" name="Rep_sunday" value="1" checked=""> <span for="Rep" style="font-size: 1.5em">Domingo</span> </input>
				        </div>
						@endif
						@if($assign->rep_sunday == 0)
							<div class="col-md-4 text-center" >
			                <input id="Rep_sunday" type="checkbox" name="Rep_sunday" value="1" > <span for="Rep" style="font-size: 1.5em">Domingo</span> </input>
				        </div>
						@endif

				        
		        </div>


		        <div class="form-group row">
		            <div class="text-center">
						<label for="Comentario" style="font-size: 1.5em">Comentario</label>
					</div>

		            <div class="col-md-12 text-center" >
		                <input style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid #59c4c5; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="comment" name="comment" placeholder="Comentarios de la asignación" value="{{ $assign->comment }}"></input>
			        </div>
		        </div>
			</div>
		</div>
		<br>
		<br>

		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary btn-lg" style="width: 30%; margin-top: 1em;">
				Guardar
			</button>
		</div>
		</form>
	</div>
</div>
@endsection
@section('scripts')
<script>
	$(".multiple").select2();
</script>
@endsection