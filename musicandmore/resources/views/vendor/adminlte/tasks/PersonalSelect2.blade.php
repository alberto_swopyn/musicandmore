<style>

.select2 {
width:100%!important;
}

.select2-container--default .select2-selection--multiple .select2-selection__rendered li {
	background-color: white;
	color: black;
	font-weight: bold;
	border: none;
	font-size: 1.5em;
	margin: .5em;
}
span.select2-selection.select2-selection--multiple{border: 1px solid #f5216e;}
input:focus {
    outline: none !important;
    border-color: #f5216e;
    box-shadow: 0 0 10px #f5216e;
}

.input-group .select2-container {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0;
  display: table;
  table-layout: fixed;
}

</style>
