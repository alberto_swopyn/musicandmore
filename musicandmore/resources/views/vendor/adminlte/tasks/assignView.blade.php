@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('personal_style')
@include('adminlte::tasks.PersonalSelect2')
@stop


@section('main-content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<div class="panel panel-primary" style="border-color: white;">
	<table class="table table-hover table table-striped">
		<thead style="background: black; color: white;">
			<tr>
				<th class="text-center">Tarea Asignada</th>
			</tr>
	    </thead>
	  </table>
	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a>
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a>
	</div>

	<div class="panel-body">
		
		<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				
				<div>
					<div class="text-center">
					<label style="font-size: 1.5em">
						<i class="fa fa-tasks" aria-hidden="true" style="color: black;"></i> Tarea: {{ $task->title }}
					</label>
					</div>
				</div>
				<br>
				<br>
				<div class="form-group row">
					<div class="col-xs-6">
			            <div class="text-center">
							<label for="Fecha_inicial" style="font-size: 1.5em">Fecha inicial</label>
						</div>

			            <div class="col-md-12 text-center" >
		                <p style="border: 1px solid; font-weight: lighter; font-size: 1.5em; border-radius: 7px">{{ $assign->initial_date }}</p>
			        </div>
			        </div>

		            <div class="col-xs-6">
			            <div class="text-center">
							<label for="Fecha_final" style="font-size: 1.5em">Fecha final</label>
						</div>

			            <div class="col-md-12 text-center" >
			                <p style="border: 1px solid; font-weight: lighter; font-size: 1.5em; border-radius: 7px">{{ $assign->final_date }}
			                </p>
				        </div>
			        </div>
		        </div>

		        <div class="form-group row">
					<div class="col-xs-6">
			            <div class="text-center">
							<label for="Fecha_inicial" style="font-size: 1.5em">Objetivo</label>
						</div>

		                <div class="col-md-12 text-center" >
		                <p style="border: 1px solid; font-weight: lighter; font-size: 1.5em; border-radius: 7px">{{ $task->objective }}</p>
			        </div>
			        </div>

		            <div class="col-xs-6">
			            <div class="text-center">
							<label for="Fecha_final" style="font-size: 1.5em">Descripción</label>
						</div>

			            <div class="col-md-12 text-center" >
		                <p style="border: 1px solid; font-weight: lighter; font-size: 1.5em; border-radius: 7px">{{ $task->description }}
		                </p>
			        </div>
			        </div>
		        </div>

		        <br>
		        <br>

		        <div class="form-group">
		        	<div class="col-xs-6">
		            <div class="text-center">
						<label for="" style="font-size: 1.5em">Documento</label>
					</div>
					
					<div class="col-md-12 text-center" >
					<a href="{{ route('task_download', ['tasks'=> $task->id]) }}" download class="btn btn-info" title="Descargar" style="width: 25%; background-color: #1bb49a; border-color: #1bb49a;"><i class="fa fa-download" style="color: white;"></i></a>
					</div>
					</div>

					<div class="col-xs-6">
			  		<div class="text-center">
			  			<label for="" style="font-size: 1.5em">Cuestionario</label>
          			</div>

				<div class="col-md-12 text-center" >
          			<a href="{{ route('introtask', ['tasks' => $task->id]) }}" class="btn btn-info" title="Cuestionario" style="width: 25%; background-color: #1bb49a; border-color: #1bb49a;"><i class="fa fa-graduation-cap" style="color: white;"></i></a>
          		</div>
				</div>
			</div>

					
				<div class="panel panel-primary" style="border-color: white;">
					<br>
		      <br>
					<div class="panel-body">
						<br>
		        	<br>
		        		<div class="form-group">
		        		
		            		<div class="text-center">
								<label for="" style="font-size: 1.5em">Evidencias</label>
							</div>
							
						<?php $a = 0; ?>						
						@foreach($evi as $evi)
						<div class="col-md-3" style="padding-top: 15px;">
							<?php $a = $evi->id; ?>
							<?php $ex = pathinfo($evi->name, PATHINFO_EXTENSION); ?>
								@if($ex == 'jpg')
								<img src="{{ asset( '../../swopyn/storage/app/evidencias/' . $evi->name ) }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'jpeg')
								<img src="{{ asset( '../../swopyn/storage/app/evidencias/' . $evi->name ) }}"  alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'png')
								<img src="{{ asset( '../../swopyn/storage/app/evidencias/' . $evi->name ) }}"  alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'doc')
								<img src="{{ asset( '../../swopyn/storage/app/word.ico') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'docx')
								<img src="{{ asset( '../../swopyn/storage/app/word.ico') }}"  alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'xlsx')
								<img src="{{ asset( '../../swopyn/storage/app/excel.png') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'pptx')
								<img src="{{ asset( '../../swopyn/storage/app/power.png') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'pdf')
								<img src="{{ asset( '../../swopyn/storage/app/pdf2.png') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'rar')
								<img src="{{ asset( '../../swopyn/storage/app/rar.png') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'zip')
								<img src="{{ asset( '../../swopyn/storage/app/zip.jpeg') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'flv')
								<img src="{{ asset( '../../swopyn/storage/app/video.png') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'mp4')
								<img src="{{ asset( '../../swopyn/storage/app/video.png') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'wmv')
								<img src="{{ asset( '../../swopyn/storage/app/video.png') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'mpg')
								<img src="{{ asset( '../../swopyn/storage/app/video.png') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == 'mpeg')
								<img src="{{ asset( '../../swopyn/storage/app/video.png') }}" alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif
								@if($ex == '3gp')
								<img src="{{ asset( '../../swopyn/storage/app/video.png') }}"  alt="" style="height: 6em; width:6em; text-align: center; border-radius: 7px;" class="img-responsive"/>
								@endif

								<div class="col-ms-6" style="padding-top: 5px;">
									<a href="{{ route('evidence_download2', ['evi'=> $evi->id]) }}" download="" class="btn btn-info" title="Descargar" style="width: 25%; background-color: #1bb49a; border-color: #1bb49a;"><i class="fa fa-download" style="color: white;"></i></a>
									@role(['admin','hr']) 
										<button type="submit" title="Eliminar" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete<?php echo $a; ?>">
											<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
										</button>
									</div>
										<!-- INICIA MODAL PARA ELIMINAR REGISTRO -->
										<div class="modal fade" id="confirm-delete<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<div class="modal-body">
														<h4 class="modal-title">¿Desea eliminar esta evidencia?</h4>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														<form action="{{ route('evidence_delete', ['assign'=> $assign->id, 'tasks'=> $task->id, 'users'=> $usuario, 'evi' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('DELETE') }}
															<button type="submit" title="Eliminar" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete"><strong>Eliminar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->
									@endrole
								</div>
						@endforeach

								<div class="form-group row">
									<div class="col-xs-6">
								<br>
								<br>
								<br>
		                		<form method="POST" enctype="multipart/form-data" action="{{ route('evidence_add', ['assign'=> $assign->id, 'tasks'=> $task->id, 'users'=> $usuario]) }}" role="form">
                           		{{ csrf_field() }}
                           		{{ method_field('PUT') }}
										<div class="col-md-12 text-center">
                							<input id="ArchivosE" type="file" name="ArchivosE" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px"@if($fini->is_completed != 0) disabled="" @endif>
	        							</div>
									</div>
									
									<div class="col-md-12 text-center">
										<br>
										<br>
										<button type="submit" class="btn btn-primary" style=""@if($fini->is_completed != 0) disabled="" @endif>
											Guardar Evidencia
										</button>
									</div>
                       			</form>
		        		</div>
		        	</div>

		        <div class="form-group row">
		        	<br>
		        	<br>
		          <div class="text-center">
							<label for="Comentario" style="font-size: 1.5em">Comentario</label>
						</div>

		            <div class="col-md-12 text-center" >
		                <p style="border: 1px solid; font-weight: lighter; font-size: 1.5em; border-radius: 7px">{{ $assign->comment }}</p>
			        </div>
		        </div>

		        <div class="panel panel-primary" style="border-color: gray;">
					<div class="panel-body">
		        		<div class="form-group">
		            		<div class="text-center">
								<label for="" style="font-size: 1.5em">Mensajes</label>
							</div>
							<div class="col-md-12">
								<textarea style="height:10em; max-width: 61em; min-width: 370; min-height: 10em; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="mensajes" name="mensajes" value="" disabled="true">
									@foreach($mens as $mens)
									{{$mens->name}}: {{$mens->message}}
									@endforeach
								</textarea>
							</div>
							<div class="col-md-12 text-center" >
		                		<form method="POST" enctype="multipart/form-data" action="{{ route('message_add', ['assign'=> $assign->id, 'tasks'=> $task->id, 'users'=> $usuario]) }}" role="form">
                           		{{ csrf_field() }}
                           		{{ method_field('PUT') }}
                           			<div class="form-group">
										<div class="col-md-12" >
                							<input id="mensaje" type="text" name="mensaje" placeholder="Escriba un mensaje..." style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px; width: 50%;"@if($fini->is_completed != 0) disabled="" @endif>
	        							</div>
									</div>
									<div class="col-md-12 text-center">
										<button type="submit" class="btn btn-primary" style="width: 15%; margin-top: 1em;"@if($fini->is_completed != 0) disabled="" @endif>
											Enviar <i class="fa fa-comments" style="color: white;"></i>
										</button>
									</div>
                       			</form>
			        		</div>
		        		</div>
		    		</div>
		    	</div>
				<!-- Inicia botón finalizar tarea -->
		            <div class="col-md-12 text-center" >
		                <button type="submit" class="btn btn-success" style="background-color: black; border-color: black;" title="Eliminar" data-toggle="modal" data-target="#confirm-send" style="width: 15%;"@if($fini->is_completed != 0) disabled="" @endif>
							<i class="fa fa-bookmark" style="color: white;"></i> Finalizar tarea
						</button>
			        </div>
			        <div class="modal fade" id="confirm-send" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<div class="modal-body">
														<div class="col-md-12 text-center" >
															<img src="../../../../../../swopyn/storage/app/signo.jpg"  alt="" style="text-align: center; border-radius: 7px;" class="img-responsive"/>
														</div>
														<h4 class="modal-title">¿Está seguro de que ha terminado <u>COMPLETAMENTE</u> la tarea?</h4>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														<form action="{{ route('finish_task', ['assign'=> $assign->id, 'tasks'=> $task->id, 'users'=> $usuario]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('PUT') }}
															<button type="submit" title="Eliminar" class="btn btn-danger" style="background-color: black; border-color: black;" data-toggle="modal" data-target="#confirm-delete"><strong>Finalizar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>

									<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->
			        <br>
			        <br>
			        <br>
			        <!-- Termina botón finalizar tarea -->
		        </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
	$(".multiple").select2();
</script>
@endsection