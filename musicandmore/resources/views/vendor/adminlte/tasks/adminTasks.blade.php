@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Tareas</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						<i class="glyphicon glyphicon-search"></i>
						<input type="text" value="" data-table="tableTasks" class="search_employees" placeholder="Buscar"/>
					</div>
					<div class="col-md-6 col-md-offset-8">
						<a href="{{ route('createtask') }}" class="btn btn-success" style="background-color: black; border-color: black;">
							<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Tarea
						</a>
						<a href="{{ route('createassign') }}" class="btn btn-success" style="background-color: black; border-color: black;">
							<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Asignación
						</a>
					</div>
					</div>

					<h3 class="titleCenter" style="text-align: center;">Tareas Creadas</h3>
					<div class="table-responsive">
						<table class="table table-hover table table-striped" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Creada por</th>
								<th class="text-center">Objetivo</th>
								<th class="text-center">Descripción</th>
								<th class="text-center">Fecha Inicio</th>
								<th class="text-center">Fecha Final</th>
								<th class="text-center">Comentario</th>
								<th class="text-center">Progreso</th>
								<th colspan="3" class="text-center">Opciones</th>
							</tr>
						</thead>
						<?php $a = 0; ?>
						@foreach($tasks as $task)
						<tbody>
							<?php $a = $task->id; ?>
								<tr>
									<td class="text-center">{{$task-> title}}</td>
									<td class="text-center">{{$task-> name}}</td>
									<td class="text-center">{{$task-> objective}}</td>
									<td class="text-center">{{$task-> description}}</td>
									<td class="text-center">{{$task-> initial_date}}</td>
									<td class="text-center">{{$task-> final_date}}</td>
									<td class="text-center">{{$task-> comment}}</td>
									@if($task->progress == 100)
										<td class="text-center">
											<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="100%" disabled="true">
											<strong>100.00% </strong><i class="fa fa-check-circle" style="color: white;"></i></td>
										</button></td>
									@elseif($task->progress == 0)
										<td class="text-center">
											<button type="button" class="btn btn-danger" aria-label="Left Align" data-toggle="tooltip" title="0.00%" disabled="disabled"><strong>0.00% </strong><i class="fa fa-times-circle" aria-hidden="true"></i>
										</button></td>
									@else
										<td class="text-center">
											<button type="button" class="btn btn-info" aria-label="Left Align" data-toggle="tooltip" title="0.00%" disabled="disabled"><strong>{{$task-> progress}}% </strong>
										</button></td>
									@endif
									<td>
										<a href="{{ route('tasks_edit', ['tasks'=> $task->id]) }}" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a;" title="Editar" ><span class="glyphicon glyphicon-edit" aria-hidden="true"> </span></a>
									</td>
									<td>
										<button type="submit" title="Eliminar" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete<?php echo $a; ?>">
											<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
										</button>
										<!-- INICIA MODAL PARA ELIMINAR REGISTRO -->
										<div class="modal fade" id="confirm-delete<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<div class="modal-body">
														<h4 class="modal-title">¿Desea eliminar este registro?</h4>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														<form action="{{ route('tasks_delete', ['tasks' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('DELETE') }}
															<button type="submit" title="Eliminar" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete"><strong>Eliminar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->
									</td>
									<td>
										<a href="{{ route('formulario_question_task', [ '$task' => $task->id ]) }}"><button type="submit" class="btn btn-default" style="background-color: #1bb49a; border-color: #1bb49a;"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span></button></a>
									</td>
								</tr>
							</tbody>
						@endforeach
						</table>
						{{$tasks->links()}}
					</div>

<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->

						<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->


						<div class="col-md-12 text-center" >

						<h3 class="titleCenter" style="text-align: center;">Tareas Asignadas</h3>
						<div class="table-responsive">
						<table class="table table-hover table table-striped" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Título</th>
								<th class="text-center">Asignada a</th>
								<th class="text-center">Fecha Inicio</th>
								<th class="text-center">Fecha Final</th>
								<th class="text-center">Comentario</th>
								<th class="text-center">Progreso</th>
								<th colspan="2" class="text-center">Opciones</th>
							</tr>
						</thead>

						<?php $b = 0; ?>
						@foreach($assign as $assigns)
							<tbody>
								<?php $b = $assigns->id; ?>
								<tr>
									<td class="text-center">
										<a href="{{ route('assign_view_admin', ['assign'=> $assign->id, 'tasks'=> $assign->task_id, 'users'=>$assign->user_id]) }}" title="{{$assign-> title}}" ><strong>{{ $assigns->title }} </strong></a>
									</td>
									<td class="text-center">{{$assigns-> name}}</td>
									<td class="text-center">{{$assigns-> initial_date}}</td>
									<td class="text-center">{{$assigns-> final_date}}</td>
									<td class="text-center">{{$assigns-> comment}}</td>
									@if($assigns->is_completed == 100)
										<td class="text-center">
											<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="100%" disabled="true">
											<strong>100.00% </strong><i class="fa fa-check-circle" style="color: white;"></i></td>
										</button></td>
									@elseif($assigns->is_completed == 0)
										<td class="text-center">
											<button type="button" class="btn btn-danger" aria-label="Left Align" data-toggle="tooltip" title="0.00%" disabled="disabled"><strong>0.00% </strong><i class="fa fa-times-circle" aria-hidden="true"></i>
										</button></td>
									@endif
									<td>
										<a href="{{ route('assign_edit', ['assign'=> $assigns->id]) }}" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a;" title="Editar"><span class="glyphicon glyphicon-edit" aria-hidden="true"> </span></a>
									</td>
									<td>
										<button type="submit" title="Eliminar" class="btn btn-danger" data-toggle="modal" data-target="#confirm-deleteassign<?php echo $b; ?>">
											<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
										</button>
										<!-- INICIA MODAL PARA ELIMINAR REGISTRO -->
										<div class="modal fade" id="confirm-deleteassign<?php echo $b; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<div class="modal-body">
														<h4 class="modal-title">¿Desea eliminar este registro?</h4>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														<form action="{{ route('assign_delete', ['assign' => $b]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('DELETE') }}
															<button type="submit" title="Eliminar" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete"><strong>Eliminar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						@endforeach
						</table>
						{{$assign->links()}}
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
		@endsection