<div class="panel panel-primary" style="border-color: #bce8f1;">
	<table class="table table-hover table table-striped">
		<thead style="background: black; color: white;">
			<tr>
				<th class="text-center">Nueva</th>
			</tr>
	    </thead>
	  </table>
	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a>
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a>
	</div>

	<div class="panel-body">


		<div class="form-group col-md-12">

			<div class="col-md-12 text-center">
				<label for="Nombre" style="font-size: 1.5em">Nombre de la tarea</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Nueva tarea"   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
			</div>

		</div>


					<div class="col-xs-6">
		            	<br>
			   		 	<br>
			            <div class="text-center">
							<label for="Descuento" style="font-size: 1.5em">Objetivo</label>
						</div>

			            <div class="col-md-12 text-center" >
			                <textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Objetivo" name="Objetivo" placeholder="Objetivo de la nueva tarea" value="" required="required"></textarea>
				        </div>
			        </div>

			        <div class="col-xs-6">
			        	<br>
			   		 	<br>
			            <div class="text-center">
							<label for="Fecha_limite" style="font-size: 1.5em;">Descripción</label>
						</div>

						
						<div class="col-md-12 text-center">
			            
			               <textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Descripcion" name="Descripcion" placeholder="Descripcion de la nueva tarea" value="" required="required"></textarea>
				   		</div>
				   	</div>

		<div class="form-group row">
			<div class="col-xs-6">
				<br>
			   	<br>
				<div class="text-center">
					<label for="Fecha_inicial" style="font-size: 1.5em">Fecha inicial</label>
				</div>

	            <div class="col-md-12 text-center" >
	                <input id="Fecha_inicial" type="date" name="Fecha_inicial" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
		        </div>
			</div>

			<div class="col-xs-6">
				<br>
			   	<br>
				<div class="text-center">
					<label for="Fecha_final" style="font-size: 1.5em">Fecha final</label>
				</div>

	            <div class="col-md-12 text-center" >
	                <input id="Fecha_final" type="date" name="Fecha_final" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
		        </div>

			</div>

        </div>

        <div class="form-group col-md-12">
        	<br>
			<br>
			<div class="text-center">
				<label for="Comentarios" style="font-size: 1.5em">Comentarios</label>
			</div>
			<div class="col-md-12">
				<textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Comentarios" name="Comentarios" placeholder="Comentarios de la nueva tarea" value="" required="required"></textarea>
			</div>
		</div>

		<div class="form-group">
				<div class="text-center">
					<label for="ArchivosT" style="font-size: 1.5em">Archivos</label>
				</div>
				<div class="col-md-8 col-md-offset-2" >
                	<input id="ArchivosT" type="file" name="ArchivosT" style="border: 1px; font-weight: bold; font-size: 1.5em; border-radius: 7px" class="form-control" required>
	        	</div>
		</div>

		<br>
		<br>
		<br>
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="">Guardar</button>
		</div>
	</div>
</div>
