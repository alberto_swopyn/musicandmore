<div class="panel panel-primary" style="border-color: #bce8f1;">
	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a>
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a>
	</div>

	<div class="panel-body">

		<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div class="text-center">
					<h3>Asignación</h3>
				</div>

				<div>
					<span style="font-size: 1.5em">
						Tareas
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-book" aria-hidden="true" style="font-size: 2em; color: #029abf"></i>
							</span>
							<select name="task[]" multiple="multiple" class="form-control multiple" id="task">
								@foreach($tasks as $task)
								<option value="{{ $task->id}}"
									{{ (in_array($task, old('task', []))) }} >{{ $task->title }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div>
					<span style="font-size: 1.5em">
						Usuarios
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-users" aria-hidden="true" style="font-size: 2em; color: #029abf"></i>
							</span>
							<select name="user[]" multiple="multiple" class="form-control multiple" id="user">
								@foreach($users as $user)
								<option value="{{ $user->id}}"
									{{ (in_array($user, old('user', []))) }} >{{ $user->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-xs-6">
			            <div class="text-center">
							<label for="Fecha_inicial" style="font-size: 1.5em">Fecha inicial</label>
						</div>

			            <div class="col-md-12 text-center" >
			                <input id="Fecha_inicial" type="date" name="Fecha_inicial" class="form-control" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
				        </div>
			        </div>

		            <div class="col-xs-6">
			            <div class="text-center">
							<label for="Fecha_final" style="font-size: 1.5em">Fecha final</label>
						</div>

			            <div class="col-md-12 text-center" >
			                <input id="Fecha_final" type="date" name="Fecha_final" class="form-control" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
				        </div>
			        </div>
		        </div>

		        <div class="col-xs-6">
						<div class="text-center">
							<label for="Hora" style="font-size: 1.5em">Hora</label>
						</div>

			            <div class=" text-center" >
			                <input id="Hora" type="time" name="Hora" class="form-control" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
				        </div>

					</div>

		        </div>

				<div class="col-xs-12">
						<div class="text-center">
							<label for="Rep" style="font-size: 1.5em">Repetir los días:</label>
						</div>

			            <div class="col-md-3 text-center" >
			                <input id="Rep_monday" type="checkbox" name="Rep_monday" value="1" > <span for="Rep" style="font-size: 1.5em">Lunes</span> </input>
				        </div>

				        <div class="col-md-3 text-center" >
			                <input id="Rep_tuesday" type="checkbox" name="Rep_tuesday" value="1" > <span for="Rep" style="font-size: 1.5em">Martes</span> </input>
				        </div>

				        <div class="col-md-3 text-center" >
			                <input id="Rep_wednesday" type="checkbox" name="Rep_wednesday" value="1" > <span for="Rep" style="font-size: 1.5em">Miércoles</span> </input>
				        </div>

				        <div class="col-md-3 text-center" >
			                <input id="Rep_thursday" type="checkbox" name="Rep_thursday" value="1" > <span for="Rep" style="font-size: 1.5em">Jueves</span> </input>
				        </div>

						<div class="col-md-4 text-center" >
			                <input id="Rep_friday" type="checkbox" name="Rep_friday" value="1" > <span for="Rep" style="font-size: 1.5em">Viernes</span> </input>
				        </div>

				        <div class="col-md-4 text-center" >
			                <input id="Rep_saturday" type="checkbox" name="Rep_saturday" value="1" > <span for="Rep" style="font-size: 1.5em">Sábado</span> </input>
				        </div>

				        <div class="col-md-4 text-center" >
			                <input id="Rep_sunday" type="checkbox" name="Rep_sunday" value="1" > <span for="Rep" style="font-size: 1.5em">Domingo</span> </input>
				        </div>
					</div>

		        </div>

		        <br>

		        <div class="form-group col-md-12">
					<div class="text-center">
						<label for="Comentarios" style="font-size: 1.5em">Comentarios</label>
					</div>
					<div class="col-md-12">
						<textarea style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 1px solid #59c4c5; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Comentarios" name="Comentarios" placeholder="Comentarios de la nueva tarea" value=""></textarea>
					</div>
				</div>

		        <br>


			</div>
		</div>

		<br>
		<br>
		<br>

		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="font-size: 1.5em">Guardar</button>
		</div>
	</div>
</div>
