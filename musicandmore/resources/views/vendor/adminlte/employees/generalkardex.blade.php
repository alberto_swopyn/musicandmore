@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')


<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h3 class="box-title" style="font-size: 2em; margin: .5em">Empleados</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-offset-4 col-md-8 col-sm-4 col-xs-4" style="text-align:right; margin-bottom: 1em;">
							<i class="glyphicon glyphicon-search"></i>
							<input type="text" value="" data-table="tableEmployees" class="search_employees" placeholder="Buscar"/>
						</div>
						<div class="table-responsive col-xs-12">
							<table class="table table-striped tablesorter" id="tableEmployees">
								<thead style="background: black; color: white;">
									<tr>
										<th class="text-center">Empleado</th>
										<th class="text-center">Puesto</th>
										<th class="text-center">Zona</th>
										<th class="text-center">Centro de trabajo</th>
										<th class="text-center">Cursos</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($employee as $e)
									<tr>
										<td>{{ $e->name }}
											<p class="text-default">&nbsp;&nbsp;
												<strong>
													<i class="fa fa-envelope-o" aria-hidden="true"></i>
												</strong> &nbsp; 
												<a href="mailto:{{ $e->email }}">{{ $e->email }} </a>
											</p>
										</td>
										<td class="text-center">{{ $e-> jobTitleName}}</td>
										<td class="text-center">
											@foreach($zones as $z)
											@if($z->id == $e->Zone)
											{{ $z->text }}
											@endif
											@endforeach
										</td>
										<td class="text-center">{{ $e-> jobCenter }}</td>
										<td class="text-center">
											@if($e->course_percentage == null)
											<button type="button" class="btn btn-danger" aria-label="Left Align" disabled="true" style="margin-top: .7em; margin-left: 1em;">
												<i class="fa fa-ban" aria-hidden="true"></i>
											Sin cursos</button>
											@elseif($e->course_percentage <= 29)
											<a href="{{route('user_trainings', ['user' => $e->employee_id])}}" type="button" class="btn btn-primary" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;">
												<i class="fa fa-spinner" aria-hidden="true"></i>
												{{$e->course_percentage}}
											</a>
											@elseif($e->course_percentage >= 30 and $e->course_percentage <= 69)
											<a href="{{route('user_trainings', ['user' => $e->employee_id])}}" type="button" class="btn btn-warning" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;">
												<i class="fa fa-spinner" aria-hidden="true"></i>
												{{$e->course_percentage}}</a>
												@elseif($e->course_percentage >= 70 and $e->course_percentage <= 99)
												<a href="{{route('user_trainings', ['user' => $e->employee_id])}}" type="button" class="btn btn-success" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;">
													<i class="fa fa-spinner" aria-hidden="true"></i>
													{{$e->course_percentage}}</a>	
													@elseif($e->course_percentage == 100)
													<a href="{{route('user_trainings', ['user' => $e->employee_id])}}" type="button" class="btn btn-info" aria-label="Left Align" style="margin-top: .7em; margin-left: 1em;">
														<i class="fa fa-check" aria-hidden="true"></i>
														{{$e->course_percentage}}</a>	
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
								</div>
								<div class="text-center">

								</div>

							</div>
							<!--box-body-->
						</div>
						<!--.box-->
					</div>
				</div>
			</div>


			@endsection
			@section('personal-js')
			<style>

			table.tablesorter thead tr .header {
				background-image: url(../img/bg.gif);
				background-repeat: no-repeat;
				background-position: center right;
				cursor: pointer;
			}
			table.tablesorter thead tr .headerSortUp {
				background-image: url(../img/asc.gif);
			}
			table.tablesorter thead tr .headerSortDown {
				background-image: url(../img/desc.gif);
			}
		</style>
		<!-- <link rel="stylesheet" href="{{ asset('css/style_sorterTable.css') }}"> -->
		<script src="{{ asset('js/jquery.tablesorter.js') }}"></script>
		<script>
    // call the tablesorter plugin 
    $("#tableEmployees").tablesorter( {sortList: [[0,0], [1,0]]} ); 
</script>
@stop