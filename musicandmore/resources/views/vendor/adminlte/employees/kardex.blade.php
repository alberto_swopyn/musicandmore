@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')


<div class="container-fluid spark-screen" id="kardex_Employee">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h3 class="box-title" style="font-size: 2em; margin: .5em">Empleados</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="row" style="margin-bottom: 1em;">
						<div class="text-right col-md-12">
							<!-- <a href="{{ route('index_employees', [ 'id' => 0 ]) }}" class="btn btn-success btn-lg"><i class="fa fa-user-plus" aria-hidden="true"></i> Crear Usuario</a> -->
							<a href="#newEmployees" id="newEmployee" class="btn btn-success" style="background-color: black; border-color: black;" data-toggle="modal"><i class="fa fa-user-plus" aria-hidden="true"></i> Crear Usuario</a>
						</div>
					</div>
					<div class="row">
						<div class="table-responsive col-xs-12">
							<table class="table table-striped tablesorter" id="tableEmployees">
								<thead style="background: black; color: white;">
									<tr>
										<th class="text-center">Empleado</th>
										<th class="text-center">Puesto</th>
										<th class="text-center">Kardex</th>
										<th class="text-center">Zona</th>
										<th class="text-center">Centro de trabajo</th>
										<th class="text-center" colspan="2">Fecha de ingreso</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($employee as $e)
									<tr>
										<td>{{ $e->name }}
											<p class="text-default">&nbsp;&nbsp;
												<strong>
													<i class="fa fa-envelope-o" aria-hidden="true"></i>
												</strong> &nbsp; 
												<a href="mailto:{{ $e->email }}">{{ $e->email }} </a>
											</p>
										</td>
										<!-- <td class="text-center" style="font-weight: bold">{{ $e->company }}</td> -->
										<td class="text-center">{{ $e-> jobTitleName}}</td>
										<td class="text-center"><a href="{{ route('user_trainings',[ '$user' => $e->employee_id]) }}"><i class="fa fa-building" style="color: #1bb49a" aria-hidden="true"></i></a></td>
										<td class="text-center">
											@foreach($zones as $z)
											@if($z->id == $e->Zone)
											{{ $z->text }}
											@endif
											@endforeach
										</td>

										
										<td class="text-center">{{ $e-> jobCenter }}</td>
										<td class="text-center" class="text-center">
											@php
											echo date("d-m-Y", strtotime($e->created_at));
											@endphp
										</td>
										<td>
											<!-- <a href="{{ route('index_employees', ['id' => $e->id]) }}" class="btn"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a> -->
											<a href="#updateEmployees" class="btnUpdateEmployee" data-toggle="modal" data-name="{{ $e->name }}" data-id="{{ $e->employee_id }}">
												<i class="fa fa-pencil-square-o fa-2x" style="color: #1bb49a" aria-hidden="true"></i>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="text-center">

					</div>
				</div>
				<!--box-body-->
			</div>
			<!--.box-->
		</div>
	</div>
</div>

<!-- START NEW EMPLOYEE MODAL -->
<div class="container-fluid spark-screen">
	<div class="modal fade" id="newEmployees" role="dialog" aria-labelledby="newEmployeesModal" style="background:transparent;">
		<div class="row">
			<div class="col-md-12">
				<!--Default box-->
				<div class="modal-dialog modal-lg" role="document">
					
					<div class="modal-content">
						<div class="box">
							<div class="modal-header">
								<div class="box-header">
									<button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
										<span aria-hidden="true">&times;</span>
									</button>
									<h3 style="color: black" class="modal-title text-center col-lg-12" id="modalTitle">Agregar empleado</h3>
								</div>
							</div>
							<div class="box-body">
								<div class="form-group row col-lg-push-8 col-lg-4">
									<label class="control-label col-sm-5" for="name">Número/Clave empleado: </label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="keyEmployee" maxlength="10">
									</div>
								</div>
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="name">Nombre: </label>
									<div class="col-sm-9">
										<select name="selectEmployee" id="selectEmployee" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;">
											@foreach($users as $us)
											<option value="{{ $us->id }}">
												{{ $us->name }}
											</option>
											@endforeach
										</select>

									</div>
								</div>
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="company">Empresa: </label>
									<div class="col-sm-9"> 
										<input type="text" class="form-control" id="newCompany" @if(isset($e)) value="{{ $e->company }}" disabled="true" @endif>
									</div>
								</div>
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="jobTitle">Puesto: </label>
									<div class="col-sm-9"> 
										<select name="inputjobTitle" id="inputjobTitle" class="jobTitleList-single form-control" style="width: 100%; font-weight: bold;">
											<option value="">Selecciona puesto</option>
											@foreach($jobTitle as $jt)
											<option value="{{ $jt->id }}">{{ $jt->job_title_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="jobTitle">Centro de Trabajo: </label>
									<div class="col-sm-9"> 
										<select name="inputJobCenter" id="inputJobCenter" class="jobCenterList-single form-control" style="width: 100%; font-weight: bold;">
											<option value="">Selecciona centro de trabajo</option>
											@foreach($jobCenter as $jc)
											<option value="{{ $jc->id }}">{{ $jc->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<div class="text-center">
									<button class="btn btn-primary btn-lg" style="background-color: black; border-color: black;" type="button" id="EmployeeNew">Guardar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END NEW EMPLOYEE MODAL -->

<!-- START UPDATE EMPLOYYE MODAL -->
<div class="container-fluid spark-screen">
	<div class="modal fade" id="updateEmployees" role="dialog" aria-labelledby="updateEmployeesModal" style="background:transparent;">
		<div class="row">
			<div class="col-md-12">
				<!--Default box-->
				<div class="modal-dialog modal-lg" role="document">
					<input type="hidden" id="Applicant_id">
					<div class="modal-content">
						<div class="box">
							<div class="modal-header">
								<div class="box-header">
									<button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
										<span aria-hidden="true">&times;</span>
									</button>
									<h3 style="color: black" class="modal-title text-center col-lg-12" id="modalTitle">Actualizar empleado</h3>
								</div>
							</div>
							<div class="box-body">
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="name">Nombre: </label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="nameEmployee" name="nameEmployee">
									</div>
								</div>
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="company">Empresa: </label>
									<div class="col-sm-9"> 
										<input type="text" class="form-control" id="inputCompany" @if(isset($e)) value="{{ $e->company }}" disabled="true" @endif>
									</div>
								</div>
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="jobTitle">Puesto: </label>
									<div class="col-sm-9"> 
										<select name="updatejobTitle" id="updatejobTitle" class="jobTitleList-single form-control" style="width: 100%; font-weight: bold;">
											<option value="">Selecciona puesto</option>
											@foreach($jobTitle as $jt)
											<option value="{{ $jt->id }}">{{ $jt->job_title_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="jobTitle">Centro de Trabajo: </label>
									<div class="col-sm-9"> 
										<select name="updateJobCenter" id="updateJobCenter" class="jobCenterList-single form-control" style="width: 100%; font-weight: bold;">
											<option value="">Selecciona centro de trabajo</option>
											@foreach($jobCenter as $jc)
											<option value="{{ $jc->id }}">{{ $jc->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<div class="text-center">
									<button class="btn btn-primary" style="background-color: black; border-color: black;" type="button" id="EmployeeUp">Actualizar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END UPDATE EMPLOYYE MODAL -->

@endsection
@section('personal-js')
<style>

table.tablesorter thead tr .header {
	background-image: url(../img/bg.gif);
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}
table.tablesorter thead tr .headerSortUp {
	background-image: url(../img/asc.gif);
}
table.tablesorter thead tr .headerSortDown {
	background-image: url(../img/desc.gif);
}
</style>
<!-- <link rel="stylesheet" href="{{ asset('css/style_sorterTable.css') }}"> -->
<script src="{{ asset('js/jquery.tablesorter.js') }}"></script>
<script>
    // call the tablesorter plugin 
    $("#tableEmployees").tablesorter( {sortList: [[0,0], [1,0]]} ); 
</script>
<script>
	$(".applicantsList-single").select2();
	$(".applicantsList-single").change(function(event) {
		$('#Applicant_id').val($(this).val());
	});
	$(".jobTitleList-single").select2();
	$(".jobCenterList-single").select2();
</script>
@stop