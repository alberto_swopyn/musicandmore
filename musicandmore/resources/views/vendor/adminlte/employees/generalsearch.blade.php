@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')



<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box-->
			<div class="box">
				<div class="box-header with-border text-center">
					<h3 class="box-title" style="font-size: 2em; margin: .5em">KARDEX</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-12" style="text-align:right; margin-bottom: 1em;">
							<div class="col-md-6 text-right">
								<i class="glyphicon glyphicon-search"></i>
								<input type="text" value="" data-table="tableEmployees" class="search_employees" placeholder="Buscar"/>
							</div>
						</div>
						<div class="table-responsive col-xs-10 col-xs-push-1">
							<table class="table table-striped tablesorter" id="tableEmployees">
								<thead style="background: black; color: white;">
									<tr>
										<th class="text-center">Centro de Trabajo</th>
									</tr>
								</thead>
								<tbody>

									<td>



									<div id="jstree_kardex" class="demo">
									<ul class="list-group">
									@foreach($column as $raw)
									@if($raw['promedio'] >= 0 && $raw['promedio'] <= 24)
										<li class="list-group-item d-flex justify-content-between align-items-center"> {{$raw['text']}}

												&nbsp;<span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$raw['promedio'], 2, '.', '')}} %</span>
									@elseif($raw['promedio'] >=25 && $raw['promedio'] <= 74)
									<li class="list-group-item d-flex justify-content-between align-items-center"> {{$raw['text']}}

												&nbsp;<span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$raw['promedio'], 2, '.', '')}} %</span>
									@elseif($raw['promedio'] >=75 && $raw['promedio'] <=99)
									<li class="list-group-item d-flex justify-content-between align-items-center"> {{$raw['text']}}

												&nbsp;<span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$raw['promedio'], 2, '.', '')}} %</span>
									@elseif($raw['promedio'] == 100)
									<li class="list-group-item d-flex justify-content-between align-items-center"> {{$raw['text']}}

												&nbsp;<span style="font-size: 0.5em;" class="btn btn-success btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$raw['promedio'], 2, '.', '')}} %</span>
									@endif
												<ul class="list-group">

														@foreach($zonas as $zone)
															@if($zone['parent_id'] == $raw['id'])
															@if($zone['promedio'] >= 0 && $zone['promedio'] <=24)
															<li class="list-group-item d-flex justify-content-between align-items-center">

																{{$zone['text']}}

																<span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="btn btn-danger btn-xs" class="border border-primary badge badge-primary badge-pill">{{number_format((float)$zone['promedio'], 2, '.', '')}} %</span>


														@elseif($zone['promedio'] >=25 && $zone['promedio'] <=74)
															<li class="list-group-item d-flex justify-content-between align-items-center">

																{{$zone['text']}}

																<span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="btn btn-danger btn-xs" class="border border-primary badge badge-primary badge-pill">{{number_format((float)$zone['promedio'], 2, '.', '')}} %</span>


														@elseif($zone['promedio'] >=75 && $zone['promedio'] <=99)
															<li class="list-group-item d-flex justify-content-between align-items-center">

																{{$zone['text']}}

																<span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="btn btn-danger btn-xs" class="border border-primary badge badge-primary badge-pill">{{number_format((float)$zone['promedio'], 2, '.', '')}} %</span>


														@elseif($zone['promedio'] == 100)
															<li class="list-group-item d-flex justify-content-between align-items-center">

																{{$zone['text']}}

																<span style="font-size: 0.5em;" class="btn btn-success btn-xs" class="btn btn-danger btn-xs" class="border border-primary badge badge-primary badge-pill">{{number_format((float)$zone['promedio'], 2, '.', '')}} %</span>


															@endif

																<ul class="list-group">
																	@foreach($determinantes as $det)
																	@if($det['parent_id'] == $zone['id'])
																	@if($det['promedio'] >= 0 && $det['promedio'] <=24)
																	<li class="list-group-item d-flex justify-content-between align-items-center">
																		{{$det['text']}}

																		<span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$det['promedio'], 2, '.', '')}} %</span>

																	@elseif($det['promedio'] >= 25 && $det['promedio'] <=74)

																	<li class="list-group-item d-flex justify-content-between align-items-center">
																		{{$det['text']}}

																		<span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$det['promedio'], 2, '.', '')}} %</span>

																	@elseif($zone['promedio'] >=75 && $zone['promedio'] <=99)

																	<li class="list-group-item d-flex justify-content-between align-items-center">
																		{{$det['text']}}

																		<span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$det['promedio'], 2, '.', '')}} %</span>

																	@elseif($zone['promedio'] == 100)

																	<li class="list-group-item d-flex justify-content-between align-items-center">
																		{{$det['text']}}

																		<span style="font-size: 0.5em;" class="btn btn-success btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$det['promedio'], 2, '.', '')}} %</span>

																	@endif

																		<ul class="list-group">
																			@foreach($sucursal as $suc)
																			@if($suc['parent_id'] == $det['id'])
																			@if($suc['promedio'] >= 0 && $suc['promedio'] <= 24)
																			<li class="list-group-item d-flex justify-content-between align-items-center">
																				{{$suc['text']}}

																				<span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$suc['promedio'], 2, '.', '')}} %</span>

																			@elseif($suc['promedio'] >=25 && $suc['promedio'] <= 74)
																			<li class="list-group-item d-flex justify-content-between align-items-center">
																				{{$suc['text']}}

																				<span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$suc['promedio'], 2, '.', '')}} %</span>

																			@elseif($suc['promedio'] >=75 && $suc['promedio'] <= 99)
																			<li class="list-group-item d-flex justify-content-between align-items-center">
																				{{$suc['text']}}

																				<span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$suc['promedio'], 2, '.', '')}} %</span>

																			@elseif($suc['promedio'] == 100)
																			<li class="list-group-item d-flex justify-content-between align-items-center">
																				{{$suc['text']}}

																				<span class="btn btn-success btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$suc['promedio'], 2, '.', '')}} %</span>

																			@endif

																				<ul class="list-group">
																					@foreach($empleado as $emp)
																					@if($emp['parent_id'] == $suc['id'])
																					@if($emp['promedio'] >= 0 && $emp['promedio'] <= 24)
																					<li class="list-group-item d-flex justify-content-between align-items-center">
																						{{$emp['text']}}

																					<span style="font-size: 0.5em;" class="btn btn-danger btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$emp['promedio'], 2, '.', '')}} %</span>

																			@elseif($emp['promedio'] >= 25 && $emp['promedio'] <= 74)

																					<li class="list-group-item d-flex justify-content-between align-items-center">
																						{{$emp['text']}}

																					<span style="font-size: 0.5em;" class="btn btn-warning btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$emp['promedio'], 2, '.', '')}} %</span>

																			@elseif($emp['promedio'] >= 75 && $emp['promedio'] <= 99)
																					<li class="list-group-item d-flex justify-content-between align-items-center">
																						{{$emp['text']}}

																					<span style="font-size: 0.5em;" class="btn btn-primary btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$emp['promedio'], 2, '.', '')}} %</span>

																			@elseif($emp['promedio'] == 100)
																					<li class="list-group-item d-flex justify-content-between align-items-center">
																						{{$emp['text']}}

																					<span style="font-size: 0.5em;" class="btn btn-success btn-xs" class="badge badge-primary badge-pill">{{number_format((float)$emp['promedio'], 2, '.', '')}} %</span>

																					</li>

																					@endif
																					@endif
																					@endforeach
																				</ul>

																			</li>
																		@endif
																			@endforeach
																		</ul>

																	</li>
																@endif
																	@endforeach
																</ul>

															</li>
														@endif
															@endforeach
														</ul>

													</li>
													
													@endforeach
												</ul>

											</div>
										</td>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!--box-body-->
				</div>
				<!--.box-->
			</div>
		</div>
	</div>



@endsection
@section('personal-js')
<style>

table.tablesorter thead tr .header {
	background-image: url(../img/bg.gif);
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}
table.tablesorter thead tr .headerSortUp {
	background-image: url(../img/asc.gif);
}
table.tablesorter thead tr .headerSortDown {
	background-image: url(../img/desc.gif);
}
</style>
<!-- <link rel="stylesheet" href="{{ asset('css/style_sorterTable.css') }}"> -->
<script src="{{ asset('js/jquery.tablesorter.js') }}"></script>
<script>
    // call the tablesorter plugin
    $("#tableEmployees").tablesorter( {sortList: [[0,0], [1,0]]} );
</script>
@stop
