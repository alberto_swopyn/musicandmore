@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('personal_style')
@include('adminlte::training.PersonalSelect2')
@stop

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h2 class="box-title">PLAN DE CAPACITACIÓN</h2>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					@include('adminlte::layouts.partials._errors')
					<form action="{{ Route('training_update_assign', ['trainingplan' => $trainingplan->id]) }}" method="POST" role="form" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
						<!-- <span style="font-size: 2em">Actualizar</span> -->
						<div class="col-md-12">
							<div class="panel panel-info">
								<table class="table table-hover table table-striped">
									<thead style="background: black; color: white;">
										<tr>
											<th class="text-center">Actualizar</th>
										</tr>
									</thead>
								</table>
								<br>
								<div class="panel-body">
									<div class="col-md-12" style="bottom: 10px">
										<!-- <div class="form-group"> -->
											<div class="col-md-12 text-center">
												<label for="Nombre" style="font-size: 1.5em">Nombre del plan</label>
											</div>
											<div class="col-md-8 col-md-offset-2">
												<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Bienvenida Auxiliar" value="{{ $trainingplan->name or old('Nombre') }}" style="border: 1px solid black; font-weight: bold; font-size: 1.5em; border-radius: 7px">
											</div>
											<!-- </div> -->
										</div>
										<br>
										<br>

										<div class="col-md-12">
											<div class="col-md-12 text-center">
												<label for="Descripcion" style="font-size: 1.5em">Descripción</label>
											</div>

											<div class="col-md-12">
												<input style="border: 1px solid black; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Descripcion" name="Descripcion" placeholder="Un saludo y una sonrisa es lo que todos nos merecemos ..." value="{{ $trainingplan->description or old('Descripcion') }}" >
												<br>
											</div>
										</div>
										<br>
										<br>
										
										<div class="form-group col-md-12">
											<div class="col-md-12 text-center">
												<label for="module" style="font-size: 1.5em">Asignación de Módulos</label>
											</div>
											<div class="col-md-8 col-md-offset-2">
												<select name="module[]" multiple="multiple" class="form-control multiple" id="module">
													@foreach($modules as $module)
														<option value="{{ $module->id}}" 
														@foreach( $modulesplan as $moduleplan)
														@if($moduleplan->id == $module->id)
														selected = 'selected'
														@endif
														@endforeach	
														{{ (in_array($module, old('module', []))) }} >{{ $module->name }}
														</option>
													@endforeach
												</select>
											</div>
										</div>
										<br>
										<br>

										<div class="col-md-12 text-center">
											<button type="submit" class="btn btn-primary" style=" background-color: black; border-color: black; font-size: 1.5em">Actualizar</button>
										</div>
											</div>
											<!-- /.box-body -->
										</div>
										<!-- /.box -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endsection
			@section('scripts')
			<script>
				$(".multiple").select2();
			</script>
			@endsection