<div class="panel panel-primary" style="border-color: #bce8f1;">
	<table class="table table-hover table table-striped">
		<thead style="background: black; color: white;">
			<tr>
				<th class="text-center">Nuevo Módulo</th>
			</tr>
	    </thead>
	 </table>
	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a> 
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a> 
	</div>
	
	<div class="panel-body">


		<div class="form-group col-md-12">
			<div class="col-md-12 text-center">
				<label for="Nombre" style="font-size: 1.5em">Nombre del módulo</label>
			</div>
			<div class="col-md-8 col-md-offset-2">
				<input type="text" class="form-control" id="moduleName" name="moduleName" placeholder="Módulo..." style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
			</div>
		</div>

		<div class="form-group col-md-12">
			<div class="col-md-12 text-center">
				<label for="Nombre" style="font-size: 1.5em">Asignación de Cursos</label>
			</div>
			<div class="col-md-8 col-md-offset-2">
				<select required="required" name="course[]" multiple="multiple" class="form-control multiple" id="course">
					@foreach($trainings as $training)
						<option value="{{ $training->id}}" {{ (in_array($training, old('course', []))) }} >{{ $training->title }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-12 text-center">
				<br>
				<button id="ModuleNew" class="btn btn-primary" style="background-color: black; border-color: black; font-size: 1.5em">Guardar</button>
			</div>
			<br>
		</div>

	</div>
</div>

<div class="panel panel-primary" style="border-color: #bce8f1;">

	<h3 class="text-center">Módulos Creados</h3>

	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a>
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a>
	</div>

	<div class="panel-body">

		<div class="table-responsive">
			<table class="table table-hover table table-striped">
				<thead><tr>
					<th class="tableCourse">Nombre</th>
					<th>Detalle</th>
					<th>Actualizar</th>
					<th>Eliminar</th>
				</tr></thead>
				<tbody>
				@foreach($modules as $module)
					<tr>
						<td>{{ $module->name }}</td>
						<td>
							<a href="{{ route('module_personal_study', ['id'=> $module->id]) }}" class="btn">
								<i class="fa fa-eye fa-2x" aria-hidden="true" style="color: #00AB7F;"></i>
							</a>
						</td>
						<td>
							<a href="{{ route('module_edit', ['module'=> $module->id]) }}" class="btn">
								<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
							</a>
						</td>
						<td>
							<form action="{{ route('delete_module', ['module' => $module->id]) }}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button type="submit" class="btn" style="background-color: transparent;">
									<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
								</button>
							</form>
						</td>
					</tr>
				@endforeach
				</tbody>

			</table>
		</div>
		{{ $modules->links() }}

	</div>
</div>





