@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Capacitaci&oacute;n</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i
                                ></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <center>
                                <div class="embed-responsive embed-responsive-16by9 pull-center">
                                  <iframe  class="embed-responsive-item" id="presentation"  src="{{ asset($training->course) }}"></iframe>
                                  <input type="hidden" id="training_id" name="training_id" value="{{ $training->id}}">
                              </div>
                          </center>

                <table width="100%" >
                    <tr>
                        <tr>
                            <td colspan="3" align="center">
                                <hr>
                                <form action="{{ route('training_study') }}" method="GET" role="form">
                                    {{ csrf_field() }}
                                    <input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
                                    <input type="hidden" id="email_user" name="email_user" value="{{ auth()->user()->email }}">
                                    <button type="submit" class="btn btn-primary hidden" aria-label="Left Align" id="btnCourse" >Finalizar
                                      <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
                                  </button>
                              </form>
                          </td>
                      </tr>
                  </table>
              </div>
          </div>
          <!-- /.box-body -->
      </div>
      <!-- /.box -->

  </div>
</div>
</div>


<script>
    var ispringPresentationConnector = {};

    truncateDecimals = function (number) {
        return Math[number < 0 ? 'ceil' : 'floor'](number);
    };

    ispringPresentationConnector.register = function(player) {
        var presentation = player.presentation();
        var playbackController = player.view().playbackController();
        var slidesCount = presentation.slides().count();
        initPlaybackControls(playbackController, slidesCount);
    };


    function initPlaybackControls(playbackController, slidesCount) {
        var prevBtn = document.getElementById("button-prev");
        prevBtn.onclick = function() {
            playbackController.gotoPreviousSlide();
        };

        var nextBtn = document.getElementById("button-next");
        nextBtn.onclick = function() {
            playbackController.gotoNextSlide();

        };

        var slideCountLabel = document.getElementById("slide-count");
        slideCountLabel.innerHTML = slidesCount.toString();
        var slideNoInput = document.getElementById("current-slide");

        playbackController.slideChangeEvent().addHandler(function(slideIndex)
        {
            slideNoInput.innerHTML = (slideIndex + 1).toString();
            slideNoInput.value = (slideIndex + 1).toString();

            $(function(){
                    // var valor=slideNoInput.value;
                    // alert(valor);
                    var slider = truncateDecimals(((slideNoInput.value *100) / slidesCount),2);

                    $('#progressControl').empty();
                    $('#progressControl').append('<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="'+slider+' " aria-valuemin="0" aria-valuemax="100" style="width: '+slider+'%" >'+slider+'</div>');


                    $('#progressControlCourse').empty();
                    $.ajax({
                      type: 'POST',
                      url:  "{{ route( 'training_progress' ) }}",
                      data: { 
                        '_token': $('meta[name=csrf-token]').attr('content'),
                        user: $('#user').val(), 
                        training_id : $('#training_id').val(),
                        slider: (slideNoInput.value *100) / slidesCount }
                    })
                    .done(function( msg ) {
                        $('#progressControlCourse').append('<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="'+slider+' " aria-valuemin="0" aria-valuemax="100" style="width: '+slider+'%" >'+slider+'</div>');
                    });
                });

            if(slidesCount == slideNoInput.value){

                $('#btnCourse').removeClass('hidden');
                $('#button-next').addClass('hidden');
            }
            else
            {
                $('#button-next').removeClass('hidden');
                $('#btnCourse').addClass('hidden');
            }
        });

        slideNoInput.onchange = function()
        {

            var currentSlideIndex = playbackController.currentSlideIndex();
            var nextSlideIndex = parseInt(slideNoInput.value) - 1;
            if (nextSlideIndex >= 0 && nextSlideIndex < slidesCount)
            {
                playbackController.gotoSlide(nextSlideIndex);
            }
            else
            {
                slideNoInput.value = (currentSlideIndex + 1).toString();
            }
        };
    }
</script>



@endsection