<div class="panel panel-primary" style="border-color: #bce8f1;">
	<table class="table table-hover table table-striped">
		<thead style="background: black; color: white;">
			<tr>
				<th class="text-center">Nuevo Plan de Capacitación</th>
			</tr>
	    </thead>
	 </table>
	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a> 
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a> 
	</div>
	
	<div class="panel-body">


		<div class="form-group col-md-12">

			<div class="col-md-12 text-center">
				<label for="Nombre" style="font-size: 1.5em">Nombre del plan</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Bienvenida Auxiliar" value="{{ $trainingplan->name or old('Nombre') }}"  style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
			</div>

		</div>

		<div class="form-group col-md-12">
			<div class="text-center">
				<label for="Descripcion" style="font-size: 1.5em">Descripción</label>
			</div>
			<div class="col-md-12">
				<textarea required="required" style="height:75px; max-height: 250px; max-width: 61em; min-width: 370; min-height: 75px; border: 0.5px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" type="text" class="form-control" id="Descripcion" name="Descripcion" placeholder="Un saludo y una sonrisa es lo que todos nos nerecemos ..." value="{{ $trainingplan->description or old('Descripcion') }}"></textarea>
			</div>
		</div>

		<div class="form-group col-md-12">

			<div class="row">
				<div class="col-md-12 text-center">
					<label for="module" style="font-size: 1.5em">Asignación de Módulos</label>
				</div>

				<a href="{{ route('new_module') }}" title="Nuevo Módulo" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a;">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"> </span>
				</a>

				<div class="col-md-8 col-md-offset-2">
					<select required="required" name="module[]" multiple="multiple" class="form-control multiple" id="module">
						@foreach($modules as $module)
							<option value="{{ $module->id}}" {{ (in_array($module, old('module', []))) }} >{{ $module->name }}</option>
						@endforeach
					</select>
				</div>
			</div>

		</div>

		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black; font-size: 1.5em">Guardar</button>
		</div>
	</div>
</div>

<style>

.select2 {
width:100%!important;
}

.select2-container--default .select2-selection--multiple .select2-selection__rendered li {
	background-color: white;
	color: black;
	font-weight: bold;
	border: none;
	font-size: 1.5em;
	margin: .5em;
}
span.select2-selection.select2-selection--multiple{border: 1px solid black;}
input:focus {
    outline: none !important;
    border-color: #f5216e;
    box-shadow: 0 0 10px #f5216e;
}

.input-group .select2-container {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0;
  display: table;
  table-layout: fixed;
}

</style>

