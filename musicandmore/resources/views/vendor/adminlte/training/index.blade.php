@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h2 class="titleCenter">Cursos</h2>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
						<div class="col-md-2 col-md-offset-10">	
						<a href="{{ route('training_create') }}" class="btn btn-success btn-lg" style="background-color: black; border-color: black;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span> Curso</a>
						</div>
					</div>
					<br>
						<div class="table-responsive">
						  <table class="table table-hover table table table-striped">
						    <thead style="background: black; color: white;"><tr>
						    	<th class="tableCourse" style="color: white">Titulo</th>
						    	<th class="tableCourse" style="color: white">Duración</th>
						    	<th class="tableCourse" style="color: white">Descripción</th>
						    	<th class="tableCourse" style="color: white">Fecha de Inicio</th>
						    	<th class="tableCourse" style="color: white">Fecha de Fin</th>
						    	<th></th>
						    	<th></th>
						    	<th></th>
						    	<th></th>
						    </tr>
							<tbody>
								@foreach($trainings as $training)
								<tr>
								<td>{{ $training->title }}</td>
								<td style="text-align: center">{{ $training->duration }}</td>
								<td>{{ $training->description }}</td>
								<td>{{ $training->start }}</td>
								<td>{{ $training->end }}</td>
								<td>
									<a href="{{ asset($training->course) }}" target="_blank" class="btn btn-success" style="background-color: black; border-color: black;" ><span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span></a>
								</td>
								<td>
									<a href="{{ route('training_edit', ['training'=> $training->id]) }}" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a;"><span class="glyphicon glyphicon-edit" aria-hidden="true"> </span></a>
								</td>
								<td>
									<a href="{{ route('kardex_question_evaluation', [ '$course' => $training->id ]) }}"><button type="submit" class="btn btn-default" style="background-color: #1bb49a; border-color: #1bb49a;"><span class="glyphicon glyphicon-tasks" style="color: white;" aria-hidden="true"></span></button></a>
								</td>
								<td>
									<form action="{{ route('training_delete', ['training' => $training->id]) }}" method="POST">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></button>
									</form>	
								</td>
								</tr>
								@endforeach
							</tbody>
						</thead>
					</table>
					</div>
					 {{ $trainings->links() }}
											
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>

		</div>
	</div>
@endsection
