<div class="panel panel-primary" style="border-color: #bce8f1;">
    <table class="table table-hover table table-striped">
        <thead style="background: black; color: white;">
            <tr>
                <th class="text-center">
                    Asignación de Planes
                </th>
            </tr>
        </thead>
    </table>
    <div class="pull-right">
        <a data-perform="panel-collapse" href="javascript:void(0)">
            <i class="ti-minus">
            </i>
        </a>
        <a data-perform="panel-dismiss" href="javascript:void(0)">
            <i class="ti-close">
            </i>
        </a>
    </div>
    <div class="panel-body">
        <div class="col-md-offset-1 col-md-10" style="top: 10px">
            <div class="col-md-12">
                <div class="text-center">
                    <h3>
                        Asignación
                    </h3>
                </div>
                <div>
                    <span style="font-size: 1.5em">
                        Nombres
                    </span>
                    <div class="form-group">
                        <div class="input-group col-md-offset-1 col-md-11">
                            <span class="input-group-addon" style="border: none; width: 1em">
                                <i aria-hidden="true" class="fa fa-user" style="font-size: 2em; color: black;">
                                </i>
                            </span>
                            <select class="form-control multiple" id="users" multiple="multiple" name="users[]">
                                @foreach($users as $user)
                                <option value="{{ $user->id }}" {{ (in_array($user, old('users', []))) }}>
                                    {{ $user->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div>
                    <span style="font-size: 1.5em">Planes</span>
                    <div class="form-group">
                        <div class="input-group col-md-offset-1 col-md-11">
                            <span class="input-group-addon" style="border: none; width: 1em;">
                                <i aria-hidden="true" class="fa fa-book" style="font-size: 2em; color: black;">
                                </i>
                            </span>
                            <select class="form-control multiple" id="plans" multiple="multiple" name="plans[]">
                                @foreach($plans as $plan)
                                <option value="{{ $plan->id }}" {{ (in_array($plan, old('plans', []))) }} >
                                    {{ $plan->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
            <br>
                <div class="col-md-12 text-center">
                    <button class="btn btn-primary" style="background-color: black; border-color: black; font-size: 1.5em" type="submit">
                        Asignar
                    </button>
                </div>
            </br>
        </br>
    </div>
</div>
<style>
    .select2 {
width:100%!important;
}

.select2-container--default .select2-selection--multiple .select2-selection__rendered li {
	background-color: white;
	color: black;
	font-weight: bold;
	border: none;
	font-size: 1.5em;
	margin: .5em;
}
span.select2-selection.select2-selection--multiple{border: 1px solid black;}
input:focus {
    outline: none !important;
    border-color: #f5216e;
    box-shadow: 0 0 10px #f5216e;
}

.input-group .select2-container {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0;
  display: table;
  table-layout: fixed;
}
</style>
