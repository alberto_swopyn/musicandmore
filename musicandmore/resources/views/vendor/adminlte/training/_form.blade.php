

<div class="form-group">
		<label for="Titulo">Titulo</label>
		<input type="text" class="form-control" id="Titulo" name="Titulo" required="required" 
		placeholder="Atención al Cliente" value="{{ $training->title or old('Titulo') }}">
</div>
							
<div class="form-group">
		<label for="Duracion">Duración</label>
		<input type="text" class="form-control" id="Duracion" name="Duracion" required="required" placeholder="14 minutos" value="{{ $training->duration or old('Duracion') }}">
</div>
							
<div class="form-group">
		<label for="Descripcion">Descripción</label>
		<input type="text" class="form-control" id="Descripcion" name="Descripcion" required="required" placeholder="Un saludo y una sonrisa es lo que todos nos nerecemos ..." value="{{ $training->description or old('Descripcion') }}">
</div>

<div class="form-group">
		<label for="Inicio">Fecha de inicio</label>
		<input type="date" class="form-control" id="Inicio" name="Inicio" required="required" placeholder="2017/01/01" value="{{ $training->start or old('Inicio') }}">
</div>
<div class="form-group">
		<label for="Fin">Fecha de fin</label>
		<input type="date" class="form-control" id="Fin" name="Fin" required="required" value="{{ $training->end or old('Fin') }}">
</div>
<div class="form-group">
	<label for="Curso">Archivo Zip del Curso</label>
	<input type="file" class="form-control" id="Curso" name="Curso" required="required" value="{{ $training->course or old('Course') }}">
</div>

<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>




