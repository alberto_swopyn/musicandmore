@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Sucursal</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						

						<div class="table-responsive">
						  <table class="table table-hover">
						    <thead><tr>
						    	<td>Nombre</td>
						    	<td>Dirección</td>
						    	<td>Teléfono</td>
						     	<td></td>
						    </tr></thead>
							<tbody>
								@foreach($branchs as $branch)
								<tr>
								<td>{{ $branch->name }}</td>
								<td>{{ $branch->bussines_name }}</td>
								<td>{{ $branch->phone }}</td>
								

								<td>
									<div class="row">
										<div class="col-md-2">
											<a href="{{ route('branch_edit', ['branch'=> $branch->id]) }}" class="btn btn-info"><span class="glyphicon glyphicon-edit" aria-hidden="true"> </span></a>
										</div>
										<div class="col-md-2">
											<form action="{{ route('branch_delete', ['branch' => $branch->id]) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></button>
											</form>
										</div>
									</div>
								</td>
								</tr>
								@endforeach
							</tbody>
	
						    
						  </table>
						</div>
						{{ $branchs->links() }}
											
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>


		</div>
	</div>
@endsection
