<div class="form-group">
		<label for="Nombre">Nombre</label>
		<input type="text" class="form-control" id="Nombre" name="Nombre" 
		placeholder="Swopyn" value="{{ $branch->name or old('Nombre') }}">
</div>
							
							
<div class="form-group">
		<label for="Direccion">Dirección</label>
		<input type="text" class="form-control" id="Direccion" name="Direccion" placeholder="Dimicilio fiscal" value="{{ $branch->address or old('Direccion') }}">
</div>

<div class="form-group">
		<label for="Telefono">Teléfono</label>
		<input type="text" class="form-control" id="Telefono" name="Telefono" placeholder="449 923 23 55" value="{{ $branch->phone or old('Telefono') }}">
</div>
<div class="form-group">
		<label for="NoEmpleados">Numero de empleados</label>
		<select class="form-control" name="NoEmpleados" id="NoEmpleados" value="{{ $company->no_employees or old('NoEmpleados') }}" >
			<option value="1">1-10</option>
			<option value="2">11-100</option>
			<option value="3">101-200</option>
			<option value="4">201-500</option>
			<option value="5">501-1000</option>
			<option value="6">1001-10000</option>
		</select>
</div>
<button type="submit" class="btn btn-primary">Guardar</button>
