@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')


<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Bienvenido</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body text-justify" id="homeVideo">
						<p>Estás dando inicio a una nueva etapa en tu vida y nos sentimos muy contentos de ser parte de ella. En Circle K, nos interesa tener al mejor talento y estamos seguros de que juntos haremos grandes cosas.</p>
						<p>En este inicio de tu proceso te recomendamos completar toda la información que se solicite y en caso de que tengas alguna duda o comentario, podrás comunicarte al 55 52619800 o bien escribirnos al siguiente correo para apoyarte <u style="color= blue;">circlek.mexico@gmail.com</u>..</p>
						<p><label>Vente pa'k</label></p>
						<hr><center>	
						<form action="{{  route('candidate_start') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
							<button type="submit" class="btn btn-primary">Continuar</button>
						</form>
						<center>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>





<!-- Modal -->
  <style>
	.modal-content {
      width: 100% !important;
    }
  </style>
<div class="modal fade" id="myModalVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel" >Nosotros</h4>
      </div>
      <div class="modal-body">

			<div class="embed-responsive embed-responsive-16by9" id="video_container">
			
		        <video  class="embed-responsive-item" id="vid">
		        	<source src="img/CircleK.mp4" type="video/mp4">
		      		<source src="img/CircleK.mov" type="video/mov">
		      		<source src="img/CircleK.ogv" type="video/ogv">
		      		<source src="img/CircleK.webm" type="video/webm">
		        </video>
        	
        	</div>
      </div>
	<input type="hidden" id="showVideo" value="{{ $trackings->video or '' }}">
		<div>
		<hr>
      				<center>	
						<button type="button" class="btn btn-primary" id="btnOpenVideo"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> Bienvenido !!</button>
						</cente>  
						<br> <br>
      </div>

      	<div class="modal-footer hide" id="btnCloseVideo">
      				<center>	
						<form action="{{  route('candidate_video') }}" method="POST" role="form">
							{{ csrf_field() }}
							<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
							<button type="submit" class="btn btn-primary">Continuar</button>
						</form>
						</center>
       
      </div>
    </div>
  </div>
</div>


@endsection
