<div class="col-xs-12">
  <ul class="nav nav-tabs nav-justified" id="steps">

    <li class="disabled" id="Paso1">
      <a data-toggle="tab" href="#section-jobapplication">
        <h3 class="stepTitle">Paso 1</h3>
        <h5 class="borra">Solicitud</h5>
      </a>
    </li>

    <li id="Paso2" class="active">
      <a id="Step2" href="#section-pruebas">
        <h3 class="stepTitle">Paso 2</h3>
        <h5 class="borra">Pruebas</h5>
      </a>
    </li>

    <li id="Paso3" class="disabled">
      <a id="Step3" href="#section-interview">
        <h3 class="stepTitle">Paso 3</h3>
        <h5 class="borra">Entrevista</h5>
      </a>
    </li>

    <li id="Paso4" class="disabled">
      <a id="Step4" href="#section-documents">
        <h3 class="stepTitle">Paso 4</h3>
        <h5 class="borra">Documentos</h5>
      </a>
    </li>
  </ul>

  

  <div class="tab-content">
    <div id="section-jobapplication" class="tab-pane fade">
    </div>
    <div id="section-pruebas" class="tab-pane fade in active" style="margin-top: 3em">
    </div>
    <div id="section-interview" class="tab-pane fade">
    </div>
    <div id="section-documents" class="tab-pane fade">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque a voluptatem itaque, laudantium fugit possimus, reprehenderit perspiciatis odio, sapiente eos quaerat dolorum, dignissimos provident. Provident quasi quis fugit, dolorem in.
    </div>
  </div>
</div>