@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

<div class="container-fluid spark-screen">
	
	<div class="row">
		<div class="col-md-12">
			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border text-center">
					@include('adminlte::psychometric/sections')
					<h3 class="box-title">Prueba Psicométrica</h3>
					@include('adminlte::psychometric.progressbar')
					<br>
					<h4 style="margin: 10px 0 10px 0; padding: .5em;" class="text-justify"><b>Instrucciones:</b>En cada uno de los cuadros siguientes hay un grupo de fichas de dominós, dentro de cada mitad los puntos varían de 0 a 6.
						Lo que usted tiene que hacer es observar bien cada grupo de fichas y calcular cuantos puntos le corresponden a la ficha que esta en blanco. Tiene usted 4 ejemplos 2 ya resueltos , observe como y porque corresponden esas soluciones.</h>
					</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead><tr>
									<td>Pregunta </td>
									<td>Respuesta</td>
									<td></td>
								</tr></thead>
								<tbody>
									<tr>
										<td><img src="{{ route('show_image_psychometric',['psychometric' => $psychometric->id]) }}" alt="pregunta" width="350" height="350"></td>

										<td>
											<input type="hidden" id="question" value="{{ $psychometric->id }}" >
											<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
											<div class="container-fluid">
												<br>
												<div class="row" 
												style="background:transparent url('../../img/Dominos-BLANK.png') no-repeat center center /cover; width: 300px; height: 300px" >


												<div class="col-xs-4 col-xs-offset-4">
													<form action="" method="POST" role="form">


														<input type="hidden" id="user_id" name="user_id" value="{{ auth()->user()->id}}" >
														<br><br><br>
														<div class="form-group">
															<div class="col-xs-12">
																<input type="text" class="form-control" id="answer1" onKeypress="if (event.keyCode < 48 || event.keyCode > 54) event.returnValue = false" placeholder="0" maxlength="1"  size="2" style="text-align:center; font-size: 20px;" value="{{ $answers->{'answer_'.$psychometric->id.'_1'}  or old('answer1') }}" >
															</div>
														</div>
														<br><br><br><br><br>
														<div class="form-group">
															<div class="col-xs-12">
																<input type="text" class="form-control" id="answer2" onKeypress="if (event.keyCode < 48 || event.keyCode > 54) event.returnValue = false" placeholder="0" maxlength="1" style="text-align:center; font-size: 20px;" value="{{ $answers->{'answer_'.$psychometric->id.'_2'}  or old('answer2') }}" >


															</div>
														</div>

													</form>
												</div>

											</div>
										</div>
									</td>
								</tr>

							</tbody>
<tfoot>
										<tr>
											<td>
												<form action="{{ route('psychometric_test_answer', ['psychometric' => $psychometric->id-1] )}}" method="GET" role="form">
													
													<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">	
													<button type="submit" class="btn btn-primary btn-arrow-left" aria-label="Left Align" id="btnPrev" style="border-color: black; font-family: 'gothambold';">Anterior
													</button>


												</form>
											</td>
											<td align="right">
												<form action="{{ route('psychometric_test_answer', ['psychometric' => $psychometric->id+1] )}}" method="GET" role="form">
													
													<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
													@if ($psychometric->id != 52)
													<button type="button" id="btnSavePsychometric" class="btn btn-primary btn-arrow-right" aria-label="Right Align" id="btnNext" style="border-color: black; font-family: 'gothambold';">Siguiente
													</button>
													@else
													
												@endif


											</form>
											@if ( str_limit($psychometric->id*100/52,$limit=3, $end='') ==100)
											<!-- <tr><td colspan="2" align="center"> -->
											<form action="{{ route('psychometric_score', ['psychometric' => $psychometric->id] )}}" method="POST" role="form">
												{{ csrf_field() }}
												<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
												<button type="submit" class="btn btn-primary btn-arrow-right" aria-label="Left Align" style="background-color: black; border-color: black;" id="btnScore" >Finalizar
													<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
												</button>

											</form>
											@endif

										</td>
									</tr>
									<!-- </td></tr> -->
								</tr>
							</tfoot>

					</table>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
</div>

@endsection
