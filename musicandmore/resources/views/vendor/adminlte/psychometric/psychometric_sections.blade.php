<div class="col-xs-12">
    <ul class="nav nav-tabs nav-justified" id="steps">
        <li class="active"><a data-toggle="tab" href="#section-intelligence"><h3 class="stepTitle">Paso 1</h3><h5 class="borra">Inteligencia</h5></a></li>

        <li id="Paso2"><a id="Step2" href="#section-behavior"><h3 class="stepTitle">Paso 2</h3><h5 class="borra">Comportamiento</h5></a></li>

        <li id="Paso3"><a id="Step3" href="#section-personality"><h3 class="stepTitle">Paso 3</h3><h5 class="borra">Personalidad</h5></a></li>
        
        <li id="Paso4"><a id="Step4" href="#section-values"><h3 class="stepTitle">Paso 4</h3><h5 class="borra">Valores</h5></a></li>
    </ul>

    <div class="tab-content">
        <div id="section-intelligence" class="tab-pane fade in active">

      </div>
      <div id="section-behavior" class="tab-pane fade">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, ad dolore. Quam necessitatibus enim pariatur consequatur! Assumenda praesentium non maiores, amet exercitationem, nesciunt quas ea sint a inventore dolorem, iusto!
      </div>
      <div id="section-personality" class="tab-pane fade">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium magnam fugit rerum sint dolores vero, ipsum ratione, cupiditate inventore magni aspernatur repudiandae, dolorem nesciunt. Saepe ullam, magnam quibusdam sit? Laboriosam.
      </div>
      <div id="section-values" class="tab-pane fade">
          <h3>Menu 3</h3>
          <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
      </div>
  </div>
  </div>