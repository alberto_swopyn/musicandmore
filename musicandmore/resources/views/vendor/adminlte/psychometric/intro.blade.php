<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div align="center">
						<h3 class="box-title">Prueba Psicométrica</h3>
					</div>
					
					<div class="box-body">
						

						<b><p>Instrucciones</p></b>

						<p align="justify">En cada uno de los cuadros siguientes hay un grupo de fichas de dominós, dentro de cada mitad los puntos varían de 0 a 6.
						Lo que tienes que hacer es observar bien cada grupo de fichas y calcular cuantos puntos le corresponden a cada ficha que esta en blanco. Tienes 4 ejemplos, ¿observe cómo? y ¿por qué? corresponden esas soluciones. </p>

						<b><p>Tiempo de aplicación </p></b>

						<p>30 min al transcurrir este tiempo se recoge la prueba ó se califica hasta donde llego en los 30 min. </p>




						<form action="{{ route('psychometric_test_answer', 1) }}" method="GET">
							{{ csrf_field() }}
							<input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
							<div align="center" style="margin-top: 3em">
								<button type="submit" class="btn btn-primary next-step" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="btnStartTest">Iniciar</button>
							</div>
						</form>					
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>


		</div>
	</div>