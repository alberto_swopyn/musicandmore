@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::training.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Pase de Lista Maestros</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
			<div class="box-body">
							<div class="row">
			<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
				{!!Form::open(['method' => 'GET','route'=>'index8','class'=>'navbar-form pull-right','role'=>'search'])!!}
  					<div class="form-group">
    				<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  					</div>
 	 				<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
				{!!Form::close()!!}
			</div>
		</div>
		<h3 class="titleCenter" style="text-align: center;">PASE DE ASISTENCIA</h3>
		<h3 class="titleCenter" style="text-align: center;">MAESTROS</h3>
		<br>
	<div class="table-responsive">
		<table class="table table-hover table table-striped tablesorter sortable" id="myTable">
			<thead style="background: black; color: white;">
				<th class="text-center">#</th>
				<th class="text-center">Nombre del Profesor</th>
				<th class="text-center">Marcar Asistencia</th>
				<th class="text-center">Asistencia de otro Día</th>
			</thead>
			<tbody>
				<?php $a = 0; $b = ""; ?>
				@foreach($groups as $g)
				<?php $a = $g['id_teacher']; $b = $g['teacher'] ?>
				<tr>
					<td class="text-center" id="Maestro" name="Maestro">{{$g['id_teacher']}}</td>
					<td class="text-center">{{$g['teacher']}}</td>
					<td class="text-center"><form action="{{ route('add_ass_teacher') }}" method="POST">
						{{ csrf_field() }}
						@if($g['assistance'] == 0)
						<button type="submit" class="btn btn-info" value="{{$g['id_teacher']}}" name="Grupo" >Marcar Asistencia</button>
						@else
						<button disabled="disabled" type="submit" class="btn btn-success" value="{{$g['id_teacher']}}" name="Grupo" >Ya se ha pasado asistencia</button>
						@endif
						</form>
					</td>

					<td class="text-center">
						<button type="submit" class="btn btn-warning" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>">
							Otra Fecha
						</button>
						<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="confirm-edit<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="{{ route('add_ass_teacher_other', ['idTeacher' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
													<div class="modal-body">
														<h3>ASISTENCIA DE OTRA FECHA</h3>
														<h4 class="modal-title"><?php echo $b; ?></h4>
														<label for="other">Fecha de la asistencia pendiente: </label>
														<input type="date" name="other" id="other" required="required">
														<label for="comments">Justificación de Edición</label>
														<textarea name="comments" id="comments" cols="27" rows="3" required="required"></textarea>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Editar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>"><strong>Marcar Asistencia</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
					</form></td>
					
				</tr>
				@endforeach
			</tbody>
		</table>
		<center>{{$groupsPage->links()}}</center>
	</div>
</div>
</div>
</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>