@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::training.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Clases Impartidas</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
		<div class="input-group col-md-offset-1 col-md-6">
			
		</div>
		<input type="hidden" value="" name="Grupo" id="Grupo" >
		<div class="table-responsive">
				<table class="table table-hover table-striped tablesorter sortable" id="tableEmployees">
			<thead style="background: black; color: white;">
				<th class="text-center">Curso</th>
				<th class="text-center">Fecha</th>
				<th class="text-center">Hora</th>
			</thead>
			<tbody>
				<tr>
					@foreach($groups as $g)
					<td class="text-center">{{$g->curso}}</td>
					<td class="text-center"><?php echo(date("d-m-Y",strtotime($g->date)))?></td>
					<td class="text-center"><?php echo(date("H:i",strtotime($g->date)))?></td>
				</tr>
					@endforeach
			</tbody>
					
      	</table>
		</div>
		<br>
		<br>
		<br>
</div>
</div>
</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>