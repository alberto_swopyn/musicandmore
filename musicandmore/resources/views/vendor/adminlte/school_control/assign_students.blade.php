@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::school_control.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Asignación de Grupos</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
<form action="{{ Route('groups_assign_add') }}" method="POST" role="form" enctype="multipart/form-data">
                           {{ csrf_field() }}
                 
                       

	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a>
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a>
	</div>

	<div class="panel-body">
	
	<div class="form-group col-md-12">
		<br>
			<div class="col-md-12">
				<span style="font-size: 1.5em">Nombre del grupo</span>
			</div>

			<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em">
								<i class="fa fa-graduation-cap" aria-hidden="true" style="font-size: 2em; color: black;">
								</i>
							</span>
							<select name="groups[]" class="form-control" id="groups">
								@foreach($group as $groups)
								<option value="{{ $groups->id}}"
									{{ (in_array($groups, old('groups', []))) }} >{{ $groups->name }}  --- {{$groups->teacher}}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

		

		<div class="col-md-12">
		<span style="font-size: 1.5em">Alumno a Inscribir</span>
					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
						<span class="input-group-addon" style="border: none; width: 1em">
								<i class="fa fa-user-graduate" aria-hidden="true" style="font-size: 2em; color: black;">
								</i>
							</span>
							<select name="students[]" class="form-control" id="students">
								@foreach($student as $students)
								<option value="{{ $students->id}}"
									{{ (in_array($students, old('students', []))) }} >{{ $students->name }}@if ('1' == $students->sample) -- Alumno Muestra @endif
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

		<div class="col-md-12 text-center">
			<br>
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
		</div>
	</form>
	</div>
</div>
<script>
      $(document).ready(function(){
          $('#groups').select2();
          $('#students').select2();
       });
    </script>
@endsection