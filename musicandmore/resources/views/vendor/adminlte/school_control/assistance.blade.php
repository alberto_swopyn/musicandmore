@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Asistencia</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<br>
						<div class="table-responsive">
						  <table class="table table-hover tablesorter sortable" id="tableEmployees">
						    <thead style="background: black; color: white;">
						    	<tr>
						    	<th class="text-center" style="color: white">Grupo</th>
						    	<th class="text-center" style="color: white">Aula</th>
						    	<th class="text-center" style="color: white">Materia</th>
						    	<th class="text-center" style="color: white">L</th>
						    	<th class="text-center" style="color: white">Ma</th>
						    	<th class="text-center" style="color: white">Mi</th>
						    	<th class="text-center" style="color: white">J</th>
						    	<th class="text-center" style="color: white">V</th>
						    	<th class="text-center" style="color: white">S</th>
						    	<th class="text-center" style="color: white">D</th>
						    	<th class="text-center" style="color: white">Horario</th>
						    	<th class="text-center" style="color: white">Alumnos</th>
						    	</tr>
						    </thead>
					<tbody>
								@foreach($group as $groups)
								<tr>
									<td align="center">{{ $groups->nombre}}</td>
									<td align="center">{{ $groups->classroom}}</td>
									<td align="center">{{ $groups->subject_name}}</td>
									<td align="center">
										@if($groups->monday == 1)
										@if($date2 == $monday)
											@if($time >= $groups->schedule and $time <= $groups->final_hour )
									<a href="{{ Route('assistance_edit', ['group' => $groups->id]) }}" class="btn">
                      				<strong><i class="fa fa-calendar-check-o" aria-hidden="true"></i></strong>
                   					 </a>
                   					</td>
                   					 	@endif
										@endif
									@endif	
									<td align="center">
										@if($groups->tuesday == 1)
										@if($date2 == $tuesday)
											@if($time >= $groups->schedule and $time <= $groups->final_hour )
									<a href="{{ Route('assistance_edit', ['group' => $groups->id]) }}" class="btn">
                      				<strong><i class="fa fa-calendar-check-o" aria-hidden="true"></i></strong>
                   					 </a>
                   					</td>@endif
										@endif
									 @endif
									<td align="center">
										@if($groups->wednesday == 1)
										@if($date2 == $wednesday)
											@if($time >= $groups->schedule and $time <= $groups->final_hour )
									<a href="{{ Route('assistance_edit', ['group' => $groups->id]) }}" class="btn">
                      				<strong><i class="fa fa-calendar-check-o" aria-hidden="true"></i></strong>
                   					 </a>
                   					</td>	@endif
										@endif
									@endif
									<td align="center">
										@if($groups->thursday == 1)
										@if($date2 == $thursday)
											@if($time >= $groups->schedule and $time <= $groups->final_hour )
									<a href="{{ Route('assistance_edit', ['group' => $groups->id]) }}" class="btn">
                      				<strong><i class="fa fa-calendar-check-o" aria-hidden="true"></i></strong>
                   					 </a>	
                   					</td>
                   					@endif
										@endif
									@endif
									<td align="center">
										@if($groups->friday == 1)
										@if($date2 == $friday)
											@if($time >= $groups->schedule and $time <= $groups->final_hour )
									<a href="{{ Route('assistance_edit', ['group' => $groups->id]) }}" class="btn">
                      				<strong><i class="fa fa-calendar-check-o" aria-hidden="true"></i></strong>
                   					 </a>
                   					</td>
                   					 		@endif
										@endif
									@endif	
									<td align="center">
										@if($groups->saturday == 1)
										@if($date2 == $saturday)
											@if($time >= $groups->schedule and $time <= $groups->final_hour )
									<a href="{{ Route('assistance_edit', ['group' => $groups->id]) }}" class="btn">
                      				<strong><i class="fa fa-calendar-check-o" aria-hidden="true"></i></strong>
                   					 </a></td>
                   					 		@endif
										@endif
									@endif
									<td align="center">
										@if($groups->sunday == 1)
										@if($date2 == $sunday)
											@if($time >= $groups->schedule and $time <= $groups->final_hour )
									<a href="{{ Route('assistance_edit', ['group' => $groups->id]) }}" class="btn">
                      				<strong><i class="fa fa-calendar-check-o" aria-hidden="true"></i></strong>
                   					 </a></td>
                   					 		@endif
										@endif
									@endif
									<td align="center"><?php $date = strtotime($groups->schedule);  
										print(date("g:i",$date));
									?>/<?php $date = strtotime($groups->final_hour);  
										print(date("g:i",$date));
									?></td>

									<td class="text-center">
										<a href="{{ Route('alum_list', ['group' => $groups->id]) }}" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a;" title="Ver lista">Ver lista</a>
									</td>
									</tr>
								@endforeach
							</tbody>

						  </table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
		</div>
			</div>

		</div>
		</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>