@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('main-content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h2 class="box-title">Grupo</h2>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					@include('adminlte::layouts.partials._errors')
					<form action="{{ Route('groups_update', ['group' => $group->id]) }}" method="POST" role="form" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
						<div class="col-md-12">
							<div class="panel panel-info">
								<table class="table table-hover table table-striped">
									<thead style="background: black; color: white;">
										<tr>
											<th class="text-center">Actualizar Grupo</th>
										</tr>
									</thead>
								</table>
								<br>
									<div class="panel-body">
										<div class="panel-body">
	
	<div class="form-group col-md-12">

			<div class="col-md-12 text-center">
				<label for="Nombre" style="font-size: 1.5em">Nombre del grupo</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Nombre del Grupo"   style="border: 0.5px solid ; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{$group->name}}">
			</div>

		</div>

		<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<br>
			<div class="col-md-12">
					<div>
						<span style="font-size: 1.5em">Aula</span>
					</div>
					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em"><i class="fa fa-address-card" aria-hidden="true" style="font-size: 2em; color: black;"></i>
							</span>
							<select name="classroom[]" class="form-control" id="classroom">
								@foreach($classroom as $classr)
							<option value="{{ $classr->id}}"
								{{ (in_array($classr, old('classr', []))) }} 
								@<?php if ($classr->id == $group->classroom): ?>
									selected
								<?php endif ?>

								>{{ $classr->name }}
							</option>
						@endforeach
							</select>
						</div>
					</div>
				</div>

			<div class="col-md-12">
			<div>
				<span style="font-size: 1.5em">Maestro/Materia</span>
			</div>
			<div class="form-group">
				<div class="input-group col-md-offset-1 col-md-11">
					<span class="input-group-addon" style="border: none; width: 1em"><i class="fa fa-chalkboard-teacher" aria-hidden="true" style="font-size: 2em; color: black;"></i>
					</span>
					<select name="cathedras[]" class="form-control" id="cathedras">
						@foreach($cathedra as $cathedras)
							<option value="{{ $cathedras->id}}"
								{{ (in_array($cathedras, old('cathedras', []))) }} 
								@<?php if ($cathedras->id == $group->id_cathedra): ?>
									selected
								<?php endif ?>

								>{{ $cathedras->subject_name }} -- {{$cathedras->teacher}}
							</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		


		<div class="form-group row">
				<div class="col-xs-6">
					<br>
						 <br>
					<div class="text-center">
						<label for="Fecha_inicial" style="font-size: 1.5em">Fecha inicial</label>
					</div>
	
								<div class="col-md-12 text-center" >
										<input id="Fecha_inicial" type="date" value="{{ $group->initial_date }}" name="initial_date" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
							</div>
				</div>
	
				<div class="col-xs-6">
					<br>
						 <br>
					<div class="text-center">
						<label for="Fecha_final" style="font-size: 1.5em">Fecha final</label>
					</div>
	
								<div class="col-md-12 text-center" >
										<input id="Fecha_final" type="date" value="{{ $group->final_date }}" name="final_date" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
							</div>
	
				</div>
	
					</div>
	
	
				<div class="form-group row">
					<div class="col-xs-6">
						<br>
						<br>
						<div class="text-center">
							<label for="Fecha_inicial" style="font-size: 1.5em">Horario</label>
						</div>
	
						<div class="col-md-12 text-center" >
							<form action="" name="form">
								<label for="initial_hour">De: </label>
								<input type="time" value="{{ $group->schedule }}" name="initial_hour" id="hora" value="00:00:00" max="22:30:00" min="10:00:00" step="1">
								<label for="final_hour"> A: </label>
								<input type="time" value="{{ $group->final_hour }}" name="final_hour" id="hora" value="00:00:00" max="22:30:00" min="10:00:00" step="1">
							</form>
						</div>
				</div>





		            <div class="col-xs-6">
		            	<br>
						<br>
			            <div class="text-center">
							<label for="Cupo" style="font-size: 1.5em">Cupo de alumnos</label>
						</div>

			            <div class="col-md-12 text-center" >
			                <label>
							<input id="cupo" name="cupo" size="20" type="number" min="0" style="padding-left: 3em; border-radius: 8px" class="col-md-8 col-md-push-3" step="1" id="" value="{{$group->quota}}" >
							</label>
				        </div>
			        </div>
						</div>
						
						<div class="col-xs-12">
							<br>
								<div class="text-center">
									<label for="Fecha_final" style="font-size: 1.5em">Color identificador</label>
								</div>

								<div class="col-md-12 text-center" >
									<label>
										<input type="color" id="color" name="color" value="{{ $group->color }}">
									</label>
								</div>
								<br>
						</div>
			<br>
			<br>
			<div class="text-center">
				<br>
						<span style="font-size: 1.5em">Días de impartir la materia</span>
			</div>
			<div class="table-responsive">
			<table class="table table-hover table table-striped" id="tableTasks">
				<thead style="background: black; color: white;">
					<th class="text-center">Lunes</th>
					<th class="text-center">Martes</th>
					<th class="text-center">Miercoles</th>
					<th class="text-center">Jueves</th>
					<th class="text-center">Vernes</th>
					<th class="text-center">Sábado</th>
					<th class="text-center">Domingo</th>
				</thead>
				<tbody>
					<tr>
						@if($group->monday == 1)
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Monday" name="Monday" value="1" checked></td>
						@else
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Monday" name="Monday" value="1"></td>
						@endif
						@if($group->tuesday == 1)
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Tuesday" name="Tuesday" value="1" checked></td>
						@else
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Tuesday" name="Tuesday" value="1"></td>
						@endif
						@if($group->wednesday == 1)
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Wednesday" name="Wednesday" value="1" checked></td>
						@else
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Wednesday" name="Wednesday" value="1"></td>
						@endif
						@if($group->thursday == 1)
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Thursday" name="Thursday" value="1" checked></td>
						@else
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Thursday" name="Thursday" value="1"></td>
						@endif
						@if($group->friday == 1)
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Friday" name="Friday" value="1" checked></td>
						@else
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Friday" name="Friday" value="1"></td>
						@endif
						@if($group->saturday == 1)
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Saturday" name="Saturday" value="1" checked></td>
						@else
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Saturday" name="Saturday" value="1"></td>
						@endif
						@if($group->sunday == 1)
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Sunday" name="Sunday" value="1" checked></td>
						@else
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Sunday" name="Sunday" value="1"></td>
						@endif
					</tr>
				</tbody>
			</table>
		</div>
		</div>
		<br>
		<br>
		<br>
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
		</div>
	</form>
</div>
<script>
      $(document).ready(function(){
          $('#subjects').select2();
          $('#cathedras').select2();
       });
    </script>
@endsection
