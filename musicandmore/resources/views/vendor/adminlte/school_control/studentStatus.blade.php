@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Alumnos</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
								
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'index_status','class'=>'navbar-form pull-right','role'=>'search'])!!}
  					<div class="form-group">
    				<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  					</div>
 	 				<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
					{!!Form::close()!!}
					
					<div class="row" style="margin-bottom: 1em">
						<div class="text-left col-md-12">
							<a data-toggle="modal" data-target="#createStudent" style="background-color: #1bb49a; border-color: #1bb49a;" class="btn btn-success"><i class="fa fa-user-plus" aria-hidden="true"></i> Crear Alumno</a>
						</div>
					</div>
					
										<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="createStudent" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="{{ Route('sampleuser_add') }}" method="POST" id="form">
															{{ csrf_field() }}
													<div class="modal-body">
														<h4 class="modal-title">Agregar Alumno</h4>
														
														<div class="form-group col-md-12">
								<div class="col-md-12 text-center">
									<label for="Nombre" style="font-size: 1.5em">Nombre</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" id="Nombreuser" name="Nombreuser" placeholder="Nombre del usuario"   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
								</div>
								<div class="col-md-12 text-center">
									<label for="Correo" style="font-size: 1.5em">Correo</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" id="CorreoUser" name="CorreoUser" placeholder="Correo del usuario"   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
								</div>
								<div class="col-md-12 text-center">
									<label for="Correo" style="font-size: 1.5em">Teléfono de contacto</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" id="Telefono" name="Telefono" placeholder="10 dígitos" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" pattern="[0-9]{10}" maxlength="10" required="required">    
								</div>
								<div class="col-md-12 text-center">
									<label for="Experience" style="font-size: 1.5em">Experiencia</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<select name="experience" class="form-control" id="experience" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="">
		                				<option value="" selected="true">Seleccione una opción...</option>
		                				<option value="1">Sí</option>
										<option value="0">No</option>
									</select>
								</div>
								<div class="col-md-12 text-center">
									<label for="Experience" style="font-size: 1.5em">Sucursal</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<select name="company" class="form-control" id="company" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="">
		                				<option value="" selected="true">Seleccione una opción...</option>
		                				@foreach($company as $company)
											<option value="{{ $company->id_inc}}" {{ (in_array($company, old('company', []))) }} >{{ $company->text }}</option>
										@endforeach
									</select>
								</div>
							</div>

														<div class="col-md-8 col-md-offset-2">

															
														</div>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Eliminar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal" data-target="#createStudent"><strong>Crear</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
					</div>
					


					<br>
						<h3 class="titleCenter" style="text-align: center;">Alumnos</h3>
						<div class="table-responsive">
						<table class="table table-striped tablesorter" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Nombre</th>
								<th class="text-center">Tipo de alumno</th>
								<th class="text-center">Estatus</th>
								<th class="text-center">Solicitud</th>
								<th class="text-center">Cuestionario</th>
								<th class="text-center">Editar muestra</th>
								<th class="text-center" colspan="2">E-mail/Password</th>

							</tr>
						</thead>
						<?php $a = 0; ?>
						@foreach($students as $student)
						<?php $a = $student->id; ?>
						<tbody>
								<tr>
									<td class="text-center">{{$student-> name}}</td>
									@if($student->sample == 1)
										<td class="text-center">
											<button type="submit" title="Inscribir" class="btn btn-warning" data-toggle="modal" data-target="#confirm-inscribe<?php echo $a; ?>">Muestra
											<span class="fa fa-user-plus" aria-hidden="true"></span>
										</button>
										<!-- INICIA MODAL PARA INSCRIBIR -->
										<div class="modal fade" id="confirm-inscribe<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<div class="modal-body">
														<h4 class="modal-title">¿Desea inscribir este Alumno?</h4>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														<form action="{{ route('sample_inscribe', ['sample' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('POST') }}
															<button type="submit" title="Inscribir" class="btn btn-success" style="background-color: black; border-color: black;" data-toggle="modal" data-target="#confirm-inscribe"><strong>Inscribir</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->
									</td>
									@else
										<td class="text-center">
										<button type="button" class="btn btn-primary" aria-label="Left Align" data-toggle="tooltip" disabled="true">
											<strong>Inscrito </strong><i class="fa fa-check-circle" style="color: white;"></i></td>
										</button>
									</td>
									@endif
									@if($student->status == 1)
										<td class="text-center">
										<button type="submit" title="Editar" class="btn btn-success" aria-label="Left Align" title="Vigente" data-toggle="modal" data-target="#status<?php echo $a; ?>">
											<strong>Vigente </strong><i class="fa fa-check-circle" style="color: white;"></i>
										</button>
									</td>
									@else
									<td class="text-center">

										<button type="submit" class="btn btn-danger" aria-label="Left Align" title="No vigente" data-toggle="modal" data-target="#status<?php echo $a; ?>">
											<strong>No vigente </strong><i class="fa fa-times-circle" aria-hidden="true"></i>
										</button>
									</td>
									@endif
									
									@if($student->sample == 0)
									<td class="text-center">
										<a title="Solicitud" class="btn btn-info" style="background-color: #8942AC; border-color: #8942AC;" href="{{ Route('student_edit', ['student' => $student->id]) }}">
											<span class="fa fa-address-card" aria-hidden="true"></span>	
										</a>
									</td>
									@else
									<td class="text-center">
										<a disabled="true" title="Solicitud" class="btn btn-info" style="background-color: #8942AC; border-color: #8942AC;">
											<span class="fa fa-address-card" aria-hidden="true"></span>	
										</a>
									</td>
									@endif

									<td class="text-center">
										<a href="{{ Route('student_quiz', ['student' => $student->id]) }}" title="Cuestionario" class="btn btn-info" style="background-color: #387A93; border-color: #387A93;">
											<span class="fa fa-book" aria-hidden="true"> </span>
										</a>
									</td>

									@if($student->sample == 1)
									<td class="text-center">

										<button type="submit" title="Editar" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a;" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>">
											<span class="glyphicon glyphicon-edit" aria-hidden="true"> </span>
										</button>
										<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="confirm-edit<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="{{ route('sampleuser_update', ['sample' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('PUT') }}
													<div class="modal-body">
														<h4 class="modal-title">Nombre del usuario</h4>
														<div class="col-md-10 col-md-offset-1">
															<input type="text" class="form-control" id="NombreSamEdit<?php echo $a; ?>" name="NombreSamEdit<?php echo $a; ?>" placeholder="Nombre" value="{{$student-> name}}" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
														</div>
															<h4 class="modal-title">Correo del usuario</h4>
														<div class="col-md-10 col-md-offset-1">
															<input type="text" class="form-control" id="CorreoSamEdit<?php echo $a; ?>" name="CorreoSamEdit<?php echo $a; ?>" placeholder="Correo" value="{{$student-> email}}" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
														</div>
														<h4 class="modal-title">Teléfono de contacto</h4>
														<div class="col-md-10 col-md-offset-1">
															<input type="text" class="form-control" id="TelSamEdit<?php echo $a; ?>" name="TelSamEdit<?php echo $a; ?>" placeholder="10 dígitos" value="{{$student-> phone_number}}" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" pattern="[0-9]{10}" maxlength="10" required="required">
														</div>
														<h4 class="modal-title">Experiencia</h4>
														<div class="col-md-10 col-md-offset-1">
															<select name="SamEditExp<?php echo $a; ?>" class="form-control" id="SamEditExp<?php echo $a; ?>" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.1em; border-radius: 7px">
		                										@if($student->experience==1)
		                										<option value="1" selected="true">Sí</option>
		                										<option value="0">No</option>
		                										@else
																<option value="1">Sí</option>
																<option value="0" selected="true">No</option>
																@endif
															</select>
														</div>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Editar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>"><strong>Guardar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
										<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
									</td>
									@else
									<td class="text-center">
											
									</td>
									@endif

									<td>
										<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="status<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="{{ route('studenStatus_update', ['studentStatus' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('PUT') }}
													<div class="modal-body">
														<h4 class="modal-title">¿Está seguro de que desea cambiar el Estatus?</h4>

														<select name="StatusEdit<?php echo $a; ?>" class="form-control" id="StatusEdit<?php echo $a; ?>" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.1em; border-radius: 7px; visibility:hidden">
		                										@if($student->status==1)
		                										<option value="1">Vigente</option>
		                										<option value="0" selected="true">No vigente</option>
		                										@else
																<option value="1" selected="true">Vigente</option>
																<option value="0">No vigente</option>
																@endif
															</select>
														<div class="col-md-8 col-md-offset-2">

															
														</div>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Aceptar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal" data-target="#status<?php echo $a; ?>"><strong>Aceptar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
									</td>
									<td class="text-center">
										<button type="submit" title="email" class="btn btn-info"  style="background-color: black; border-color: black;"data-toggle="modal" data-target="#check-mail<?php echo $a; ?>">
											<span class="fa fa-envelope-o" aria-hidden="true"> </span>
										</button>
										<!--Modal para mostrar correo y contraseña-->
										<div class="modal fade" id="check-mail<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">E-mail/Password</h4>
											      	</div>
													<!-- Modal body -->
													<div class="modal-body">
													<label style="font-weight: bold; color: black; font-size: 18px">Email:</label>
													<p style="font-size: 18px">{{$student->email}}</p>

														
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Aceptar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal"><strong>Aceptar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						@endforeach
						</table>
						<center>{{$students->links()}}</center>
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>

		
		@endsection
	