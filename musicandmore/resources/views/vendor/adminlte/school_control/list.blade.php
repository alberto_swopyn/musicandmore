@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::training.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
<div class="panel panel-primary" style="border-color: white;">
	<form action="{{ Route('assistance_add') }}" method="POST" role="form" enctype="multipart/form-data">{{ csrf_field() }}
        <div class="pull-right">
			<a href="javascript:void(0)" data-perform="panel-collapse">
				<i class="ti-minus"></i></a>
			<a href="javascript:void(0)" data-perform="panel-dismiss">
				<i class="ti-close"></i></a>
		</div>

	<div class="box">
		<div class="box-header with-border">
			<div class="panel-body">

		<div class="box-body">
		<div class="row">
			<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
				<i class="glyphicon glyphicon-search"></i>
			</div>
		</div>
		<h3 class="titleCenter" style="text-align: center;">LISTA DE ASISTENCIA</h3>
		<h4 class="titleCenter" style="text-align: center;">{{$group->name}}</h4>
		<input type="hidden" value="{{$group->id}}" name="Grupo" id="Grupo" >
		<div class="table-responsive">
			@foreach($student as $students)
		<input type="hidden" value="{{$students->id}}" name="Alumno[]" id="Alumno[]" >
		<div class="table-responsive">
			@endforeach
			<table class="table table-hover table table-striped tablesorter sortable" id="myTable">
				<thead style="background: black; color: white;">
					<th class="text-center">#</th>
					<th class="text-center">Nombre del alumo</th>
					<th class="text-center">A</th>
					<th class="text-center">R</th>
					<th class="text-center">F</th>
				</thead>
				<tbody>
					@foreach($student as $students)
					<tr>
						
						<td class="text-center">{{$students->id}}</td>
						<td class="text-center">{{$students->student}}</td>
						<td class="text-center">
							<input type="checkbox" class="form-check-input" id="Assistance[]" name="Assistance[]" value="1"></td>
						<td class="text-center">
							<input type="checkbox" class="form-check-input" id="Assistance[]" name="Assistance[]" value="2"></td>
						<td class="text-center">
							<input type="checkbox" class="form-check-input" id="Assistance[]" name="Assistance[]" value="0"></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<br>
	<br>
		<br>
		<br>
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
		</div>
	</form>
</div>
</div>
</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>