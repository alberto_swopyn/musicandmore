@extends('adminlte::layouts.app')
@section('htmlheader_title')
 @section('personal_style')
@stop
@endsection
@section('main-content')
<div class="box">
	<div class="box-header with-border">
	<h3 class="box-title">Lista de alumnos</h3>

	<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
				<i class="fa fa-times"></i></button>
			</div>
		</div>

	{{ csrf_field() }}
		<div class="panel-body">

	<div class="box-body">
	<h4 class="titleCenter" style="text-align: center;">{{$group->name}}</h4>
	<input type="hidden" value="{{$group->id}}" name="Grupo" id="Grupo" >
	<div class="table-responsive">
		<table class="table table-hover table-striped tablesorter sortable" id="tableEmployees">
			<thead style="background: black; color: white;">
				<th class="text-center">#</th>
				<th class="text-center">Nombre</th>
			</thead>
			<tbody>
				@foreach($student as $students)
				<tr>
					<td class="text-center">{{$students->id}}</td>
					<td class="text-center"><a href="{{ Route('student_detail', ['users' => $students->id]) }}"> {{$students->student}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>
