@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('personal_style')
@include('adminlte::tasks.PersonalSelect2')
@stop
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Empleados</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
								<div class="row" style="margin-bottom: 1em">
										<div class="text-right col-md-12">
											<a href="#newEmployees" id="newEmployee" class="btn btn-success" data-toggle="modal" style="background-color: #1bb49a; border-color: #1bb49a;"><i class="fa fa-user-plus" aria-hidden="true"></i> Crear Empleado</a>
										</div>
									</div>
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
									{!!Form::open(['method' => 'GET','route'=>'indexM','class'=>'navbar-form pull-right','role'=>'search'])!!}
  					<div class="form-group">
    				<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  					</div>
 	 				<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
					{!!Form::close()!!}

						<div class="col-md-12">
						<form method="POST" action="{{ Route('teacher_add') }}" role="form">
                           {{ csrf_field() }}
                           <div class="form-group col-md-12">
								<div class="col-md-12 text-center">
									<label for="Nombre" style="font-size: 1.5em">Nombre</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" id="Nombreuser" name="Nombreuser" placeholder="Nombre del usuario"   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
								</div>
								<div class="col-md-12 text-center">
									<label for="Correo" style="font-size: 1.5em">Correo</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" id="CorreoUser" name="CorreoUser" placeholder="Correo del usuario"   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
								</div>
								<div class="col-md-12 text-center">
									<label for="Correo" style="font-size: 1.5em">Rol</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<select name="RoleUser" class="form-control" id="role" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="">
		                				<option value="" selected="true">Seleccione una opción...</option>
		                				<option value="1">Administrador</option>
										<option value="3">Maestro</option>
									</select>
								</div>
							</div>
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
							</div>

                       </form>
					</div>
					</div>
					</div>
						<h3 class="titleCenter" style="text-align: center;">Empleados</h3>
						<div class="table-responsive">
						<table class="table table-hover table table-striped" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Nombre</th>
								<th class="text-center">Rol</th>
								<th class="text-center">Estatus</th>
								<th colspan="2" class="text-center">E-mail</th>
							</tr>
						</thead>
						<?php $a = 0; ?>
						@foreach($teachers as $teacher)
						<?php $a = $teacher->id; ?>
						<tbody>
								<tr>
									<td class="text-center">{{$teacher-> name}}</td>
									@if($teacher->role_id == 1)
										<td class="text-center">
											<button type="button" class="btn btn-info" aria-label="Left Align" data-toggle="tooltip" title="Administrador" disabled="true" style="background-color: #8942AC; border-color: #8942AC;">
												<strong>Administrador </strong>
											</button>
										</td>
									@else
										<td class="text-center">
											<button type="button" class="btn btn-info" aria-label="Left Align" data-toggle="tooltip" title="Maestro" disabled="disabled" style="background-color: #387A93; border-color: #387A93;">
												<strong>Maestro</strong>
											</button>
										</td>
									@endif

									@if($teacher->status == 1)
										<td class="text-center">
										<button type="submit" title="Editar" class="btn btn-success" aria-label="Left Align" title="Vigente" data-toggle="modal" data-target="#status<?php echo $a; ?>">
											<strong>Vigente </strong><i class="fa fa-check-circle" style="color: white;"></i>
										</button>
									</td>
									@else
									<td class="text-center">

										<button type="submit" class="btn btn-danger" aria-label="Left Align" title="No vigente" data-toggle="modal" data-target="#status<?php echo $a; ?>">
											<strong>No vigente </strong><i class="fa fa-times-circle" aria-hidden="true"></i>
										</button>
									</td>
									@endif
									<td class="text-center">
										<button type="submit" title="Editar" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a;" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>">
											<span class="glyphicon glyphicon-info-sign" aria-hidden="true"> </span>
										</button>
										<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="confirm-edit<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('PUT') }}
													<div class="modal-body">
														<h4 class="modal-title">E-mail:</h4>
														<p style="font-size: 1.5em"><strong><u></u></strong> {{$teacher-> email}}</p>

														
														<div class="col-md-8 col-md-offset-2">

															
														</div>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="button" title="Editar" style="background-color: black; border-color: black;" class="btn btn-info" data-dismiss="modal"><strong>Aceptar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
									</td>
									<td>
										<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="status<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="{{ route('teacherStatus_update', ['teacherStatus' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('PUT') }}
													<div class="modal-body">
														<h4 class="modal-title">¿Está seguro de que desea cambiar el Estatus?</h4>

														<select name="StatusEdit<?php echo $a; ?>" class="form-control" id="StatusEdit<?php echo $a; ?>" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.1em; border-radius: 7px; visibility:hidden">
		                										@if($teacher->status==1)
		                										<option value="1">Vigente</option>
		                										<option value="0" selected="true">No vigente</option>
		                										@else
																<option value="1" selected="true">Vigente</option>
																<option value="0">No vigente</option>
																@endif
															</select>
														<div class="col-md-8 col-md-offset-2">

															
														</div>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Aceptar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal" data-target="#status<?php echo $a; ?>"><strong>Aceptar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
									</td>
								</tr>
							</tbody>
						@endforeach
						</table>
						<center>{{$teachers->links()}}</center>
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>

		<!-- START NEW EMPLOYEE MODAL -->
<div class="container-fluid spark-screen">
	<div class="modal fade" id="newEmployees" role="dialog" aria-labelledby="newEmployeesModal" style="background:transparent;">
		<div class="row">
			<div class="col-md-12">
				<!--Default box-->
				<div class="modal-dialog modal-lg" role="document">
					
					<div class="modal-content">
						<div class="box">
							<div class="modal-header">
								<div class="box-header">
									<button type="button" class="close btn-lg" data-dismiss="modal" aria-label="CERRAR">
										<span aria-hidden="true">&times;</span>
									</button>
									<h3 class="modal-title text-center col-lg-12" id="modalTitle">Agregar empleado</h3>
								</div>
							</div>
							<div class="box-body">
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="name">Nombre: </label>
									<div class="col-sm-9">
										<select name="selectEmployee" id="selectEmployee" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;" required="required">
											@foreach($users as $us)
											<option value="{{ $us->id }}">
												{{ $us->name }}
											</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="jobTitle">Puesto: </label>
									<div class="col-sm-9"> 
										<select name="inputjobTitle" id="inputjobTitle" class="jobTitleList-single form-control" style="width: 100%; font-weight: bold;" required="required">
											<option value="">Selecciona puesto</option>
											@foreach($jobTitle as $jt)
											<option value="{{ $jt->id_inc }}">{{ $jt->text }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group row col-lg-push-1 col-lg-11">
									<label class="control-label col-sm-3" for="jobTitle">Centro de Trabajo: </label>
									<div class="col-sm-9"> 
										<select name="inputJobCenter[]" multiple="multiple" class="form-control multiple jobCenterList-single" id="inputJobCenter" style="width: 100%; font-weight: bold;" required="required">
											@foreach($jobCenter as $jc)
											<option value="{{ $jc->id_inc}}"
												{{ (in_array($jc, old('jc', []))) }} >{{ $jc->text }}
											</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<div class="text-center">
									<button class="btn btn-primary btn-lg" type="button" id="EmployeeNew">Guardar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END NEW EMPLOYEE MODAL -->
@endsection


@section('personal-js')
<style>

table.tablesorter thead tr .header {
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
}
table.tablesorter thead tr .headerSortUp {
}
table.tablesorter thead tr .headerSortDown {
}
</style>
<!-- <link rel="stylesheet" href="{{ asset('css/style_sorterTable.css') }}"> -->
<script src="{{ asset('js/jquery.tablesorter.js') }}"></script>
<script>
    // call the tablesorter plugin 
    $("#myTable").tablesorter( {sortList: [[0,0], [1,0]]} ); 
</script>
<script>
	$(".applicantsList-single").select2();
	$(".applicantsList-single").change(function(event) {
		$('#Applicant_id').val($(this).val());
	});
	$(".jobTitleList-single").select2();
	$(".jobCenterList-single").select2();
</script>
<script src="{{ asset('js/sorttable.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/kardex.js') }}"></script>
<script>
	$(".multiple").select2();
  </script>
@stop