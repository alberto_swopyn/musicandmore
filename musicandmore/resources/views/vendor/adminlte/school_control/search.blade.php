@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::training.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
<div class="panel panel-primary" style="border-color: white;">
        <div class="pull-right">
			<a href="javascript:void(0)" data-perform="panel-collapse">
				<i class="ti-minus"></i></a>
			<a href="javascript:void(0)" data-perform="panel-dismiss">
				<i class="ti-close"></i></a>
		</div>

	<div class="box">
		<div class="box-header with-border">
			<div class="panel-body">

		<div class="box-body">
		<div class="row">

			<div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i
                                ></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i>
                                </button>
             </div>

<h3 class="titleCenter" style="text-align: center;">Resultado de la búsqueda:</h3>
 <br>
	<br>
	<div class="container">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered tablesorter sortable" id="myTable">
					<thead>
						<tr>
							<th class="text-center">Nombre del docente</th>
							<th class="text-center">Grupo</th>
							<th class="text-center">Materia</th>
							<th class="text-center">Horario</th>
							<th class="text-center">Fecha de asistencia</th>
							<th class="text-center">Evidencia</th>
						</tr>
					</thead>
					<tbody>
					@if(issets)
						<tr>
							@foreach($assistance as $assistan)
						<td class="text-center">{{$assistan->teacher}}</td>
						<td class="text-center">{{$assistan->group}}</td>
						<td class="text-center">{{$assistan->subject}}</td>
						<td class="text-center">{{$assistan->schedule}}</td>
						<td class="text-center">{{$assistan->date}}</td>
						<td class="text-center"><img src="{{asset( '../../swopyn/public/assets/Images/'.$assistan->evidence)}}" class="img-responsive center-block" style="width: 100px; height: 100px"></td>	
					</tr>
					@endforeach
                     @endif
				</div>
			</div>
		</div>
	</td>
	</tbody>
	</table>
</div>
</div>
</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>