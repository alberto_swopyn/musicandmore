@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">GRUPOS</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
						<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'indexG','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						<div class="col-md-4 col-md-offset-8">
						<a href="{{ route('groups_create') }}" class="btn btn-success" style="background-color: black; border-color: black;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span>Agregar Grupo</a>

						<a href="{{ Route('groups_assign')}}" class="btn btn-success" style="background-color: black; border-color: black;">
							<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Asignación
						</a>
						</div>
					</div>
					<br>
					<h3 class="titleCenter" style="text-align: center;">Grupos Actuales</h3>
						<div class="table-responsive">
						  <table class="table table-hover tablesorter sortable" id="myTable">
						    <thead style="background: black; color: white;">
						    	<tr>
						    	<th class="tableCourse" style="color: white">Nombre</th>
						    	<th class="tableCourse" style="color: white">Aula</th>
						    	<th class="tableCourse" style="color: white">Maestro</th>
						    	<th class="tableCourse" style="color: white">Materia</th>
						    	<th class="tableCourse" style="color: white">Horario</th>
						    	<th class="tableCourse" style="color: white">L</th>
						    	<th class="tableCourse" style="color: white">Ma</th>
						    	<th class="tableCourse" style="color: white">Mi</th>
						    	<th class="tableCourse" style="color: white">J</th>
						    	<th class="tableCourse" style="color: white">V</th>
						    	<th class="tableCourse" style="color: white">S</th>
						    	<th class="tableCourse" style="color: white">D</th>
						    	<th class="tableCourse" style="color: white">Cupo</th>
									<th class="tableCourse" style="color: white">Alumnos actuales</th>
									<th class="tableCourse" style="color: white">Color</th>
						    	<th colspan="2" class="text-center" style="color: white">Opciones</th>
						    	</tr>
						    </thead>
					<tbody>
								@foreach($group as $groups)
								<tr>
									<td>{{ $groups->name}}</td>
									<td>{{ $groups->classroom}}</td>
									<td>{{ $groups->teacher}}</td>
									<td>{{ $groups->subject_name}}</td>
									<td><?php $date = strtotime($groups->schedule);  
										print(date("g:i",$date));
									?>-<br><?php $date = strtotime($groups->final_hour);  
										print(date("g:i",$date));
									?></td>
									@if($groups->monday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($groups->tuesday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
										@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($groups->wednesday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($groups->thursday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($groups->friday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($groups->saturday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($groups->sunday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									<td>{{ $groups->quota}}</td>
									<td>{{ $groups->current_students}}</td>
									<td>
										<input type="color" id="color" name="color" disabled value="{{ $groups->color }}">
									</td>
									<td>
										<a href="{{ Route('groups_edit', ['group' => $groups->id]) }}" class="btn">
											<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
										</a>
									</td>
									<td>
										<form action="{{ route('groups_delete', ['group' => $groups->id]) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn" style="background-color: transparent;">
												<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
											</button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>

						  </table>
						  <center>{{$group->links()}}</center>
						</div>

					
		
	<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'indexA','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search2" id="search2">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>

						<h3 class="titleCenter" style="text-align: center;">Alumnos Inscritos</h3>
						<div class="table-responsive">
						<table class="table table-hover tablesorter sortable" id="table1">
						<thead style="background: black; color: white;">
							<tr>
								<th class="tableCourse" style="color: white">Estudiante</th>
						    	<th class="tableCourse" style="color: white">Grupo</th>
						    	<th class="tableCourse" style="color: white">Maestro</th>
						    	<th class="tableCourse" style="color: white">Aula</th>
						    	<th class="tableCourse" style="color: white">Materia</th>
						    	<th class="tableCourse" style="color: white">Horario</th>
						    	<th class="tableCourse" style="color: white">L</th>
						    	<th class="tableCourse" style="color: white">Ma</th>
						    	<th class="tableCourse" style="color: white">Mi</th>
						    	<th class="tableCourse" style="color: white">J</th>
						    	<th class="tableCourse" style="color: white">V</th>
						    	<th class="tableCourse" style="color: white">S</th>
						    	<th class="tableCourse" style="color: white">D</th>
								<th colspan="2" class="text-center">Opciones</th>
							</tr>
						</thead>
							<tbody>
								<tr>
									@foreach($assign as $a)
									<td class="text-center">{{$a->student}}</td>
									<td class="text-center">{{$a->name}}</td>
									<td class="text-center">{{$a->teacher}}</td>
									<td>{{ $a->classroom}}</td>
									<td class="text-center">{{$a->subject_name}}</td>
									<td class="text-center"><?php $date = strtotime($a->schedule);  
										print(date("g:i",$date));
									?>-<br><?php $date = strtotime($a->final_hour);  
										print(date("g:i",$date));
									?></td>
									@if($a->monday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($a->tuesday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
										@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($a->wednesday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($a->thursday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($a->friday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($a->saturday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									@if($a->sunday == 1)
									<td><i class="fa fa-check-circle" aria-hidden="true"></td>
									@else
									<td><i class="fa fa-times-circle" aria-hidden="true"></td>
									@endif
									<td>
										<form action="{{ route('groups_assign_delete', $a->ga_id) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn" style="background-color: transparent;">
												<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
											</button>
										</form>
									</td>
								</tr>
							</tbody>
						@endforeach
						</table>
						<center>{{$assign->links()}}</center>
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>
		</div>
			</div>

		</div>
		</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>