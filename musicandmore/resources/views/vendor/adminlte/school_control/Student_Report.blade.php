@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::training.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Reporte de Asistencia Alumnos</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
                              <div class="row">
                                    <div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
                                          {!!Form::open(['method' => 'GET','route'=>'index7','class'=>'navbar-form pull-right','role'=>'search'])!!}
                                                <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Buscar alumno" name="search" id="search">
                                                </div>
                                                <button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
                                          {!!Form::close()!!}
                                    </div>
                              </div>
		<h3 class="titleCenter" style="text-align: center;">REPORTE DE ASISTENCIA</h3>
		<h3 class="titleCenter" style="text-align: center;">ALUMNOS</h3>
		<h4 class="titleCenter" style="text-align: center;"></h4>
		<div class="row">
			<div class="col-md-9 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
				
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
				<form class="navbar-form pull-right" role="search" method="GET" action="{{ Route('searchByMonthStudent') }}">
                <div class="form-group">
                	<select name="year" class="form-control" id="year">
                		@if($year == '2018')
                		<option selected="selected" value="2018">2018</option>
                		<option value="2019">2019</option>
						@else                		
                		<option value="2018">2018</option>
                		<option selected="selected" value="2019">2019</option>
                		@endif
                	</select>
                    <select name="months" class="form-control" id="months">
                    	@if($mes == '1')
                        <option selected="selected" value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '2')
                        <option value="1">Enero</option>
                        <option selected="selected" value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '3')
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option selected="selected" value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '4')
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option selected="selected" value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '5')
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option selected="selected" value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '6')
                        <option value="1">Enero</option>
                        <option value="2">Febrero<option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option selected="selected" value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '7')
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option selected="selected" value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '8')
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option selected="selected" value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '9')
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option selected="selected" value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '10')
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option selected="selected" value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '11')
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option selected="selected" value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                        @elseif($mes == '12')
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option selected="selected" value="12">Diciembre</option>
                        @endif
                    </select>
                </div>
                <button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
                </form>
			</div>
		</div>
		<div class="input-group col-md-offset-1 col-md-6"></div>
		<input type="hidden" value="" name="Grupo" id="Grupo" >
		<div class="table-responsive">
			<table class="table table-hover table-striped tablesorter sortable" id="tableEmployees">
				<thead style="background: black; color: white;">
					<th class="text-center">ID Alumno</th>
					<th class="text-center">Nombre del alumno</th>
					<th class="text-center">Asistencias</th>
					<th class="text-center">Faltas</th>
					<th class="text-center">Justificadas</th>
				</thead>
				<tbody>
					<tr>
						@foreach($assistance as $assistan)
						<td class="text-center">{{ $assistan['user_id'] }}</td>
						<td class="text-center">
							{{ $assistan['student'] }}
						</td>
						<td class="text-center">
							<a type="button" class="btn btn-success" aria-label="Left Align" title="Asistencias" href="{{ route('report_assistance_detail_student', ['idStudent'=> $assistan['user_id'], 'month'=> $mes, 'year'=> $year, 'type' => 1]) }}"><strong>
							{{ $assistan['assists'] }}</strong>
							</a>
						</td>
						<td class="text-center">
							<a type="button" class="btn btn-danger" aria-label="Left Align" title="Faltas" href="{{ route('report_assistance_detail_student', ['idStudent'=> $assistan['user_id'], 'month'=> $mes, 'year'=> $year, 'type' => 0]) }}"><strong>
							{{ $assistan['faults'] }}</strong>
							</a>
						</td>
						<td class="text-center">
							<a type="button" class="btn btn-info" aria-label="Left Align" title="Justificadas" href="{{ route('report_assistance_detail_student', ['idStudent'=> $assistan['user_id'], 'month'=> $mes, 'year'=> $year, 'type' => 2]) }}"
							><strong>
							{{ $assistan['justified'] }}</strong>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<center>{{$assistancePag->links()}}</center>
		</div>
		<br>
		<br>
		<br>
</div>
</div>
</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>