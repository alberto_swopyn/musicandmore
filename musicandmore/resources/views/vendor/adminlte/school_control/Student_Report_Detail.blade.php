@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::training.PersonalSelect2')
@stop
@endsection
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Reporte de Asistencia Detallado</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
		@if($type == 1)
		<h3 class="titleCenter" style="text-align: center;">REPORTE DE ASISTENCIAS</h3>
		@elseif($type == 2)
		<h3 class="titleCenter" style="text-align: center;">REPORTE DE RETARDOS</h3>
		@else
		<h3 class="titleCenter" style="text-align: center;">REPORTE DE FALTAS</h3>
		@endif
		<h3 class="titleCenter" style="text-align: center;">
        </h3>
        <h4 class="titleCenter" style="text-align: center;"></h4>
		<div class="row">
			<div class="col-md-9 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
				
			</div>
		</div>
		<div class="input-group col-md-offset-1 col-md-6">
			
		</div>
		<input type="hidden" value="" name="Grupo" id="Grupo" >
		<div class="table-responsive">
			<table class="table table-hover table-striped tablesorter sortable" id="tableEmployees">
				<thead style="background: black; color: white;">
					<th class="text-center">Alumno</th>
					<th class="text-center">Maestro</th>
					<th class="text-center">Asignautra</th>
					<th class="text-center">Grupo</th>
					<th class="text-center">Fecha</th>
					<th class="text-center">Hora</th>
				</thead>
				<tbody>
					<tr>
                        @if (!empty($assistance[0]->student))
						@foreach($assistance as $assistan)
						<td class="text-center">{{$assistan->student}}</td>
						<td class="text-center">{{$assistan->teacher}}</td>
						<td class="text-center">{{$assistan->subject}}</td>
						<td class="text-center">{{$assistan->group}}</td>
						<td class="text-center"><?php echo date("d-m-Y",strtotime($assistan->date)); ?></td>
						<td class="text-center"><?php echo date("H:m",strtotime($assistan->date)); ?></td>
					</tr>
                        @endforeach
                        @else
                        <td class="text-center">No hay registros</td>
                        @endif
				</tbody>
			</table>
			<center>{{$assistance->links()}}</center>
		</div>
		<br>
		<br>
		<br>
</div>
</div>
</div>
@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>