@extends('adminlte::layouts.app')
@section('htmlheader_title')
 {{ trans('adminlte_lang::message.home') }}
 @section('personal_style')
@include('adminlte::school_control.PersonalSelect2')
@stop
@endsection
@section('main-content')

<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Nuevo Grupo</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
	<form action="{{ Route('groups_add') }}" method="POST" role="form" enctype="multipart/form-data">{{ csrf_field() }}
        <div class="pull-right">
			<a href="javascript:void(0)" data-perform="panel-collapse">
				<i class="ti-minus"></i></a>
			<a href="javascript:void(0)" data-perform="panel-dismiss">
				<i class="ti-close"></i></a>
		</div>
		<div class="panel-body">
			<div class="form-group col-md-12">
				<div class="col-md-12 text-center">
					<label for="Nombre" style="font-size: 1.5em">Nombre del grupo</label>
				</div>
				<div class="col-md-8 col-md-offset-2">
					<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Nombre"   style="border: 0.5px solid ; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
				</div>
			</div>
			
			<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<br>
				<div class="col-md-12">
					<div>
						<span style="font-size: 1.5em">Aula</span>
					</div>
					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em"><i class="fa fa-address-card" aria-hidden="true" style="font-size: 2em; color: black;"></i>
							</span>
							<select name="classroom[]" class="form-control" id="classroom" required="required">
								@foreach($classroom as $classr)
									<option value="{{ $classr->id}}"
									{{ (in_array($classr, old('classr', []))) }} >{{ $classr->name }}
									</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

			<div class="col-md-12">
			<div>
				<span style="font-size: 1.5em">Maestro/Materia</span>
			</div>
			<div class="form-group">
				<div class="input-group col-md-offset-1 col-md-11">
					<span class="input-group-addon" style="border: none; width: 1em"><i class="fa fa-chalkboard-teacher" aria-hidden="true" style="font-size: 2em; color: black;"></i>
					</span>
					<select name="cathedras[]" class="form-control" id="cathedras" required="required">
						@foreach($cathedra as $cathedras)
							<option value="{{ $cathedras->id}}"
								{{ (in_array($cathedras, old('cathedras', []))) }} >{{ $cathedras->subject_name }} -- {{$cathedras->teacher}}
							</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<div class="col-xs-6">
				<br>
			   	<br>
				<div class="text-center">
					<label for="Fecha_inicial" style="font-size: 1.5em">Fecha inicial</label>
				</div>

	            <div class="col-md-12 text-center" >
	                <input id="Fecha_inicial" type="date" name="initial_date" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
		        </div>
			</div>

			<div class="col-xs-6">
				<br>
			   	<br>
				<div class="text-center">
					<label for="Fecha_final" style="font-size: 1.5em">Fecha final</label>
				</div>

	            <div class="col-md-12 text-center" >
	                <input id="Fecha_final" type="date" name="final_date" class="form-control" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required>
		        </div>

			</div>

        </div>


			<div class="form-group row">
				<div class="col-xs-6">
					<br>
					<br>
			    <div class="text-center">
						<label for="Fecha_inicial" style="font-size: 1.5em">Horario</label>
					</div>

			    <div class="col-md-12 text-center" >
			      <form action="" name="form">
							<label for="initial_hour">De: </label>
							<input type="time" name="initial_hour" id="hora" value="00:00:00" max="22:00:00" min="10:00:00" step="1" required="required">
							<label for="final_hour"> A: </label>
							<input type="time" name="final_hour" id="hora" value="00:00:00" max="22:00:00" min="10:00:00" step="1" required="required">
						</form>
				  </div>
			</div>

		            <div class="col-xs-6">
		            	<br>
									<br>
			            <div class="text-center">
										<label for="Fecha_final" style="font-size: 1.5em">Cupo de alumnos</label>
									</div>

			            <div class="col-md-12 text-center" >
			              <label>
										<input id="cupo" name="cupo" size="20" type="number" min="0" style="padding-left: 3em; border-radius: 8px" class="col-md-8 col-md-push-3" step="1" id="" value="" required="required">
										</label>
				        	</div>
							</div>
							
							<div class="col-xs-12">
		            	<br>
									<br>
			            <div class="text-center">
										<label for="Fecha_final" style="font-size: 1.5em">Color identificador</label>
									</div>

			            <div class="col-md-12 text-center" >
			              <label>
											<input type="color" id="color" name="color">
										</label>
						</div>
						
						<div class="col-md-12 text-center">
							<br>
							<label style="font-size: 1.5em" for="selectEmployee">Sucursal</label>
							<select name="selectEmployee" id="selectEmployee" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;" required="required">
								@foreach($su as $s)
									<option value="{{ $s->tree_job_center_id }}">
										{{ $s->text }}
									</option>
								@endforeach
							</select>
						</div>
			        </div>
		    </div>
			<br>
			<br>

			<div class="text-center">
						<span style="font-size: 1.5em">Días de impartir la materia</span>
			</div>
			<div class="table-responsive">
			<table class="table table-hover table table-striped" id="tableTasks">
				<thead style="background: black; color: white;">
					<th class="text-center">Lunes</th>
					<th class="text-center">Martes</th>
					<th class="text-center">Miercoles</th>
					<th class="text-center">Jueves</th>
					<th class="text-center">Viernes</th>
					<th class="text-center">Sábado</th>
					<th class="text-center">Domingo</th>
				</thead>
				<tbody>
					<tr>
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Monday" name="Monday" value="1"></td>
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Tuesday" name="Tuesday" value="1"></td>
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Wednesday" name="Wednesday" value="1"></td>
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Thursday" name="Thursday" value="1"></td>
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Friday" name="Friday" value="1"></td>
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Saturday" name="Saturday" value="1"></td>
						<td class="text-center"><input type="checkbox" class="form-check-input" id="Sunday" name="Sunday" value="1"></td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>
		<br>
		<br>
		<br>
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
		</div>
	</form>
</div>
<script>
      $(document).ready(function(){
          $('#subjects').select2();
          $('#cathedras').select2();
       });
    </script>
@endsection