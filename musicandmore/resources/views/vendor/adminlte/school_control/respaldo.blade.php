@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Alumnos</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'index_status','class'=>'navbar-form pull-right','role'=>'search'])!!}
  					<div class="form-group">
    				<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  					</div>
 	 				<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
					{!!Form::close()!!}
					<button type="submit" title="Editar" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a; " data-toggle="modal" data-target="#createStudent">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Alumno
					</button>
					</div>
					
										<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="createStudent" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="{{ Route('sampleuser_add') }}" method="POST" id="form">
															{{ csrf_field() }}
													<div class="modal-body">
														<h4 class="modal-title">Agregar Alumno</h4>
														
														<div class="form-group col-md-12">
								<div class="col-md-12 text-center">
									<label for="Nombre" style="font-size: 1.5em">Nombre</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" id="Nombreuser" name="Nombreuser" placeholder="Nombre del usuario"   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
								</div>
								<div class="col-md-12 text-center">
									<label for="Correo" style="font-size: 1.5em">Correo</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" id="CorreoUser" name="CorreoUser" placeholder="Correo del usuario"   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
								</div>
								<div class="col-md-12 text-center">
									<label for="Correo" style="font-size: 1.5em">Teléfono de contacto</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" id="Telefono" name="Telefono" placeholder="10 dígitos" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" pattern="[0-9]{10}" maxlength="10" required="required">    
								</div>
								<div class="col-md-12 text-center">
									<label for="Experience" style="font-size: 1.5em">Experiencia</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<select name="experience" class="form-control" id="experience" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.1em; border-radius: 7px" required="">
		                				<option value="" selected="true">Seleccione una opción...</option>
		                				<option value="1">Sí</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>

														<div class="col-md-8 col-md-offset-2">

															
														</div>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Eliminar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal" data-target="#createStudent"><strong>Crear</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
					</div>
					

					<br>
						<h3 class="titleCenter" style="text-align: center;">Alumnos</h3>
						<div class="table-responsive">
						<table class="table table-hover table table-striped tablesorter sortable" id="myTable">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Nombre</th>
								<th class="text-center">Tipo de alumno</th>
								<th class="text-center">Estatus</th>
								<th class="text-center">Solicitud</th>
								<th class="text-center">Cuestionario</th>
							</tr>
						</thead>
						<?php $a = 0; ?>
						@foreach($students as $student)
						<?php $a = $student->id; ?>
						<tbody>
								<tr>
									<td class="text-center">
										<a href="{{ Route('student_edit', ['student' => $student->id]) }}">{{$student-> name}}</a>
									</td>
									@if($student->sample == 1)
										<td class="text-center">
											<button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="Muestra" disabled="true">
											<strong>Muestra </strong><i class="fa fa-check-circle" style="color: white;"></i></td>
											</button>
										</td>
									@else
										<td class="text-center">
										
										</td>
									@endif
									@if($student->status == 1)
										<td class="text-center">
											<!-- <button type="button" class="btn btn-success" aria-label="Left Align" data-toggle="tooltip" title="Vigente" disabled="true">
											<strong>Vigente </strong><i class="fa fa-check-circle" style="color: white;"></i></td>
											</button> -->
											<button type="submit" title="Editar" class="btn btn-success" aria-label="Left Align" title="Vigente" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>">
											<strong>Vigente </strong><i class="fa fa-check-circle" style="color: white;"></i>
											</button>
										</td>
									@else
										<td class="text-center">
											<!-- <button type="button" class="btn btn-danger" aria-label="Left Align" data-toggle="tooltip" title="No vigente" disabled="disabled"><strong>No vigente </strong><i class="fa fa-times-circle" aria-hidden="true"></i>
										</button> -->
										<button type="submit" class="btn btn-danger" aria-label="Left Align" title="No vigente" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>">
											<strong>No vigente </strong><i class="fa fa-times-circle" aria-hidden="true"></i>
										</button>
									</td>
									@endif
									

										<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="confirm-edit<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="{{ route('studenStatus_update', ['studentStatus' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('PUT') }}
													<div class="modal-body">
															<h4 class="modal-title">¿Está seguro de que desea cambiar el Estatus?</h4>
														

														<select name="StatusEdit<?php echo $a; ?>" class="form-control" id="StatusEdit<?php echo $a; ?>" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.1em; border-radius: 7px; visibility:hidden">
		                										@if($student->status==1)
		                										<option value="1">Vigente</option>
		                										<option value="0" selected="true">No vigente</option>
		                										@else
																<option value="1" selected="true">Vigente</option>
																<option value="0">No vigente</option>
																@endif
															</select>
														<div class="col-md-8 col-md-offset-2">

															
														</div>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Aceptar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>"><strong>Aceptar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
									
									<td class="text-center">
										<a title="Solicitud" class="btn btn-info" style="background-color: #8942AC; border-color: #8942AC;" href="{{ Route('student_edit', ['student' => $student->id]) }}">
											<span class="fa fa-address-card" aria-hidden="true"></span>	
										</a>
									</td>

									<td class="text-center">
										<a href="{{ Route('student_quiz', ['student' => $student->id]) }}" title="Editar" class="btn btn-info" style="background-color: #387A93; border-color: #387A93;">
											<span class="fa fa-book" aria-hidden="true"> </span>
										</a>
									</td>

								</tr>
							</tbody>
						@endforeach
						</table>
						<center>{{$students->links()}}</center>
						</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->

				</div>

			</div>
		</div>




		@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>