@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h2 class="titleCenter">ESTRUCTURA DE CENTROS DE TRABAJO</h2>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-4 col-sm-8 col-xs-8">
									<button type="button" class="btn btn-primary" id="saveTree"><i class="glyphicon glyphicon-floppy-save"></i> Guardar</button>
					<!--	<button type="button" class="btn btn-warning btn-sm" onclick="demo_rename();"><i class="glyphicon glyphicon-pencil"></i> Rename</button>
						<button type="button" class="btn btn-danger btn-sm" onclick="demo_delete();"><i class="glyphicon glyphicon-remove"></i> Delete</button>-->
					</div> 
					<div class="col-md-8 col-sm-4 col-xs-4" style="text-align:right;">
						<i class="glyphicon glyphicon-search"></i>
						<input type="text" value=""  id="demo_q" placeholder="Buscar"  />
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="jstree_jobcenter" class="demo" style="margin-top:1em; min-height:200px;">
						</div>
					</div>
				</div>


			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</div>
</div>
</div>
@endsection


