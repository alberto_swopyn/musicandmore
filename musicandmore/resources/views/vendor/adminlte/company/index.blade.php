@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Empresas</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						

						<div class="table-responsive">
						  <table class="table table-hover">
						    <thead><tr>
						    	<td>Nombre</td>
						    	<td>Nombre comercial</td>
						    	<td>Teléfono</td>
						    	<td>Contacto</td>
						    	<td>Imagen</td>
						    	<td></td>
						    </tr></thead>
							<tbody>
								@foreach($companies as $company)
								<tr>
								<td>{{ $company->name }}</td>
								<td>{{ $company->bussines_name }}</td>
								<td>{{ $company->phone }}</td>
								<td>{{ $company->contact }}</td>
								<td><img src="{{ route('show_image',['company' => $company->id]) }}" alt="logo" width="70" height="30"></td>

								<td>
									<div class="row">
										<div class="col-md-2">
											<a href="{{ route('company_edit', ['company'=> $company->id]) }}" class="btn btn-info"><span class="glyphicon glyphicon-edit" aria-hidden="true"> </span></a>
										</div>
										<div class="col-md-2">
											<form action="{{ route('company_delete', ['company' => $company->id]) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></button>
											</form>
										</div>
									</div>
								</td>
								</tr>
								@endforeach
							</tbody>
	
						    
						  </table>
						</div>
						{{ $companies->links() }}
											
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>


		</div>
	</div>
@endsection
