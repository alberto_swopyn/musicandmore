@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
  
 <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Empresa</h3>
                        
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
    
                            <div class="embed-responsive embed-responsive-16by9">
                              <iframe  class="embed-responsive-item" id="presentation" style="float: left;width: 864px;height: 540px; border: 1px solid #000;" src="{{ asset('iSpring/index.html') }}"></iframe>
                            </div>

                            <table width="100%"><tr>
                            <td>
                                <button type="button" class="btn btn-default btn-lg" aria-label="Right Align" id="button-prev"  style="float: left; display: inline-block; width: 80px; height: 35px;">
                                 <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                                </button>

                            </td>
                            <td align="center">
                               <div id="slide-counter" style="float: left; text-align: center; min-width: 80px; border: 1px solid #000; vertical-align: bottom; display: inline-block; height: 45px;" >
                               <input id="current-slide" type="text" size="3" style="margin-top: 7px; border: 1px solid; width: 35%;">
                               <span>/</span>
                                <span id="slide-count">0</span>
                                </div> 
                            </td>
                            <td align="right">
                                <button type="button" class="btn btn-default btn-lg" aria-label="Right Align" id="button-next" style="float: left; display: inline-block; width: 80px; height: 35px;" >
                                 <span class="glyphicon glyphicon-menu-right glyphicon-menu-right" aria-hidden="true"></span>
                                </button> 
                            </td></tr>
                            <tr><td colspan="3" align="center"><hr>
                                    <form action="{{ route('course_finish') }}" method="POST" role="form">
                                    {{ csrf_field() }}
                                <input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
                                 <input type="hidden" id="email_user" name="email_user" value="{{ auth()->user()->email }}">
                                <button type="submit" class="btn btn-default" aria-label="Left Align" id="btnCourse" >Finalizar
                                      <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
                                    </button>
                                    
                                
                                    
                                </form></td></tr></table>
                            <input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
                           
                          

                        </div>
                        </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>

    
    <script>
        var ispringPresentationConnector = {};

        truncateDecimals = function (number) {
            return Math[number < 0 ? 'ceil' : 'floor'](number);
        };

        ispringPresentationConnector.register = function(player) {
            var presentation = player.presentation();
            var playbackController = player.view().playbackController();
            var slidesCount = presentation.slides().count();
            initPlaybackControls(playbackController, slidesCount);
        };


        function initPlaybackControls(playbackController, slidesCount) {
            var prevBtn = document.getElementById("button-prev");
            prevBtn.onclick = function() {
                playbackController.gotoPreviousSlide();
            };

            var nextBtn = document.getElementById("button-next");
            nextBtn.onclick = function() {
                playbackController.gotoNextSlide();

            };

             var slideCountLabel = document.getElementById("slide-count");
            slideCountLabel.innerHTML = slidesCount.toString();
            var slideNoInput = document.getElementById("current-slide");

            playbackController.slideChangeEvent().addHandler(function(slideIndex)
            {

                slideNoInput.value = (slideIndex + 1).toString();
                $(function(){
                    // var valor=slideNoInput.value;
                    // alert(valor);
                    var slider = truncateDecimals(((slideNoInput.value *100) / slidesCount),2);

                    $('#progressControlCourse').empty();
                    $.ajax({
                          type: 'POST',
                          url:  "{{ route( 'candidate_course' ) }}",
                          data: { 
                            '_token': $('meta[name=csrf-token]').attr('content'),
                            user: $('#user').val(), 
                            slider: (slideNoInput.value *100) / slidesCount }
                        })
                          .done(function( msg ) {
                            $('#progressControlCourse').append('<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="'+slider+' " aria-valuemin="0" aria-valuemax="100" style="width: '+slider+'%" >'+slider+'</div>');
                          });
                    });
            });

            slideNoInput.onchange = function()
            {
                
                var currentSlideIndex = playbackController.currentSlideIndex();
                var nextSlideIndex = parseInt(slideNoInput.value) - 1;
                if (nextSlideIndex >= 0 && nextSlideIndex < slidesCount)
                {
                    playbackController.gotoSlide(nextSlideIndex);
                }
                else
                {
                    slideNoInput.value = (currentSlideIndex + 1).toString();
                }
            };
        }
  </script>

       

@endsection