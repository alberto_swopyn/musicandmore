

<div class="form-group">
		<label for="Nombre">Nombre</label>
		<input type="text" class="form-control" id="Nombre" name="Nombre" 
		placeholder="Swopyn" value="{{ $company->name or old('Nombre') }}">
</div>
							
<div class="form-group">
		<label for="Rfc">R.F.C.</label>
		<input type="text" class="form-control" id="Rfc" name="Rfc" placeholder="AABB123456CCC" value="{{ $company->rfc or old('Rfc') }}">
</div>
							
<div class="form-group">
		<label for="Direccion">Dirección</label>
		<input type="text" class="form-control" id="Direccion" name="Direccion" placeholder="Dimicilio fiscal" value="{{ $company->address or old('Direccion') }}">
</div>

<div class="form-group">
		<label for="Telefono">Teléfono</label>
		<input type="text" class="form-control" id="Telefono" name="Telefono" placeholder="449 923 23 55" value="{{ $company->phone or old('Telefono') }}">
</div>
<div class="form-group">
		<label for="Logo">Logo</label>
		<input type="file" class="form-control" id="Logo" name="Logo" value="{{ $company->logo or old('Logo') }}">
</div>
<div class="form-group">
		<label for="Imagen">Imagen</label>
		<input type="file" class="form-control" id="Imagen" name="Imagen" value="{{ $company->image or old('Imagen') }}" >
</div>
<div class="form-group">
		<label for="NombreCompañia">Nombre Compañia</label>
		<input type="text" class="form-control" id="NombreCompañia" name="NombreCompañia" placeholder="Nombre de grupo" value="{{ $company->bussines_name or old('NombreCompañia') }}">
</div>
<div class="form-group">
		<label for="Persona">Persona</label>
		<select class="form-control" name="Persona" id="Persona" value="{{ $company->tax_regime or old('Persona') }}">
				<option value="1">Física</option>
				<option value="2">Moral</option>
		</select>
</div>
<div class="form-group">
		<label for="CuentaBanco">Cuenta banco</label>
		<input type="text" class="form-control" id="CuentaBanco" name="CuentaBanco" placeholder="1234-1234-1234-1234" value="{{ $company->bank_account or old('CuentaBanco') }}">
</div>
<div class="form-group">
		<label for="TipoPago">Tipo de pago</label>
		<select class="form-control" name="TipoPago" id="TipoPago"  value="{{ $company->pay_type_id or old('TipoPago') }}">
			@foreach($paytypes as $paytype)
				<option value="{{ $paytype->id}}">{{ $paytype->name}}</option>
			@endforeach
		</select>
		</div>
<div class="form-group">
		<label for="Giro">Giro</label>
		<select class="form-control" name="Giro" id="Giro"  value="{{ $company->company_type or old('Giro') }}">
				<option value="1">Servicios</option>
				<option value="2">Comercio</option>
				<option value="3">Manufactura</option>
		</select>
</div>
<div class="form-group">
		<label for="Especialidad">Especialidad</label>
		<select class="form-control" name="Especialidad" id="Especialidad"  value="{{ $company->specialty or old('Especialidad') }}">
			<option value="1">Abarrotes</option>
			<option value="2">Papelería</option>	
			<option value="3">Ferretería</option>
			<option value="4">Boutique</option>
			<option value="5">Electrónica</option>
			<option value="6">Zapateria</option>
		</select>
</div>
<div class="form-group">
		<label for="NoEmpleados">Numero de empleados</label>
		<select class="form-control" name="NoEmpleados" id="NoEmpleados" value="{{ $company->no_employees or old('NoEmpleados') }}" >
			<option value="1">1-10</option>
			<option value="2">11-100</option>
			<option value="3">101-200</option>
			<option value="4">201-500</option>
			<option value="5">501-1000</option>
			<option value="6">1001-10000</option>
		</select>
</div>
<div class="form-group">
		<label for="NoSucursal">Numero de sucursales</label>
		<select class="form-control" name="NoSucursal" id="NoSucursal" value="{{ $company->no_branch_office or old('NoSucursal') }}">
			<option value="1">1-10</option>
			<option value="2">11-100</option>
			<option value="3">101-200</option>
			<option value="4">201-500</option>
			<option value="5">501-1000</option>
			<option value="6">1001-10000</option>
		</select>
</div>
<button type="submit" class="btn btn-primary">Guardar</button>




