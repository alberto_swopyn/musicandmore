@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Alumnos muestra</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
							<i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						<i class="glyphicon glyphicon-search"></i>
						<input type="text" value="" data-table="tableTasks" class="search_employees" placeholder="Buscar"/>
					</div>
					
					</div>
 
					<h3 class="titleCenter" style="text-align: center;">Alumnos muestra actuales</h3>
					<div class="table-responsive">
						<table class="table table-hover table table-striped" id="tableTasks">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Nombre</th>
								<th colspan="3" class="text-center">Opciones</th>
							</tr>
						</thead>
						<?php $a = 0; ?>
						@foreach($sam as $sams)
						<tbody>
							<?php $a = $sams->id; ?>
								<tr>
									<td class="text-center">{{$sams-> name}}</td>
									<td class="text-center">
										<button type="submit" title="Editar" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a;" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>">
											<span class="glyphicon glyphicon-edit" aria-hidden="true"> </span>
										</button>
										<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="confirm-edit<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="{{ route('sampleuser_update', ['sample' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('PUT') }}
													<div class="modal-body">
														<h4 class="modal-title">Nombre del usuario</h4>
														<div class="col-md-10 col-md-offset-1">
															<input type="text" class="form-control" id="NombreSamEdit<?php echo $a; ?>" name="NombreSamEdit<?php echo $a; ?>" placeholder="Nombre" value="{{$sams-> name}}" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
														</div>
															<h4 class="modal-title">Correo del usuario</h4>
														<div class="col-md-10 col-md-offset-1">
															<input type="text" class="form-control" id="CorreoSamEdit<?php echo $a; ?>" name="CorreoSamEdit<?php echo $a; ?>" placeholder="Correo" value="{{$sams-> email}}" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
														</div>
														<h4 class="modal-title">Teléfono de contacto</h4>
														<div class="col-md-10 col-md-offset-1">
															<input type="text" class="form-control" id="TelSamEdit<?php echo $a; ?>" name="TelSamEdit<?php echo $a; ?>" placeholder="10 dígitos" value="{{$sams-> phone_number}}" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" pattern="[0-9]{10}" maxlength="10" required="required">
														</div>
														<h4 class="modal-title">Experiencia</h4>
														<div class="col-md-10 col-md-offset-1">
															<select name="SamEditExp<?php echo $a; ?>" class="form-control" id="SamEditExp<?php echo $a; ?>" style="border: 1px solid #59c4c5; font-weight: bold; font-size: 1.1em; border-radius: 7px">
		                										@if($sams->experience==1)
		                										<option value="1" selected="true">Sí</option>
		                										<option value="0">No</option>
		                										@else
																<option value="1">Sí</option>
																<option value="0" selected="true">No</option>
																@endif
															</select>
														</div>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Editar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>"><strong>Guardar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
									</td>
									
									<td class="text-center">
										<button type="submit" title="Eliminar" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete<?php echo $a; ?>">
											<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
										</button>
										<!-- INICIA MODAL PARA ELIMINAR REGISTRO -->
										<div class="modal fade" id="confirm-delete<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<div class="modal-body">
														<h4 class="modal-title">¿Desea eliminar este Alumno?</h4>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														<form action="{{ route('sample_delete', ['sample' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('DELETE') }}
															<button type="submit" title="Eliminar" class="btn btn-danger" style="background-color: black; border-color: black;" data-toggle="modal" data-target="#confirm-delete"><strong>Eliminar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->
									</td>
								</tr>
							</tbody>
						@endforeach
						</table>
					</div>


					</div>
					<!-- /.box -->
		

			</div>

		</div>
	</div>
</div>
		@endsection