@section('style_evaluation')
<style>
input.hidden-inputs.answer{
	width: 1.3em;
	height: 1.3em;
}
span.optionsEvaluation{
	font-size: 1.3em;
	text-align: justify;
	color: black;
	font-weight: normal;
	font-family: Gotham;
}
label.input-group.input-group-radio.row{
	margin-left: .5em;
}
span.text-muted{
	font-weight: bold;
}
</style>
@stop