<div class="col-md-8 col-md-offset-2">
	@include('adminlte::layouts.partials._errors')
	<legend>Agendar nueva entrevista</legend>
	<div class="form-horizontal">
		<div class="form-group">
			<b><p class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_solicitante"></p></b>
			<label class="letters col-md-4 control-label">Solicitante</label>
			<div class="col-md-8">
				<select name="Applicant_id" id="Applicant_id" class="form-control"  value="{{ old('Applicant_id') }}">
					<option value="">Selecciona a un solicitante</option>
					@foreach ($applicants as $applicant)
					{{ $name = $applicant -> first_name . ' ' . $applicant -> last_name }}
					<option value="{{ $applicant-> id }}">{{ $name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="form-group">
			<b><p class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_diaentrevista"></p></b>
			<label class="letters col-md-4 control-label">Día de entrevista</label>
			<div class="col-md-8">
				<input type="date" class="form-control" id="Fecha_entrevista" name="Fecha_entrevista"  value="{{ old('Fecha_entrevista') }}">
			</div>
		</div>
		<div class="form-group">
			<b><p class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_horaentrevista"></p></b>
			<label class="letters col-md-4 control-label">Hora de entrevista</label>
			<div class="col-md-8">
				<input type="time" class="form-control" id="Hora_entrevista" name="Hora_entrevista" value="{{ old('Hora_entrevista') }}">
			</div>
		</div>
		<div class="form-group">
			<b><p class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_entrevistador"></p></b>
			<label class="control-label col-md-4">Entrevistador</label>
			<div class="col-md-8">
				<select name="Entrevistador" id="Entrevistador" class="form-control" value="{{ old('Entrevistador') }}">
					<option value="">Selecciona un entrevistador</option>
					<option value="1">Entrevistador número 1</option>
					<option value="2">Entrevistador número 2</option>
					<option value="3">Entrevistador número 3</option>
					<option value="4">Entrevistador número 4</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<b><p class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_lugarentrevista"></p></b>
			<label class="letters col-md-4 control-label">Lugar de Entrevista</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="Lugar_entrevista" name="Lugar_entrevista" value="{{ old('Lugar_entrevista') }}">
			</div>
		</div>
		<div class="text-center">
			<br>
			<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em" id="agenda_entrevista">Guardar</button>
		</div>
	</div>
</div>