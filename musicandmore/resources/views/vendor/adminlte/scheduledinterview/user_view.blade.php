<div class="container-fluid spark-screen">
	<div class="row">
		<br>
		<div class="col-md-12">
			<!-- Default box-->
			@if (isset($interview_see))
			<div class="box-header text-center with-border">
				<h2>QUEREMOS CONOCERTE</h2>
			</div>
			<div class="box-body">
				<div class="col-md-12">
					<div class="flex-parent">
						<div class="col-md-5 flex-child text-center">
							<div style="background: #F2F2F2; margin-top: 7em">
								<br>
								<p>Tu PRESENTACIÓN AL COMITE está agendada para</p>
								@php
								setlocale(LC_ALL,"es_ES");
								@endphp
								<h3>{{ strftime("%e %B %Y", strtotime($interview_see->interview_date)) }}</h3>
								<h4>{{ $interview_see->interview_time }}</h4>
								<p>tu entrevistador es</p>
								<h3>{{ $interview_see->interviewer_name }}</h3>
								<br>
							</div>
						</div>
						<div class="col-md-7 flex-child text-center">
							@if($interview_data != null)
							<p>Favor de presentarse en:</p>
							<h4>{{$interview_data->street}} {{$interview_data->num_ext}} @if($interview_data->num_int != null) {{$interview_data->num_int}}@endif {{$interview_data->district}} C.P {{$interview_data->zip_code}} {{$interview_data->location}} {{$interview_data->state}}</h4>
							<div class="col-md-12">
								<iframe src="https://www.google.com/maps/embed/v1/place?q={{$interview_data->ubication}}&key=AIzaSyCljRNbfautXf9Hy1LkIAaGPd2aPQpiQxQ" frameborder="0" style="border:0; width: 100%; height: 25em"  allowfullscreen></iframe>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
			<!--box-body-->
			@else
			<div class="box-header text-center with-border">
				<h2>Estamos Revisando tu solicitud</h2>
			</div>
			<div class="box-body text-center">
				<div class="col-md-12">
					<div class="col-md-12">
						<p>El personal de Recursos Humanos se encuentra revisando tu solicitud, cuando se te agende una entrevista serás notificado y podras visualizar la programación en esta misma pestaña. </p>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
	<!--.box-->
</div>