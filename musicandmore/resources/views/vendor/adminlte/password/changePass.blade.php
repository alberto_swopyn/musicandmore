@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<link rel="stylesheet" href="{{ asset('/css/style_auth.css')}}">
<script src="{{ asset('/js/ProgressDialogModal.js') }}" ></script>

<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Cambiar Contraseña</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							
						<h3 class="titleCenter" style="text-align: center;">Para cambiar su contraseña complete los siguientes campos...</h3>
						
						<form action="{{ route('update_pass') }}" name="comprobacion" onsubmit='return comprobarClave()'> 
              			<div class="form-group col-md-12">

							<div class="col-md-12 text-center">
								<label for="Nombre" style="font-size: 1.5em">Nueva contraseña</label>
							</div>

							<div class="col-md-8 col-md-offset-2">
								<input class="form-control" placeholder="Contraseña" required id="contra" name="contra" type="password" autocomplete="off" minlength="6" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px">
							</div>

						</div>

						<div class="form-group col-md-12">

							<div class="col-md-12 text-center">
								<label for="Nombre" style="font-size: 1.5em">Vuelva a escribir la nueva contraseña</label>
							</div>

							<div class="col-md-8 col-md-offset-2">
								<input class="form-control" placeholder="Confirmar contraseña" required id="password_confirmation" name="password_confirmation" type="password" autocomplete="off" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px">
							</div>

						</div>

						<div class="col-md-12 text-center">
							<button type="submit" class="btn btn-primary" style="">Guardar</button>
						</div>
						
						</form>

						
					</div>
						<!-- /.box-body -->
			</div>
					<!-- /.box -->

		</div>

	</div>
</div>

<script type="text/javascript"> 
function comprobarClave(){ 
   	clave1 = document.comprobacion.contra;
   	clave2 = document.comprobacion.password_confirmation;

   		if (clave1.value==clave2.value && clave1.value.length>=6) 
   		{
   			//Todo correcto!!!
        	return true;
   		}else{
   			if(clave1.value.length<6)
            alert("La contraseña es muy corta...\nEscriba una contraseña de mínimo 6 caracteres.");
        else if(clave1.value!=clave2.value)
            alert("Las dos claves son distintas...\nEscriba la misma contraseña en ambos campos.");
        else if(pasNew1.value.search(patron1)<0 || pasNew1.value.search(patron2)<0)
            id_epassNew.innerHTML="La contraseña tiene que tener numeros y letras";
        return false;
   		}
 
} 
</script> 
@endsection