@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<link rel="stylesheet" href="{{ asset('/css/style_auth.css')}}">
<script src="{{ asset('/js/ProgressDialogModal.js') }}" ></script>

<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Cambiar Contraseña</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							
						<h2 class="titleCenter" style="text-align: center;"><i class="fa fa-check-circle" style="color: green;"></i> Felicidades tu contraseña ha sido cambiada. La próxima vez que entres ingresa con tu nueva contraseña.</h2>
						

						
					</div>
						<!-- /.box-body -->
			</div>
					<!-- /.box -->

		</div>

	</div>
</div>
@endsection