@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Asignaturas</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
							<i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
							<div class="row">
								<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'indexS','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						</div>
					<div class="col-md-12">
						<form method="POST" action="{{ Route('subject_add') }}" role="form">
                           {{ csrf_field() }}
                           <div class="form-group col-md-12">
								<div class="col-md-12 text-center">
									<label for="Nombre" style="font-size: 1.5em">Nombre de la Asignatura</label>
								</div>
								<div class="col-md-8 col-md-offset-2">
									<input type="text" class="form-control" id="NombreSub" name="NombreSub" placeholder="Nueva asignatura"   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
								</div>
							</div>
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Guardar</button>
							</div>
                       </form>
					</div>
					</div>

					<h3 class="titleCenter" style="text-align: center;">Asignaturas Actuales</h3>
					<div class="table-responsive">
						<table class="table table-hover table-striped sortable" id="tableEmployees">
						<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Nombre</th>
								<th colspan="2" class="text-center">Opciones</th>
							</tr>
						</thead>
						<?php $a = 0; ?>
						@foreach($sub as $subs)
						<tbody>
							<?php $a = $subs->id; ?>
								<tr>
									<td class="text-center">{{$subs-> name}}</td>
									<td class="text-center">
										<button type="submit" title="Editar" class="btn btn-info" style="background-color: #1bb49a; border-color: #1bb49a;" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>">
											<span class="glyphicon glyphicon-edit" aria-hidden="true"> </span>
										</button>
										<!-- INICIA MODAL PARA EDITAR REGISTRO -->
										<div class="modal fade" id="confirm-edit<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<form action="{{ route('subject_update', ['subject' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('PUT') }}
													<div class="modal-body">
														<h4 class="modal-title">Nuevo nombre de la Asignatura</h4>
														<div class="col-md-8 col-md-offset-2">
															<input type="text" class="form-control" id="NombreSubEdit<?php echo $a; ?>" name="NombreSubEdit<?php echo $a; ?>" placeholder="Asignatura" value="{{$subs-> name}}" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
														</div>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														
															<button type="submit" title="Eliminar" style="background-color: black; border-color: black;" class="btn btn-info" data-toggle="modal" data-target="#confirm-edit<?php echo $a; ?>"><strong>Editar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA EDITAR REGISTRO -->
									</td>
									
									<td class="text-center">
										<button type="submit" title="Eliminar" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete<?php echo $a; ?>">
											<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
										</button>
										<!-- INICIA MODAL PARA ELIMINAR REGISTRO -->
										<div class="modal fade" id="confirm-delete<?php echo $a; ?>" tabindex="-1">
											<div class="modal-dialog">
												<div class="modal-content">
													<!-- Modal Header -->
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
											      	</div>
													<!-- Modal body -->
													<div class="modal-body">
														<h4 class="modal-title">¿Desea eliminar esta Asignatura?</h4>
													</div>
													<!-- Modal footer -->
													<div class="modal-footer">
														<form action="{{ route('subject_delete', ['subject' => $a]) }}" method="POST" id="form">
															{{ csrf_field() }}
															{{ method_field('DELETE') }}
															<button type="submit" title="Eliminar" class="btn btn-danger" style="background-color: black; border-color: black;" data-toggle="modal" data-target="#confirm-delete"><strong>Eliminar</strong>
															</button>
														</form>	
													</div>
												</div>
											</div>
										</div>
									<!-- TERMINA MODAL PARA ELIMINAR REGISTRO -->
									</td>
								</tr>
							</tbody>
						@endforeach
						</table>
						<center>{{$sub->links()}}</center>
					</div>


					</div>
					<!-- /.box -->
		

			</div>

		</div>
	</div>
</div>
		@endsection
		<script src="{{ asset('js/sorttable.js') }}"></script>