@extends('adminlte::layouts.app')
@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('personal_style')
@include('adminlte::training.PersonalSelect2')
@stop

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">
			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h2 class="box-title">Plan</h2>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					@include('adminlte::layouts.partials._errors')
					<form action="{{ Route('cathedra_update', ['cathedra' => $cathedra->id]) }}" method="POST" role="form" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{ method_field('PUT') }}
						<!-- <span style="font-size: 2em">Actualizar</span> -->
						<div class="col-md-12">
							<div class="panel panel-info">
								<table class="table table-hover table table-striped">
									<thead style="background: black; color: white;">
										<tr>
											<th class="text-center">Actualizar</th>
										</tr>
									</thead>
								</table>
								<br>
								<div class="panel-body">

										<div class="col-md-offset-1 col-md-10" style="top: 10px">
											<div class="col-md-12">
												<div class="text-center">
													<h3>Clase</h3>
												</div>

												<div class="panel-body">
													<div class="row col-md-12">
														<div>
																<span style="font-size: 1.5em">
																	Curso
																</span>

																<div class="form-group">
																	<div class="input-group col-md-offset-1 col-md-11">
																		<span class="input-group-addon" style="border: none; width: 1em">
																			<i class="fa fa-graduation-cap" aria-hidden="true" style="font-size: 2em; color: black">
																			</i>
																		</span>
																		<select name="subjects[]" class="form-control" id="subjects">
																			<option value="{{ $subjectDs -> id }}" selected="true" >
																				{{ $subjectDs->name }}
																			</option>
																			@foreach($subjects as $subject)
																			<option value="{{ $subject->id}}"
																				{{ (in_array($subject, old('subjects', []))) }} >{{ $subject->name }}
																			</option>
																			@endforeach
																		</select>
																	</div>
																</div>
															</div>
														<div>
															<div>
																<span style="font-size: 1.5em">
																	Maestro
																</span>
																<div class="form-group">
																	<div class="input-group col-md-offset-1 col-md-11">
																		<span class="input-group-addon" style="border: none; width: 1em;">
																			<i class="fa fa-user" aria-hidden="true" style="font-size: 2em; color: black"></i>
																		</span>
																		<select name="teacher[]"  class="form-control" id="teacher">
																			<option value="{{ $teacherDs -> id }}" selected="true" >
																				{{ $teacherDs->name }}
																			</option>
																			@foreach($teachers as $teacher)
																			<option value="{{ $teacher -> id }}"
																				{{ (in_array($teacher, old('teachers', []))) }} >{{ $teacher->name }}
																			</option>
																			@endforeach
																		</select>
																	</div>
																</div>
															</div>
														</div>

														<br>
														<br>

														<div class="col-md-12" style="bottom: 10px">
													<!-- <div class="form-group"> -->
														<div class="col-md-12 text-center">
															<label for="Pago_hora" style="font-size: 1.5em">Pago por hora</label>
														</div>
														<div class="col-md-8 col-md-offset-2">
															<input type="text" class="form-control" id="Pago_hora" name="Pago_hora" placeholder="00.0" value="{{ $cathedra->hour_payment or old('Pago_hora') }}" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px">
													</div>
													</div>


													<br><br><br><br>

													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary" style="" >Guardar</button>
													</div>
												</form>
											</div>
											<!-- /.box-body -->
										</div>


											<!-- </div> -->
										</div>
										<!-- /.box -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endsection
			@section('scripts')
			<script>
				$(".multiple").select2();
			</script>
			@endsection
