@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h2 class="titleCenter">Clases</h2>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
					<div class="row">
						<div class="col-md-12 col-sm-6 col-xs-6" style="text-align:right; margin-bottom: 20px;">
						{!!Form::open(['method' => 'GET','route'=>'indexC','class'=>'navbar-form pull-right','role'=>'search'])!!}
  						<div class="form-group">
    					<input type="text" class="form-control" placeholder="Buscar" name="search" id="search">
  						</div>
 	 					<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
						{!!Form::close()!!}
						</div>
						<div class="col-md-2 col-md-offset-10">
						<a href="{{ route('cathedra_create') }}" class="btn btn-success" style="background-color: black; border-color: black;"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span>Clase</a>
						</div>
					</div>
					<br>
						<div class="table-responsive">
						  <table class="table table-hover table-striped sortable" name= "tableEmployees" id="tableEmployees">
						    <thead style="background: black; color: white;"><tr>
						    	<th class="tableCourse" style="color: white;">Curso</th>
						    	<th class="tableCourse" style="color: white;">Maestro</th>
						    	<th class="tableCourse" style="color: white;">Pago por hora</th>
						    	<th class="tableCourse" style="color: white;" colspan="2">Opciones</th>
						    </tr></thead>
							<tbody>
								@foreach($cathedras as $cathedra)
								<tr>
									<td>{{ $cathedra->subject_name}}</td>
									<td>{{ $cathedra->name}}</td>
									<td>{{ $cathedra->hour_payment}}</td>
									<td>
										<a href="{{ Route('cathedra_edit', ['cathedra' => $cathedra->id]) }}" class="btn">
											<i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
										</a>
									</td>
									<td>
										<form action="{{ route('cathedra_delete', ['cathedra' => $cathedra->id]) }}" method="POST">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}
											<button type="submit" class="btn" style="background-color: transparent;">
												<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
											</button>
										</form>
									</td>
								</tr>
								@endforeach
							</tbody>


						  </table>
						</div>
						<center>{{$cathedras->links()}}</center>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>

		</div>
	</div>

@endsection
<script src="{{ asset('js/sorttable.js') }}"></script>