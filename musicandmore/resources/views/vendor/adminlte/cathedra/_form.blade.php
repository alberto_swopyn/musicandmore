<div class="panel panel-primary" style="border-color: #bce8f1;">
	<table class="table table-hover table table-striped">
		<thead style="background: black; color: white;">
							<tr>
								<th class="text-center">Nueva</th>
							</tr>
	    </thead>
	 </table>
	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a>
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a>
	</div>

	<div class="panel-body">

		<div class="col-md-offset-1 col-md-10" style="top: 10px">
			<div class="col-md-12">
				<div class="text-center">
					<h3>Crear clase</h3>
				</div>

				<div>
					<span style="font-size: 1.5em">
						Curso
					</span>

					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em">
								<i class="fa fa-graduation-cap" aria-hidden="true" style="font-size: 2em; color: black">
								</i>
							</span>
							<select name="subjects[]" class="form-control" id="subjects">
								@foreach($subjects as $subject)
								<option value="{{ $subject->id}}"
									{{ (in_array($subject, old('subjects', []))) }} >{{ $subject->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div>
					<span style="font-size: 1.5em">
						Maestro
					</span>
					<div class="form-group">
						<div class="input-group col-md-offset-1 col-md-11">
							<span class="input-group-addon" style="border: none; width: 1em;">
								<i class="fa fa-user" aria-hidden="true" style="font-size: 2em; color: black"></i>
							</span>
							<select name="teacher[]"  class="form-control" id="teacher">
								@foreach($teachers as $teacher)
								<option value="{{ $teacher->id}}"
									{{ (in_array($teacher, old('teachers', []))) }} >{{ $teacher->name }}
								</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="form-group col-md-12">

			<div class="col-md-12 text-center">
				<label for="Pago_hora" style="font-size: 1.5em">Pago por hora</label>
			</div>

			<div class="col-md-8 col-md-offset-2">
				<input type="text" class="form-control" id="Pago_hora" name="Pago_hora" placeholder="00.0" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required">
			</div>

		</div>
		<br>
		<br>

		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-primary" style="font-size: 1.5em">Guardar</button>
		</div>
	</div>
</div>

<style>

.select2 {
width:100%!important;
}

.select2-container--default .select2-selection--multiple .select2-selection__rendered li {
	background-color: white;
	color: black;
	font-weight: bold;
	border: none;
	font-size: 1.5em;
	margin: .5em;
}
span.select2-selection.select2-selection--multiple{border: 1px solid #f5216e;}
input:focus {
    outline: none !important;
    border-color: #f5216e !important;
    box-shadow: 0 0 10px #f5216e !important;
}

.input-group .select2-container {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0;
  display: table;
  table-layout: fixed;
}

</style>
