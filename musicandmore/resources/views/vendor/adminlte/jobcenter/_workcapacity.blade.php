<div class="form-horizontal panel-body">
	<div class="principal-container">
		<div class="col-md-8 col-md-offset-2" style="height: unset;overflow-x:auto;">
			<label>
				<table class="table table-striped" name="tablaPuestos" id="tablaPuestos" style="margin-bottom: 0px;">
					<thead>
						<tr>
							<th class="text-center"><b><p class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_puestojobcenter"></p></b>Puesto</th>
							<th class="text-center"><b><p class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" style="color: red; margin-bottom: 0;" id="error_cantidad"></p></b>Cantidad</th>
						</tr>
					</thead>
					<tbody class="text-left" id="puestostabla">
						<tr>
							<td>
								<div class="col-md-8">
									<select name="puestoJobCenter" id="puestoJobCenter" class="form-control">
										@foreach($jobtitles as $p)
										<option value="{{$p->id}}">{{$p->job_title_name}}</option>
										@endforeach
									</select>
								</div>
							</td>
							<td class="text-center">
								<input type="number" id="cantidadPuesto" name="cantidadPuesto" class="col-md-pull-6 text-center" min="1" max="100" >
							</td>
							<td class="text-center">
								<div>
									<button type="button" class="btn btn-warning form-control" style="background-color: #1bb49a; border-color: #1bb49a; max-width: 3em;" id="add_puesto">
										<span class="glyphicon glyphicon-plus-sign"></span>
									</button>
								</div>
							</td>
						</tr>
						@foreach ($work as $work)
						@foreach ($jobtitles as $jobTitles)
						@if ($work->jobtitle_capacity == $jobTitles->id_inc)
						<tr style="color: black" class="puestostabla">
							<td id="jobtitle_capacity" class="text-center">{{ $jobTitles->text }}</td>
							<td class="text-center">{{ $work->quantity }}</td>
							<td><button class="btn btn-sm btn-circle delete_Puesto" data-info="{{ $work->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
								<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button></td>
						</tr>
						@endif
						@endforeach
						@endforeach
					</tbody>
				</table>
			</label>
		</div>
		<div class="form-group col-xs-12 btn-lg" align="center">
			<br>
			<button type="button" class="btn btn-primary btn-lg" id="guardar_workcapacity" style="background-color: black; border-color: black; margin-right: 2%; width:12em; height:3em">Siguiente</button>
		</div>
	</div>
</div>