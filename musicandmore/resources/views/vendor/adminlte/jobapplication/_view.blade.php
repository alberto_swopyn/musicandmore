<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
</head>
<body>
	<style>
		.salto{
			page-break-after: always;
		}
		body{
			font-size: 12px;
		}
		th{
			font-size: 12px;  	
		}
		td{
			text-align: center;
			padding: .5em;
		}
		table{
			background-color: #f5f5f5;
		}
		.well.well-lg {
			/*min-height: 20px;*/
			padding: 19px;
			margin-bottom: 20px;
			border-radius: 4px;
			-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
			box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
		}
		.text-center{
			text-align: center;
		}
		.ancho{
			font-size: 10px; width: 80%; padding-left: 2em
		}
		.well.well-lg.personalData {
			margin-top: -3em;
		}
		.well.well-lg.title {
			margin-top: -2em;
		}
		.radio-label {
			display: inline-block;
		}
		.radio-input {
			display: inline-block;
			padding-left: 5em;
		}
		.radio-inputVive {
			display: inline-block;
			padding-left: 3em;
		}
		tbody tr:nth-child(odd) {
			background-color: #ccc;
		}
	</style>
	<div class="container-fluid" style="margin-top: 2em">
		<div class="row">
			<div class="col-md-8 col-md-offset-1 well well-lg" style="margin-right: 2em">
				<table width="100%" style="margin-top: -3em;" >
					<tr style="border: 1px">
						<td style="font-size: 10px; width: 70%; padding-left: 2em" align="center" colspan="2"><h2>Solicitud de empleo</h2>
							<h6 class="text-center">La información aquí proporcionada será tratada confidencialmente</h6>
						</td>
						<td style="text-align: center; width: 30%; height: 20%" >FOTO</td>
					</tr>
					<tr>
						<th style="font-size: 10px; width: 50%; padding-left: 2em">Fecha:	 </th>
						<th style="font-size: 10px; width: 50%; padding-left: 2em">Puesto solicitado: </th> 
						<th style="font-size: 10px; width: 50%; padding-left: 2em">Centro de trabajo solicitado: </th>
					</tr>
					<tr>
						<td>{{ $work_g->first_day }}</td>
						<td>{{ $work_g->job_title_profile_id }}</td>
						<td>{{ $work_g->profile_job_centers_id }}</td>
					</tr>
				</div>
			</div>
		</table>

		<!-- General Personal Data -->
		<div class="row text-center">
			<h3 class="text-center col-md-12">Datos Personales</h3>
			<br>
			<div class="row col-md-10 col-md-offset-1">
				<table class="well well-lg title" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 45%; padding-left: 2em;">Apellido(s):</th>
						<th style="font-size: 10px; width: 45%; padding-left: 2em;">Nombre(s): </th>
						<th style="font-size: 10px; width: 10%; padding-left: 2em;">Edad: </th>
					</tr>
					<tr>
						<td>{{ $temp->last_name }}</td>
						<td>{{ $temp->first_name}}</td>
						<td>{{ $temp->age}}</td>
					</tr>
				</table>

				<table class="well well-lg personalData" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 30%; padding-left: 2em">Calle:	 </th>
						<th style="font-size: 10px; width: 10%; padding-left: 2em">Número ext: </th>
						<th style="font-size: 10px; width: 20%; padding-left: 2em">Número int: </th> 
						<th style="font-size: 10px; width: 40%; padding-left: 2em">Colonia o Fraccionamiento: </th>
					</tr>
					<tr>
						<td>{{ $dir->street }}</td>
						<td>{{ $dir->ext_number }}</td>
						<td>{{ $dir->int_number }}</td>
						<td>{{ $dir->fraccionamiento }}</td>
					</tr>
				</table>
				<table class="well well-lg personalData" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 45%; padding-left: 2em">Ciudad:	 </th>
						<th style="font-size: 10px; width: 40%; padding-left: 2em">Estado: </th>
						<th style="font-size: 10px; width: 15%; padding-left: 2em">Código Postal:	 </th>
					</tr>
					<tr>
						<td>{{ $dir->city }}</td>
						<td>{{ $dir->state }}</td>
						<td>{{ $dir->zip_code }}</td>
					</tr>
				</table>

				<table class="well well-lg personalData" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 40%; padding-left: 2em">Lugar de Nacimiento:	 </th>
						<th style="font-size: 10px; width: 30%; padding-left: 2em">Fecha de Nacimiento: </th> 
						<th style="font-size: 10px; width: 20%; padding-left: 2em">Género: </th>
					</tr>
					<tr>
						<td>{{ $temp->birth_place }}</td>
						<td>{{ $temp->birthday }}</td>
						<td>{{ $temp->gender }}</td>
					</tr>
				</table>

				<table class="well well-lg personalData" style="width:100%;">
					<tr>
						<th class="ancho">Vive con:	 </th>
						<th style="font-size: 10px; width: 10%; text-align: center;">Estatura: </th>
						<th style="font-size: 10px; width: 10%; text-align: center;">Peso: </th> 
					</tr>
					<tr>
						<td style="font-size: 10px; padding-bottom: 1.5em;">
							<input type="radio" class="radio-inputVive" style="width:1px;height:1px"
							@if ('Padres' == $temp->live_with) checked='checked' @endif>
							<label class="radio-label">Padres</label>
							<input type="radio" class="radio-inputVive" style="width:1px;height:1px"
							@if ('Familia' == $temp->live_with) checked='checked' @endif>
							<label class="radio-label">Familia</label>
							<input type="radio" class="radio-inputVive" style="width:1px;height:1px"
							@if ('Pareja' == $temp->live_with) checked='checked' @endif>
							<label class="radio-label">Pareja</label>
							<input type="radio" class="radio-inputVive" style="width:1px;height:1px"
							@if ('Amigos' == $temp->live_with) checked='checked' @endif>
							<label class="radio-label">Amigos</label>
							<input type="radio" class="radio-inputVive" style="width:1px;height:1px"
							@if ('Solo' == $temp->live_with) checked='checked' @endif>
							<label class="radio-label">Solo</label>
						</td>
						<td>{{ $temp->height }}</td>
						<td>{{ $temp->weight }}</td>
					</tr>
				</table>

				<table class="well well-lg personalData" style="width:100%;  padding-bottom: 1.5em;">
					<tr>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Personas que dependen de usted:	 </th>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Estado civil: </th>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Teléfono: </th>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Celular: </th> 
					</tr>
					<tr>
						<td>{{ $temp->dependent }}</td>
						<td>{{ $temp->civil_status }}</td>
						<td>{{ $temp->phone_number }}</td>
						<td>{{ $temp->cellphone }}</td>
					</tr>
				</table>

				<table class="well well-lg personalData" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 33%; padding-left: 2em">Clave única de registro de la población (CURP):	 </th>
						<th style="font-size: 10px; width: 33%; padding-left: 2em">Reg. Fed. de Contribuyentes No.: </th> 
						<th style="font-size: 10px; width: 33%; padding-left: 2em">Afiliación al Seguro Social No.: </th>
					</tr>
					<tr>
						<td>{{ $temp->curp }}</td>
						<td>{{ $temp->rfc }}</td>
						<td>{{ $temp->nss }}</td>
					</tr>
				</table>
			</div>
		</div>


		<div class="salto"></div>

		<div class="row text-center">
			<h3 class="text-center col-md-12">Información Familiar</h3>
			<br>
			<div class="row col-md-10 col-md-offset-1">
				<table class="well well-lg" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Nombre completo:	 </th>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Parentesto: </th> 
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Ocupación: </th>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Número telefónico: </th>
					</tr>
					@foreach ($fam as $fam)
					<tr>
						<td>{{ $fam->name }}</td>
						<td>{{ $fam->relationship }}</td>
						<td>{{ $fam->occupation }}</td>
						<td>{{ $fam->phone_number }}</td>
					</tr>
					@endforeach
				</table>

			</div>
		</div>

		<div class="row text-center">
			<h3 class="text-center col-md-12">Formación Académica</h3>
			<br>
			<div class="row col-md-10 col-md-offset-1">
				<table class="well well-lg" style="width:100%; height: 10%">
					<tr>
						<th style="font-size: 10px; width: 20%; padding-left: 2em">Grado académico: </th> 
						<th style="font-size: 10px; width: 20%; padding-left: 2em">Área educativa: </th>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Nombre de la escuela: </th>
						<th style="font-size: 10px; width: 35%; padding-left: 2em">Dirección de la escuela: </th>
						<th style="font-size: 10px; width: 10%; padding-left: 2em">Concluyó: </th>
					</tr>
					@foreach ($schools as $schools)
					<tr>
						<td>{{ $schools->degree_name }}</td>
						<td>{{ $schools->education_area }}</td>
						<td>{{ $schools->school_name }}</td>
						<td>{{ $schools->school_address }}</td>
						<td>{{ $schools->finish_studies }}</td>
					</tr>
					@endforeach
				</table>

				<table class="well well-lg personalData" style="width:100%;margin-top: -4em;">
					<tr>
						<p style="font-size: 12px; text-align: left; margin-bottom: 2em; margin-top: -1em;">Estudios que esta efectuando en la actualidad</p>
						<th style="font-size: 10px; width: 33%; padding-left: 2em">Grado de estudio: </th>
						<th style="font-size: 10px; width: 33%; padding-left: 2em">Periodo inicial: </th>
						<th style="font-size: 10px; width: 33%; padding-left: 2em">Periodo final: </th>
					</tr>

					<tr>
						<td>{{ $schools->school_address }}</td>
						<td>{{ $schools->school_address }}</td>
						<td>{{ $schools->school_address }}</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="row text-center">
			<h3 class="text-center col-md-12" style="margin-top: -4em;">Conocimientos</h3>
			<br>
			<div class="row col-md-10 col-md-offset-1">
				<table class="well well-lg" style="width:100%; margin-top: -4em;">
					<tr>
						<th style="font-size: 10px; padding-left: 2em"  colspan="12" >Idiomas que dominas:	 	(Porcentaje %)</th>
					</tr>
					<tr>
						@foreach($lan as $lan)
						<td>{{ $lan->language }} , &nbsp; {{ $lan->level }}%</td>
						@endforeach
					</tr>
				</table>
				<table class="well well-lg" style="width:100%; margin-top: -4em;">
					<tr>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Máquinas de oficina o taller que sepa manejar: </th>
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Software que conoce: </th> 
						<th style="font-size: 10px; width: 25%; padding-left: 2em">Funciones de oficina que domina: </th> 
					</tr>
					<tr>
						<td>{{ $machines->machines }}</td>
						<td>{{ $software->software }}</td>
						<td>{{ $office->office }}</td>
					</tr>
				</table>
				<table class="well well-lg" style="width:100%; margin-top: -4em;">
					<tr>
						<th colspan="2" style="text-align: center;font-size: 10px; width: 25%; padding-left: 2em">Otros trabajos o funciones que domina: </th>
					</tr>
					<tr>
						<td style="text-align: left; padding-left: 1em;">{{ $others->others }}</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="salto"></div>

		<div class="row text-center">
			<h3 class="text-center col-md-12">Experiencia laboral</h3>
			<br>
			<div class="row col-md-10 col-md-offset-1">
				<table class="well well-lg" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 15%; padding-left: 2em; text-align: center;">Concepto: </th>
						<th style="font-size: 10px; width: 20%; padding-left: 2em; text-align: center;">Empleo actual o último: </th>
						<th style="font-size: 10px; width: 20%; padding-left: 2em; text-align: center;">Empleo anterior: </th>
						<th style="font-size: 10px; width: 20%; padding-left: 2em; text-align: center;">Empleo anterior: </th>
						<th style="font-size: 10px; width: 20%; padding-left: 2em; text-align: center;">Empleo anterior: </th>
					</tr>

					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Periodo inicial</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Periodo final</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Compañía</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Dirección</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Número telefónico</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Puesto</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Ingreso inicial</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Ingreso final</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Motivo separación</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Experiencia</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Área de experiencia</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Jefe directo</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Puesto jefe directo</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Correo</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">Número telefónico</td>
						<td id="lastJob" >mundo</td>
						<td id="firstJob" >esta</td>
						<td id="secondJob" >es</td>
						<td id="thirdJob" >una tabla</td>
					</tr>
					<tr>
						<td style="font-weight: bold; text-align: left; padding-left: 1em">¿Referencias?</td>
						<td id="firstJob" >
							<input type="radio" style="vertical-align: middle;"></input>
							<label style="text-align: center;">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" style="vertical-align: middle;"></input> 
							<label>No</label>
						</td>
						<td id="secondJob" >
							<input type="radio" style="vertical-align: middle;"></input>
							<label style="text-align: center;">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" style="vertical-align: middle;"></input> 
							<label>No</label>
						</td>
						<td id="thirdJob" >
							<input type="radio" style="vertical-align: middle;"></input>
							<label style="text-align: center;">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" style="vertical-align: middle;"></input> 
							<label>No</label>
						</td>
						<td id="lastJob" >
							<input type="radio" style="vertical-align: middle;"></input>
							<label style="text-align: center;">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" style="vertical-align: middle;"></input> 
							<label>No</label>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="salto"></div>

		<div class="row text-center">
			<h3 class="text-center col-md-12">Referencias</h3>
			<br>
			<div class="row col-md-10 col-md-offset-1">
				<table class="well well-lg text-center" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 16%; text-align: center;">Nombre: </th>
						<th style="font-size: 10px; width: 16%; text-align: center;">Dirección: </th> 
						<th style="font-size: 10px; width: 16%; text-align: center;">Relación: </th>
						<th style="font-size: 10px; width: 16%; text-align: center;">Ocupación: </th>
						<th style="font-size: 10px; width: 16%; text-align: center;">Teléfono: </th>
						<th style="font-size: 10px; width: 16%; text-align: center;">Tiempo de conocerlo: </th>
					</tr>

					@foreach ($reference as $reference)
					<tr>
						<td style="text-align: center; padding-left: 0;">{{ $reference->name }}</td>
						<td style="text-align: center; padding-left: 0;">{{ $reference->address }}</td>
						<td style="text-align: center; padding-left: 0;">{{ $reference->relationship }}</td>
						<td style="text-align: center; padding-left: 0;">{{ $reference->occupation }}</td>
						<td style="text-align: center; padding-left: 0;">{{ $reference->phone_number }}</td>
						<td style="text-align: center; padding-left: 0;">{{ $reference->meet_time }}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>

		<div class="row text-center">
			<h3 class="text-center col-md-12">Información económica</h3>
			<br>
			<div class="row col-md-8 col-md-offset-2">
				<table class="well well-lg" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 50%; text-align: center;">¿Tiene algún ingreso económico extra?</th>
						<th style="font-size: 10px; width: 50%; text-align: center;">Monto total del ingreso extra</th> 
					</tr>
					<tr>
						<td>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">No</label>
						</td>
						<td>$ {{ $economic->other_ammount_income }}</td>
					</tr>
				</table>
				<table class="well well-lg" style="width:100%; margin-top: -2em;">
					<tr>
						<th style="font-size: 10px; width: 50%; padding-left: 2em; text-align: center;">¿Su pareja tiene empleo?</th>
						<th style="font-size: 10px; width: 50%; padding-left: 2em; text-align: center;">Domicilio trabajo</th> 
					</tr>
					<tr>
						<td>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">No</label>
						</td>
						<td>{{ $economic->couple_work_place }}</td>
					</tr>
				</table>
				<table class="well well-lg" style="width:100%; margin-top: -2em;">
					<tr>
						<th style="font-size: 10px; width: 25%; padding-left: 2em; text-align: center;">¿Cuenta con casa propia?</th>
						<th style="font-size: 10px; width: 25%; padding-left: 2em; text-align: center;">¿Renta casa?</th> 
					</tr>
					<tr>
						<td>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">No</label>
						</td>
						<td>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">No</label>
						</td>
					</tr>
				</table>
				<table class="well well-lg" style="width:100%; margin-top: -2em;">
					<tr>
						<th style="font-size: 10px; width: 50%; padding-left: 2em; text-align: center;">¿Cuenta con automóvil propio?</th>
						<th style="font-size: 10px; width: 50%; padding-left: 2em; text-align: center;">¿Tiene deudas?</th> 
					</tr>
					<tr>
						<td>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">No</label>
						</td>
						<td>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">No</label>
						</td>
					</tr>
				</table>
				<table class="well well-lg" style="width:100%; margin-top: -2em;">
					<tr>
						<th style="font-size: 10px; width: 50%; padding-left: 2em; text-align: center;">En caso de tener deudas, ¿Cuánto abona mensualmente?</th>
						<th style="font-size: 10px; width: 50%; padding-left: 2em; text-align: center;">¿A cúanto ascienden sus gastos mensualmente?</th> 
					</tr>
					<tr>
						<td>$ {{ $economic->pay_month }}</td>
						<td>$ {{ $economic->monthly_expenses }}</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="salto"></div>

		<div class="row text-center">
			<h3 class="text-center col-md-12">Otras</h3>
			<br>
			<div class="row col-md-8 col-md-offset-2">
				<table class="well well-lg" style="width:100%;">
					<tr>
						<th style="font-size: 10px; width: 50%; text-align: center;">¿Cómo se enteró del trabajo?</th>
						<th style="font-size: 10px; width: 50%; text-align: center;">¿Tiene familiares dentro de la empresa?</th> 
					</tr>
					<tr>
						<td>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">No</label>
						</td>
						<td>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
							<label class="radio-label">No</label></td>
						</tr>
					</table>
					<table class="well well-lg" style="width:100%; margin-top: -2em;">
						<tr>
							<th style="font-size: 10px; width: 50%; padding-left: 2em; text-align: center;">¿Alguna vez ha estado asegurado?</th>
							<th style="font-size: 10px; width: 50%; padding-left: 2em; text-align: center;">¿Cuándo puede presentarse a trabajar?</th> 
						</tr>
						<tr>
							<td>
								<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
								<label class="radio-label">Sí&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
								<input type="radio" class="radio-input" style="width:1px;height:1px;"/> 
								<label class="radio-label">No</label>
							</td>
							<td>01-01-2000</td>
						</tr>
					</table>
				</div>
			</div>
			<br>
			<br>
		</body>
		</html>