<div class="col-xs-12">
  <ul class="nav nav-tabs nav-justified" id="steps">

    <li class="active Pasos_Tracking">
      <a data-toggle="tab" href="#section-jobapplication" class="enlace_Pasos">
        <h3 class="stepTitle">Paso 1</h3>
        <h5 class="borra">Solicitud de maestro</h5>
      </a>
    </li>

    <li class="disabled Pasos_Tracking">
      <a id="Step2" href="#section-pruebas" class="enlace_Pasos">
        <h3 class="stepTitle">Paso 2</h3>
        <h5 class="borra">Pruebas psicométricas</h5>
      </a>
    </li>

    <li class="disabled Pasos_Tracking">
      <a id="Step3" href="#section-interview" class="enlace_Pasos">
        <h3 class="stepTitle">Paso 3</h3>
        <h5 class="borra">Entrevista laboral</h5>
      </a>
    </li>

    <li class="disabled Pasos_Tracking">
      <a id="Step4" href="#section-documents" class="enlace_Pasos">
        <h3 class="stepTitle">Paso 4</h3>
        <h5 class="borra">Documentos personales</h5>
      </a>
    </li>

    <li class="disabled Pasos_Tracking">
      <a id="Step5" href="#section-contract" class="enlace_Pasos">
        <h3 class="stepTitle">Paso 5</h3>
        <h5 class="borra">Contrato</h5>
      </a>
    </li>
  </ul>

  <div class="tab-content">
    <div id="section-jobapplication" class="tab-pane fade in active">
      @include('adminlte::jobapplication/form_sections')
    </div>
    <div id="section-pruebas" class="tab-pane fade" style="margin-top: 3em">
     @include('adminlte::psychometric/intro')
   </div>
   <div id="section-interview" class="tab-pane fade">
    @include('adminlte::scheduledinterview.user_view')
  </div>
  <div id="section-documents" class="tab-pane fade">
    @include('adminlte::jobapplication._form_documents')
  </div>
  <div id="section-contract" class="tab-pane fade">
    @include('adminlte::contracttype/view_contract')
  </div>
</div>
</div>