<div class="wizard text-center">
	<div class="wizard-inner row">
		<ul class="nav nav-tabs" role="tablist" style="left: 4%; margin: 1em; border-bottom: 0">

			<li role="presentation" class="active">
				<a href="#general_personal_datas" data-toggle="tab" class="tabs_application" aria-controls="general_personal_datas" role="tab" title="Datos Personales" id="tab_datos_personales">
					<span class="round-tab">
						<i class="glyphicon glyphicon-user" style="font-size: 1.6em;"></i>
					</span>
				</a>
			</li>
			<li role="presentation" class="disabled">
				<a href="#personal_addresses" data-toggle="tab" class="tabs_application" aria-controls="personal_addresses" role="tab" title="Domicilio" id="tab_domicilio">
					<span class="round-tab">
						<i class="fa fa-home" style="font-size: 1.6em;"></i>
					</span>
				</a>
			</li>
			<li role="presentation" class="disabled">
				<a href="#family_datas" data-toggle="tab" class="tabs_application" aria-controls="family_datas" role="tab" title="Informaci贸n Familiar" id="tab_informacionpersonal">
					<span class="round-tab">
						<i class="fa fa-heart" style="font-size: 1.6em;"></i>
					</span>
				</a>
			</li>
			<li role="presentation" class="disabled">
				<a href="#scholarship_datas" data-toggle="tab" class="tabs_application" aria-controls="scholarship_datas" role="tab" title="Informaci贸n Escolar" id="tab_informacionescolar">
					<span class="round-tab">
						<i class="fa fa-graduation-cap" style="font-size: 1.6em;"></i>
					</span>
				</a>
			</li>
			<li role="presentation" class="disabled">
				<a href="#general_knowledge" data-toggle="tab" class="tabs_application" aria-controls="general_knowledge" role="tab" title="Conocimientos" id="tab_conocimientos">
					<span class="round-tab">
						<i class="fa fa-wrench" style="font-size: 1.6em;"></i>
					</span>
				</a>
			</li>
			<li role="presentation" class="disabled">
				<a href="#work_history" data-toggle="tab" class="tabs_application" aria-controls="work_history" role="tab" title="Historial de Trabajos" id="tab_trabajos">
					<span class="round-tab">
						<i class="fa fa-briefcase" style="font-size: 1.6em;"></i>
					</span>
				</a>
			</li>
			<li role="presentation" class="disabled">
				<a href="#personal_references" data-toggle="tab" class="tabs_application" aria-controls="personal_references" role="tab" title="Referencias" id="tab_referenciaspersonales">
					<span class="round-tab">
						<i class="fa fa-users" style="font-size: 1.6em;"></i>
					</span>
				</a>
			</li>
			<li role="presentation" class="disabled">
				<a href="#economic_datas" data-toggle="tab" class="tabs_application" aria-controls="economic_datas" role="tab" title="Informaci贸n Econ贸mica" id="tab_infoeconomica">
					<span class="round-tab">
						<i class="fa fa-usd" style="font-size: 1.6em;"></i>
					</span>
				</a>
			</li>
			<li role="presentation" class="disabled">
				<a href="#work_general_datas" data-toggle="tab" class="tabs_application" aria-controls="work_general_datas" role="tab" title="Otras" id="tab_otros">
					<span class="round-tab">
						<i class="fa fa-plus" style="font-size: 1.6em;"></i>
					</span>
				</a>
			</li>
		</ul>
	</div>
	<div class="tab-content">
		@include('adminlte::jobapplication._form')
	</div>
</div>