@section('personal_style')
@include('adminlte::tasks.PersonalSelect2')
@stop

<div class="row tab-pane fade" id="work_general_datas">

	<h3 class="titleJob text-center">OTRAS</h3>
	<h4 class="subtitleJob text-center">Ayúdanos con estos datos adicionales</h4>
	
	<div class="principal-container">
		<div class="col-lg-6">
			<label>¿Cómo se enteró del trabajo? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_conocimientoTrabajo"></span>
				<input type="text" class="form-control text-center" id="ConocimientoTrabajo" name="ConocimientoTrabajo" placeholder="Periódico, T.V., Radio" maxlength="50" required value="{{ $work_g->knowledge_employment or old('ConocimientoTrabajo') }}">
			</label>
		</div>

		
		<div class="col-lg-6">
			<label>¿Tienes familiares dentro de la empresa? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_familiaEmpresa"></span>
				<select id="FamiliaEmpresa" name="FamiliaEmpresa" class="form-control" data-live-search="true">
					<option value="" > Seleccione...</option>
					<option value="Si" @if ('Si' === $work_g->family) selected='selected' @endif>Si</option>
					<option value="No" @if ('No' === $work_g->family) selected='selected' @endif>No</option>
				</select>
			</label>
		</div>

		
		<div class="col-lg-6">
			<label>¿Alguna vez ha estado asegurado? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_seguro"></span>
				<select id="Seguro" name="Seguro" class="form-control" data-live-search="true">
					<option value=""> Seleccione...</option>
					<option value="Si"@if ('Si' === $work_g->enssurance) selected='selected' @endif>Si</option>
					<option value="No"@if ('No' === $work_g->enssurance) selected='selected' @endif>No</option>
				</select>
			</label>
		</div>

		
		<div class="col-lg-6">
			<label>¿Cuándo puede presentarse a trabajar? <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_primerDia"></span>
				<input type="date" class="form-control" id="PrimerDia" name="PrimerDia" required value="{{ $work_g->first_day or old('PrimerDia') }}">
			</label>
		</div>
<!-- dvsdc -->
		<div class="col-lg-6">
			<label>Especialista en:<span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_puesto"></span>
				<select id="Puesto_workgeneral" name="Puesto_workgeneral" class="form-control" data-live-search="true">
					<option value=""> Seleccione...</option>
					@foreach($subjects as $subject)
					<option value="{{ $subject-> id}}" @if ($subject->id == $work_g->subject_id) selected='selected' @endif>{{ $subject->name }}</option>
					@endforeach
				</select>
			</label>
		</div>
<!-- dvsdc -->
		<div class="col-lg-6">
			<label>Centro de trabajo <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_jobcenter"></span>
				<select id="Jobcenter_workgeneral" name="Jobcenter_workgeneral" class="form-control" data-live-search="true">
					<option value=""> Seleccione...</option>
					@foreach ($jobcenter as $j)
					<option value="{{ $j->id }}" @if ($j->id == $work_g->profile_job_centers_id) selected='selected' @endif>{{ $j->name }}</option>
					@endforeach
				</select>
			</label>
		</div>
	</div>
	<br>

	<div class="row" align="center">
		<button type="button" class="btn-lg btn-success" id="guarda_workgeneral" style="margin-top: 1em;"><span class="glyphicon glyphicon-ok"></span> Finalizar Solicitud</button>
	</div>
</div>
