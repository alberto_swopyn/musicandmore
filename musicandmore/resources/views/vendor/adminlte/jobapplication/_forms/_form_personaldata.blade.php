<div class="row tab-pane fade" id="general_personal_datas">
	<h3 class="titleJob text-center " style="text-align: center;">Datos Personales</h3>
	<h4 class="subtitleJob text-center">Llena tus datos generales</h4>
	<div class="principal-container">
		<div class="col-lg-6" style="height: 15em; vertical-align: middle;">
			<label style="border-bottom: 1px solid #E4E4E4;">
				Apellidos
				<input type="text" class="form-control hola" id="Apellidos" name="Apellidos" placeholder="Apellidos"  value="{{ $info->last_name or old('Apellidos') }}">
			</label>
			<label>
				Nombre(s) <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_primernombre"></span>
				<input type="text" class="form-control" id="PrimerNombre" name="PrimerNombre" placeholder="Nombre(s)" value="{{ $info->first_name or old('PrimerNombre') }}">
			</label>
		</div>
		<div class="col-lg-6" style="height: 15em">
			@if ($info->picture != null)
				<img src="{{ route('showPicture', ['id' => $info->id]) }}" alt="" width="120em" style="padding-top: 1em">
			@else
			<form action="{{ route('add_picture') }}" method="POST" role="form" enctype="multipart/form-data" class="dropzone"  id="my-dropzone" name="file">
				{{ csrf_field() }}
				<div class="dz-message" data-dz-message>
					<i class="glyphicon glyphicon-cloud-upload" style="font-size:30px"></i>
					<br/>
					<span>Arrastra y suelta tus archivos aquí.</span>
				</div>
			</form>
			@endif
		</div>
		<div class="col-lg-4">
			<label>
				Genéro <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_genero"></span>
				<div class="form-group">
					<div class="col-xs-6">
						<span class="radio-inline ok">
							<input type="radio" name="Genero" id="Genero" value="Masculino" @if ('Masculino' == $info->gender) checked='checked' @endif>Masculino
						</span>
					</div>
					<div class="col-xs-6">
						<span class="radio-inline ok">
							<input type="radio" name="Genero" id="Genero" value="Femenino" @if ('Femenino' == $info->gender) checked='checked' @endif>Femenino  
						</span>
					</div>
				</div>
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Fecha de nacimiento <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_fechanacimiento"></span>
				<input type="date" class="form-control" name="Nacimiento" id="Nacimiento" min="1950-01-01" max="2000-12-31"  value="{{ $info->birthday or old('Nacimiento') }}">
			</label>
		</div>
		<div class="col-lg-4">
			<label>
				Lugar Nacimiento <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_lugarnacimiento"></span>
				<input type="text" class="form-control" id="LugarNacimiento" name="LugarNacimiento" placeholder="Ej. Ciudad de México"  value="{{ $info->birth_place or old('LugarNacimiento') }}" style="text-transform: capitalize;">
			</label>
		</div>
		<div class="col-lg-2 col-xs-6">
			<label>
				Peso <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_peso"></span>
				<center>
					<input type="number" class="form-control" id="Peso" name="Peso" placeholder="72" min="50" max="150" step="0.1"  value="{{ $info->weight or old('Peso') }}">
				</center>
			</label>
		</div>
		<div class="col-lg-2 col-xs-6">
			<label>
				Altura <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_estatura"></span>
				<center>	
					<input type="number" class="form-control" id="Estatura" name="Estatura" placeholder="1.70" min="1.20" max="3" step="0.1"  value="{{ $info->height or old('Estatura') }}">
				</center>
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				Número Teléfonico <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_telefonojob"></span>
				<input type="text" class="form-control" id="Telefono_job" name="Telefono_job" placeholder="10 dígitos" maxlength="10" value="{{ $info->phone_number or old('Telefono_job') }}"/>
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				Número de Celular <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_celular"></span>
				<input type="text" class="form-control" id="Celular" name="Celular" placeholder="10 dígitos" maxlength="10"  value="{{ $info->cellphone or old('Celular') }}"/>
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				Estado Civil <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_estadocivil"></span>
				<select id="EstadoCivil" name="EstadoCivil" class="form-control" data-live-search="true">
					<option value="">Selecciona una opción</option>
					<option value="Casado" @if ('Casado' == $info->civil_status) selected='selected' @endif >Casado</option>
					<option value="Soltero" @if ('Soltero' == $info->civil_status) selected='selected' @endif>Soltero</option>
					<option value="Unión Libre" @if ('Unión Libre' === $info->civil_status) selected='selected' @endif>Unión Libre</option>
					<option value="Divorciado" @if ('Divorciado' == $info->civil_status) selected='selected' @endif>Divorciado</option>
					<option value="Viudo" @if ('Viudo' == $info->civil_status) selected='selected' @endif>Viudo</option>
				</select>
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				Vive con <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_vivecon"></span>
				<select id="ViveCon" name="ViveCon" class="form-control" data-live-search="true">
					<option value="">Selecciona una opción</option>
					<option value="Padres"@if ('Padres' === $info->live_with) selected='selected' @endif>Padres</option>
					<option value="Familia"@if ('Familia' === $info->live_with) selected='selected' @endif>Familia</option>
					<option value="Pareja"@if ('Pareja' === $info->live_with) selected='selected' @endif>Pareja</option>
					<option value="Amigos"@if ('Amigos' === $info->live_with) selected='selected' @endif>Amigos</option>
					<option value="Solo"@if ('Solo' === $info->live_with) selected='selected' @endif>Solo</option>
				</select>
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				Dependientes <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_dependientes"></span>
				<select aria-required="true" name="Dependientes" id="Dependientes" class="form-control" data-live-search="true"  value="{{ old('Dependientes') }}">
					<option value="">Selecciona una opción</option>
					<option value="Padres" @if ('Padres' === $info->dependent) selected='selected' @endif>Padres</option>
					<option value="Familia" @if ('Familia' === $info->dependent) selected='selected' @endif>Familia</option>
					<option value="Hijos" @if ('Hijos' === $info->dependent) selected='selected' @endif>Hijos</option>
					<option value="Nadie" @if ('Nadie' === $info->dependent) selected='selected' @endif>Nadie</option>
				</select>
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				CURP <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_curp"></span>
				<input type="text" id="Curp" name="Curp" class="form-control" placeholder="CURP" maxlength="18"  value="{{ $info->curp or old('Curp') }}" style="text-transform: uppercase;">
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				RFC <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_rfc"></span>
				<input type="text" id="Rfc" name="Rfc" class="form-control" placeholder="RFC" maxlength="13"  value="{{ $info->rfc or old('Rfc') }}" style="text-transform: uppercase;">
			</label>
		</div>
		<div class="col-lg-4 col-xs-12">
			<label>
				Numero de Seguro Social <span class="text-center text-danger hidden error" style="color: red; margin-bottom: 0;" id="error_nss"></span>
				<input type="text" id="Nss" name="Nss" class="form-control" placeholder="NSS" maxlength="11"  value="{{ $info->nss or old('Nss') }}" style="text-transform: uppercase;">
			</label>
		</div>
	</div>
	<div class="col-xs-12 text-right">
		<br>
		<button type="button" class="btn btn-primary btn-lg next-step button_job" style="background-color: black; border-color: black" id="guarda_personaldata">Siguiente</button>
	</div>
</div>