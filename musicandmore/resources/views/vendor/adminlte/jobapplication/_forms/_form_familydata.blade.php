<div class="row tab-pane fade" id="family_datas">
	<h3 class="titleJob text-center">INFORMACIÓN FAMILIAR</h3>
	<h4 class="subtitleJob text-center">Llena un formulario por cada familiar que vive contigo</h4>
	<div class="principal-container col-md-8 col-md-offset-2">
		<div class="col-lg-8">
			<label>
				Nombre completo <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_nombrefamilia"></span>
				<input type="text" id="NombreFamiliar" name="NombreFamiliar" class="form-control" placeholder="Nombre completo del familiar" value="{{ old('NombreFamiliar') }}">
			</label>
		</div>
		<div class="col-lg-8">
			<label>
				Parentesco <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_parentesco"></span>
				<input type="text" class="form-control" id="Parentesco" name="Parentesco" value="{{ old('Parentesco') }}" placeholder="Cónyuge, Padre, Madre, etc. ">
			</label>
		</div>
		<div class="col-lg-8">
			<label>
				Ocupación <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_ocupacion"></span>
				<input type="text" id="Ocupacion" name="Ocupacion" class="form-control" value="{{ old('Ocupacion') }}" placeholder="">
			</label>
		</div>
		<div class="col-lg-8">
			<label>
				Vive con usted <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_viveconfamilia"></span>
				<div class="form-group">
					<div class="col-xs-6">
						<span class="radio-inline ok">
							<input type="radio" name="ViveConUsted" id="ViveConUsted" value="0" value="{{ old('ViveConUsted') }}">Si
						</span>
					</div>
					<div class="col-xs-6">
						<span class="radio-inline ok">
							<input type="radio" name="ViveConUsted" id="ViveConUsted" value="0" value="{{ old('ViveConUsted') }}">No  
						</span>
					</div>
				</div>
			</label>
		</div>
		<div class="col-lg-8">
			<label>
				Número Telefónico <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_numerofamilia"></span>
				<input type="text" id="NumeroFamiliar" name="NumeroFamiliar" class="form-control" placeholder="10 digitos" maxlength="10">
			</label>
		</div>
	</div>
	<div class="row	text-center">
		<div class="col-xs-12">
			@if (isset($fam))
			<button id="fam_visible" style="background: transparent; border: 0;" value="1">Mostrar Tabla de Familiares</button>
			@endif
			<div class="row col-md-10 col-md-offset-1 table-responsive hidden" id="div_table_fam">
				<table class="table table-striped text-center">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Parentesco</th>
							<th>Ocupación</th>
							<th>Num. Tel.</th>
						</tr>
					</thead>
					<tbody id="family_table" name="family_table">
						@if (isset($fam))
						@foreach($fam as $f)
						<tr id="Familiar{{ $f->id }}">
							<td>{{ $f->name }}</td>
							<td>{{ $f->relationship }}</td>
							<td>{{ $f->occupation }}</td>
							<td>{{ $f->phone_number }}</td>
							<td><button class="btn btn-sm btn-circle delete_fam" data-info="{{ $f->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
								<i class="fa fa-trash " aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button></td>
						</tr>
						@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-xs-12 text-center">
		<br>
		<button type="button" class="btn btn-danger" style="background-color: black; border-color: black;" id="guarda_familydata" style="width:8em; height:3em">Guardar</button>
	</div>
	<div class="col-xs-12 text-right">
		<br>
		<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black;" id="next-familyinformation">Siguiente</button>
	</div>
</div>
