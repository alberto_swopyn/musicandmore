<div class="row tab-pane fade" id="scholarship_datas">	
	<h3 class="titleJob text-center">FORMACIÓN ACADÉMICA</h3>
	<h4 class="subtitleJob text-center">Compártenos tus últimos dos grados de estudios</h4>
	<div class="principal-container">
		<div class="col-lg-4 col-xs-12">
			<label>
				Grado Académico <span class="text-center text-danger error hidden" style="color: red" id="error_gradoacademico" name="error_gradoacademico"></span>
				<br>
				<select id="GradoAcademico" name="GradoAcademico"  class="js-example-basic-single form-control"  required value="{{ old('GradoAcademico') }}" style="width: 100%; font-weight: bold;">
					<option value="">Seleccione uno ...</option>
					@foreach ($edu_level as $edu_level)
					<option value="{{ $edu_level-> id }}">{{ $edu_level -> name }}</option>
					@endforeach
				</select>
			</label>
		</div>

		<div class="col-lg-4 col-xs-12">
			<label>
				Área académica <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_areaacademica"></span>
				<input type="text" id="AreaAcademica" class="form-control" name="AreaAcademica" placeholder="Bachiller, Licenciado, Maestro, Doctor, etc." required>
			</label>
		</div>

		<div class="col-lg-4 col-xs-12">
			<label>
				Carreras <span class="text-center text-danger error hidden" style="color:red" id="error_carreras"></span>
				<br>
				<select id="Carreras" name="Carreras"  class="form-control js-example-basic-single"  required value="{{ old('Carreras') }}" style="width: 100%;" disabled="true">
					<option value="">Seleccione uno ...</option>
				</select>
			</label>
		</div>
		
		<div class="col-lg-6 col-xs-12">
			<label>
				Nombre de la Escuela <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_nombreescuela"></span>
				<input type="text" id="NombreEscuela" name="NombreEscuela" class="form-control" placeholder="Nombre completo de la institución" required value="{{ old('NombreEscuela') }}">
			</label>
		</div>
		<div class="col-lg-6 col-xs-12">
			<label>
				Dirección <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_direccionescuela"></span>
				<input type="text" id="DireccionEscuela" name="DireccionEscuela" class="form-control" placeholder="Dirección de la institución" required value="{{ old('DireccionEscuela') }}">
			</label>
		</div>
		<div class="col-lg-3 col-xs-6">
			<label>
				Período Inicio <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_escperiodoini"></span>
				<input type="date" id="EscuelaPeriodoInicial" name="EscuelaPeriodoInicial" class="form-control" required value="{{ old('EscuelaPeriodoInicial') }}" min="1950-01-01" max="2018-12-31">
			</label>
		</div>
		<div class="col-lg-3 col-xs-6">
			<label>
				Periodo Fin <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_escperiodofin"></span>
				<input type="date" id="EscuelaPeriodoFinal" name="EscuelaPeriodoFinal" class="form-control" required value="{{ old('EscuelaPeriodoFinal') }}" min="1950-01-01" max="2023-12-31">
			</label>
		</div>
		<div class="col-lg-3 col-xs-12">
			<label>
				Finalización <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_terminoestudios"></span>
				<select id="TerminoEstudios" name="TerminoEstudios" class="form-control" data-live-search="true" required value="{{ old('TerminoEstudios') }}">
					<option value="">Seleccione uno...</option>
					<option value="Concluido">Concluido</option>
					<option value="Trunco">Trunco</option>
				</select>
			</label>
		</div>
		<div class="col-lg-3 col-xs-12">
			<label>
				Certificado <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_certificadogrado"></span>
				<input type="text" id="CertificadoGrado" name="CertificadoGrado" class="form-control" placeholder="Certificado secundaria, Técnico Bachiller, Título universitario" required value="{{ old('CertificadoGrado') }}">
			</label>
		</div>
	</div>
	<div class="col-md-12">
		@if (isset($schools))
		<button id="schools_visible" style="background: transparent; border: 0;" value="1">Mostrar Tabla de Estudios</button>
		@endif
		<div class="row col-md-10 col-md-offset-1 table-responsive hidden" id="div_table_schools">
			<!--  -->
			<table class="table table-striped text-center">
				<thead>
					<tr>
						<th>Grado Academico</th>
						<th>Instituto</th>
						<th>Periodo Final</th>
						<th>Certificado</th>
						<th>Concluido/Trunco</th>
					</tr>
				</thead>
				<tbody id="schools_table" name="schools_table">
					@foreach($schools as $s)
					<tr id="Familiar{{ $s->id }}">
						<td>{{ $s->education_area }}</td>
						<td>{{ $s->school_name }}</td>
						<td>{{ $s->final_period }}</td>
						<td>{{ $s->degree_certificate }}</td>
						<td>{{ $s->finish_studies }}</td>
						<td><button class="btn btn-sm btn-circle delete_schools" data-info="{{ $s->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
								<i class="fa fa-trash " aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
							</button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-xs-12 text-center">
		<br>
		<button type="button" class="btn btn-danger" style="background-color: black; border-color: black;" id="guarda_scholarshipdata" style="width:8em; height:3em">Guardar</button>
	</div>
	<div class="col-md-12">
		<div class="form-group text-center">
			<br>
			<p style="margin-bottom: -1em;">¿Actualmente estudias?</p>
			<div class="col-xs-1 col-xs-offset-5">
				<span class="radio-inline ok">
					<input type="radio" onchange="javascript:showContent()" name="check"id="checkYES"> SI
				</span>
			</div>
			<div class="col-xs-1">
				<span class="radio-inline ok">
					<input type="radio" onchange="javascript:showContent()" name="check"id="checkNo"> NO 
				</span>
			</div>
		</div>
	</div>
	@include('adminlte::jobapplication._forms._form_actualstudy')
	<div class="col-xs-12 text-right">
		<br>
		<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black;" id="next-scholarship">Siguiente</button>
	</div>
</div>
<script type="text/javascript">
	function showContent() {
		element = document.getElementById("actualstudies");
		check = document.getElementById("checkYES");
		if (check.checked) {
			element.style.display='block';
		}
		else {
			element.style.display='none';
		}
	}
</script>