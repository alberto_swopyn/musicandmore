<div class="row tab-pane fade" id="work_history">
	<h3 class="titleJob text-center">EXPERIENCIA LABORAL</h3>
	<h4 class="subtitleJob text-center">Cuéntanos en qué has trabajado</h4>
	<h5 class="subtitleJob text-center"> Incluir al menos los últimos 3 empleos</h5>	
	<div class="principal-container">
		<h4 class="subtitleJob text-center">Datos generales del trabajo</h4>

		<div class="col-lg-6">

			<label>Periodo Inicial <span class="text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_perinicial" style="margin-left: 9.5em"></span>
				<input type="date" class="form-control" id="PeriodoInicial" name="PeriodoInicial" required value="{{ old('PeriodoInicial') }}" min="1950-01-01" max="2018-12-31">
			</label>
		</div>

		<div class="col-lg-6">

			<label>Periodo Final <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_perfinal"></span>
				<input type="date" class="form-control" id="PeriodoFinal" name="PeriodoFinal" required value="{{ old('PeriodoFinal') }}" min="1950-01-01" max="2018-12-31">
			</label>
		</div>

		
		<div class="col-lg-6">
			<label>Compañía <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_nombrecompañia"></span>
				<input type="text" class="form-control" id="NombreCompañia" name="NombreCompañia" placeholder="Nombre Compañía" required maxlength="50" required value="{{ old('NombreCompañia') }}">
			</label>
		</div>
		
		
		<div class="col-lg-6">
			<label>Número Telefónico <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_telcompañia"></span>
				<input type="text" class="form-control" id="TelefonoCompañia" name="TelefonoCompañia" placeholder="449-123-4567" required value="{{ old('TelefonoCompañia') }}" maxlength="10">
			</label>
		</div>

		
		<div class="col-lg-12">
			<label>Dirección <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_direccioncompañia"></span>
				<input type="text" id="DireccionCompañia" name="DireccionCompañia" class="form-control" placeholder="Blvd. Aguascalientes No. 606 Col. Pulgas Pandas C.P. 20130" required value="{{ old('DireccionCompañia') }}">
			</label>
		</div>
	</div>
	<div class="principal-container">
		<h4 class="text-center" style="margin-top: 17em">Especificaciones del puesto</h4>
		<div class="col-lg-6">
			<label>Puesto <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_puesto"></span>
				<input type="text" class="form-control" id="Puesto" name="Puesto" placeholder="Obrero, Supervisor" required value="{{ old('Puesto') }}">
			</label>
		</div>

		<div class="col-lg-6">
			<label>Ingresos Iniciales <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_ingresoinicial"></span> <br>
				<span class="glyphicon glyphicon-usd">&nbsp;</span>&nbsp;<input type="number" class="form-control" id="IngresoInicial" name="IngresoInicial" placeholder="1524" required value="{{ old('IngresoInicial') }}" min="150" max="999999" step="50" style=" width: 15em; text-align: center;">&nbsp;<span style="text-align: right;">mensuales</span>
			</label>
		</div>

		
		<div class="col-lg-6">
			<label>Ingresos Finales <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_ingresofinal"></span> <br>
				<span class="glyphicon glyphicon-usd">&nbsp;</span>&nbsp;<input type="number" class="form-control" id="IngresoFinal" name="IngresoFinal" placeholder="1524" required value="{{ old('IngresoFinal') }}" min="150" max="999999" step="50" style=" width: 15em; text-align: center;">&nbsp;<span style="text-align: right;">mensuales</span>
			</label>
		</div>

		
		<div class="col-lg-6">
			<label>Motivo de separación <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_motivoseparacion"></span>
				<input type="text" class="form-control" id="MotivoSeparacion" name="MotivoSeparacion" placeholder="Mejora de Trabajo" required value="{{ old('MotivoSeparacion') }}">
			</label>
		</div>

		
		<div class="col-lg-6">
			<label>Experiencia <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_experiencia"></span>
				<input type="text" class="form-control" id="Experiencia" name="Experiencia" required value="{{ old('Experiencia') }}" placeholder="meses, años, etc,">
			</label>
		</div>

		
		<div class="col-lg-6">
			<label>Área de experiencia <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_areaexperiencia"></span>
				<input type="text" class="form-control" id="AreaExperiencia" name="AreaExperiencia" required value="{{ old('AreaExperiencia') }}" placeholder="Ventas, Administración, Finanzas, etc.">
			</label>
		</div>
	</div>
	<div class="principal-container">
		<h4 class="text-center" style="margin-top: 17em">Referencias del trabajo</h4>
		<div class="col-lg-6">
			<label>Nombre Jefe Directo <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_nombrejefe"></span>
				<input type="text" class="form-control" id="NombreJefe" name="NombreJefe" placeholder="Nombre completo" required value="{{ old('NombreJefe') }}">
			</label>
		</div>

		<div class="col-lg-6">
			<label>Puesto del Jefe Directo <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_puestojefe"></span>	
				<input type="text" class="form-control" id="PuestoJefe" name="PuestoJefe" placeholder="Gerente" required value="{{ old('PuestoJefe') }}">
			</label>
		</div>

		<div class="col-lg-6">
			<label>Correo electrónico <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_correojefe"></span>
				<input type="email" class="form-control" id="CorreoJefe" name="CorreoJefe" placeholder="example@dominio.com" required value="{{ old('CorreoJefe') }}">
			</label>
		</div>

		<div class="col-lg-6">
			<label>Número telefónico <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_teljefe"></span>
				<input type="text" class="form-control" id="TelefonoJefe" name="TelefonoJefe" placeholder="449-123-4567" maxlength="10" required value="{{ old('TelefonoJefe') }}">
			</label>
		</div>

		<div class="col-lg-5">
			<label>¿Referencias? <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_referencia"></span>	
				<div class="form-group">
					<div class="col-xs-6">
						<span class="radio-inline ok">
							<input onclick="Razones.disabled = true; Razones.val=false" type="radio" name="Referencias" id="Referencias" value="Si"> Sí
						</span>
					</div>
					<div class="col-xs-6">
						<span class="radio-inline ok">
							<input onclick="Razones.disabled = false;" type="radio" name="Referencias" id="Referencias" value="No"> No
						</span>
					</div>
				</div>
			</label>
		</div>

		<div class="col-lg-7">
			<label>Razones <span class="text-center text-danger error hidden" style="color: red; margin-bottom: 0;" id="error_razones"></span>
				<input type="text" class="form-control" id="Razones" name="Razones" placeholder="Explique las razones" maxlength="10" required value="{{ old('Razones') }}">
			</label>
		</div>
	</div>
	<div class="row text-center">
		<button type="button" class="btn btn-danger" id="guarda_workhistory" style="background-color: black; border-color: black; width:8em; height:3em; margin-top: 1em">Guardar</button>
	</div>
	@if (count($work_h)<=0)
	<div class="row col-md-10 col-md-offset-1 table-responsive hidden" id="div_table_jobs" style="padding-bottom: 2em;">
		@else
		<div class="row col-md-10 col-md-offset-1 table-responsive" id="div_table_jobs" style="padding-bottom: 2em; margin-top: 5em;">
			@endif
			<table class="table table-striped text-center">
				<thead>
					<tr>
						<th>Compañia</th>
						<th>Puesto</th>
						<th>Teléfono</th>
						<th>Jefe</th>
						<th>Tiempo de labor</th>
					</tr>
				</thead>
				<tbody id="jobs_table" name="jobs_table">
					@foreach($work_h as $work_h)
					<tr id="School_{{ $work_h->id }}">
						<td>{{ $work_h->company_name }}</td>
						<td>{{ $work_h->job_title }}</td>
						<td>{{ $work_h->company_phone }}</td>
						<td>{{ $work_h->boss_name }}</td>
						<td>{{ $work_h->time_job }}</td>
						<td><button class="btn btn-sm btn-circle deletework_h" data-info="{{ $work_h->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
									<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
								</button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-xs-12 text-right">
			<br>
			<button type="button" class="btn btn-primary next-step button_job" style="background-color: black; border-color: black;" id="next-jobs">Siguiente</button>
		</div>
	</div>
