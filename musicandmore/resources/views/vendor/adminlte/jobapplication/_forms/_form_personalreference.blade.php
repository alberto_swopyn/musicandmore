<div class="row tab-pane fade" id="personal_references">
	<h3 class="titleJob text-center">REFERENCIAS</h3>
	<h4 class="subtitleJob text-center">A quién podemos contactar como referencia tuya</h4>
	<div>
		<div class="principal-container">
			<br>
			<div class="col-lg-6">
				<label>Nombre <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_NombreReferencia"></span>
					<input type="text" id="NombreReferencia" name="NombreReferencia" class="form-control" maxlength="50" required value="{{ old('NombreReferencia') }}" placeholder="Nombre completo">
				</label>
			</div>

			<div class="col-lg-6">
				<label>Dirección <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_DireccionReferencia"></span>
					<input type="text" class="form-control" id="DireccionReferencia" name="DireccionReferencia" required value="{{ old('DireccionReferencia') }}" placeholder="Dirección completa">
				</label>
			</div>

			<div class="col-lg-6"> 
				<label>Relación con la persona <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_RelacionReferencia"></span>
					<select id="RelacionReferencia" name="RelacionReferencia" class="form-control" data-live-search="true" required value="{{ old('RelacionReferencia') }}">
						<option value="">Seleccione uno...</option>
						<option value="Familiar">Familiar</option>
						<option value="Conocido">Conocido</option>
						<option value="Amigo">Amigo</option>
						<option value="Otros">Otros</option>
					</select>
				</label>
			</div>

			<div class="col-lg-6">
				<label>Ocupación <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_OcupacionReferencia"></span>
					<input type="text" class="form-control" id="OcupacionReferencia" name="OcupacionReferencia" placeholder="Comerciante, Supervisor, Ama de Hogar, etc." required value="{{ old('OcupacionReferencia') }}">
				</label>
			</div>

			<div class="col-lg-5">
				<label>Número Telefónico <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_NumeroReferencia"></span>
					<input type="text" id="NumeroReferencia" name="NumeroReferencia" class="form-control" placeholder="449-123-4567" maxlength="10" required value="{{ old('NumeroReferencia') }}">
				</label>
			</div>

			<div class="col-lg-5">
				<label>Tiempo de conocerlo <span class="text-center text-danger hidden error" style="color: red; margin-bottom: -3px;" id="error_TiempoReferencia" style="font-size: 5px"></span>
					<input type="number" id="TiempoReferencia" name="TiempoReferencia" class="form-control text-center" min="1" required value="{{ old('TiempoReferencia') }}" placeholder="2">
				</label>
			</div>

			<div class="col-lg-2">
				<label>
					<select class="form-control" data-live-search="true" name="timeReferences" id="timeReferences">
						<option value="">Seleccione...</option>
						<option value="meses">meses</option>
						<option value="años">años</option>
					</select>
				</label>
			</div>
		</div>
	</div>
	<div class="col-lg-12 text-center" style="padding-bottom: 3em; padding-top: 1em">
		<button type="button" class="btn btn-danger" style="background-color: black; border-color: black;" id="guarda_personalreferences" style="width:8em; height:3em">Guardar</button>
	</div>
	@if (count($reference)<=0)
	<div class="row col-md-12 hidden table-responsive" id="div_table_reference">
		@else
		<div class="row col-md-12 table-responsive" id="div_table_reference">
			@endif
			<table class="table table-striped text-center">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Dirección</th>
						<th>Relación</th>
						<th>Ocupación</th>
						<th>Num. Tel.</th>
						<th>Tiempo de conocerlo</th>
					</tr>
				</thead>
				<tbody id="reference_table" name="reference_table">
					@foreach($reference as $reference)
					<tr id="PersonalReferences_{{ $reference->id }}">
						<td>{{ $reference->name }}</td>
						<td>{{ $reference->address }}</td>
						<td>{{ $reference->relationship }}</td>
						<td>{{ $reference->occupation }}</td>
						<td>{{ $reference->phone_number }}</td>
						<td>{{ $reference->meet_time }}</td>
						<td><button class="btn btn-sm btn-circle deletework_PersonalReferences" data-info="{{ $reference->id }}" style="background: none; height: auto; width: 15px; padding: 0" >
									<i class="fa fa-trash" aria-hidden="true" style="color: red; font-size: 1.5em;"></i>
								</button></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-xs-12 text-right">
			<br>
			<button type="button" class="btn btn-primary next-step" style="background-color: black; border-color: black;" id="next-personalreferences">Siguiente
			</button>
		</div>
	</div>
