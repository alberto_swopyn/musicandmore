<!DOCTYPE html>
<html lang="es">
<head>
  <!-- Required meta tags always come first -->
  <meta charset="utf-8">
  <?php header('X-Frame-Options: SAMEORIGIN');
        header('X-Content-Type-Options: nosniff');
        header("X-XSS-Protection: 1; mode=block"); ?>
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.png')}}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Swopyn</title>
  <link href="{{ asset('/css/style_page.css') }}" rel="stylesheet">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap-flex.css')}}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">

  <!-- Scroll -->
  <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/smooth-scroll.min.js')}}"></script>
  <script>
    smoothScroll.init({
      selector: '[data-scroll]', // Selector for links (must be a class, ID, data attribute, or element tag)
      selectorHeader: null, // Selector for fixed headers (must be a valid CSS selector) [optional]
      speed: 2000, // Integer. How fast to complete the scroll in milliseconds
      easing: 'easeInOutCubic', // Easing pattern to use
      offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
      callback: function ( anchor, toggle ) {} // Function to run after scrolling
  });
</script>

</head>
<body>
  <nav class="col-md-12 navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <a href="#" class="navbar-brand"><img src="{{ asset('img/FirmaSwopynCVS-01.png')}}" width="70em" height="100em"></a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
       <li><a href="#sec-1" class="smoothScroll">{{ trans('adminlte_lang::message.home') }}</a></li>
       <li><a href="#sec-2"> Conócenos</a></li>
        <li><a href="Https://www.SwopynSecurity.com"> Swopyn Security</a></li>
   </ul>
   <ul class="nav navbar-nav navbar-right">
      @if (Auth::guest())
      <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
      <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
      @else
      <li><a href="{{route('training_study')}}">{{ Auth::user()->name }}</a></li>
      @endif
  </ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>

<!-- SECCIÓN 1 -->
<div class="container-fluid col-md-12 full">  
    <div class="row flex-items-xs-middle slider-1 flex-items-md-middle " id="sec-1" >
      <div class="col-md-8 col-md-offset-2">
        <p class="titulo" style="margin-top: 1em; margin-bottom: -.3em;"><span class="glyphicon glyphicon-chevron-left"></span> Inspira acciones<br></p>
        <p class="titulo" style=" margin-left: .5em;"> Impulsa Resultados<span class="glyphicon glyphicon-chevron-right"></span></p>
        <p class="subtitulo">Te acompañamos inspirando acciones para controlar los pilares <br> fundamentales de tu negocio en cada área: <br> CAPITAL HUMANO, OPERACIÓN, COMERCIAL Y ADMINISTRACIÓN <br> para impulsar verdaderos resultados y alcanzar tus metas.
        </p>
    </div>
</div>
<!-- SECCIÓN 2 -->
<div class="row flex-items-md-center slider-2" style="margin-left: 0em; margin-right: 1em"  id="sec-2">
    <div class="col-md-12" style="margin-top: 5em; margin-bottom: 3em;" >
      <div class="col-md-6">
        <div class="col-md-4">
            <!-- {{ asset('/img/intro01.png') }} -->
            <img src="{{ asset('/img/operaciones.png')}}" class="img-responsive text-right" alt="">
        </div>
        <div class="col-md-8" style="text-align: left;">
          <img class="img-responsive" src="{{ asset('/img/FONDO_ope_R.png')}}">
          <br>
          <h4>Resuelve y opera fácil con total control</h4>
          ¿Cuál es la productividad y desempeño de los empleados en tu empresa?
          ¿Las actividades que deben realizar los empleados se están llevando a cabo?
          Mejora la gestión y supervisión de las principales actividades de la operación diaria en tu negocio, optimizalas y mejora tus procesos al identificar el rendimiento y focos rojos de las actividades principales de tu empresa. </p>
      </div>
  </div>
  <!-- SECCIÓN 3 -->
  <div class="col-md-6">
    <div class="col-md-8" style="text-align: left;">
      <img class="img-responsive" src="{{ asset('/img/FONDO_CAPHUM_R.png')}}">
      <br>
      <h4>Monitorea el proceso de cada colaborador</h4>
      <p>¿Los empleados que estas contratando realmente cubren el perfil del puesto requerido?
        ¿Sabes cuál es la capacidad laboral en tu empresa? 
        ¿Todo el personal en tu empresa está preparado y capacitado para desempeñar sus funciones?
        Conoce y toma el control de las cuatro áreas más importantes de Recursos Humanos,  Reclutamiento, Selección, Capacitación y Administración de personal.</p>
    </div>
    <div class="col-md-4">
        <img src="{{ asset('/img/CAPITAL_HUMANO.png')}}" class="img-responsive text-right" alt="">
    </div>
</div>
<!-- SECCIÓN 4 -->
<div class="col-md-12" style="margin-top: 6em">
  <div class="col-md-6">
    <div class="col-md-4">
      <img src="{{ asset('/img/comercial.png')}}" class="img-responsive text-right" alt="">
  </div>
  <div class="col-md-8" style="text-align: left;">
      <img class="img-responsive" src="{{ asset('/img/FONDO_comercial_R.png')}}">
      <br>
      <h4>Seguimiento puntual hasta concretar la venta</h4>
      <p>¿Se lograrán los resultados esperados con la estrategia actual del negocio? ó ¿Dónde estoy fallando para llegar a los resultados que deseo obtener?
        Visualiza la información de tu negocio relacionada con las ventas de la sucursal que sirven para fijar rumbo en el proceso de supervisión y alcanzar los objetivos comerciales.
        Mide el pulso del negocio para poder tomar decisiones rápidas basadas en métricas e información generada de manera interna permitiendo una visión 360° del negocio. </p>
    </div>
</div>
<!-- SECCIÓN 5 -->
<div class="col-md-6">
  <div class="col-md-8" style="text-align: left;">
    <img class="img-responsive" src="{{ asset('/img/FONDO_admin_R.png')}}">
    <br>
    <h4>Todo bajo control para toma de decisiones en tiempo real.</h4>
    <p>¿Los lineamientos de tu empresa se están cumpliendo?
      Permite el seguimiento, alineamiento y administración en la ejecución de la estrategia de cada compañía en tiempo real así como el ahorro de costos en procesamiento y centralización de la información necesaria. </p>
  </div>
  <div class="col-md-4">
      <img src="{{ asset('/img/admin.png')}}" class="img-responsive text-right" alt="">
  </div>
</div>
</div>
</div>
</div>
          <!-- SECCIÓN 6 -->
          <div class="col-md-10 col-md-offset-1 slider-3 text-center">
            <img class="img-responsive" src="{{ asset('/img/swopynbca.png')}}" style="margin: auto;">
            <h3><br>Alcanza todo lo que te propongas</h3>
          </div>
          <div>
            <footer>
              <div class="container" >
                <p style="color: white; text-align: center; ">
                  <a href="https://www.swopyn.com"><a><b>Swopyn </b></a>|</a>
                  <strong >Copyright &copy; 2017 Todos los Derechos Reservados.</strong> 
                </p>
              </div>
            </footer>
          </div>
</div>
<!-- FIN SECCIÓN --> 
</body>
</html>