<!DOCTYPE html>

<html lang="en">


@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')
@show


<body class="skin-blue sidebar-mini">
<div id="app">
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('adminlte::layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    
    @include('adminlte::layouts.partials.controlsidebar')

    @include('adminlte::layouts.partials.footer')

    
</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show
@yield('personal-js')
@yield('circliful')

</body>

<script>
	ProgressDialogModal.InitModal($("#app"));
</script>
</html>