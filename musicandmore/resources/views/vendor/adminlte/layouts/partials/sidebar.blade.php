<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
            </div>
        </div>
        @endif

        

        <!-- Sidebar Menu -->

        <ul class="sidebar-menu">
            
            <li>
                <a href="{{ route('training_study') }}">
                <i class="fa fa-film" aria-hidden="true"></i>Tus Cursos</a>
            </li>
            
            @role(['docente'])
            <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                        <span>Solicitud</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('jobapplication') }}">Solicitud</a>
                        </li>
                    </ul>
            </li>
            @endrole

            @role(['admin', 'docente'])
            <!-- CONTROL ESCOLAR -->
            <li class="treeview">
                <a href=""><i class='fa fa-folder-open'></i> <span>Control Escolar</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @role(['admin'])
                        <li><a href="{{ route('assistance_teacher') }}">Pase de Asistencia<br>Maestros</a></li>
                        <li><a href="{{ route('report_assistance') }}">Reporte de Asistencia<br>Maestros</a></li>
                    @endrole

                    <li><a href="{{ route('report_assistances') }}">Reporte de Asistencia<br>Alumnos</a></li>
                    
                    @role(['docente'])
                        <li><a href="{{ route('assistance') }}">Asistencia</a></li>
                    @endrole
                </ul>
            </li>
            <!-- FIN CONTROL ESCOLAR -->
            @endrole

            <!-- LISTAS -->
            @role(['admin'])
                <li class="treeview">
                    <a href=""><i class='fa fa-list-ul'></i> <span>Listas</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('sample_users') }}">Alumnos Muestra</a></li>
                        <li><a href="{{ route('student_status') }}">Alumnos</a></li>
                        <li><a href="{{ route('teachers') }}">Empleados</a></li>
                    </ul>
                </li>
            @endrole
            <!-- FIN LISTAS -->

            @role(['admin'])
            <!-- INSCRIPCION -->
            <li class="treeview">
                <a href=""><i class='fa fa-tasks'></i> <span>Clases</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('subjects') }}">Asignaturas</a></li>
                    <li><a href="{{ route('indexcath') }}">Catedra</a></li>
                    <li><a href="{{ route('groups') }}">Grupos</a></li>
                    <li><a href="{{ route('classroom') }}">Aulas</a></li>
                </ul>
            </li>
            <!-- FIN INSCRIPCION -->
            @endrole

            @role(['admin'])
            <!-- HORARIO ADMIN -->
                <li><a href="{{ route('schedule') }}" target="_blank"><i class="fa fa-calendar" aria-hidden="true"></i><span>Horarios</span></a></li>
            <!-- FIN HORARIO -->
            @endrole

            @role(['docente'])
            <!-- HORARIO MAESTRO -->
                <li><a href="{{ route('schedule_teacher') }}" target="_blank"><i class="fa fa-calendar" aria-hidden="true"></i><span>Horario</span></a></li>
            <!-- FIN HORARIO -->
            @endrole

            @role(['alumno'])
            <!-- HORARIO ALUMNO -->
                <li><a href="{{ route('schedule_student') }}" target="_blank"><i class="fa fa-calendar" aria-hidden="true"></i><span>Mi Horario</span></a></li>
            <!-- FIN HORARIO -->
            @endrole

            <!-- PAGOS -->
            <li class="treeview">
                <a href=""><i class='fa fa-money'></i> <span>Pagos</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @role(['alumno'])
                        <li><a href="{{ route('pays') }}">Mis pagos</a></li>
                    @endrole
                    @role(['admin'])
                        <li><a href="{{ route('pays_admin') }}">Catálogo de Pagos</a></li>
                         <li><a href="{{ route('discount_admin') }}">Catálogo de Descuentos</a></li>
                          <li><a href="{{ route('box_admin') }}">Caja de Cobro</a></li>
                          <li><a href="{{ route('cut_box_report') }}">Cortes de Caja</a></li>
                          <li><a href="{{ route('report_adeudos') }}">Reporte de Adeudos</a></li>
                        <li><a href="{{ route('pays_teacher') }}">Nomina</a></li>

                    @endrole
                    @role(['docente'])
                        <li><a href="{{ route('nomine') }}">Mi Nomina</a></li>
                    @endrole
                </ul>
            </li>
            <!-- FIN PAGOS -->
            
            <!-- TAREAS -->
            @role(['alumno', 'docente'])
            <li class="treeview">
                    <a href="#"><i class='fa fa-book'></i> <span>Tareas</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        @role(['alumno'])
                            <li><a href="{{ route('tasks') }}">Tus Tareas</a></li>
                        @endrole
                        @role(['docente'])
                            <li><a href="{{ route('tasks_admin') }}">Tareas</a></li>
                        @endrole
                    </ul>
            </li>
            @endrole
            <!-- FIN TAREAS -->




            @role(['admin'])
            <li class="treeview">
                    <a href="{{ route('company_show') }}#"><i class="fa fa-area-chart" aria-hidden="true"></i><span>Administrativo</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('tree_jobcenter') }}">Estructura de Centros <br>de Trabajo</a></li>
                            <li><a href="{{ route('tree_jobprofile') }}">Estructura de Puestos</a></li>
                            <li><a href="{{ route('training_index') }}">Cursos</a></li>
                            <li><a href="{{ route('training_index_assign') }}">Plan de capacitación</a></li>
                        </ul>
            </li>
            @endrole

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
