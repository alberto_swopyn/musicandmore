<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <img src="{{ asset( 'img/logo_swopyn_azul.png') }}" alt="logo">
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="http://acacha.org"></a>.</strong>
</footer>
