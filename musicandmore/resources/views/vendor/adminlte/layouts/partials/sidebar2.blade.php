<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
            </div>
        </div>
        @endif

        <!-- search form (Optional) -->
      <!--  <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="{{ url('training/study') }}"><i class='fa fa-home'></i> <span>{{ trans('adminlte_lang::message.home') }}</span></a></li>
            <li class="treeview">
                <a href="{{ route('training_study') }}#"><i class='fa fa-graduation-cap'></i> <span>Curso</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('training_study') }}">Tus Cursos</a></li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{ route('training_study') }}#"><i class='fa fa-book'></i> <span>Tareas</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('tasks') }}">Tus Tareas</a></li>
                    @role(['admin','hr'])
                          <li><a href="{{ route('tasks_admin') }}">Tareas</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{ route('company_show') }}#">
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                        <span>Aspirante</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            @role(['alumno']) <!-- Alumno -->
                             <a href="{{ route('jobapplicationstudent') }}">Datos del alumnos</a>
                            @endrole
                            @role(['admin', 'docente']) <!-- Administrador y/o Maestro -->
                             <a href="{{ route('jobapplication') }}">Solicitud</a>
                            @endrole
                        </li>
                        <li>
                            <a href="{{ route('documents_personal') }}">Subir Documentos</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{ route('company_show') }}#"><i class="fa fa-area-chart" aria-hidden="true"></i><span>Administrativo</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('tracking_index') }}">Aspirantes</a></li>
                            <li><a href="{{ route('tree_jobcenter') }}">Estructura de Centros <br>de Trabajo</a></li>
                            <li><a href="{{ route('tree_jobcenter_summary') }}">Centros de Trabajo-<br>Resumen Vigencias</a></li>
                            <li><a href="{{ route('tree_jobprofile') }}">Estructura de Puestos</a></li>
                            <li><a href="{{ route('training_index') }}">Cursos</a></li>
                            <li><a href="{{ route('training_index_assign') }}">Asignación Cursos</a></li>
                            <li><a href="{{ route('companyEvaluation') }}">Evaluación de empresa</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href=""><i class='fa fa-tasks'></i> <span>Catedras</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('indexcath') }}">Clases</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="{{ route('company_show') }}#"><i class='fa fa-calendar'></i> <span>Horarios</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                        @role(['alumno'])
                        <ul class="treeview-menu">
                            <li><a href="{{ route('schedule_student') }}">Alumno</a></li>
                        </ul>
                        @endrole
                        @role(['docente'])
                        <ul class="treeview-menu">
                            <li><a href="{{ route('schedule_teacher') }}">Maestro</a></li>
                        </ul>
                        @endrole
                        @role(['admin'])
                        <ul class="treeview-menu">
                            <li><a href="{{ route('schedule') }}">Administrador</a></li>
                        </ul>
                        @endrole
                    </li>

                    <li class="treeview">
                        <a href=""><i class='fa fa-folder-open'></i> <span>Control Escolar</span>
                        <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('groups') }}">Grupos</a></li>
                            <li><a href="{{ route('classroom') }}">Aulas</a></li>
                            <li><a href="{{ route('assistance') }}">Asistencia</a></li>
                            <li><a href="{{ route('assistance_teacher') }}">Pase de Asistencia<br>Maestros</a></li>
                            <li><a href="{{ route('report_assistance') }}">Reporte de Asistencia<br>Maestros</a></li>
                             <li><a href="{{ route('report_assistances') }}">Reporte de Asistencia<br>Alumnos</a></li>
                            @role(['admin','hr'])
                            <li><a href="{{ route('student_status') }}">Alumnos</a></li>
                            @endrole
                        </ul>
                    </li>

                    <li><a href="{{ route('kardex_employees') }}"><i class="fa fa-users" aria-hidden="true"></i><span>Empleados</span></a></li>
                    <li><a href="{{ route('SearchKardex', ['search' => 'jobcenters']) }}"><i class="fa fa-building" aria-hidden="true"></i><span>Kardex</span></a></li>
                    <li><a href="{{ route('subjects') }}"><i class="fa fa-clipboard" aria-hidden="true"></i><span>Asignaturas</span></a></li>

                    @endrole
                    <li class="treeview">
                    <a href="{{ route('training_study') }}#"><i class='fa fa-money'></i> <span>Pagos</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('pays') }}">Mis pagos</a></li>
                    @role(['admin','hr'])
                          <li><a href="{{ route('pays_admin') }}">Agregar pago</a></li>
                          <li><a href="{{ route('pays_teacher') }}">Nomina</a></li>
                          <li><a href="{{ route('nomine') }}">Mi Nomina</a></li>
                    </ul>
                </li>
                @endrole

             <!-- <li class="treeview">
                <a href="{{ route('company_show') }}#"><i class='fa fa-cogs'></i> <span>Compañia</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('company_show') }}">Ver</a></li>
                   <li><a href="{{ route('paytype_create') }}">Tipos pago</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Centro de trabajo</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('branch_index') }}">Lista</a></li>
                    <li><a href="{{ route('branch_create') }}">Nueva</a></li>
                </ul>
            </li> -->

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
