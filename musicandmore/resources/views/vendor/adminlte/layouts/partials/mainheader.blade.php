<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src=" {{ asset( 'img/M&M2.png') }}" style="width: 50px; height: 50px; " alt="logo"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="{{ asset( 'img/M&M2.png') }}" style="width: 100px; height: 100px; " alt="logo" width="100"></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                    @foreach(auth()->user()->unreadNotifications as $notification)
                        <span class="label" style="background-color: #1bb49a">{{$notification->where('type','=','Swopyn\Notifications\MessageAlert')->where('notifiable_id',auth()->user()->id)->where('read_at',null)->count()}}
                        </span>
                    @endforeach
                    </a>
                    <ul class="dropdown-menu">
                         <li class="header">
                            @forelse(auth()->user()->unreadNotifications as $not)
                                    @if($not->type == 'Swopyn\Notifications\MessageAlert')
                                    <a href="{{url('notifications/'.$not->id)}}" class="text-justify">{{$not->data['user']['name']}} envio un <br>mensaje en: {{$not->data['message']['title']}}</a>
                                    @endif
                                    @empty
                                    <a href="#">No tiene mensajes nuevos</a>   
                            @endforelse
                        </li>
                       <li>
                        @foreach(auth()->user()->unreadNotifications as $notification)
                        @if($notification->where('type','=','Swopyn\Notifications\MessageAlert')->where('notifiable_id',auth()->user()->id)->where('read_at',null)->count() > 0)
                            <script>
                                    Push.create('Nuevo Mensaje',{
                                    body: "Has recibido un nuevo mensaje",
                                    icon: "../img/icon_swopyn.png",
                                    timeout: 4000,
                                });
                            </script>
                        @endif
                        @endforeach

                           <!--  <li class="header" id="messageCount">Usted tiene 0 mensajes</li>
                            <li>
                                
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <div class="pull-left" id="messageGravar">
                                                
                                                <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image"/>
                                            </div>
                                            
                                            <h4 id="messageTitle">
                                                Vacio
                                                <small><i class="fa fa-clock-o"></i> 0 mins</small>
                                            </h4>
                                            
                                            <p id="messageDescription">Sin Mensajes</p>
                                        </a>
                                    </li>
                        
                      
                            </ul>

                        </li>
                        <li class="footer"><a href="#">c</a></li> -->
                    </ul>
                </li><!-- /.messages-menu -->

                <!-- Notifications Menu -->
                <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="" class="dropdown-toggle" data-toggle="dropdown">
                     <i class="fa fa-bell-o"></i>
                     @foreach(auth()->user()->unreadNotifications as $notification)
                         <span class="label " style="background-color:  #f5216e
">{{$notification->where('type','=','Swopyn\Notifications\TaskAlert')->where('notifiable_id',auth()->user()->id)->where('read_at',null)->count()}}</span>
                    @endforeach
                    </a>
                   <ul class="dropdown-menu">

                        <li class="header">
                            @forelse(auth()->user()->unreadNotifications as $not)
                                    @if($not->type == 'Swopyn\Notifications\TaskAlert')
                                    <a href="{{url('notifications/'.$not->id)}}" class="text-justify">{{$not->data['user']['name']}} te asigno: <br>{{$not->data['tasks']['title']}}</a>
                                    @endif
                                    @empty
                                    <a href="#">No tiene tareas nuevas</a>   
                            @endforelse
                        </li>
                       <li>
                     @foreach(auth()->user()->unreadNotifications as $notification)
                        @if($notification->where('type','=','Swopyn\Notifications\TaskAlert')->where('notifiable_id',auth()->user()->id)->where('read_at',null)->count() > 0)
                            <script>
                                    Push.create('Nueva Tarea',{
                                    body: "Nueva Tarea Asignada",
                                    icon: "../img/icon_swopyn.png",
                                    timeout: 4000,
                                });
                            </script>
                        @endif
                    @endforeach
                           <!--  <ul class="menu">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i> {{ trans('adminlte_lang::message.newmembers') }}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">{{ trans('adminlte_lang::message.viewall') }}</a></li>
                    </ul> -->
                    </li>
                </ul>
                <!-- Tasks Menu -->
                <li class="dropdown tasks-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                    @foreach(auth()->user()->unreadNotifications as $notification)
                       <span class="label " style="background-color: #48099d">{{$notification->where('type','=','Swopyn\Notifications\EvidencesAlert')->where('notifiable_id',auth()->user()->id)->where('read_at',null)->count()}}</span>
                    @endforeach
                    </a>
                    <ul class="dropdown-menu">

                        <li class="header">
                               @forelse(auth()->user()->unreadNotifications as $not)
                                    @if($not->type == 'Swopyn\Notifications\EvidencesAlert')
                                    <a href="{{url('notifications/'.$not->id)}}" class="text-justify">{{$not->data['user']['name']}} agrego una <br>evidencia en: {{$not->data['evidence']['title']}}</a>
                                    @endif
                                    @empty
                                    <a href="#">No hay evidencias nuevas</a>   
                            @endforelse
                        </li>
                       <li>
                    @foreach(auth()->user()->unreadNotifications as $notification)
                        @if($notification->where('type','=','Swopyn\Notifications\EvidencesAlert')->where('notifiable_id',auth()->user()->id)->where('read_at',null)->count() > 0)
                            <script>
                                    Push.create('Evidencias',{
                                    body: "Nueva Evidencia Agregada",
                                    icon: "../img/icon_swopyn.png",
                                    timeout: 4000,
                                });
                            </script>
                        @endif
                    @endforeach
                     </li>
                </ul>

                    <!-- <ul class="dropdown-menu">
                        <li class="header">{{ trans('adminlte_lang::message.tasks') }}</li>
                        <li>
                            
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        
                                        <h3>
                                            {{ trans('adminlte_lang::message.tasks') }}
                                            <small class="pull-right">20%</small>
                                        </h3>
                                        
                                        <div class="progress xs">
                                            
                                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">20% {{ trans('adminlte_lang::message.complete') }}</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">{{ trans('adminlte_lang::message.alltasks') }}</a>
                        </li>
                    </ul> -->
                </li>
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{ Gravatar::get($user->email) }}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                                <p>
                                    {{ Auth::user()->name }}
                                    <input type="hidden" id="user" name="user" value="{{ auth()->user()->id }}">
                                    </td>
                                    <small>Miembro desde: {{ $user->created_at }}</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!-- <li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.followers') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.sales') }}</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">{{ trans('adminlte_lang::message.friends') }}</a>
                                </div>
                            </li> -->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <!-- <div class="pull-left">
                                    <a href="{{ url('/settings') }}" class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>
                                </div> -->
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ trans('adminlte_lang::message.signout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
                @endif

                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
