<head>
    <meta charset="UTF-8">
    <title> Swopyn</title>
    <?php header('X-Frame-Options: SAMEORIGIN');
          header('X-Content-Type-Options: nosniff');
          header("X-XSS-Protection: 1; mode=block");
           ?>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.circliful.js') }}"></script>
    <link href="{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/style_jobapplication.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/venobox.css') }}" />
    <link href="{{ asset('/css/sweetalert2.min.css') }}" rel="stylesheet"/>
    <script src="{{ asset('/js/sweetalert2.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('/css/style.min.css') }}" />
    <link rel="shortcut icon" href="{{ asset('/img/favicon.ico') }}" />
    <link rel="stylesheet" href="{{ asset('/css/style_psychometric.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/style_tabs_jobapplication.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-directional-buttons.css') }}" />
    <script src="{{ asset('/js/dropzone.js') }}"></script>
    <link href="{{ asset('/css/dropzone.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/select2.css')}}" rel="stylesheet" />
    <script src="{{ asset('/js/select2.min.js')}}"></script>
    <script src="{{ asset('/js/ProgressDialogModal.js') }}" ></script>
    <script src="{{ asset('js/jquery.tablesorter.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/build/app.js') }}"></script>
    <script src="{{ asset('/js/push.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('/plugins/fullcalendar/fullcalendar.min.css') }}" />
    <script type="text/javascript" src="{{ asset('/plugins/fullcalendar/lib/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugins/fullcalendar/locale/es.js') }}"></script>

    @yield('personal_style')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <script>
        //See https://laracasts.com/discuss/channels/vue/use-trans-in-vuejs
        window.trans = @php
            // copy all translations from /resources/lang/CURRENT_LOCALE/* to global JS variable
            $lang_files = File::files(resource_path() . '/lang/' . App::getLocale());
            $trans = [];
            foreach ($lang_files as $f) {
                $filename = pathinfo($f)['filename'];
                $trans[$filename] = trans($filename);
            }
            $trans['adminlte_lang_message'] = trans('adminlte_lang::message');
            echo json_encode($trans);
            @endphp
        </script>

    <!-- <script>

        var userMessage = {!! auth()->user()->id !!};

         $.ajax({
                type:'POST',
                url: '../show_message',
                data: {
                        '_token': $('meta[name=csrf-token]').attr('content'),
                        user    : userMessage
                         
                        },
                    success: function (data) {  
                               
                     if ((data.errors)) {
                        //alert('errors');    
                    } else {
                       
                        var json = JSON.parse(JSON.stringify(data));
                        if (json.length>=0){
                                var html='';
                                $('#messageMenu').empty();
                                $('#messageNumber').empty();
                                $('#messageCount').empty();
                                $('#messageTitle').empty();
                                $('#messageDescription').empty();
                                $('#messageGravar').empty();
                                $('#messageNumber').append(json.length);
                                html+= ' <li class="header" id="messageCount">Usted tiene ' + json.length+'  mensajes</li><li>'
                                for (var i=0;i<json.length;++i)
                                    {
                                        html += '<ul class="menu"><li><a href="#"><div class="pull-left"><img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image"/></div><h4 id="messageTitle">'+json[i].title+
                                        '<small><i class="fa fa-clock-o"></i> 0 mins</small></h4><p>'+
                                        json[i].description+
                                        '</p></a></li></ul>';
                                        
                                        
                                    }
                                    html += '</li><li class="footer"><a href="#">c</a></li>';
                                    $('#messageMenu').append(html);
                            }
                        
                    }
                }
            });
        </script> -->
        @yield('css-personal')
    </head>


