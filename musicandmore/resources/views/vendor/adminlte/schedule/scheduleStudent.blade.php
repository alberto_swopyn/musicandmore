@extends('adminlte::layouts.apphorario')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Horario</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
							<i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<div>
						<div id='calendar' class="col-md-12"></div>
					</div>
				</div>
				<!-- /.box -->


			</div>

		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	$(function() {
		var repeatingEvents =
		[
			@foreach ($events as $events)
   	    		{
				    title:'{{ '\n' . $events -> title . '\n' . $events -> teacher }}',
				    color: '{{ $events -> color }}',
				    id: '{{ $events -> id }}',
				    start: '{{ $events -> start }}',
				    end: '{{ $events -> end }}',
				    dow: '{{ $events -> dow }}',
				    ranges: [{
				        start: moment('{{ $events -> initial_date }}'),
				        end: moment('{{ $events -> final_date }}'),
				    }],
				},
			@endforeach
		];

		var getEvents = function( start, end ){
		    return repeatingEvents;
		}

		$('#calendar').fullCalendar({
		    defaultDate: moment(),
		    header: {
		        left: 'prev,next today',
		        center: 'title',
		        right: 'month,agendaWeek,agendaDay'
		    },
    		defaultView: 'month',
		    eventRender: function(event, element, view){
		        console.log(event.start.format());
		        return (event.ranges.filter(function(range){
		            return (event.start.isBefore(range.end) &&
		                    event.end.isAfter(range.start));
		        }).length)>0;
		    },
		    events: function( start, end, timezone, callback ){
		        var events = getEvents(start,end); //this should be a JSON request

		        callback(events);
		    },
		});
	});
</script>
@endsection
