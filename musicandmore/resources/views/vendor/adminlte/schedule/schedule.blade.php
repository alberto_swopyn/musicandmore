@extends('adminlte::layouts.apphorario')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection

@section('personal_style')
@include('adminlte::tasks.PersonalSelect2')
@stop

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-12">

			<!-- Default box -->
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Horario</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
							<i class="fa fa-minus"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Cerrar">
							<i class="fa fa-times"></i></button>
					</div>
				</div>

				<div class="box-body">
					@if($cont > 1)
					<form method="GET" action="{{ Route('schedule_by_jobcenter') }}" role="form">
							{{ csrf_field() }}
					<div>
						<label for="selectEmployee" >Sucursales:</label>
					</div>
					<div class="col-md-6">
						<select name="selectEmployee" id="selectEmployee" class="applicantsList-single form-control" style="width: 100%; font-weight: bold;">
							@foreach($sucs as $su)
							<option value="{{ $su->tree_job_center_id }}">
								{{ $su->text }}
							</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6">
						<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
					</div>
					</form>
					@endif
					<div>
						<div id='calendar' class="col-md-12"></div>
					</div>
				</div>
				<!-- /.box -->


			</div>

		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	$(function() {
		var repeatingEvents =
		[
			@foreach ($events as $events)
   	    		{
				    title:'{{ $events -> title }}',
				    color: '{{ $events -> color }}',
				    id: '{{ $events -> id }}',
				    start: '{{ $events -> start }}',
				    end: '{{ $events -> end }}',
				    dow: '{{ $events -> dow }}',
				    ranges: [{
				        start: moment('{{ $events -> initial_date }}'),
				        end: moment('{{ $events -> final_date }}'),
				    }],
				},
			@endforeach
		];
		console.log(repeatingEvents);
		var getEvents = function( start, end ){
			return repeatingEvents;
		}

		$('#calendar').fullCalendar({
		    defaultDate: moment(),
		    header: {
		        left: 'prev,next today',
		        center: 'title',
		        right: 'month,agendaWeek,agendaDay'
		    },
    		defaultView: 'month',
		    eventRender: function(event, element, view){
		        return (event.ranges.filter(function(range){
		            return (event.start.isBefore(range.end) &&
		                    event.end.isAfter(range.start));
		        }).length)>0;
		    },
		    events: function( start, end, timezone, callback ){
		        var events = getEvents(start,end); //this should be a JSON request

		        callback(events);
		    },
		    eventClick: function(calEvent, jsEvent, view) {

				location.href = `https://www.swopyn.com/musicandmore/assistance/index`;

			}
		});
	});
</script>
<script>
	$(".applicantsList-single").select2();
	$(".applicantsList-single").change(function(event) {
		$('#Applicant_id').val($(this).val());
	});
	$(".jobTitleList-single").select2();
	$(".jobCenterList-single").select2();
</script>
<script>
	$(".multiple").select2();
</script>
@endsection
