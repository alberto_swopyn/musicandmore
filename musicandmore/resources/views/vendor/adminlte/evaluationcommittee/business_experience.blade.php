<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#_experienciaEmpresario">II. Experiencia del Empresario</a>
		</h4>
	</div>
	<div id="_experienciaEmpresario" class="panel-collapse collapse">
		<div class="panel-body">
			<div class="col-md-12">
				<br>
				<p> Seleccione según la experiencia, conocimiento o percepción de los siguientes puntos:</p>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-1">
						<table class="table table-striped" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<th>Experiencia</th>
									<th>Si</th>
									<th>No</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										Experiencia laboral Ventas y/o Servicio a Clientes
									</td>
									<td>
										<input type="radio" name="Ventas_Servicio" id="Ventas_Servicio" value="Si">
									</td>
									<td>
										<input type="radio" name="Ventas_Servicio" id="Ventas_Servicio" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Experiencia en el manejo de personal
									</td>
									<td>
										<input type="radio" name="ManejoPersonal" id="ManejoPersonal" value="Si">
									</td>
									<td>
										<input type="radio" name="ManejoPersonal" id="ManejoPersonal" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Experiencia en el Manejo de Inventarios
									</td>
									<td>
										<input type="radio" name="ManejoInventarios" id="ManejoInventarios" value="Si">
									</td>
									<td>
										<input type="radio" name="ManejoInventarios" id="ManejoInventarios" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Experiencia en el Manejo de Caja
									</td>
									<td>
										<input type="radio" name="ManejoCaja" id="ManejoCaja" value="Si">
									</td>
									<td>
										<input type="radio" name="ManejoCaja" id="ManejoCaja" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Experiencia en Ventas
									</td>
									<td>
										<input type="radio" name="ManejoVentas" id="ManejoVentas" value="Si">
									</td>
									<td>
										<input type="radio" name="ManejoVentas" id="ManejoVentas" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Conocimientos básicos de computo
									</td>
									<td>
										<input type="radio" name="ConocimientoComputo" id="ConocimientoComputo" value="Si">
									</td>
									<td>
										<input type="radio" name="ConocimientoComputo" id="ConocimientoComputo" value="No">
									</td>
								</tr>
							</tbody>
						</table>
						<hr>
						<table class="table table-striped" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<th>Competencias</th>
									<th>Si</th>
									<th>No</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										Es capaz de comunicarse efectivamente y con Fluidez
									</td>
									<td>
										<input type="radio" name="CapacidadComunicar" id="CapacidadComunicar" value="Si">
									</td>
									<td>
										<input type="radio" name="CapacidadComunicar" id="CapacidadComunicar" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Muestra habilidad para ser buen líder
									</td>
									<td>
										<input type="radio" name="Lider" id="Lider" value="Si">
									</td>
									<td>
										<input type="radio" name="Lider" id="Lider" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Muestra ser una persona ordenada
									</td>
									<td>
										<input type="radio" name="Orden" id="Orden" value="Si">
									</td>
									<td>
										<input type="radio" name="Orden" id="Orden" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Muestra gran tolerancia a la tensión
									</td>
									<td>
										<input type="radio" name="Tolerancia" id="Tolerancia" value="Si">
									</td>
									<td>
										<input type="radio" name="Tolerancia" id="Tolerancia" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Muestra ser una persona positiva y entusiasta
									</td>
									<td>
										<input type="radio" name="Positivismo" id="Positivismo" value="Si">
									</td>
									<td>
										<input type="radio" name="Positivismo" id="Positivismo" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Muestra disciplina en sus actividades cotidianas
									</td>
									<td>
										<input type="radio" name="Disciplina" id="Disciplina" value="Si">
									</td>
									<td>
										<input type="radio" name="Disciplina" id="Disciplina" value="No">
									</td>
								</tr>
							</tbody>
						</table>
						<hr>
						<table class="table table-striped" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<th>Perfil acorde para el manejo de la tienda</th>
									<th>Si</th>
									<th>No</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										Su perfil demuestra que puede ser candidato potencial para ser Empresario
									</td>
									<td>
										<input type="radio" name="CandidatoPotencial" id="CandidatoPotencial" value="Si">
									</td>
									<td>
										<input type="radio" name="CandidatoPotencial" id="CandidatoPotencial" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Es una persona que cuida su aseo personal y su presentación personal
									</td>
									<td>
										<input type="radio" name="PersonaPulcra" id="PersonaPulcra" value="Si">
									</td>
									<td>
										<input type="radio" name="PersonaPulcra" id="PersonaPulcra" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Se muestra comprometido con esta propuesta de trabajo
									</td>
									<td>
										<input type="radio" name="Compromiso" id="Compromiso" value="Si">
									</td>
									<td>
										<input type="radio" name="Compromiso" id="Compromiso" value="No">
									</td>
								</tr>
								<tr>
									<td>
										El candidato conoce la responsabilidad de administrar y operar una tienda
									</td>
									<td>
										<input type="radio" name="Responsabilidad" id="Responsabilidad" value="Si">
									</td>
									<td>
										<input type="radio" name="Responsabilidad" id="Responsabilidad" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Tiene una idea clara y concisa de la administración de una tienda de conveniencia
									</td>
									<td>
										<input type="radio" name="IdeaClara" id="IdeaClara" value="Si">
									</td>
									<td>
										<input type="radio" name="IdeaClara" id="IdeaClara" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Su historial de trabajo es acorde a las necesidades de la operación de la tienda
									</td>
									<td>
										<input type="radio" name="HistorialTrabajo" id="HistorialTrabajo" value="Si">
									</td>
									<td>
										<input type="radio" name="HistorialTrabajo" id="HistorialTrabajo" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Su experiencia laboral muestra que es una persona estable
									</td>
									<td>
										<input type="radio" name="PersonaEstable" id="PersonaEstable" value="Si">
									</td>
									<td>
										<input type="radio" name="PersonaEstable" id="PersonaEstable" value="No">
									</td>
								</tr>
							</tbody>
						</table>
						<hr>
					</div>
				</div>	
				<div class="text-center">
					<button type="button" class="btn btn-primary" style="background-color: black; border-color: black;" id="guardar_ExperienciaEmpresario">Guardar</button>
				</div>
			</div>
		</div>
	</div>
</div>
