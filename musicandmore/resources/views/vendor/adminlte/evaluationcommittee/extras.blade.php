<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#_extras">V. Comentarios (Obligatorio)</a>
		</h4>
	</div>
	<div id="_extras" class="panel-collapse collapse">
		<div class="panel-body">
			<div class="col-md-12">
				<br>
				<p><b>Nota:</b> En caso de detectar algún riesgo, deberá registrarlo en este espacio.</p>
				<div class="form-group">
					<div class="col-md-12">
						<table class="table table-striped" style="margin-bottom: 0px;">
							<thead>
							</thead>
							<tbody class="text-left" id="tablaDocumentos">
								<tr>
									<th>Comisionista</th>
									<th><textarea class="form-control" id="Comisionista" name="Comisionista"></textarea></th>
								</tr>
								<tr>
									<th>Enc de Turno</th>
									<th><textarea class="form-control" id="EncTienda1" name="EncTienda1"></textarea></th>
								</tr>
								<tr>
									<th>Enc de Turno</th>
									<th><textarea class="form-control" id="EncTienda2" name="EncTienda2"></textarea></th>
								</tr>
								<tr>
									<th>Auxiliar de piso</th>
									<th><textarea class="form-control" id="AuxPiso1" name="AuxPiso1"></textarea></th>
								</tr>
								<tr>
									<th>Auxiliar de piso</th>
									<th><textarea class="form-control" id="AuxPiso2" name="AuxPiso2"></textarea></th>
								</tr>
								<tr>
									<th>Auxiliar de piso</th>
									<th><textarea class="form-control" id="AuxPiso3" name="AuxPiso3"></textarea></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="form-group text-center">
					<button type="button" class="btn btn-primary" id="guardar_extras">Guardar Perfil de Puesto</button>

					<hr>
						<form action="{{ route('tracking_interview') }}"  method="POST" role="form" >
						{{ csrf_field() }}
						<button type="submit" class="btn btn-primary" style="background-color: black; border-color: black;">Finalizar</button>				
						</form>	
				</div>
			</div>
		</div>
	</div>
</div>
