@extends('adminlte::layouts.app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<!-- Default box-->
			<div class="box">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1">
						<div class="row">
							<div class="col-md-10 col-xs-12 col-md-offset-1">
							<h1 class="text-center text-danger">Evaluación Entrevista Comité</h1>
								<div class="panel-group" id=accordion>
								<input type="hidden" id="id_evaluation_committee" name="id_evaluation_committee" value="">

									@include('adminlte::evaluationcommittee.general_interview')
									@include('adminlte::evaluationcommittee.committee')
									@include('adminlte::evaluationcommittee.business_experience')
									@include('adminlte::evaluationcommittee.experienceteam')
									@include('adminlte::evaluationcommittee.family_evaluation')
									@include('adminlte::evaluationcommittee.extras')
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--box-body-->
			</div>
			<!--.box-->
		</div>
	</div>
</div>
@endsection