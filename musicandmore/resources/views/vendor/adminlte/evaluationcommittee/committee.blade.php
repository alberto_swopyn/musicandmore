<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#_committee">I. Integrantes del Comité</a>
		</h4>
	</div>
	<div id="_committee" class="panel-collapse collapse">
		<div class="panel-body">
			<div class="form-group">
				<div class="col-md-10 col-md-offset-1">
					<table class="table table-striped" style="margin-bottom: 0px;">
						<thead>
							<tr class="text-center">
								<th class="text-center">
									Nombre
								</th>
								<th class="text-center">
									Área
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width: 60%">
									<input type="text" class="form-control" name="NombreGerenteRegional" id="NombreGerenteRegional">
								</td>
								<td>
									Gerente Regional
								</td>
							</tr>
							<tr>
								<td style="width: 60%">
									<input type="text" class="form-control" name="NombreRecursosHumanos" id="NombreRecursosHumanos">
								</td>
								<td>
									Recursos Humanos
								</td>
							</tr>
							<tr>
								<td style="width: 60%">
									<input type="text" class="form-control" name="NombreAsesorCaptacion" id="NombreAsesorCaptacion">
								</td>
								<td>
									Asesor Captación
								</td>
							</tr>
						</tbody>
					</table>
					
				</div>
			</div>
			<div class="text-center">
				<button type="button" class="btn btn-primary" style="background-color: black; border-color: black;" id="guardar_committee">Guardar Perfil de Puesto</button>
			</div>
		</div>
	</div>
</div>

