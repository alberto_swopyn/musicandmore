<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#_general_interview">Aspirante</a>
		</h4>
	</div>
	<div id="_general_interview" class="panel-collapse collapse in">
		<div class="panel-body">
			<div class="col-md-12 form-horizontal">
				<div class="row">
					<div class="form-group col-md-6">
						<label class="col-md-4 control-label">Apellido Paterno:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" placeholder="Martinez" name="EvaluadoApellidoPaterno" id="EvaluadoApellidoPaterno">
						</div>
					</div>
					<div class="form-group col-md-6">
						<label class="col-md-4 control-label">Apellido Materno:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" placeholder="Supervidor" name="EvaluadoApellidoMaterno" id="EvaluadoApellidoMaterno">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label class="col-md-4 control-label">Nombre:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" placeholder="Supervidor" name="EvaluadoNombre" id="EvaluadoNombre">
						</div>
					</div>
					<div class="form-group col-md-6">
						<label class="col-md-4 control-label">Procede:</label>
						<div class="col-md-8">
							<select name="EvaluacionProcede" id="EvaluacionProcede" class="form-control">
								<option value="">Seleccione uno</option>
								<option value="SI">SÍ</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="text-center">
				<button type="button" class="btn btn-primary" style="background-color: black; border-color: black;" id="guardar_general_interview">Guardar Perfil de Puesto</button>
			</div>
		</div>
	</div>
</div>
