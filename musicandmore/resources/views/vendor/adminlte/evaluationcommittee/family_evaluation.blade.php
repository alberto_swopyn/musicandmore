<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#EvaluacionFamilia">IV. Evaluación de la familia (equipo de trabajo) </a>
		</h4>
	</div>
	<div id="EvaluacionFamilia" class="panel-collapse collapse">
		<div class="panel-body">
			<div class="col-md-12">
				<br>
				<p> Seleccione según la experiencia, conocimiento o percepción de los siguientes puntos:</p>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-1">
						<table class="table table-striped" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<th>Aspecto</th>
									<th>Si</th>
									<th>No</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										Mostraron integración familiar
									</td>
									<td>
										<input type="radio" name="IntegracionFamiliar" id="IntegracionFamiliar" value="Si">
									</td>
									<td>
										<input type="radio" name="IntegracionFamiliar" id="IntegracionFamiliar" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Acudieron a la entrevista de manera puntual:
									</td>
									<td>
										<input type="radio" name="EntrevistaPuntual" id="EntrevistaPuntual" value="Si">
									</td>
									<td>
										<input type="radio" name="EntrevistaPuntual" id="EntrevistaPuntual" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Mostraron cuidado en su aseo y apariencia personal:
									</td>
									<td>
										<input type="radio" name="AseoPersonal" id="AseoPersonal" value="Si">
									</td>
									<td>
										<input type="radio" name="AseoPersonal" id="AseoPersonal" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Mostraron fluidez en su comunicación
									</td>
									<td>
										<input type="radio" name="FluidezComunicacion" id="FluidezComunicacion" value="Si">
									</td>
									<td>
										<input type="radio" name="FluidezComunicacion" id="FluidezComunicacion" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Sus valores son compatibles con los de la empresa
									</td>
									<td>
										<input type="radio" name="CompatibilidadValores" id="CompatibilidadValores" value="Si">
									</td>
									<td>
										<input type="radio" name="CompatibilidadValores" id="CompatibilidadValores" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Las ventas de la tienda cubren con sus gastos:
									</td>
									<td>
										<input type="radio" name="CubrirGastos" id="CubrirGastos" value="Si">
									</td>
									<td>
										<input type="radio" name="CubrirGastos" id="CubrirGastos" value="No">
									</td>
								</tr>
								<tr>
									<td>
										Sus actividades interfieren con la operación de la tienda
									</td>
									<td>
										<input type="radio" name="OperatividadTienda" id="OperatividadTienda" value="Si">
									</td>
									<td>
										<input type="radio" name="OperatividadTienda" id="OperatividadTienda" value="No">
									</td>
								</tr>
								<tr>
									<td>
										La plantilla conoce la responsabilidad de administrar y operar una tienda
									</td>
									<td>
										<input type="radio" name="ResponsabilidadTienda" id="ResponsabilidadTienda" value="Si">
									</td>
									<td>
										<input type="radio" name="ResponsabilidadTienda" id="ResponsabilidadTienda" value="No">
									</td>
								</tr>
								<tr>
									<td>
										La plantilla cuenta con el perfil para trabajar con el candidato
									</td>
									<td>
										<input type="radio" name="PerfilApto" id="PerfilApto" value="Si">
									</td>
									<td>
										<input type="radio" name="PerfilApto" id="PerfilApto" value="No">
									</td>
								</tr>
							</tbody>
						</table>
						<hr>
					</div>
				</div>
				<div class="form-group text-center">
					<button type="button" class="btn btn-primary" style="background-color: black; border-color: black;" id="guardar_family_evaluation">Guardar</button>
				</div>
			</div>
		</div>
	</div>
</div>
