<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#trabajo_equipo">III. Experiencia del Equipo de Trabajo</a>
		</h4>
	</div>
	<div id="trabajo_equipo" class="panel-collapse collapse">
		<div class="panel-body">
			<div class="col-md-12">
				<br>
				<p> Anexar Organigrama del equipo de trabajo y preguntar directamente a las personas según su puesto, la experiencia laboral que tienen. Marca un “Si” o un “No”</p>
				<div class="form-group">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-striped" style="margin-bottom: 0px;">
								<thead>
									<tr>
										<th>Experiencia</th>
										<th>Encargado de turno 1</th>
										<th>Encargado de turno 2</th>
										<th>Auxiliar de piso 1</th>
										<th>Auxiliar de piso 2</th>
										<th>Auxiliar de piso 3</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											Ventas
										</td>
										<td><select class="form-control" name="VentasEncargado1" id="VentasEncargado1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="VentasEncargado2" id="VentasEncargado2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="VentasAuxiliar1" id="VentasAuxiliar1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="VentasAuxiliar2" id="VentasAuxiliar2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="VentasAuxiliar3" id="VentasAuxiliar3">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
									</tr>
									<tr>
										<td>
											Inventarios
										</td>
										<td><select class="form-control" name="InventariosEncargado1" id="InventariosEncargado1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="InventariosEncargado2" id="InventariosEncargado2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="InventariosAuxiliar1" id="InventariosAuxiliar1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="InventariosAuxiliar2" id="InventariosAuxiliar2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="InventariosAuxiliar3" id="InventariosAuxiliar3">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
									</tr>
									<tr>
										<td>
											Servicio al cliente
										</td>
										<td><select class="form-control" name="ServicioClienteEncargado1" id="ServicioClienteEncargado1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="ServicioClienteEncargado2" id="ServicioClienteEncargado2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="ServicioClienteAuxiliar1" id="ServicioClienteAuxiliar1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="ServicioClienteAuxiliar2" id="ServicioClienteAuxiliar2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="ServicioClienteAuxiliar3" id="ServicioClienteAuxiliar3">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
									</tr>
									<tr>
										<td>
											Manejo de caja
										</td>
										<td><select class="form-control" name="ManejoCajaEncargado1" id="ManejoCajaEncargado1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="ManejoCajaEncargado2" id="ManejoCajaEncargado2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="ManejoCajaAuxiliar1" id="ManejoCajaAuxiliar1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="ManejoCajaAuxiliar2" id="ManejoCajaAuxiliar2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="ManejoCajaAuxiliar3" id="ManejoCajaAuxiliar3">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
									</tr>
									<tr>
										<td>
											Puesto que desempeño
										</td>
										<td><select class="form-control" name="PuestoDesempeñoEncargado1" id="PuestoDesempeñoEncargado1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="PuestoDesempeñoEncargado2" id="PuestoDesempeñoEncargado2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="PuestoDesempeñoAuxiliar1" id="PuestoDesempeñoAuxiliar1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="PuestoDesempeñoAuxiliar2" id="PuestoDesempeñoAuxiliar2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="PuestoDesempeñoAuxiliar3" id="PuestoDesempeñoAuxiliar3">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
									</tr>
									<tr>
										<td>
											Sueldo percibido
										</td>
										<td><select class="form-control" name="SueldoPercibidoEncargado1" id="SueldoPercibidoEncargado1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="SueldoPercibidoEncargado2" id="SueldoPercibidoEncargado2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="SueldoPercibidoAuxiliar1" id="SueldoPercibidoAuxiliar1">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="SueldoPercibidoAuxiliar2" id="SueldoPercibidoAuxiliar2">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
										<td><select class="form-control" name="SueldoPercibidoAuxiliar3" id="SueldoPercibidoAuxiliar3">
											<option value="No">No</option>
											<option value="Si">Si</option>
										</select></td>
									</tr>
								</tbody>
							</table>
						</div>
						<hr>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4 col-md-offset-1">
						¿Quién determino los roles para la tienda?
					</label>
					<div class="col-md-6">
						<input type="text" class="form-control" id="DeterminacionRoles" name="DeterminacionRoles">
					</div>
				</div>
				<div class="text-center">
					<button type="button" class="btn btn-primary" style="background-color: black; border-color: black;" id="guardar_ExperienciaEquipo">Guardar</button>
				</div>
			</div>
		</div>
	</div>
</div>