<div class="panel panel-primary" style="border-color: #bce8f1;">
	
	<!--DATOS PERSONALES-->
	<div class="panel panel-primary" style="border-color: #bce8f1;">
		<table class="table table-hover table table-striped">
			<thead style="background: #FF0080; color: white;">
				<tr>
					<th class="text-center">Datos personales</th>
				</tr>
			</thead>
		  </table>
		<div class="pull-right">
			<a href="javascript:void(0)" data-perform="panel-collapse">
				<i class="ti-minus"></i>
			</a>
			<a href="javascript:void(0)" data-perform="panel-dismiss">
				<i class="ti-close"></i>
			</a>
		</div>
	
		<div class="panel-body">
	
	
			<div class="form-group col-xs-6">
				<label for="Apellidos" style="font-size: 1.5em">Apellidos</label>
				<input type="text" class="form-control" id="Apellidos" name="Apellidos" placeholder="Apellidos"   style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" required="required" value="{{ $gpds->last_name or old('Apellidos') }}" >			
			</div>
	
			<div class="form-group col-xs-6"> 
				<label for="Nombre" style="font-size: 1.5em">Nombre(s)</label>
				<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Nombre(s)"   style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" required="required" value="{{ $gpds->first_name or old('Nombre') }}">
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Genero" style="font-size: 1.5em">Género</label>
				<br>
					<input type="radio" id="Genero" name="Genero" value="masculino" @if($gpds->gender == 'masculino')checked @endif> <label for="Genero" style="font-weight: bold; font-size: 1em">Masculino</label>
				
					&nbsp;&nbsp;&nbsp;
					<input type="radio" id="Genero" name="Genero" value="femenino" style="font-size: 1.5em" @if($gpds->gender == 'femenino')checked @endif> <label for="Genero" style="font-weight: bold; font-size: 1em">Femenino</label>
				
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Nacimiento" style="font-size: 1.5em">Fecha de nacimiento</label>
				<input type="date" class="form-control" name="Nacimiento" id="Nacimiento" min="1950-01-01" max="2000-12-31" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $gpds->birthday }}">
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Nombre" style="font-size: 1.5em">Lugar de nacimiento</label>
				<input type="text" class="form-control" id="LugarNacimiento" name="LugarNacimiento" placeholder="Ej. Ciudad de México" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $gpds->birth_place or old('LugarNacimiento') }}" >
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Genero" style="font-size: 1.5em">Número telefónico</label>
				<input type="text" class="form-control" id="Telefono_job" name="Telefono_job" placeholder="10 dígitos" maxlength="10" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $gpds->phone_number or old('Telefono_job') }}">
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Nacimiento" style="font-size: 1.5em">Número de celular</label>
				<input type="text" class="form-control" id="Celular" name="Celular" placeholder="10 dígitos" maxlength="10" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $gpds->cellphone or old('Celular') }}">
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Nombre" style="font-size: 1.5em">E-mail</label>
				<input type="email" id="Email" name="Email" class="form-control" placeholder="ejemplo@ejemplo.com" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $gpds->email or old('Email') }}">
			</div>
			
			<div class="form-group col-xs-4">
				<label for="Beca" style="font-size: 1.5em">Beca (porcentaje)</label>
				<input type="text" id="Beca" name="Beca" class="form-control" placeholder="50" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $gpds->schoolarship or old('Beca') }}" required="required">
			</div>
		</div>
	
	
	</div>
	
	<!--DOMICILIO-->
	
	<div class="panel panel-primary" style="border-color: #bce8f1;">
		<table class="table table-hover table table-striped">
			<thead style="background: #00AB7F; color: white;">
				<tr>
					<th class="text-center">Domicilio</th>
				</tr>
			</thead>
		  </table>
		<div class="pull-right">
			<a href="javascript:void(0)" data-perform="panel-collapse">
				<i class="ti-minus"></i>
			</a>
			<a href="javascript:void(0)" data-perform="panel-dismiss">
				<i class="ti-close"></i>
			</a>
		</div>
	
		<div class="panel-body">
	
	
			<div class="form-group col-xs-6">
				<label for="Calle" style="font-size: 1.5em">Calle</label>
				<input type="text" id="Calle" name="Calle" class="form-control" placeholder="Ej. Av. de la Convención de 1914"maxlength="30" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $pas -> street or old ('Calle')}}">			
			</div>
	
			<div class="form-group col-xs-3">
				<label for="Numero_exterior" style="font-size: 1.5em">Número exterior</label>
				<input type="number" id="NumeroExterior" name="NumeroExterior" class="form-control" placeholder="Número Exterior" min="1" maxlength="10" style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" value="{{ $pas -> ext_number or old ('NumeroExterior')}}">
			</div>
	
			<div class="form-group col-xs-3">
				<label for="Numero_interior" style="font-size: 1.5em">Número interior</label>
				<input type="text" id="NumeroInterior" name="NumeroInterior" class="form-control" placeholder="Número Interior" min="1" style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" value="{{ $pas -> int_number or old ('NumeroInterior')}}">
			</div>
	
			<div class="form-group col-xs-6">
				<label for="Fraccionamiento" style="font-size: 1.5em">Fraccionamiento</label>
				<br>
				<input type="text" id="Fraccionamiento" name="Fraccionamiento" class="form-control" placeholder="Ej. Zona Centro" maxlength="50" style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" value="{{ $pas -> fraccionamiento or old ('Fraccionamiento')}}">
			</div>
	
			<div class="form-group col-xs-6">
				<label for="Codigo_postal" style="font-size: 1.5em">Código postal</label>
				<input type="text" id="CodigoPostal" name="CodigoPostal" class="form-control" placeholder="Ej. 20000" maxlength="5" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $pas -> zip_code or old ('CodigoPostal')}}">
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Ciudad" style="font-size: 1.5em">Ciudad</label>
				<input type="text" id="Ciudad" name="Ciudad" class="form-control" placeholder="Aguascalientes"  style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $pas -> city or old ('Ciudad')}}">
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Estado" style="font-size: 1.5em">Estado</label>
				<input type="text" id="Estado" name="Estado" class="form-control" placeholder="Aguascalientes"  style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $pas -> state or old ('Estado')}}">
			</div>
		</div>
	</div>
	
	
	<!-- INFORMACION FAMILIAR -->
	
	<div class="panel panel-primary" style="border-color: #bce8f1;">
		<table class="table table-hover table table-striped">
			<thead style="background: #3400AB; color: white;">
				<tr>
					<th class="text-center">Información del padre o tutor</th>
				</tr>
			</thead>
		  </table>
		<div class="pull-right">
			<a href="javascript:void(0)" data-perform="panel-collapse">
				<i class="ti-minus"></i>
			</a>
			<a href="javascript:void(0)" data-perform="panel-dismiss">
				<i class="ti-close"></i>
			</a>
		</div>
	
		<div class="panel-body">
	
	
			<div class="form-group col-xs-6">
				<label for="nombre_completo" style="font-size: 1.5em">Nombre completo</label>
				<input type="text" id="NombreFamiliar" name="NombreFamiliar" class="form-control" placeholder="Nombre completo del familiar" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $prs -> name or old ('NombreFamiliar')}}">			
			</div>
	
			<div class="form-group col-xs-6">
				<label for="Parentesco" style="font-size: 1.5em">Parentesco</label>
				<input type="text" class="form-control" id="Parentesco" name="Parentesco" placeholder="Cónyuge, Padre, Madre, etc. " style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" value="{{ $prs -> relationship or old ('Parentesco')}}">
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Ocupacion" style="font-size: 1.5em">Ocupación</label>
				<input type="text" id="Ocupacion" name="Ocupacion" class="form-control" style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" value="{{ $prs -> occupation or old ('Ocupacion')}}">
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Vivir" style="font-size: 1.5em">Vive con usted</label>
				<br>
				<input type="radio" name="ViveConUsted" id="ViveConUsted" value="1"> <label for="ViveConUsted" style="font-weight: bold; font-size: 1em" @if($prs->meet_time == 1)checked @endif>Si</label> &nbsp;&nbsp;&nbsp;
				
				<input type="radio" name="ViveConUsted" id="ViveConUsted" value="0" style="font-size: 1.5em"> <label for="ViveConUsted" style="font-weight: bold; font-size: 1em" @if($prs->meet_time == 0)checked @endif>No</label>
			</div>
	
			<div class="form-group col-xs-4">
				<label for="Numero_telefonico" style="font-size: 1.5em">Número telefónico</label>
				<input type="text" id="NumeroFamiliar" name="NumeroFamiliar" class="form-control" placeholder="10 digitos" maxlength="10" style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" value="{{ $prs -> phone_number or old ('NumeroFamiliar') }}">
			</div>
			<br>
			<br>
			<br>
		</div>
	</div>
	
	<div class="col-md-12 text-center">
		<button type="submit" class="btn btn-primary" style="">Guardar</button>
	</div>
	
	</div>
	
	<script>
		  $(document).ready(function(){
			  $('#alumnos').select2();
		   });
	</script>