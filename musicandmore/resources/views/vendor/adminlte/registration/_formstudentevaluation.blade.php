<div class="panel panel-primary" style="border-color: #bce8f1;">
	<table class="table table-hover table table-striped">
		<thead style="background: #FF0080; color: white;">
			<tr>
				<th class="text-center">Datos personales</th>
			</tr>
	    </thead>
	  </table>
	<div class="pull-right">
		<a href="javascript:void(0)" data-perform="panel-collapse">
			<i class="ti-minus"></i>
		</a>
		<a href="javascript:void(0)" data-perform="panel-dismiss">
			<i class="ti-close"></i>
		</a>
	</div>

	<div class="panel-body">
		<div class="form-group col-xs-6">
			<label style="font-size: 1.5em">¿Cómo se enteró de la academia?</label>
			<select style="border: 1px solid; font-weight: bold; font-size: 1.5em; border-radius: 7px" id="Otros" name="Otros[]" class="form-control" data-live-search="true" required="true">
					<option value="">Seleccione una opción</option>
					<option value="Redes sociales">Redes sociales</option>
				    <option value="Volantes">Volantes</option>
				    <option value="Publicidad en internet">Publicidad en internet</option>
				    <option value="Recomendacion">Recomendación</option>
				    <option value="E-mail">E-mail</option>
				    <option value="Periodico o revista">Periodico o revista</option>
				    <option value="Otro">Otro</option>
			</select>
			<br>
		</div>
		<div class="form-group col-xs-12">
			<label style="font-size: 1.5em">¿Cuál crees que sería tu mayor meta a lograr, musicalmente hablando?</label>
			<input type="text" id="Q2" name="Q2" class="form-control" maxlength="1000" required placeholder="Escribe tu respuesta" style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" value="{{ $question->pregunta2 or old('Apellidos') }}">
		</div>
		<div class="form-group col-xs-12">
			<label style="font-size: 1.5em">¿Cómo te gustaría o pudiéramos ayudarte con esa meta?</label>
			<input type="text" id="Q3" name="Q3" class="form-control" maxlength="1000" required  placeholder="Escribe tu respuesta" style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" value="{{ $question->pregunta3 or old('Apellidos') }}">
			
		</div>
		<div class="form-group col-xs-12">
			<label style="font-size: 1.5em">¿Hay algún otro producto o servicio similar al nuestro que estés utilizando y del que quieras platicarnos?</label>
			<input type="text" id="Q4" name="Q4" class="form-control" maxlength="1000" required style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" placeholder="Escribe tu respuesta" value="{{ $question->pregunta4 or old('Apellidos') }}">			
		</div>
		<div class="form-group col-xs-12">
			<label style="font-size: 1.5em">¿En qué lugares, negocios, plazas o centros comerciales te gusta pasar el tiempo? </label>
			<input type="text" id="Q5" name="Q5" class="form-control" maxlength="1000" required style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" placeholder="Escribe tu respuesta" value="{{ $question->pregunta5 or old('Apellidos') }}">			
		</div>
		<div class="form-group col-xs-12">
			<label style="font-size: 1.5em">¿Qué redes sociales y aplicaciones utilizas más?</label>
			<input type="text" id="Q6" name="Q6" class="form-control" maxlength="1000" required style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" placeholder="Escribe tu respuesta" value="{{ $question->pregunta6 or old('Apellidos') }}">
		</div>
		<div class="form-group col-xs-12">
			<label style="font-size: 1.5em">Aparte de la música ¿tienes otros hobbies? </label>
			<input type="text" id="Q7" name="Q7" class="form-control" maxlength="1000" required style="border: 1px solid; ; font-size: 1.5em; border-radius: 7px" placeholder="Escribe tu respuesta" value="{{ $question->pregunta7 or old('Apellidos') }}">
		</div>
	</div>
</div>

<div class="col-md-12 text-center">
	<button type="submit" class="btn btn-primary" style="">Finalizar cuestionario</button>
</div>

