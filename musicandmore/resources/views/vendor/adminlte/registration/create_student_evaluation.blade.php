@extends('adminlte::layouts.app')
@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection
@section('personal_style')
@stop
@include('adminlte::school_control.PersonalSelect2')
@section('main-content')
<div class="container-fluid spark-screen">
  <div class="row">
    <div class="col-md-12">
      <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">{{ $users->name }}</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <form method="POST" enctype="multipart/form-data" action="{{ Route('add_evaluation', ['student' => $users->id]) }}" role="form">
              {{ csrf_field() }}
              @include('adminlte::registration._formstudentevaluation')
            </form>
        </div>
      <!-- /.box -->
    </div>
  </div>
</div>
<script>
      $(document).ready(function(){
          $('#subjects').select2();
          $('#cathedras').select2();
       });
    </script>
@endsection