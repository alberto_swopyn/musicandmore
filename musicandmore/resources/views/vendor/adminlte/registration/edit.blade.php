@extends('adminlte::layouts.app')

@section('htmlheader_title')
  {{ trans('adminlte_lang::message.home') }}
@endsection
@include('adminlte::school_control.PersonalSelect2')
@section('personal_style')
@stop
@section('main-content')
<div class="container-fluid spark-screen">
  <div class="row">
    <div class="col-md-12">
      <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Registro de alumnos</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
              </button>
            </div>
          </div>        

          <div class="box-body">
            <form method="POST" enctype="multipart/form-data" action="{{ Route('student_up', ['student' => $users->id]) }}" role="form">
              {{ method_field('PUT') }}
              {{ csrf_field() }}
              @include('adminlte::registration._formstudentedit')
            </form>
        </div>
      <!-- /.box -->
    </div>
  </div>
</div>
@endsection