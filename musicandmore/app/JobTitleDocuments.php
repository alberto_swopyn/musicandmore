<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleDocuments extends Model
{
    protected $table = "job_title_documents";
    protected $primarykey = "id";
    protected $fillable = ['id', 'job_title_documents_id', 'job_title_profiles_id','document_name'];


    // public function tracking(){

    //     return $this->hasMany(Tracking::class, 'fk_doc', 'job_title_profiles_id');
    // }
 
}
