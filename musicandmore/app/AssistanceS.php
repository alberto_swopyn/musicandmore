<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AssistanceS extends Model
{
    protected $table      = 'assistance_student';
	protected $primarykey = 'id';
	protected $fillable   = ['id','id_group','id_teacher', 'id_student' , 'assistance','date'];
}
