<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleSoftware extends Model
{
	protected $table = "job_title_softwares";
    protected $primarykey = "job_title_softwares_id";
    protected $fillable = ['id', 'job_title_softwares_id', 'job_title_profiles_id','software_name'];
}
