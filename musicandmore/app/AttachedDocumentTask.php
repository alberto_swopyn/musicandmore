<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AttachedDocumentTask extends Model
{
    protected $table = "attached_document_tasks";
    protected $primarykey = "id";
    protected $fillable = ["attached_document_tasks_id", "attached_documents_id", "tasks_id"];
}
