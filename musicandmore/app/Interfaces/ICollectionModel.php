<?php

namespace Swopyn\Interfaces;

use Swopyn\Interfaces\IModel;

interface ICollectionModel extends IModel
{
    public function toJson();
}