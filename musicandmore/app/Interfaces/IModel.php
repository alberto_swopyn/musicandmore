<?php

namespace Swopyn\Interfaces;

use Google\Protobuf\Internal\Message;




/**
 * Delcaracion de la interfaz de los Modelos.
 */
interface IModel
{
    /**
     * Tranforma el Modelo a DTO.
     *
     * @return Message
     */
    public function ToDTO();

    /**
     * Crea el modelo a partir de un DTO.
     *
     * @param Message $dto
     * @return Model
     */
    public static function FromDTO(Message $dto);
}
