<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleMachine extends Model
{
    protected $table = "job_title_machines";
    protected $primarykey = "job_title_machines_id";
    protected $fillable = ['id', 'job_title_machines_id', 'job_title_profiles_id','machine_name'];
}
