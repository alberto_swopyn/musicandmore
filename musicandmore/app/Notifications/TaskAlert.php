<?php

namespace Swopyn\Notifications;

use Carbon\Carbon;
use Swopyn\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use DB;

class TaskAlert extends Notification
{
    use Queueable;
    private $task;
    private $user;
    private $assign;
   

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($task, $user, $assign)
    {
        $this->task = $task;
        $this->user = $user;
        $this->assign = $assign;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
         return ['database'];
    }


    public function toDatabase($notifiable)
    {
        if ($this->user == null)
        {
            return[
                'tasks' => $this->task,
                'user' => auth()->user(),
                'assing' => $this->assign];
        }

        return[
             'tasks' => $this->task,
             'user' => $this->user,
             'assing' => $this->assign];
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            ];
    }
}
