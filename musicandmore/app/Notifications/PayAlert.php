<?php

namespace Swopyn\Notifications;

use Carbon\Carbon;
use Swopyn\Pay;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use DB;

class PayAlert extends Notification
{
    use Queueable;
    private $pay;
    private $user;
    private $asignacion;
   

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($pay, $user, $asignacion)
    {
        $this->pay = $pay;
        $this->user = $user;
        $this->asignacion = $asignacion;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
         return ['database'];
    }


    public function toDatabase($notifiable)
    {
        if ($this->user == null)
        {
            return[
                'pays' => $this->pay,
                'user' => auth()->user(),
                'assing' => $this->asignacion];
        }

        return[
             'pays' => $this->pay,
             'user' => $this->user,
             'assing' => $this->asignacion];
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            ];
    }
}
