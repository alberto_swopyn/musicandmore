<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class UserAttachedFormAnswer extends Model
{
    protected $table = "user_attached_form_answers";
    protected $primarykey = "id";
    protected $fillable = ['user_attached_form_answers_id','assignments_id', 'attached_form_answers_id'];
}
