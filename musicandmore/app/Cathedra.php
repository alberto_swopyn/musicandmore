<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Cathedra extends Model
{
    protected $table      = "cathedras";
    protected $primarykey = "id";
    protected $fillable   = ['subject_id', 'employee_id', 'hour_payment'];
}
