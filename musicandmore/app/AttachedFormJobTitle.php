<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AttachedFormJobTitle extends Model
{
    protected $table = "attached_form_job_titles";
    protected $primarykey = "id";
    protected $fillable = ["attached_form_job_titles_id", "job_titles_id", "attached_forms_id"];
}
