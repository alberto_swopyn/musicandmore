<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class EmployeeJobCenter extends Model
{
    protected $table      = 'employee_tree_job_center';
    protected $primarykey = "id";
    protected $fillable   = ['employee_id', 'tree_job_center_id'];

     //Nombre de la tabla
     const TableName = 'employee_tree_job_center';

     //Nombres de los campos de la tabla
     const Id          = "id";
     const EmployeeId      = 'employee_id';
     const JobCenterId = 'tree_job_center_id';
}
