<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AttachedFormTask extends Model
{
    protected $table = "attached_form_tasks";
    protected $primarykey = "id";
    protected $fillable = ["attached_form_tasks_id", "attached_forms_id", "tasks_id"];
}
