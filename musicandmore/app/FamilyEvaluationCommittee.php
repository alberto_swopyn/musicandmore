<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class FamilyEvaluationCommittee extends Model
{
    protected $table = "family_evaluation_committees";
    protected $primarykey = "family_evaluation_committees_id";
    protected $fillable = ['id', 'family_evaluation_committees_id', 'evaluation_committees_id','family_integration', 'puntuality', 'personal_cleanliness', 'family_communication', 'supported_values', 'expenses', 'operative_vs_activity', 'store_responsability', 'candidate_integration'];
}
