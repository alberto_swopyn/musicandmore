<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
   	protected $table      = 'groups';
	protected $primarykey = 'id';
	protected $fillable   = ['id','name','classroom', 'id_cathedra' , 'quota', 'monday', 'tuesday', 'wednesday','thursday','friday', 'saturday', 'sunday', 'current_students', 'schedule', 'final_hour', 'dow', 'ranges', 'initial_date', 'final_date', 'color'];

	public function groups_alumns()
    {
        return $this->hasMany(GroupAlums::class);
    }
}
