<?php

namespace Swopyn;

use DTO\Tasks\TaskDTO;
use Google\Protobuf\Internal\Message;
use Illuminate\Database\Eloquent\Model;
use Swopyn\Interfaces\IModel;

class Task extends Model implements IModel
{
    protected $table      = "tasks";
    protected $primarykey = "id";
    protected $fillable   = ['tasks_id', 'companies_id', 'users_id', 'description', 'initial_date', 'final_date', 'comment'];

    //Nombre de la tabla
    const TableName = 'tasks';

    //Nombres de los campos de la tabla
    const Id          = "id";
    const TaskId      = 'tasks_id';
    const CompaniesId = 'companies_id';
    const UsersId     = 'users_id';
    const Description = 'description';
    const InitialDate = 'initial_date';
    const FinalDate   = 'final_date';
    const Comment     = 'comment';
    const Title       = 'title';
    const Objectives  = 'objective';

    //Alias
    const AliasTable       = "Tasks";
    const AliasId          = self::AliasTable . "Id";
    const AliasTaskId      = self::AliasTable . 'TasksId';
    const AliasCompaniesId = self::AliasTable . 'CompaniesId';
    const AliasUsersId     = self::AliasTable . 'UsersId';
    const AliasDescription = self::AliasTable . 'Description';
    const AliasInitialDate = self::AliasTable . 'InitialDate';
    const AliasFinalDate   = self::AliasTable . 'FinalDate';
    const AliasComment     = self::AliasTable . 'Comment';
    const AliasCreatedAt   = self::AliasTable . 'CreatedAt';
    const AliasUpdateAt    = self::AliasTable . 'UpdateAt';
    const AliasTitle       = self::AliasTable . 'Title';
    const AliasObjectives  = self::AliasTable . 'Objective';

    //AliasSelect
    const AliasPrompId          = self::TableName . '.' . self::Id;
    const AliasPrompTaskId      = self::TableName . '.' . self::TaskId;
    const AliasPrompUsersId     = self::TableName . '.' . self::UsersId;
    const AliasPrompCompaniesId = self::TableName . '.' . self::CompaniesId;
    const AliasPrompInitialDate = self::TableName . '.' . self::InitialDate;
    const AliasPrompFinalDate   = self::TableName . '.' . self::FinalDate;
    const AliasPrompComment     = self::TableName . '.' . self::Comment;
    const AliasPrompDescription = self::TableName . '.' . self::Description;
    const AliasPrompCreatedAt   = self::TableName . '.' . self::CREATED_AT;
    const AliasPrompUpdateAt    = self::TableName . '.' . self::UPDATED_AT;
    const AliasPrompTitle       = self::TableName . '.' . self::Title;
    const AliasPrompObjectives  = self::TableName . '.' . self::Objectives;

    //AliasQuery
    const AliasQueryTable       = self::TableName . '.*';
    const AliasQueryId          = self::AliasPrompId . ' AS ' . self::AliasId;
    const AliasQueryTaskId      = self::AliasPrompTaskId . ' AS ' . self::AliasTaskId;
    const AliasQueryUsersId     = self::AliasPrompUsersId . ' AS ' . self::AliasUsersId;
    const AliasQueryCompaniesId = self::AliasPrompCompaniesId . ' AS ' . self::AliasCompaniesId;
    const AliasQueryInitialDate = self::AliasPrompInitialDate . ' AS ' . self::AliasInitialDate;
    const AliasQueryFinalDate   = self::AliasPrompFinalDate . ' AS ' . self::AliasFinalDate;
    const AliasQueryComment     = self::AliasPrompComment . ' AS ' . self::AliasComment;
    const AliasQueryDescription = self::AliasPrompDescription . ' AS ' . self::AliasDescription;
    const AliasQueryCreatedAt   = self::AliasPrompCreatedAt . ' AS ' . self::AliasCreatedAt;
    const AliasQueryUpdateAt    = self::AliasPrompUpdateAt . ' AS ' . self::AliasUpdateAt;
    const AliasQueryTitle       = self::AliasPrompTitle . ' AS ' . self::AliasTitle;
    const AliasQueryObjectives  = self::AliasPrompObjectives . ' AS ' . self::AliasObjectives;

    /**
     * Transforma un Task a TaskDTO.
     *
     * @return TaskDTO
     */
    public function ToDTO()
    {
        $taskDTO = new TaskDTO();
        $taskDTO->mergeFromJsonString(json_encode($this->toArray()));
        return $taskDTO;
    }

    /**
     * Crea una nueva Task a partir de un TaskDTO
     *
     * @param Message $taskDTO
     * @return Task
     */
    public static function FromDTO(Message $taskDTO)
    {
        $taskModel               = new Task();
        $taskModel->id           = $taskDTO->getId();
        $taskModel->companies_id = $taskDTO->getCompaniesId();
        $taskModel->users_id     = $taskDTO->getUsersId();
        $taskModel->title        = $taskDTO->getTitle();
        $taskModel->objective    = $taskDTO->getObjective();
        $taskModel->description  = $taskDTO->getDescription();
        $taskModel->initial_date = $taskDTO->getInitialDate();
        $taskModel->final_date   = $taskDTO->getFinalDate();
        $taskModel->comment      = $taskDTO->getComment();

        return $taskModel;
    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function taskdocument() 
    { 
 
        return $this->hasOne(TaskDocument::class); 
    } 
}
