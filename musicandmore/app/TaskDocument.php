<?php 
 
namespace Swopyn; 
 
use Illuminate\Database\Eloquent\Model; 
 
class TaskDocument extends Model 
{ 
    protected $table      = "task_documents"; 
    protected $primarykey = "id"; 
    protected $fillable   = ['name', 'task_id']; 
 
    public function task() 
    { 
        return $this->belongsTo(Task::class); 
    } 
} 