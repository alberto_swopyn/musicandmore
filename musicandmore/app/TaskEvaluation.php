<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class TaskEvaluation extends Model
{
    protected $table     = "task_evaluations";
    protected $protected = "id";
    protected $fillable  = ['tasks_id', 'question', 'type', 'value'];

}
