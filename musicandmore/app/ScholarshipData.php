<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class ScholarshipData extends Model
{
    protected $table = "scholarship_datas";
    protected $primarykey = "scholarship_datas_id";
   	protected $fillable = ['id', 'scholarship_datas_id', 'degree_name', 'education_area', 'academic_grade', 'school_name', 'school_address', 'initial_period', 'final_period', 'degree_certificates'];
}
