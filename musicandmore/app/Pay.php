<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Pay extends Model
{
    protected $table      = "pays";
    protected $primarykey = "id";
    protected $fillable   = ['id_course', 'id_teacher', 'users_id', 'description', 'limit_date', 'quantity', 'comment','discount','title','id','type','aument','habilitar','habilitar_s'];
}
