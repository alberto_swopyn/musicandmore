<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AttachedDocument extends Model
{
    protected $table = "attached_documents";
    protected $primarykey = "id";
    protected $fillable = ['attached_documents_id', 'name', 'document', 'type', 'comment'];
}
