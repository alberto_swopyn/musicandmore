<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AnswersPsychometricTest extends Model
{
    //
    protected $table = "answers_psychometric_tests";
    protected $primarykey = "id";
    protected $fillable = ['id','personal_id',
    'answer_1_1','answer_1_2',
    'answer_2_1','answer_2_2',
    'answer_3_1','answer_3_2',
    'answer_4_1','answer_4_2',
    'answer_5_1','answer_5_2',
    'answer_6_1','answer_6_2',
    'answer_7_1','answer_7_2',
    'answer_8_1','answer_8_2',
    'answer_9_1','answer_9_2',
    'answer_10_1','answer_10_2',
    'answer_11_1','answer_11_2',
    'answer_12_1','answer_12_2',
    'answer_13_1','answer_13_2',
    'answer_14_1','answer_14_2',
    'answer_15_1','answer_15_2',
    'answer_16_1','answer_16_2',
    'answer_17_1','answer_17_2',
    'answer_18_1','answer_18_2',
    'answer_19_1','answer_19_2',
    'answer_20_1','answer_20_2',
    'answer_21_1','answer_21_2',
    'answer_22_1','answer_22_2',
    'answer_23_1','answer_23_2',
    'answer_24_1','answer_24_2',
    'answer_25_1','answer_25_2',
    'answer_26_1','answer_26_2',
    'answer_27_1','answer_27_2',
    'answer_28_1','answer_28_2',
    'answer_29_1','answer_29_2',
    'answer_30_1','answer_30_2',
    'answer_31_1','answer_31_2',
    'answer_32_1','answer_32_2',
    'answer_33_1','answer_33_2',
    'answer_34_1','answer_34_2',
    'answer_35_1','answer_35_2',
    'answer_36_1','answer_36_2',
    'answer_37_1','answer_37_2',
    'answer_38_1','answer_38_2',
    'answer_39_1','answer_39_2',
    'answer_40_1','answer_40_2',
    'answer_41_1','answer_41_2',
    'answer_42_1','answer_42_2',
    'answer_43_1','answer_43_2',
    'answer_44_1','answer_44_2',
    'answer_45_1','answer_45_2',
    'answer_46_1','answer_46_2',
    'answer_47_1','answer_47_2',
    'answer_48_1','answer_48_2',
    'answer_49_1','answer_49_2',
    'answer_50_1','answer_50_2',
    'answer_51_1','answer_51_2',
    'answer_52_1','answer_52_2',
    'answer_53_1','answer_53_2'
    ]; 
}
