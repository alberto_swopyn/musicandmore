<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table      = 'employees';
	protected $primarykey = 'id';
	protected $fillable   = ['employees_id', 'key_employee' , 'name', 'id_company', 'job_title_profile_id', 'profile_job_center_id','course_percentage'];

	public function jobCenter()
    {
        return $this->belongsToMany(TreeJobCenter::class);
    }
}
