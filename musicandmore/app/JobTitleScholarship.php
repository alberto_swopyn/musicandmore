<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleScholarship extends Model
{
    protected $table = "job_title_scholarship";
    protected $primarykey = "job_title_scholarship_id";
    protected $fillable = ['id', 'job_title_scholarship_id', 'job_title_profiles_id','academic_grade','degree_name'];
}
