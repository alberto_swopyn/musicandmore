<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Assitance extends Model
{
    protected $table      = 'assistances';
	protected $primarykey = 'id';
	protected $fillable   = ['id','id_group','id_teacher', 'evidence' , 'num_assistance', 'date'];
}
