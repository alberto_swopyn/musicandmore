<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class PsychometricTest extends Model
{
    protected $table = "psychometric_tests";
    protected $primarykey = 'id';
    protected $fillable = ['id','image','result1','result2'];

    
}
