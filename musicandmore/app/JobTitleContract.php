<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleContract extends Model
{
    protected $table = "job_title_contracts";
    protected $primarykey = "job_title_contracts_id";
    protected $fillable = ['id', 'job_title_contracts_id', 'job_title_profiles_id','contract_types_id'];
}
