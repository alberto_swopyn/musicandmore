<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AnswerUser extends Model
{
    protected $table = 'answer_users';
    protected $primarykey = 'id';
    protected $fillable = ['answer_users_id', 'users_id', 'sections_id', 'questions_id', 'value'];

    const Table = 'answer_users';
    const Id = 'id';
    const AnswerUserId = 'answer_users_id';
    const UsersId = 'users_id';
    const AnswersId = 'sections_id';
    const QuestionsId = 'questions_id';
    const Value = 'value';
}
