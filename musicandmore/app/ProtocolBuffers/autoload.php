<?php

/**
 * Path general.
 * @path ProtocolBuffers/
 */
$path = app_path()."/ProtocolBuffers//"; 

/**
 * Path delos DTO generados.
 * @path ProtocolBuffers/Build/
 */
$pathBuild = $path."Build/"; 

$pathControllers = $path."Controllers/"; 
$pathUtilities = $path."Utilities/";

// ProtocolBuffers/Build/DTO
$pathBuildDTO = $pathBuild."DTO/";
// ProtocolBuffers/Build/DTO/Web/
$pathBuildWeb = $pathBuildDTO."Web/";
// ProtocolBuffers/Build/DTO/Tasks/
$pathBuildTasks = $pathBuildDTO."Tasks/";


/**
 * Path de los metadatos generados.
 * @path ProtocolBuffers/Build/GPBMetadata/Schemas/
 */
$pathBuildGPBMetadataSchemas = $pathBuild . "GPBMetadata/Schemas/";

// ProtocolBuffers/Build/GPBMetadata/Schemas/Web/
$pathBuildGPBMetadataSchemasWeb = $pathBuildGPBMetadataSchemas . "Web/";

// ProtocolBuffers/Build/GPBMetadata/Schemas/Tasks/
$pathBuildGPBMetadataSchemasTasks = $pathBuildGPBMetadataSchemas . "Tasks/";

//Controller
include_once($pathControllers."DTOWebResponseUtilities.php");

//Utilities
include_once($pathUtilities."DTOUtilities.php");

//DTO
include_once($pathBuildDTO."UserDTO.php");
include_once($pathBuildGPBMetadataSchemas. "UserDTO.php");

//DTO.Web
include_once($pathBuildWeb."WebResponseDTO.php");
include_once($pathBuildGPBMetadataSchemasWeb."WebResponseDTO.php");
include_once($pathBuildWeb."CodeResponseDTO.php");
include_once($pathBuildGPBMetadataSchemasWeb."CodeResponseDTO.php");
include_once($pathBuildWeb."LoginResponseDTO.php");
include_once($pathBuildGPBMetadataSchemasWeb."LoginResponseDTO.php");

//DTO.Tasks
include_once($pathBuildTasks."TaskDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."TaskDTO.php");
include_once($pathBuildTasks."TasksDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."TasksDTO.php");
include_once($pathBuildTasks."AssignmentsDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."AssignmentsDTO.php");
include_once($pathBuildTasks."AssignmentDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."AssignmentDTO.php");
include_once($pathBuildTasks."CategoriesDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."CategoriesDTO.php");
include_once($pathBuildTasks."TaskUsersDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."TaskUsersDTO.php");
include_once($pathBuildTasks."AssignmentEvidenceDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."AssignmentEvidenceDTO.php");
include_once($pathBuildTasks."TasksStatusDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."TasksStatusDTO.php");
include_once($pathBuildTasks."AssignmentMessageDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."AssignmentMessageDTO.php");
include_once($pathBuildTasks."AssignmentMessagesDTO.php");
include_once($pathBuildGPBMetadataSchemasTasks."AssignmentMessagesDTO.php");
