<?php

namespace Swopyn\ProtocolBuffers\Utilities;

use Google\Protobuf\Internal\Message;


class DTOUtilities
{
    /**
     * Deserializa el DTO dependiendo de el protocolo de peticion.
     *
     * @param object $dto DTO a deserializar.
     */
    public static /* dto */ function DeserialiceDTO(Message $dto)
    {
        $objString = file_get_contents('php://input');
        if (isset($_SERVER["CONTENT_TYPE"]) && $_SERVER["CONTENT_TYPE"] == 'application/x-protobuf') {
            $dto->mergeFromString($objString);
        } else {
            $dto->mergeFromJsonString($objString);
        }

        return $dto;
    }

    /**
     * Deserializa el DTO dependiendo de el protocolo de peticion.
     *
     * @param object $dto DTO a deserializar.
     * @param array $data Array con la informacion a parsear.
     * @return object DTO con la la información del array.
     */
    public static function SerialiceArrayToDTO(Message $dto, array $data) 
    {
        try
        {
            $dto->mergeFromJsonString(json_encode($data));
        }
        catch(\Exception $ex)
        {
            //DTO vacío
            $dto->mergeFromJsonString("{}");
        }
        
        return $dto;
    }

    public static function SerialiceJSONToDTO(Message $dto, $data) 
    {
        try
        {
            $dto->mergeFromJsonString($data);
        }
        catch(\Exception $ex)
        {
            //DTO vacío
            $dto->mergeFromJsonString("{}");
        }
        
        return $dto;
    }
}