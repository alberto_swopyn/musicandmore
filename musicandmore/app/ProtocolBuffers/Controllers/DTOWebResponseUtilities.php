<?php 

namespace Swopyn\ProtocolBuffers\Controllers;

use DTO\Web\WebResponseDTO;
use Google\Protobuf\Internal\Message;
use Illuminate\Database\Eloquent\Model;
use Swopyn\Interfaces\IModel;
use DTO\Web\CodeResponseDTO;

/**
 * Manipula las respuestas del servidor.
 */
class DTOWebResponseController
{
    /**
     * Muestra una respuesta http con un mensaje.
     *
     * @param int $code Codigo de peticion.
     * @param string $message Mensaje a mostrar.
     */
    public static function SendWebResponse($code = CodeResponseDTO::Ok, $message = '')
    {
        http_response_code($code);
        $webResponseDTO = new WebResponseDTO();
        $webResponseDTO->setCode($code);
        $webResponseDTO->setMessage($message);

        DTOWebResponseController::SendResponseDTO($webResponseDTO);
    }

    /**
     * Muestra una respuesta http con un mensaje del tipo que hace la peticion del usuario.
     *
     * @param object $dto DTO con la informacion a mostrar.
     */
    
    public static /* void */ function SendResponseDTO($dto)
    {
        if (isset($_SERVER["CONTENT_TYPE"]) && $_SERVER["CONTENT_TYPE"] == 'application/x-protobuf') { 
            if($dto instanceof Message)
            {
                echo $dto->serializeToString();
            }
            else if($dto instanceof IModel)
            {
                $dto = $dto->ToDTO();
                echo $dto->serializeToString();
            }
        } else {
            if($dto instanceof Message)
            {
                echo $dto->serializeToJsonString();
            }
            else if($dto instanceof IModel)
            {
                $dto = $dto->ToDTO();
                echo $dto->serializeToJsonString();
            }
        }
    }

}