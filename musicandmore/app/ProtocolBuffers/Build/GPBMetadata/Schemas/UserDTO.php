<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: Schemas/UserDTO.proto

namespace GPBMetadata\Schemas;

class UserDTO
{
    public static $is_initialized = false;

    public static function initOnce() {
        $pool = \Google\Protobuf\Internal\DescriptorPool::getGeneratedPool();

        if (static::$is_initialized == true) {
          return;
        }
        $pool->internalAddGeneratedFile(hex2bin(
            "0ad6020a15536368656d61732f5573657244544f2e70726f746f12034454" .
            "4f22af020a075573657244544f120a0a02696418012001280512100a0875" .
            "736572735f6964180220012809120c0a046e616d65180320012809120d0a" .
            "05656d61696c18042001280912100a0870617373776f7264180520012809" .
            "12160a0e72656d656d6265725f746f6b656e18062001280912120a0a6372" .
            "65617465645f617418072001280912120a0a757064617465645f61741808" .
            "2001280912100a08766572696669656418092001280512130a0b656d6169" .
            "6c5f746f6b656e180a2001280912160a0e6a6f625f7469746c655f6e616d" .
            "65180b20012809121d0a157461736b735f746f74616c5f61737369676e65" .
            "6473180c20012805121d0a157461736b735f746f74616c5f636f6d706c65" .
            "746564180d20012805121a0a126e6f74696669636174696f6e5f746f6b65" .
            "6e180e20012809620670726f746f33"
        ));

        static::$is_initialized = true;
    }
}

