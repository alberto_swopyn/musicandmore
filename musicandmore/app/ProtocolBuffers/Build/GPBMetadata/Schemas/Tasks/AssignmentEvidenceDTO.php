<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: Schemas/Tasks/AssignmentEvidenceDTO.proto

namespace GPBMetadata\Schemas\Tasks;

class AssignmentEvidenceDTO
{
    public static $is_initialized = false;

    public static function initOnce() {
        $pool = \Google\Protobuf\Internal\DescriptorPool::getGeneratedPool();

        if (static::$is_initialized == true) {
          return;
        }
        $pool->internalAddGeneratedFile(hex2bin(
            "0acb010a29536368656d61732f5461736b732f41737369676e6d656e7445" .
            "766964656e636544544f2e70726f746f120944544f2e5461736b73228a01" .
            "0a1541737369676e6d656e7445766964656e636544544f120a0a02696418" .
            "0120012805121f0a1761737369676e6d656e745f65766964656e6365735f" .
            "696418022001280512160a0e61737369676e6d656e74735f696418032001" .
            "2805120c0a0474797065180420012809120c0a046e616d65180520012809" .
            "12100a08646f63756d656e74180620012809620670726f746f33"
        ));

        static::$is_initialized = true;
    }
}

