<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class SoftwareApplication extends Model
{
   	protected $table = "software_applications";
    protected $primarykey = "software_applications_id";
    protected $fillable = ['id', 'software_applications_id', 'software', 'job_applications_id'];
}
