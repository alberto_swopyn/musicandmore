<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class CoursesKardex extends Model
{
	protected $table = "courses_kardex";
	protected $primarykey = "id";
	protected $fillable = ["id", "employee_id", "training_user_grades_id", "course_progresses"];
}
//mandar llamar la funcion cada vez que se guarda el progeso de cada curso o cada vez que se finaliza el examen. 