<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class OfficeApplication extends Model
{
    protected $table = "office_applications";
    protected $primarykey = "office_applications_id";
    protected $fillable = ['id', 'office_applications_id', 'office', 'job_applications_id'];
}
