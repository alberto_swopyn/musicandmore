<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class QuizStudent extends Model
{
    protected $table      = "quiz_student";
    protected $primarykey = "id";
    protected $fillable   = ['id_user', 'id_question'];
}
