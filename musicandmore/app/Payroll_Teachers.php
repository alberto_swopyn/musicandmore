<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Payroll_Teachers extends Model
{
    protected $table      = "payroll_teacher";
    protected $primarykey = "id";
    protected $fillable   = ['id_group', 'user_id', 'name_pay', 'num_hour', 'quantity_pay', 'is_completed','id','date'];
}
