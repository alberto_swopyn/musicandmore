<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AnswerEvaluation extends Model
{
    protected $table = "answer_evaluations";
    protected $primarykey = "answer_evaluations_id";
    protected $fillable = ['id', 'answer_evaluations_id', 'user_id','option_evaluations_id', 'value'];
}
