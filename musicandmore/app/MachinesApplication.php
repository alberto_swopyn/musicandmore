<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class MachinesApplication extends Model
{
    protected $table = "machines_applications";
    protected $primarykey = "machines_applications_id";
    protected $fillable = ['id', 'machines_applications_id', 'machines', 'job_applications_id'];
}
