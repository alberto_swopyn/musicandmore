<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class GeneralInformationJobCenter extends Model
{
    protected $table = "general_information_job_centers";
    protected $fillable = ['id','general_information_job_centers_id','profile_job_centers_id','hour_init','hour_final','start_workdays','final_workdays','small_box','breakeven','rent_contract','validity_rent_contract','hacienda','validity_hacienda','commercial_permission','validity_commercial_permission','fumigation','validity_fumigation','extinguisher','validity_extinguisher','salubrity','validity_salubrity'];
}