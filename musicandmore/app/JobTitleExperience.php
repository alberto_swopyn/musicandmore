<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleExperience extends Model
{
    protected $table = "job_title_experiences";
    protected $primarykey = "job_title_experiences_id";
    protected $fillable = ['id', 'job_title_experiences_id', 'job_title_profiles_id','experience_name', 'experience_year'];
}
