<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AssingmentPay extends Model
{
    protected $table      = 'assingment_pays';
    protected $primarykey = "id";
    protected $fillable   = ['pays_id', 'users_id','is_completed','comment','id','date_pay','type_pay, no_aument'];

    public function pay(){
    	return $this->belongsToMany(Pay::class);
    }
    
    public function user(){
        return $this->belongsToMany(User::class);
    }
}
