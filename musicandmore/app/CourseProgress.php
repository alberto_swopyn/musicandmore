<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class CourseProgress extends Model
{
    protected $table='course_progresses';
    protected $primarykey = 'id';
    protected $fillable = ['id','user_id','training_id','progress','course_grades', 'correct'];

    public function training(){
    	return $this->belongsTo(Training::class);
    }

    

}
