<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class TrainingUserGrade extends Model
{
	protected $table = 'training_user_grades';
	protected $primarykey='id';
	protected $fillable = ['user_id','training_id', 'correct', 'grades'];
}
