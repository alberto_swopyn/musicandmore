<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class MedicalStudy extends Model
{
    protected $table = "medical_studies";
    protected $primarykey = "medical_studies_id";
    protected $fillable = ['id', 'medical_studies_id', 'blood_type', 'allergics', 'cronics_disseases', 'medicine', 'name_medicine', 'family_disseases', 'surgery', 'cancer', 'diabetes', 'heart_disseases', 'brain_contution', 'smoke', 'drunk', 'drugs'];
}
