<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	protected $table      = 'contacts';
	protected $primarykey = 'id';
	protected $fillable   = ['id', 'name'];

    public function company()
    {
    	return $this->hasOne(Company::class);
    }
}
