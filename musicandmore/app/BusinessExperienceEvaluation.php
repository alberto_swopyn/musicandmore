<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class BusinessExperienceEvaluation extends Model
{
    protected $table = "business_experience_evaluations";
    protected $primarykey = 'business_experience_evaluations_id';
    protected $fillable = ['id','business_experience_evaluations_id','evaluation_committees_id','sales_service','staff_management','inventory_management','cash_management','sales_experience','computer_skills','fluid_communication','good_leader','ordained_person','tolerance', 'positive_enthusiastic_person','discipline_person','potential_candidate','neat_person','committed_person','shop_management','clear_idea','track_record','stable_person'];
}
