<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobArea extends Model
{
    protected $table = "job_areas";
    protected $primarykey = 'job_areas_id';
    protected $fillable = ['id','job_areas_id','name'];
}
