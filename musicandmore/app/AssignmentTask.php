<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AssignmentTask extends Model
{
    protected $table      = 'assignment_task';
    protected $primarykey = "id";
    protected $fillable   = ['assignment_id', 'task_id'];

     //Nombre de la tabla
     const TableName = 'assignment_task';

     //Nombres de los campos de la tabla
     const Id          = "id";
     const TaskId      = 'task_id';
     const AssignmentId = 'assignment_id';
}
