<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class PsychometricLevel extends Model
{
    protected $table = "psychometric_levels";
    protected $primarykey = 'id';
    protected $fillable = ['id','function_typ','level','hits1','hits2'];
}
