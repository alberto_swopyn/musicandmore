<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AssignmentUser extends Model
{
    protected $table      = "assignment_user";
    protected $primarykey = "id";
    protected $fillable   = ['assignment_id', 'user_id'];

    //Nombre de la tabla
    const TableName = 'assignment_user';

    //Nombres de los campos de la tabla
    const Id          = "id";
    const UserId      = 'user_id';
    const AssignmentId = 'assignment_id';
}
