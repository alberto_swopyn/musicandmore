<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleLanguage extends Model
{
	protected $table = "job_title_languages";
    protected $primarykey = "job_title_languages_id";
    protected $fillable = ['id', 'job_title_languages_id', 'job_title_profiles_id','language', 'level','level_write','level_read','level_speak','level_listen'];
}
