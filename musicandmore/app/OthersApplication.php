<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class OthersApplication extends Model
{
    protected $table = "others_applications";
    protected $primarykey = "others_applications_id";
    protected $fillable = ['id', 'others_applications_id', 'others', 'job_applications_id'];
}
