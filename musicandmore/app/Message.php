<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';
	protected $primarykey='id';
	protected $fillable = ['id','user_id','assignment_user_id','email_assignment_user_id','title','description','url'];
}
