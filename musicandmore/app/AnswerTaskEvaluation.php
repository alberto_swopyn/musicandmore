<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AnswerTaskEvaluation extends Model
{
    protected $table      = "answer_task_evaluations";
    protected $primarykey = "id";
    protected $fillable   = ['user_id', 'task_evaluations_id', 'option_task_evaluations_id', 'value'];
}
