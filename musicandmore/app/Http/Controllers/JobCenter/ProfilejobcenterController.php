<?php

namespace Swopyn\Http\Controllers\JobCenter;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\ProfileJobCenter;
use Swopyn\GeneralInformationJobCenter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Validator;
use Image;
use PDF;

class ProfilejobcenterController extends Controller
{
	protected $rules =
	[
	'NombreJobCenter' => 'Required',
	'CodigoJobCenter' => 'Required',
	//'Imagen'=>'Required',
	'TipoPuesto' => 'Required'
	];

	public function create($jobcenter_id,$jobcenter_name){
		$tmp = DB::table('profile_job_centers')->where('profile_job_centers_id', $jobcenter_id)->first();
		if (count($tmp) <= 0){
			$jobcenter                         = New ProfileJobCenter;
			$jobcenter->profile_job_centers_id = $jobcenter_id;
		}
		else{
			$jobcenter = ProfileJobCenter::find($tmp->id);
		}
		$jobcenter->name = $jobcenter_name;
		$jobaddress = DB::table('address_job_centers')->where('profile_job_centers_id', $jobcenter_id)->first();
		$work = DB::table('work_capacities')->where('profile_job_centers_id', $jobcenter_id)->get();
		$general = DB::table('general_information_job_centers')->where('profile_job_centers_id', $jobcenter_id)->first();
		$goals = DB::table('goal_job_centers')->where('profile_job_centers_id', $jobcenter_id)->first();
		$jobtitles = DB::table('tree_job_profiles')->join('job_title_profiles as jtp', 'id_inc', 'jtp.job_title_profiles_id')->where('company_id','4')->get();
		$jobcenter->save();
		 return view('adminlte::jobcenter.create')->with(['jobcenter' => $jobcenter, 'jobaddress' => $jobaddress, 'work' => $work, 'general' => $general, 'goals' => $goals, 'jobtitles' => $jobtitles]);
		//return view('adminlte::jobcenter._ubication');
	}
	//treejobcentersummary
	public function create2(){
        $jobcenters = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('prof.id', 'prof.name', 'gen.validity_rent_contract', 'gen.validity_hacienda', 'gen.validity_commercial_permission', 'gen.validity_fumigation', 'gen.validity_extinguisher', 'gen.validity_salubrity')->get();
        $rent = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_rent_contract')->get();
        $hacienda = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_hacienda')->get();
        $commercial = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_commercial_permission')->get();
        $fumigation = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_fumigation')->get();
        $extinguisher = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_extinguisher')->get();
        $salubrity = DB::table('profile_job_centers as prof')->join('general_information_job_centers as gen', 'prof.profile_job_centers_id', '=', 'gen.profile_job_centers_id')->select('gen.validity_salubrity')->get();


        return view('adminlte::tree.treejobcentersummary')->with(['jobcenters'=> $jobcenters, 'rent' => $rent, 'hacienda' => $hacienda, 'commercial' => $commercial, 'fumigation' => $fumigation, 'extinguisher' => $extinguisher, 'salubrity' => $salubrity]);

    }

	public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		}
		else {
			$jobcenter = DB::table('profile_job_centers')->where('profile_job_centers_id', $request->jobcenter_id)->first();
			if (count($jobcenter) <= 0){
				$profile_job_centers = New ProfileJobCenter;

			}else{
				$profile_job_centers = ProfileJobCenter::find($jobcenter->id);
			}
				$profile_job_centers->profile_job_centers_id = $request->jobcenter_id;
				$profile_job_centers->name                   = $request->NombreJobCenter;
				$profile_job_centers->code                   = $request->CodigoJobCenter;
				//$profile_job_centers->image                = $request->Imagen;
				$profile_job_centers->type                   = $request->TipoPuesto;
				$profile_job_centers->save();

			 return response()->json($profile_job_centers );


		}
	}


}
