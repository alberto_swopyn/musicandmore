<?php

namespace Swopyn\Http\Controllers\JobCenter;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\AddressJobCenter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;
use Validator;

class AddressjobcenterController extends Controller
{
	protected $rules =
	[
	'Calle' => 'Required',
	'NumExt' => 'Required|numeric',
	'Colonia' => 'Required',
	'CodigoPostal' => 'Required|numeric',
	'Localidad' => 'Required',
	'Municipio' => 'Required',
	'Estado' => 'Required',
	'Pais' => 'Required',
	'Telefono' => 'Required|numeric',
	'Ubicacion' => 'Required',
	'Correo' => 'Required|email',
	'Identificador' =>'Required'
	];


	public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {  
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} 
		else { 
			$addressJob = DB::table('address_job_centers')->where('profile_job_centers_id', $request->Identificador)->first();
			if (count($addressJob) <= 0) {
				$address_job_centers = New AddressJobCenter;
				$address_job_centers->profile_job_centers_id=$request->Identificador;
				$address_job_centers->address_job_centers_id=$request->Identificador;
			}
			else{
				$id = $addressJob->id;
				$address_job_centers = AddressJobCenter::find($id);
			}
			
			$address_job_centers->street=$request->Calle;
			$address_job_centers->num_ext=$request->NumExt;
			$address_job_centers->district=$request->Colonia;
			$address_job_centers->zip_code=$request->CodigoPostal;
			$address_job_centers->location=$request->Localidad;
			$address_job_centers->municipality=$request->Municipio;
			$address_job_centers->state=$request->Estado;
			$address_job_centers->country=$request->Pais;
			$address_job_centers->ubication=$request->Ubicacion;
			$address_job_centers->phone=$request->Telefono;
			$address_job_centers->email=$request->Correo;

			$address_job_centers->save();

			return response()->json($address_job_centers);
		}
	}
}