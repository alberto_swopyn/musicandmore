<?php

namespace Swopyn\Http\Controllers\Pays;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Swopyn\Pay;
use Swopyn\PayType;
use Illuminate\Support\Facades\Crypt;
use Swopyn\User;
use Swopyn\AssingmentPay;
use Swopyn\TrainingPlan;
use Swopyn\Employee;
use Swopyn\GroupAlumn;
use Swopyn\Subject;
use DB;
use Swopyn\Assitance;
use Swopyn\AssistanceS;
use Swopyn\Payroll_Teachers;
use Swopyn\Discount;
use Swopyn\CutBox;
use Carbon\Carbon;
use Swopyn\Notifications\PayAlert;
use PDF;
use Image;
use DateTime;
use Swopyn\Arqueo;
use Notification;
use NumerosEnLetras;
use Swopyn\Deposito;
use Swopyn\DebitReport;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class PayController extends Controller
{
	public function myPays(){

    $search = \Request::get('search');
        $asignacion = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('pay_types','ap.type_pay','pay_types.id')->select('ap.id as assi','ap.pays_id','ap.users_id','ap.is_completed','ap.discount','ap.total','ap.date_pay','ap.comment','p.id','p.title','p.description','p.quantity','p.limit_date','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','pay_types.name as metodo')->where('ap.users_id',auth()->user()->id)->where('users.name','LIKE','%'.$search.'%')->orderBy('ap.id','DESC')->paginate(10);

        $ass_rec = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('subjects', 'p.id_course','subjects.id')->join('pay_types','ap.type_pay','pay_types.id')->select('ap.id as assi','ap.users_id','ap.pays_id','ap.discount','ap.date_pay','ap.total','ap.is_completed','ap.comment','p.id','p.title','p.description','p.quantity','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','subjects.id as sub', 'subjects.name as subject','pay_types.name as metodo','ap.aument')->where('ap.users_id', auth()->user()->id)->where('users.name','LIKE','%'.$search.'%')->orderBy('ap.id','DESC')->paginate(10);

        return view('adminlte::alumn_pay.MyPays')->with(['asignacion' => $asignacion, 'ass_rec' => $ass_rec]);
    }

    public function createpay()
    {
        $user = User::all();
        return view('adminlte::alumn_pay.createpay')->with(['users' => $user]);
    }

    public function createpay_r()
    {
       $user = User::all();
        $group = Subject::all();
        $a = GroupAlumn::all();
        return view('adminlte::alumn_pay.createpay_r')->with(['users' => $user,'group' => $group, 'a' => $a]);
    }

    public function editPays($id)
    {
        $pay = Pay::find($id);
         return view('adminlte::alumn_pay.edit')->with(['pay' => $pay]);
    }

     public function editPays_r($id)
    {
        $pay = Pay::find($id);
        $group = Subject::all();
         return view('adminlte::alumn_pay.edit_r')->with(['pay' => $pay, 'group' => $group]);
    }

    public function updatePays($id, Request $request)
    {
        $pays              = Pay::find($id);
        $pays->title       = $request->Nombre;
        $pays->description = $request->Descripcion;
        $pays->users_id     = auth()->user()->id;
        $pays->id_course   = 0;
         if($request->Fecha_limite != null){
            $date = Carbon::createFromFormat('Y-m-d',$request->Fecha_limite);
             $pays->limit_date   = $date->format('Y-m-d');
         }else{
            $pays->limit_date = null;
         }
        if ($request->no_fecha == null) {
            $pays->habilitar = 0;
        }else{
            $pays->habilitar = 1;
        }
        $pays->quantity     = $request->Cantidad;
        $pays->discount     = 0;
        $pays->comment      = $request->Comentarios;
        $pays->type         = 1;
        $pays->save();

        session()->flash('message_update', 'Pago Actualizado');

        $pay = Pay::simplePaginate(10);
        return redirect()->route('pays_admin')->with(['pay' => $pay]);
    }

    public function updatePays_r($id, Request $request)
    {
        $pays              = Pay::find($id);
        $pays->title       = $request->Nombre;
        $pays->description = $request->Descripcion;
        $pays->users_id     = auth()->user()->id;
        $pays->id_course   = $request->group[0];
        $pays->habilitar   = 0;
        $pays->habilitar_s = 0;
        //$pays->limit_date  = $request->Fecha_limite;
        $pays->quantity     = $request->Cantidad;
        $pays->discount     = 0;
        $pays->comment      = $request->Comentarios;
        $pays->type        = 2;
        $pays->aument      = $request->Sancion;

        $pays->save();

        session()->flash('message_update', 'Pago Actualizado');

        $pay = Pay::simplePaginate(10);
        return redirect()->route('pays_admin')->with(['pay' => $pay]);
    }

    public function deletePays($id)
    {
        $pay = Pay::find($id);
        $pay->delete();
        $a = AssingmentPay::find($pay->id);
        if ($a == true) {
            $a->delete();
        }
        $pay = Pay::simplePaginate(10);
        return redirect()->route('pays_admin')->with(['pay' => $pay]);
    }

     public function deletePays_r($id)
    {
        $pay = Pay::find($id);
        $pay->delete();
        $a = AssingmentPay::find($pay->id);
        if ($a == true) {
            $a->delete();
        }
        $pay = Pay::simplePaginate(10);
        return redirect()->route('pays_admin')->with(['pay' => $pay]);
    }

    public function add(Request $request)
    {
        $pay  = new Pay;
        $pay->title        = $request->Nombre;
        $pay->description  = $request->Descripcion;
        $pay->users_id     = auth()->user()->id;
        $pay->id_course    = 0;
        if($request->Fecha_limite != null){
             $pay->limit_date   = Carbon::createFromFormat('Y-m-d',$request->Fecha_limite);
         }else{
            $pay->limit_date = null;
         }
        if ($request->no_fecha == null) {
            $pay->habilitar = 0;
        }else{
            $pay->habilitar = 1;
        }
        $pay->quantity     = $request->Cantidad;
        $pay->discount     = 0;
        $pay->comment      = $request->Comentarios;
        $pay->type         = 1;
        $pay->aument = 0;
        $pay->habilitar_s = 0;
        $pay->save();
           
        return redirect()->route('pays_admin');

	}
     public function add_r(Request $request)
    {
        $pay  = new Pay;
        $pay->title        = $request->Nombre;
        $pay->description  = $request->Descripcion;
        $pay->users_id     = auth()->user()->id;
        $pay->id_course    = $request->group[0];
        //$pay->limit_date   = $request->Fecha_limite;
        $pay->habilitar    = 0;
        $pay-> habilitar_s = 0;
        $pay->aument       = $request->Sancion;
        $pay->quantity     = $request->Cantidad;
        $pay->discount     = 0;
        $pay->comment      = $request->Comentarios;
        $pay->type         = 2;
        $pay->save();
           
        return redirect()->route('pays_admin');

    }

	public function adminPays(){

        $search = \Request::get('search');
    	$pay = DB::table('pays')->join('users', 'pays.users_id', '=', 'users.id')->join('groups','pays.id_course','=','groups.id')->join('cathedras','groups.id_cathedra','=','cathedras.id')->join('employees','cathedras.employee_id','employees.employee_id')->select('users.name', 'pays.title', 'pays.description', 'pays.limit_date', 'pays.quantity', 'pays.discount', 'pays.id', 'pays.comment','groups.name as curso','employees.name as maestro','pays.type','pays.aument')->where('pays.title','LIKE','%'.$search.'%')->orderBy('pays.id','DESC')->paginate(10);
        $search2 = \Request::get('search2');
        $rec = DB::table('pays')->join('users', 'pays.users_id', '=', 'users.id')->join('subjects','pays.id_course','subjects.id')->select('users.name', 'pays.title', 'pays.description', 'pays.quantity', 'pays.discount', 'pays.id', 'pays.comment','pays.type','pays.aument','subjects.id as sub_id','subjects.name as subject')->where('pays.title','LIKE','%'.$search2.'%')->orderBy('pays.id','DESC')->paginate(10);
        $fecha = Carbon::now();
        $dia = 10;
        $mes = $fecha->format('m');
        $year = $fecha->format('Y');
        $limit_date = $dia.'/'.$mes.'/'.$year;

    	return view('adminlte::alumn_pay.adminPays')->with(['pay' => $pay, 'rec' => $rec, 'limit_date' => $limit_date]);
	}

    public function adminDiscount()
    {
        $search = \Request::get('search');
        $discount = DB::table('discounts')->select('id','title','description','users_id','quantity')->where('title','LIKE','%'.$search.'%')->orderBy('title','ASC')->paginate(10);
        return view('adminlte::alumn_pay.admin_discount')->with(['discount' => $discount]);
    }
    public function create_discount()
    {
        return view('adminlte::alumn_pay.creatediscount');
    }
    public function add_discount(Request $request)
    {
        $discount  = new Discount;
        $discount->title        = $request->Nombre;
        $discount->description  = $request->Descripcion;
        $discount->users_id     = auth()->user()->id;
        $discount->quantity     = $request->Cantidad;
        $discount->save();
           
        return redirect()->route('discount_admin');
    }
    public function edit_discount($id)
    {
        $discount = Discount::find($id);
        return view('adminlte::alumn_pay.editdiscount')->with(['discount' => $discount]);

    }

    public function update_discount($id, Request $request)
    {
        $discount  = Discount::find($id);
        $discount->title        = $request->Nombre;
        $discount->description  = $request->Descripcion;
        $discount->users_id     = auth()->user()->id;
        $discount->quantity     = $request->Cantidad;
        $discount->save();
           
        return redirect()->route('discount_admin');
        
    }

    public function delete_discount($id)
    {
        $discount = Discount::find($id);
        $discount->delete();
        return redirect()->route('discount_admin');
    }

    public function box_admin()
    {
        $search = \Request::get('search');
        $asignacion = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('pay_types','ap.type_pay','pay_types.id')->select('ap.id as assi','ap.pays_id','ap.users_id','ap.is_completed','ap.discount','ap.total','ap.date_pay','ap.comment','p.id','p.title','p.description','p.quantity','p.limit_date','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','pay_types.name as metodo','ap.update_reason as reason')->where('users.name','LIKE','%'.$search.'%')->orderBy('ap.id','DESC')->paginate(15);
        $search2 = \Request::get('search2');
        $ass_rec = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('subjects', 'p.id_course','subjects.id')->join('pay_types','ap.type_pay','pay_types.id')->join('general_personal_datas as gpa','users.id','gpa.general_personal_data_id')->select('ap.id as assi','ap.users_id','ap.pays_id','ap.discount','ap.date_pay','ap.total','ap.is_completed','ap.comment','p.id','p.title','p.description','p.quantity','p.limit_date','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','subjects.id as sub', 'subjects.name as subject','pay_types.name as metodo','ap.aument','gpa.schoolarship as beca','ap.update_reason as reason')->where('users.name','LIKE','%'.$search2.'%')->orderBy('ap.id','DESC')->paginate(15);
        $hoy = Carbon::now()->toDateString();
        $cut = DB::table('cut_boxs')->where('date',$hoy)->first();
        $fecha = Carbon::now();
        $dia = 10;
        $mes = $fecha->format('m');
        $year = $fecha->format('Y');
        $limit_date = $dia.'/'.$mes.'/'.$year;

        return view('adminlte::alumn_pay.box_admin')->with(['asignacion' => $asignacion, 'ass_rec' => $ass_rec, 'cut' => $cut,'limit_date' => $limit_date]);
    }

    public function box_unic()
    {
       $student = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->select('users.id', 'users.name', 'users.sample', 'role_user.role_id', 'role_user.status')->where('role_user.role_id', '=', '2')->where('role_user.status', '=', '1')->orderBy('users.name','ASC')->get();
       $types = PayType::all();
       $pay = DB::table('pays')->where('type',1)->get();
       $discount = Discount::orderBy('id')->get();

       return view('adminlte::alumn_pay.box_unic')->with(['discount' => $discount, 'student' => $student, 'pay' => $pay,'types' => $types]);
    }

    public function box_rec()
    {
       $student = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->select('users.id', 'users.name', 'users.sample', 'role_user.role_id', 'role_user.status')->where('role_user.role_id', '=', '2')->where('role_user.status', '=', '1')->orderBy('users.name','ASC')->get();
       $pay = DB::table('pays')->join('subjects','pays.id_course','subjects.id')->select('pays.title','pays.description','pays.type','pays.quantity','pays.limit_date','pays.comment','pays.id','subjects.id as sub','subjects.name as subject')->where('type',2)->get();
       $discount = Discount::orderBy('id')->get();
       $types = PayType::all();
       $hoy = Carbon::now()->toDateString();
       $fecha = Carbon::now();
       $dia = 10;
       $mes = $fecha->format('m');
       $year = $fecha->format('Y');
       $day = $fecha->format('d');
       $limit_date = $year.'/'.$mes.'/'.$dia;

       return view('adminlte::alumn_pay.box_rec')->with(['discount' => $discount, 'student' => $student, 'pay' => $pay, 'types' => $types, 'hoy' => $hoy, 'limit_date' => $limit_date, 'dia' => $dia, 'day' => $day ]);
    }

    public function box_unic_add(Request $request)
    {
        $discount = DB::table('discounts')->select('quantity','id')->where('id',$request->discount)->first();
        $pay = DB::table('pays')->select('quantity','id')->where('id',$request->pay)->first();
        if ($discount->quantity == 0) {
            $asignacion = new AssingmentPay;
            $asignacion->pays_id = $request->pay[0];
            $asignacion->users_id = $request->student[0];
            $asignacion->discount = $request->discount[0];
            $asignacion->type_pay = $request->pago[0];
            $r = $pay->quantity;
            $asignacion->total = $r;
            $asignacion->comment = $request->Comentarios;
            $asignacion->date_pay = Carbon::now()->toDateString();
            $asignacion->is_completed = 1;
            $asignacion->save();
            return redirect()->route('box_admin');
        }else{
            $asignacion = new AssingmentPay;
            $asignacion->pays_id = $request->pay[0];
            $asignacion->users_id = $request->student[0];
            $asignacion->discount = $request->discount[0];
            $asignacion->type_pay = $request->pago[0];
            $t = $pay->quantity*$discount->quantity;
            $r = $t/100;
            $r2 = $pay->quantity-$r;
            $asignacion->total = $r2;
            $asignacion->comment = $request->Comentarios;
            $asignacion->date_pay = Carbon::now()->toDateString();
            $asignacion->is_completed = 1;
            $asignacion->save();
            return redirect()->route('box_admin');
        }
    }

    public function box_unic_calc(Request $request)
    {
        $curso = $request->curso;
        $student = $request->student;
        $discount = $request->discount;

        $pago = DB::table('pays')->select('id','quantity')->where('id',$curso)->first();
        $desc = DB::table('discounts')->select('id','quantity')->where('id',$discount)->first();

        if($desc->quantity == 0){
            $price = $pago->quantity;
        }else{
            $t = $pago->quantity*$desc->quantity;
            $p = $t/100;
            $price = $pago->quantity-$p;
        }

        return response()->json(['price'=>$price]);
    }

    public function box_unic_edit($id)
    {
       $ass = AssingmentPay::find($id);
       $student = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->select('users.id', 'users.name', 'users.sample', 'role_user.role_id', 'role_user.status')->where('role_user.role_id', '=', '2')->where('role_user.status', '=', '1')->orderBy('users.name','ASC')->get();
       $types = PayType::all();
       $pay = DB::table('pays')->where('type',1)->get();
       $discount = Discount::all();

       return view('adminlte::alumn_pay.box_unic_edit')->with(['discount' => $discount, 'student' => $student, 'pay' => $pay,'types' => $types,'ass' => $ass]);
    }

    public function box_unic_update($id, Request $request)
    {
        $discount = DB::table('discounts')->select('quantity','id')->where('id',$request->discount)->first();
        $pay = DB::table('pays')->select('quantity','id')->where('id',$request->pay)->first();
        if ($discount->quantity == 0) {
            $asignacion = AssingmentPay::find($id);
            $asignacion->pays_id = $request->pay[0];
            $asignacion->users_id = $request->student[0];
            $asignacion->discount = $request->discount[0];
            $asignacion->type_pay = $request->pago[0];
            $r = $pay->quantity;
            $asignacion->total = $r;
            $asignacion->comment = $request->Comentarios;
            $asignacion->is_completed = 1;
            $asignacion->update_reason = $request->Reason;
            $asignacion->save();
            return redirect()->route('box_admin');
        }else{
            $asignacion = AssingmentPay::find($id);
            $asignacion->pays_id = $request->pay[0];
            $asignacion->users_id = $request->student[0];
            $asignacion->discount = $request->discount[0];
            $asignacion->type_pay = $request->pago[0];
            $t = $pay->quantity*$discount->quantity;
            $r = $t/100;
            $r2 = $pay->quantity-$r;
            $asignacion->total = $r2;
            $asignacion->comment = $request->Comentarios;
            $asignacion->is_completed = 1;
            $asignacion->update_reason = $request->Reason;
            $asignacion->save();
            return redirect()->route('box_admin');
        }
    }

    public function box_unic_delete($id)
    {
        $asignacion = AssingmentPay::find($id);
        $asignacion->delete();
        return redirect()->route('box_admin');
    }

    public function box_rec_add(Request $request)
    {
        $discount = DB::table('discounts')->select('quantity','id')->where('id',$request->discount)->first();
        $pay = DB::table('pays')->select('quantity','id','aument')->where('id',$request->pay)->first();
        $beca = DB::table('general_personal_datas')->select('general_personal_data_id','schoolarship')->where('general_personal_data_id',$request->student[0])->first();
        $fecha = Carbon::now();
        $dia = 10;
        $mes = $fecha->format('m');
        $year = $fecha->format('Y');
        $limit_date = $year.'/'.$mes.'/'.$dia;
        $limite = Carbon::parse($limit_date);
        $hoy = Carbon::now()->toDateString();
        if ($discount->quantity == 0) {
            $asignacion = new AssingmentPay;
            $asignacion->pays_id = $request->pay[0];
            $asignacion->users_id = $request->student[0];
            $asignacion->discount = $request->discount[0];
            $asignacion->type_pay = $request->pago[0];
            if ($limite < $hoy) {
                if ($beca->schoolarship > 0) {
                    $r = $pay->quantity;
                    $bec = $beca->schoolarship;
                    $ds = $r*$bec;
                    $dss = $ds/100;
                    $cant = $r-$dss;
                    if($request->no_aument == 1){
                        //$aument = 0;
                        //$t = $cant*$aument;
                        //$t2 = $t/100;
                        //$r2 = $cant+$t2;
                        $asignacion->total = $cant;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = 0;
                        $asignacion->no_aument = 1;
                        $asignacion->save();
                        return redirect()->route('box_admin'); 
                    }else{
                        $aument = $pay->aument;
                        $t = $cant*$aument;
                        $t2 = $t/100;
                        $r2 = $cant+$t2;
                        $asignacion->total = $r2;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = 0;
                        $asignacion->no_aument = 0;
                        $asignacion->save();
                        return redirect()->route('box_admin'); 
                    } 
                }else{
                    if($request->no_aument == 1){
                        $r = $pay->quantity;
                        $asignacion->total = $r;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = 0;
                        $asignacion->no_aument = 1;
                        $asignacion->save();
                        return redirect()->route('box_admin'); 
                    }else{
                        $r = $pay->quantity;
                        $aument = $pay->aument;
                        $t = $r*$aument;
                        $t2 = $t/100;
                        $r2 = $r+$t2;
                        $asignacion->total = $r2;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = $pay->aument;
                        $asignacion->no_aument = 0;
                        $asignacion->save();
                        return redirect()->route('box_admin');    
                    }
                } 
            }else{
                if ($beca->schoolarship > 0) {
                    $r = $pay->quantity;
                    $bec = $beca->schoolarship;
                    $ds = $r*$bec;
                    $dss = $ds/100;
                    $cant = $r-$dss;
                    $asignacion->total = $cant;
                    $asignacion->comment = $request->Comentarios;
                    $asignacion->is_completed = 1;
                    $asignacion->date_pay = Carbon::now()->toDateString();
                    $asignacion->aument  = 0;
                    $asignacion->no_aument = 0;
                    $asignacion->save();
                    return redirect()->route('box_admin'); 
                }
                else{
                    $r = $pay->quantity;
                    $asignacion->total = $r;
                    $asignacion->comment = $request->Comentarios;
                    $asignacion->is_completed = 1;
                    $asignacion->date_pay = Carbon::now()->toDateString();
                    $asignacion->aument  = 0;
                    $asignacion->no_aument = 0;
                    $asignacion->save();
                    return redirect()->route('box_admin'); 
                }
            }
        }else{
            $asignacion = new AssingmentPay;
            $asignacion->pays_id = $request->pay[0];
            $asignacion->users_id = $request->student[0];
            $asignacion->discount = $request->discount[0];
            $asignacion->type_pay = $request->pago[0];
            if ($limite < $hoy) {
                if ($beca->schoolarship > 0) {
                    $bec = $beca->schoolarship;
                    $t = $pay->quantity*$bec;
                    $ds = $t/100;
                    $cant = $pay->quantity-$ds;
                    $desc = $cant*$discount->quantity;
                    $r = $desc/100;
                    $r2 = $cant-$r;
                    if ($request->no_aument == 1) {
                        $asignacion->total = $r2;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = 0;
                        $asignacion->no_aument = 1;
                        $asignacion->save();
                        return redirect()->route('box_admin'); 
                    }else{
                        $aument = $cant*$pay->aument;
                        $a2 = $aument/100;
                        $rt = $r2+$a2;
                        $asignacion->total = $rt;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = $pay->aument;
                        $asignacion->no_aument = 0;
                        $asignacion->save();
                        return redirect()->route('box_admin');   
                    }
                    
                }else{
                    $t = $pay->quantity*$discount->quantity;
                    $r = $t/100;
                    $r2 = $pay->quantity-$r;
                    if ($request->no_aument == 1) {
                        $asignacion->total = $r2;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = $pay->aument;
                        $asignacion->no_aument = 1;
                        $asignacion->save();
                        return redirect()->route('box_admin');
                    }else{
                        $aument = $pay->quantity*$pay->aument;
                        $a2 = $aument/100;
                        $rt = $r2+$a2;
                        $asignacion->total = $rt;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = $pay->aument;
                        $asignacion->no_aument = 0;
                        $asignacion->save();
                        return redirect()->route('box_admin');   
                    }
                    
                }
            }else{
                if ($beca->schoolarship > 0) {
                    $bec = $beca->schoolarship;
                    $t = $pay->quantity*$bec;
                    $ds = $t/100;
                    $cant = $pay->quantity-$ds;
                    $t = $cant*$discount->quantity;
                    $r = $t/100;
                    $r2 = $cant-$r;
                    $asignacion->total = $r2;
                    $asignacion->comment = $request->Comentarios;
                    $asignacion->is_completed = 1;
                    $asignacion->date_pay = Carbon::now()->toDateString();
                    $asignacion->aument  = 0;
                    $asignacion->no_aument = 0;
                    $asignacion->save();
                    return redirect()->route('box_admin'); 
                }
                else{
                  $t = $pay->quantity*$discount->quantity;
                  $r = $t/100;
                  $r2 = $pay->quantity-$r;
                  $asignacion->total = $r2;
                  $asignacion->comment = $request->Comentarios;
                  $asignacion->is_completed = 1;
                  $asignacion->date_pay = Carbon::now()->toDateString();
                  $asignacion->aument  = 0;
                  $asignacion->no_aument = 0;
                  $asignacion->save();
                  return redirect()->route('box_admin');  
                } 
            }
        }
    }

    public function box_rec_edit($id)
    {
       $ass = AssingmentPay::find($id);
       $student = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->select('users.id', 'users.name', 'users.sample', 'role_user.role_id', 'role_user.status')->where('role_user.role_id', '=', '2')->where('role_user.status', '=', '1')->orderBy('users.name','ASC')->get();
       $types = PayType::all();
       $pay = DB::table('pays')->join('subjects','pays.id_course','subjects.id')->select('pays.title','pays.description','pays.type','pays.quantity','pays.limit_date','pays.comment','pays.id','subjects.id as sub','subjects.name as subject')->where('type',2)->get();
       $discount = Discount::all();
       $hoy = Carbon::now()->toDateString();
       $fecha = Carbon::now();
       $dia = 10;
       $mes = $fecha->format('m');
       $year = $fecha->format('Y');
       $day = $fecha->format('d');
       $limit_date = $year.'/'.$mes.'/'.$dia;

       return view('adminlte::alumn_pay.box_rec_edit')->with(['discount' => $discount, 'student' => $student, 'pay' => $pay,'types' => $types,'ass' => $ass, 'dia' => $dia, 'day' => $day]);
    }

    public function box_rec_calc(Request $request)
    {
        $discount = DB::table('discounts')->select('quantity','id')->where('id',$request->discount)->first();
        $pay = DB::table('pays')->select('quantity','id','aument')->where('id',$request->curso)->first();
        $beca = DB::table('general_personal_datas')->select('general_personal_data_id','schoolarship')->where('general_personal_data_id',$request->student)->first();
        $fecha = Carbon::now();
        $dia = 10;
        $mes = $fecha->format('m');
        $year = $fecha->format('Y');
        $limit_date = $year.'/'.$mes.'/'.$dia;
        $limite = Carbon::parse($limit_date);
        $hoy = Carbon::now()->toDateString();
        if($discount->quantity == 0){
            if($limite < $hoy){
                if($beca->schoolarship > 0){
                    if($request->aument == 1){
                        $bec = $pay->quantity*$beca->schoolarship;
                        $x = $bec/100;
                        $price = $pay->quantity-$x;
                    }else{
                        $bec = $pay->quantity*$beca->schoolarship;
                        $x = $bec/100;
                        $y = $pay->quantity-$x;
                        $a = $y*$pay->aument;
                        $ay = $a/100;
                        $price = $y+$ay;
                    }
                }else{
                    if($request->aument == 1){
                        $price = $pay->quantity;
                    }else{
                        $s = $pay->quantity*$pay->aument;
                        $r = $s/100;
                        $price = $pay->quantity+$r;
                    }
                }
            }else{
                if($beca->schoolarship > 0){
                    $bec = $pay->quantity*$beca->schoolarship;
                    $x = $bec/100;
                    $price = $pay->quantity-$x;
                }else{
                    $price = $pay->quantity;
                }
            }
        }else{
            if($limite < $hoy){
                if($beca->schoolarship > 0){
                    if($request->aument == 1){
                        $bec = $pay->quantity*$beca->schoolarship;
                        $x = $bec/100;
                        $p = $pay->quantity-$x;
                        $q = $p*$discount->quantity;
                        $w = $q/100;
                        $price = $p-$w;
                    }else{
                        $bec = $pay->quantity*$beca->schoolarship;
                        $x = $bec/100;
                        $p = $pay->quantity-$x;
                        $q = $p*$discount->quantity;
                        $w = $q/100;
                        $pr = $p-$w;
                        $e = $pr*$pay->aument;
                        $o = $e/100;
                        $price = $pr+$o;
                    }
                }else{
                    if($request->aument == 1){
                        $z = $pay->quantity*$discount->quantity;
                        $c = $z/100;
                        $price = $pay->quantity-$c;
                    }else{
                        $z = $pay->quantity*$discount->quantity;
                        $c = $z/100;
                        $pri = $pay->quantity-$c;
                        $f = $pri*$pay->aument;
                        $v = $f/100;
                        $price = $pri+$v;
                    }
                }
            }else{
                if($beca->schoolarship > 0){
                    $bec = $pay->quantity*$beca->schoolarship;
                        $x = $bec/100;
                        $p = $pay->quantity-$x;
                        $q = $p*$discount->quantity;
                        $w = $q/100;
                        $price = $p-$w;
                }else{
                    $n = $pay->quantity*$discount->quantity;
                    $l = $n/100;
                    $price = $pay->quantity-$l;
                }
            }
        }

        return response()->json(['price' => $price]);
    }

    public function box_rec_up($id, Request $request)
    {
        $discount = DB::table('discounts')->select('quantity','id')->where('id',$request->discount)->first();
        $pay = DB::table('pays')->select('quantity','id','aument')->where('id',$request->pay)->first();
        $beca = DB::table('general_personal_datas')->select('general_personal_data_id','schoolarship')->where('general_personal_data_id',$request->student[0])->first();
        $fecha = Carbon::now();
        $dia = 10;
        $mes = $fecha->format('m');
        $year = $fecha->format('Y');
        $limit_date = $year.'/'.$mes.'/'.$dia;
        $limite = Carbon::parse($limit_date);
        $hoy = Carbon::now()->toDateString();
        if ($discount->quantity == 0) {
            $asignacion = AssingmentPay::find($id);
            $asignacion->pays_id = $request->pay[0];
            $asignacion->users_id = $request->student[0];
            $asignacion->discount = $request->discount[0];
            $asignacion->type_pay = $request->pago[0];
            if ($limite < $hoy) {
                if ($beca->schoolarship > 0) {
                    $r = $pay->quantity;
                    $bec = $beca->schoolarship;
                    $ds = $r*$bec;
                    $dss = $ds/100;
                    $cant = $r-$dss;
                    if($request->no_aument == 1){
                        //$aument = 0;
                        //$t = $cant*$aument;
                        //$t2 = $t/100;
                        //$r2 = $cant+$t2;
                        $asignacion->total = $cant;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = 0;
                        $asignacion->no_aument = 1;
                        $asignacion->update_reason = $request->Reason;
                        $asignacion->save();
                        return redirect()->route('box_admin'); 
                    }else{
                        $aument = $pay->aument;
                        $t = $cant*$aument;
                        $t2 = $t/100;
                        $r2 = $cant+$t2;
                        $asignacion->total = $r2;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = 0;
                        $asignacion->no_aument = 0;
                        $asignacion->update_reason = $request->Reason;
                        $asignacion->save();
                        return redirect()->route('box_admin'); 
                    } 
                }else{
                    if($request->no_aument == 1){
                        $r = $pay->quantity;
                        $asignacion->total = $r;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = 0;
                        $asignacion->no_aument = 1;
                        $asignacion->update_reason = $request->Reason;
                        $asignacion->save();
                        return redirect()->route('box_admin'); 
                    }else{
                        $r = $pay->quantity;
                        $aument = $pay->aument;
                        $t = $r*$aument;
                        $t2 = $t/100;
                        $r2 = $r+$t2;
                        $asignacion->total = $r2;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = $pay->aument;
                        $asignacion->no_aument = 0;
                        $asignacion->update_reason = $request->Reason;
                        $asignacion->save();
                        return redirect()->route('box_admin');    
                    }
                } 
            }else{
                if ($beca->schoolarship > 0) {
                    $r = $pay->quantity;
                    $bec = $beca->schoolarship;
                    $ds = $r*$bec;
                    $dss = $ds/100;
                    $cant = $r-$dss;
                    $asignacion->total = $cant;
                    $asignacion->comment = $request->Comentarios;
                    $asignacion->is_completed = 1;
                    $asignacion->date_pay = Carbon::now()->toDateString();
                    $asignacion->aument  = 0;
                    $asignacion->no_aument = 0;
                    $asignacion->update_reason = $request->Reason;
                    $asignacion->save();
                    return redirect()->route('box_admin'); 
                }
                else{
                    $r = $pay->quantity;
                    $asignacion->total = $r;
                    $asignacion->comment = $request->Comentarios;
                    $asignacion->is_completed = 1;
                    $asignacion->date_pay = Carbon::now()->toDateString();
                    $asignacion->aument  = 0;
                    $asignacion->no_aument = 0;
                    $asignacion->update_reason = $request->Reason;
                    $asignacion->save();
                    return redirect()->route('box_admin'); 
                }
            }
        }else{
            $asignacion = AssingmentPay::find($id);
            $asignacion->pays_id = $request->pay[0];
            $asignacion->users_id = $request->student[0];
            $asignacion->discount = $request->discount[0];
            $asignacion->type_pay = $request->pago[0];
            if ($limite < $hoy) {
                if ($beca->schoolarship > 0) {
                    $bec = $beca->schoolarship;
                    $t = $pay->quantity*$bec;
                    $ds = $t/100;
                    $cant = $pay->quantity-$ds;
                    $desc = $cant*$discount->quantity;
                    $r = $desc/100;
                    $r2 = $cant-$r;
                    if ($request->no_aument == 1) {
                        $asignacion->total = $r2;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = 0;
                        $asignacion->no_aument = 1;
                        $asignacion->update_reason = $request->Reason;
                        $asignacion->save();
                        return redirect()->route('box_admin'); 
                    }else{
                        $aument = $cant*$pay->aument;
                        $a2 = $aument/100;
                        $rt = $r2+$a2;
                        $asignacion->total = $rt;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = $pay->aument;
                        $asignacion->no_aument = 0;
                        $asignacion->update_reason = $request->Reason;
                        $asignacion->save();
                        return redirect()->route('box_admin');   
                    }
                    
                }else{
                    $t = $pay->quantity*$discount->quantity;
                    $r = $t/100;
                    $r2 = $pay->quantity-$r;
                    if ($request->no_aument == 1) {
                        $asignacion->total = $r2;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = $pay->aument;
                        $asignacion->no_aument = 1;
                        $asignacion->update_reason = $request->Reason;
                        $asignacion->save();
                        return redirect()->route('box_admin');
                    }else{
                        $aument = $pay->quantity*$pay->aument;
                        $a2 = $aument/100;
                        $rt = $r2+$a2;
                        $asignacion->total = $rt;
                        $asignacion->comment = $request->Comentarios;
                        $asignacion->is_completed = 1;
                        $asignacion->date_pay = Carbon::now()->toDateString();
                        $asignacion->aument  = $pay->aument;
                        $asignacion->no_aument = 0;
                        $asignacion->update_reason = $request->Reason;
                        $asignacion->save();
                        return redirect()->route('box_admin');   
                    }
                    
                }
            }else{
                if ($beca->schoolarship > 0) {
                    $bec = $beca->schoolarship;
                    $t = $pay->quantity*$bec;
                    $ds = $t/100;
                    $cant = $pay->quantity-$ds;
                    $t = $cant*$discount->quantity;
                    $r = $t/100;
                    $r2 = $cant-$r;
                    $asignacion->total = $r2;
                    $asignacion->comment = $request->Comentarios;
                    $asignacion->is_completed = 1;
                    $asignacion->date_pay = Carbon::now()->toDateString();
                    $asignacion->aument  = 0;
                    $asignacion->no_aument = 0;
                    $asignacion->update_reason = $request->Reason;
                    $asignacion->save();
                    return redirect()->route('box_admin'); 
                }
                else{
                  $t = $pay->quantity*$discount->quantity;
                  $r = $t/100;
                  $r2 = $pay->quantity-$r;
                  $asignacion->total = $r2;
                  $asignacion->comment = $request->Comentarios;
                  $asignacion->is_completed = 1;
                  $asignacion->date_pay = Carbon::now()->toDateString();
                  $asignacion->aument  = 0;
                  $asignacion->no_aument = 0;
                  $asignacion->update_reason = $request->Reason;
                  $asignacion->save();
                  return redirect()->route('box_admin');  
                } 
            }
        }
    }

    public function box_rec_delete($id)
    {
        $asignacion = AssingmentPay::find($id);
        $asignacion->delete();
        return redirect()->route('box_admin');
    }

    public function payticket($id)
    {
         $asignacion = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('pay_types','ap.type_pay','pay_types.id')->join('personal_references as pr', 'pr.personal_references_id', 'ap.users_id')->select('ap.id as folio','ap.pays_id','ap.users_id','ap.is_completed','ap.discount','ap.total','ap.date_pay','ap.comment','p.id','p.title','p.description','p.quantity','p.limit_date','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','pay_types.name as metodo','pr.name as padre')->where('ap.id', $id)->first();
        $pdf = PDF::loadView('adminlte::alumn_pay.pay_ticket',['asignacion' => $asignacion]);
        //return $pdf->stream();
        return $pdf->download('Pago'.' '.'#'.$id.' '.$asignacion->name.'.pdf');
    }

    public function payticket_r($id)
    {
        $asignacion = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('subjects', 'p.id_course','subjects.id')->join('pay_types','ap.type_pay','pay_types.id')->join('personal_references as pr', 'pr.personal_references_id', 'ap.users_id')->select('ap.id as assi','ap.users_id','ap.pays_id','ap.discount','ap.date_pay','ap.total','ap.is_completed','ap.comment','p.id','p.title','p.description','p.quantity','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','subjects.id as sub', 'subjects.name as subject','pay_types.name as metodo','ap.aument','pr.name as padre')->where('ap.id', $id)->first();
        $pdf = PDF::loadView('adminlte::alumn_pay.pay_ticket_r',['asignacion' => $asignacion]);
        //return $pdf->stream();
        return $pdf->download('Pago'.' '.'#'.$id.' '.$asignacion->name.'.pdf');
    }


    public function paysteacher()
    {
        $search = \Request::get('search');
        $teacher = DB::table('employees')->select('employee_id','name')->whereNotIn('employee_id', [ 263, 256, 258, 138, 264])->orderBy('name', 'asc')->where('name','LIKE','%'.$search.'%')->get();

       $hoy = Carbon::now()->toDateString();
       $fecha = Carbon::parse($hoy);
       $diferencia = $fecha->subDays(15);
       $a = DB::table('assistances as a')->join('groups','a.id_group','groups.id')->join('employees as e','a.id_teacher','e.employee_id')->select('groups.name as curso','a.id','groups.id as group', 'a.date','e.employee_id', 'a.id_teacher','a.id_group','e.name as teacher')->whereBetween('a.date', array($diferencia, $hoy))->get();

        $search2 = \Request::get('search2');
        $nomine = DB::table('payroll_teacher as p')->join('employees','p.user_id','employees.employee_id')->select('employees.name as teacher','p.name_pay','p.num_hour','p.quantity_pay','p.is_completed','p.comment','p.id','p.date')->where('employees.name','LIKE','%'.$search2.'%')->orderBy('p.id','DESC')->paginate(10);

        return view('adminlte::alumn_pay.Payroll_Teachers')->with(['teacher' => $teacher, 'a' => $a, 'nomine' => $nomine]);
    }

    public function nominecreate($id)
    {
       $hoy = Carbon::now()->toDateString();
       $fecha = Carbon::parse($hoy);
       $diferencia = $fecha->subDays(15);
        $assistance = DB::table('assistances')->where('id_teacher',$id)->whereBetween('assistances.date', array($diferencia, $hoy))->select(DB::raw('count(*) as user_count'))->first();
        $pay_standar = 100;
        $r = $pay_standar*$assistance->user_count;
        
        $nomine = new Payroll_Teachers;
        $nomine->name_pay = 'Nomina'.' '.Carbon::now()->format('Y-m-d');
        $nomine->user_id = $id;
        $nomine->id_group = 0;
        $nomine->num_hour = $assistance->user_count;
        $nomine->quantity_pay = $r;
        $nomine->comment = 'Nomina realizada exitosamente';
        $nomine->is_completed = 1;
        $nomine->date = Carbon::now()->toDateString();
        $nomine->save();

        //$assistances = DB::table('assistances')->where('id_teacher',$id)->delete();

        return redirect()->route('pays_teacher');
    }

    public function ticket_nomine($id)
    {
        $nomine = DB::table('payroll_teacher as p')->join('employees','p.user_id','employees.employee_id')->select('employees.name as teacher','p.name_pay','p.num_hour','p.quantity_pay','p.is_completed','p.comment','p.id','employees.employee_id','p.date')->where('p.id',$id)->first();
        $pdf = PDF::loadView('adminlte::alumn_pay.ticket_nomine',['nomine' => $nomine]);
        return $pdf->download('Nomina'.' '.'#'.$id.' '.$nomine->teacher.'.pdf');
        return redirect()->route('pays_teacher');

    }

    public function payroll_teacher()
    {
        $search = \Request::get('search');
        $nomine = DB::table('payroll_teacher as p')->join('employees','p.user_id','employees.employee_id')->select('employees.name as teacher','p.name_pay','p.num_hour','p.quantity_pay','p.is_completed','p.comment','p.id','p.date')->where('employees.employee_id',auth()->user()->id)->where('p.name_pay','LIKE','%'.$search.'%')->orderBy('p.id','DESC')->paginate(10);


        return view('adminlte::alumn_pay.PayTeachers')->with(['nomine' => $nomine]);
    }

    public function cut_box(Request $request)
    {
        $hoy = Carbon::now()->toDateString();
        $pays = DB::table('assingment_pays')->where('date_pay',$hoy)->select(DB::raw('count(*) as user_count'))->first();
        $total_efec = DB::table('assingment_pays')->where('date_pay',$hoy)->where('type_pay',1)->select(DB::raw('SUM(total) as tot_pays_efec'))->first();
        $total_transf = DB::table('assingment_pays')->where('date_pay',$hoy)->where('type_pay',2)->select(DB::raw('SUM(total) as tot_pays_transf'))->first();
        $total_cheque = DB::table('assingment_pays')->where('date_pay',$hoy)->where('type_pay',3)->select(DB::raw('SUM(total) as tot_pays_cheq'))->first();
        $total_cred = DB::table('assingment_pays')->where('date_pay',$hoy)->where('type_pay',4)->select(DB::raw('SUM(total) as tot_pays_cred'))->first();
        $total_deb = DB::table('assingment_pays')->where('date_pay',$hoy)->where('type_pay',5)->select(DB::raw('SUM(total) as tot_pays_deb'))->first();
        $total_depos = DB::table('assingment_pays')->where('date_pay',$hoy)->where('type_pay',6)->select(DB::raw('SUM(total) as tot_pays_depos'))->first();
        $total_other = DB::table('assingment_pays')->where('date_pay',$hoy)->where('type_pay',7)->select(DB::raw('SUM(total) as tot_pays_otros'))->first();
        $total = DB::table('assingment_pays')->where('date_pay',$hoy)->select(DB::raw('SUM(total) as tot_pays'))->first();

        $user = Auth::user()->id;
        $emplo = DB::table('employees')->select('id')->where('employee_id',$user)->first();
        $su = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id as sucursal', 'tree_job_centers.text as text')
        ->where('employee_tree_job_center.employee_id', $emplo->id)->first();
       

        $mil = $request->mil*1000;
        $quinientos = $request->quinientos*500;
        $doscientos = $request->doscientos*200;
        $cien = $request->cien*100;
        $cincuenta = $request->cincuenta*50;
        $veinte = $request->veinte*20;
        $diez = $request->diez*10;
        $cinco = $request->cinco*5;
        $dos = $request->dos*2;
        $uno = $request->uno*1;
        $cincuenta_centavos = $request->cincuenta_centavos*.50;
        $veinte_centavos = $request->veinte_centavos*.20;
        $diez_centavos = $request->diez_centavos*.10;
        $sum_arq = $mil+$quinientos+$doscientos+$cien+$cincuenta+$veinte+$diez+$cinco+$dos+$uno+$cincuenta_centavos+$veinte_centavos+$diez_centavos;
        $desf = $total_efec->tot_pays_efec-$sum_arq;

        $cut = new CutBox;
        $cut->name_cut = 'Corte de Caja'.' '.Carbon::now()->format('Y-m-d');
        $cut->users_id = auth()->user()->id;
        $cut->total = $total->tot_pays;
        $cut->num_cobros = $pays->user_count;
        $cut->total_efec = $total_efec->tot_pays_efec;
        $cut->total_transf = $total_transf->tot_pays_transf;
        $cut->total_cheq = $total_cheque->tot_pays_cheq;
        $cut->total_cred = $total_cred->tot_pays_cred;
        $cut->total_deb = $total_deb->tot_pays_deb;
        $cut->total_depos = $total_depos->tot_pays_depos;
        $cut->total_other = $total_other->tot_pays_otros;
        $cut->date = $hoy;
        $cut->arqueo = 1;
        $cut->depositado = 0;
        $cut->sucursal_id = $su->sucursal;
        $cut->total_arqueo = $sum_arq;
        $cut->desface = $desf;
        $cut->save();

        $arqueo = new Arqueo;
        $arqueo->users_id = auth()->user()->id;
        $arqueo->cutbox_id = $cut->id;
        $arqueo->mil = $request->mil;
        $arqueo->quinientos = $request->quinientos;
        $arqueo->doscientos = $request->doscientos;
        $arqueo->cien = $request->cien;
        $arqueo->cincuenta = $request->cincuenta;
        $arqueo->veinte = $request->veinte;
        $arqueo->diez = $request->diez;
        $arqueo->cinco = $request->cinco;
        $arqueo->dos = $request->dos;
        $arqueo->uno = $request->uno;
        $arqueo->cincuenta_centavos = $request->cincuenta_centavos;
        $arqueo->veinte_centavos = $request->veinte_centavos;
        $arqueo->diez_centavos = $request->diez_centavos;
        $arqueo->date = $hoy;
        $arqueo->total_arqueo = $sum_arq;
        $arqueo->save();

        return redirect()->route('cut_box_report');
    }

    public function cut_box_report()
    {
        $user = Auth::user()->id;
        $emplo = DB::table('employees')->select('employees.id')->where('employees.employee_id', '=', $user)->first()->id;
        $su = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id', 'tree_job_centers.text')
        ->where('employee_tree_job_center.employee_id', '=', $emplo)->get();
        $sucount = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id', 'tree_job_centers.text')
        ->where('employee_tree_job_center.employee_id', '=', $emplo)->count();

        $search = \Request::get('search');

        $cut = DB::table('cut_boxs as c')->join('users','c.users_id','users.id')->join('arqueos','c.id','arqueos.cutbox_id')->select('users.id','users.name','c.name_cut','c.users_id','c.total','c.num_cobros','c.date','c.id as box','arqueo','arqueos.id as arq','arqueos.cutbox_id','total_efec','total_transf','total_cheq','total_cred','total_deb','total_depos','total_other','depositado','desface','c.total_arqueo as sum_arq')->where('name_cut','LIKE','%'.$search.'%')->where('sucursal_id', $su[0]->tree_job_center_id)->orderBy('c.id','DESC')->paginate(10);

        $cut_sn = DB::table('cut_boxs as c')->join('users','c.users_id','users.id')->select('users.id','users.name','c.name_cut','c.users_id','c.total','c.num_cobros','c.date','c.id as box','arqueo')->where('sucursal_id', $su[0]->tree_job_center_id)->where('name_cut','LIKE','%'.$search.'%')->orderBy('c.id','DESC')->paginate(10);

        $arqueo = DB::table('arqueos')->select('id as arqueo','cutbox_id','total_arqueo')->get();
        $deposito = Deposito::all();
        
        return view('adminlte::alumn_pay.cut_box')->with(['cut'=>$cut,'arqueo'=>$arqueo,'cut_sn' => $cut_sn, 'deposito' => $deposito, 'sucursal' => $su, 'cont' => $sucount]);
    }

    public function cutsByJobCenter(Request $request){

        $user = Auth::user()->id;
        $emplo = DB::table('employees')->select('employees.id')->where('employees.employee_id', '=', $user)->first()->id;
        $su = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id', 'tree_job_centers.text')
        ->where('employee_tree_job_center.employee_id', '=', $emplo)->get();
        $sucount = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id', 'tree_job_centers.text')
        ->where('employee_tree_job_center.employee_id', '=', $emplo)->count();

        $search = \Request::get('search');

        $cut = DB::table('cut_boxs as c')->join('users','c.users_id','users.id')->join('arqueos','c.id','arqueos.cutbox_id')->select('users.id','users.name','c.name_cut','c.users_id','c.total','c.num_cobros','c.date','c.id as box','arqueo','arqueos.id as arq','arqueos.cutbox_id','total_efec','total_transf','total_cheq','total_cred','total_deb','total_depos','total_other','depositado','desface','c.total_arqueo as sum_arq')->where('name_cut','LIKE','%'.$search.'%')->where('sucursal_id', $request->selectEmployee)->orderBy('c.id','DESC')->paginate(10);

        $cut_sn = DB::table('cut_boxs as c')->join('users','c.users_id','users.id')->select('users.id','users.name','c.name_cut','c.users_id','c.total','c.num_cobros','c.date','c.id as box','arqueo')->where('name_cut','LIKE','%'.$search.'%')->where('sucursal_id', $request->selectEmployee)->orderBy('c.id','DESC')->paginate(10);

        $arqueo = DB::table('arqueos')->select('id as arqueo','cutbox_id','total_arqueo')->get();
        $deposito = Deposito::all();
        
        return view('adminlte::alumn_pay.cut_box')->with(['cut'=>$cut,'arqueo'=>$arqueo,'cut_sn' => $cut_sn, 'deposito' => $deposito, 'sucursal' => $su, 'cont' => $sucount]);
    }

    public function add_arqueo($idcut, Request $request)
    {
        $cut_box = DB::table('cut_boxs')->select('total_efec')->where('id',$idcut)->first();

        $mil = $request->mil*1000;
        $quinientos = $request->quinientos*500;
        $doscientos = $request->doscientos*200;
        $cien = $request->cien*100;
        $cincuenta = $request->cincuenta*50;
        $veinte = $request->veinte*20;
        $diez = $request->diez*10;
        $cinco = $request->cinco*5;
        $dos = $request->dos*2;
        $uno = $request->uno*1;
        $cincuenta_centavos = $request->cincuenta_centavos*.50;
        $veinte_centavos = $request->veinte_centavos*.20;
        $diez_centavos = $request->diez_centavos*.10;
        $sum_arq = $mil+$quinientos+$doscientos+$cien+$cincuenta+$veinte+$diez+$cinco+$dos+$uno+$cincuenta_centavos+$veinte_centavos+$diez_centavos;
        $desf = $cut_box->total_efec-$sum_arq;

        $arqueo = new Arqueo;
        $arqueo->users_id = auth()->user()->id;
        $arqueo->cutbox_id = $idcut;
        $arqueo->mil = $request->mil;
        $arqueo->quinientos = $request->quinientos;
        $arqueo->doscientos = $request->doscientos;
        $arqueo->cien = $request->cien;
        $arqueo->cincuenta = $request->cincuenta;
        $arqueo->veinte = $request->veinte;
        $arqueo->diez = $request->diez;
        $arqueo->cinco = $request->cinco;
        $arqueo->dos = $request->dos;
        $arqueo->uno = $request->uno;
        $arqueo->cincuenta_centavos = $request->cincuenta_centavos;
        $arqueo->veinte_centavos = $request->veinte_centavos;
        $arqueo->diez_centavos = $request->diez_centavos;
        $arqueo->date = Carbon::now()->toDateString();
        $arqueo->total_arqueo = $sum_arq;
        $arqueo->save();

        $cut = CutBox::find($idcut);
        $cut->users_id = auth()->user()->id;
        $cut->arqueo = 1;
        $cut->total_arqueo = $sum_arq;
        $cut->desface = $desf;
        $cut->save();

        return redirect()->route('cut_box_report');
    }

    public function add_deposito($idcut, Request $request)
    {
        $file = Input::file('ruta');
        $img = Image::make($file)->fit(600, 800, function ($constraint) { $constraint->upsize(); })->encode('jpeg');
        $deposito = new Deposito;
        $deposito->cutbox_id = $idcut;
        $deposito->users_id = auth()->user()->id;
        $deposito->folio = $request->folio;
        $deposito->cuenta = $request->cuenta;
        $deposito->aut = $request->aut;
        $deposito->banco = $request->cie;
        $deposito->total = $request->tot;
        $deposito->image = $img;
        $deposito->date = Carbon::now()->toDateString();
        $deposito->save();

        $cut = CutBox::find($idcut);
        $cut->depositado = 1;
        $cut->save();

        return redirect()->route('cut_box_report');
    }

    public function detail_box_pdf($id)
    {
       $cut = DB::table('cut_boxs as c')->select('name_cut','users_id','total','num_cobros','date','id','total_efec','total_transf','total_cheq','total_cred','total_deb','total_depos','total_other')->where('id',$id)->first();
       $format = Carbon::parse($cut->date)->toDateString();
       $box_unic = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('pay_types','ap.type_pay','pay_types.id')->select('ap.id as assi','ap.pays_id','ap.users_id','ap.is_completed','ap.discount','ap.total','ap.date_pay','ap.comment','p.id','p.title','p.description','p.quantity','p.limit_date','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','pay_types.name as metodo')->where('ap.date_pay',$format)->where('type',1)->get();
       $array_unic = $box_unic->toArray();
        $box_rec = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('subjects', 'p.id_course','subjects.id')->join('pay_types','ap.type_pay','pay_types.id')->join('general_personal_datas as gpa','users.id','gpa.general_personal_data_id')->select('ap.id as assi','ap.users_id','ap.pays_id','ap.discount','ap.date_pay','ap.total','ap.is_completed','ap.comment','p.id','p.title','p.description','p.quantity','p.limit_date','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','subjects.id as sub', 'subjects.name as subject','pay_types.name as metodo','ap.aument','gpa.schoolarship as beca')->where('ap.date_pay',$format)->where('type',2)->get();
       $arqueo = DB::table('arqueos')->select('id','cutbox_id','mil','quinientos','doscientos as d','cien','cincuenta','veinte','diez','cinco','dos','uno','cincuenta_centavos','veinte_centavos','diez_centavos','date')->where('cutbox_id',$id)->first();
       $mil = $arqueo->mil*1000;
       $quinientos = $arqueo->quinientos*500;
       $doscientos = $arqueo->d*200;
       $cien = $arqueo->cien*100;
       $cincuenta = $arqueo->cincuenta*50;
       $veinte = $arqueo->veinte*20;
       $diez = $arqueo->diez*10;
       $cinco = $arqueo->cinco*5;
       $dos = $arqueo->dos*2;
       $uno = $arqueo->uno*1;
       $cincuenta_centavos = $arqueo->cincuenta_centavos*.50;
       $veinte_centavos = $arqueo->veinte_centavos*.20;
       $diez_centavos = $arqueo->diez_centavos*.10;
       $sum_arq = $mil+$quinientos+$doscientos+$cien+$cincuenta+$veinte+$diez+$cinco+$dos+$uno+$cincuenta_centavos+$veinte_centavos+$diez_centavos;
       $desface = $cut->total_efec-$sum_arq;

       $pdf = PDF::loadView('adminlte::alumn_pay.detail_box_pdf',['cut' => $cut,'box_unic'=>$box_unic,'box_rec'=>$box_rec, 'arqueo' => $arqueo, 'sum_arq' => $sum_arq, 'desface' => $desface, 'array_unic', $array_unic]);
       return $pdf->stream();
        //return $pdf->download('Corte de Caja'.' '.'#'.$id.' '.$cut->date.'.pdf');
        //return redirect()->route('cut_box_report');
    }

    public function desface_pay($id)
    {
        $cut = DB::table('cut_boxs as c')->join('users','c.users_id','users.id')->join('arqueos','c.id','arqueos.cutbox_id')->select('users.id','users.name','users.email','c.name_cut','c.users_id','c.total','c.num_cobros','c.date','c.id as box','arqueo','arqueos.id as arq','arqueos.cutbox_id','total_efec','total_transf','total_cheq','total_cred','total_deb','total_depos','total_other','depositado','desface')->where('c.id',$id)->first();
        $des_letter = NumerosEnLetras::convertir($cut->desface);
        setlocale(LC_TIME,"es_ES");
        $fecha = Carbon::now();
        $fecha_pago = Carbon::now()->addDays(3);
        $dia = $fecha_pago->format('l j F Y');
        $dia2 = $fecha->format('l j F Y');
        $pdf = PDF::loadView('adminlte::alumn_pay.desface_pay',['cut' => $cut,'fecha'=>$fecha,'fecha_pago' => $fecha_pago, 'dia' => $dia,'dia2' =>$dia2,'des_letter'=> $des_letter]);
        return $pdf->download('Pagare'.' '.'#'.$id.'.pdf');
        return redirect()->route('cut_box_report');

    }

    public function details_depos($id)
    {
        $deposito = DB::table('depositos as d')->join('users','d.users_id','users.id')->join('cut_boxs','d.cutbox_id','cut_boxs.id')->select('cut_boxs.name_cut','users.name','d.date','d.folio','d.aut','d.banco','d.total','d.image','d.id as depos')->where('d.cutbox_id',$id)->first();

        return view('adminlte::alumn_pay.details_depos')->with(['deposito'=>$deposito]);

    }

    public function details_box($id)
    {
        $cut = DB::table('cut_boxs as c')->select('name_cut','users_id','total','num_cobros','date','id')->where('id',$id)->first();
        $box_unic = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('pay_types','ap.type_pay','pay_types.id')->select('ap.id as assi','ap.pays_id','ap.users_id','ap.is_completed','ap.discount','ap.total','ap.date_pay','ap.comment','p.id','p.title','p.description','p.quantity','p.limit_date','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','pay_types.name as metodo')->where('ap.date_pay',$cut->date)->paginate(10);
        $box_rec = DB::table('assingment_pays as ap')->join('pays as p','p.id','ap.pays_id')->join('users','users.id','ap.users_id')->join('discounts as d', 'd.id', 'ap.discount')->join('subjects', 'p.id_course','subjects.id')->join('pay_types','ap.type_pay','pay_types.id')->join('general_personal_datas as gpa','users.id','gpa.general_personal_data_id')->select('ap.id as assi','ap.users_id','ap.pays_id','ap.discount','ap.date_pay','ap.total','ap.is_completed','ap.comment','p.id','p.title','p.description','p.quantity','p.limit_date','p.type','users.id','users.name','d.id','d.title as descuento', 'd.quantity as desc_cant','subjects.id as sub', 'subjects.name as subject','pay_types.name as metodo','ap.aument','gpa.schoolarship as beca')->where('ap.date_pay',$cut->date)->paginate(10);

            return view('adminlte::alumn_pay.Details_box')->with(['cut'=>$cut,'box_unic' => $box_unic, 'box_rec' => $box_rec]);

    }

    public function details_np($id)
    {
       $hoy = Carbon::now()->toDateString();
       $fecha = Carbon::parse($hoy);
       $diferencia = $fecha->subDays(15);
       $teacher = DB::table('employees')->select('name')->where('employee_id',$id)->first();
       $students = AssistanceS::all();
       $a = DB::table('assistances as a')->join('groups','a.id_group','groups.id')->join('employees as e','a.id_teacher','e.employee_id')->select('groups.name as curso','a.id','groups.id as group', 'a.date','e.employee_id', 'a.id_teacher','a.id_group','e.name as teacher')->where('a.id_teacher',$id)->whereBetween('a.date', array($diferencia, $hoy))->get();

       return view('adminlte::alumn_pay.Details_np')->with(['a' => $a,'teacher' => $teacher,'students' => $students]);
    }

    public function details_npp($id)
    {
       $nomine = DB::table('payroll_teacher as pt')->join('employees','pt.user_id','employees.employee_id')->select('employees.name as teacher','pt.name_pay','pt.user_id','pt.num_hour','pt.quantity_pay','pt.comment','pt.is_completed','pt.date')->where('pt.id',$id)->first();
       $teacher = DB::table('employees')->select('name','employee_id')->where('employee_id',$nomine->user_id)->first();
       $hoy = $nomine->date;
       $fecha = Carbon::parse($hoy);
       $diferencia = $fecha->subDays(15);
       $students = AssistanceS::all();

       $a = DB::table('assistances as a')->join('groups','a.id_group','groups.id')->join('employees as e','a.id_teacher','e.employee_id')->select('groups.name as curso','a.id','groups.id as group', 'a.date','e.employee_id', 'a.id_teacher','a.id_group','e.name as teacher')->where('a.id_teacher',$teacher->employee_id)->whereBetween('a.date', array($diferencia, $hoy))->get();

       return view('adminlte::alumn_pay.Details_npp')->with(['a' => $a,'students' => $students,'teacher' => $teacher]);
    }

    /**
     * Reporte de Adeudos
     */
    public function reporteAdeudos()
    {
        $inscripciones = DB::table('assingment_pays as ins')->join('users as u', 'ins.users_id', 'u.id')->join('role_user', 'ins.users_id', 'role_user.user_id')->join('pays','pays.id','ins.pays_id')->select('u.id as users_id', 'u.name', 'date_pay','pays.type')->where('pays.type', 1)->where('role_user.status', 1)->get();
        $array = [];
        $date = Carbon::now();
        $dateMes = $date->format('m');
        $dateDia = $date->format('d');
        $año = $date->format('Y');
        
        
        foreach ($inscripciones as $inscripcion) {
            $mensualidades = DB::table('assingment_pays as ap')->join('pays','pays.id','ap.pays_id')->select('ap.users_id', 'ap.date_pay as date', 'ap.total as tot','pays.type')->where('pays.type', 2)->where('ap.users_id', $inscripcion->users_id)->orderBy('date','DESC')->get()->last();

            $fecha = Carbon::parse($mensualidades->date);
            $ultimaMensualidad = $fecha->format('m');

            $mes_mens = Carbon::parse($mensualidades->date);
            $mfecha = $mes_mens->month;
            $fechax = $mes_mens->diff($date);
            $resta = $fechax->m;
            //$resta = $dateMes - $mfecha; 

            $monto = $mensualidades->tot * $resta;

            if ($dateDia > 10) {
                if ($ultimaMensualidad != $dateMes) {
                    setlocale(LC_TIME, 'es_ES');
                        $mfecha++;
                        if ($mfecha == 13) {
                            $mfecha = 1;
                        }
                    for ($i = 0; $mfecha <= $dateMes; $mfecha++) {
                        

                        $fecha = DateTime::createFromFormat('!m', $mfecha);
                        $mes = strftime("%B", $fecha->getTimestamp()); // marzo

                        $debits = DB::table('debit_reports')->select('user_id')->where('user_id', $mensualidades->users_id)->where('description', 'Mensualidad ' . $mes . ' ' . $año)->select(DB::raw('count(user_id) as user'))->first();

                        $schoolarship = DB::table('general_personal_datas')->select('schoolarship')->where('general_personal_data_id', $mensualidades->users_id)->first();

                        $desc = 660 * $schoolarship->schoolarship / 100;
                        $mount = 660 - $desc;
                        $cargo = $mount * 5 / 100;

                        if ($debits->user == 0) {
                            $debit = new DebitReport();
                            $debit->user_id = $mensualidades->users_id;
                            $debit->type_pay = 2;
                            $debit->description = 'Mensualidad ' . $mes . ' ' . $año;
                            $debit->mount = $mount;
                            $debit->cargo = $cargo;
                            $debit->mount_total = $mount + $cargo;
                            $debit->status = 0;
                            $debit->save();
                        }
                    }
                }
                else {
                    
                }
            }

            $debitsReport = DB::table('debit_reports')->select('mount_total')->where('user_id', $mensualidades->users_id)->get();
            
            $debitTotal = 0;
            foreach ($debitsReport as $debit) {
                $debitTotal = $debitTotal + $debit->mount_total;
            }

            $mensualidades = ['id_user' => $mensualidades->users_id, 'name' => $inscripcion->name, 'ultima_mens' => $mensualidades->date, 'adeudos' => $resta, 'monto' =>  $debitTotal];

            if ($resta != 0) {
                array_push($array, $mensualidades);
            }
        

        }
        
        $adeudos = collect($array);
        //dd($adeudos);
        //dd($adeudos[0]['meses'][0]['mes']);
        //dd($adeudos[0]['meses'][0]['mes']);
        return view('adminlte::alumn_pay.report_adeudos')->with(['adeudos' => $adeudos]);
    }

    /**
     * Retorna todos los adeudos con detalle por id de usuario.
     * @param integer idUser
     */
    public function reporteAdeudosByuser($idUser){
        $debitsDetail = DB::table('debit_reports')->where('user_id', $idUser)->get();
        $total = 0;
        foreach ($debitsDetail as $debitDetail) {
            $total = $total + $debitDetail->mount_total;
        }
        return view('adminlte::alumn_pay.report_adeudos_detail')->with(['debits' => $debitsDetail, 'total' => $total]);
    }
}