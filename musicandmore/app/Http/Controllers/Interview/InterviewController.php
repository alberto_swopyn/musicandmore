<?php

namespace Swopyn\Http\Controllers\Interview;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use DB;
use Validator; 
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Swopyn\AnswerUser;
use Swopyn\SectionGrade;
use Swopyn\User;
use Swopyn\Tracking;

class InterviewController extends Controller
{
	// public function create($usuario){
	// 	$temp = DB::table('general_personal_datas')->get();
	// 	$user = User::find($usuario);
	// 	$sections = DB::table('sections')->where('commands_id',2)->get();
	// 	$questions = array();
	// 	$questionSupports = array();
	// 	$answers = array();
	// 	$answersUser = array();
	// 	foreach ($sections as $key => $sec) {
	// 		$tmp = DB::table('questions')->where('sections_id',$sec->id)->get();
	// 		foreach ($tmp as $key) {
	// 			$tmpQuestions = DB::table('question_supports')->where('questions_id',$key->id)->get();
	// 			$tmpAnswers = DB::table('answers')->where('questions_id',$key->id)->get();
	// 			$tempA = AnswerUser::where(AnswerUser::UsersId, $usuario)->where(AnswerUser::QuestionsId, $key->id)->get();
	// 			foreach ($tmpQuestions as $qsuppport) {
	// 				array_push($questionSupports, $qsuppport);
	// 			}
	// 			foreach ($tmpAnswers as $answer) {
	// 				array_push($answers, $answer);
	// 			}
	// 			foreach ($tempA as $userAnswer) {
	// 				array_push($answersUser, $userAnswer);
	// 			}
	// 			array_push($questions, $key);
	// 		}
	// 	}

	// 	return view('adminlte::interview_operative.create')->with(['sections' => $sections, 'questions' => $questions, 'supports' => $questionSupports, 'answers' => $answers, 'usersInterview' => $user, 'userAnswers' => $answersUser]);
	// }

	public function create($usuario){
		$user = User::find($usuario);
		$sections = DB::table('sections')->where('commands_id',3)->get();
		$tmp = DB::table('sections')->join('question_seconds as qs', 'sections.id', 'qs.sections_id')->where('commands_id', 3)->select('qs.*')->get();
		$tempA = AnswerUser::where(AnswerUser::UsersId, $usuario)->get();
		return view('adminlte::interview_operative.create')->with(['sections' => $sections, 'questions' => $tmp, 'usersInterview' => $user, 'answerUsers' => $tempA]);
	}

	protected $rules =[
		'Answers' => 'Required',
	];

	public function save(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} else {
			$suma = 0;
			$contador = 0;
			foreach ($request->Answers as $key => $answer) {
				$temp = AnswerUser::where(AnswerUser::QuestionsId, $answer['idQuestion'])->where(AnswerUser::UsersId, $request->UserId)->first();
				if (count($temp) > 0) 
					$tmp = AnswerUser::find($temp->id);
				else
					$tmp = new AnswerUser;
				$tmp->users_id = $request->UserId;
				$tmp->questions_id = $answer['idQuestion'];
				$tmp->sections_id = $request->SectionId;
				$tmp->value = $answer['valor'];
				$tmp->save();
				$suma += $answer['valor'];
				$contador += 1;
			}
			$this->grades($request->UserId, $suma, $request->SectionId, $contador, $request->Validator);
			return response()->json();
		}	
	}

	public function grades($user, $suma, $section, $cont, $pass)
	{
		$tmp = DB::table('section_grades')->where(SectionGrade::SectionId, $section)->where(SectionGrade::UsersId, $user)->first();
		$val = DB::table('sections')->where('id', $section)->select('value')->first();
		$grade = ($suma*$val->value) / ($cont*3);
		if (count($tmp) <= 0) 
			$sec = new SectionGrade;
		else
			$sec = SectionGrade::find($tmp->id);
		$sec->users_id = $user;
		$sec->grade = $grade;
		$sec->sections_id = $section;
		$sec->save();
		if($pass == 'true'){
			$prom = DB::table('section_grades')->where(SectionGrade::UsersId, $user)->sum('grade');
			$tracking = Tracking::where('user_id', $user)->first();
			$tracking->interview = $prom;
			$tracking->save();
		}
	}
}
