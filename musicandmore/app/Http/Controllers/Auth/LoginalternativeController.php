<?php

namespace Swopyn\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Swopyn\User;
use Validator; 
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;

class LoginalternativeController extends Controller
{
	public function index(){
		return view('adminlte::auth.login_alternative');
	}
	public function update(Request $request){
		$temp = DB::table('users')->where('id', $request->user)->first();
		$db = User::find($temp->id);
		$contraseña = $password = bcrypt($request->pass_auth);
		$db->email = $request->correo;
		$db->password = $contraseña;
		$db->save();
		return response()->json($db);
	}

	private $rules=[
	'user' => 'Required',
	'pass' => 'Required',
	];

	public function search(Request $request){
		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} else {
			$temp = DB::table('users')->where('users_id', $request->user)->first();
			if(count($temp)>0){
				if (Hash::check($request->pass,$temp->password)) {
					if ($temp->email == $temp->users_id) {
						return response()->json($temp);
					}
					else
						return Response::json(array('errors' => ['EmailExists'=>'Ya se has registrado un correo valido']));
				}
				else{
					return Response::json(array('errors' => ['Auth'=>'Corroborar información de acceso']));
				}
			}
			else
				return Response::json(array('errors' => ['Auth'=>'Corroborar información de acceso']));
		}
	}
}
