<?php

namespace Swopyn\Http\Controllers\Auth;

use Swopyn\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DTO\Web\CodeResponseDTO;
use DTO\Web\LoginResponseDTO;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        attemptLogin as attemptLoginAtAuthenticatesUsers;
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('adminlte::auth.login');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/training/study';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Returns field name to use at login.
     *
     * @return string
     */
    public function username()
    {
        return config('auth.providers.users.field','email');
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        if ($this->username() === 'email') return $this->attemptLoginAtAuthenticatesUsers($request);
        if ( ! $this->attemptLoginAtAuthenticatesUsers($request)) {
            return $this->attempLoginUsingUsernameAsAnEmail($request);
        }
        return false;
    }

    /**
     * Attempt to log the user into application using username as an email.
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attempLoginUsingUsernameAsAnEmail(Request $request)
    {
        return $this->guard()->attempt(
            ['email' => $request->input('username'), 'password' => $request->input('password')],
            $request->has('remember'));
    }

    /**
     * Valida los datos de intento de sesión desde app para generar el login.
     */
    public function authenticate($email, $password)
    {
            
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                $user = Auth::user();
                $idUser = DB::table('users')->select('id', 'name', 'email')->where('email', $email)->first();
                $user = DB::table('employees as e')
                ->join('companies as c', 'e.id_company', '=', 'c.id_company')
                ->join('role_user as roles', 'e.employee_id', '=', 'roles.user_id')
                ->select('e.employee_id', 'e.id_company', 'c.name', 'role_id')
                ->where('e.employee_id', $idUser->id)->first();
                $loginResponse = ['loginResponse' => 1, 'employee_id' => $user->employee_id, 'user_name' => $idUser->name, 'email' => $idUser->email, 'id_company' => $user->id_company, 'company_name' => $user->name, 'role_id' => $user->role_id];
                $collection = collect($loginResponse);
                echo $collection->toJson();
            }
            else
            {
                $loginResponse = ['loginResponse' => 0];
                $collection = collect($loginResponse);
                echo $collection->toJson();
            }
        }
    }
