<?php

namespace Swopyn\Http\Controllers\Employees;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Swopyn\User;
use Illuminate\Support\Facades\Storage;
use Swopyn\Employee;
use Illuminate\Support\Facades\Input;
use Validator;

class EmployeeController extends Controller
{

	protected $rules =
	[
		'nameEmployee'   => 'Required',
		'inputjobTitle'  => 'Required',
		'inputJobCenter' => 'Required'
	];

	public function kardex(){
		$employee = DB::table('employees as e')
		->join('companies as com', 'e.id_company', '=', 'com.id')
		->join('users as u','e.employee_id','=','u.id')
		->join('job_title_profiles as jp','e.job_title_profile_id', '=', 'jp.id')
		->join('profile_job_centers as jc', 'e.profile_job_center_id', '=','jc.id')
		->join('tree_job_centers as tjc', 'jc.profile_job_centers_id', '=', 'tjc.id_inc')
		->select('e.*', 'u.email', 'jp.job_title_name as jobTitleName', 'jc.name as jobCenter', 'jc.profile_job_centers_id', 'jp.job_title_profiles_id' ,'com.name as company', 'tjc.parent as Zone')->get();
		$jt = DB::table('job_title_profiles')->get();
		$jc = DB::table('profile_job_centers')->get();
		$zones = DB::table('tree_job_centers')->get();
		$usr = DB::table('employees')->where('employee_id', auth()->user()->id)->first();
		$trainingJobs = DB::table('job_title_profile_training_plan as jtp')
		->join('training_training_plan as ttp','ttp.training_plan_id','jtp.training_plan_id')
		->join('trainings','ttp.training_id','trainings.id')->where('job_title_profile_id',$usr->job_title_profile_id)->select('trainings.*','jtp.training_plan_id as TrainingPlan')->get();
		$trainingsUser = DB::table('training_plans as tp')->join('training_plan_user as tpu','tp.id','tpu.training_plan_id')->join('training_training_plan as ttp','tp.id','ttp.training_plan_id')->join('trainings','ttp.training_id','trainings.id')->where('user_id',auth()->user()->id)->select('trainings.*','ttp.training_plan_id as TrainingPlan')->get();

		$training = $trainingJobs->merge($trainingsUser)->unique();

		$array = array();
		foreach ($training as $t) {
			$isConteined = false;
			foreach ($array as $item) {
				if($t->TrainingPlan == $item)
					$isConteined = true;
			}
			if(!$isConteined)
				array_push($array,$t->TrainingPlan);
		}

		$training->count();
		$users = User::all();
		$countCourses = DB::table('training_plans as tp')->whereIn('id', $array)->get();
		$progress = DB::table('course_progresses')->where('user_id', $usr->employee_id)->get();
		$evaluations = DB::table('training_user_grades')->where('user_id', $usr->employee_id)->get();

		return view('adminlte::employees.kardex', ['employee' => $employee, 'zones' => $zones, 'trainings' => $training, 'progress' => $progress, 'usuario' => $usr, 'plans' => $countCourses, 'evaluations' => $evaluations, 'jobTitle' => $jt, 'jobCenter' => $jc, 'users' => $users]);
	}

	public function kardexEvaluations($jobCenterID, $jobTitle){

		$employee = DB::table('employees as e')
		->join('companies as com', 'e.id_company', '=', 'com.id')
		->join('users as u','e.employee_id','=','u.id')
		->join('job_title_profiles as jp','e.job_title_profile_id', '=', 'jp.id')
		->join('profile_job_centers as jc', 'e.profile_job_center_id', '=','jc.id')
		->join('tree_job_centers as tjc', 'jc.profile_job_centers_id', '=', 'tjc.id_inc')
		->where('tjc.id_inc',$jobCenterID)
		->orWhere('jp.job_title_profiles_id',$jobTitle)
		->select('e.*', 'u.email', 'jp.job_title_name as jobTitleName', 'jc.name as jobCenter', 'jc.profile_job_centers_id', 'com.name as company', 'tjc.parent as Zone')->get();

		$progress = DB::table('course_progresses');
		$grades = DB::table('training_user_grades');

		$zones = DB::table('tree_job_centers')->get();

		return view('adminlte::employees.generalkardex', ['employee' => $employee, 'zones' => $zones, 'progress' => $progress, 'grades' => $grades]);
	}

	public function kardexSucursales($search)
	// {
	// 	$raw;
	// 	$column;
	// 	$title = $search;
	// 	if ($search == 'jobcenters') {
	// 		// $raw = DB::table('tree_job_centers as tree')->join('address_job_centers as address', 'address.address_job_centers_id', 'tree.id_inc')->get();
	// 		$raw = DB::table('tree_job_centers as tree')->join('tree_job_centers as t', 't.id', 'tree.parent')->select('tree.*', 't.text as padre')->get();
	// 		$column = DB::table('employees')->join('profile_job_centers as jobTreeCenter','jobTreeCenter.id','employees.profile_job_center_id')->select('jobTreeCenter.profile_job_centers_id as jobCenterID', DB::raw('AVG(course_percentage) as promedio'))->groupBy('profile_job_centers_id')->get();
	// 	}
	// 	elseif ($search == 'jobtitles'){
	// 		$raw = DB::table('tree_job_profiles')->get();
	// 		$column = DB::table('employees')->join('job_title_profiles as jobTitle','jobTitle.id','employees.job_title_profile_id')->select('jobTitle.job_title_profiles_id as jobCenterID', DB::raw('AVG(course_percentage) as promedio'))->groupBy('job_title_profiles_id')->get();
	// 	}
	// 	return view('adminlte::employees.generalsearch', ['raw' => $raw, 'column' => $column, 'title' => $title]);
	// }
	{
		$raw;
		$column;
		$title = $search;
		if ($search == 'jobcenters') {
			$raw = DB::table('tree_job_centers')->where('id',1)->get();
			$region;
			
			$region1 = DB::table('tree_job_centers')->where('tree_job_centers.parent', 1 )->get();
			$regiones = array();
			$zonas = array();
			$determinantes = array();
			$sucursal = array();
			$empleado = array();

			foreach ($region1 as $cont => $region ) {
				$sumzonas=0;

				$zona1 = DB::table('tree_job_centers')->where('parent', $region->id)->get();
				
				foreach ($zona1 as $key => $zona) {
					$det1 = DB::table('tree_job_centers')->where('parent',$zona->id)->get();
					$sumdet = 0;
					
					foreach ($det1 as $contDet => $det) {
						
						$suc1 = DB::table('tree_job_centers')->where('parent', $det->id)->get();
						
						$sum = 0;

						foreach ($suc1 as $contsuc => $suc) {

							$temp = DB::table('employees')->join('profile_job_centers as jobTreeCenter','jobTreeCenter.id','employees.profile_job_center_id')->where('jobTreeCenter.profile_job_centers_id', $suc->id_inc)->select(DB::raw('AVG(course_percentage) as promedio'))->first();
							
							$promedio = 0;
							if($temp->promedio != null)
								$promedio = $temp->promedio;
							array_push($sucursal, ['text' => $suc->text , 'id' => $suc->id, 'parent_id' => $det->id, 'parent' => $det->text, 'promedio' => $promedio]);
							$emp1 = DB::table('employees')->join('profile_job_centers as jobTreeCenter','jobTreeCenter.id','employees.profile_job_center_id')->where('jobTreeCenter.profile_job_centers_id', $suc->id_inc)->get();
							
							foreach ($emp1 as $contemp => $emp) {
								$empleado1 = DB::table('employees')->where('employee_id',$emp->employee_id)->first();	
								
								array_push($empleado, ['text' => $empleado1->name , 'id' => $empleado1->employee_id, 'parent_id' => $suc->id, 'promedio' => $empleado1->course_percentage]);

							}

						}
						

						for ($i=0; $i < count($sucursal); $i++) { 
							if($sucursal[$i]['parent_id'] == $det->id)
								$sum += (float) $sucursal[$i]['promedio'];
						}
						if(count($suc1)>0)
						array_push($determinantes, ['text' => $det->text , 'id' => $det->id, 'parent_id' => $zona->id, 'parent' => $zona->text, 'promedio' => ($sum / count($suc1))]);
					else
						array_push($determinantes, ['text' => $det->text , 'id' => $det->id, 'parent_id' => $zona->id, 'parent' => $zona->text, 'promedio' => 0]);
					}
					for ($i=0; $i < count($determinantes); $i++) { 
						if($determinantes[$i]['parent_id'] == $zona->id)
							$sumdet += (float) $determinantes[$i]['promedio'];
					}
					if(count($det1)>0){
						array_push($zonas, ['text' => $zona->text , 'id' => $zona->id, 'parent_id' => $region->id, 'parent' => $region->text, 'promedio' => ($sumdet / count($det1))]);
					}
					else{
						array_push($zonas, ['text' => $zona->text , 'id' => $zona->id, 'parent_id' => $region->id, 'parent' => $region->text, 'promedio' => 0]);	
					}
				}
				for ($i=0; $i < count($zonas); $i++) { 
					if($zonas[$i]['parent_id'] == $region->id)
						$sumzonas += (float) $zonas[$i]['promedio'];
				}
				array_push($regiones, ['text' => $region->text , 'id' => $region->id, 'parent' => 'Circle K', 'promedio' => ($sumzonas / count($zona1))]);
			}
			$column = $regiones;
						
						return view('adminlte::employees.generalsearch', ['column' => $column,'zonas' => $zonas, 'determinantes' => $determinantes, 'sucursal' => $sucursal,'empleado' => $empleado, 'title' => $search]);
		}
		elseif ($search == 'jobtitles'){
			$raw = DB::table('tree_job_profiles')->get();
			$column = DB::table('employees')->join('job_title_profiles as jobTitle','jobTitle.id','employees.job_title_profile_id')->select('jobTitle.job_title_profiles_id as jobCenterID', DB::raw('AVG(course_percentage) as promedio'))->groupBy('job_title_profiles_id')->get();
		}
		
		//return view('adminlte::employees.generalsearch', ['raw' => $raw, 'column' => $column, 'title' => $title]);
	}

	public function index($idEmployee)
	{
		
		$employee = null;
		if ($idEmployee != 0)
		{
			$employee = DB::table('employees as e')->join('companies as c','c.id', '=', 'e.id_company')->select('e.name','e.id as id', 'e.job_title_profile_id as jobTitle', 'e.profile_job_center_id as jobCenter', 'c.name as company')->where('e.id', $idEmployee)->first();
		}
		else{
			$employee = null;
		}
		$jt = DB::table('job_title_profiles')->get();
		$jc = DB::table('profile_job_centers')->get();
		return view('adminlte::employees.index')->with(['jobTitle' => $jt, 'jobCenter' => $jc, 'employee' => $employee]);
	}

	public function update(Request $request)
	{
		$employee_count = DB::table('employees')->select('profile_job_center_id')->where('profile_job_center_id', $request->inputJobCenter)->count();
		$total = DB::table('profile_job_centers')->select('total')->where('profile_job_centers_id',$request->inputJobCenter)->first();
		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} 
		else {
			if ($employee_count <= $total->total) {
				$temp = Employee::where('employee_id',$request->Applicant_id)->first();
				if (count($temp) <= 0) {
					$tmp = new Employee;
					$tmp->employee_id = $request->Applicant_id;
					$tmp->id_company = 1;
				}
				else{
					$tmp = Employee::find($temp->id);
				}

				$tmp -> name = $request->nameEmployee;
				//$tmp -> key_employee = $request-> keyEmployee;
				$tmp -> job_title_profile_id = $request->inputjobTitle;
				$tmp -> profile_job_center_id = $request->inputJobCenter;
				$tmp -> save();

				return redirect()->route('kardex_employees');
			}
		}
	}

	public function searchEmployee(Request $request)
	{
		$tempo = DB::table('employees')->join('companies as c','c.id','employees.id_company')->select('employees.name','c.name as inputCompany', 'job_title_profile_id as inputjobTitle', 'profile_job_center_id as inputJobCenter')->where('employee_id', $request->Employee_id)->first();
		return response()->json($tempo);
	}

	public function createEmployee(Request $request){
		$employee = new Employee;
		$employee->employee_id = $request->idEmployee;
		$employee->name = $request->nameEmployee;
		$employee->id_company = 1;
		$employee->job_title_profile_id = $request->inputjobTitle;
		$employee->save();

		
		/**
		 * Relación muchos a muchos: "sync" genera en automatico el insert a la(s) tablas pivote, por lo tanto si da error
		 * REVISAR EL ARCHIVO LARAVEL.LOG PARA VER EL NOMBRE DE LA TABLA Y CAMPOS <-- IMPORTANTE
		 */
		$employee->jobCenter()->sync($request->inputJobCenter); 
		
		//Check if user got saved
		if ($employee->save())
		{
			return response()->json($employee);
		}
	}

}
