<?php

namespace Swopyn\Http\Controllers\Paytype;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;

use Swopyn\PayType;
class PaytypeController extends Controller
{
    //

     public function create(){
     	
    	return view('adminlte::paytype.create');
    }


    public function add(Request $request){

        $this->validate($request,[
            '' => 'required'
            ]);

    	$carrera = new PayType;
    	$carrera->name = $request->get('txtName');
    	$carrera->save();

    	return view('adminlte::paytype.create');
    

    }
}
