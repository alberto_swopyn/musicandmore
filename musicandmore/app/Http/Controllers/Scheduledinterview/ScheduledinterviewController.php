<?php

namespace Swopyn\Http\Controllers\Scheduledinterview;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\GeneralPersonalData;
use Swopyn\ScheduledInterview;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Swopyn\Http\Requests\Scheduledinterview\ScheduledinterviewRequest;
use Validator;

class ScheduledinterviewController extends Controller
{
    private $rules =[
        'Applicant_id' => 'Required',
        'Fecha_entrevista' => 'Required',
        'Hora_entrevista' => 'Required',
        'Entrevistador' => 'Required',
        'Lugar_entrevista' => 'Required',
    ];
    public function create(){
      $solicitante = DB::table('general_personal_datas')
      ->join('trackings', 'general_personal_data_id', '=', 'trackings.user_id')->select('general_personal_datas.*')->where('psychometric', '!=', NULL)->get();  
      
      return view('adminlte::scheduledinterview.create')->with(['applicants'=>$solicitante]);
  }

  public function userview(){
    $entrevista = ScheduledInterview::all();
    return view('adminlte::scheduledinterview.user_view');
}

public function add(Request $request){
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
        $temp = DB::table('scheduled_interviews')->where('general_personal_data_id', $request->Applicant_id)->first();
        if (count($temp)<=0) {
            $entrevista = new ScheduledInterview;
            $entrevista -> general_personal_data_id = $request ->Applicant_id;    
        }
        else{
            $entrevista = ScheduledInterview::find($temp->id);
        }
        $entrevista -> interview_date = $request ->Fecha_entrevista;
        $entrevista -> interview_time = $request ->Hora_entrevista;
        $entrevista -> interviewer_name = $request ->Entrevistador;
        $entrevista -> interview_place = $request ->Lugar_entrevista;

        $entrevista -> save();
        return response()->json($entrevista);
    }
}
public function search(Request $request)
{
    $tmp = DB::table('scheduled_interviews')->select('general_personal_data_id as applicant_id', 'interview_date as dateInterview', 'interview_time as timeInterview', 'interviewer_name as interviewerAdd', 'interview_place as JobcenterInterview')->where('general_personal_data_id', $request->Applicant_id)->first();
    return response()->json($tmp);
}
}
