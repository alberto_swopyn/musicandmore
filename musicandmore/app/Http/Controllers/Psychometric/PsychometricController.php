<?php

namespace Swopyn\Http\Controllers\Psychometric;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Swopyn\PsychometricTest;
use Swopyn\AnswersPsychometricTest;
use Image;
use Swopyn\Tracking;
use Illuminate\Support\Facades\DB;

class PsychometricController extends Controller
{
 	public function intro(){
 		return view('adminlte::psychometric.intro');
 	}

 	public function test(){
 		$psychometric = PsychometricTest::first();
 		return view('adminlte::psychometric.test')->with(['psychometric' => $psychometric]);

 	}

 	public function answer(Request $request){

 		$id = $request->psychometric;
 		if ($id<=0){
 			$id=1;
 		}
 		if ($id>=53) {
 			$id=52;
 		}

 		$answer1 = "answer_".$id."_1";
 		$answer2 = "answer_".$id."_2";
 		
 		$answer =  DB::table('answers_psychometric_tests')->select($answer1, $answer2 )->where('personal_id',$request->get('user'))->first();
 		
 		$psychometric = PsychometricTest::findOrFail($id);
 		return view('adminlte::psychometric.test')->with(['psychometric' => $psychometric, 'answers' => $answer]);
 	}

	public function add(Request $request){
 		
 		$answerTemp = DB::table('answers_psychometric_tests')->where('personal_id',$request->user)->first();

 		if (count($answerTemp) <= 0){
 			$answer = New AnswersPsychometricTest;
	 		$answer->personal_id = $request->user;
	
 		}else{
 			$id = $answerTemp->id;
 			$answer = AnswersPsychometricTest::find($id);

 		}
 		
 		$answer->{"answer_".$request->question."_1"} = $request->answer1;
 		$answer->{"answer_".$request->question."_2"} = $request->answer2; 

 		$answer->save();
 		return response()->json($answer);
 	}
 	

 	public function score(Request $request){
 		$psychometric = PsychometricTest::orderBy('id', 'desc')->take(49)->get();
 		$score=0;
 		foreach ($psychometric as $psy) {
 			$id = $psy->id;
 			$answer1 = "answer_".$id."_1";
	 		$answer2 = "answer_".$id."_2";
	 		
	 		$answer =  DB::table('answers_psychometric_tests')->select($answer1, $answer2 )->where('personal_id',$request->get('user'))->first();
    			
				
 		 		if (($psy->result1 == $answer->$answer1) && ($psy->result2 == $answer->$answer2)){
 		 			$score++;	
 		 	}
 		 }
 		 $answer =  DB::table('answers_psychometric_tests')->where('personal_id',$request->get('user'))->first();
    	 $result = AnswersPsychometricTest::find($answer->id);
 		 $result->result = $score;
 		 $result->save();

 		 $trackingTemp = DB::table('trackings')->where('user_id',$request->get('user'))->first();
 		 $tracking = Tracking::find($trackingTemp->id);
 		 $tracking->psychometric = $score;
 		 $tracking->save();
 		 return view('adminlte::psychometric.thanks');

 	}

 	public function showimg($id){
		$image = PsychometricTest::findOrFail($id);
	    $ima = Image::make($image->image);
	    $response = Response::make($ima->encode('png'));
	    $response->header('Content-Type', 'image/png');
	    return $response;

 	}

 	public function thanks(){
 		return view('adminlte::psychometric.thanks');
 	}

 	
}
