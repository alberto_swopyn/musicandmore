<?php

namespace Swopyn\Http\Controllers\Groups;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Swopyn\Assitance;
use Swopyn\Group;
use Swopyn\GroupAlumn;
use Swopyn\Classr;
use Swopyn\AssistanceS;
use Swopyn\AssistanceRes;
use Swopyn\AssistanceAdmin;
use Swopyn\Http\Controllers\Controller;
use Swopyn\User;
use Illuminate\Support\Facades\Auth;
use Swopyn\Http\Requests\GroupRequest;
use Swopyn\Http\Requests\ClassRoomRequest;

class GroupController extends Controller
{
    public function index()
    {
        $search = \Request::get('search');
        $group = DB::table('groups as g')->join('cathedras', 'g.id_cathedra', 'cathedras.id')->join('subjects', 'cathedras.subject_id', 'subjects.id')->join('employees', 'cathedras.employee_id', 'employees.employee_id')->join('classroom','g.classroom','classroom.id')->select('g.name', 'classroom.name as classroom', 'quota', 'schedule', 'final_hour', 'current_students', 'g.id', 'subjects.name as subject_name', 'employees.name as teacher', 'color','monday','tuesday','wednesday','thursday','saturday','sunday','friday')->where('g.name','LIKE','%'.$search.'%')->orderBy('employees.name','ASC')->paginate(10);

        $search2 = \Request::get('search2');
        $assign = DB::table('groups_alumns as ga')->join('groups', 'ga.id_group', 'groups.id')->join('users', 'ga.user_id', 'users.id')->join('cathedras', 'groups.id_cathedra', 'cathedras.id')->join('subjects', 'cathedras.subject_id', 'subjects.id')->join('employees', 'cathedras.employee_id', 'employees.employee_id')->join('classroom','groups.classroom','classroom.id')->select('groups.name', 'users.name as student', 'groups.schedule', 'groups.id', 'subjects.name as subject_name', 'employees.name as teacher', 'ga.id as ga_id', 'groups.final_hour','groups.monday','groups.tuesday','groups.wednesday','groups.thursday','groups.saturday','groups.sunday','groups.friday', 'classroom.name as classroom')->where('users.name','LIKE','%'.$search2.'%')->orderBy('employees.name','ASC')->paginate(10);
        return view('adminlte::school_control.index')->with(['group' => $group, 'assign' => $assign]);
    }

    public function create()
    {
        $cathedra = DB::table('cathedras')
            ->join('subjects', 'cathedras.subject_id', '=', 'subjects.id')
            ->join('employees', 'cathedras.employee_id', '=', 'employees.employee_id')
            ->select('subjects.name as subject_name', 'employees.name as teacher', 'cathedras.hour_payment', 'cathedras.id')
            ->get();
        $classroom = Classr::all();

        $user = Auth::user()->id;
        $emplo = DB::table('employees')->select('employees.id')->where('employees.employee_id', '=', $user)->first()->id;
        $su = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id', 'tree_job_centers.text')
        ->where('employee_tree_job_center.employee_id', '=', $emplo)->get();

        return view('adminlte::school_control.Group')->with(['cathedra' => $cathedra, 'classroom' => $classroom, 'su' => $su]);
    }

    public function add(Request $request)
    {
        if ($request->Monday == 1) {
            $monday = 1;
            $filter = "Mon";
        } else {
            $monday = 7;
        }
        if ($request->Tuesday == 1) {
            $tuesday = 2;
            $filter = "Tue";
        } else {
            $tuesday = 7;
        }
        if ($request->Wednesday == 1) {
            $wednesday = 3;
            $filter = "Wed";
        } else {
            $wednesday = 7;
        }
        if ($request->Thursday == 1) {
            $thursday = 4;
            $filter = "Thu";
        } else {
            $thursday = 7;
        }
        if ($request->Friday == 1) {
            $friday = 5;
            $filter = "Fri";
        } else {
            $friday = 7;
        }
        if ($request->Saturday == 1) {
            $saturday = 6;
            $filter = "Sat";
        } else {
            $saturday = 7;
        }
        if ($request->Sunday == 1) {
            $sunday = 0;
            $filter = "Sun";
        } else {
            $sunday = 7;
        }

        $dow = "[" . $monday . "," . $tuesday . "," . $wednesday . "," . $thursday . "," . $friday . "," . $saturday . "," . $sunday . "]";

        $group                   = new Group;
        $group->name             = $request->Nombre;
        $group->classroom        = $request->classroom[0];
        $group->id_cathedra      = $request->cathedras[0];
        $group->schedule         = $request->initial_hour;
        $group->final_hour       = $request->final_hour;
        $group->initial_date     = $request->initial_date;
        $group->final_date       = $request->final_date;
        $group->color            = $request->color;
        $group->id_job_center    = $request->selectEmployee;
        $group->dow              = $dow;
        $group->quota            = $request->cupo;
        $group->monday           = $request->Monday;
        $group->tuesday          = $request->Tuesday;
        $group->wednesday        = $request->Wednesday;
        $group->thursday         = $request->Thursday;
        $group->friday           = $request->Friday;
        $group->saturday         = $request->Saturday;
        $group->sunday           = $request->Sunday;
        $group->current_students = 0;
        $group->save();

        return redirect()->route('groups');
    }

    public function edit(Group $group)
    {
        $group    = Group::find($group->id);
        $cathedra = DB::table('cathedras')
            ->join('subjects', 'cathedras.subject_id', '=', 'subjects.id')
            ->join('employees', 'cathedras.employee_id', '=', 'employees.employee_id')
            ->select('subjects.name as subject_name', 'employees.name as teacher', 'cathedras.hour_payment', 'cathedras.id')
            ->get();
        $classroom = Classr::all();

        return view('adminlte::school_control.edit')->with(['group' => $group, 'cathedra' => $cathedra, 'classroom' => $classroom]);
    }

    public function update(Group $group, Request $request)
    {
        if ($request->Monday == 1) {
            $monday = 1;
        } else {
            $monday = 7;
        }
        if ($request->Tuesday == 1) {
            $tuesday = 2;
        } else {
            $tuesday = 7;
        }
        if ($request->Wednesday == 1) {
            $wednesday = 3;
        } else {
            $wednesday = 7;
        }
        if ($request->Thursday == 1) {
            $thursday = 4;
        } else {
            $thursday = 7;
        }
        if ($request->Friday == 1) {
            $friday = 5;
        } else {
            $friday = 7;
        }
        if ($request->Saturday == 1) {
            $saturday = 6;
        } else {
            $saturday = 7;
        }
        if ($request->Sunday == 1) {
            $sunday = 0;
        } else {
            $sunday = 7;
        }

        $dow = "[" . $monday . "," . $tuesday . "," . $wednesday . "," . $thursday . "," . $friday . "," . $saturday . "," . $sunday . "]";

        $group               = Group::find($group->id);
        $group->name         = $request->Nombre;
        $group->classroom    = $request->classroom[0];
        $group->id_cathedra  = $request->cathedras[0];
        $group->schedule     = $request->initial_hour;
        $group->final_hour   = $request->final_hour;
        $group->initial_date = $request->initial_date;
        $group->final_date   = $request->final_date;
        $group->color        = $request->color;
        $group->dow          = $dow;
        $group->quota        = $request->cupo;
        $group->monday       = $request->Monday;
        $group->tuesday      = $request->Tuesday;
        $group->wednesday    = $request->Wednesday;
        $group->thursday     = $request->Thursday;
        $group->friday       = $request->Friday;
        $group->saturday     = $request->Saturday;
        $group->sunday       = $request->Sunday;

        $group->save();

        session()->flash('message_update', 'Grupo Actualizado');

        $group = Group::simplePaginate(10);
        return redirect()->route('groups')->with(['group' => $group]);
    }

    public function delete(Group $group)
    {
        $assign = DB::table('groups_alumns')->select('id')->where('id_group', $group->id)->delete();
        $group->delete();
        $group = Group::simplePaginate(10);
        return redirect()->route('groups')->with(['group' => $group]);
    }

    public function createassign()
    {
        $group = DB::table('groups as g')->join('cathedras', 'g.id_cathedra', 'cathedras.id')->join('subjects', 'cathedras.subject_id', 'subjects.id')->join('employees', 'cathedras.employee_id', 'employees.employee_id')->join('classroom','g.classroom','classroom.id')->select('g.name', 'classroom.name as classroom', 'quota', 'schedule', 'final_hour', 'current_students', 'g.id', 'subjects.name as subject_name', 'employees.name as teacher', 'color','monday','tuesday','wednesday','thursday','saturday','sunday','friday')->get();
        $student = DB::table('users')->join('role_user', 'users.id', '=', 'role_user.user_id')->select('users.id', 'users.name', 'users.sample', 'role_user.role_id', 'role_user.status')->where('role_user.role_id', '=', '2')->where('role_user.status', '=', '1')->orderBy('users.name','ASC')->get();

        return view('adminlte::school_control.assign_students')->with(['group' => $group, 'student' => $student]);

    }
    public function addassign(Request $request)
    {
        $assign           = new GroupAlumn;
        $assign->id_group = $request->groups[0];
        $assign->user_id  = $request->students[0];
        $assign->save();

        $group                   = Group::find($assign->id_group);
        $group->current_students = $group->current_students + 1;
        $group->save();

        return redirect()->route('groups');
    }
    public function deleteassign($id)
    {
        //dd($a);
        $a = GroupAlumn::find($id);
        $a->delete();
        $group                   = Group::find($a->id_group);
        $group->current_students = $group->current_students - 1;
        $group->save();

        return redirect()->route('groups');
    }

    public function assistance()
    {
        $group = DB::table('groups as g')
            ->join('cathedras', 'g.id_cathedra', 'cathedras.id')
            ->join('subjects', 'cathedras.subject_id', 'subjects.id')
            ->join('employees', 'cathedras.employee_id', 'employees.employee_id')
            ->join('classroom','g.classroom','classroom.id')
            ->select('g.name as nombre', 'classroom.name as classroom', 'schedule', 'g.id as id', 'subjects.name as subject_name', 'employees.name as teacher', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'final_hour')
            ->where('employees.employee_id', auth()->user()->id)
            ->get();

        $date      = Carbon::parse('now');
        $date2     = $date->dayOfWeek;
        $monday    = Carbon::MONDAY;
        $tuesday   = Carbon::TUESDAY;
        $wednesday = Carbon::WEDNESDAY;
        $thursday  = Carbon::THURSDAY;
        $friday    = Carbon::FRIDAY;
        $saturday  = Carbon::SATURDAY;
        $sunday    = Carbon::SUNDAY;
        $time      = $date->toTimeString();

        return view('adminlte::school_control.assistance')->with(['group' => $group, 'date2' => $date2, 'monday' => $monday, 'tuesday' => $tuesday, 'wednesday' => $wednesday, 'thursday' => $thursday, 'friday' => $friday, 'saturday' => $saturday, 'sunday' => $sunday, 'date' => $date, 'time' => $time]);
    }

    public function createassistance(Group $group)
    {
        $group   = Group::find($group->id);
        $student = DB::table('groups_alumns as ga')
            ->join('groups', 'ga.id_group', '=', 'groups.id')
            ->join('users', 'ga.user_id', '=', 'users.id')
            ->select('users.name as student', 'users.id as id')->where('ga.id_group', $group->id)->get();
         $check = DB::table('assistances_admin as ad')->join('employees as e','ad.id_teacher','e.employee_id')->where('date',Carbon::now()->toDateString())->where('e.employee_id',auth()->user()->id)->first();
        $check2 = DB::table('assistance_student as st')->join('groups as g','st.id_group','g.id')->join('users', 'st.id_teacher','users.id')->where('id_group',$group->id)->where('date_validator',Carbon::now()->toDateString())->first();
        if ($check == true && $check2 == false) {
            return view('adminlte::school_control.list')->with(['group' => $group, 'student' => $student]);
        }else {
           return redirect()->route('assistance');
        }

    }

    public function addassistance(Request $request)
    {
        $assistance                 = new Assitance;
        $assistance->id_group       = $request->Grupo;
        $assistance->id_teacher     = auth()->user()->id;
        $assistance->date           = Carbon::now()->toDateString();
        $assistance->evidence       = 1;
        $assistance->num_assistance = 1;
        $assistance->save();

        $assistances                 = new AssistanceRes;
        $assistances->id_group       = $request->Grupo;
        $assistances->id_teacher     = auth()->user()->id;
        $assistances->date           = Carbon::now()->toDateString();
        $assistances->evidence       = 1;
        $assistances->num_assistance = 1;
        $assistances->save();

        foreach ($request->get('Alumno') as $key => $value) {
            $ass = new AssistanceS;
            $ass->id_group = $request->Grupo;
            $ass->id_teacher = auth()->user()->id;
            $ass->id_student = $value;
            $ass->assistance = $request->get('Assistance')[$key];
            $ass->date = Carbon::now()->toDateString();
            $date = Carbon::now();
            $ass->date_validator = $date->format('Y-m-d'); 
            $ass->save();

            $sample = DB::table('users')->select('id','sample')->where('id',$value)->where('sample',1)->first();
            if($sample == true){
                $role = DB::table('role_user')->select('user_id','role_id','status')->where('user_id',$sample->id)->update(['status' => 0]);
                $assig = DB::table('groups_alumns')->where('id_group',$request->Grupo)->where('user_id',$sample->id)->delete();
            }
        }

        return redirect()->route('assistance');

    }

    public function report_assistance(Request $request)
    {

        $date = Carbon::now();
        $dateMes = $date->format('m');
        $dateDia = $date->format('d');
        $año = $date->format('Y');

        $search = \Request::get('search');
        $assistances = DB::table('users')
            ->join('role_user','users.id','role_user.user_id')
            ->join('employees', 'users.id','employees.employee_id')
            ->select('users.name as teacher', 'role_user.user_id', 'role_user.role_id', 'users.email')
            ->where('role_id', 3)
            ->where('status', 1)
            ->orderBy('users.name', 'asc')
            ->where('users.name','LIKE','%'.$search.'%')->paginate(10);

        $array = []; 

        foreach ($assistances as $assistance) {

            if (!empty($request->months)) {
                $dateMes = $request->months;
                $año = $request->year;
            }

            $assists = DB::table('assistances_admin')->where('id_teacher', $assistance->user_id)->where('assistance', 1)->whereMonth('created_at', $dateMes)->whereYear('created_at', $año)->count();
            $faults = DB::table('assistances_admin')->where('id_teacher', $assistance->user_id)->where('assistance', 0)->whereMonth('created_at', $dateMes)->whereYear('created_at', $año)->count();
            $justified = DB::table('assistances_admin')->where('id_teacher', $assistance->user_id)->where('assistance', 2)->whereMonth('created_at', $dateMes)->whereYear('created_at', $año)->count();

            $usersAsist = ['teacher' => $assistance->teacher, 'user_id' => $assistance->user_id, 'email' => $assistance->email, 'assists' => $assists, 'faults' => $faults, 'justified' =>  $justified];

            array_push($array, $usersAsist);
        }

        $totAssistances = collect($array);

        return view('adminlte::school_control.Assist_Report')->with(['assistance' => $totAssistances, 'assistancePag' => $assistances, 'mes' => $dateMes, 'year' => $año]);
    }

    /**
     * FIXME: Cambiar Select dinamico a mes actual en base a Carbon::new
     */
    public function report_assistance_detail(int $idTeacher, $month, $year, $type)
    {
        $assistance = DB::table('assistances_admin as asis')
            ->join('users', 'asis.id_teacher', 'users.id')
            ->select('users.name as teacher', 'asis.created_at as date', 'asis.comments','asis.id as asistencia')
            ->where('asis.id_teacher', $idTeacher)
            ->where('assistance', $type)
            ->whereMonth('asis.created_at', $month)
            ->whereYear('asis.created_at', $year)
            ->orderBy('asis.created_at','DESC')
            ->paginate(15);

        $groups = DB::table('assistances as a')->join('groups as g','g.id','a.id_group')->join('users as u','u.id','a.id_teacher')->select('g.name as curso','u.name as teacher', 'a.date','a.id_teacher')->where('a.id_teacher',$idTeacher)->whereMonth('a.date',$month)->whereYear('a.date',$year)->get();
        

        return view('adminlte::school_control.Assist_Report_Detail')->with(['assistance' => $assistance, 'type' => $type, 'groups' => $groups, 'month' => $month, 'year' => $year]);
    }

    public function group_detail($id, $month, $year)
    {
        $ass = DB::table('assistances_admin as aa')->select('date','id_teacher')->where('id',$id)->first();
        $teacher = $ass->id_teacher;
        $date = Carbon::parse($ass->date);
        $day = $date->format('d');
        $groups = DB::table('assistances as a')->join('groups as g','g.id','a.id_group')->join('users as u','u.id','a.id_teacher')->select('g.name as curso','u.name as teacher', 'a.date','a.id_teacher')->where('a.id_teacher',$teacher)->whereMonth('a.date',$month)->whereYear('a.date',$year)->whereDay('a.date',$day)->get();
        return view('adminlte::school_control._detail_assitance_date')->with(['groups' => $groups]);
    }

    /**
     * Genera el reporte de asistencias generales por estudiante.
     */
    public function report_assistance_student(Request $request)
    {
        $date = Carbon::now();
        $dateMes = $date->format('m');
        $dateDia = $date->format('d');
        $año = $date->format('Y');

        $search = \Request::get('search');
        $assistances = DB::table('users')
            ->join('role_user','users.id','role_user.user_id')
            ->select('users.name as student', 'role_user.user_id', 'role_user.role_id', 'users.email')
            ->where('role_id', 2)
            ->where('status', 1)
            ->orderBy('users.name', 'asc')
            ->where('users.name','LIKE','%'.$search.'%')->paginate(10);

        $array = []; 

        foreach ($assistances as $assistance) {

            if (!empty($request->months)) {
                $dateMes = $request->months;
                $año = $request->year;
            }

            $assists = DB::table('assistance_student')->where('id_student', $assistance->user_id)->where('assistance', 1)->whereMonth('created_at', $dateMes)->whereYear('created_at', $año)->count();
            $faults = DB::table('assistance_student')->where('id_student', $assistance->user_id)->where('assistance', 0)->whereMonth('created_at', $dateMes)->whereYear('created_at', $año)->count();
            $justified = DB::table('assistance_student')->where('id_student', $assistance->user_id)->where('assistance', 2)->whereMonth('created_at', $dateMes)->whereYear('created_at', $año)->count();

            $usersAsist = ['student' => $assistance->student, 'user_id' => $assistance->user_id, 'email' => $assistance->email, 'assists' => $assists, 'faults' => $faults, 'justified' =>  $justified];

            array_push($array, $usersAsist);
        }

        $totAssistances = collect($array);

        return view('adminlte::school_control.Student_Report')->with(['assistance' => $totAssistances, 'assistancePag' => $assistances, 'mes' => $dateMes, 'year' => $año]);
    }


    /**
     * Genera el reporte individual del alumno por su id.
     * @param integer $idStudent
     */
    public function report_assistance_detail_student(int $idStudent, $month, $year, $type)
    {
        $assistance = DB::table('assistance_student as asis')
            ->join('users', 'asis.id_student', 'users.id')
            ->join('groups', 'asis.id_group', 'groups.id')
            ->join('cathedras', 'groups.id_cathedra', 'cathedras.id')
            ->join('subjects', 'cathedras.subject_id', 'subjects.id')
            ->join('employees', 'cathedras.employee_id', 'employees.employee_id')
            ->select('users.name as student', 'asis.created_at as date', 'groups.name as group', 'employees.name as teacher', 'subjects.name as subject')
            ->where('asis.id_student', $idStudent)
            ->where('assistance', $type)
            ->whereMonth('asis.created_at', $month)
            ->whereYear('asis.created_at', $year)
            ->orderBy('asis.created_at','DESC')
            ->paginate(15); 

        return view('adminlte::school_control.Student_Report_Detail')->with(['assistance' => $assistance, 'type' => $type]);
    }


#region Functions JSON

    /**
     * Retorna un objeto JSON con los grupos asignados a un docente por su id de empleado y el día actual.
     * @param integer idTeacher
     */
    public function getGroupsByTeacherByDayForJSON(int $idTeacher)
    {
        $date = Carbon::now();
        $date = $date->format('D');
        $day = 'g.monday';

        if ($date == 'Mon') {
            $day = 'g.monday';
        }
        elseif ($date == 'Tue') {
            $day = 'g.tuesday';
        }
        elseif ($date == 'Wed') {
            $day = 'g.wednesday';
        }
        elseif ($date == 'Thu') {
            $day = 'g.thursday';
        }
        elseif ($date == 'Fri') {
            $day = 'g.friday';
        }
        elseif ($date == 'Sat') {
            $day = 'g.saturday';
        }
        elseif ($date == 'Sun') {
            $day = 'g.sunday';
        }

        $groups = DB::table('groups as g')
            ->join('cathedras', 'g.id_cathedra', 'cathedras.id')
            ->join('subjects', 'cathedras.subject_id', 'subjects.id')
            ->join('employees', 'cathedras.employee_id', 'employees.employee_id')
            ->select('g.name as nombre', 'classroom', 'schedule', 'g.id as id', 'subjects.name as subject_name', 'employees.name as teacher', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'final_hour', 'color')
            ->where('employees.employee_id', $idTeacher)
            ->where($day, 1)
            ->whereNotIn('g.id', [0])
            ->orderBy('schedule', 'asc')
            ->get();

        echo $groups->toJson();
    }

    /**
     * Retorna un objeto JSON con los grupos asignados a un docente por su id de empleado.
     * @param integer idTeacher
     */
    public function getGroupsByTeacherForJSON(int $idTeacher)
    {
    
        $groups = DB::table('groups as g')
            ->join('cathedras', 'g.id_cathedra', 'cathedras.id')
            ->join('subjects', 'cathedras.subject_id', 'subjects.id')
            ->join('employees', 'cathedras.employee_id', 'employees.employee_id')
            ->select('g.name as nombre', 'classroom', 'schedule', 'g.id as id', 'subjects.name as subject_name', 'employees.name as teacher', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'final_hour', 'color')
            ->where('employees.employee_id', $idTeacher)
            ->whereNotIn('g.id', [0])
            ->orderBy('schedule', 'asc')
            ->get();

        echo $groups->toJson();
    }

    /**
     * Retorna un objeto JSON con la lista de los alumnos inscritos al grupo.
     * @param integer idGroup
     */
    public function getListStudentsByIdGroup(int $idGroup)
    {
        $students = DB::table('groups_alumns as ga')
            ->join('groups', 'ga.id_group', '=', 'groups.id')
            ->join('users', 'ga.user_id', '=', 'users.id')
            ->join('role_user as r', 'users.id', 'r.user_id')
            ->select('users.name as student', 'users.id as id')->where('ga.id_group', $idGroup)->where('r.status', 1)->get();

        echo $students->toJson();
    }

    /**
     * Guarda una nueva asistencia de cada alumno.
     */
    public function saveAsistanceStudentbyJson()
    {
        $objString = file_get_contents('php://input');
        if (isset($objString))
        {
            $request = json_decode($objString);
            
            foreach($request as $student) {
                $assistance = new AssistanceS;
                $assistance->id_group = $student->id_group;
                $assistance->id_teacher = $student->id_teacher;
                $assistance->date = Carbon::now();
                $assistance->date_validator = Carbon::now()->format('Y-m-d');
                $assistance->id_student = $student->id;
                $assistance->assistance = $student->assistance;
                $assistance->save();

                $sample = DB::table('users')->select('id','sample')->where('id', $student->id)->where('sample',1)->first();
                if($sample == true){
                    $role = DB::table('role_user')->select('user_id','role_id','status')->where('user_id',$sample->id)->update(['status' => 0]);
                    $assig = DB::table('groups_alumns')->where('id_group',$request->Grupo)->where('user_id',$sample->id)->delete();
                }
            }
        }
    }

    /**
     * Guarda una nueva asistencia de grupo.
     */
    public function saveAsistancebyJson()
    {
        $objString = file_get_contents('php://input');
        if (isset($objString))
        {
            $request = json_decode($objString);
            $assistance = new Assitance;
            $assistance->id_group = $request->id_group;
            $assistance->num_assistance = 1;
            $assistance->id_teacher = $request->id_teacher;
            $assistance->date = Carbon::now();
            $assistance->evidence = 1;
            $assistance->save();
        }
    }

    /**
     * Verifica el pase de lista por día.
     */
    public function verifyAssistance($id_group, $id_teacher){
        $verify = DB::table('assistance_student')
            ->select('assistance')
            ->where('id_group', $id_group)
            ->where('id_teacher', $id_teacher)
            ->where('date_validator', Carbon::now()->format('Y-m-d'))
            ->count();
        if ($verify > 0) {
            // ya se paso lista
            $codeResponse = ['codeResponse' => 1];
            $collection = collect($codeResponse);
            echo $collection->toJson();
        }
        else{
            // no se ha pasado lista
            $codeResponse = ['codeResponse' => 0];
            $collection = collect($codeResponse);
            echo $collection->toJson();
        }
    }

    /**
     * Verifica el pase de lista por grupo en base a la asistencia del maestro.
     */
    public function verifyAssistanceByTeacher($id_group, $id_teacher){
        $verify = DB::table('assistances_admin')
            ->select('assistance')
            ->where('id_teacher', $id_teacher)
            ->where('date', Carbon::now()->toDateString())
            ->count();
        if ($verify > 0) {
            // ya se paso lista
            $codeResponse = ['codeResponse' => 1];
            $collection = collect($codeResponse);
            echo $collection->toJson();
        }
        else{
            // no se ha pasado lista
            $codeResponse = ['codeResponse' => 0];
            $collection = collect($codeResponse);
            echo $collection->toJson();
        }
    }

#endregion

    public function alum_list(Group $group)
    {
        $group   = Group::find($group->id);
        $student = DB::table('groups_alumns as ga')
            ->join('groups', 'ga.id_group', '=', 'groups.id')
            ->join('users', 'ga.user_id', '=', 'users.id')
            ->select('users.name as student', 'users.id as id')->where('ga.id_group', $group->id)->get();

        return view('adminlte::school_control.alum_list')->with(['group' => $group, 'student' => $student]);
    }

    public function status()
    {
        $search = \Request::get('search');
        $students = DB::table('users')->join('role_user','users.id','role_user.user_id')->select('users.name', 'users.id', 'role_user.user_id', 'role_user.role_id', 'role_user.status', 'users.email', 'users.phone_number', 'users.experience', 'users.sample', 'users.password')->where('role_id', 2)->where('users.name','LIKE','%'.$search.'%')->orderBy('users.name','ASC')->paginate(10);
        $company = DB::table('tree_job_centers')->select('tree_job_centers.id_inc', 'tree_job_centers.text')->whereNotIn('id_inc', [1])->orderBy('tree_job_centers.text','ASC')->get();
        
        return view('adminlte::school_control.studentStatus')->with(['students' => $students, 'company' => $company]);
    }

    public function updateStatus($studentStatus, Request $request)
    {
        $status = $request->get('StatusEdit' . $studentStatus);
        $sta = DB::table('role_user')->where('user_id', '=', $studentStatus)->where('role_id', '=', 2)->update(['status' => $status]);

        return redirect()->route('student_status');
    }

    public function classroom_index()
    {
        $search = \Request::get('search');
        $classroom = Classr::where('name','LIKE','%'.$search.'%')->paginate(10);

        return view('adminlte::school_control.Classroom')->with(['classroom' => $classroom]);
    }

    public function classroom_add(Request $request)
    {
        $classroom       = new Classr;
        $classroom->name = $request->NombreC;
        $classroom->save();

        $sub = Classr::all();

        return redirect()->route('classroom');
    }

    public function classroom_update(Classr $classroom, Request $request)
    {
        $classroom->name = $request->get('NombreClassEdit' . $classroom->id);
        $classroom->save();

        return redirect()->route('classroom');
    }

    public function classroom_delete(Classr $classroom)
    {
       $class_delete = DB::table('classroom')->where('id', '=', $classroom->id)->delete();

        return redirect()->route('classroom');
    }

    public function assistance_teacher()
    {
        $search = \Request::get('search');
         $groups = DB::table('employees as e')
            ->join('role_user as r', 'e.employee_id', 'r.user_id')
            ->select('e.name as teacher','e.employee_id as id_teacher','e.job_title_profile_id')
            ->where('r.role_id', 3)
            ->where('r.status', 1)
            ->where('e.name','LIKE','%'.$search.'%')
            ->orderBy('e.name','ASC')->paginate(10);

         $array = [];

         foreach ($groups as $teacher) {
             $assistance = DB::table('assistances_admin')
                ->where('id_teacher', $teacher->id_teacher)
                ->where('date', Carbon::now()->toDateString())
                ->count();
            if ($assistance > 0) {
                $assistances = ['teacher' => $teacher->teacher, 'id_teacher' => $teacher->id_teacher, 'assistance' => 1];
            }
            else{
                $assistances = ['teacher' => $teacher->teacher, 'id_teacher' => $teacher->id_teacher, 'assistance' => 0];
            }
            array_push($array, $assistances);
         }

         $teachers = collect($array);

        return view('adminlte::school_control.Teacher_list')->with(['groups' => $teachers, 'groupsPage' => $groups]);
    }

    public function add_ass_teacher(Request $request)
    {   
        $check = DB::table('assistances_admin as assistances')->join('users', 'assistances.id_teacher', 'users.id')->where('assistances.id_teacher',$request->Grupo)->where('assistances.date',Carbon::now()->toDateString())->first();

        if ($check == null) {
            $admin = new AssistanceAdmin;
            $admin->id_group = 0;
            $admin->id_teacher = $request->Grupo;
            $admin->date = Carbon::now()->toDateString();
            $admin->assistance = 1;
            $admin->save();

            return redirect()->route('assistance_teacher');
        }else{
           return redirect()->route('assistance_teacher'); 
        }
        
    }

    public function add_ass_teacher_other($idTeacher, Request $request)
    {
        $admin = new AssistanceAdmin;
        $admin->id_group = 0;
        $admin->id_teacher = $idTeacher;
        $admin->date = $request->other;
        $admin->assistance = 1;
        $admin->comments = $request->comments;
        $admin->save();
        return redirect()->route('assistance_teacher');
    }

    public function teachers()
    {
        $search = \Request::get('search');

        $teachers = DB::table('users')->join('role_user','users.id','role_user.user_id')->select('users.name', 'role_user.user_id', 'role_user.role_id','role_user.status', 'users.email', 'users.id')->whereNotIn('role_id', [2])->whereNotIn('user_id', [138, 201, 263, 256, 258, 200])->where('users.name','LIKE','%'.$search.'%')->orderBy('name', 'asc')->paginate(10);

        $users = User::all();
        $jt = DB::table('tree_job_profiles')->get();
		$jc = DB::table('tree_job_centers')->get();
        
        return view('adminlte::school_control.teachers')->with(['teachers' => $teachers, 'jobTitle' => $jt, 'jobCenter' => $jc, 'users' => $users]);
    }

    public function addTeacher(Request $request)
    {
        $usuario = User::create([
                'name'=>$request->Nombreuser , 
                'email'=>$request->CorreoUser,
                'password'=>'$2y$10$R0zGQlmr35zLebbCrCxUreRLIO.Jvo7YXVpweV3OZoFB4xB4BXmXi']
                );

        $samp = DB::table('users')->where('id', '=', $usuario->id)->update(['verified' => 1]);
        $sampl = DB::table('role_user')->insert(['user_id' => $usuario->id, 'role_id' => $request->RoleUser, 'status' => 1]);

        return redirect()->route('teachers');
    }

    public function updateStatusTeacher($teacherStatus, Request $request)
    {
        $status = $request->get('StatusEdit' . $teacherStatus);
        $sta = DB::table('role_user')->where('user_id', '=', $teacherStatus)->update(['status' => $status]);

        return redirect()->route('teachers');
    }

}
