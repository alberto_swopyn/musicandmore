<?php

namespace Swopyn\Http\Controllers\Contracttype;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\Contract;
use Illuminate\Support\Facades\DB;
use Validator; 
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Swopyn\Tracking;
use Image;
use PDF;


class ContractController extends Controller
{
	public function addDownload(Request $request){
		$temp = DB::table('contracts')->where('users_id', auth()->user()->id)->first();
		$id = auth()->user()->id;
		if (count($temp) <= 0) {
			$contract = new Contract;
			$contract -> users_id = $id;
		}
		else{
			$id_contract = $temp->id;
			$contract = Contract::find($id_contract);
		}
		$contract->download_file = $request->Respuesta;
		$contract->save();
		return response()->json($contract);
	}

	private $rules= [
	'Contractfile' => 'Required',
	];

	public function addUpload(Request $request){
			$temp = DB::table('contracts')->where('users_id', $request->usuario)->first();
			$id_contract = $temp->id;
			$contract = Contract::find($id_contract);
			$contract->send_file = 'Si';
			$name = 'Contract_' . $request->usuario . '.pdf';
			Storage::putFileAs('public',$request->file('Contractfile'), $name);
			$contract->file = $name;
			$contract->save();
			\Session::flash('flash_message', 'Contrato Firmado Actualizado');

			$temp_tracking = DB::table('trackings')->where('user_id', $request->usuario)->first();

 		if (count($temp_tracking) <= 0){
			$tracking = New Tracking;
	
 		}else{
 			$id = $temp_tracking->id;
 			$tracking = Tracking::find($id);

 		}
		$tracking->contract_upload = 1;
		$tracking->contract = 1;
		$tracking->save();

			return redirect()->route('tracking_index');
	}
}
