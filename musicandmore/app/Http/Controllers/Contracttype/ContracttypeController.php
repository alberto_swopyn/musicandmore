<?php

namespace Swopyn\Http\Controllers\Contracttype;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Swopyn\Contract;
use Swopyn\Tracking;
use Swopyn\GeneralPersonalData;
use Swopyn\PersonalAddress;
use PDF;

class ContracttypeController extends Controller
{
    public function index($usuario){
        $temp = DB::table('work_general_datas')->where('work_general_datas_id',$usuario)->first();
        $profile = DB::table('job_title_profiles')->where('id', $temp->job_title_profile_id)->first();
        $tcontract = DB::table('job_title_contracts')->where('job_title_profiles_id', $profile->job_title_profiles_id)->get();
        $structure = array();
        foreach ($tcontract as $temp) {
            $tmp = DB::table('contract_types')->where('id', $temp->contract_types_id)->first();
            array_push($structure, $tmp->contract_structure);
        }
        $pass = DB::table('contracts')->where('users_id', $usuario)->where('download_file','Si')->first(); 
        $pasador = 0;
        if (count($pass)>0){
            $pasador = 1;}
        else{
            $pasador = 0;}

        $data = $this->getdata($usuario);
        $dir = DB::table('personal_addresses')->where('personal_addresses_id', $usuario)->first();
        return view('adminlte::contracttype.index')->with(['users' => $data['usuario'], 'contrato' => $data['contrato'], 'fecha' => $data['fecha'], 'direccion' => $data['direccion'], 'CURP' => $data['Curp'], 'RFC' => $data['Rfc'],  'pasador' => $pasador, 'temp_structure' => $structure, 'dir' => $dir]);
    }

    public function invoice($usuario) {
        $temp = DB::table('work_general_datas')->where('work_general_datas_id',$usuario)->first();
        $profile = DB::table('job_title_profiles')->where('id', $temp->job_title_profile_id)->first();
        $tcontract = DB::table('job_title_contracts')->where('job_title_profiles_id', $profile->job_title_profiles_id)->get();
        $array = array();
        foreach ($tcontract as $temp) {
            $tmp = DB::table('contract_types')->where('id', $temp->contract_types_id)->first();
            array_push($array, $tmp->contract_structure);
        }
        $data = $this->getdata($usuario);
        $pdf = PDF::loadView('adminlte::contracttype.pdfview', ['users' => $data['usuario'], 'contrato' => $data['contrato'], 'fecha' => $data['fecha'], 'direccion' => $data['direccion'], 'CURP' => $data['Curp'], 'RFC' => $data['Rfc'], 'temp' => $array ]);
        $temp = DB::table('contracts')->where('users_id', $usuario)->first();
        if (count($temp) <= 0) {
            $contract = new Contract;
            $contract -> users_id = $usuario;
        }
        else{
            $id_contract = $temp->id;
            $contract = Contract::find($id_contract);
        }
        $contract->save();
        $temp = DB::table('trackings')->where('user_id', $usuario)->first();
        if (count($temp) <= 0) {
            $contract = new Tracking;
            $contract -> user_id = $usuario;
        }
        else{
            $id_contract = $temp->id;
            $contract = Tracking::find($id_contract);
        }
        $contract->contract_download = '1';
        $contract->save();
        return $pdf->download('contrato.pdf');
    }

    public function getdata($usuario){
        $contract = DB::table('contract_types')->where('id','5')->first();
        $user = DB::table('users')->where('id',$usuario)->first();
        $dir = DB::table('personal_addresses')->where('personal_addresses_id', $user->id)->first();
        $personal_data = DB::table('general_personal_datas')->where('general_personal_data_id', $user->id)->first();
        $direccion = $dir -> street . " " . $dir -> ext_number . " " . $dir->int_number . " " . $dir->fraccionamiento . " " . $dir->city . " " . $dir->state;
        setlocale(LC_TIME,"es_ES");
        $data = [
        'contrato' => $contract -> contract_structure,
        'val' => $contract -> id,
        'fecha' => strftime("%e %B %Y"),
        'usuario' => $personal_data,
        'direccion' => $direccion,
        'Curp' => $personal_data->curp,
        'Rfc' => $personal_data->rfc,
        ];
        return $data;
    }

    public function update(Request $request)
    {
        $user = GeneralPersonalData::where('general_personal_data_id', $request->usuario)->first();
        $dir = PersonalAddress::where('personal_addresses_id', $request->usuario)->first();

        //update data
        $user->first_name = $request->first_name_upgrade;
        $user->last_name = $request->last_name_upgrade;
        $user->curp = $request->curp_upgrade;
        $user->rfc = $request->rfc_upgrade;
        $user->save();

        $dir->street = $request->street_upgrade;
        $dir->ext_number = $request->ext_number_upgrade;
        $dir->int_number = $request->int_number_upgrade;
        $dir->state = $request->Estado_personal;
        $dir->fraccionamiento = $request->fraccionamiento;
        $dir->zip_code = $request->zip_code_upgrade;
        $dir->save();

        return redirect()->route('contracts_index', [ '$usuario' => $request->usuario ]);
    }
}