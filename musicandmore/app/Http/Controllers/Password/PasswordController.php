<?php

namespace Swopyn\Http\Controllers\Password;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Swopyn\Http\Controllers\Controller;
use Swopyn\User;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    public function password()
    {
        return view('adminlte::password.changePass');
    }

    public function passSuccess()
    {
        return view('adminlte::password.changePassSuccess');
    }

    public function passUpdate(Request $request)
    {
        $contraseña = $password = bcrypt($request->contra);

        DB::table('users')
            ->where('id', auth()->user()->id)
            ->update(['password' => $contraseña]);

        return redirect()->route('success_pass');
    }

}

