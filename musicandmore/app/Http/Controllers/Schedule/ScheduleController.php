<?php

namespace Swopyn\Http\Controllers\Schedule;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Swopyn\Group;
use Swopyn\Http\Controllers\Controller;
use Swopyn\User;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{

    public function schedule()
    {
        $user = Auth::user()->id;
        $emplo = DB::table('employees')->select('employees.id')->where('employees.employee_id', '=', $user)->first()->id;
        $su = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id', 'tree_job_centers.text')
        ->where('employee_tree_job_center.employee_id', '=', $emplo)->get();
        $sucount = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id', 'tree_job_centers.text')
        ->where('employee_tree_job_center.employee_id', '=', $emplo)->count();
       
        $events = Group::select("id", "name as title", "schedule as start", "final_hour as end", "dow", "color", "initial_date", "final_date")->where('id_job_center', $su[0]->tree_job_center_id)->get();
        
        return view('adminlte::schedule.schedule')->with(['events' => $events, 'sucs' => $su, 'cont' => $sucount]);
    }

    public function scheduleByJobCenter(Request $request){

        $user = Auth::user()->id;
        $emplo = DB::table('employees')->select('employees.id')->where('employees.employee_id', '=', $user)->first()->id;
        $su = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id', 'tree_job_centers.text')
        ->where('employee_tree_job_center.employee_id', '=', $emplo)->get();
        $sucount = DB::table('employee_tree_job_center')
        ->join('tree_job_centers', 'tree_job_centers.id_inc', '=', 'employee_tree_job_center.tree_job_center_id')
        ->select('employee_tree_job_center.tree_job_center_id', 'tree_job_centers.text')
        ->where('employee_tree_job_center.employee_id', '=', $emplo)->count();

        $events = Group::select("id", "name as title", "schedule as start", "final_hour as end", "dow", "color", "initial_date", "final_date")->where('id_job_center', $request->selectEmployee)->get();

        return view('adminlte::schedule.schedule')->with(['events' => $events, 'sucs' => $su, 'cont' => $sucount]);
    }

    public function schedule_student()
    {
        $events = DB::table('groups')
            ->join('groups_alumns', 'groups_alumns.id_group', '=', 'groups.id')
            ->join('users', 'users.id', '=', 'groups_alumns.user_id')
            ->join('cathedras', 'cathedras.id', '=', 'groups.id_cathedra')
            ->join('employees', 'employees.id', '=', 'cathedras.employee_id')
            ->select('groups.id', 'groups.name as title', 'groups.schedule as start', 'groups.final_hour as end', 'groups.dow', 'groups.color', 'groups.initial_date', 'groups.final_date', 'groups_alumns.user_id', 'employees.name as teacher')
            ->where('groups_alumns.user_id', '=', auth()->user()->id)
            ->get();

        return view('adminlte::schedule.scheduleStudent')->with(['events' => $events]);
    }

    public function schedule_teacher()
    {
        $events = DB::table('groups')
            ->join('cathedras', 'cathedras.id', '=', 'groups.id_cathedra')
            ->join('employees', 'employees.employee_id', '=', 'cathedras.employee_id')
            ->join('users', 'users.id', '=', 'employees.employee_id')
            ->select('groups.id', 'groups.name as title', 'groups.schedule as start', 'groups.final_hour as end', 'groups.dow', 'groups.color', 'groups.initial_date', 'groups.final_date', 'employees.employee_id')
            ->where('employees.employee_id', '=', auth()->user()->id)
            ->get();

        return view('adminlte::schedule.scheduleTeacher')->with(['events' => $events]);
    }

#region Funciones JSON

    /**
     * Retorna un objeto JSON con los eventos (clases o materias) de un alumno.
     * @param integer idUser
     */
    public function getEventsScheduleJson(int $idUser)
    {

        $events = DB::table('groups')
            ->join('groups_alumns', 'groups_alumns.id_group', '=', 'groups.id')
            ->join('users', 'users.id', '=', 'groups_alumns.user_id')
            ->join('cathedras', 'cathedras.id', '=', 'groups.id_cathedra')
            ->join('employees', 'employees.id', '=', 'cathedras.employee_id')
            ->select('groups.id', 'groups.name as title', 'groups.schedule as start', 'groups.final_hour as end', 'groups.dow', 'groups.color', 'groups.initial_date', 'groups.final_date', 'groups_alumns.user_id', 'employees.name as teacher')
            ->where('groups_alumns.user_id', '=', $idUser)
            ->get();

        echo $events->toJson();
    }

    /**
     * Retorna un objeto JSON con los eventos (clases o materias) impartidas por un maestro.
     * @param integer idTeacher
     */
    public function getEventsScheduleByTeacher(int $idTeacher)
    {
        $events = DB::table('groups')
            ->join('cathedras', 'cathedras.id', '=', 'groups.id_cathedra')
            ->join('employees', 'employees.employee_id', '=', 'cathedras.employee_id')
            ->join('users', 'users.id', '=', 'employees.employee_id')
            ->select('groups.id', 'groups.name as title', 'groups.schedule as start', 'groups.final_hour as end', 'groups.dow', 'groups.color', 'groups.initial_date', 'groups.final_date', 'employees.employee_id', 'employees.name as teacher')
            ->where('employees.employee_id', '=', $idTeacher)
            ->get();

            echo $events->toJson();
    }
#endregion
}
