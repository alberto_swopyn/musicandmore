<?php

namespace Swopyn\Http\Controllers\Registrations;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\User;
use DB;
use Swopyn\GeneralPersonalData;
use Swopyn\PersonalAddress;
use Swopyn\PersonalReference;
use Swopyn\QuizStudent;

class RegistrationController extends Controller
{
    public function registration_student()
    {
    	$student = DB::table('users')
    	->join('role_user', 'users.id', 'role_user.user_id')
    	->where('role_user.role_id', '2')
		->get();
		$verify = 0;
    	return view('adminlte::registration.create_student')->with(['student' => $student, 'verify' => $verify]);
    }

    public function registration_add(Request $request)
    {
		//Validación para verificar si ya existe el registro.
		$verify = DB::table('general_personal_datas')->select('first_name')->where('general_personal_data_id', $request->alumnos[0])->count();
		if ($verify == 0) {
			// No existe el registro.
			$GeneralPersonalData = new GeneralPersonalData;
    		$GeneralPersonalData->general_personal_data_id = $request->alumnos[0];
    		$GeneralPersonalData->first_name = $request->Nombre;
    		$GeneralPersonalData->last_name = $request->Apellidos;
    		$GeneralPersonalData->gender = $request->Genero;
    		$GeneralPersonalData->birthday = $request->Nacimiento;
    		$GeneralPersonalData->birth_place = $request->LugarNacimiento;
    		$GeneralPersonalData->phone_number = $request->Telefono_job;
    		$GeneralPersonalData->cellphone = $request->Celular;
			$GeneralPersonalData->email = $request->Email;
			$GeneralPersonalData->schoolarship = $request->Beca;
    		$GeneralPersonalData->save();

    		$PersonalAddress = new PersonalAddress;
    		$PersonalAddress->personal_addresses_id = $request->alumnos[0];
    		$PersonalAddress->street = $request->Calle;
    		$PersonalAddress->ext_number = $request->NumeroExterior;
    		$PersonalAddress->int_number = $request->NumeroInterior;
    		$PersonalAddress->fraccionamiento = $request->Fraccionamiento;
    		$PersonalAddress->zip_code = $request->CodigoPostal;
    		$PersonalAddress->city = $request->Ciudad;
    		$PersonalAddress->state = $request->Estado;
    		$PersonalAddress->save();

    		$PersonalReference = new PersonalReference;
            $PersonalReference->personal_references_id = $request->alumnos[0];
    		$PersonalReference->name = $request->NombreFamiliar;
    		$PersonalReference->relationship = $request->Parentesco;
    		$PersonalReference->occupation = $request->Ocupacion;
    		$PersonalReference->address = $request->Fraccionamiento;
    		$PersonalReference->meet_time = $request->ViveConUsted;
    		$PersonalReference->phone_number = $request->NumeroFamiliar;
    		$PersonalReference->save();
		}
        
        $search = \Request::get('search');
        $students = DB::table('users')->join('role_user','users.id','role_user.user_id')->join('general_personal_datas as gen','users.id','gen.general_personal_data_id')->select('users.name', 'users.id', 'role_user.user_id', 'role_user.role_id', 'role_user.status', 'users.email', 'gen.phone_number', 'gen.cellphone')->where('role_id', 2)->where('users.name','LIKE','%'.$search.'%')->orderBy('users.name','ASC')->paginate(10);
        
        return view('adminlte::school_control.studentStatus')->with(['students' => $students]);
    }

    public function registration_student_evaluation(User $student)
    {
        $user = User::find($student->id);
        $questions = DB::table('quiz_student')->where('id_user', $student->id)->first();
    	return view('adminlte::registration.create_student_evaluation')->with(['users' => $user, 'question' => $questions]);
    }

    public function add_student_evaluation(User $student, Request $request){

        $user = User::find($student->id);
        $QuizStudent = new QuizStudent;
        $QuizStudent->id_user = $student->id;
        $QuizStudent->pregunta1 = $request->Otros[0];
        $QuizStudent->pregunta2 = $request->Q2;
        $QuizStudent->pregunta3 = $request->Q3;
        $QuizStudent->pregunta4 = $request->Q4;
        $QuizStudent->pregunta5 = $request->Q5;
        $QuizStudent->pregunta6 = $request->Q6;
        $QuizStudent->pregunta7 = $request->Q7;
        $QuizStudent->save();

        return redirect()->route('student_status');
    }

    public function registration_teacher()
    {
    	return view('adminlte::registration.create_teacher');
    }

    public function editstudent(User $student)
    {
        $user = User::find($student->id);
        $gpd = DB::table('general_personal_datas')->where('general_personal_data_id', $student->id)->first();
        $pa = DB::table('personal_addresses')->where('personal_addresses_id', $student->id)->first();
        $pr = DB::table('personal_references')->where('personal_references_id', $student->id)->first();
        return view('adminlte::registration.edit')->with(['users' => $user, 'gpds' => $gpd, 'pas' => $pa, 'prs' => $pr]);
    }

    public function updatestudent(User $student, Request $request)
    {
        $user = User::find($student->id);
        $GeneralPersonalData = GeneralPersonalData::where('general_personal_data_id', $student->id)->first();
        $GeneralPersonalData->first_name = $request->Nombre;
        $GeneralPersonalData->last_name = $request->Apellidos;
        $GeneralPersonalData->gender = $request->Genero;
        $GeneralPersonalData->birthday = $request->Nacimiento;
        $GeneralPersonalData->birth_place = $request->LugarNacimiento;
        $GeneralPersonalData->phone_number = $request->Telefono_job;
        $GeneralPersonalData->cellphone = $request->Celular;
        $GeneralPersonalData->email = $request->Email;
        $GeneralPersonalData->schoolarship = $request->Beca;
        $GeneralPersonalData->save();

        $PersonalAddress = PersonalAddress::where('personal_addresses_id', $student->id)->first();
        $PersonalAddress->street = $request->Calle;
        $PersonalAddress->ext_number = $request->NumeroExterior;
        $PersonalAddress->int_number = $request->NumeroInterior;
        $PersonalAddress->fraccionamiento = $request->Fraccionamiento;
        $PersonalAddress->zip_code = $request->CodigoPostal;
        $PersonalAddress->city = $request->Ciudad;
        $PersonalAddress->state = $request->Estado;
        $PersonalAddress->save();

        $PersonalReference = PersonalReference::where('personal_references_id', $student->id)->first();
        $PersonalReference->name = $request->NombreFamiliar;
        $PersonalReference->relationship = $request->Parentesco;
        $PersonalReference->occupation = $request->Ocupacion;
        $PersonalReference->address = $request->Fraccionamiento;
        $PersonalReference->meet_time = $request->ViveConUsted;
        $PersonalReference->phone_number = $request->NumeroFamiliar;
        $PersonalReference->save();

        return redirect()->route('student_status');
    }
}