<?php

namespace Swopyn\Http\Controllers\Jobtitleprofile;

use Swopyn\JobTitleDocuments;
use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class JobtitledocumentController extends Controller
{
	protected $rules =
	[
		'Id_Profile' => 'Required',
		'Documento' => 'Required',
	];

	public function add(Request $request){
		$tmp = DB::table('job_title_documents')->where('job_title_profiles_id', $request->Id_Profile)->get();
		if(count($tmp)<1){
			$validator = Validator::make(Input::all(), $this->rules);
			if ($validator->fails()) {
				return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
			} 
			else{
				$delete = DB::table('job_title_documents')->where('job_title_profiles_id', $request->Id_Profile)->delete();
				foreach ($request->Documento as $d) {
					$jobtitle = New JobTitleDocuments;
					$jobtitle->job_title_profiles_id = $request->Id_Profile;
					$jobtitle->job_title_documents_id = $request->Id_Profile;
					$jobtitle->document_name = $d;
					$jobtitle->save();
				}
				return response()->json($jobtitle);
			}
		}
		elseif(isset($request->Documento)){
			$docsDB = DB::table('job_title_documents')->where('job_title_profiles_id', $request->Id_Profile)->get();
			foreach ($docsDB as $ddb) {
				$isContained = false;
				foreach ($request->Documento as $d) {
					if($ddb->document_name == $d)
						$isContained = true;
				}
				if(!$isContained){
					$delete = DB::table('job_title_documents')->where('id', $ddb->id)->delete();
				}
			}
			$docsDB2 = DB::table('job_title_documents')->where('job_title_profiles_id', $request->Id_Profile)->get();
			foreach ($request->Documento as $d) {
				$isContained = false;
				foreach ($docsDB2 as $ddb) {
					if($d == $ddb->document_name)
						$isContained = true;
				}
				if(!$isContained){
					$jobtitle = New JobTitleDocuments;
					$jobtitle->job_title_profiles_id = $request->Id_Profile;
					$jobtitle->job_title_documents_id = $request->Id_Profile;
					$jobtitle->document_name = $d;
					$jobtitle->save();
				}
			}
			return response()->json();
		}
	}
}
