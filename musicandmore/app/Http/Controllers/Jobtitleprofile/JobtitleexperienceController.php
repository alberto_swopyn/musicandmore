<?php

namespace Swopyn\Http\Controllers\Jobtitleprofile;

use Swopyn\JobTitleExperience;
use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class JobtitleexperienceController extends Controller
{
	protected $rules =
    [
    'Experiencia' => 'Required',
    'AñosExperiencia' => 'Required',
    'Id_Profile' => 'Required',
    ];

    public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} else {
        //add data 
			$language = New JobTitleExperience;
			$language->save();

			$lang = JobTitleExperience::all()->last();

			$lang->job_title_experiences_id = $lang->id;
			$lang->job_title_profiles_id = $request->Id_Profile;
			$lang->job_title_experiences_id = $request->Id_Profile;
			$lang->experience_name = $request->Experiencia;
			$lang->experience_time = $request->AñosExperiencia;

			$lang->save();

			return response()->json($lang);
		}
	}
	public function delete(Request $request){
    $tmp = DB::table('job_title_experiences')->where('id', $request->Id)->delete();
    return response()->json($tmp);
	}  
}
