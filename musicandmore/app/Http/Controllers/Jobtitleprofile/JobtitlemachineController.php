<?php

namespace Swopyn\Http\Controllers\Jobtitleprofile;

use Swopyn\JobTitleMachine;
use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class JobtitlemachineController extends Controller
{
    protected $rules =
    [
    'Maquinas' => 'Required',
    'Id_Profile' => 'Required',
    ];

    public function add(Request $request){

		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} else {
        //add data 
			$job = New JobTitleMachine;
			$job->job_title_machines_id = $request->Id_Profile;
			$job->job_title_profiles_id = $request->Id_Profile;
			$job->machine_name = $request->Maquinas;

			$job->save();

			return response()->json($job);
		}
	}
	public function delete(Request $request){
		$tmp = DB::table('job_title_machines')->where('id', $request->Id)->delete();
        return response()->json($tmp);
	}
}
