<?php

namespace Swopyn\Http\Controllers\Jobtitleprofile;

use Swopyn\JobTitleProfile;
use Swopyn\JobTitleScholarship;
use Swopyn\JobTitleContract;
use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;


class JobtitleprofileController extends Controller
{   


    protected $rules =
    [
        'Objetivo_Puesto' => 'Required',
        'SalarioMin' => 'Required',
        'SalarioMax' => 'Required',
        'Funcion_Puesto' => 'Required',
    ];

    protected $rulescontract =
    [
        'Id_Profile' => 'Required',
        'Contrato' => 'Required',
    ];

    protected $rulesinterview =
    [
        'Id_Profile' => 'Required',
        'EntrevistaPuesto' => 'Required',
    ];

    protected $rulesdemographic =
    [
        'Id_Profile' => 'Required',
        'minAge' => 'Required',
        'maxAge' => 'Required',
        'GeneroPuesto' => 'Required',
        'EstadoCivilPuesto' => 'Required',
    ];

    protected $rulesscholar =
    [
        'GradoAcademicoProfile' => 'Required',
        'CarreraProfile' => 'Required',
        'Id_Profile' => 'Required',
    ];
    protected $rulesscholardelete =
    [
        'Id_Profile' => 'Required',
    ];

    public function create($jobprofile_id, $jobprofile_name){
        $temp_jobprofile = DB::table('job_title_profiles')->where('job_title_profiles_id', $jobprofile_id)->first();
        $contract = DB::table('contract_types')->get();
        $docs = DB::table('general_documents')->get();
        if (count($temp_jobprofile) <= 0){
            $jobprofile = New JobTitleProfile;
        }else
        {
            $jobprofile = JobTitleProfile::find($temp_jobprofile->id);
        }
        $jobprofile->job_title_profiles_id = $jobprofile_id;
        $jobprofile->job_title_name = $jobprofile_name;
        $jobprofile->save();
        $res = DB::table('job_title_responsabilities')->where('job_title_profiles_id', $jobprofile_id)->get();
        $fun = DB::table('job_title_functions')->where('job_title_profiles_id', $jobprofile_id)->get();
        $att = DB::table('job_title_attitudes')->where('job_title_profiles_id', $jobprofile_id)->get();
        $exp = DB::table('job_title_experiences')->where('job_title_profiles_id', $jobprofile_id)->get();
        $lan = DB::table('job_title_languages')->where('job_title_profiles_id', $jobprofile_id)->get();
        $lan2 = DB::table('job_title_languages')->where('job_title_profiles_id',$jobprofile_id)->get();
        $funoff = DB::table('job_office_functions')->where('job_title_profiles_id', $jobprofile_id)->get();
        $mac = DB::table('job_title_machines')->where('job_title_profiles_id', $jobprofile_id)->get();
        $soft = DB::table('job_title_softwares')->where('job_title_profiles_id', $jobprofile_id)->get();
        $doc = DB::table('job_title_documents')->where('job_title_profiles_id', $jobprofile_id)->get();
        $con = DB::table('job_title_contracts')->where('job_title_profiles_id', $jobprofile_id)->get();
        $lanknow =DB::table('language_knowledge')->select('language')->get();
        $interviews = DB::table('commands')->get();
        $jobCenters = DB::table('tree_job_centers')->where('company_id', 4)->get();
        
        $arraylan=array();
        foreach ($lanknow as $lk) {
            $isContained = false;
            foreach ($lan2 as $lan) {
                if ($lk->language == $lan->language) {
                    $isContained = true;
                }
            }
            if (!$isContained) {
                $arraylan[] = $lk;
            }
        }
        $arr=array("lanknow" => array($arraylan));
        $lanlevel = DB::table('language_level_knowledge')->get();
        $edu_level = DB::table('education_levels')->get();
        $carrers = DB::table('carrers')->get();
        $scho_profile = DB::table('job_title_scholarship as jt')->join('education_levels as el', 'jt.academic_grade','=', 'el.id')->join('carrers','degree_name', '=', 'carrers.id')->select('jt.id', 'el.name as grade_academic', 'carrers.name as carrer') ->where('job_title_profiles_id', $jobprofile_id)->get();
        $extraDoc = array();
        foreach ($doc as $d) {
            $isContent = false;
            foreach ($docs as $ds) {
                if ($d->document_name == $ds->text) {
                    $isContent = true;
                }
            }
            if (!$isContent) {
                $extraDoc[] = $d->document_name;
            }
        }

        return view('adminlte::jobtitleprofile.create')->with([ 'jobprofile' => $jobprofile , 'contracts' => $contract, 'res' => $res ,'fun' => $fun,'att' => $att, 'exp' => $exp, 'lan' => $lan2, 'funoff' => $funoff, 'mac' => $mac, 'soft' => $soft, 'doc' => $doc, 'con' => $con, 'edu_level' => $edu_level, 'carrers' => $carrers, 'lanknow' => $lanknow, 'lanlevel' => $lanlevel, 'scho_profile' => $scho_profile, 'arr' => $arraylan, 'docs' => $docs, 'extraDoc' => $extraDoc, 'interviews' => $interviews, 'jobcenters' => $jobCenters]);
    }

    
    public function addprofile(Request $request){

     $validator = Validator::make(Input::all(), $this->rules);
     if ($validator->fails()) {
         return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
     } else {
        $job = JobTitleProfile::where('job_title_profiles_id', '=', $request->Id_Profile)->first();
        $job->job_title_objetive = $request->Objetivo_Puesto;
        $job->min_salary = $request->SalarioMin;
        $job->max_salary = $request->SalarioMax;
        $job->function_type = $request->Funcion_Puesto;
        
        $job->save();
        
        return response()->json($job);
    }
}

public function addContract(Request $request){
    $tmp=DB::table('job_title_contracts')->where('job_title_profiles_id', $request->Id_Profile)->get();
    if(count($tmp)<1){
        $validator = Validator::make(Input::all(), $this->rulescontract);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        else {
            $delete = DB::table('job_title_contracts')->where('job_title_profiles_id', $request->Id_Profile)->delete();
            foreach ($request->Contrato as $c) {
                $jobtitle = New JobTitleContract;
                $jobtitle->job_title_profiles_id = $request->Id_Profile;
                $jobtitle->job_title_contracts_id = $request->Id_Profile;
                $jobtitle->contract_types_id = $c;
                $jobtitle->save();
            }
            return response()->json($jobtitle);
        }
    }
    elseif(isset($request->Contrato)){
        $DB = DB::table('job_title_contracts')->where('job_title_profiles_id', $request->Id_Profile)->get();
        foreach ($DB as $db) {
            $isContained = false;
            foreach ($request->Contrato as $c) {
                if($db->contract_types_id == $c)
                    $isContained = true;
            }
            if(!$isContained){
                $delete = DB::table('job_title_contracts')->where('id', $db->id)->delete();
            }
        }
        $DB2 = DB::table('job_title_contracts')->where('job_title_profiles_id', $request->Id_Profile)->get();
        foreach ($request->Contrato as $c) {
            $isContained = false;
            foreach ($DB2 as $db) {
                if($c == $db->contract_types_id)
                    $isContained = true;
            }
            if(!$isContained){
                $jobtitle = New JobTitleContract;
                $jobtitle->job_title_profiles_id = $request->Id_Profile;
                $jobtitle->job_title_contracts_id = $request->Id_Profile;
                $jobtitle->contract_types_id = $c;
                $jobtitle->save();
            }
        }
    }
}
public function addInterview(Request $request){
    $validator = Validator::make(Input::all(), $this->rulesinterview);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    }else {
        $agregar_entrevista = JobTitleProfile::where('job_title_profiles_id', '=', $request->Id_Profile)->first();
        $agregar_entrevista->interview_type = $request->EntrevistaPuesto;
        $agregar_entrevista->save();
        return response()->json($agregar_entrevista);
    }
}

public function addDemographic(Request $request){
    $validator = Validator::make(Input::all(), $this->rulesdemographic);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    }else {
        $job = JobTitleProfile::where('job_title_profiles_id', '=', $request->Id_Profile)->first();
        $job->min_age = $request->minAge;
        $job->max_age = $request->maxAge;
        $job->gender = $request->GeneroPuesto;
        $job->civil_status = $request->EstadoCivilPuesto;
        $job->save();
        return response()->json($job);
    }
}
public function select_education_profile(Request $request){
    $temp = DB::table('carrers')->where('education_level_id', $request->Edu_level)->get();
    return response()->json($temp);
}
public function addscholar(Request $request){
    $validator = Validator::make(Input::all(), $this->rulesscholar);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    }else {
        $job = new JobTitleScholarship;
        $job->job_title_profiles_id = $request->Id_Profile;
        $job->job_title_scholarship_id = $request->Id_Profile;
        $job->academic_grade = $request->GradoAcademicoProfile;
        $job->degree_name = $request->CarreraProfile;
        $job->save();
        return response()->json($job);
    }
}
public function deletescholar(Request $request){
    $tmp = DB::table('job_title_scholarship')->where('id', $request->Id)->delete();
    return response()->json($tmp);
}
}
