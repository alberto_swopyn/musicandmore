<?php

namespace Swopyn\Http\Controllers\Company;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\PayType;
use Swopyn\Company;
use Swopyn\Http\Requests\Company\CreateCompanyRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;



class CompanyController extends Controller
{
	public function login(){
		return view('adminlte::company.login');
	}


	public function index(){
		$company = Company::with("contact")->simplePaginate(3);
		//sdd($company);
		return view('adminlte::company.index')->with(['companies' => $company ]);
	}


     public function create(){
     	$paytype = PayType::all();
     	$company = new Company;
    	return view('adminlte::company.create')->with(['paytypes'=> $paytype, 'company'=>$company]);
    }


    public function add(CreateCompanyRequest $request){

    	// images config
    	$imglogo = Input::file('Logo');
		$logo = Image::make($imglogo)->fit(200,80)->encode('png');

		
		$imgimage = Input::file('Imagen');
		$imagen = Image::make($imgimage)->fit(300)->encode('png');

		//add data
    	$company = New Company;
    	$company->name = $request->get('Nombre');
		$company->rfc = $request->get('Rfc');
		$company->address = $request->get('Direccion');
		$company->logo = $logo;
		$company->image = $imagen;
		$company->phone = $request->get('Telefono');
		$company->bussines_name = $request->get('NombreCompañia');
		$company->tax_regime = $request->get('Persona');
		$company->bank_account = $request->get('CuentaBanco');
		$company->pay_type_id = $request->get('TipoPago');
		$company->company_type = $request->get('Giro');
		$company->specialty = $request->get('Especialidad');
		$company->no_employees = $request->get('NoEmpleados');
		$company->no_branch_office = $request->get('NoSucursal');

		$company->save();

		return redirect('');


    }

    public function edit(Company $company){
    	//dd($company);
    	$paytype = PayType::all();
    	return view('adminlte::company.edit')->with(['company' => $company, 'paytypes' => $paytype]);

    }


    public function showimg($id){
    	$image = Company::findOrFail($id);
    	$ima = Image::make($image->logo);
    	$response = Response::make($ima->encode('jpeg'));
    	$response->header('Content-type', 'image/jpeg');
    	return $response;

    }


    public function update(Company $company, Request $request ){
    	
    	$this->validate($request,[
    	    'Nombre' => 'Required',
            'Rfc' => 'Required',
            'Direccion' => 'Required',
            'Telefono' => 'Required',
            'NombreCompañia' => 'Required',
            'Persona' => 'Required',
            'CuentaBanco' => 'Required',
            'TipoPago' => 'Required',
            'Giro' => 'Required',
            'Especialidad' => 'Required',
            'NoEmpleados' => 'Required',
            'NoSucursal' => 'Required',
           	]);
    	// images config

    	if ($request->hasFile('Imagen')){
    		$imgimage = Input::file('Imagen');
			$imagen = Image::make($imgimage)->fit(300)->encode('png');
			$company->image = $imagen;

    	}
    	if ($request->hasFile('Logo')){
    		$imglogo = Input::file('Logo');
			$logo = Image::make($imglogo)->fit(200,80)->encode('png');
			$company->logo = $logo;
		}


    	$company->name = $request->get('Nombre');
		$company->rfc = $request->get('Rfc');
		$company->address = $request->get('Direccion');
		$company->phone = $request->get('Telefono');
		$company->bussines_name = $request->get('NombreCompañia');
		$company->tax_regime = $request->get('Persona');
		$company->bank_account = $request->get('CuentaBanco');
		$company->pay_type_id = $request->get('TipoPago');
		$company->company_type = $request->get('Giro');
		$company->specialty = $request->get('Especialidad');
		$company->no_employees = $request->get('NoEmpleados');
		$company->no_branch_office = $request->get('NoSucursal');

		$company->save();

		return view('adminlte::home');

    }

    public function delete(Company $company){
    	$company->delete();
    	return redirect('');

    }

    public function course(){
    	return view('adminlte::company.course');
    }

    public function courseupdate(Company $company, Request $request){
    	$id_company = $request->name;
    	return;

    }

    
}











