<?php

namespace Swopyn\Http\Controllers\Evaluationcommittee;

use Swopyn\EvaluationCommittee;
use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class EvaluationcommitteeController extends Controller
{
  protected $rules =
  [
    'Nombre' => 'required',
    'Apellidos' => 'required',
    'Procede' => 'required',
  ];

  protected $rulesCommittee = 
  [
  	'NombreGerenteRegional' => 'required',
    'NombreRecursosHumanos' => 'required',
    'NombreAsesorCaptacion' => 'required',
    'Id' => 'required',
  ];

  protected $rulesComment = 
  [
    'Id' => 'required',
  ];

  public function add(Request $request){

         $validator = Validator::make(Input::all(), $this->rules);
         if ($validator->fails()) {
             return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
         } else {

        $evaluation = New EvaluationCommittee;
        $evaluation->save();

        $committee = EvaluationCommittee::all()->last();

        $committee->evaluation_committees_id = $committee->id;
        $committee->candidate_first_name = $request->Nombre;
        $committee->candidate_last_name = $request->Apellidos;
        $committee->proceeds_evaluation = $request->Procede;

        $committee->save();
        
        return response()->json($committee);
        }
    }
    public function addCommittee(Request $request){
        $validator = Validator::make(Input::all(), $this->rulesCommittee);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {
                $committee = EvaluationCommittee::find($request->Id);
                $committee->territorial_manager = $request->NombreGerenteRegional;
                $committee->human_resources = $request->NombreRecursosHumanos;
                $committee->capture_advisor = $request->NombreAsesorCaptacion;
                $committee->save();
                return response()->json($committee);
        }
    }
    public function addComment(Request $request){
        $validator = Validator::make(Input::all(), $this->rulesComment);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {
                $committee = EvaluationCommittee::find($request->Id);
                $committee->commission_agent = $request->Comisionista;
                $committee->duty_manager_1 = $request->EncTienda1;
                $committee->duty_manager_2 = $request->EncTienda2;
                $committee->floor_assistant_1 = $request->AuxPiso1;
                $committee->floor_assistant_2 = $request->AuxPiso2;
                $committee->floor_assistant_3 = $request->AuxPiso3;
                $committee->save();
                return response()->json($committee);
        }
    }
}
