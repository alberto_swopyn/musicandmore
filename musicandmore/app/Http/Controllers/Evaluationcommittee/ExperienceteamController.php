<?php

namespace Swopyn\Http\Controllers\Evaluationcommittee;

use Swopyn\ExperienceTeam;
use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class ExperienceteamController extends Controller
{
	protected $rules = 
	[
	'id' => 'Required',
	'ventas_1' => 'Required',
	'ventas_2' => 'Required',
	'ventas_3' => 'Required',
	'ventas_4' => 'Required',
	'ventas_5' => 'Required',
	'inventario_1' => 'Required',
	'inventario_2' => 'Required',
	'inventario_3' => 'Required',
	'inventario_4' => 'Required',
	'inventario_5' => 'Required',
	'serviciocliente_1' => 'Required',
	'serviciocliente_2' => 'Required',
	'serviciocliente_3' => 'Required',
	'serviciocliente_4' => 'Required',
	'serviciocliente_5' => 'Required',
	'manejocaja_1' => 'Required',
	'manejocaja_2' => 'Required',
	'manejocaja_3' => 'Required',
	'manejocaja_4' => 'Required',
	'manejocaja_5' => 'Required',
	'puestodesempeñado_1' => 'Required',
	'puestodesempeñado_2' => 'Required',
	'puestodesempeñado_3' => 'Required',
	'puestodesempeñado_4' => 'Required',
	'puestodesempeñado_5' => 'Required',
	'salario_1' => 'Required',
	'salario_2' => 'Required',
	'salario_3' => 'Required',
	'salario_4' => 'Required',
	'salario_5' => 'Required',
	'asignacionroles' => 'Required'
	];
	public function add(Request $request){

         $validator = Validator::make(Input::all(), $this->rules);
         if ($validator->fails()) {
             return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
         } else {

        $evaluation = New ExperienceTeam;
        $evaluation->save();

        $committee = ExperienceTeam::all()->last();

        $committee->experience_teams_id = $committee->id;
        $committee->evaluation_committees_id = $request->id;
        $committee->sale_1 = $request->ventas_1;
        $committee->sale_2 = $request->ventas_2;
        $committee->sale_3 = $request->ventas_3;
        $committee->sale_4 = $request->ventas_4;
        $committee->sale_5 = $request->ventas_5;
        $committee->inventory_1 = $request->inventario_1;
        $committee->inventory_2 = $request->inventario_2;
        $committee->inventory_3 = $request->inventario_3;
        $committee->inventory_4 = $request->inventario_4;
        $committee->inventory_5 = $request->inventario_5;
        $committee->customer_service_1 = $request->serviciocliente_1;
        $committee->customer_service_2 = $request->serviciocliente_2;
        $committee->customer_service_3 = $request->serviciocliente_3;
        $committee->customer_service_4 = $request->serviciocliente_4;
        $committee->customer_service_5 = $request->serviciocliente_5;
        $committee->cash_management_1 = $request->manejocaja_1;
        $committee->cash_management_2 = $request->manejocaja_2;
        $committee->cash_management_3 = $request->manejocaja_3;
        $committee->cash_management_4 = $request->manejocaja_4;
        $committee->cash_management_5 = $request->manejocaja_5;
        $committee->since_performance_1 = $request->puestodesempeñado_1;
        $committee->since_performance_2 = $request->puestodesempeñado_2;
        $committee->since_performance_3 = $request->puestodesempeñado_3;
        $committee->since_performance_4 = $request->puestodesempeñado_4;
        $committee->since_performance_5 = $request->puestodesempeñado_5;
        $committee->salary_1 = $request->salario_1;
        $committee->salary_2 = $request->salario_2;
        $committee->salary_3 = $request->salario_3;
        $committee->salary_4 = $request->salario_4;
        $committee->salary_5 = $request->salario_5;
        $committee->role_assignment = $request->asignacionroles;


        $committee->save();
        
        return response()->json($committee);
        }
    }
}
