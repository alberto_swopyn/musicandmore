<?php

namespace Swopyn\Http\Controllers\Evaluationcommittee;

use Swopyn\FamilyEvaluationCommittee;
use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class FamilyevaluationController extends Controller
{
    protected $rules = [
    'id' => 'Required',
	'family_integration' => 'Required',
	'puntuality' => 'Required',
	'personal_cleanliness' => 'Required',
	'family_communication' => 'Required',
	'supported_values' => 'Required',
	'expenses' => 'Required',
	'operative_vs_activity' => 'Required',
	'store_responsability' => 'Required',
	'candidate_integration' => 'Required'
    ];

    public function add(Request $request){

         $validator = Validator::make(Input::all(), $this->rules);
         if ($validator->fails()) {
             return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
         } else {

        $evaluation = New FamilyEvaluationCommittee;
        $evaluation->evaluation_committees_id = $request->id;
        $evaluation->family_integration = $request->family_integration;
        $evaluation->puntuality = $request->puntuality;
        $evaluation->personal_cleanliness = $request->personal_cleanliness;
        $evaluation->family_communication = $request->family_communication;
        $evaluation->supported_values = $request->supported_values;
        $evaluation->expenses = $request->expenses;
        $evaluation->operative_vs_activity = $request->operative_vs_activity;
        $evaluation->store_responsability = $request->store_responsability;
        $evaluation->candidate_integration = $request->candidate_integration;

        $evaluation->save();
        
        return response()->json($evaluation);
        }
    }

}
