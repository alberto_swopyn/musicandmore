<?php

namespace Swopyn\Http\Controllers\Evaluationcommittee;

use Swopyn\BusinessExperienceEvaluation;
use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class BusinessExperienceController extends Controller
{
    private $rules = 
    [
    	'Id' => 'Required',
    	'sales_service' => 'Required',
    	'staff_management' => 'Required',
    	'inventory_management' => 'Required',
    	'cash_management' => 'Required',
    	'sales_experience' => 'Required',
    	'computer_skills' => 'Required',
    	'fluid_communication' => 'Required',
    	'good_leader' => 'Required',
    	'ordained_person' => 'Required',
    	'tolerance' => 'Required',
    	'positive_enthusiastic_person' => 'Required',
    	'discipline_person' => 'Required',
    	'potential_candidate' => 'Required',
    	'neat_person' => 'Required',
    	'committed_person' => 'Required',
    	'shop_management' => 'Required',
    	'clear_idea' => 'Required',
    	'track_record' => 'Required',
    	'stable_person' => 'Required',
    ];
    public function add(Request $request){

         $validator = Validator::make(Input::all(), $this->rules);
         if ($validator->fails()) {
             return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
         } else {

        $evaluation = New BusinessExperienceEvaluation;
        $evaluation->save();

        $committee = BusinessExperienceEvaluation::all()->last();

        $committee->business_experience_evaluations_id = $committee->id;
        $committee->evaluation_committees_id = $request->Id;
        $committee->sales_service = $request->sales_service;
        $committee->staff_management = $request->staff_management;
        $committee->inventory_management = $request->inventory_management;
        $committee->cash_management = $request->cash_management;
        $committee->sales_experience = $request->sales_experience;
        $committee->computer_skills = $request->computer_skills;
        $committee->fluid_communication = $request->fluid_communication;
        $committee->good_leader = $request->good_leader;
        $committee->ordained_person = $request->ordained_person;
        $committee->tolerance = $request->tolerance;
        $committee->positive_enthusiastic_person = $request->positive_enthusiastic_person;
        $committee->discipline_person = $request->discipline_person;
        $committee->potential_candidate = $request->potential_candidate;
        $committee->neat_person = $request->neat_person;
        $committee->committed_person = $request->committed_person;
        $committee->shop_management = $request->shop_management;
        $committee->clear_idea = $request->clear_idea;
        $committee->track_record = $request->track_record;
        $committee->stable_person = $request->stable_person;

        $committee->save();
        
        return response()->json($committee);
        }
    }
}
