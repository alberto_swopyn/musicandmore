<?php

namespace Swopyn\Http\Controllers\Tracking;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Swopyn\OperativeBehavioralInterview;
use Swopyn\PsychometricLevel;
use Swopyn\Tracking;
use Swopyn\User;
use Illuminate\Support\Facades\Storage;


class TrackingController extends Controller
{


	public function welcome(){
		return view('adminlte::tracking.welcome');
	}

	public function start(Request $request){
		$candidateTemp = DB::table('trackings')->where('user_id',$request->user)->first();

		if (count($candidateTemp) <= 0){
			$tracking = New Tracking;
			$tracking->user_id = $request->user;

		}else{
			$id = $candidateTemp->id;
			$tracking = Tracking::find($id);

		}

		$tracking->save();

		return redirect('/jobapplication/create');
	}

	public function video(Request $request){
		$candidateTemp = DB::table('trackings')->where('user_id',$request->user)->first();

		if (count($candidateTemp) <= 0){
			$tracking = New Tracking;
			$tracking->user_id = $request->user;

		}else{
			$id = $candidateTemp->id;
			$tracking = Tracking::find($id);

		}
		$tracking->video = 1;
		$tracking->save();

		return redirect()->route('jobapplication');

	}

	public function document(){
		$candidateTemp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();

		if (count($candidateTemp) <= 0){
			$tracking = New Tracking;
			$tracking->user_id = $request->user;

		}else{
			$id = $candidateTemp->id;
			$tracking = Tracking::find($id);

		}
		$tracking->document = 1;
		$tracking->save();
		return redirect()->route('tracking_index');
	}

	public function interview(){
		$candidateTemp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();

		if (count($candidateTemp) <= 0){
			$tracking = New Tracking;
			$tracking->user_id = $request->user;

		}else{
			$id = $candidateTemp->id;
			$tracking = Tracking::find($id);

		}
		$tracking->interview = 1;
		$tracking->save();
		$tracking = Tracking::all();
		return view('adminlte::tracking.index')->with(['trackings' => $tracking]);
	}

	public function course(Request $request){

		$candidateTemp = DB::table('trackings')->where('user_id',$request->user)->first();
		$cont = 0;

		if (count($candidateTemp) <= 0){
			$tracking = New Tracking;
			$tracking->user_id = $request->user;

		}else{
			$id = $candidateTemp->id;
			$tracking = Tracking::find($id);

		}
		if ($tracking->lms < $request->slider){
			$tracking->lms= $request->slider;
			$tracking->save();
		}

	}

	public function level(Request $request){

		$levels = DB::table('psychometric_levels')->where('hits2', '<=', $request->result)->where('hits1', '>=', $request->result)->get();


		return Response::json($levels);
	}

	public function interview_result(Request $request){
		$interview_result = DB::table('section_grades as sg')->join('sections as sc', 'sg.sections_id', 'sc.id')->select('sg.grade as grade', 'sc.name','sc.value', 'sc.max')->where('users_id', $request->Applicant_id)->orderby('sc.value', 'asc')->get();
		//dd($interview_result);
		return Response::json($interview_result);
	}

	public function index(){

		$tracking = Tracking::orderBy('created_at', 'desc')->simplePaginate(10);
		$jobCenter = DB::table('tree_job_centers')->join('profile_job_centers as jobCenters', 'id_inc', 'jobCenters.profile_job_centers_id')->where('company_id', '4')->get();
		$interviews = DB::table('scheduled_interviews')->get();
		$column = DB::table('course_progresses')->select(DB::raw('AVG(progress) as promedio'),'user_id')->groupBy('course_progresses.user_id')->get();
		$type = DB::table('job_title_profiles as job')->join('trackings as tra', 'job.id', 'tra.job_title_profile_id')->select('tra.user_id', 'job.function_type', 'user_id', 'job.id')->get();

		return view('adminlte::tracking.index')->with(['types' => $type, 'trackings' => $tracking, 'jobcenter' => $jobCenter, 'allInterviews' => $interviews, 'progress' => $column ]);
	}


	public function contract($user){
    	//$files = Storage::disk('local')->files('public');
    	//$file = Storage::url('Contract_'.$user.'.pdf');
		// unset($directories[0], $directories[1]);
		//dd($files);

		return response()->file(storage_path('app/public/'.'Contract_'.$user.'.pdf'));
	}


	public function show(){

	}
	public function match(){
    	//$user = DB::table('users')->where('name','!=',NULL)->first();
		//->with([ 'user' => $user ]);
		$array = array();
		$temp1 = DB::table('trackings')->where('job_application','!=',NULL)->get();
		foreach ($temp1 as $tmp) {
			$temp = DB::table('general_personal_datas')->where('first_name','!=', '')->where('general_personal_data_id',$tmp->user_id)->first();
			$array[] = $temp;
		}
		return view('adminlte::tracking.match') -> with(['applicants' => $array]);
	}

	public function matchporcentaje(Request $request)
	{
		$porcentaje = DB::table('trackings')->select('percentage_match')->where('user_id', $request->applicant)->first();
		$temp = DB::table('general_personal_datas')->select('general_personal_data_id','first_name','last_name')->where('general_personal_data_id', $request->applicant)->first();
		$work = DB::table('work_general_datas')->where('work_general_datas_id', $request->applicant)->first();
		$job_title =DB::table('job_title_profiles')->select('job_title_name')->where('id', $work->job_title_profile_id)->first();
		$arr = array("personal_information" => array($temp), "work" => array($job_title),'porcentaje' => array($porcentaje));
		return response()->json($arr);

	}

	public function matchdetalles(Request $request){
		$temp = DB::table('general_personal_datas')->select('gender','age','civil_status')->where('general_personal_data_id', $request->applicant)->first();
		$work = DB::table('work_general_datas')->where('work_general_datas_id', $request->applicant)->first();
		$job_title =DB::table('job_title_profiles')->select('gender','min_age','max_age','civil_status')->where('id', $work->job_title_profile_id)->first();
		$languagessol = DB::table('job_title_languages')->select('language','level')->where('job_title_profiles_id',$work->job_title_profile_id)->get();
		$languagesasp = DB::table('language_applications')->select('language','level')->where('job_applications_id', $request->applicant)->get();
		$scho_data = DB::table('scholarship_datas')->select('degree_name','academic_grade')->where('scholarship_datas_id', $request->applicant)->get();
		$scho_data_sol = DB::table('job_title_scholarship')->select('degree_name','academic_grade')->where('job_title_scholarship_id', $work->job_title_profile_id)->get();
		$array_aca_grade = array();
		$array_degree_name=array();
		$array_aca_grade_sol = array();
		$array_degree_name_sol =array();
		foreach ($scho_data as $scho_data) {
			$aca_grade =DB::table('education_levels')->where('id', $scho_data->academic_grade)->get();
			foreach ($aca_grade as $aca_grade) {
				$array_aca_grade[] = $aca_grade->name;
			}
			$degree_name=Db::table('carrers')->where('id', $scho_data->degree_name)->get();
			foreach ($degree_name as $degree_name) {
				$array_degree_name[] = $degree_name->name;
			}
		}
		foreach ($scho_data_sol as $scho_data_sol) {
			$aca_grade =DB::table('education_levels')->where('id', $scho_data_sol->academic_grade)->get();
			foreach ($aca_grade as $aca_grade) {
				$array_aca_grade_sol[] = $aca_grade->name;
			}
			$degree_name=Db::table('carrers')->where('id', $scho_data_sol->degree_name)->get();
			foreach ($degree_name as $degree_name) {
				$array_degree_name_sol[] = $degree_name->name;
			}
		}

		$arr = array("personal_information" => array($temp),"work" => array($job_title),'lansol' => array($languagessol ),"lanasp"=> array($languagesasp ),"aca_grade"=> array($array_aca_grade),"degree_name"=>array($array_degree_name),"scho_data_sol"=>array($scho_data_sol),"aca_grade_sol"=> array($array_aca_grade_sol),"degree_name_sol"=>array($array_degree_name_sol));
		return response()->json($arr);
	}
}
