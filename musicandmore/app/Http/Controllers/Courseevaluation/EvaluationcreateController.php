<?php

namespace Swopyn\Http\Controllers\Courseevaluation;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\OptionEvaluation;
use Swopyn\QuestionEvaluation;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use DB;

class EvaluationcreateController extends Controller
{
	protected $rules = 
	[
		'Question' => 'required',
	];

	public function create($course,$question_id){
		$tmp = DB::table('trainings')->where('id', $course)->first();
		isset($question_id) ? $question = DB::table('question_evaluations')->where('id', $question_id)->first() : $question = null;
		!empty($question_id) ? $options = DB::table('option_evaluations')->where('question_evaluation_id', $question_id)->get() : $options = null;
		return view('adminlte::courseevaluation.creation.index')->with(['course' => $tmp, 'question' => $question, 'options' => $options]);
	}

	public function editView($question_id){
		$question = DB::table('question_evaluations')->where('id', $question_id)->first();
		$tmp = DB::table('trainings')->where('id', $question->trainings_id)->first();
		return view('adminlte::courseevaluation.creation.index')->with(['course' => $tmp, 'question' => $question]);
	}

	public function kardex($course){
		$tmp = DB::table('trainings')->where('id', $course)->first();
		$questions = DB::table('question_evaluations')->where('trainings_id', $tmp->id)->get();
		return view('adminlte::courseevaluation.creation.kardex')->with(['course' => $tmp, 'questions' => $questions]);
	}

	public function save(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} 
		else {
			$tmp = new QuestionEvaluation;
			$tmp -> trainings_id = $request -> CourseID;
			$tmp -> question = $request -> Question;
			$tmp -> type = $request -> Type;
			$tmp->save();
			if(!empty($request->Options)){
				foreach ($request->Options as $op) {
					$options = new OptionEvaluation;
					$options -> question_evaluation_id = $tmp -> id;
					$options -> option = $op[1];
					$options -> validation = $op[0];
					$options -> save();
				}
			}

			return response()->json($tmp);
		}
	}
	public function edit(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules);
		if ($validator->fails()) {
			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
		} 
		else {
			$tmp = QuestionEvaluation::find($request->QuestionId);
			$tmp -> question = $request -> Question;
			$tmp -> type = $request -> Type;
			$tmp->save();
			$optionsTmp = OptionEvaluation::where('question_evaluation_id', '=', $tmp->id)->get();
			if(!empty($request->Options)){
				foreach ($optionsTmp as $oS) {
					$isContained = false;
					foreach ($request->Options as $oJ){
						if ($oS -> id == $oJ[2])
							$isContained = true;
					}
					if (!$isContained) {
						$delete = DB::table('option_evaluations')->where('id', $oS->id)->delete();
					}
				}
				foreach ($request->Options as $oJ) {
					if($oJ[2] == ""){
						$options = new OptionEvaluation;
						$options -> question_evaluation_id = $tmp -> id;
						$options -> option = $oJ[1];
						$options -> validation = $oJ[0];
						$options -> save();
					}
				}
			}
			return response()->json($optionsTmp);
		}
	}

	public function delete($question_id)
	{
		$option = DB::table('option_evaluations')->where('question_evaluation_id', $question_id)->delete();
		$tmp = QuestionEvaluation::find($question_id);
		$course_id = $tmp->trainings_id;
		$tmp = DB::table('question_evaluations')->where('id', $question_id)->delete();
		return redirect()->route('kardex_question_evaluation', ['course' => $course_id]);
		//$route = "location:".route('kardex_question_evaluation', ['course' => $course_id]);
		//header($route);
	}
}
