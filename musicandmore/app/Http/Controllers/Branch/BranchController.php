<?php

namespace Swopyn\Http\Controllers\Branch;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\BranchOffice;
use Swopyn\Http\Requests\Branch\CreateBranchRequest;


class BranchController extends Controller
{

    public function index(){
    	$branch = BranchOffice::simplePaginate(3);

    	return view('adminlte::branch.index')->with(['branchs' => $branch]);
    }

    public function create(){
    	return view('adminlte::branch.create');
    }

    public function add(CreateBranchRequest $request){

    	$branch = New BranchOffice;
    	$branch->name = $request->get('Nombre');
    	$branch->address = $request->get('Direccion');
		$branch->phone = $request->get('Telefono');
		$branch->no_employees = $request->get('NoEmpleados');
		$branch->company_id = 3;
		$branch->save();
		return redirect(route('branch_index'));
    }

    public function edit(BranchOffice $branch){
    	return view('adminlte::branch.edit')->with(['branch' => $branch]);
    }

    public function update(BranchOffice $branch, CreateBranchRequest $request){
    	$branch->name = $request->get('Nombre');
    	$branch->address = $request->get('Direccion');
		$branch->phone = $request->get('Telefono');
		$branch->no_employees = $request->get('NoEmpleados');
		$branch->company_id = 3;
		$branch->save();
		return redirect(route('branch_index'));

    }

    public function delete(BranchOffice $branch){
    	$branch->delete();
		return redirect(route('branch_index'));


    }
}
