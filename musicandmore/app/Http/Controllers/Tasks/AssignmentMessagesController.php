<?php

namespace Swopyn\Http\Controllers\Tasks;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\AssignmentMessages;
use DTO\Tasks\AssignmentMessageDTO;
use Swopyn\ProtocolBuffers\Utilities\DTOUtilities;
use Swopyn\ProtocolBuffers\Controllers\DTOWebResponseController;
use DTO\Web\CodeResponseDTO;
use Illuminate\Support\Collection;
use Swopyn\Assignment;

class AssignmentMessagesController extends Controller
{
#region Metodos Comunes
    private function GetMessagesByAssigmet(int $idAssigment) : Collection
    {
        $messages = AssignmentMessages::where(AssignmentMessages::IdAssignments , $idAssigment)->get();
        return $messages;
    } 
#endregion

#region Json
    public function GetMessagesByAssigmetJson(int $idAssigment) : void
    {
        $messages = $this->GetMessagesByAssigmet($idAssigment);

        echo $messages->toJson();
    } 
#endregion

#region Protocol Buffers

    public function GetMessagesByAssigmetProtoc(int $idAssigment) : void
    {
        $messages = $this->GetMessagesByAssigmet($idAssigment);

        $messagesList = array();
        $messagesList['list'] = $messages->toArray();

        $messagesDTO = new \DTO\Tasks\AssignmentMessagesDTO();
        $messagesDTO->mergeFromJsonString(json_encode($messagesList));

        echo $messagesDTO->serializeToString();
    } 

    /**
     * Guarda un assignmentMessageDTO. 
     *
     * @return void
     */
    public function SaveAssignmentMessagesProtoc() : void
    {
        $objString = file_get_contents('php://input');
        $assignmentMessageDTO = new AssignmentMessageDTO();
        $assignmentMessageDTO->mergeFromString($objString);

        $assignmentMessagesModel = AssignmentMessages::FromDTO($assignmentMessageDTO);

        $isSave = $assignmentMessagesModel->save();

        if ($isSave) {
            DTOWebResponseController::SendWebResponse(CodeResponseDTO::Accepted, "Registro agregado");
        } else {
            DTOWebResponseController::SendWebResponse(CodeResponseDTO::Conflict, "Error: No se pudo agregar el registro.");
        }
    }
#endregion
}
