<?php

namespace Swopyn\Http\Controllers\Tasks;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\ProtocolBuffers\Utilities\DTOUtilities;
use Swopyn\Assignment;
use Swopyn\AssignmentUser;
use Swopyn\AssignmentTask;
use DTO\Tasks\AssignmentsDTO;
use Swopyn\Managers\WebResponseManager;
use Swopyn\ModelCollections\AssignmentCollection;
use Illuminate\Support\Facades\DB;
use Swopyn\Task;

/**
 * Controlador de Asignaciones.
 */
class AssignmentController extends Controller
{
    /**
     * Obtiene la asignación por Id.
     *
     * @param int $id
     * @return JSON|Bytes[]
     */
    public function AssignmentById($id): void
    {
        $assigment = Assignment::find($id);
        WebResponseManager::SendResponse($assigment);
    }

    /**
     * Agrega un asignación.
     *
     * @return void
     */
    public function AddAssignment(): void
    {
        $assigmentDTO = DTOUtilities::DeserialiceDTO(new AssignmentsDTO());
        $assigment = Assignment::FromDTO($assigmentDTO);
        $assigment->save();
    }

    /**
     * Obtiene la asignación por usuario.
     *
     * @param int $id
     * @return JSON
     */
    public function GetAssignmetByUserJson($id): void
    {
        $assigmentDTO = Assignment::where(Assignment::UsersId, $id)->get();
        echo $assigmentDTO->ToJson();
    }

    /**
     * Obtiene la asignación por usuario.
     *
     * @param int $id
     * @return Byte[]
     */
    public function GetAssignmetByUserProtoc($id): void
    {
        $assignmentsDTO = DB::table('assignment_user')
            ->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')
            ->where('assignment_user.user_id', '=', $id)
            ->select('assignments.*')->get();
        $assigmentsListDTO = new AssignmentsDTO();
        $assigmentsList = array();
        $assigmentsList['list'] = $assignmentsDTO->toArray();
        $assigmentsListDTO->mergeFromJsonString(json_encode($assigmentsList));

        echo $assigmentsListDTO->serializeToString();
        //$assigmentDTO = Assignment::where(Assignment::UsersId, $id)->get();
        //$assigmentDTO = new AssignmentCollection($assigmentDTO);
        //$assignmentsDTO = new AssignmentCollection($assignmentsDTO);

        //echo $assigmentDTO->ToDTO()->serializeToString();
    }

    /**
     * Obtiene las asignaciones por tareas.
     *
     * @param int $id
     * @return JSON
     */
    public function GetAssignmetByTaskIdJson($id): void
    {
        $assigmentDTO = Assignment::where(Assignment::TaskId, $id)->get();
        echo $assigmentDTO->ToJson();
    }

    /**
     * Obtiene las asignaciones por tareas.
     *
     * @param int $id
     * @return Byte[]
     */
    public function GetAssignmetByTaskIdProtoc($id): void
    {
        $assigments = DB::table(Assignment::TableName)
            ->join('users', function ($join) {
                $join->on('assignments.users_id', '=', 'users.id');
            })
            ->select('assignments.*', 'users.id AS UserID', 'users.name AS UserName', 'users.email AS UserEmail')
            ->where(Assignment::TaskId, $id)
            ->get();

            
        $assigmentsListDTO = new AssignmentsDTO();

        foreach ($assigments as &$assigment)
        {
            $assigment->user = array(
                'id' => $assigment->UserID,
                'name' => $assigment->UserName,
                'email'=> $assigment->UserEmail
            );
        }

        $assigmentsList = array();
        $assigmentsList['list'] = $assigments->toArray();
        $assigmentsListDTO->mergeFromJsonString(json_encode($assigmentsList));

        echo $assigmentsListDTO->serializeToString();
    }

    /**
     * Undocumented function
     *
     * @param int $idUser
     * @return void
     */
    public function GetTaskCompletedByUserJson($idUser): void
    {
        $assigments = $this->GetTaskCompletedByUser($idUser);
        echo $assigments->ToJson();
    }

    /**
     * Undocumented function
     *
     * @param int $idUser
     * @return Byte []
     */
    public function GetTaskCompletedByUserProtoc($idUser): void
    {
        $assigments = $this->GetTaskCompletedByUser($idUser);

        $assigmentsListDTO = new AssignmentsDTO();
        $assigmentsList = array();
        $assigmentsList['list'] = $assigments->toArray();
        $assigmentsListDTO->mergeFromJsonString(json_encode($assigmentsList));

        echo $assigmentsListDTO->serializeToString();
    }

    /**
     * Undocumented function
     *
     * @param int $idUser
     * @return void
     */
    public function GetTaskInCompleteByUserJson($idUser): void
    {
        $assigments = $this->GetTaskCompletedByUser($idUser, 0);
        echo $assigments->ToJson();
    }

    /**
     * Undocumented function
     *
     * @param int $idUser
     * @return Byte []
     */
    public function GetTaskInCompleteByUserProtoc($idUser): void
    {
        $assigments = $this->GetTaskCompletedByUser($idUser, 0);

        $assigmentsListDTO = new AssignmentsDTO();
        $assigmentsList = array();
        $assigmentsList['list'] = $assigments->toArray();
        $assigmentsListDTO->mergeFromJsonString(json_encode($assigmentsList));

        echo $assigmentsListDTO->serializeToString();
    }

    /**
     * Undocumented function
     *
     * @param integer $idUser
     * @param integer $completed
     * @return \Illuminate\Support\Collection
     */
    private function GetTaskCompletedByUser(int $idUser, int $completed = 1) : \Illuminate\Support\Collection
    {
        $assigments = DB::table(Assignment::TableName)
            ->join(Task::TableName, Assignment::TableName . '.' . Assignment::TaskId, '=', Task::TableName . '.' . Assignment::Id)
            ->select(
                Assignment::AliasQueryTable,
                Task::AliasQueryId, Task::AliasQueryCompaniesId,
                Task::AliasQueryDescription, Task::AliasQueryInitialDate,
                Task::AliasQueryFinalDate, Task::AliasQueryComment,
                Task::AliasQueryCreatedAt, Task::AliasQueryUpdateAt
            )
            ->where(Assignment::AliasPrompUsersId, $idUser)
            ->where(Assignment::AliasPrompIsCompleted, $completed)
            
            ->get();

        foreach ($assigments as &$assigment) {
            $assigment->task = array(
                Task::Id => $assigment->{Task::AliasId},
                Task::CompaniesId => $assigment->{Task::AliasCompaniesId},
                Task::Description => $assigment->{Task::AliasDescription},
                Task::InitialDate => $assigment->{Task::AliasInitialDate},
                Task::FinalDate => $assigment->{Task::AliasFinalDate},
                Task::Comment => $assigment->{Task::AliasComment},
                Task::CREATED_AT => $assigment->{Task::AliasCreatedAt},
                Task::UPDATED_AT => $assigment->{Task::AliasUpdateAt}
            );

            unset($assigment->{Task::AliasId});
            unset($assigment->{Task::AliasCompaniesId});
            unset($assigment->{Task::AliasDescription});
            unset($assigment->{Task::AliasInitialDate});
            unset($assigment->{Task::AliasFinalDate});
            unset($assigment->{Task::AliasComment});
            unset($assigment->{Task::AliasCreatedAt});
            unset($assigment->{Task::AliasUpdateAt});
        }

        return $assigments;
    }
}
