<?php

namespace Swopyn\Http\Controllers\Tasks;

use Carbon\Carbon;
use FCM;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Mail;
use Notification;
use Swopyn\AnswerTaskEvaluation;
use Swopyn\Assignment;
use Swopyn\AssignmentEvidence;
use Swopyn\AssignmentEvidencias;
use Swopyn\AssignmentMensajes;
use Swopyn\AssignmentMessages;
use Swopyn\AssignmentUser;
use Swopyn\Http\Controllers\Controller;
use Swopyn\Mail\EmailEvidence;
use Swopyn\Mail\EmailMessage;
use Swopyn\Mail\EmailTask;
use Swopyn\Notifications\EvidencesAlert;
use Swopyn\Notifications\MessageAlert;
use Swopyn\Notifications\TaskAlert;
use Swopyn\OptionTaskEvaluation;
use Swopyn\Task;
use Swopyn\TaskDocument;
use Swopyn\TaskEvaluation;
use Swopyn\TaskResult;
use Swopyn\User;
use Validator;
use Swopyn\Http\Requests\TaskRequest;
use Swopyn\Http\Requests\AssigmentRequest;


class TaskController extends Controller
{

    #region rules
    protected $rules =
        [
        'Question' => 'required',
    ];

    protected $ruleseva =
        [
        'Answer' => 'required',
    ];
    #endregion

#region Funciones JSON

    /**
     * Retorna un JSON con los nombres de usuarios.
     *
     */
    public function GetEmployeesJson()
    {
        $users = DB::table('users')
        ->join('role_user', 'users.id', 'role_user.user_id')
        ->select('id', 'name')->orderBy('name', 'asc')
        ->where('role_id', 2)
        ->where('role_user.status', 1)
        ->get();
        echo $users->toJson();
    }

    /**
     * Retorna un JSON con el ultimo ID de TASK_DOCUMENTS y el ultimo ID de TAREAS.
     *
     */
    public function GetDataDocumentTask()
    {
        $number     = DB::table('task_documents')->select('id')->orderby('created_at', 'DESC')->first()->id;
        $task       = DB::table('tasks')->select('id')->orderby('created_at', 'DESC')->first()->id;
        $array      = ['id_document' => $number + 1, 'task_id' => $task];
        $collection = collect($array);
        echo $collection->toJson();
    }

    /**
     * Retorna un JSON con el ultimo ID de ASSIGNMENT_EVIDENCIAS.
     *
     */
    public function GetDataDocumentEvidence()
    {
        $number     = DB::table('assignment_evidencias')->select('id')->orderby('created_at', 'DESC')->first()->id;
        $array      = ['id_evidence' => $number + 1];
        $collection = collect($array);
        echo $collection->toJson();
    }

    /**
     * Retorna un JSON con todas las evidencias de una asignación por usuario.
     *
     */
    public function GetEvidencesByUser($assigment_id, $task_id, $user_id)
    {
        $evidences = DB::table('assignment_evidencias')->select('name')->where('assignments_id', $assigment_id)->where('task_id', $task_id)->where('user_id', $user_id)->get();
        echo $evidences->toJson();
    }

    /**
     * Guarda un nuevo archivo (Documento Tarea) en la carpeta 'storage/tareas/'.
     */
    public function saveFileTaskDocument()
    {
        header('Access-Control-Allow-Origin:    *');
        $target_path = "../../../storage/app/tareas/";
        $target_path = $target_path . basename($_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo "Upload and move success";
        } else {
            echo $target_path;
            echo "There was an error uploading the file, please try again!";
        }
    }

    /**
     * Descargar el documento de la tarea por su Id.
     * @param Task $task
     */
    public function downloadDocumentTask(Task $task)
    {
        $tasks = Task::find($task->id);
        $docu  = DB::table('task_documents')->select('name')->where('task_id', '=', $task->id)->first();

        return response()->download(storage_path('app/tareas/') . $docu->name, $docu->name);
    }

    /**
     * Retorna un JSON con la tarea por su id.
     *
     * @param integer $idTask
     *
     */
    public function TaskByIdJson(int $idTask)
    {
        $tasks = DB::table('tasks')->where('id', $idTask)->get();
        echo $tasks->toJson();
    }

    /**
     * Retorna un JSON con las tareas de un usuario por su id.
     *
     * @param integer $idUser
     *
     */
    public function CreatedByUserIdJson(int $idUser)
    {
        $tasks = DB::table('tasks')->where('users_id', $idUser)->latest()->get();
        echo $tasks->toJson();
    }

    /**
     * Retorna un JSON con la asignación de una tarea por el Id de asignación.
     * @param integer $idUserAssign, $idUser
     */
    public function GetAssignmentsByAssignIdJson(int $idAssign, $idUser)
    {
        $assigntask = DB::table('assignments')->join('assignment_user', 'assignments.id', '=', 'assignment_user.assignment_id')->join('assignment_task', 'assignments.id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('task_documents', 'task_documents.task_id', '=', 'tasks.id')->select('tasks.id', 'assignment_user.assignment_id', 'assignment_user.user_id', 'tasks.title', 'tasks.objective', 'tasks.description', 'assignments.initial_date', 'assignments.final_date', 'assignments.comment', 'assignment_user.is_completed', 'task_documents.name')->where('assignments.id', '=', $idAssign)->where('assignment_user.user_id', '=', $idUser)->get();

        echo $assigntask->toJson();
    }

    /**
     * Retorna un JSON con el estatus de las tareas creadas por el Id de usuario.
     *
     * @param integer $idUser
     *
     */
    public function GetStatusTaskCreatedByUserJson(int $idUser)
    {

        $tasks      = DB::table('tasks')->where('users_id', $idUser)->latest()->count();
        $completed  = DB::table('tasks')->where('users_id', $idUser)->where('progress', 100)->latest()->count();
        $array      = ['total' => $tasks, 'completed' => $completed];
        $collection = collect($array);
        echo $collection->toJson();
    }

    /**
     * Retorna un JSON con el estatus de las tareas asignadas por el Id de usuario.
     *
     * @param integer $idUser
     *
     */
    public function GetStatusTasksAssignedsByUserJson(int $idUser)
    {
        $assigns    = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_user.user_id', $idUser)->orderby('assignments.created_at', 'DESC')->count();
        $completed  = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_user.user_id', $idUser)->orderby('assignments.created_at', 'DESC')->where('assignment_user.is_completed', 100)->count();
        $array      = ['total' => $assigns, 'completed' => $completed];
        $collection = collect($array);
        echo $collection->toJson();
    }

    /**
     * Retorna un JSON con el estatus de asignaciones de una tarea.
     * @param integer $idTask
     */
    public function GetStatusTaskByIdTaskJson(int $idTask)
    {
        $assigns = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_task.task_id', '=', $idTask)->count();

        $assignsCompleted = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_task.task_id', '=', $idTask)->where('assignment_user.is_completed', 100)->count();

        $progressTask = DB::table('tasks')->select('progress')->where('id', $idTask)->first()->progress;
        if ($assigns == 0) {
            $progressTotalByTask = 0;
        }
        else{
            $progressTotalByTask = (100 / $assigns) * $assignsCompleted;
        }
        $array = ['progressTotal' => $progressTotalByTask, 'total' => $assigns, 'completeds' => $assignsCompleted, 'progressTask' => $progressTask];
        $collection = collect($array);
        echo $collection->toJson();
    }

    /**
     * Retorna un JSON con el progreso de una tarea por su Id.
     * @param integer $idTask
     */
    public function GetProgressByIdTask(int $idTask)
    {
        $progressTask = DB::table('tasks')->select('progress')->where('id', $idTask)->first()->progress;
        $array        = ['progress' => $progressTask];
        $collection   = collect($array);
        echo $collection->toJson();
    }

    /**
     * Guarda una nueva tarea.
     *
     */
    public function AddNewTaskJson()
    {
        $objString = file_get_contents('php://input');

        if (isset($objString)) {
            $request             = json_decode($objString);
            $tarea               = new Task;
            $tarea->companies_id = $request->companies_id;
            $tarea->users_id     = $request->user_id;
            $tarea->title        = $request->title;
            $tarea->objective    = $request->objective;
            $tarea->description  = $request->description;
            $tarea->initial_date = $request->initial_date;
            $tarea->final_date   = $request->final_date;
            $tarea->comment      = $request->comments;
            $tarea->save();
        }
    }

    /**
     * Guarda una nueva asignación.
     *
     */
    public function AddNewAssignmentTaskJson()
    {
        $objString = file_get_contents('php://input');

        if (isset($objString)) {
            $request                  = json_decode($objString);
            $assignment               = new Assignment;
            $assignment->score        = 0;
            $assignment->comment      = $request->comments;
            $assignment->is_completed = 0;
            $assignment->initial_date = $request->initial_date;
            $assignment->final_date   = $request->final_date;
            $assignment->save();

            $assignment->task()->sync($request->task);
            $assignment->user()->sync($request->student);

            //START Notification Web
            $taskTitle = DB::table('tasks')->select('title')->where('id', $request->idTask)->first();
            $user      = DB::table('users')->where('id', $request->idUser)->first();
            $assign    = DB::table('assignments')->select('final_date')->where('id', $assignment->id)->first();
            Notification::send(User::findorFail($request->userAssign), new \Swopyn\Notifications\TaskAlert($taskTitle, $user, $assign));
            //END Notification Web

            //START Notification Movil
            $userName      = $user->name;
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);
            $notificationBuilder = new PayloadNotificationBuilder('Tarea Asignada');
            $notificationBuilder->setBody($userName . ' te asigno: ' . $taskTitle->title)->setSound('default');
            $option       = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $token        = DB::table('users')->select('notification_token')->where('id', $request->userAssign)->first();
            $tokenstring  = $token->notification_token;
            if ($tokenstring != null) {
                $downstreamResponse = FCM::sendTo($tokenstring, $option, $notification);
            }
            //END Notification Movil
        }
    }

    /**
     * Guarda un nuevo documento de tarea.
     *
     */
    public function AddNewDocumentTaskJson()
    {
        $objString = file_get_contents('php://input');

        if (isset($objString)) {
            $request               = json_decode($objString);
            $taskDocument          = new TaskDocument;
            $taskDocument->name    = $request->name;
            $taskDocument->task_id = $request->task_id;
            $taskDocument->save();
        }
    }

    /**
     * Guarda una nueva evidencia de tarea en la bd.
     *
     */
    public function AddNewEvidenceTaskJson()
    {
        $objString = file_get_contents('php://input');

        if (isset($objString)) {
            $request                  = json_decode($objString);
            $evidence                 = new AssignmentEvidencias;
            $evidence->assignments_id = $request->assignments_id;
            $evidence->task_id        = $request->task_id;
            $evidence->user_id        = $request->user_id;
            $evidence->name           = $request->name;
            $evidence->save();
        }
    }

    /**
     * Retorna un JSON con las asignaciones de una tarea por su Id.
     *
     * @param integer $idTask
     *
     */
    public function GetAssignmentsByTaskIdJson(int $idTask)
    {
        $assigns = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_task.task_id', '=', $idTask)->get();

        echo $assigns->toJson();
    }

    /**
     * Retorna un JSON con todas las asignaciones que ha creado un usuario.
     */
    /**
     * Retorna un JSON con las asignaciones de una tarea por su Id.
     *
     * @param integer $idTask
     *
     */
    public function GetAssignmentsByUserIdJson(int $idUser)
    {
        $assigns = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('tasks.users_id', '=', $idUser)->get();

        $userByAssigments = $assigns->groupBy('user_id');

        echo $userByAssigments->toJson();
    }

    /**
     * Descarga una evidencia por el nombre.
     */
    public function downloadEvidenceByUserJson($name)
    {

        return response()->download(storage_path('app/evidencias/') . $name);
    }

    /**
     * Elimina una evidencia por el nombre.
     */
    public function deleteEvidenceByNameJson($name)
    {
        Storage::delete('evidencias/' . $name);
        $evi_delete = DB::table('assignment_evidencias')->where('name', $name)->delete();
    }

    /**
     * Finaliza una tarea por usuario.
     */
    public function SaveCompletedTask()
    {
        $objString = file_get_contents('php://input');

        if (isset($objString)) {
            $request                       = json_decode($objString);
            $id                            = DB::table('assignment_user')->select('id')->where('assignment_id', $request->assignment_id)->where('user_id', $request->user_id)->first()->id;
            $assignmentModel               = AssignmentUser::find($id);
            $assignmentModel->is_completed = 100;
            $assignmentModel->save();
        }
    }

    /**
     * Actualiza el progreso de una tarea por su Id.
     */
    public function UpdateProgressTask()
    {
        $objString = file_get_contents('php://input');

        if (isset($objString)) {
            $request             = json_decode($objString);
            $taskModel           = Task::find($request->id);
            $taskModel->progress = $request->progress;
            $taskModel->save();
        }
    }

    /**
     * Guarda un nuevo mensaje de una tarea por su id.
     */
    public function SaveMessageTaskByUser()
    {
        $objString = file_get_contents('php://input');

        if (isset($objString)) {
            $request                 = json_decode($objString);
            $message                 = new AssignmentMensajes;
            $message->id_assignments = $request->id_assignments;
            $message->id_task        = $request->id_task;
            $message->id_user        = $request->id_user;
            $message->message        = $request->message;
            $message->save();
        }
    }

    /**
     * Obtiene la conversación de mensajes de una tarea.
     */
    public function getMessagesTaskByIdJson($id_assignments, $id_task)
    {

        $mens = DB::table('assignment_mensajes')->join('users', 'users.id', '=', 'assignment_mensajes.id_user')->select('assignment_mensajes.message', 'assignment_mensajes.created_at', 'assignment_mensajes.id_user', 'users.name')->where('id_assignments', $id_assignments)->where('id_task', $id_task)->oldest()->get();

        echo $mens->toJson();
    }

    /**
     * Retorna un JSON con todas las asignaciones de un usuario.
     */
    public function getAssignmentsFromUserJson(int $idUser)
    {
        $assign = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_user.user_id', $idUser)->orderby('assignments.created_at', 'DESC')->get();

        echo $assign->toJson();
    }

    /**
     * Guarda el token FCM del dispositivo usuario.
     */
    public function SaveTokenUserJson()
    {
        $objString = file_get_contents('php://input');
        if (isset($objString)) {
            $request                       = json_decode($objString);
            $userModel                     = User::find($request->id_user);
            $userModel->notification_token = $request->notification_token;
            $userModel->save();
        }
    }

    /**
     * Recive una notificación de asignación de tarea desde la APP.
     *
     * @param integer $id
     */
    public function notificationReciveJson($user, $task, $userAssign)
    {
        //START Notification Web
        $taskTitle = DB::table('tasks')->select('title')->where('id', $task)->first();
        $user      = DB::table('users')->where('id', $user)->first();
        $assign    = DB::table('assignments')->select('final_date')->where('id', $idAssign)->first();
        Notification::send(User::findorFail($userAssign), new \Swopyn\Notifications\TaskAlert($taskTitle, $user, $assign));
        //END Notification Web

        //START Notification Movil
        $userName      = $user->name;
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder('Tarea Asignada');
        $notificationBuilder->setBody($userName . ' te asigno: ' . $taskTitle->title)->setSound('default');
        $option       = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $token        = DB::table('users')->select('notification_token')->where('id', $userAssign)->first();
        $tokenstring  = $token->notification_token;
        if ($tokenstring != null) {
            $downstreamResponse = FCM::sendTo($tokenstring, $option, $notification);
        }
        //END Notification Movil
    }

#endregion

#region Métodos Comunes

    /**
     * Obtiene las asignasiones creadas por un usuario de otro en especifico
     *
     * @param integer $idUserCreate
     * @param integer $idUserAssigned
     * @return Collection
     *
     * TODO:Cambiar el parametro de $idUserCreate por un la autentificación de Auth().
     */
    private function GetTaskAssignedUsers(int $idUserCreate, int $idUserAssigned): Collection
    {
        $assigments = DB::table(Assignment::TableName)
            ->join(Task::TableName, Task::AliasPrompId, '=', Assignment::AliasPrompTaskId)
            ->select(
                Assignment::AliasQueryTable,
                Task::AliasQueryId,
                Task::AliasQueryCompaniesId,
                Task::AliasQueryDescription,
                Task::AliasQueryInitialDate,
                Task::AliasQueryFinalDate,
                Task::AliasQueryComment,
                Task::AliasQueryCreatedAt,
                Task::AliasQueryUpdateAt,
                Task::AliasQueryTitle,
                Task::AliasQueryObjectives
            )
            ->where(Assignment::AliasPrompUsersId, $idUserAssigned)
            ->where(Task::AliasPrompUsersId, $idUserCreate)
            ->get();

        foreach ($assigments as &$assigment) {
            $assigment->task = array(
                Task::Id          => $assigment->{Task::AliasId},
                Task::CompaniesId => $assigment->{Task::AliasCompaniesId},
                Task::Description => $assigment->{Task::AliasDescription},
                Task::InitialDate => $assigment->{Task::AliasInitialDate},
                Task::FinalDate   => $assigment->{Task::AliasFinalDate},
                Task::Comment     => $assigment->{Task::AliasComment},
                Task::CREATED_AT  => $assigment->{Task::AliasCreatedAt},
                Task::UPDATED_AT  => $assigment->{Task::AliasUpdateAt},
                Task::Title       => $assigment->{Task::AliasTitle},
                Task::Objectives  => $assigment->{Task::AliasObjectives},
            );

            unset($assigment->{Task::AliasId});
            unset($assigment->{Task::AliasCompaniesId});
            unset($assigment->{Task::AliasDescription});
            unset($assigment->{Task::AliasInitialDate});
            unset($assigment->{Task::AliasFinalDate});
            unset($assigment->{Task::AliasComment});
            unset($assigment->{Task::AliasCreatedAt});
            unset($assigment->{Task::AliasUpdateAt});
            unset($assigment->{Task::AliasTitle});
            unset($assigment->{Task::AliasObjectives});
        }

        return $assigments;
    }

    /**
     * Obtiene el status de los usuarios a los cuales le asignaste tareas.
     *
     * @param integer $idUser
     * @return Collection
     *
     * TODO:Cambiar el parametro de $idUser por un la autentificación de Auth().
     */
    private function GetUsersAssinedStatus(int $idUser): Collection
    {
        $assigments = DB::table(Assignment::TableName)
            ->join(Task::TableName, Task::AliasPrompId, '=', Assignment::AliasPrompTaskId)
            ->select(Assignment::AliasQueryTable)
            ->where(Task::AliasPrompUsersId, $idUser)
            ->get();

        $userByAssigments = $assigments->groupBy(Assignment::UsersId);
        $users            = collect();

        foreach ($userByAssigments as $id => $userByAssigment) {
            $user                        = \Swopyn\User::find($id);
            $user->tasks_total_assigneds = $userByAssigment->count();

            $grouped = $userByAssigment->groupBy(Assignment::IsCompleted);

            $taskCompleted = $grouped->get(1);

            $user->tasks_total_completed = $taskCompleted ? count($taskCompleted) : 0;
            //$user->task_assigment = $userByAssigment;
            $users->push($user);
        }

        return $users;
    }

    /**
     * Obtiene el estatus de las tareas de creadas por el usuasio.
     *
     * @param integer $idUser
     * @return Collection
     *
     * TODO:Cambiar el parametro de $idUser por un la autentificación de Auth().
     */
    private function GetTaskAssignedStatus(int $idUser): Collection
    {
        $tasks = DB::table(Task::TableName)->select(Task::AliasQueryTable)->where(Task::UsersId, $idUser)->get();

        foreach ($tasks as &$task) {
            $assigments            = DB::table(Assignment::TableName)->where(Assignment::TaskId, $task->id)->get();
            $grouped               = $assigments->groupBy(Assignment::IsCompleted);
            $task->total_assigneds = $assigments->count();

            $taskCompleted = $grouped->get(1);

            $task->total_completed = $taskCompleted ? count($taskCompleted) : 0;
        }

        return $tasks;
    }

    /**
     * Retorna las asignaciones de una tarea.
     *
     * @param integer $idTask
     * @return void
     */
    private function GetTasksAssigned(int $idTask): Collection
    {
        /*Pruebas
        $assigmentTask = DB::table(AssignmentTask::TableName)
        ->select(AssignmentTask::AssignmentId)
        ->where(AssignmentTask::TaskId, $idTask)->first();

        $idAss = $assigmentTask->assignment_id;

        $assigmentUser = DB::table(AssignmentUser::TableName)
        ->select(AssignmentUser::UserId)
        ->where(AssignmentUser::AssignmentId, $idAss)->get();

        $userAss = $assigmentUser[0]->user_id;

        $assigments = DB::table(Assignment::TableName)
        ->where(Assignment::Id, $idAss)
        ->get();

        $assigments[0]->tasks_id = $idTask;
        $assigments[0]->users_id = $userAss;

        $users = collect();
        foreach ($assigments as &$assigment) {
        $user                        = \Swopyn\User::find($userAss);
        $user->tasks_total_assigneds = 1;
        $user->tasks_total_completed = $assigment->is_completed;

        $assigment->user = $user->toArray();
        }

        return $assigments;
        ENDPRUEBAS*/
        $assigments = DB::table(Assignment::TableName)
            ->where(Assignment::TaskId, $idTask)
            ->get();

        $users = collect();
        foreach ($assigments as &$assigment) {
            $user                        = \Swopyn\User::find($assigment->users_id);
            $user->tasks_total_assigneds = 1;
            $user->tasks_total_completed = $assigment->is_completed;

            $assigment->user = $user->toArray();
        }

        return $assigments;
    }

    /**
     * Retorna la asignacion completa por su id.
     *
     * @param integer $id
     * @return Assignment
     */
    private function GetAssigmentById(int $id): Assignment
    {
        $assigment          = Assignment::find($id);
        $task               = Task::find($assigment->tasks_id);
        $assignmentEvidence = AssignmentEvidence::where(AssignmentEvidence::AssignmentsId, $assigment->id)->get();

        $assigment->task      = $task->toArray();
        $assigment->evidences = $assignmentEvidence->toArray();

        $messages                = AssignmentMessages::where(AssignmentMessages::IdAssignments, $id)->get();
        $assigment->conversation = $messages->toArray();

        return $assigment;
    }

    /**
     * Retorna las asignaciones de las tareas que se le hacen a un usuario.
     *
     * @param integer $idUser
     * @return void
     *
     * TODO:Cambiar el parametro de $idUser por la autentificación de Auth().
     */
    private function GetTaskAssignedToUser(int $idUser): Collection
    {
        $assigments = DB::table(Assignment::TableName)
            ->where(Assignment::AliasPrompUsersId, $idUser)
            ->get();

        foreach ($assigments as &$assigment) {
            $task            = Task::find($assigment->tasks_id);
            $assigment->task = $task->toArray();
        }

        return $assigments;
    }

    /**
     * Retorna estado de las tareas de manera global.
     *
     * @param integer $idUser
     * @return Collection
     */
    private function GetTasksStatus(int $idUser): Collection
    {
        $taskStatus = collect();

        $tasksCreatedAssigned  = 0;
        $tasksCreatedCompleted = 0;

        $tasksCreatedStatus = $this->GetTaskAssignedStatus($idUser);

        foreach ($tasksCreatedStatus as $task) {
            $tasksCreatedAssigned += $task->total_assigneds;
            $tasksCreatedCompleted += $task->total_completed;
        }

        $taskStatus->put('tasks_created_assigned', $tasksCreatedAssigned);
        $taskStatus->put('tasks_created_completed', $tasksCreatedCompleted);

        $tasksAssignedTotal          = 0;
        $tasksAssignedCompletedTotal = 0;

        $tasksAssignedStatus = $this->GetTaskAssignedToUser($idUser);

        foreach ($tasksAssignedStatus as $task) {
            $tasksAssignedTotal += 1;
            $tasksAssignedCompletedTotal += $task->is_completed;
        }

        $taskStatus->put('tasks_assigned_total', $tasksAssignedTotal);
        $taskStatus->put('tasks_assigned_completed_total', $tasksAssignedCompletedTotal);

        return $taskStatus;
    }

#endregion
    public function returntask()
    {
        return view('adminlte::tasks.index');

    }
#region return view

    public function createtask()
    {
        $task = Task::all();
        $user = User::all();
        return view('adminlte::tasks.createtask')->with(['tasks' => $task, 'users' => $user]);

    }

    public function createassign()
    {
        $task = Task::all();
        $user = User::all();
        return view('adminlte::tasks.createassign')->with(['tasks' => $task, 'users' => $user]);

    }
#end region

#region CRUD
    public function addassign(AssigmentRequest $request)
    {
        $asignacion               = new Assignment;
        $asignacion->initial_date = $request->Fecha_inicial;
        $asignacion->final_date   = $request->Fecha_final;
        $asignacion->score        = 0.00;
        $asignacion->comment      = $request->Comentarios;
        $asignacion->is_completed = 0;
        $asignacion->admin_task   = auth()->user()->id;
        $asignacion->hour         = $request->Hora;

        if ($request->Rep_monday == 1) {
            $asignacion->rep_monday = $request->Rep_monday;
        } else { $asignacion->rep_monday = 0;}

        if ($request->Rep_tuesday == 1) {
            $asignacion->rep_tuesday = $request->Rep_tuesday;
        } else { $asignacion->rep_tuesday = 0;}

        if ($request->Rep_wednesday == 1) {
            $asignacion->rep_wednesday = $request->Rep_wednesday;
        } else { $asignacion->rep_wednesday = 0;}

        if ($request->Rep_thursday == 1) {
            $asignacion->rep_thursday = $request->Rep_thursday;
        } else { $asignacion->rep_thursday = 0;}

        if ($request->Rep_friday == 1) {
            $asignacion->rep_friday = $request->Rep_friday;
        } else { $asignacion->rep_friday = 0;}

        if ($request->Rep_saturday == 1) {
            $asignacion->rep_saturday = $request->Rep_saturday;
        } else { $asignacion->rep_saturday = 0;}

        if ($request->Rep_sunday == 1) {
            $asignacion->rep_sunday = $request->Rep_sunday;
        } else { $asignacion->rep_sunday = 0;}

        $asignacion->save();

        $asignacion->task()->sync($request->task);
        $asignacion->user()->sync($request->user);

        $user     = DB::table('users')->select('email', 'id', 'name')->where('id', $request->user)->first();
        $task     = DB::table('tasks')->select('title')->where('id', $request->task)->first();
        $userNull = null;
        $assign   = DB::table('assignments')->select('final_date')->where('assignments.id', $asignacion->id)->first();
        $email    = new EmailTask($user);
        Mail::to($user)->send($email);
        Notification::send(User::findorFail($request->user), new \Swopyn\Notifications\TaskAlert($task, $userNull, $assign));

        //Enviar la notificacion al movil
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder('Tarea Asignada');
        $notificationBuilder->setBody(auth()->user()->name . ' te asigno: ' . $task->title)->setSound('default');
        $option       = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $token        = DB::table('users')->select('notification_token')->where('id', $request->user)->first();
        $tokenstring  = $token->notification_token;
        if ($tokenstring != null) {
            $downstreamResponse = FCM::sendTo($tokenstring, $option, $notification);
        }
        return redirect()->route('tasks_admin');
    }

    public function deleteNot($id)
    {
        $user         = \Auth::user();
        $notification = $user->notifications()->where('id', $id)->first();
        if ($notification->type == 'Swopyn\Notifications\TaskAlert') {
            if (Carbon::now() >= $notification->data['assing']['final_date']) {
                $notification->markAsRead();
                return redirect()->route('tasks');
            } else {
                return redirect()->route('tasks');
            }
        }
        if ($notification) {
            $notification->markAsRead();
            return redirect()->route('tasks');
        }
    }

    /**
     * TODO: Error Request
     */
    public function add(TaskRequest $request) 
    {
        $tarea               = new Task;
        $tarea->companies_id = DB::table('employees')
            ->select('employees.id_company')
            ->where('employees.employee_id', Auth::user()->id)
            ->first()
            ->id_company;
        $tarea->users_id     = auth()->user()->id;
        $tarea->title        = $request->Nombre;
        $tarea->objective    = $request->Objetivo;
        $tarea->description  = $request->Descripcion;
        $tarea->initial_date = $request->Fecha_inicial;
        $tarea->final_date   = $request->Fecha_final;
        $tarea->comment      = $request->Comentarios;
        $tarea->save();

        $document = new TaskDocument;
        $number   = DB::table('task_documents')
            ->select('id')
            ->orderby('created_at', 'DESC')
            ->first()
            ->id;
        $document->task_id = DB::table('tasks')
            ->select('id')
            ->orderby('created_at', 'DESC')
            ->first()
            ->id;

        $ext = pathinfo($_FILES['ArchivosT']['name'], PATHINFO_EXTENSION);
        $document->name    = 'Tarea' . $number . '.' . $ext;

        Storage::putFileAs('tareas', $request->file('ArchivosT'), 'Tarea' . $number . '.' . $ext);
        $document->save();

        //$task = DB::table('tasks')->select('id')->orderby('created_at', 'DESC')->first();

        /**
         * Consultas para redireccionar a vista "myTasks".
         */

        $assign = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_user.user_id', '=', auth()->user()->id)->paginate(10);

        $tasks = DB::table('tasks')->join('users', 'tasks.users_id', '=', 'users.id')->select('tasks.title', 'tasks.initial_date', 'tasks.final_date', 'tasks.objective', 'tasks.description', 'tasks.users_id', 'tasks.progress')->where('tasks.users_id',auth()->user()->id)->paginate(10);

        return view('adminlte::tasks.myTasks')->with(['tasks' => $tasks, 'assign' => $assign]);
    }
#end region

    public function myTasks()
    {

        $fecha = Carbon::parse('now')->format('y/m/d');
        $adeudo = DB::table('assingment_pays as ap')->join('pays','ap.pays_id','pays.id')->select('ap.users_id as user','pays.limit_date as final', 'ap.is_completed')->where('ap.users_id',auth()->user()->id)->where('ap.is_completed',0)->where('pays.limit_date',$fecha)->first();
        if($adeudo == true){
            return view('adminlte::auth.login');
        }else{
        $assign = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_user.user_id', '=', auth()->user()->id)->paginate(10);

        $tasks = DB::table('tasks')->join('users', 'tasks.users_id', '=', 'users.id')->select('tasks.title', 'tasks.initial_date', 'tasks.final_date', 'tasks.objective', 'tasks.description', 'tasks.users_id', 'tasks.progress')->where('tasks.users_id',auth()->user()->id)->paginate(10);

        return view('adminlte::tasks.myTasks')->with(['tasks' => $tasks, 'assign' => $assign]);
        }
    }

//START Assign-Task View
    public function assignview(Assignment $assign, Task $tasks, $users)
    {
        $assign = Assignment::find($assign->id);
        $task = Task::find($tasks->id);
        $user = User::all();

        $usersassign = $assign->user;
        $taskassign = $assign->task;

        $assigntask = DB::table('assignments')->join('assignment_task', 'assignments.id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('task_documents', 'task_documents.task_id', '=', 'tasks.id')->select('tasks.objective', 'tasks.description', 'assignments.id')->where('assignments.id', '=', $assign->id)->get();

        $evi = DB::table('assignment_evidencias')->join('assignments', 'assignments.id', '=', 'assignment_evidencias.assignments_id')->join('tasks', 'tasks.id', '=', 'assignment_evidencias.task_id')->select('assignment_evidencias.name', 'assignment_evidencias.id', 'assignments.id as assid', 'tasks.id as taskid')->where('assignment_evidencias.task_id', '=', $tasks->id)->where('assignment_evidencias.assignments_id', '=', $assign->id)->where('assignment_evidencias.user_id', '=', $users)->get();


        $mens = DB::table('assignment_mensajes')->join('users', 'users.id', '=', 'assignment_mensajes.id_user')->select('assignment_mensajes.id_assignments', 'assignment_mensajes.id_task', 'assignment_mensajes.id_user', 'assignment_mensajes.message', 'users.name')->where('id_assignments', '=', $assign->id)->where('id_task', '=', $tasks->id)->get();
        

        $docu = DB::table('task_documents')->select('name')->where('task_id', '=', $task->id)->first();

        $fini = DB::table('assignment_user')->select('is_completed')->where('assignment_id', '=', $assign->id)->where('user_id', '=', $users)->first();

        return view('adminlte::tasks.assignView')->with(['assign'=>$assign, 'users'=>$user, 'task'=>$task, 'usersassign'=>$usersassign, 'docu'=>$docu, 'mens'=>$mens, 'evi'=>$evi, 'usuario'=>$users, 'fini'=>$fini]);
    }

    public function assignviewAdmin(Assignment $assign, Task $tasks, $users){
        $assign = Assignment::find($assign->id);
        $task = Task::find($tasks->id);
        $user = User::all();

        $usersassign = $assign->user;
        $taskassign = $assign->task;

        $assigntask = DB::table('assignments')->join('assignment_task', 'assignments.id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('task_documents', 'task_documents.task_id', '=', 'tasks.id')->select('tasks.objective', 'tasks.description', 'assignments.id')->where('assignments.id', '=', $assign->id)->get();

        $evi = DB::table('assignment_evidencias')->join('assignments', 'assignments.id', '=', 'assignment_evidencias.assignments_id')->join('tasks', 'tasks.id', '=', 'assignment_evidencias.task_id')->select('assignment_evidencias.name', 'assignment_evidencias.id', 'assignments.id as assid', 'tasks.id as taskid')->where('assignment_evidencias.task_id', '=', $tasks->id)->where('assignment_evidencias.assignments_id', '=', $assign->id)->where('assignment_evidencias.user_id', '=', $users)->get();


        $mens = DB::table('assignment_mensajes')->join('users', 'users.id', '=', 'assignment_mensajes.id_user')->select('assignment_mensajes.id_assignments', 'assignment_mensajes.id_task', 'assignment_mensajes.id_user', 'assignment_mensajes.message', 'users.name')->where('id_assignments', '=', $assign->id)->where('id_task', '=', $tasks->id)->get();
        

        $docu = DB::table('task_documents')->select('name')->where('task_id', '=', $task->id)->first();

        $fini = DB::table('assignment_user')->select('is_completed')->where('assignment_id', '=', $assign->id)->where('user_id', '=', $users)->first();

        return view('adminlte::tasks.assignViewAdmin')->with(['assign'=>$assign, 'users'=>$user, 'task'=>$task, 'usersassign'=>$usersassign, 'docu'=>$docu, 'mens'=>$mens, 'evi'=>$evi, 'usuario'=>$users, 'fini'=>$fini]);//agregué users.
    }

    public function finishTask($assign, $tasks, $users){
        $id = DB::table('assignment_user')->select('id')->where('assignment_id', '=', $assign)->where('user_id', '=', $users)->first()->id;
        $assignsfini = AssignmentUser::find($id);
        $assignsfini->is_completed = 100;
        $assignsfini->save();

        $assigns = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_task.task_id', '=', $tasks)->count();

        $assignsCompleted = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id as taskid')->where('assignment_task.task_id', '=', $tasks)->where('assignment_user.is_completed', 100)->count();


        $progressTask = DB::table('tasks')->select('id')->where('id', $tasks)->first()->id;
        $progressTotalByTask = Task::find($progressTask);
        $progressTotalByTask->progress = (100 / $assigns) * $assignsCompleted;
        $progressTotalByTask->save();

        return redirect()->route('assign_view', ['assign'=> $assign, 'tasks'=> $tasks, 'users'=> $users]);
    }

    public function downloadDoc(Task $task)
    {
        $tasks = Task::find($task->id);
        $docu  = DB::table('task_documents')->select('name')->where('task_id', '=', $task->id)->first();

        return response()->download(storage_path('app/tareas/') . $docu->name, $tasks->title);
    }

    public function addEvidence($assign, $tasks, $users, Request $request){
        $assigns = Assignment::find($assign);
        $task = Task::find($tasks);
        $user = User::all();

        $evidencia = new AssignmentEvidencias;
        $evidencia->assignments_id = $assigns->id;
        $evidencia->task_id = $task->id;
        $evidencia->user_id = auth()->user()->id;/*Evidencia individual*/

        $numero   = DB::table('assignment_evidencias') 
            ->select('id') 
            ->orderby('created_at', 'DESC') 
            ->first() 
            ->id;

        $ext=pathinfo($_FILES['ArchivosE']['name'], PATHINFO_EXTENSION);
        
        $evidencia->name    = 'Evidencia' . $numero . '.' . $ext;
        Storage::putFileAs('evidencias', $request->file('ArchivosE'), 'Evidencia' . $numero . '.' . $ext);

        $evidencia->save();

        //Notificaciones
        $task2 = DB::table('assignment_evidencias')->join('assignments', 'assignments.id', '=', 'assignment_evidencias.assignments_id')->join('tasks', 'tasks.id', '=', 'assignment_evidencias.task_id')->select('tasks.title')->first();

        $user_mail = DB::table('users')->select('id','email','name')->where('id', $assigns->admin_task)->first();
        $email = new EmailEvidence($user_mail);
        Mail::to($user_mail)->send($email);
        Notification::send(User::findorFail($assigns->admin_task), new \Swopyn\Notifications\EvidencesAlert($task2));

          //Enviar la notificacion al movil
        $optionBuilder  =  new  OptionsBuilder(); 
        $optionBuilder->setTimeToLive( 60 * 20 );
        $notificationBuilder = new PayloadNotificationBuilder('Evidencia Entregada');
        $notificationBuilder->setBody(auth()->user()->name, ' agrego evidencia en: ', $task2->title)->setSound('default');
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $token = DB::table('users')->select('notification_token')->where('id',$assigns->admin_task)->first();
        $tokenstring = $token->notification_token;
        if($tokenstring != null){
            $downstreamResponse = FCM::sendTo($tokenstring, $option, $notification);
        }

       return redirect()->route('assign_view', ['assign'=> $assign, 'tasks'=> $tasks, 'users'=> $users]);
    }

    public function addEvidenceAdmin($assign, $tasks, $users, Request $request){
        $assigns = Assignment::find($assign);
        $task = Task::find($tasks);
        $user = User::all();

        $evidencia = new AssignmentEvidencias;
        $evidencia->assignments_id = $assigns->id;
        $evidencia->task_id = $task->id;
        $evidencia->user_id = auth()->user()->id;/*Evidencia individual*/

        $numero   = DB::table('assignment_evidencias') 
            ->select('id') 
            ->orderby('created_at', 'DESC') 
            ->first() 
            ->id;

        $ext=pathinfo($_FILES['ArchivosE']['name'], PATHINFO_EXTENSION);
        
        $evidencia->name    = 'Evidencia' . $numero . '.' . $ext;
        Storage::putFileAs('evidencias', $request->file('ArchivosE'), 'Evidencia' . $numero . '.' . $ext);

        $evidencia->save();

        //Notificaciones
        $task2 = DB::table('assignment_evidencias')->join('assignments', 'assignments.id', '=', 'assignment_evidencias.assignments_id')->join('tasks', 'tasks.id', '=', 'assignment_evidencias.task_id')->select('tasks.title')->first();

        $user_mail = DB::table('users')->select('id','email','name')->where('id', $assigns->admin_task)->first();
        $email = new EmailEvidence($user_mail);
        Mail::to($user_mail)->send($email);
        Notification::send(User::findorFail($assigns->admin_task), new \Swopyn\Notifications\EvidencesAlert($task2));

          //Enviar la notificacion al movil
        $optionBuilder  =  new  OptionsBuilder(); 
        $optionBuilder->setTimeToLive( 60 * 20 );
        $notificationBuilder = new PayloadNotificationBuilder('Evidencia Entregada');
        $notificationBuilder->setBody(auth()->user()->name, ' agrego evidencia en: ', $task2->title)->setSound('default');
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $token = DB::table('users')->select('notification_token')->where('id',$assigns->admin_task)->first();
        $tokenstring = $token->notification_token;
        if($tokenstring != null){
            $downstreamResponse = FCM::sendTo($tokenstring, $option, $notification);
        }

       return redirect()->route('assign_view_admin', ['assign'=> $assign, 'tasks'=> $tasks, 'users'=> $users]);
    }

    public function downloadEvidence(AssignmentEvidencias $evi)
    {

        $docu = DB::table('assignment_evidencias')->select('name')->where('id', '=', $evi->id)->first();

        return response()->download(storage_path('app/evidencias/') . $docu->name);
    }

    public function deleteEvidence($assign, $tasks, $users, AssignmentEvidencias $evi){
        $assigns = Assignment::find($assign);
        $task = Task::find($tasks);
        $user = User::all();

        $evidencias = DB::table('assignment_evidencias')->select('name')->where('id', '=', $evi->id)->first();
        Storage::delete('evidencias/'.$evidencias->name);
        $evi_delete = DB::table('assignment_evidencias')->where('id', '=', $evi->id)->delete();

        return redirect()->route('assign_view', ['assign'=> $assign, 'tasks'=> $tasks, 'users'=> $users]);
    }

    public function deleteEvidenceAdmin($assign, $tasks, $users, AssignmentEvidencias $evi){
        $assigns = Assignment::find($assign);
        $task = Task::find($tasks);
        $user = User::all();

        $evidencias = DB::table('assignment_evidencias')->select('name')->where('id', '=', $evi->id)->first();
        Storage::delete('evidencias/'.$evidencias->name);
        $evi_delete = DB::table('assignment_evidencias')->where('id', '=', $evi->id)->delete();


        return redirect()->route('assign_view_admin', ['assign'=> $assign, 'tasks'=> $tasks, 'users'=> $users]);
    }

   public function addMessage($assign, $tasks, $users, Request $request)
   {
        $assigns = Assignment::find($assign);
        $task = Task::find($tasks);
        $user = User::all();

        $mensaje                 = new AssignmentMensajes;
        $mensaje->id_assignments = $assigns->id;
        $mensaje->id_task        = $task->id;
        $mensaje->id_user        = auth()->user()->id;
        $mensaje->message        = $request->mensaje;
        $mensaje->check          = 0;

        $mensaje->save();

        //Notificaciones
        $task2     = DB::table('assignment_mensajes')->join('users', 'users.id', '=', 'assignment_mensajes.id_user')->join('tasks', 'tasks.id', '=', 'assignment_mensajes.id_task')->select('tasks.title')->first();
        $user_mail = DB::table('users')->select('id', 'email', 'name')->where('id', $assigns->admin_task)->first();
        $email     = new EmailMessage($user_mail);
        Mail::to($user_mail)->send($email);
        Notification::send(User::findorFail($assigns->admin_task), new \Swopyn\Notifications\MessageAlert($task2));
        //Enviar la notificacion al movil
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder('Nuevo Mensaje');
        $notificationBuilder->setBody(auth()->user()->name . ' envio un mensaje en: ' . $task2->title)->setSound('default');
        $option       = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $token        = DB::table('users')->select('notification_token')->where('id', $assigns->admin_task)->first();
        $tokenstring  = $token->notification_token;
        if ($tokenstring != null) {
            $downstreamResponse = FCM::sendTo($tokenstring, $option, $notification);
        }

        return redirect()->route('assign_view', ['assign'=> $assign, 'tasks'=> $tasks, 'users'=> $users]);
    }

    public function addMessageAdmin($assign, $tasks, $users, Request $request){
        $assigns = Assignment::find($assign);
        $task = Task::find($tasks);
        $user = User::all();

        $mensaje = new AssignmentMensajes;
        $mensaje->id_assignments = $assigns->id;
        $mensaje->id_task = $task->id;
        $mensaje->id_user = auth()->user()->id;
        $mensaje->message = $request->mensaje;
        $mensaje->check = 0;

        $mensaje->save();

        //Notificaciones
        $task2 = DB::table('assignment_mensajes')->join('users', 'users.id', '=', 'assignment_mensajes.id_user')->join('tasks', 'tasks.id', '=', 'assignment_mensajes.id_task')->select('tasks.title')->first();
        $user_mail = DB::table('users')->select('id','email','name')->where('id', $assigns->admin_task)->first();
        $email = new EmailMessage($user_mail);
        Mail::to($user_mail)->send($email);
        Notification::send(User::findorFail($assigns->admin_task), new \Swopyn\Notifications\MessageAlert($task2));
          //Enviar la notificacion al movil
        $optionBuilder  =  new  OptionsBuilder(); 
        $optionBuilder->setTimeToLive( 60 * 20 );
        $notificationBuilder = new PayloadNotificationBuilder('Evidencia Entregada');
        $notificationBuilder->setBody(auth()->user()->name, ' agrego evidencia en: ', $task2->title)->setSound('default');
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $token = DB::table('users')->select('notification_token')->where('id',$assigns->admin_task)->first();
        $tokenstring = $token->notification_token;
        if($tokenstring != null){
            $downstreamResponse = FCM::sendTo($tokenstring, $option, $notification);
        }

       return redirect()->route('assign_view_admin', ['assign'=> $assign, 'tasks'=> $tasks, 'users'=> $users]);
    }
    //END Assign-Task View

    public function adminTasks()
    {
        $tasks = DB::table('tasks')->join('users', 'tasks.users_id', '=', 'users.id')->select('users.name', 'tasks.title', 'tasks.initial_date', 'tasks.final_date', 'tasks.objective', 'tasks.description', 'tasks.id', 'tasks.comment', 'tasks.progress')->paginate(10);

        $assign = DB::table('assignment_user')->join('assignment_task', 'assignment_user.assignment_id', '=', 'assignment_task.assignment_id')->join('tasks', 'assignment_task.task_id', '=', 'tasks.id')->join('assignments', 'assignments.id', '=', 'assignment_user.assignment_id')->join('users', 'users.id', '=', 'assignment_user.user_id')->select('tasks.title', 'users.name', 'assignments.initial_date', 'assignments.final_date', 'assignment_user.is_completed', 'assignments.comment', 'assignments.id', 'assignment_user.user_id', 'assignment_task.task_id')->paginate(10);

        $taskassign = DB::table('tasks')->select('id', 'title')->get();

        return view('adminlte::tasks.adminTasks')->with(['tasks' => $tasks, 'assign' => $assign, 'taskassign' => $taskassign]);
    }

    public function editTasks(Task $tasks)
    {
        $tasks         = Task::find($tasks->id);
        $document      = TaskDocument::all();
        $doc           = DB::table('task_documents')->join('tasks', 'task_documents.task_id', '=', 'tasks.id')->select('task_documents.name')->get();
        $task_document = $tasks->taskdocument;

        return view('adminlte::tasks.tasksEdit')->with(['tasks' => $tasks, 'task_document' => $task_document, 'document' => $document, 'doc' => $doc]);
    }

    public function updateTasks(Task $tasks, Request $request)
    {

        $tasks->title        = $request->get('title');
        $tasks->objective    = $request->get('objective');
        $tasks->description  = $request->get('description');
        $tasks->initial_date = $request->get('initial_date');
        $tasks->final_date   = $request->get('final_date');
        $tasks->comment      = $request->get('comment');

        $tasks->save();

        $doc = DB::table('task_documents')->select('name')->where('task_id', '=', $tasks->id)->get();
        if ($request->get('ArchivosT') != null) {
            foreach ($doc as $doc) {
                $request->file('ArchivosT')->storeAs('tareas', $doc->name);
            }
        }
        return redirect()->route('tasks_admin');
    }

    public function deleteTasks(Task $tasks)
    {
        $doc = DB::table('task_documents')->select('name')->where('task_id', '=', $tasks->id)->get();
        foreach ($doc as $doc) {
            Storage::delete('tareas/' . $doc->name);
        }
        $assi_task = DB::table('assignment_task')->select('assignment_id')->where('task_id', '=', $tasks->id)->get();
        foreach ($assi_task as $assi_task) {
            $evi = DB::table('assignment_evidencias')->select('name')->where('assignments_id', '=', $assi_task->assignment_id)->get();
            foreach ($evi as $evi) {
                Storage::delete('evidencias/' . $evi->name);
            }
            $assign_evi = DB::table('assignment_evidencias')->where('assignments_id', '=', $assi_task->assignment_id)->delete();

            $assign_mes = DB::table('assignment_mensajes')->where('id_assignments', '=', $assi_task->assignment_id)->delete();

            $assign_user = DB::table('assignment_user')->where('assignment_id', '=', $assi_task->assignment_id)->delete();

            $assign_task = DB::table('assignment_task')->where('assignment_id', '=', $assi_task->assignment_id)->delete();

            $assigndelete = DB::table('assignments')->where('id', '=', $assi_task->assignment_id)->delete();
        }

        $docdelete = DB::table('task_documents')->where('task_id', '=', $tasks->id)->delete();

        $tasks->delete();

        return redirect()->route('tasks_admin');
    }

    public function editAssign(Assignment $assign)
    {
        $assign = Assignment::find($assign->id);
        $task   = Task::all();
        $user   = User::all();

        $usersassign = $assign->user;
        $taskassign  = $assign->task;

        $hora = DB::table('assignments')->select('hour')->where('id', '=', $assign->id)->first();

        return view('adminlte::tasks.assignEdit')->with(['assign' => $assign, 'users' => $user, 'tasks' => $task, 'usersassign' => $usersassign, 'tasksassign' => $taskassign, 'hora' => $hora]);
    }
    public function updateAssign(Assignment $assign, Request $request)
    {

        $assign               = Assignment::find($assign->id);
        $assign->initial_date = $request->get('initial_date');
        $assign->final_date   = $request->get('final_date');
        $assign->comment      = $request->get('comment');
        $assign->hour         = $request->Hora;

        if ($request->Rep_monday == 1) {
            $assign->rep_monday = $request->Rep_monday;
        } else { $assign->rep_monday = 0;}

        if ($request->Rep_tuesday == 1) {
            $assign->rep_tuesday = $request->Rep_tuesday;
        } else { $assign->rep_tuesday = 0;}

        if ($request->Rep_wednesday == 1) {
            $assign->rep_wednesday = $request->Rep_wednesday;
        } else { $assign->rep_wednesday = 0;}

        if ($request->Rep_thursday == 1) {
            $assign->rep_thursday = $request->Rep_thursday;
        } else { $assign->rep_thursday = 0;}

        if ($request->Rep_friday == 1) {
            $assign->rep_friday = $request->Rep_friday;
        } else { $assign->rep_friday = 0;}

        if ($request->Rep_saturday == 1) {
            $assign->rep_saturday = $request->Rep_saturday;
        } else { $assign->rep_saturday = 0;}

        if ($request->Rep_sunday == 1) {
            $assign->rep_sunday = $request->Rep_sunday;
        } else { $assign->rep_sunday = 0;}
        $assign->save();

        $assign->task()->sync($request->task);
        $assign->user()->sync($request->users);

        return redirect()->route('tasks_admin');
    }

    public function deleteAssign(Assignment $assign)
    {
        $evi = DB::table('assignment_evidencias')->select('name')->where('assignments_id', '=', $assign->id)->get();
        foreach ($evi as $evi) {
            Storage::delete('evidencias/' . $evi->name);
        }
        $assign_evi = DB::table('assignment_evidencias')->where('assignments_id', '=', $assign->id)->delete();

        $assign_mes = DB::table('assignment_mensajes')->where('id_assignments', '=', $assign->id)->delete();

        $assign_user = DB::table('assignment_user')->where('assignment_id', '=', $assign->id)->delete();

        $assign_task = DB::table('assignment_task')->where('assignment_id', '=', $assign->id)->delete();

        $assign->delete();
        return redirect()->route('tasks_admin');
    }
#endregion

#region QUESTION TASK
    public function formulario($task)
    {
        $task      = DB::table('tasks')->where('id', $task)->first();
        $questions = DB::table('task_evaluations')->where('tasks_id', $task->id)->get();
        return view('adminlte::tasks.evaluation.formulario')->with(['task' => $task, 'questions' => $questions]);
    }

    public function create($task, $question_id)
    {
        $task                           = DB::table('tasks')->where('id', $task)->first();
        isset($question_id) ? $question = DB::table('task_evaluations')->where('id', $question_id)->first() : $question = null;
        !empty($question_id) ? $options = DB::table('option_task_evaluations')->where('task_evaluations_id', $question_id)->get() : $options = null;
        return view('adminlte::tasks.evaluation.index')->with(['task' => $task, 'question' => $question, 'options' => $options]);
    }

    public function edit(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $tmp           = TaskEvaluation::find($request->QuestionId);
            $tmp->question = $request->Question;
            $tmp->type     = $request->Type;
            $tmp->save();
            $optionsTmp = OptionTaskEvaluation::where('task_evaluations_id', '=', $tmp->id)->get();
            if (!empty($request->Options)) {
                foreach ($optionsTmp as $oS) {
                    $isContained = false;
                    foreach ($request->Options as $oJ) {
                        if ($oS->id == $oJ[2]) {
                            $isContained = true;
                        }

                    }
                    if (!$isContained) {
                        $delete = DB::table('option_task_evaluations')->where('id', $oS->id)->delete();
                    }
                }
                foreach ($request->Options as $oJ) {
                    if ($oJ[2] == "") {
                        $options                     = new OptionTaskEvaluation;
                        $options->task_evaluation_id = $tmp->id;
                        $options->option             = $oJ[1];
                        $options->validation         = $oJ[0];
                        $options->save();
                    }
                }
            }
            return response()->json($optionsTmp);
        }
    }

    public function save(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $tmp           = new TaskEvaluation;
            $tmp->tasks_id = $request->CourseID;
            $tmp->question = $request->Question;
            $tmp->type     = $request->Type;
            $tmp->save();
            if (!empty($request->Options)) {
                foreach ($request->Options as $op) {
                    $options                      = new OptionTaskEvaluation;
                    $options->task_evaluations_id = $tmp->id;
                    $options->option              = $op[1];
                    $options->validation          = $op[0];
                    $options->save();
                }
            }

            return response()->json($tmp);
        }
    }

    public function delete($question_id)
    {
        $option    = DB::table('option_task_evaluations')->where('task_evaluations_id', $question_id)->delete();
        $tmp       = TaskEvaluation::find($question_id);
        $course_id = $tmp->tasks_id;
        $tmp       = DB::table('task_evaluations')->where('id', $question_id)->delete();
        return redirect()->route('formulario_question_task', ['task' => $course_id]);
        //$route = "location:".route('kardex_question_evaluation', ['course' => $course_id]);
        //header($route);
    }
#end region

#region TASK EVALUATION
    public function intro($tasks)
    {
        $tmp = DB::table('task_evaluations')->where('tasks_id', $tasks)->count();
        return view('adminlte::tasks.evaluation.intro')->with(['tasks' => $tasks, 'pass' => $tmp]);
    }

    public function index($task)
    {

        $tmp      = DB::table('task_evaluations')->where('tasks_id', $task);
        $question = $tmp->paginate(1);
        $option   = DB::table('option_task_evaluations')->where('task_evaluations_id', $question[0]->id)->inRandomorder()->get();
        $ans      = DB::table('answer_task_evaluations')->where('task_evaluations_id', $question[0]->id)->where('user_id', auth()->user()->id)->orderBy('id', 'asc')->get();
        $time     = DB::table('answer_task_evaluations')->select('task_evaluations_id')->where('user_id', auth()->user()->id)->groupBy('task_evaluations_id')->get();

        return view('adminlte::tasks.evaluation.index_eval')->with(['question' => $question, 'option' => $option, 'answers' => $ans, 'time' => $time, 'task' => $task]);
    }

    public function saveResult(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->ruleseva);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $quiz = TaskEvaluation::where('id', '=', $request->question_id)->first();
            $ans  = DB::table('answer_task_evaluations')->where('task_evaluations_id', '=', $request->question_id)->where('user_id', auth()->user()->id)->delete();
            $temp;
            if ($quiz->type == 'multioption' || $quiz->type == 'order') {
                foreach ($request->Answer as $d) {
                    $temp                             = new AnswerTaskEvaluation;
                    $temp->user_id                    = auth()->user()->id;
                    $temp->task_evaluations_id        = $request->question_id;
                    $temp->option_task_evaluations_id = $d;
                    $temp->save();
                }
            } else if ($quiz->type == 'only') {
                $temp                             = new AnswerTaskEvaluation;
                $temp->task_evaluations_id        = $request->question_id;
                $temp->user_id                    = auth()->user()->id;
                $temp->option_task_evaluations_id = $request->Answer;
                $temp->save();
            }

            $porcentage = TaskResult::where('user_id', auth()->user()->id)->where('task_id', $request->CourseId)->first();
            if (count($porcentage) <= 0) {
                $porcentage          = new TaskResult;
                $porcentage->user_id = auth()->user()->id;
                $porcentage->task_id = $request->CourseId;
            }
            $cont              = $this->Score($request->CourseId);
            $porcentage->grade = $cont;
            $porcentage->save();
            return response()->json($cont);

        }
    }

    public function resultsUser($task)
    {
        $question = DB::table('task_evaluations')->where('tasks_id', $task)->get();
        $cont     = 0;
        foreach ($question as $q) {
            $option = DB::table('option_task_evaluations')->where('task_evaluations_id', $q->id)->get();
            $cont += $this->grade($q, $option);
        }

        $result = TaskResult::where('user_id', auth()->user()->id)->where('task_id', $task)->first();

        return view('adminlte::tasks.evaluation.resultsUser')->with(['TrainingUser' => $result, 'count' => $cont]);
    }

    public function grade($q, $option)
    {
        $cont = 0;
        switch ($q->type) {
            case TipoRespuesta::Numeric:
                $cont += count($option);
                break;
            default:
                foreach ($option as $o) {
                    if ($o->validation == TipoRespuesta::Correcto) {
                        $cont++;
                    }

                }
                break;
        }
        return $cont;
    }

    public function Score($task)
    {
        $question = DB::table('task_evaluations')->where('tasks_id', $task)->get();
        $count    = 0;
        $total    = 0;
        foreach ($question as $q) {
            $aux    = 'CAST(validation as SIGNED)';
            $option = DB::table('option_task_evaluations')->where('task_evaluations_id', $q->id)->orderByRaw($aux, 'asc')->get();
            $type   = $q->type;
            switch ($type) {
                case TipoRespuesta::Numeric:
                    $answer = DB::table('answer_task_evaluations')->where('task_evaluations_id', $q->id)->where('user_id', auth()->user()->id)->orderByRaw('id', 'asc')->get();
                    foreach ($option as $o) {
                        $total++;
                        for ($i = 0; $i < count($answer); $i++) {
                            if ($o->id == $answer[$i]->option_task_evaluations_id && $o->validation == $i + 1) {
                                $count++;
                            }

                        }
                    }
                    break;

                case TipoRespuesta::Only:
                    $answer = DB::table('answer_task_evaluations')->where('task_evaluations_id', $q->id)->where('user_id', auth()->user()->id)->first();
                    foreach ($option as $o) {
                        if ($o->validation == 'si') {
                            $total++;
                        }

                        if (isset($answer) && $answer->option_task_evaluations_id == $o->id && $o->validation == TipoRespuesta::Correcto) {
                            $count++;
                        }
                    }
                    break;

                default:
                    $answer = DB::table('answer_task_evaluations')->where('task_evaluations_id', $q->id)->where('user_id', auth()->user()->id)->get();
                    foreach ($option as $o) {
                        if ($o->validation == 'si') {
                            $total++;
                        }

                        foreach ($answer as $a) {
                            if ($a->option_task_evaluations_id == $o->id && $o->validation == TipoRespuesta::Correcto) {
                                $count++;
                            }
                        }
                    }

                    break;
            }
        }
        DB::table('task_results')
            ->where('user_id', auth()->user()->id)
            ->where('task_id', $task)
            ->update(['correct' => $count]);
        return (($count * 100) / $total);
    }
#end region
}
abstract class TipoRespuesta
{
    const Multi    = 'multioption';
    const Only     = 'only';
    const Numeric  = 'order';
    const Correcto = 'si';
}