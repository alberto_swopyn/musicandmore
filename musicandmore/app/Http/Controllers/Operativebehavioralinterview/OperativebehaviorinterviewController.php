<?php

namespace Swopyn\Http\Controllers\Operativebehavioralinterview;

use Swopyn\OperativeBehavioralInterview;
use Swopyn\Tracking;
use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class OperativebehaviorinterviewController extends Controller
{

    public function create(){
        $temp = DB::table('general_personal_datas')->get();
        $sections = DB::table('sections')->where('commands_id',1)->get();
        $questions = array();
        $questionSupports = array();
        $answers = array();
        foreach ($sections as $key => $sec) {
            $tmp = DB::table('questions')->where('sections_id',$sec->id)->get();
            foreach ($tmp as $key) {
                $tmpQuestions = DB::table('question_supports')->where('questions_id',$key->id)->get();
                $tmpAnswers = DB::table('answers')->where('questions_id',$key->id)->get();
                foreach ($tmpQuestions as $qsuppport) {
                    array_push($questionSupports, $qsuppport);
                }
                foreach ($tmpAnswers as $answer) {
                    array_push($answers, $answer);
                }
                array_push($questions, $key);
            }
        }

        $solicitante = DB::table('general_personal_datas')
        ->join('scheduled_interviews as s', 'general_personal_datas.id', '=', 's.general_personal_data_id')->select('general_personal_datas.*','s.interview_date')->where('interview_date', date('Y-m-d'))->get();  
        return view('adminlte::interview_operative.create')->with(['applicants' => $solicitante, 'sections' => $sections, 'questions' => $questions, 'supports' => $questionSupports, 'answers' => $answers]);
    }
    
    protected $rulesexperience =
    [
        'personal_data_id' => 'Required',
        'Experience_1' => 'Required',
        'Experience_2' => 'Required',
        'Experience_3' => 'Required',
        'Experience_4' => 'Required',
        'Experience_5' => 'Required',
        'Experience_6' => 'Required',
        'Experience_7' => 'Required',
    ];

    protected $rulesorder =
    [
        'id' => 'Required',
        'Order_Organization_1' => 'Required',
        'Order_Organization_2' => 'Required',
        'Order_Organization_3' => 'Required',
        'Order_Organization_4' => 'Required'
    ];
    
    protected $rulesnorms =
    [
        'id' => 'Required',
        'Norms_1' => 'Required',
        'Norms_2' => 'Required',
        'Norms_3' => 'Required',
    ];

    protected $rulesbehaviors =
    [
        'id' => 'Required',
        'Behaviors_1' => 'Required',
        'Behaviors_2' => 'Required',
        'Behaviors_3' => 'Required',
        'Behaviors_4' => 'Required',
    ];

    public function addExperience(Request $request){

      $validator = Validator::make(Input::all(), $this->rulesexperience);
      if ($validator->fails()) {
         return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
     } else {

        $temp = DB::table('operative_behavioral_interviews')->where('general_personal_datas_id', $request->personal_data_id)->first();
        if (count($temp)<=0) {
            $OperativeInterview = new OperativeBehavioralInterview;
            $OperativeInterview -> save();

            $experience = OperativeBehavioralInterview::all()->last();
            $experience -> operative_behavioral_interviews_id = $experience -> id;
            $experience -> general_personal_datas_id = $request -> personal_data_id;
        }
        else{

            $experience = OperativeBehavioralInterview::find($temp->id);

        }
        $experience -> experience_ask_1 = $request -> Experience_1;
        $experience -> experience_ask_2 = $request -> Experience_2;
        $experience -> experience_ask_3 = $request -> Experience_3;
        $experience -> experience_ask_4 = $request -> Experience_4;
        $experience -> experience_ask_5 = $request -> Experience_5;
        $experience -> experience_ask_6 = $request -> Experience_6;
        $experience -> experience_ask_7 = $request -> Experience_7;
        $experience -> experience_score = $request -> Score;

        $experience -> save();

        return response()->json($experience);
    }
}

public function addOrder(Request $request){

    $validator = Validator::make(Input::all(), $this->rulesorder);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
        $order = OperativeBehavioralInterview::find($request->id);
        $order -> order_organization_ask_1 = $request -> Order_Organization_1;
        $order -> order_organization_ask_2 = $request -> Order_Organization_2;
        $order -> order_organization_ask_3 = $request -> Order_Organization_3;
        $order -> order_organization_ask_4 = $request -> Order_Organization_4;
        $order -> order_organization_score = $request -> Score;

        $order->save();

        return response()->json($order);
    }
}

public function addNorms(Request $request){

    $validator = Validator::make(Input::all(), $this->rulesnorms);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
        $norms = OperativeBehavioralInterview::find($request->id);
        $norms -> norms_ask_1 = $request -> Norms_1;
        $norms -> norms_ask_2 = $request -> Norms_2;
        $norms -> norms_ask_3 = $request -> Norms_3;
        $norms -> norms_score = $request -> Score;

        $norms->save();

        return response()->json($norms);
    }
}
public function addBehaviors(Request $request){

    $validator = Validator::make(Input::all(), $this->rulesbehaviors);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
        $behaviors = OperativeBehavioralInterview::find($request->id);
        $behaviors -> behaviors_ask_1 = $request -> Behaviors_1;
        $behaviors -> behaviors_ask_2 = $request -> Behaviors_2;
        $behaviors -> behaviors_ask_3 = $request -> Behaviors_3;
        $behaviors -> behaviors_ask_4 = $request -> Behaviors_4;
        $behaviors -> behaviors_score = $request -> Score;
        $behaviors -> final_score = $request -> Score + $behaviors -> experience_score + $behaviors ->order_organization_score + $behaviors -> norms_score;
        $behaviors->save();

        // $sol = $behaviors->general_personal_datas_id; //29
        $temp = DB::table('general_personal_datas')->where('id', $behaviors->general_personal_datas_id)->first();
        $trackingtemp = DB::table('trackings')->where('user_id', $temp->general_personal_data_id)->first();
        $tracking = Tracking::find($trackingtemp->id);
        $tracking->interview = $behaviors->final_score;
        
        $tracking -> save();

        return response()->json($behaviors);
    }
}
}


