<?php

namespace Swopyn\Http\Controllers\Cathedra;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Swopyn\Cathedra;
use Swopyn\Http\Controllers\Controller;
use Swopyn\Subject;
use Swopyn\Http\Requests\SubjectRequest;
use Swopyn\Http\Requests\CathedraRequest;

class CathedraController extends Controller
{
    public function subjects()
    {
        $search = \Request::get('search');
        $sub = Subject::where('name','LIKE','%'.$search.'%')->paginate(10);

        return view('adminlte::cathedra.adminSubject')->with(['sub' => $sub]);
    }

    public function addSubject(Request $request)
    {
        $asignatura       = new Subject;
        $asignatura->name = $request->NombreSub;
        $asignatura->save();

        $sub = Subject::all();

        return redirect()->route('subjects');
    }

    public function deleteSubject(Subject $subject)
    {
        $sub_delete = DB::table('subjects')->where('id', '=', $subject->id)->delete();

        return redirect()->route('subjects');
    }

    public function updateSubject(Subject $subject, Request $request)
    {
        $subject->name = $request->get('NombreSubEdit' . $subject->id);
        $subject->save();

        return redirect()->route('subjects');
    }

    public function index()
    {
        $search = \Request::get('search');

        $cathedra = DB::table('cathedras')
            ->join('subjects', 'cathedras.subject_id', '=', 'subjects.id')
            ->join('employees', 'cathedras.employee_id', '=', 'employees.employee_id')
            ->select('subjects.name as subject_name', 'employees.name', 'cathedras.hour_payment', 'cathedras.id')
            ->where('subjects.name','LIKE','%'.$search.'%')
            ->orderBy('subjects.name','ASC')
            ->paginate(10);
    
        return view('adminlte::cathedra.index')->with(['cathedras' => $cathedra]);
    }

    public function createcath()
    {
        $subject = Subject::all();

        $teacher = DB::table('users')->join('role_user','users.id','role_user.user_id')->select('users.name', 'role_user.user_id', 'role_user.role_id','role_user.status', 'users.email', 'users.id')->where('role_id', 3)->where('status', 1)->orderBy('name', 'asc')->get();

        return view('adminlte::cathedra.create')->with(['subjects' => $subject, 'teachers' => $teacher]);
    }

    public function addcath(Request $request)
    {
        //dd($request);
        $cathedra               = new Cathedra;
        $cathedra->subject_id   = $request->subjects[0];
        $cathedra->employee_id  = $request->teacher[0];
        $cathedra->hour_payment = $request->Pago_hora;
        $cathedra->save();

        return redirect()->route('indexcath');
    }

    public function editcath(Cathedra $cathedra)
    {
        $cathedra = Cathedra::find($cathedra->id);
        $subject  = Subject::all();
        $subjectD = Cathedra::join('subjects', 'cathedras.subject_id', '=', 'subjects.id')->find($cathedra->id);
        $teacher  = DB::table('employees')->where('job_title_profile_id', '=', 4)->get();
        $teacherD = Cathedra::join('employees', 'cathedras.employee_id', '=', 'employees.employee_id')->find($cathedra->id);

        return view('adminlte::cathedra.edit')->with(['cathedra' => $cathedra, 'subjects' => $subject, 'teachers' => $teacher, 'teacherDs' => $teacherD, 'subjectDs' => $subjectD]);
    }

    public function updatecathedra(Cathedra $cathedra, Request $request)
    {
        $cathedra               = Cathedra::find($cathedra->id);
        $cathedra->subject_id   = $request->subjects[0];
        $cathedra->employee_id  = $request->teacher[0];
        $cathedra->hour_payment = $request->Pago_hora;

        $cathedra->save();

        session()->flash('message_update', 'Plan Actualizado');

        $cathedra = Cathedra::simplePaginate(10);
        return redirect()->route('indexcath')->with(['cathedras' => $cathedra]);
    }

    public function delete(Cathedra $cathedra)
    {
        $cathedra->delete();
        $cathedra = Cathedra::simplePaginate(10);
        return redirect()->route('indexcath')->with(['cathedras' => $cathedra]);
    }
}
