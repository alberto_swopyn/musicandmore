<?php

namespace Swopyn\Http\Controllers\Tree;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Swopyn\TreeJobCenter;

class TreeController extends Controller
{
    public function create(){
        return view('adminlte::tree.treejobcenter');
    }
    
    public function read(){
        $tree = DB::table('tree_job_centers')->select('id_inc', 'id','parent','text')->where('company_id',4)->get();
        $args = array();
        foreach ($tree as $key => $dat) {
            $row = array();
            $row['id']= $dat->id;
            $row['parent'] = $dat->parent;
            $row['text'] = $dat->text;
            $row['a_attr']['id_test'] = $dat->id_inc;
            array_push($args, $row);
        }
        return Response::json($args);
    }
    public function write(Request $request){
        $tmp = TreeJobCenter::where('company_id', '=', '4')->get();
        if( $request->ajax()){
            $data = $request->json()->all();
            $this->delete($data, $tmp);
            $this->update($data, $tmp);
            $this->add($data);
        }
    }

    public function update($data, $tmp)
    {
        foreach ($tmp as $t) {
            foreach ($data as $datas) {
                if(isset($datas['a_attr']['id_test'])){
                    if($t->id_inc == $datas['a_attr']['id_test']){
                        $update = TreeJobCenter::where('id_inc','=',$t->id_inc)->first();
                        $update->id = $datas['id'];
                        $update->parent = $datas['parent'];
                        $update->text = $datas['text'];
                        $update->save();
                    }
                }
            }
        }
    }

    public function delete($data, $tmp)
    {
        foreach ($tmp as $tmp) {
            $is_Contained = false;
            foreach ($data as $d) {
                if(isset($d['a_attr']['id_test'])){
                    if ( ($tmp->id_inc == $d['a_attr']['id_test']))
                        $is_Contained = true;
                }
            }
            if(!$is_Contained)
                $del = TreeJobCenter::where('id_inc', '=', $tmp->id_inc)->delete();
        }   
    }

    public function add($data)
    {
        foreach ($data as $var) {
            if (!isset($var['a_attr']['id_test'])) {
                $update = new TreeJobCenter;
                $update->id = $var['id'];
                $update->parent = $var['parent'];
                $update->company_id = 4;
                $update->text = $var['text'];
                $update->save();
            }
        }
    }
}
