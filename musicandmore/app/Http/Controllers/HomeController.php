<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace Swopyn\Http\Controllers;

use Swopyn\Http\Requests;
use Illuminate\Http\Request;
use Swopyn\Tracking;

/**
 * Class HomeController
 * @package Swopyn\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $tracking = Tracking::where('user_id',auth()->user()->id)->first();
        if (isset($tracking->video))
            return redirect('/jobapplication/create');  
        else
            return view('adminlte::home')->with(['trackings' => $tracking]);
    }
    public function pagenotfound()
    {
        return view('errors.404');
    }

    public function prohibit()
    {
        return view('errors.403');
    }

    public function internal()
    {
        return view('errors.500');
    }

    public function indexS()
    {
        return view('adminlite::Security.index');
    }
}