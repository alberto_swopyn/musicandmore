<?php

namespace Swopyn\Http\Controllers\Sampleusers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Swopyn\SampleUsers;
use Swopyn\Http\Controllers\Controller;
use Swopyn\User;
use Illuminate\Http\Request;
use Swopyn\Http\Requests\SampleUsersRequest;

class SampleusersController extends Controller
{

    public function index()
    {
        $sam = DB::table('users')->select('users.id', 'users.name', 'users.email', 'users.sample', 'users.experience', 'users.phone_number')->where('users.sample', '=', '1')->get();

        return view('adminlte::sampleusers.sampleUsers')->with(['sam' => $sam]);
    }
    public function add(Request $request)
    {
        $usuario = User::create([
                'name'=>$request->Nombreuser , 
                'email'=>$request->CorreoUser,
                'password'=>'$2y$10$R0zGQlmr35zLebbCrCxUreRLIO.Jvo7YXVpweV3OZoFB4xB4BXmXi']
                );

        $veri = DB::table('users')->where('id', '=', $usuario->id)->update(['verified' => 1]);
        $exp = DB::table('users')->where('id', '=', $usuario->id)->update(['experience' => $request->experience]);
        $samp = DB::table('users')->where('id', '=', $usuario->id)->update(['sample' => 1]);
        $comp = DB::table('users')->where('id', '=', $usuario->id)->update(['id_company' => $request->company]);
        $rol = DB::table('role_user')->insert(['user_id' => $usuario->id, 'role_id' => 2, 'status' => 1]);
        $tel = DB::table('users')->where('id', '=', $usuario->id)->update(['phone_number' => $request->Telefono]);
        $sampla = DB::table('general_personal_datas')->insert(['general_personal_data_id' => $usuario->id, 'first_name' => $request->get('Nombreuser'), 'phone_number' => $request->get('Telefono'), 'email' => $request->get('CorreoUser')]);
        $pers_add = DB::table('personal_addresses')->insert(['personal_addresses_id' => $usuario->id, 'job_application_id' => $usuario->id]);
        $sample = DB::table('personal_references')->insert(['personal_references_id' => $usuario->id]);
        $track = DB::table('trackings')->insert(['user_id' => $usuario->id, 'video' => 1]);

        return redirect()->route('student_status');
    }

    public function updateSample(User $sample, Request $request)
    {
        $sample->name = $request->get('NombreSamEdit' . $sample->id);
        $sample->email = $request->get('CorreoSamEdit' . $sample->id);
        $sample->experience = $request->get('SamEditExp' . $sample->id);
        $sample->phone_number = $request->get('TelSamEdit' . $sample->id);
        $sample->save();

        return redirect()->route('student_status');
    }

    public function deleteSample(User $sample)
    {
        $sam_delete = DB::table('users')->where('id', '=', $sample->id)->delete();

        return redirect()->route('sample_users');
    }

    public function inscribe($sample)
    {
        $sam = User::find($sample);
        $samp = DB::table('users')->where('id', '=', $sample)->update(['sample' => 0]);

        return redirect()->route('student_status');
    }

}
