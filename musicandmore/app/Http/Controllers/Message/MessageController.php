<?php

namespace Swopyn\Http\Controllers\Message;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Swopyn\Message;

class MessageController extends Controller
{
    public function addInterview(Request $request){

    	$message = New Message;

    	$message->user_id = $request->get('user');
    	$message->assignment_user_id = $request->get('user');
    	$message->email_assignment_user_id = $request->get('email_user');
    	$message->title = 'Entrevista';
    	$message->description = 'El comité técnico quiere platicar contigo';
    	$message->save();
    	$message = New Message;
    	$message->user_id = $request->get('user');
    	$message->assignment_user_id = $request->get('user');
    	$message->email_assignment_user_id = $request->get('email_user');
    	$message->title = 'Prueba Psicométrica';
    	$message->description = 'Tú particiación es importante';
    	$message->save();
    	return redirect()->route('training_index');
    
    }

    public function showMessage(Request $request){
    	$messages = DB::table('messages')->where('user_id',$request->get('user'))->get();
    	return Response::json($messages);

    }
}
