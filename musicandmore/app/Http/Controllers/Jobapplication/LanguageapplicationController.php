<?php
namespace Swopyn\Http\Controllers\Jobapplication;

 use Illuminate\Http\Request;
 use Swopyn\Http\Controllers\Controller;
 use Swopyn\LanguageApplication;
 use Validator;
 use Illuminate\Support\Facades\DB;
 use Illuminate\Support\Facades\Input;
 use Illuminate\Support\Facades\Response;
 use Image;

 class LanguageapplicationController extends Controller
 {
 	protected $rules =
 	[
 	'Lenguaje' => 'Required',
 	'languageread' => 'Required',
 	'languagewrite' => 'Required',
 	'languagespeak' => 'Required',
 	'languagelisten' => 'Required',
 	];
 	public function add(Request $request){
 		$validator = Validator::make(Input::all(), $this->rules);
 		if ($validator->fails()) {
 			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
 		} else {
 			$temp = new LanguageApplication;
 			$temp->language = $request->Lenguaje;
 			
 			$level = (($request->languagewrite+$request->languageread+$request->languagespeak+$request->languagelisten)/4);

 			$temp->level                 = $level;
   		    $temp->level_write           = $request->languagewrite;
    		$temp->level_read            = $request->languageread;
    		$temp->level_speak           = $request->languagespeak;
    		$temp->level_listen          = $request->languagelisten;
 			$temp->job_applications_id = auth()->user()->id;
 			$temp->language_applications_id = auth()->user()->id;
 			$temp->save();

 			return response()->json($temp);
 		}
 	}
 }
