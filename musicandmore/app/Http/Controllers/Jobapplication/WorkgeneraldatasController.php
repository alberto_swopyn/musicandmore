<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
//use Swopyn\Http\Requests\Workgeneraldatas\CreateWorkgeneraldatasRequest;
use Validator;
use Swopyn\WorkGeneralData;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Swopyn\Tracking;
use Image;
use PDF;

class WorkgeneraldatasController extends Controller
{

    protected $rules =
    [            
         //Work_general_datas
        'ConocimientoTrabajo'   => 'Required',
        'FamiliaEmpresa'        => 'Required',
        'Seguro'                => 'Required',
        'PrimerDia'             => 'Required',
        'Puesto_jobapplication' => 'Required',
        'Jobcenter_workgeneral' => 'Required'
    ];

    protected $rulesStudent =
    [            
         //Work_general_datas
        'otros'   => 'Required'
    ];

    public function add(Request $request){

        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
           return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
       } 
       else { 
        $workTemp = DB::table('work_general_datas')->where('work_general_datas_id', $request->user)->first();
        
        if (count($workTemp) <= 0) {
            $work_general_datas                        = New WorkGeneralData;
            $work_general_datas->work_general_datas_id = $request->user;
            $work_general_datas->job_application_id =   $request->user; 
        }
        else{
            $id = $workTemp->id;
            $work_general_datas = WorkGeneralData::find($id);
        }


        $work_general_datas->knowledge_employment  = $request->ConocimientoTrabajo;
        $work_general_datas->family                = $request->FamiliaEmpresa;
        $work_general_datas->enssurance             = $request->Seguro;
        $work_general_datas->first_day             = $request->PrimerDia;
        $work_general_datas->subject_id  = $request->Puesto_jobapplication;
        $work_general_datas->profile_job_centers_id = $request->Jobcenter_workgeneral;

        $work_general_datas->save();

        $docs = DB::table('job_title_documents as JobTitleDocuments')->join('job_title_profiles as JobProfile', 'JobTitleDocuments.job_title_profiles_id', 'JobProfile.job_title_profiles_id')->where('JobProfile.id', $request->Puesto_jobapplication)->get();

        $tracking = Tracking::where('user_id', $request->user)->first();
         // $tracking->job_application = $request->Puesto;
        $tracking->job_title_profile_id = $request->Puesto_jobapplication;
        $tracking->document = count($docs);
        $porcentaje = $this->match();
        $tracking->percentage_match = $porcentaje;

        $tracking->save();

        return response()->json($work_general_datas);
    }
}

public function addStudent(Request $request){

    $validator = Validator::make(Input::all(), $this->rulesStudent);
    if ($validator->fails()) {
       return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
   } 
   else { 
    $workTemp = DB::table('work_general_datas')->where('work_general_datas_id', $request->user)->first();
    
    if (count($workTemp) <= 0) {
        $work_general_datas                        = New WorkGeneralData;
        $work_general_datas->work_general_datas_id = $request->user;
        $work_general_datas->job_application_id =   $request->user; 
    }
    else{
        $id = $workTemp->id;
        $work_general_datas = WorkGeneralData::find($id);
    }


    $work_general_datas->knowledge_employment  = $request->otros;

    $work_general_datas->save();

    return response()->json($work_general_datas);
}
}
public function update(Request $request){

    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
       return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
   } 
   else { 

     $work_general_datas->knowledge_employment  = $request->ConocimientoTrabajo;
     $work_general_datas->family                = $request->FamiliaEmpresa;
     $work_general_datas->enssurance            = $request->Seguro;
     $work_general_datas->change_residence      = $request->Residencia;
     $work_general_datas->first_day             = $request->PrimerDia;
     $work_general_datas->subject_id  = $request->Puesto_jobapplication;
     $work_general_datas->save();

     return response()->json($work_general_datas);
 }

}
public function match()
{
    $porcentaje = 0;
    $temp = DB::table('general_personal_datas')->select('gender','age','civil_status')->where('general_personal_data_id', auth()->user()->id)->first();
    $work = DB::table('work_general_datas')->where('work_general_datas_id', auth()->user()->id)->first();
    $job_title =DB::table('job_title_profiles')->select('job_title_name','gender','min_age','max_age','civil_status')->where('id', $work->job_title_profile_id)->first();
    $languagessol = DB::table('job_title_languages')->select('language','level')->where('job_title_profiles_id',$work->job_title_profile_id)->get();
    $languagesasp = DB::table('language_applications')->select('language','level')->where('job_applications_id', auth()->user()->id)->get();
    $scho_data = DB::table('scholarship_datas')->select('degree_name','academic_grade')->where('scholarship_datas_id', auth()->user()->id)->get();
    $scho_data_sol = DB::table('job_title_scholarship')->select('degree_name','academic_grade')->where('job_title_scholarship_id', $work->job_title_profile_id)->get();

    if ($temp->gender == $job_title->gender || $job_title->gender = 'Indistinto') {
        $porcentaje = $porcentaje + 20;
    }

    if ($temp->civil_status == $job_title->civil_status || $job_title->civil_status = 'Indistinto') {
        $porcentaje = $porcentaje + 20;
    }

    if ($temp->age <= $job_title->max_age && $temp->age >= $job_title->min_age) {
        $porcentaje = $porcentaje + 20;
    }
    $array_aca_grade = array();
    $array_degree_name=array();
    $array_aca_grade_sol = array();
    $array_degree_name_sol =array();

    foreach ($languagessol as $ls) {
        foreach ($languagesasp as $la) {
            if ($ls->language == $la->language) {
                $porcentaje= $porcentaje + (10/count($languagessol));
                if ($la->level >= $ls->level   ) {
                    $porcentaje= $porcentaje + (10/count($languagessol));
                }
            }
        }
    }

    foreach ($scho_data as $scho_data) {
        $aca_grade =DB::table('education_levels')->where('id', $scho_data->academic_grade)->get();
        foreach ($aca_grade as $aca_grade) {
            $array_aca_grade[] = $aca_grade->name;
        }
        $degree_name=Db::table('carrers')->where('id', $scho_data->degree_name)->get();
        foreach ($degree_name as $degree_name) {
            $array_degree_name[] = $degree_name->name;
        }
    }

    foreach ($scho_data_sol as $scho_data_sol) {
        $aca_grade =DB::table('education_levels')->where('id', $scho_data_sol->academic_grade)->get();
        foreach ($aca_grade as $aca_grade) {
            $array_aca_grade_sol[] = $aca_grade->name;
        }
        $degree_name=Db::table('carrers')->where('id', $scho_data_sol->degree_name)->get();
        foreach ($degree_name as $degree_name) {
            $array_degree_name_sol[] = $degree_name->name;
        }
    }

    // foreach ($array_aca_grade as $acg) {
    //     foreach ($array_aca_grade_sol as $acgs) {
    //         if ($acg == $acgs) {
    //             $porcentaje= $porcentaje + (10/count($array_aca_grade_sol));
    //         }
    //     }
    // }

    foreach ($array_degree_name as $adn) {
        foreach ($array_degree_name_sol as $adns) {
            if ($adn == $adns) {
                
            }
        }
    }

    for ($i=0; $i < count($array_aca_grade); $i++) { 
     for ($c=0; $c < count($array_aca_grade_sol); $c++) { 
        if ($array_aca_grade[$i] == $array_aca_grade_sol[$c]) {
            if ($array_degree_name[$i] == $array_degree_name_sol[$c]) {
              $porcentaje= $porcentaje + (10/count($array_aca_grade_sol));
              $porcentaje = $porcentaje + (10/count($array_degree_name_sol));
                }

            }
        }
    }


return $porcentaje;
}
}