<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\ActualStudy;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Swopyn\Tracking;
use Validator;
use Image;
use PDF;

class ActualstudiesController extends Controller
{

  protected $rules =
  [

  'NombreEscuela' => 'Required|min:7|max:50',
  'DireccionEscuela' => 'Required|min:10|max:50',
  'GradoActual' => 'Required|min:5|max:30',
  'PeriodoInicialEscuela' => 'Required',
  'PeriodoFinalEscuela' => 'Required',

  ];

  public function add(Request $request){

   $validator = Validator::make(Input::all(), $this->rules);
   if ($validator->fails()) {
     return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
   } else {
    $temp = DB::table('actual_studies')->where('actual_studies_id',auth()->user()->id)->first();
    if (count($temp) <= 0) {
     $actual_studies = New ActualStudy; 
     $actual_studies->actual_studies_id = $request->get('user');
   }
   else{
    $id = $temp->id;
    $actual_studies = ActualStudy::find($id);
  }
  $actual_studies->school_name = $request->get('NombreEscuela');
  $actual_studies->school_address = $request->get('DireccionEscuela');
  $actual_studies->degree = $request->get('GradoActual');
  $actual_studies->initial_period = $request->get('PeriodoInicialEscuela');
  $actual_studies->final_period = $request->get('PeriodoFinalEscuela');

  $actual_studies->save();
  $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
  $temp = Tracking::find($tmp->id);
  $temp->job_application = 4;
  $temp->save();

  return response()->json($actual_studies);
}
}   

public function update(Request $request){

  $validator = Validator::make(Input::all(), $this->rules);
  if ($validator->fails()) {
   return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
 } 

 else {

   $actual_studies = New ActualStudy;
   $actual_studies->school_name = $request->get('NombreEscuela');
   $actual_studies->school_address = $request->get('DireccionEscuela');
   $actual_studies->degree = $request->get('GradoActual');
   $actual_studies->initial_period = $request->get('PeriodoInicialEscuela');
   $actual_studies->final_period = $request->get('PeriodoFinalEscuela');

   $actual_studies->save();
   $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
   $temp = Tracking::find($tmp->id);
   $temp->job_application = 4;
   $temp->save();

   return response()->json($actual_studies);      
 }
}
}