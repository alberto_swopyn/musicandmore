<?php

namespace Swopyn\Http\Controllers\Jobapplication;

 use Illuminate\Http\Request;
 use Swopyn\Http\Controllers\Controller;
 use Swopyn\OthersApplication;
 use Validator;
 use Illuminate\Support\Facades\DB;
 use Illuminate\Support\Facades\Input;
 use Illuminate\Support\Facades\Response;
 use Image;

class OthersapplicationController extends Controller
{
    protected $rules =
 	[
 	'Others' => 'Required'
 	];
 	public function add(Request $request){
 		$validator = Validator::make(Input::all(), $this->rules);
 		if ($validator->fails()) {
 			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
 		} else {
 			$temp = new OthersApplication;
 			$temp->others = $request->Others;
 			$temp->job_applications_id = auth()->user()->id;
 			$temp->others_applications_id = auth()->user()->id;

 			$temp->save();

 			return response()->json($temp);
 		}
 	}
}
