<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Facades\Response;
use Swopyn\FamilyData;
use Illuminate\Support\Facades\DB;
use Swopyn\Tracking;

class FamilydataController extends Controller
{
  protected $rules =
  [
  'NombreFamiliar' => 'required|min:5|string',
  'Ocupacion' => 'required|min:5',
  'ViveConUsted' => 'required',  
  'Parentesco' => 'required|min:3',
  'NumeroFamiliar' => 'numeric|min:10'
  ];

  public function add(Request $request){

   $validator = Validator::make(Input::all(), $this->rules);
   if ($validator->fails()) {
     return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
   } 
   else {
    //add data 
    $family = New FamilyData;
    $family->family_datas_id = $request->user;
    $family->job_application_id = $request->user;
    $family->relationship = $request->Parentesco;
    $family->name = $request->NombreFamiliar;
    $family->occupation = $request->Ocupacion;
    $family->live_with = $request->ViveConUsted;
    $family->phone_number = $request->NumeroFamiliar; 

    $family->save();
    $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
    $temp = Tracking::find($tmp->id);
    $temp->job_application = 3;
    $temp->save();

    return response()->json($family);
  }

}
public function delete(Request $request){
    $tmp = DB::table('family_datas')->where('id', $request->Id)->delete();
        return response()->json($tmp);
  }
}