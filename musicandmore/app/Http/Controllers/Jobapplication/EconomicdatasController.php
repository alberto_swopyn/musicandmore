<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\EconomicData;
use Swopyn\Tracking;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Validator;
use Image;
use PDF;

class EconomicdatasController extends Controller
{   

  protected $rules =
  [
  'IngresoExtra'  => 'Required',
  'ParejaTrabaja' => 'Required',
  'CasaPropia'    => 'Required',
  'RentaCasa'     => 'Required',
  'CarroPropio'   => 'Required',
  'Deudas'        => 'Required',
  'GastoMensual'  => 'Required|numeric',
  ];

  public function add(Request $request){

   $validator = Validator::make(Input::all(), $this->rules);
   if ($validator->fails()) {  
     return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
   } 
   else { 
    $economicTemp = DB::table('economic_datas')->where('economic_datas_id', $request->user)->first();
    if (count($economicTemp) <= 0) {
     $economic_datas                       = New EconomicData;
     $economic_datas->economic_datas_id    = $request->user;
   }
   else{
    $id = $economicTemp->id;
    $economic_datas = EconomicData::find($id);
  }
  
  $economic_datas->other_income         = $request->IngresoExtra;
  $economic_datas->other_ammount_income = $request->MontoIngreso;
  $economic_datas->couple_works         = $request->ParejaTrabaja;
  $economic_datas->couple_work_place    = $request->LugarPareja;
  $economic_datas->own_home             = $request->CasaPropia;
  $economic_datas->rent_home            = $request->RentaCasa;
  $economic_datas->own_car              = $request->CarroPropio;
  $economic_datas->debs                 = $request->Deudas;
  $economic_datas->pay_month            = $request->MontoDeudas;
  $economic_datas->monthly_expenses     = $request->GastoMensual;

  $economic_datas->save();
  $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
  $temp = Tracking::find($tmp->id);
  $temp->job_application = 8;
  $temp->save();

  return response()->json($economic_datas);
}
}

public function update(Request $request){

 $validator = Validator::make(Input::all(), $this->rules);
 if ($validator->fails()) {
   return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
 } 
 else {

  $economic_datas->other_income         = $request->IngresoExtra;
  $economic_datas->other_ammount_income = $request->MontoIngreso;
  $economic_datas->couple_works         = $request->ParejaTrabaja;
  $economic_datas->couple_work_place    = $request->LugarPareja;
  $economic_datas->own_home             = $request->CasaPropia;
  $economic_datas->rent_home            = $request->RentaCasa;
  $economic_datas->own_car              = $request->CarroPropio;
  $economic_datas->debs                 = $request->Deudas;
  $economic_datas->pay_month            = $request->MontoDeudas;
  $economic_datas->monthly_expenses     = $request->GastoMensual;

  $economic_datas->save();

  return response()->json($economic_datas);
}
}
}