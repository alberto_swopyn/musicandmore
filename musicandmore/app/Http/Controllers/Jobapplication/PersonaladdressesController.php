<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\PersonalAddress;
use Swopyn\JobApplicationDocument;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Swopyn\Tracking;
use Validator;
use Image;
use PDF;

class PersonaladdressesController extends Controller
{
 protected $rules =
 [
  //Personal_addresses
   'Calle' => 'Required|max:50|min:5|string',
   'NumeroExterior' => 'Required|numeric',
   'Fraccionamiento' => 'Required|min:5',
   'CodigoPostal' => 'Required|numeric',
   'Ciudad' => 'Required|string',
   'Estado_personal' => 'Required' ,
 ];   

 public function add(Request $request){

  $validator = Validator::make(Input::all(), $this->rules);
  if ($validator->fails()) {
   return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
 } 
 else {
  $addressTemp = DB::table('personal_addresses')->where('personal_addresses_id', $request->user)->first();
  if (count($addressTemp) <= 0) {
   $personal_addresses = New PersonalAddress;
   $personal_addresses->personal_addresses_id = $request->user;
 }
 else{
  $id = $addressTemp->id;
  $personal_addresses = PersonalAddress::find($id);
}

$personal_addresses->street = $request->Calle;
$personal_addresses->ext_number = $request->NumeroExterior;
$personal_addresses->int_number = $request->NumeroInterior;
$personal_addresses->fraccionamiento = $request->Fraccionamiento;
$personal_addresses->zip_code = $request->CodigoPostal;
$personal_addresses->city = $request->Ciudad;
$personal_addresses->state = $request->Estado_personal;

$personal_addresses->save();
$tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
$temp = Tracking::find($tmp->id);
$temp->job_application = 2;
$temp->save();

return response()->json($personal_addresses);
}
}

public function update(Request $request){

 $validator = Validator::make(Input::all(), $this->rules);
 if ($validator->fails()) {
   return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
 } 
 else {

   $personal_addresses->street = $request->Calle;
   $personal_addresses->ext_number = $request->NumeroExterior;
   $personal_addresses->int_number = $request->NumeroInterior;
   $personal_addresses->fraccionamiento = $request->Fraccionamiento;
   $personal_addresses->zip_code = $request->CodigoPostal;
   $personal_addresses->city = $request->Ciudad;
   $personal_addresses->state = $request->Estado_personal;

   $personal_addresses->save();

   return response()->json($personal_addresses);

 }
}
}
