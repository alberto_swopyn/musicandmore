<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\Http\Requests\Generalknowledge\CreateGeneralknowledgeRequest;
use Swopyn\GeneralKnowledge;
use Validator;
use Swopyn\Tracking;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class GeneralknowledgeController extends Controller
{
    public function add(Request $request){
        $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
        $temp = Tracking::find($tmp->id);
        $temp->job_application = 5;
        $temp->save();
    }
    public function deleteLang(Request $request) 
    { 
        $tmp = DB::table('language_applications')->where('id', $request->Id)->delete(); 
        return response()->json($tmp); 
    }
    public function deleteOffice(Request $request)
    {
        $tmp = DB::table('office_applications')->where('id', $request->Id)->delete();
        return response()->json($tmp);
    }
    public function deleteMac(Request $request)
    {
        $tmp = DB::table('machines_applications')->where('id', $request->Id)->delete();
        return response()->json($tmp);
    }
    public function deleteSoft(Request $request)
    {
        $tmp = DB::table('software_applications')->where('id', $request->Id)->delete();
        return response()->json($tmp);
    }
    public function deleteOther(Request $request)
    {
        $tmp = DB::table('others_applications')->where('id', $request->Id)->delete();
        return response()->json($tmp);
    }
}


