<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\Http\Requests\Workhistory\CreateWorkhistoryRequest;
use Swopyn\WorkHistory;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Swopyn\Tracking;
use Image;
use PDF;
use DateTime;

class WorkhistoryController extends Controller
{

    protected $rules =
    [
    'PeriodoInicial'    => 'required',
    'PeriodoFinal'      => 'required',
    'NombreCompañia'    => 'required',
    'DireccionCompañia' => 'Required',
    'TelefonoCompañia'  => 'Required|numeric',
    'Puesto'            => 'required',
    'IngresoInicial'    => 'required',
    'IngresoFinal'      => 'required',
    'MotivoSeparacion'  => 'required',
    'Experiencia'       => 'required',
    'AreaExperiencia'   => 'required',
    'NombreJefe'        => 'Required',
    'PuestoJefe'        => 'Required',
    'TelefonoJefe'      => 'required|numeric',
    'Referencias'       => 'required'
    ];

    public function add(Request $request){

       $validator = Validator::make(Input::all(), $this->rules);
       if ($validator->fails()) {
           return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
       } 
       else {
            //Images config 
        // $imgimage = Input::file('Fotografia');
        // $fotografia = Image::make($imgimage)->fit(300)->encode('png');

        //calculate time between Initial_period & final_period
        $inicio=$request-> PeriodoInicial;
        $fin=$request-> PeriodoFinal;

        $datetime1=new DateTime($inicio);
        $datetime2=new DateTime($fin);
        $interval=$datetime2->diff($datetime1);
        $experiencia_job = $interval->y. " años ". $interval->m . " meses";

        //add data 
        $work_history                    = New WorkHistory;
        $work_history->work_histories_id = $request->user;
        $work_history->job_application_id = $request->user;
        $work_history->initial_period    = $request->PeriodoInicial;
        $work_history->final_period      = $request->PeriodoFinal;
        $work_history->company_name      = $request->NombreCompañia;
        $work_history->company_address   = $request->DireccionCompañia;
        $work_history->company_phone     = $request->TelefonoCompañia;
        $work_history->job_title         = $request->Puesto;
        $work_history->initial_income    = $request->IngresoInicial;
        $work_history->final_income      = $request->IngresoFinal;
        $work_history->separate_reason   = $request->MotivoSeparacion;
        $work_history->boss_name         = $request->NombreJefe;
        $work_history->boss_job_title    = $request->PuestoJefe;
        $work_history->boss_email        = $request->CorreoJefe;
        $work_history->boss_phone        = $request->TelefonoJefe;
        $work_history->references        = $request->Referencias;
        $work_history->time_job          = $experiencia_job;
        $work_history->experience        = $request->Experiencia;
        $work_history->experience_area   = $request->AreaExperiencia;

        $work_history->save();
        $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
        $temp = Tracking::find($tmp->id);
        $temp->job_application = 6;
        $temp->save();
        
        return response()->json($work_history);
    }

}
public function delete(Request $request){
    $tmp = DB::table('work_histories')->where('id', $request->Id)->delete();
        return response()->json($tmp);
  }
}
