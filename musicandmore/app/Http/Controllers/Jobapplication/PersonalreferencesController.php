<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
//use Swopyn\Http\Requests\Personalreferences\CreatePersonalreferencesRequest;
use Validator;
use Swopyn\PersonalReference;
use Swopyn\Tracking;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;

class PersonalreferencesController extends Controller
{

  protected $rules =
  [            
              //Personal_references
  'NombreReferencia'    => 'Required|min:5|max:30|string',
  'DireccionReferencia' => 'Required|min:10|max:70',
  'RelacionReferencia'  => 'Required',
  'OcupacionReferencia' => 'Required|min:5|max:30',
  'NumeroReferencia'    => 'Required|numeric',
  'TiempoReferencia'    => 'Required'
  ];

  public function add(Request $request){

    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
     return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
   } 
   else {  
    $personal_references                         = New PersonalReference;
    $personal_references->personal_references_id = $request->user;
    $personal_references->job_application_id     = $request->user;
    $personal_references->name                   = $request->get('NombreReferencia');
    $personal_references->address                = $request->get('DireccionReferencia');
    $personal_references->relationship           = $request->get('RelacionReferencia');
    $personal_references->occupation             = $request->get('OcupacionReferencia');
    $personal_references->phone_number           = $request->get('NumeroReferencia');
    $personal_references->meet_time              = $request->get('TiempoReferencia');

    $personal_references->save();
    $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
    $temp = Tracking::find($tmp->id);
    $temp->job_application = 7;
    $temp->save();

    return response()->json($personal_references);

  }
}

public function update(Request $request){

  $validator = Validator::make(Input::all(), $this->rules);
  if ($validator->fails()) {
   return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
 } 
 else {    
  $personal_references->name         = $request->get('NombreReferencia');
  $personal_references->address      = $request->get('DireccionReferencia');
  $personal_references->relationship = $request->get('RelacionReferencia');
  $personal_references->occupation   = $request->get('OcupacionReferencia');
  $personal_references->phone_number = $request->get('NumeroReferencia');
  $personal_references->meet_time    = $request->get('TiempoReferencia');

  $personal_references->save();

  return response()->json($personal_references);
}

}
public function delete(Request $request){
    $tmp = DB::table('personal_references')->where('id', $request->Id)->delete();
        return response()->json($tmp);
  }
}
