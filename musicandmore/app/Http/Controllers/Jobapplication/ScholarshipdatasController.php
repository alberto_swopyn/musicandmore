<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\Http\Requests\Scholarshipdatas\CreateScholarshipdatasRequest;
use Swopyn\ScholarshipData;
use Validator;
use Illuminate\Support\Facades\DB;
use Swopyn\Tracking;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;
use PDF;


class ScholarshipdatasController extends Controller
{

    protected $rules =
    [
        'Carreras' => 'Required',
        'AreaAcademica' => 'Required|min:5',
        'GradoAcademico' => 'Required',
        'TerminoEstudios' => 'Required',
        'NombreEscuela' => 'Required|min:5',
        'DireccionEscuela' => 'Required|min:5',
        'EscuelaPeriodoInicial' => 'Required|date',
        'EscuelaPeriodoFinal' => 'Required|date',
        'CertificadoGrado' => 'Required|min:5'
    ];

    public function add(Request $request){

       $validator = Validator::make(Input::all(), $this->rules);
       if ($validator->fails()) {
           return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
       } 
       else {
        //add data 
        $scholarship = New ScholarshipData;
        $scholarship->scholarship_datas_id = $request->user;
        $scholarship->job_application_id = $request->user;
        $scholarship->degree_name = $request->Carreras;
        $scholarship->education_area = $request->AreaAcademica;
        $scholarship->academic_grade = $request->GradoAcademico;
        $scholarship->finish_studies = $request->TerminoEstudios;
        $scholarship->school_name = $request->NombreEscuela;
        $scholarship->school_address = $request->DireccionEscuela;
        $scholarship->initial_period = $request->EscuelaPeriodoInicial;
        $scholarship->final_period = $request->EscuelaPeriodoFinal;
        $scholarship->degree_certificate = $request->CertificadoGrado;
        $scholarship->save();
        $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
        $temp = Tracking::find($tmp->id);
        $temp->job_application = 4;
        $temp->save();
        
        return response()->json($scholarship);
    }

}
public function delete(Request $request){
    $tmp = DB::table('scholarship_datas')->where('id', $request->Id)->delete();
    return response()->json($tmp);
}
}
