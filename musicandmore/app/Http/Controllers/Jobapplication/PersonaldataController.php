<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Swopyn\Http\Controllers\Controller;
use Swopyn\GeneralPersonalData;
use Swopyn\JobApplicationDocument;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Validator;
use Swopyn\Tracking;
use Image;
use Mail;
use Swopyn\Mail\EmailComment;

//use Swopyn\Http\Requests\Personaldata\CreatePersonaldataRequest;

class PersonaldataController extends Controller
{

  protected $rules =
  [
    'Apellidos' => 'required',
    'PrimerNombre' => 'required',
    'Peso' => 'required',
    'Nacimiento' => 'Required|date',
    'LugarNacimiento' => 'Required|min:4|max:30|string',
    'Estatura' => 'required',
    'Telefono_job' => 'Required|numeric',
    'Celular' => 'Required|numeric',
    'Curp' => 'required|min:18',
    'Rfc' => 'required|min:13',
    'Nss' => 'required|min:11',
    'Dependientes' => 'required',
    'ViveCon' => 'required',
    'EstadoCivil' =>'required',    
    'Genero' =>'required'    
  ];

  protected $rulesStudent =
  [
    'Apellidos' => 'required',
    'PrimerNombre' => 'required',
    'Nacimiento' => 'Required|date',
    'LugarNacimiento' => 'Required|min:4|max:30|string',
    'Telefono_job' => 'Required|numeric',
    'Celular' => 'Required|numeric',
    'Curp' => 'required|min:18',  
    'Genero' =>'required'    
  ];

  public function add(Request $request){

   $validator = Validator::make(Input::all(), $this->rules);
   if ($validator->fails()) {
     return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
   } 
   else {
    $generalTemp = DB::table('general_personal_datas')->where('general_personal_data_id', $request->user)->first();

    if (count($generalTemp) <= 0) {
      $general = New GeneralPersonalData;
      $general->general_personal_data_id = $request->get('user'); 
    }
    else{
      $id = $generalTemp->id;
      $general = GeneralPersonalData::find($id);
    }
    $general->last_name = $request->Apellidos;
    $general->first_name = $request->PrimerNombre;
    $general->birthday = $request->Nacimiento;
    $general->birth_place = $request->LugarNacimiento;
    $general->weight = $request->Peso;
    $general->height = $request->Estatura;
    $general->phone_number = $request->Telefono_job;
    $general->cellphone = $request->Celular;
    $general->curp = $request->Curp;
    $general->rfc = $request->Rfc;
    $general->nss = $request->Nss;
    $general->civil_status = $request->EstadoCivil;
    $general->dependent = $request->Dependientes;
    $general->live_with = $request->ViveCon;
    $general->gender = $request->Genero;
    $general->age = $request->Edad;
    
    $general->save();
    $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
    $temp = Tracking::find($tmp->id);
    $temp->job_application = 1;
    $temp->save();

    return response()->json();
  }

}

public function addStudent(Request $request){

  $validator = Validator::make(Input::all(), $this->rulesStudent);
  if ($validator->fails()) {
    return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
  } 
  else {
   $generalTemp = DB::table('general_personal_datas')->where('general_personal_data_id', $request->user)->first();

   if (count($generalTemp) <= 0) {
     $general = New GeneralPersonalData;
     $general->general_personal_data_id = $request->get('user'); 
   }
   else{
     $id = $generalTemp->id;
     $general = GeneralPersonalData::find($id);
   }
   $general->last_name = $request->Apellidos;
   $general->first_name = $request->PrimerNombre;
   $general->birthday = $request->Nacimiento;
   $general->birth_place = $request->LugarNacimiento;
   $general->phone_number = $request->Telefono_job;
   $general->cellphone = $request->Celular;
   $general->email = $request->Curp;
   $general->gender = $request->Genero;
   
   $general->save();
   $tmp = DB::table('trackings')->where('user_id',auth()->user()->id)->first();
   $temp = Tracking::find($tmp->id);
   $temp->job_application = 1;
   $temp->save();

   return response()->json();
 }

}

public function createDocument(){
  //Requiered Documents 
  $tmp = DB::table('job_title_profiles as JobProfile')->join('work_general_datas as user', 'JobProfile.id', 'user.job_title_profile_id')->join('job_title_documents as JobDocuments', 'JobDocuments.job_title_profiles_id', 'JobProfile.job_title_profiles_id')->where('user.job_application_id', auth()->user()->id)->select('JobDocuments.*')->get();
  
  //User Documents
  $tmp2 = DB::table('job_title_profiles as JobProfiles')->join('work_general_datas as workDatas', 'JobProfiles.id', 'workDatas.job_title_profile_id')->where('workDatas.job_application_id',auth()->user()->id)->first();


  $uploaddoc =  DB::table('job_application_documents as JobApplication')->where('user_id',auth()->user()->id)->where('job_title_profile_id', $tmp2->job_title_profile_id)->get();

  return view('adminlte::jobapplication.createDocument')->with(['documents' => $tmp, 'uploads' => $uploaddoc]);
}

public function addPicture(Request $request)
{
  $file = Input::file('file');
  $img = Image::make($file)->fit(600, 800, function ($constraint) { $constraint->upsize(); })->encode('jpeg');

  $picture = GeneralPersonalData::where('general_personal_data_id',auth()->user()->id)->first();
  $picture->picture = $img;
  $picture->save();
}

public function showPicture($id)
{
  $image = GeneralPersonalData::findOrFail($id);
  $ima = Image::make($image->picture);
  $response = Response::make($ima->encode('jpg'));
  $response->header('Content-Type', 'image/jpeg');
  return $response;
}

public function addDocument(Request $request){
  $file = Input::file('file');
  $jobTitleProfileId = $_POST["Perfil"];
  $nombreImagen = $_POST["NombreImagen"];

  $img = Image::make($file)->fit(600, 800, function ($constraint) { $constraint->upsize(); })->encode('jpeg');

  $document = new JobApplicationDocument;
  $document->user_id = auth()->user()->id;
  $document->job_title_profile_id = $jobTitleProfileId;
  $document->job_title_documents_id = $nombreImagen;
  $document->image = $img;

  $document->save();

  $db = DB::table('job_application_documents')->where('user_id',auth()->user()->id)->where('job_title_profile_id',$jobTitleProfileId)->count();

  $tmp = Tracking::where('user_id', auth()->user()->id)->first();
  $tmp->doc_upload = $db;
  $tmp->save(); 

  return redirect('documents/createdocument');
}

public function deleteDocument($document){
  $document = JobApplicationDocument::find($document);  
  $document->delete();

  $db = DB::table('job_application_documents')->where('user_id',auth()->user()->id)->where('job_title_profile_id',$document->job_title_profile_id)->count();

  $tmp = Tracking::where('user_id', auth()->user()->id)->first();
  $tmp->doc_upload = $db;
  $tmp->save();

  return redirect('documents/createdocument');
} 

public function showDocument($id){
  $image = GeneralPersonalData::findOrFail($id);
  $ima = Image::make($image->picture);
  $response = Response::make($ima->encode('jpg'));
  $response->header('Content-Type', 'image/jpeg');
  return $response;

}

public function showDocumentUser($id){
  $documents = DB::table('job_application_documents')->where('user_id',$id)->get();

  return view('adminlte::jobapplication.showdocument')->with(['documents' => $documents]);


}


public function verifiedDocument(Request $request){
 $docuemnt = DB::table('job_application_documents')->where('id',$request->id)->where('job_title_documents_id',$request->title)->update(['validated' => true]);

 $db = DB::table('job_application_documents')->where('user_id',$request->iduser)->where('validated', '<>', 0)->count();

 $tmp = Tracking::where('user_id', '=', $request->iduser)->first();
 $tmp->doc_verified = $db;
 $tmp->save();
}

public function invalidatedDocument(Request $request){
  $docuemnt = DB::table('job_application_documents')->where('id',$request->id)->where('job_title_documents_id',$request->title)->update(['validated' => false]);

  $db = DB::table('job_application_documents')->where('user_id',$request->iduser)->where('validated', '<>', 0)->count();

 $tmp = Tracking::where('user_id', '=', $request->iduser)->first();
 $tmp->doc_verified = $db;
 $tmp->save();
}

public function updateCommentaryDocument(Request $request)
{
  $docuemnt = DB::table('job_application_documents')->where('id',$request->id)->update(['comment' => $request->messageDocument]);
  $user = DB::table('users')->join('job_application_documents as d','users.id','d.user_id')->select('users.email','users.id')->where('d.id',$request->id)->first();
  $email = new EmailComment($user);
  Mail::to($user)->send($email);
}
}


