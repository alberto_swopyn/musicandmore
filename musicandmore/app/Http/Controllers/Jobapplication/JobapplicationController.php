<?php

namespace Swopyn\Http\Controllers\Jobapplication;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use Swopyn\EconomicData;
use Swopyn\GeneralPersonalData;
use Swopyn\Http\Controllers\Controller;
use Swopyn\PersonalAddress;
use Swopyn\Subject;
use Swopyn\User;
use Swopyn\WorkGeneralData;

class JobapplicationController extends Controller
{
    public function create()
    {
        $search = DB::table('general_personal_datas')->where('general_personal_data_id', auth()->user()->id)->first();
        if (count($search) <= 0) {
            $gpd                           = new GeneralPersonalData;
            $gpd->general_personal_data_id = auth()->user()->id;

            $per_address                        = new PersonalAddress;
            $per_address->personal_addresses_id = auth()->user()->id;
            $per_address->job_application_id    = auth()->user()->id;

            $eco_data                     = new EconomicData;
            $eco_data->economic_datas_id  = auth()->user()->id;
            $eco_data->job_application_id = auth()->user()->id;

            $workdatas                        = new WorkGeneralData;
            $workdatas->work_general_datas_id = auth()->user()->id;
            $workdatas->job_application_id    = auth()->user()->id;

            $gpd->save();
            $per_address->save();
            $eco_data->save();
            $workdatas->save();
        }
        $jobtitleprofile = DB::table('tree_job_profiles')->join('job_title_profiles as jobProfiles', 'id_inc', 'jobProfiles.job_title_profiles_id')->where('company_id', '4')->get();
        $subject         = Subject::all();
        $temp            = DB::table('general_personal_datas')->where('general_personal_data_id', auth()->user()->id)->first();
        $entrevista      = DB::table('scheduled_interviews')->where('general_personal_data_id', auth()->user()->id)->first();
        $dir             = DB::table('personal_addresses')->where('personal_addresses_id', auth()->user()->id)->first();
        $fam             = DB::table('family_datas')->where('family_datas_id', auth()->user()->id)->get();
        $schools         = DB::table('scholarship_datas')->where('scholarship_datas_id', auth()->user()->id)->get();
        $actualschool    = DB::table('actual_studies')->where('actual_studies_id', auth()->user()->id)->first();
        $work_h          = DB::table('work_histories')->where('work_histories_id', auth()->user()->id)->get();
        $reference       = DB::table('personal_references')->where('personal_references_id', auth()->user()->id)->get();
        $economic        = DB::table('economic_datas')->where('economic_datas_id', auth()->user()->id)->first();
        $work_g          = DB::table('work_general_datas')->where('work_general_datas_id', auth()->user()->id)->first();
        $tracking        = DB::table('trackings')->where('user_id', auth()->user()->id)->first();
        $lan             = DB::table('language_applications')->where('job_applications_id', auth()->user()->id)->get();
        //falta sustituir id de language aplications por lanknowledge
        $off       = DB::table('office_applications')->where('job_applications_id', auth()->user()->id)->get();
        $mac       = DB::table('machines_applications')->where('job_applications_id', auth()->user()->id)->get();
        $soft      = DB::table('software_applications')->where('job_applications_id', auth()->user()->id)->get();
        $others    = DB::table('others_applications')->where('job_applications_id', auth()->user()->id)->get();
        $lanknow   = DB::table('language_knowledge')->get();
        $lanlevel  = DB::table('language_level_knowledge')->get();
        $edu_level = DB::table('education_levels')->get();
        $carrers   = DB::table('carrers')->get();
        $jobcenter = DB::table('tree_job_centers')->join('profile_job_centers as jobCenters', 'id_inc', 'jobCenters.profile_job_centers_id')->where('company_id', '4')->get();

        if (isset($entrevista)) {
            $interview_data = DB::table('address_job_centers as address')->join('profile_job_centers as jobCenter', 'address.profile_job_centers_id', 'jobCenter.profile_job_centers_id')->where('jobCenter.id', $entrevista->interview_place)->first();
        } else {
            $interview_data = null;
        }

        return view('adminlte::jobapplication.create')->with(['jobtitleprofiles' => $jobtitleprofile, 'subjects' => $subject, 'info' => $temp, 'dir' => $dir, 'fam' => $fam, 'schools' => $schools, 'actualschool' => $actualschool, 'work_h' => $work_h, 'reference' => $reference, 'economic' => $economic, 'work_g' => $work_g, 'interview_see' => $entrevista, 'tabs_pass' => $tracking, 'jobcenter' => $jobcenter, 'interview_data' => $interview_data, 'lan' => $lan, 'off' => $off, 'mac' => $mac, 'soft' => $soft, 'others' => $others, 'edu_level' => $edu_level, 'carrers' => $carrers, 'lanknow' => $lanknow, 'lanlevel' => $lanlevel]);
    }
    public function createStudent()
    {
        $search = DB::table('general_personal_datas')->where('general_personal_data_id', auth()->user()->id)->first();
        if (count($search) <= 0) {
            $gpd                           = new GeneralPersonalData;
            $gpd->general_personal_data_id = auth()->user()->id;

            $per_address                        = new PersonalAddress;
            $per_address->personal_addresses_id = auth()->user()->id;
            $per_address->job_application_id    = auth()->user()->id;

            $eco_data                     = new EconomicData;
            $eco_data->economic_datas_id  = auth()->user()->id;
            $eco_data->job_application_id = auth()->user()->id;

            $workdatas                        = new WorkGeneralData;
            $workdatas->work_general_datas_id = auth()->user()->id;
            $workdatas->job_application_id    = auth()->user()->id;

            $gpd->save();
            $per_address->save();
            $eco_data->save();
            $workdatas->save();
        }
        $jobtitleprofile = DB::table('tree_job_profiles')->join('job_title_profiles as jobProfiles', 'id_inc', 'jobProfiles.job_title_profiles_id')->where('company_id', '4')->get();
        $subject         = Subject::all();
        $temp            = DB::table('general_personal_datas')->where('general_personal_data_id', auth()->user()->id)->first();
        $entrevista      = DB::table('scheduled_interviews')->where('general_personal_data_id', auth()->user()->id)->first();
        $dir             = DB::table('personal_addresses')->where('personal_addresses_id', auth()->user()->id)->first();
        $fam             = DB::table('family_datas')->where('family_datas_id', auth()->user()->id)->get();
        $schools         = DB::table('scholarship_datas')->where('scholarship_datas_id', auth()->user()->id)->get();
        $actualschool    = DB::table('actual_studies')->where('actual_studies_id', auth()->user()->id)->first();
        $work_h          = DB::table('work_histories')->where('work_histories_id', auth()->user()->id)->get();
        $reference       = DB::table('personal_references')->where('personal_references_id', auth()->user()->id)->get();
        $economic        = DB::table('economic_datas')->where('economic_datas_id', auth()->user()->id)->first();
        $work_g          = DB::table('work_general_datas')->where('work_general_datas_id', auth()->user()->id)->first();
        $tracking        = DB::table('trackings')->where('user_id', auth()->user()->id)->first();
        $lan             = DB::table('language_applications')->where('job_applications_id', auth()->user()->id)->get();
        //falta sustituir id de language aplications por lanknowledge
        $off       = DB::table('office_applications')->where('job_applications_id', auth()->user()->id)->get();
        $mac       = DB::table('machines_applications')->where('job_applications_id', auth()->user()->id)->get();
        $soft      = DB::table('software_applications')->where('job_applications_id', auth()->user()->id)->get();
        $others    = DB::table('others_applications')->where('job_applications_id', auth()->user()->id)->get();
        $lanknow   = DB::table('language_knowledge')->get();
        $lanlevel  = DB::table('language_level_knowledge')->get();
        $edu_level = DB::table('education_levels')->get();
        $carrers   = DB::table('carrers')->get();
        $jobcenter = DB::table('tree_job_centers')->join('profile_job_centers as jobCenters', 'id_inc', 'jobCenters.profile_job_centers_id')->where('company_id', '4')->get();

        if (isset($entrevista)) {
            $interview_data = DB::table('address_job_centers as address')->join('profile_job_centers as jobCenter', 'address.profile_job_centers_id', 'jobCenter.profile_job_centers_id')->where('jobCenter.id', $entrevista->interview_place)->first();
        } else {
            $interview_data = null;
        }

        return view('adminlte::jobapplicationstudent.create')->with(['jobtitleprofiles' => $jobtitleprofile, 'subjects' => $subject, 'info' => $temp, 'dir' => $dir, 'fam' => $fam, 'schools' => $schools, 'actualschool' => $actualschool, 'work_h' => $work_h, 'reference' => $reference, 'economic' => $economic, 'work_g' => $work_g, 'interview_see' => $entrevista, 'tabs_pass' => $tracking, 'jobcenter' => $jobcenter, 'interview_data' => $interview_data, 'lan' => $lan, 'off' => $off, 'mac' => $mac, 'soft' => $soft, 'others' => $others, 'edu_level' => $edu_level, 'carrers' => $carrers, 'lanknow' => $lanknow, 'lanlevel' => $lanlevel]);
    }

    public function editStudent(User $users)
    {
        $u      = DB::table('users')->where('id', $users->id)->select('id')->first()->id;
        $search = DB::table('general_personal_datas')->where('general_personal_data_id', $u)->first();

        if (count($search) <= 0) {
            $gpd                           = new GeneralPersonalData;
            $gpd->general_personal_data_id = $u;

            $per_address                        = new PersonalAddress;
            $per_address->personal_addresses_id = $u;
            $per_address->job_application_id    = $u;

            $eco_data                     = new EconomicData;
            $eco_data->economic_datas_id  = $u;
            $eco_data->job_application_id = $u;

            $workdatas                        = new WorkGeneralData;
            $workdatas->work_general_datas_id = $u;
            $workdatas->job_application_id    = $u;

            $gpd->save();
            $per_address->save();
            $eco_data->save();
            $workdatas->save();
        }
        $jobtitleprofile = DB::table('tree_job_profiles')->join('job_title_profiles as jobProfiles', 'id_inc', 'jobProfiles.job_title_profiles_id')->where('company_id', '4')->get();
        $subject         = Subject::all();
        $temp            = DB::table('general_personal_datas')->where('general_personal_data_id', $u)->first();
        $entrevista      = DB::table('scheduled_interviews')->where('general_personal_data_id', $u)->first();
        $dir             = DB::table('personal_addresses')->where('personal_addresses_id', $u)->first();
        $fam             = DB::table('family_datas')->where('family_datas_id', $u)->get();
        $schools         = DB::table('scholarship_datas')->where('scholarship_datas_id', $u)->get();
        $actualschool    = DB::table('actual_studies')->where('actual_studies_id', $u)->first();
        $work_h          = DB::table('work_histories')->where('work_histories_id', $u)->get();
        $reference       = DB::table('personal_references')->where('personal_references_id', $u)->get();
        $economic        = DB::table('economic_datas')->where('economic_datas_id', $u)->first();
        $work_g          = DB::table('work_general_datas')->where('work_general_datas_id', $u)->first();
        $tracking        = DB::table('trackings')->where('user_id', $u)->first();
        $lan             = DB::table('language_applications')->where('job_applications_id', $u)->get();
        //falta sustituir id de language aplications por lanknowledge
        $off       = DB::table('office_applications')->where('job_applications_id', $u)->get();
        $mac       = DB::table('machines_applications')->where('job_applications_id', $u)->get();
        $soft      = DB::table('software_applications')->where('job_applications_id', $u)->get();
        $others    = DB::table('others_applications')->where('job_applications_id', $u)->get();
        $lanknow   = DB::table('language_knowledge')->get();
        $lanlevel  = DB::table('language_level_knowledge')->get();
        $edu_level = DB::table('education_levels')->get();
        $carrers   = DB::table('carrers')->get();
        $jobcenter = DB::table('tree_job_centers')->join('profile_job_centers as jobCenters', 'id_inc', 'jobCenters.profile_job_centers_id')->where('company_id', '4')->get();

        if (isset($entrevista)) {
            $interview_data = DB::table('address_job_centers as address')->join('profile_job_centers as jobCenter', 'address.profile_job_centers_id', 'jobCenter.profile_job_centers_id')->where('jobCenter.id', $entrevista->interview_place)->first();
        } else {
            $interview_data = null;
        }

        return view('adminlte::jobapplicationstudent.create')->with(['jobtitleprofiles' => $jobtitleprofile, 'subjects' => $subject, 'info' => $temp, 'dir' => $dir, 'fam' => $fam, 'schools' => $schools, 'actualschool' => $actualschool, 'work_h' => $work_h, 'reference' => $reference, 'economic' => $economic, 'work_g' => $work_g, 'interview_see' => $entrevista, 'tabs_pass' => $tracking, 'jobcenter' => $jobcenter, 'interview_data' => $interview_data, 'lan' => $lan, 'off' => $off, 'mac' => $mac, 'soft' => $soft, 'others' => $others, 'edu_level' => $edu_level, 'carrers' => $carrers, 'lanknow' => $lanknow, 'lanlevel' => $lanlevel]);
    }

    public function pdf($id_user)
    {
        $temp         = DB::table('general_personal_datas')->where('general_personal_data_id', $id_user)->first();
        $dir          = DB::table('personal_addresses')->where('personal_addresses_id', $id_user)->first();
        $fam          = DB::table('family_datas')->where('family_datas_id', $id_user)->get();
        $schools      = DB::table('scholarship_datas')->where('scholarship_datas_id', $id_user)->get();
        $actualschool = DB::table('actual_studies')->where('actual_studies_id', $id_user)->first();
        $office       = DB::table('office_applications')->where('job_applications_id', $id_user)->first();
        $machines     = DB::table('machines_applications as ma')->where('ma.job_applications_id', $id_user)->first();
        $software     = DB::table('software_applications as sa')->where('sa.job_applications_id', $id_user)->first();
        $others       = DB::table('others_applications as oa')->where('oa.job_applications_id', $id_user)->first();
        $work_h       = DB::table('work_histories')->where('work_histories_id', $id_user)->get();
        $reference    = DB::table('personal_references')->where('personal_references_id', $id_user)->get();
        $economic     = DB::table('economic_datas')->where('economic_datas_id', $id_user)->first();
        $work_g       = DB::table('work_general_datas')->where('work_general_datas_id', $id_user)->first();
        $jobTitle     = DB::table('job_title_profiles')->where('id', $work_g->job_title_profile_id)->first();
        $lan          = DB::table('language_applications')->where('job_applications_id', $id_user)->get();
        $pdf          = PDF::loadView('adminlte::jobapplication._view', ['temp' => $temp, 'dir' => $dir, 'fam' => $fam, 'schools' => $schools, 'actualschool' => $actualschool, 'office' => $office, 'machines' => $machines, 'software' => $software, 'others' => $others, 'work_h' => $work_h, 'reference' => $reference, 'economic' => $economic, 'work_g' => $work_g, 'jobTitle' => $jobTitle, 'lan' => $lan]);
        return $pdf->stream("Solicitud_empleo.$temp->last_name.$temp->first_name.pdf", array("Attachment" => false));
    }
    public function select_education(Request $request)
    {
        $temp = DB::table('carrers')->where('education_level_id', $request->Edu_level)->get();
        return response()->json($temp);
    }
}
