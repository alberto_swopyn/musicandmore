<?php

namespace Swopyn\Http\Controllers\Jobapplication;

 use Illuminate\Http\Request;
 use Swopyn\Http\Controllers\Controller;
 use Swopyn\MachinesApplication;
 use Validator;
 use Illuminate\Support\Facades\DB;
 use Illuminate\Support\Facades\Input;
 use Illuminate\Support\Facades\Response;
 use Image;

class MachinesapplicationController extends Controller
{
    protected $rules =
 	[
 	'Machines' => 'Required'
 	];
 	public function add(Request $request){
 		$validator = Validator::make(Input::all(), $this->rules);
 		if ($validator->fails()) {
 			return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
 		} else {
 			$temp = new MachinesApplication;
 			$temp->machines = $request->Machines;
 			$temp->job_applications_id = auth()->user()->id;
 			$temp->machines_applications_id = auth()->user()->id;

 			$temp->save();

 			return response()->json($temp);
 		}
 	}
}
