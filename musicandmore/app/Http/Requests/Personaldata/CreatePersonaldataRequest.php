<?php

namespace Swopyn\Http\Requests\Personaldata;

use Illuminate\Foundation\Http\FormRequest;

class CreatePersonaldataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'UltimoNombre' => 'Required',
            'PrimerNombre' => 'Required',

            
        ];
    }
}
