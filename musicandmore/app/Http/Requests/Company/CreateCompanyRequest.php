<?php

namespace Swopyn\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Nombre' => 'Required',
            'Rfc' => 'Required',
            'Direccion' => 'Required',
            'Telefono' => 'Required',
            'Logo' => 'Required',
            'Imagen' => 'Required',
            'NombreCompañia' => 'Required',
            'Persona' => 'Required',
            'CuentaBanco' => 'Required',
            'TipoPago' => 'Required',
            'Giro' => 'Required',
            'Especialidad' => 'Required',
            'NoEmpleados' => 'Required',
            'NoSucursal' => 'Required',

            // 'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ];
    }
}
