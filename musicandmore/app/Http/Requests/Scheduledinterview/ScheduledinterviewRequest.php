<?php

namespace Swopyn\Http\Requests\Scheduledinterview;

use Illuminate\Foundation\Http\FormRequest;

class ScheduledinterviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [   
            'Applicant_id' => 'Required',
            'Fecha_entrevista' => 'Required',
            'Hora_entrevista' => 'Required',
            'Entrevistador' => 'Required',
            'Lugar_entrevista' => 'Required',
        ];
    }
}
