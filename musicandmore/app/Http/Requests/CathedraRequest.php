<?php

namespace Swopyn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CathedraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Pago_hora' => 'required|numeric|between:1,1000000'
        ];
    }
}
