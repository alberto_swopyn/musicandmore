<?php

namespace Swopyn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Nombre' => 'required',
            'initial_date' => 'required',
            'final_date' => 'required',
            'initial_hour' => 'required|max:22:00:00|min:10:00:00',
            'final_hour' => 'required|max:22:00:00|min:10:00:00'
        ];
    }
}
