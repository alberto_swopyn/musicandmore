<?php

namespace Swopyn\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Nombre' => 'required',
            'Objetivo' => 'required',
            'Descripcion' => 'required',
            'Fecha_inicial' => 'required',
            'Fecha_final' => 'required',
            'Comentarios' => 'required'
        ];
    }
}
