<?php

namespace Swopyn\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //TODO Quitar despues de pruebas
        'api/task/task',
        'api/task/assignment'
    ];
}
