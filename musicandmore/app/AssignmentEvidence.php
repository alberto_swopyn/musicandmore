<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;
use DTO\Tasks\AssignmentEvidenceDTO;

class AssignmentEvidence extends Model
{
    protected $table = "assignment_evidences";
    protected $primarykey = "id";
    protected $fillable = ["assignment_evidences_id", "assignments_id", "type", "name", "document"];

    //Nombre de la tabla
    const TableName = 'assignment_evidences';

    //Nombres de los campos de la tabla
    const Id = "id";
    const AssignmentsId = 'assignments_id';
    const Type = 'type';
    const Name = 'name';
    const Document = 'document';


    /**
     * Transforma un AssignmentEvidence a AssignmentEvidenceDTO.
     *
     * @return AssignmentEvidenceDTO
     */
    public function ToDTO() : AssignmentEvidenceDTO
    {
        $assignmentEvidenceDTO = new AssignmentEvidenceDTO();
        $assignmentEvidenceDTO->mergeFromJsonString(json_encode($this->toArray()));
        return $assignmentEvidenceDTO;
    }

    /**
     * Transforma un AssignmentEvidenceDTO a AssignmentEvidence.
     *
     * @param AssignmentEvidenceDTO $assignmentEvidenceDTO
     * @return AssignmentEvidence
     */
    public static function FromDTO(AssignmentEvidenceDTO $assignmentEvidenceDTO) : AssignmentEvidence
    {
        $assignmentEvidence = new AssignmentEvidence();
        $assignmentEvidence->id = $assignmentEvidenceDTO->getId();
        $assignmentEvidence->assignments_id = $assignmentEvidenceDTO->getAssignmentsId();
        $assignmentEvidence->type = $assignmentEvidenceDTO->getType();
        $assignmentEvidence->name = $assignmentEvidenceDTO->getName();
        $assignmentEvidence->document = $assignmentEvidenceDTO->getDocument();

        return $assignmentEvidence;
    }
}
