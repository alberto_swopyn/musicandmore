<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $table='trainings';
    protected $primarykey = 'id';
    protected $fillable = ['id','title','duration','description','start','end','course'];


    public function users(){
    	return $this->belongsToMany(User::class);
    }
    
    public function job_title(){
    	return $this->belongsToMany(JobTitleProfile::class);
    }
    
    public function training_plan(){
        return $this->belongsToMany(TrainingPlan::class);
    }
}
