<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AssignmentPlan extends Model
{
    protected $table      = "assigns_plan";
    protected $primarykey = "id";

    public function plan()
    {
        return $this->belongsToMany(TrainingPlan::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }
}
