<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class OptionEvaluation extends Model
{
    protected $table = "option_evaluations";
    protected $primarykey = "option_evaluations_id";
    protected $fillable = ['id', 'option_evaluations_id', 'question_evaluations_id','option', 'validation', 'value'];
}
