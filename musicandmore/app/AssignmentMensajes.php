<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;
use \DTO\Tasks\AssignmentMessageDTO;

class AssignmentMensajes extends Model
{
    protected $table = "assignment_mensajes";
    protected $primarykey = "id";
    protected $fillable = ['assignment_conversation_id', 'id_assignments', 'id_task', 'id_user', 'message', 'check'];

    //Nombre de la tabla
    const TableName = 'assignment_mensajes';
    const Id = 'id';
    const AssignmentMessagesId = 'assignment_conversation_id';
    const IdAssignments = 'id_assignments';
    const IdTasks = 'id_task';
    const IdUser = 'id_user';
    const Message = 'message'; 
    const Check = 'check';

    /**
     * Transforma un AssignmentMessages a AssignmentMessageDTO.
     *
     * @return AssignmentMessageDTO
     */
    public function ToDTO() : AssignmentMessageDTO
    {
        $assignmentMessageDTO = new AssignmentMessageDTO();
        $assignmentMessageDTO->mergeFromJsonString(json_encode($this->toArray()));
        return $assignmentMessageDTO;
    }

    /**
     * Crea un nuevo AssignmentMessages a partir de un AssignmentMessageDTO
     *
     * @param AssignmentMessageDTO $assignmentMessageDTO
     * @return Task
     */
    public static function FromDTO(AssignmentMessageDTO $assignmentMessageDTO) : AssignmentMessages
    {
        $assignmentMessages = new AssignmentMessages();
        $assignmentMessages->id = $assignmentMessageDTO->getId();
        $assignmentMessages->id_assignments = $assignmentMessageDTO->getIdAssignments();
        $assignmentMessages->id_user = $assignmentMessageDTO->getIdUser();
        $assignmentMessages->message = $assignmentMessageDTO->getMessage();
        $assignmentMessages->check = $assignmentMessageDTO->getCheck();

        return $assignmentMessages;
    }
}
