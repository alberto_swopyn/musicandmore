<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class SampleUsers extends Model
{
    protected $table      = "sample_users";
    protected $primarykey = "id";
    protected $fillable   = ['name', 'email', 'experience'];
}
