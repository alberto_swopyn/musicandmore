<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

	protected $table = 'companies';
	protected $primarykey='id';
	protected $fillable = ['id','id_company','name','rfc','address','logo','image','phone','company_contract_id','contact_id', 'bussines_name','tax_regime','bank_account','pay_type','company_type','specialty','no_employees','no_branch_office'];

   /* public function paytype(){

    	return $this->belongsTo(PayType::class);
    } 

    public function branchoffice(){

    	return $this->hasMany(BranchOffice::class);
    } */

    public function contact(){

    	return $this->belongsTo(Contact::class);
    }

}
