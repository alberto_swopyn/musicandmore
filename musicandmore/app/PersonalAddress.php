<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class PersonalAddress extends Model
{
    protected $table = "personal_addresses";
    protected $primarykey = "personal_addresses_id";
    protected $fillable = ['id', 'personal_addresses_id', 'job_application_id','street', 'ext_number', 'int_number', 'fraccionamiento', 'zip_code', 'city', 'state'];
}
