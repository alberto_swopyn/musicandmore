<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class BranchOffice extends Model
{
	protected $table = 'branch_offices';
	protected $primarykey = 'id';
	protected $fillable =['id','name','rfc','address','phone','no_employees','company_id'];
	
     public function Company(){

    	return $this->belongsTo(Company::class);
    } 
}
