<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AssistanceRes extends Model
{
    protected $table      = 'assistances_res';
	protected $primarykey = 'id';
	protected $fillable   = ['id','id_group','id_teacher', 'evidence' , 'num_assistance', 'date'];
}
