<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class TrainingPlan extends Model
{
	protected $table = "training_plans";
	protected $primarykey = "id";
	protected $fillable = ['id','name','description','module'];

	public function user(){
		return $this->belongsToMany(User::class); 
	}  

	public function training(){
		return $this->belongsToMany(Training::class);
	}

	public function module(){
		return $this->belongsToMany(Module::class);
	}  

	public function job_title(){
    	return $this->belongsToMany(JobTitleProfile::class);
    }
}
