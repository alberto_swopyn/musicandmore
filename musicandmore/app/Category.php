<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = "categories";
	protected $primarykey = "id";
	protected $fillable = ['categories_id', 'name', 'description'];
}
