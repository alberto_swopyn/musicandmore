<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class GoalJobCenter extends Model
{
    protected $table = "goal_job_centers";
    protected $fillable = ['id','goal_job_centers_id','value_january','value_february','value_march','value_april','value_may','value_june','value_july','value_august','value_september','value_october','value_november','value_december'];
}