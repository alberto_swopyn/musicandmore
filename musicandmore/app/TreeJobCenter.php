<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class TreeJobCenter extends Model
{
    protected $table = 'tree_job_centers';
	protected $primarykey='id_inc';
	protected $fillable = ['id_inc','id','company_id','parent','text'];

	public function employee()
    {
        return $this->belongsToMany(Employee::class);
    }

}
