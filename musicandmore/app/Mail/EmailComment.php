<?php

namespace Swopyn\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\URL;
use DB;
use Illuminate\Http\Request;

class EmailComment extends Mailable
{
    protected $user;
    use Queueable, SerializesModels;

    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $comment = DB::table('job_application_documents')->where('id',$request->id)->first();
        return $this->subject('Validación de Documentos')->markdown('emails.emailcomment')->with(['comment' => $comment]);
    }
}
