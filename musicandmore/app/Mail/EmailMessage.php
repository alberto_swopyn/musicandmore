<?php

namespace Swopyn\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\URL;
use DB;
use Illuminate\Http\Request;

class emailMessage extends Mailable
{
    protected $user;
    use Queueable, SerializesModels;

    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
       $task2 = DB::table('assignment_mensajes')->join('users', 'users.id', '=', 'assignment_mensajes.id_user')->join('tasks', 'tasks.id', '=', 'assignment_mensajes.id_task')->select('tasks.title')->first();
        return $this->subject('Ha recibido un mensaje')->markdown('emails.emailmessage')->with(['task' => $task2]);
    }
}
