<?php

namespace Swopyn\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\URL;
use DB;
use Illuminate\Http\Request;

class emailEvidence extends Mailable
{
    protected $user;
    use Queueable, SerializesModels;

    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $task2 = DB::table('assignment_evidencias')->join('assignments', 'assignments.id', '=', 'assignment_evidencias.assignments_id')->join('tasks', 'tasks.id', '=', 'assignment_evidencias.task_id')->select('tasks.title')->first();
        return $this->subject('Entrega de Evidencias')->markdown('emails.emailevidence')->with(['task' => $task2]);
    }
}
