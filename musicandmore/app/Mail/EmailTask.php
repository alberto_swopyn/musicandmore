<?php

namespace Swopyn\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\URL;
use DB;
use Illuminate\Http\Request;

class emailTask extends Mailable
{
    protected $user;
    use Queueable, SerializesModels;

    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $task = DB::table('tasks')->where('id',$request->task)->first();
        return $this->subject('Asignación de Tareas')->markdown('emails.emailtask')->with(['task' => $task]);
    }
}
