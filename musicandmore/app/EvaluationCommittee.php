<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class EvaluationCommittee extends Model
{
    protected $table = "evaluation_committees";
    protected $primarykey = "evaluation_committees_id";
    protected $fillable = ['id', 'evaluation_committees_id', 'candidate_first_name','candidate_last_name', 'proceeds_evaluation', 'territorial_manager', 'human_resources', 'capture_advisor', 'commission_agent', 'duty_manager_1', 'duty_manager_2', 'floor_assistant_1', 'floor_assistant_2', 'floor_assistant_3'];
}
