<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;
use \DTO\Tasks\AssignmentMessageDTO;

class AssignmentMessages extends Model
{
    protected $table = "assignment_messages";
    protected $primarykey = "id";
    protected $fillable = ['assignment_messages_id', 'id_assignments', 'id_user', 'message', 'check'];

    //Nombre de la tabla
    const TableName = 'assignment_messages';
    const Id = 'id';
    const AssignmentMessagesId = 'assignment_messages_id';
    const IdAssignments = 'id_assignments';
    const IdUser = 'id_user';
    const Message = 'message'; 
    const Check = 'check';

    /**
     * Transforma un AssignmentMessages a AssignmentMessageDTO.
     *
     * @return AssignmentMessageDTO
     */
    public function ToDTO() : AssignmentMessageDTO
    {
        $assignmentMessageDTO = new AssignmentMessageDTO();
        $assignmentMessageDTO->mergeFromJsonString(json_encode($this->toArray()));
        return $assignmentMessageDTO;
    }

    /**
     * Crea un nuevo AssignmentMessages a partir de un AssignmentMessageDTO
     *
     * @param AssignmentMessageDTO $assignmentMessageDTO
     * @return Task
     */
    public static function FromDTO(AssignmentMessageDTO $assignmentMessageDTO) : AssignmentMessages
    {
        $assignmentMessages = new AssignmentMessages();
        $assignmentMessages->id = $assignmentMessageDTO->getId();
        $assignmentMessages->id_assignments = $assignmentMessageDTO->getIdAssignments();
        $assignmentMessages->id_user = $assignmentMessageDTO->getIdUser();
        $assignmentMessages->message = $assignmentMessageDTO->getMessage();
        $assignmentMessages->check = $assignmentMessageDTO->getCheck();

        return $assignmentMessages;
    }
}
