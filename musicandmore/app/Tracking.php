<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $table = 'trackings';
    protected $primatykey = 'id';
    protected $fillable =['id', 'user_id', 'job_title_profile_id', 'video', 'lms', 'job_application', 'document', 'interview', 'psychometric', 'service_survey'];


    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function job_title_profile(){
    	return $this->belongsTo(JobTitleProfile::class);
    }

    // public function documents(){
    // 	return $this->belongsTo(JobTitleDocuments::class);
    // }



}



