<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class PersonalReference extends Model
{
    protected $table = "personal_references";
    protected $fillable = ['id','personal_references_id','job_application_id','name','address','relationship','occupation','phone_number','meet_time'];
}
