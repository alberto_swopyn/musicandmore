<?php
namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class LanguageApplication extends Model
{
    protected $table = "language_applications";
    protected $primarykey = "language_applications_id";
    protected $fillable = ['id', 'language', 'level', 'type', 'job_applications_id'];
}
