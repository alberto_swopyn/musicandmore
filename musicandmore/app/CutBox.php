<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class CutBox extends Model
{
   protected $table      = "cut_boxs";
    protected $primarykey = "id";
    protected $fillable   = ['id', 'users_id', 'name_cut', 'total', 'num_cobros','date','total_efec','total_transf','total_cred','total_deb','total_cheq','total_depos'];
}
