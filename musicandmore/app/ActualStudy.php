<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class ActualStudy extends Model
{
    protected $table = "actual_studies";
    protected $primarykey = 'actual_studies_id';
    protected $fillable = ['id','actual_studies_id','job_application_id','school_name','school_address','degree','initial_period','final_period'];
}
