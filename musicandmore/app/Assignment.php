<?php

namespace Swopyn;

use DTO\Tasks\AssignmentDTO;
use Google\Protobuf\Internal\Message;
use Illuminate\Database\Eloquent\Model;
use Swopyn\Interfaces\IModel;

class Assignment extends Model implements IModel
{
    protected $table      = "assignments";
    protected $primarykey = "id";
    protected $fillable   = ['assignments', 'tasks_id', 'users_id', 'initial_date', 'final_date', 'score', 'comment', 'hour', 'rep_monday', 'rep_tuesday', 'rep_wednesday', 'rep_thursday', 'rep_friday', 'rep_saturday', 'rep_sunday'];

    //Nombre de la tabla
    const TableName = 'assignments';

    //Nombres de los campos de la tabla
    const Id          = "id";
    const TaskId      = 'tasks_id';
    const UsersId     = 'users_id';
    const Score       = 'score';
    const InitialDate = 'initial_date';
    const FinalDate   = 'final_date';
    const Comment     = 'comment';
    const IsCompleted = 'is_completed';

    //Alias
    const AliasTable       = "Assignments";
    const AliasId          = self::AliasTable . "Id";
    const AliasTaskId      = self::AliasTable . 'TasksId';
    const AliasUsersId     = self::AliasTable . 'UsersId';
    const AliasScore       = self::AliasTable . 'Score';
    const AliasInitialDate = self::AliasTable . 'InitialDate';
    const AliasFinalDate   = self::AliasTable . 'FinalDate';
    const AliasComment     = self::AliasTable . 'Comment';
    const AliasIsCompleted = self::AliasTable . 'IsCompleted';
    const AliasCreatedAt   = self::AliasTable . 'CreatedAt';
    const AliasUpdateAt    = self::AliasTable . 'UpdateAt';

    //AliasSelect
    const AliasPrompId          = self::TableName . '.' . self::Id;
    const AliasPrompTaskId      = self::TableName . '.' . self::TaskId;
    const AliasPrompUsersId     = self::TableName . '.' . self::UsersId;
    const AliasPrompScore       = self::TableName . '.' . self::Score;
    const AliasPrompInitialDate = self::TableName . '.' . self::InitialDate;
    const AliasPrompFinalDate   = self::TableName . '.' . self::FinalDate;
    const AliasPrompComment     = self::TableName . '.' . self::Comment;
    const AliasPrompIsCompleted = self::TableName . '.' . self::IsCompleted;
    const AliasPrompCreatedAt   = self::TableName . '.' . self::CREATED_AT;
    const AliasPrompUpdateAt    = self::TableName . '.' . self::UPDATED_AT;

    //AliasQuery
    const AliasQueryTable       = self::TableName . '.*';
    const AliasQueryId          = self::AliasPrompId . ' AS ' . self::AliasId;
    const AliasQueryTaskId      = self::AliasPrompTaskId . ' AS ' . self::AliasTaskId;
    const AliasQueryUsersId     = self::AliasPrompUsersId . ' AS ' . self::AliasUsersId;
    const AliasQueryScore       = self::AliasPrompScore . ' AS ' . self::AliasScore;
    const AliasQueryInitialDate = self::AliasPrompInitialDate . ' AS ' . self::AliasInitialDate;
    const AliasQueryFinalDate   = self::AliasPrompFinalDate . ' AS ' . self::AliasFinalDate;
    const AliasQueryComment     = self::AliasPrompComment . ' AS ' . self::AliasComment;
    const AliasQueryIsCompleted = self::AliasPrompIsCompleted . ' AS ' . self::AliasIsCompleted;
    const AliasQueryCreatedAt   = self::AliasPrompCreatedAt . ' AS ' . self::AliasCreatedAt;
    const AliasQueryUpdateAt    = self::AliasPrompUpdateAt . ' AS ' . self::AliasUpdateAt;

    /**
     * Transforma un Assignment a AssignmentsDTO.
     *
     * @return AssignmentDTO
     */
    public function ToDTO(): AssignmentDTO
    {
        $assignmentsDTO = new AssignmentDTO();
        $assignmentsDTO->mergeFromJsonString(json_encode($this->toArray()));
        return $assignmentsDTO;
    }

    /**
     * Crea un nuevo Assignment a partir de un AssignmentsDTO
     *
     * @param Message $assignmentsDTO
     * @return Task
     */
    public static function FromDTO(Message $assignmentsDTO): Assignment
    {
        $assignment               = new Assignment();
        $assignment->id           = $assignmentsDTO->getId();
        $assignment->tasks_id     = $assignmentsDTO->getTaskId();
        $assignment->users_id     = $assignmentsDTO->getUsersId();
        $assignment->score        = $assignmentsDTO->getScore();
        $assignment->initial_date = $assignmentsDTO->getInitialDate();
        $assignment->final_date   = $assignmentsDTO->getFinalDate();
        $assignment->comment      = $assignmentsDTO->getComment();

        return $assignment;
    }

    public function task()
    {
        return $this->belongsToMany(Task::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

}
