<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitle extends Model
{
    protected $table = "job_titles";
    protected $primarykey = "job_titles_id";
    protected $fillable = ['id', 'job_titles_id','name'];
}
