<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class WorkHistory extends Model
{
    protected $table = "work_histories";
    protected $fillable = ['id','work_histories_id','job_application_id','initial_period','final_period','company_name','company_address','company_phone','job_title','initial_income','final_income','separate_reason','boss_name','boss_job_title','boss_email','boss_phone','references','time_job','experience','experience_area'];
}
