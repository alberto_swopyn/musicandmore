<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class WorkCapacity extends Model
{
    protected $table = "work_capacities";
    protected $fillable = ['id','work_capacities_id','profile_job_centers_id','jobtitle_capacity','quantity'];
}