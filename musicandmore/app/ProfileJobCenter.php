<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class ProfileJobCenter extends Model
{
    protected $table = "profile_job_centers";
    protected $primarykey = 'profile_job_centers_id';
    protected $fillable = ['id','profile_job_centers_id','name','code','image','type','total'];
}