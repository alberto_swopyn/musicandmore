<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
   protected $table      = "discounts";
    protected $primarykey = "id";
    protected $fillable   = ['id','users_id', 'description', 'quantity','title'];
}
