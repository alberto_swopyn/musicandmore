<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleAttitude extends Model
{
    protected $table = "job_title_attitudes";
    protected $primarykey = "id";
    protected $fillable = ['id', 'job_title_attitudes_id', 'job_title_profiles_id','attitude_name'];
}
