<?php

namespace Swopyn\ModelCollections;

use Illuminate\Database\Eloquent\Collection;
use DTO\Tasks\TasksDTO;
use Swopyn\Interfaces\ICollectionModel;
use Swopyn\ProtocolBuffers\Utilities\DTOUtilities;
use Google\Protobuf\Internal\Message;

class TaskCollection implements ICollectionModel
{

    private $collection;

    function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function ToDTO()
    {
        $tasksList = array();
        $tasksList['list'] = $this->collection->toArray();
        $tasksList = DTOUtilities::SerialiceArrayToDTO(new TasksDTO(), $tasksList);

        return $tasksList;
    }

    public static function FromDTO(Message $taskDTO)
    {
        
    }

    public function toJson()
    {
        return $this->collection->toJson();
    }
}
