<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'modules';
	protected $primarykey='id';
	protected $fillable = ['id','name'];

	public function training_plan(){
        return $this->belongsToMany(TrainingPlan::class);
    }

    public function training(){
        return $this->belongsToMany(Training::class);
    }
}