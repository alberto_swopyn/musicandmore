<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Classr extends Model
{
    protected $table      = "classroom";
    protected $primarykey = "id";
    protected $fillable   = ['id','name'];
}
