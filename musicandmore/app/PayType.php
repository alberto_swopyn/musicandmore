<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class PayType extends Model
{
	protected $table = 'pay_types';
	protected $primarykey = 'id';
	protected $fillable = ['id','name'];

    public function company(){

    	return $this->belongsTo(Company::class);
    } 

    
}
