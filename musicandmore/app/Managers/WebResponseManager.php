<?php

namespace Swopyn\Managers;

use Swopyn\Message;
use Swopyn\Interfaces\IModel;
use Swopyn\ProtocolBuffers\Controllers\DTOWebResponseController;


class WebResponseManager
{
    public static function SendResponse($obj)
    {
        if(!array_key_exists("CONTENT_TYPE", $_SERVER))
        {
            WebResponseManager::ToJson($obj);
        } else if($_SERVER["CONTENT_TYPE"] == ContentType::ProtobufString || $_SERVER["CONTENT_TYPE"] == ContentType::ProtobufJson)
        {
            DTOWebResponseController::SendResponseDTO($obj);
        }
        else
        {
            WebResponseManager::ToJson($obj);
        }
    }

    public static function ToJson($obj)
    {
        if($obj instanceof IModel)
        {
            echo $obj->toJson();
        } else if(is_array($obj))
        {
            echo json_encode($obj);
        }
        else
        {
            throw new \Exception("Tipo no soportado");
        }
    }
}

class ContentType
{
    const ProtobufString = "application/x-protobuf";
    const ProtobufJson = "application/x-protobuf-json";
    const Json = "application/json";
}