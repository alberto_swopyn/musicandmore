<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = "documents";
    protected $primarykey = 'documents_id';
    protected $fillable = ['id','documents_id','birth_certificate','proof_address', 'identification','study_certificate','cv'];
}
