<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AddressJobCenter extends Model
{
    protected $table = "address_job_centers";
    protected $fillable = ['id','address_job_centers_id','street','num_ext','num_int','colonia','zip_code','location','municipality','state','country','ubication','phone','email'];
}
