<?php namespace Swopyn;

use Acoustep\EntrustGui\Contracts\HashMethodInterface;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, ValidatingModelInterface, HashMethodInterface
{
    use Authenticatable, CanResetPassword, ValidatingModelTrait, EntrustUserTrait;
    use Notifiable;

    protected $throwValidationExceptions = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'email_token', 'notification_token'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $hashable = ['password'];

    protected $rulesets = [

        'creating' => [
            'email'    => 'required|email|unique:users',
            'password' => 'required',
        ],

        'updating' => [
            'email'    => 'required|email|unique:users',
            'password' => '',
        ],
    ];

    public function entrustPasswordHash()
    {
        $this->password = Hash::make($this->password);
        $this->save();
    }

    public function tracking()
    {

        return $this->hasOne(Tracking::class);
    }

    public function trainings()
    {
        return $this->belongsToMany(Training::class);
    }

    public function training_plan()
    {
        return $this->belongsToMany(TrainingPlan::class);
    }

    public function task()
    {
        return $this->belongsToMany(Task::class);
    }

    public function group_alumn()
    {
        return $this->belongsToMany(GroupAlumn::class);
    }

}
