<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class ExperienceTeam extends Model
{
    protected $table = "experience_teams";
    protected $primarykey = "experience_teams_id";
    protected $fillable = ['id', 'experience_teams_id', 'evaluation_committees_id','sale_1', 'sale_2', 'sale_3', 'sale_4', 'sale_5', 'inventory_1', 'inventory_2', 'inventory_3', 'inventory_4', 'inventory_5', 'customer_service_1', 'customer_service_2', 'customer_service_3', 'customer_service_4','customer_service_5', 'cash_management_1', 'cash_management_2', 'cash_management_3', 'cash_management_4', 'cash_management_5', 'since_performance_1', 'since_performance_2', 'since_performance_3', 'since_performance_4', 'since_performance_5', 'salary_1', 'salary_2', 'salary_3', 'salary_4', 'salary_5', 'role_assignment'];
}
