<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AssigPlan extends Model
{
    protected $table      = "assignment_plan_training_plan";
    protected $primarykey = "id";
    protected $fillable   = ['assignment_plan_id', 'training_plan_id'];
}
