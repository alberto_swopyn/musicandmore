<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class ScheduledInterview extends Model
{
    protected $table = 'scheduled_interviews';
    protected $primarykey = 'scheduled_interviews_id';
    protected $fillable = ['id','scheduled_interviews_id','general_personal_data_id','interview_date','interview_time','interviewer_name', 'interview_place'];
}
