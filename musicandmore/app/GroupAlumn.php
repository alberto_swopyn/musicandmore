<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class GroupAlumn extends Model
{
    protected $table      = 'groups_alumns';
	protected $primarykey = 'id';
	protected $fillable   = ['id_group', 'user_id','id'];

public function group()
    {
        return $this->belongsTo(Group::class);
    }
	
}
