<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleResponsability extends Model
{
    protected $table = "job_title_responsabilities";
    protected $primarykey = "job_title_responsabilities_id";
    protected $fillable = ['id', 'job_title_responsabilities_id', 'job_title_profiles_id','responsability_name'];
}
