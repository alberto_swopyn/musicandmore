<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class OptionTaskEvaluation extends Model
{
    protected $table      = "option_task_evaluations";
    protected $primarykey = "id";
    protected $fillable   = ['task_evaluations_id', 'option', 'validation', 'value'];
}
