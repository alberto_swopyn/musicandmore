<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class GeneralKnowledge extends Model
{
    protected $table = "general_knowledges";
    protected $primarykey = "general_knowledges_id";
    protected $fillable  = ['id', 'general_knowledges_id','job_application_id', 'lenguage', 'level', 'office', 'machines', 'software', 'other_jobs'];
}
