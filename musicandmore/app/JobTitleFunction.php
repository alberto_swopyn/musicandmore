<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleFunction extends Model
{
    protected $table = "job_title_functions";
    protected $primarykey = "job_title_functions_id";
    protected $fillable = ['id', 'job_title_functions_id', 'job_title_profiles_id','function_name'];
}
