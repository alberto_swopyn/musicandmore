<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobDescription extends Model
{
	protected $table = "job_descriptions";
	protected $primarykey = "job_descriptions_id";
	protected $filleable = ['id', 'job_descriptions_id', 'job_areas_id', 'objetive', 'activities', 'mision', 'responsabilities'];


	//duda sobre llave foranea id_job_areas
}
