<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobTitleProfile extends Model
{
    protected $table = "job_title_profiles";
    protected $primarykey = "id";
    protected $fillable = ['id', 'job_title_profiles_id', 'job_title_name', 'job_title_objetive', 'min_salary', 'max_salary', 'function_type','min_age', 'max_age', 'gender','civil_status', 'scholarship', 'college_career', 'interview_type', 'contract'];


    public function tracking(){

        return $this->hasMany(Tracking::class, 'fk_profile', 'job_title_profile_id');
    }

    public function training_plan(){
    	return $this->belongsToMany(TrainingPlan::class);
    }
    

}
