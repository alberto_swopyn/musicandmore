<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class EconomicData extends Model
{
    protected $table = "economic_datas";
    protected $primarykey = 'economic_datas_id';
    protected $fillable = ['id','economic_datas_id','job_application_id','other_income','other_ammount_income','couple_works','couple_works_place','own_home','rent_home','own_car','debs','pay_month','monthly_expenses'];
}
