<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
	protected $table = "job_applications";
	protected $primarykey = 'job_applications_id';
	protected $fillable = ['id','job_applications_id'];
}
