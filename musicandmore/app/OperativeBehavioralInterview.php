<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class OperativeBehavioralInterview extends Model
{
    protected $table = "operative_behavioral_interviews";
    protected $primarykey = "operative_behavioral_interviews_id";
    protected $fillable = ['id', 'operative_behavioral_interviews_id','general_personal_datas_id','experience_ask_1', 'experience_ask_2', 'experience_ask_3', 'experience_ask_4', 'experience_ask_5', 'experience_ask_6', 'experience_ask_7', 'order_organization_ask_1', 'order_organization_ask_2', 'order_organization_ask_3', 'order_organization_ask_4', 'norms_ask_1', 'norms_ask_2', 'norms_ask_3', 'behaviors_ask_1', 'behaviors_ask_2', 'behaviors_ask_3', 'behaviors_ask_4'];
}
