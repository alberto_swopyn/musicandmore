<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class DebitReport extends Model
{
    protected $table      = "debit_reports";
    protected $primarykey = "id";
    protected $fillable   = ['user_id', 'type_pay', 'descripction', 'mount', 'cargo', 'mount_total', 'status'];
}
