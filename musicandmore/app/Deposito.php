<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Deposito extends Model
{
    protected $table      = "depositos";
    protected $primarykey = "id";
    protected $fillable   = ['id', 'users_id', 'cutbox_id', 'image','date','folio','aut','banco', 'total', 'cuenta'];
}
