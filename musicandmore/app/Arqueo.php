<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Arqueo extends Model
{
    protected $table      = "arqueos";
    protected $primarykey = "id";
    protected $fillable   = ['id', 'users_id','cutbox_id', 'mil', 'quinientos', 'doscientos','cien','cincuenta','veinte','diez','cinco','dos','uno','cincuenta_centavos','veinte_centavos','diez_centavos','date','total_arqueo'];
}
