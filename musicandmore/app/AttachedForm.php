<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AttachedForm extends Model
{
    protected $table = "attached_forms";
    protected $primarykey = "id";
    protected $fillable = ['attached_forms_id', 'categories_id'];
}
