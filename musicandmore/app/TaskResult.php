<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class TaskResult extends Model
{
    protected $table      = 'task_results';
    protected $primarykey = 'id';
    protected $fillable   = ['user_id', 'task_id', 'grade', 'correct'];
}
