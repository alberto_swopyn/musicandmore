<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $table      = 'contracts';
	protected $primarykey = 'contracts_id';
	protected $fillable   = ['id', 'contracts_id', 'users_id', 'download_file', 'send_file', 'file'];
}
