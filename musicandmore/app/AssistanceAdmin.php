<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AssistanceAdmin extends Model
{
    protected $table      = 'assistances_admin';
	protected $primarykey = 'id';
	protected $fillable   = ['id','id_group','id_teacher','assistance', 'date'];
}
