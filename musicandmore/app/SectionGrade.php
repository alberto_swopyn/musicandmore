<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class SectionGrade extends Model
{
    protected $table = 'section_grades';
    protected $primarykey = 'id';
    protected $fillable = ['section_grades_id', 'sections_id', 'users_id', 'grade'];

    const Table = 'section_grades';
    const Id = 'id';
    const SectionGradeId = 'section_grades_id';
    const UsersId = 'users_id';
    const SectionId = 'sections_id';
    const Grade = 'grade';
}
