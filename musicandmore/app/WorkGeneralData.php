<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class WorkGeneralData extends Model
{
    protected $table = "work_general_datas";
    protected $primarykey = 'id';
    protected $fillable = ['id','work_general_datas_id','job_application_id','knowledge_employment','family','syndicate','enssurance','traveling','change_residence','first_day','subject_id'];

}
