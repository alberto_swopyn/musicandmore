<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AttachedFormAnswer extends Model
{
    protected $table = "attached_form_answers";
    protected $primarykey = "id";
    protected $fillable = ["attached_form_answers_id", "attached_form_questions_id", "text"];
}
