<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobApplicationDocument extends Model
{
    protected $table = "job_application_documents";
    protected $primarykey = "id";
    protected $fillable = ['id', 'user_id','job_title_profile_id','name','image','comment','validated'];
}
