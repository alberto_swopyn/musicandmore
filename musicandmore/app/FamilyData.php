<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class FamilyData extends Model
{
    protected $table = "family_datas";
    protected $primarykey = "family_datas_id";
    protected $fillable = ['id', 'family_datas_id', 'job_application_id','relationship', 'name', 'occupation', 'live_with', 'phone_number'];
}
