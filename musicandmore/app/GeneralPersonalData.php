<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class GeneralPersonalData extends Model
{
    protected $table = "general_personal_datas";
    protected $primarykey = "general_personal_datas_id";
    protected $fillable = ['id', 'general_personal_datas_id','job_application_id','first_name', 'last_name', 'gender', 'birthday', 'birth_place', 'weight', 'height', 'phone_number', 'cellphone', 'picture', 'civil_status', 'live_with', 'dependent', 'curp', 'rfc', 'nss'];
}
