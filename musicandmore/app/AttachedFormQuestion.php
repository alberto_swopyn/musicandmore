<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AttachedFormQuestion extends Model
{
    protected $table = "attached_form_questions";
    protected $primarykey = "id";
    protected $fillable = ['attached_form_questions_id', 'attached_forms_id', 'type', 'text'];
}
