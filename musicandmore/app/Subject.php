<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table      = "subjects";
    protected $primarykey = "id";
    protected $fillable   = ['name'];
}
