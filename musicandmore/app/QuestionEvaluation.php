<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class QuestionEvaluation extends Model
{
    protected $table = "question_evaluations";
    protected $primarykey = "id";
    protected $fillable = ['id', 'question_evaluations_id', 'job_title_name','question', 'type', 'value'];
}
