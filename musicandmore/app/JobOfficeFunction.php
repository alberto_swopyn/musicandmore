<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class JobOfficeFunction extends Model
{
    protected $table = "job_office_functions";
    protected $primarykey = "job_office_functions_id";
    protected $fillable = ['id', 'job_office_functions_id', 'job_title_profiles_id','function'];
}
