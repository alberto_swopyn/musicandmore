<?php

namespace Swopyn;

use Illuminate\Database\Eloquent\Model;

class AssigUser extends Model
{
    protected $table      = "assignment_plan_user'";
    protected $primarykey = "id";
    protected $fillable   = ['assignment_plan_id', 'user_id'];
}
