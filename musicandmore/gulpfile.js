// https://laravel.com/docs/5.3/elixir
// sudo npm install -g gulp    Coamdno para correr por primera vez el gulp.
var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.scripts(['resources/assets/js/app/**//*.js'], '../Public/musicandmore/js/build/app.js');
});


/*
//Para generar el min.js
var gulp = require('gulp');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

var jsFiles = 'resources/assets/js/app/**//*.js';
jsDest = '../Public/circlek/js/build/';

gulp.task('default', function() {
  return gulp.src(jsFiles)
      .pipe(rename({suffix: '.min'}))
      .pipe(uglify())
      .pipe(concat('app.min.js'))
      .pipe(gulp.dest(jsDest));
  });
*/