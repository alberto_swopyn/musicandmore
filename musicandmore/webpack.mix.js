const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public_html/Circle/js')
   .js('resources/assets/js/app-landing.js', 'public_html/Circle/js/app-landing.js')
   .sass('resources/assets/sass/app.scss', 'public_html/Circle/css')
   .less('node_modules/bootstrap-less/bootstrap/bootstrap.less', 'public_html/Circle/css/bootstrap.css')
   .less('resources/assets/less/adminlte-app.less','public_html/Circle/css/adminlte-app.css')
   .less('node_modules/toastr/toastr.less','public_html/Circle/css/toastr.css')
   .combine([
       'public_html/Circle/css/app.css',
       'node_modules/admin-lte/dist/css/skins/_all-skins.css',
       'public_html/Circle/css/adminlte-app.css',
       'node_modules/icheck/skins/square/blue.css',
       'public_html/Circle/css/toastr.css'
   ], 'public_html/Circle/css/all.css')
   .combine([
       'public_html/Circle/css/bootstrap.css',
       'resources/assets/css/main.css'
   ], 'public_html/Circle/css/all-landing.css')
   //APP RESOURCES
   .copy('resources/assets/img/*.*','public_html/Circle/img')
   //VENDOR RESOURCES
   .copy('node_modules/font-awesome/fonts/*.*','public_html/Circle/fonts/')
   .copy('node_modules/ionicons/dist/fonts/*.*','public_html/Circle/fonts/')
   .copy('node_modules/admin-lte/bootstrap/fonts/*.*','public_html/Circle/fonts/bootstrap')
   .copy('node_modules/admin-lte/dist/css/skins/*.*','public_html/Circle/css/skins')
   .copy('node_modules/admin-lte/dist/img','public_html/Circle/img')
   .copy('node_modules/admin-lte/plugins','public_html/Circle/plugins')
   .copy('node_modules/icheck/skins/square/blue.png','public_html/Circle/css')
   .copy('node_modules/icheck/skins/square/blue@2x.png','public_html/Circle/css');
